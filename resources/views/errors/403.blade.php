@extends('layouts.app')

@section('content')
    <div class="container-fluid">
        <div class="row">
            <div class="col-md-12">
                <div class="row">
                    <div class="col-lg-8 col-md-12 vendorbanner">
                        @if ($bannerPath != '')
                            <img src="{{ $bannerPath }}">
                        @endif
                    </div>
                    <div class="col-lg-4 hidden-md-down">
                        <div class="vendorabout">
                            <h3>About Us</h3>{{ $vendor->company_info }}
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="row">
            <div class="col-md-12 text-right">
                {{ Breadcrumbs::render('vendor_page', $vendor) }}
            </div>
        </div>
        <div class="row">
            <div class="sidebar col-xl-2 col-lg-4 order-lg-1">
                <button class="sidebar-toggle position-left" data-toggle="modal" data-target="#modalShopFilters"><i class="icon-layout"></i></button>
                <aside class="sidebar-offcanvas">
                    @if ($smallBannerPath != '')
                        <img src="{{ $smallBannerPath }}">
                    @endif

                    <section class="vendorinfo">
                        <ul>
                            <li><b>COMPANY: </b> {{ $vendor->company_name }}</li>
                            <li><b>ADDRESS: </b> {{ $vendor->billing_address }}</li>
                            <li><b>CITY:</b> {{ $vendor->billing_city }}</li>
                            <li><b>ZipCode:</b> {{ $vendor->billing_zip }}</li>
                            <li><b>Phone:</b> {{ $vendor->billing_phone }}</li>
                        </ul>
                    </section>
                    <section class="widget widget-categories">
                        <h3 class="widget-title">VENDOR CATEGORY</h3>
                        <ul>
                            @foreach($vendor->parentCategories as $cat)
                                <li><a href="{{ route('vendor_category_page', ['category' => $cat->id]) }}">{{ $cat->name }} ({{ $cat->count }})</a></li>
                            @endforeach
                        </ul>
                    </section>
                </aside>
            </div>

            <div class="content col-xl-10 col-lg-8 order-lg-2">
                <div class="row">
                    <div class="col-md-12 text-center">
                        <img src="{{ asset('images/Access_Denied.png') }}" width="200px"> <br><br>
                        <h1>This Vendor has restricted your access to their page.</h1>
                        <p>Due to special conditions placed by this vendor, access to this page is not available at this time. Please contact the vendor directly to get more information on how to obtain access. We apologize for the inconvenience. </p>
                    </div>
                </div>
            </div>
        </div>
    </div>
@stop

<div class="modal fade" id="modalShopFilters" tabindex="-1">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <h4 class="modal-title">Filters</h4>
                <button class="close" type="button" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
            </div>
            <div class="modal-body">
                @if ($smallBannerPath != '')
                    <img src="{{ $smallBannerPath }}">
                @endif

                <section class="vendorinfo">
                    <ul>
                        <li><b>COMPANY: </b> {{ $vendor->company_name }}</li>
                        <li><b>ADDRESS: </b> {{ $vendor->billing_address }}</li>
                        <li><b>CITY:</b> {{ $vendor->billing_city }}</li>
                        <li><b>ZipCode:</b> {{ $vendor->billing_zip }}</li>
                        <li><b>Phone:</b> {{ $vendor->billing_phone }}</li>
                    </ul>
                </section>
                <section class="widget widget-categories">
                    <h3 class="widget-title">VENDOR CATEGORY</h3>
                    <ul>
                        @foreach($vendor->parentCategories as $cat)
                            <li><a href="{{ route('vendor_category_page', ['category' => $cat->id]) }}">{{ $cat->name }} ({{ $cat->count }})</a></li>
                        @endforeach
                    </ul>
                </section>
            </div>
        </div>
    </div>
</div>