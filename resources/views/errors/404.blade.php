<!DOCTYPE html>
<html lang="{{ app()->getLocale() }}">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <!-- CSRF Token -->
    <meta name="csrf-token" content="{{ csrf_token() }}">

    <title>{{ config('app.name', 'Wholesalepeople') }}</title>

    <link rel="stylesheet" media="screen" href="{{ asset('themes/unishop/css/vendor.min.css') }}">
    <link id="mainStyles" rel="stylesheet" media="screen" href="{{ asset('themes/unishop/css/styles.css') }}">
</head>
<!-- Body-->
<body>
<!-- Off-Canvas Wrapper-->
<div class="offcanvas-wrapper">
    <!-- Page Content-->
    <div class="container padding-top-3x padding-bottom-3x mb-1"><img class="d-block m-auto" src="{{ asset('images/404_art.png') }}" style="width: 100%; max-width: 550px;" alt="404">
        <div class="padding-top-1x mt-2 text-center">
            <h3>Page Not Found</h3>
            <p>It seems we can’t find page you are looking for. <a href="{{ route('home') }}">Go back to Homepage</a></p>
        </div>
    </div>
</div>
<!-- Back To Top Button--><a class="scroll-to-top-btn" href="#"><i class="icon-arrow-up"></i></a>
<!-- Backdrop-->
<div class="site-backdrop"></div>
<!-- JavaScript (jQuery) libraries, plugins and custom scripts-->
<script src="js/vendor.min.js"></script>
<script src="js/scripts.min.js"></script>
</body>
</html>