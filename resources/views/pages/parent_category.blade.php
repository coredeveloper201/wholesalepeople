<?php
    use App\Enumeration\Availability;
    use App\Enumeration\Role;
?>

@extends('layouts.app')

@section('additionalCSS')
    <link href="{{ cdn('plugins/toastr/toastr.min.css') }}" rel="stylesheet">
@stop

@section('content')
<div class="container-fluid">
    @if ($topBannerUrl != '')
        <div class="row">
            <div class="col-md-12">
                <a href="{{ route('vendor_or_parent_category', ['text' => changeSpecialChar($topBannerVendor)]) }}">
                    <img src="{{ $topBannerUrl }}" width="100%">
                </a>
            </div>
        </div>
    @endif

    {{ Breadcrumbs::render('parent_category_page', $category) }}
    <div class="row">
        <div class="col-xl-2 col-lg-4 order-lg-1">
            <button class="sidebar-toggle position-left" data-toggle="modal" data-target="#modalShopFilters"><i class="icon-layout"></i></button>
            <aside class="sidebar-offcanvas">
                <!-- Widget Categories-->
                <section class="widget widget-categories">
                    <h3 class="widget-title">CATEGORY</h3>
                    <ul>
                        @foreach($category->subCategories as $sub)
                            <li><a href="{{ route('second_category', ['category' => changeSpecialChar($sub->name), 'parent' => changeSpecialChar($sub->parentCategory->name)]) }}">{{ $sub->name }}</a></li>
                        @endforeach
                    </ul>
                </section>

                <!-- Widget Brand Filter-->
                <section class="widget widget-categories">
                    <h3 class="widget-title">{{ $category->name }} VENDOR</h3>
                    <ul>
                        @foreach($vendors as $vendor)
                            <li><a href="{{ route('vendor_or_parent_category', ['text' => changeSpecialChar($vendor->company_name)]) }}">{{ $vendor->company_name }}</a></li>
                        @endforeach
                    </ul>
                </section>
            </aside>
        </div>
        <div class="col-xl-10 col-lg-8 order-lg-2">
            @if (sizeof($newArrivals) == 0)
                <div class="text-center">
                    <img src="{{ cdn('images/coming-soon-png.png') }}" alt="coming soon" width="400px">
                </div>
            @endif


            @if (sizeof($topSliderVendors) > 0)
                <section class="category-top-slider d-none d-sm-block">
                    @foreach($topSliderVendors as $vendor)
                        <h4 id="cts_title_{{ $vendor->id }}" class="cts_title d-none">{{ $vendor->company_name }}</h4>
                    @endforeach

                    <div class="cts-item-container">
                        @foreach($topSliderVendors as $vendor)
                            <ul id="cts_items_ul_{{ $vendor->id }}" class="d-none cts_items_ul">
                                @foreach($vendor->items as $item)
                                    <li>
                                        <a href="{{ route('item_details_page', ['item' => $item->id]) }}">
                                            @if (sizeof($item->images) > 0)
                                                <img src="{{ cdn($item->images[0]->list_image_path) }}" alt="{{ $item->name }} - Stylepick.net">
                                            @else
                                                <img src="{{ cdn('images/no-image.png') }}" alt="No Image Found">
                                            @endif

                                            <div class="cts-style-no text-center">
                                                {{ $item->style_no }}
                                            </div>
                                        </a>
                                    </li>
                                @endforeach
                            </ul>
                        @endforeach

                        <div class="cts-navigation text-center">
                            @foreach($topSliderVendors as $vendor)
                                <span class="cts-dot cts_nav" data-id="{{ $vendor->id }}"></span>
                            @endforeach
                        </div>
                    </div>
                </section> {{--End top slider--}}
            @endif

            @if (sizeof($smallBannerVendors) > 0)
                <section class="category-page-small-banners padding-top-1x d-none d-sm-block">
                    <ul>
                        @foreach($smallBannerVendors as $vendor)
                            <li>
                                <a href="{{ route('vendor_or_parent_category', ['text' => changeSpecialChar($vendor->company_name)]) }}">
                                    <img src="{{ cdn($vendor->images[0]->image_path) }}" alt="{{ $vendor->company_name }}">
                                </a>
                            </li>
                        @endforeach
                    </ul>
                </section>
            @endif

            @if (sizeof($secondSliderVendors) > 0)
                <section class="category-slider-2 d-none d-sm-block">
                    <div class="cs2-title">
                        <div class="row">
                            <div class="col-md-6">
                                @foreach($secondSliderVendors as $vendor)
                                    <h4 class="cs2_title d-none" id="cs2_title_{{ $vendor->id }}">{{ $vendor->company_name }}</h4>
                                @endforeach
                            </div>

                            <div class="col-md-6 text-right">
                                @foreach($secondSliderVendors as $vendor)
                                    <span class="cts-dot cs2_nav" data-id="{{ $vendor->id }}"></span>
                                @endforeach
                            </div>
                        </div>
                    </div>

                    <div class="cs-2-item-container">
                        @foreach($secondSliderVendors as $vendor)
                            <ul class="cs2_items_ul d-none" id="cs2_items_ul_{{ $vendor->id }}">
                                @foreach($vendor->items as $item)
                                    <li>
                                        <a href="{{ route('item_details_page', ['item' => $item->id]) }}">
                                            @if (sizeof($item->images) > 0)
                                                <img src="{{ cdn($item->images[0]->list_image_path) }}" alt="{{ $item->name }} - Stylepick.net">
                                            @else
                                                <img src="{{ cdn('images/no-image.png') }}" alt="No Image Found">
                                            @endif
                                        </a>
                                    </li>
                                @endforeach
                            </ul>
                        @endforeach
                    </div>
                </section> {{--category slider 2--}}
            @endif

            @if (sizeof($newVendors) > 0)
                <section class="new-vendor margin-top-1x margin-bottom-1x d-none d-sm-block">
                    <div class="block-title">
                        <h3>New Vendor</h3>
                        <span><a href="#">+ More</a></span>
                    </div>

                    <div class="new-vendor-slider">
                        <ul class="nvs-tab">
                            @foreach($newVendors as $vendor)
                                <li class="nvs-tab-item" data-id="{{ $vendor->id }}"><a href="{{ route('vendor_or_parent_category', ['text' => changeSpecialChar($vendor->company_name)]) }}">{{ $vendor->company_name }}</a></li>
                            @endforeach
                        </ul>

                        @foreach($newVendors as $vendor)
                            <div class="nvs-items-container d-none" id="nvs-items-container-{{ $vendor->id }}">
                                <div class="nvs-banner">
                                    <a href="{{ route('vendor_or_parent_category', ['text' => changeSpecialChar($vendor->company_name)]) }}">
                                        <img src="{{ cdn($vendor->images[0]->image_path) }}" alt="{{ $vendor->company_name }}">
                                    </a>
                                </div>

                                <ul class="nvs-items nvs-items-4x">
                                    @foreach($vendor->items as $item)
                                        <li>
                                            <a href="{{ route('item_details_page', ['item' => $item->id]) }}">
                                                @if (sizeof($item->images) > 0)
                                                    <img src="{{ cdn($item->images[0]->list_image_path) }}" alt="{{ $item->name }} - Stylepick.net">
                                                @else
                                                    <img src="{{ cdn('images/no-image.png') }}" alt="No Image Found">
                                                @endif
                                            </a>
                                        </li>
                                    @endforeach
                                </ul>
                            </div>
                        @endforeach
                    </div>
                </section>
            @endif

            @if (sizeof($newArrivals) > 0)
                <section class="section-new-arrival">
                    <div class="block-title">
                        <h3>{{ $category->name }} New Arrivals</h3>
                        <span><a href="#">+ More</a></span>
                    </div>

                    <ul class="product-container-6x">
                        <?php $s = (sizeof($newArrivals) >= 6 ? 6 : sizeof($newArrivals)); ?>

                        @for($i=0; $i<$s; $i++)
                            <?php $item = $newArrivals[$i]; ?>
                            <li>
                                <div class="product-card">
                                    <a class="product-thumb" href="{{ route('item_details_page', ['item' => $item->id]) }}">
                                            @if (sizeof($item->images) > 0)
                                                <img src="{{ cdn($item->images[0]->list_image_path) }}" alt="{{ $item->name }} - Stylepick.net">
                                            @else
                                                <img src="{{ cdn('images/no-image.png') }}" alt="No Image Found">
                                            @endif
                                    </a>

                                    <div class="product-title">
                                        <b><a href="{{ route('vendor_or_parent_category', ['text' => changeSpecialChar($item->vendor->company_name)]) }}" class="vendor-name">{{ $item->vendor->company_name }}</a></b>
                                    </div>
                                    <h3 class="product-title">
                                        <a class="style-no" href="{{ route('item_details_page', ['item' => $item->id]) }}">{{ $item->style_no }}</a>

                                        @if (Auth::check() && Auth::user()->role == Role::$BUYER)
                                            <span class="price">
                                                @if ($item->orig_price != null)
                                                    <del>${{ number_format($item->orig_price, 2, '.', '') }}</del>
                                                @endif
                                                ${{ sprintf('%0.2f', $item->price) }}
                                            </span>
                                        @endif
                                    </h3>

                                    <div class="product-extra-info">
                                        @if (sizeof($item->colors) > 1)
                                            <img class="multi-color" src="{{ cdn('images/multi-color.png') }}" title="Multi Color Available">
                                        @endif

                                        @if ($item->availability == Availability::$ARRIVES_SOON && $item->available_on != null)
                                            <span title="Available On">
                                                <img class="calendar-icon" src="{{ cdn('images/calendar-icon.png') }}"> {{ date('m/d/Y', strtotime($item->available_on)) }}
                                            </span>
                                        @endif
                                    </div>
                                </div>
                            </li>
                        @endfor
                    </ul>

                    @if (sizeof($newArrivals) > 6)
                        <ul class="product-container-6x">
                            @for($i=6; $i<sizeof($newArrivals); $i++)
                                <?php $item = $newArrivals[$i]; ?>
                                <li>
                                    <div class="product-card">
                                        <a class="product-thumb" href="{{ route('item_details_page', ['item' => $item->id]) }}">
                                                @if (sizeof($item->images) > 0)
                                                    <img src="{{ cdn($item->images[0]->list_image_path) }}" alt="{{ $item->name }} - Stylepick.net">
                                                @else
                                                    <img src="{{ cdn('images/no-image.png') }}" alt="No Image Found">
                                                @endif
                                        </a>

                                        <div class="product-title">
                                            <b><a href="{{ route('vendor_or_parent_category', ['text' => changeSpecialChar($item->vendor->company_name)]) }}" class="vendor-name">{{ $item->vendor->company_name }}</a></b>
                                        </div>
                                        <h3 class="product-title">
                                            <a class="style-no" href="{{ route('item_details_page', ['item' => $item->id]) }}">{{ $item->style_no }}</a>

                                            @if (Auth::check() && Auth::user()->role == Role::$BUYER)
                                                <span class="price">
                                                    @if ($item->orig_price != null)
                                                        <del>${{ number_format($item->orig_price, 2, '.', '') }}</del>
                                                    @endif
                                                    ${{ sprintf('%0.2f', $item->price) }}
                                                </span>
                                            @endif
                                        </h3>

                                        <div class="product-extra-info">
                                            @if (sizeof($item->colors) > 1)
                                                <img class="multi-color" src="{{ cdn('images/multi-color.png') }}" title="Multi Color Available">
                                            @endif

                                            @if ($item->availability == Availability::$ARRIVES_SOON && $item->available_on != null)
                                                <span title="Available On">
                                                    <img class="calendar-icon" src="{{ cdn('images/calendar-icon.png') }}"> {{ date('m/d/Y', strtotime($item->available_on)) }}
                                                </span>
                                            @endif
                                        </div>
                                    </div>
                                </li>
                            @endfor
                        </ul>
                    @endif
                </section>
            @endif
        </div>
    </div>
</div>
@stop

@section('mobile_filter')
<div class="modal fade" id="modalShopFilters" tabindex="-1">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <h4 class="modal-title">Filters</h4>
                <button class="close" type="button" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
            </div>
            <div class="modal-body">
                <section class="widget widget-categories">
                    <h3 class="widget-title">CATEGORY</h3>
                    <ul>
                        @foreach($category->subCategories as $sub)
                            <li><a href="{{ route('second_category', ['category' => changeSpecialChar($sub->name), 'parent' => changeSpecialChar($sub->parentCategory->name)]) }}">{{ $sub->name }}</a></li>
                        @endforeach
                    </ul>
                </section>

                <!-- Widget Brand Filter-->
                <section class="widget widget-categories">
                    <h3 class="widget-title">{{ $category->name }} VENDOR</h3>
                    <ul>
                        @foreach($vendors as $vendor)
                            <li><a href="{{ route('vendor_or_parent_category', ['text' => changeSpecialChar(changeSpecialChar($vendor->company_name))]) }}">{{ $vendor->company_name }}</a></li>
                        @endforeach
                    </ul>
                </section>
            </div>
        </div>
    </div>
</div>
@stop

@section('additionalJS')
    <script type="text/javascript" src="{{ cdn('plugins/toastr/toastr.min.js') }}"></script>
    <script type="text/javascript" src="{{ cdn('js/slider.js') }}"></script>
    <script>
        $.ajaxSetup({
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            }
        });

        // WishList
        $(document).on('click', '.btnAddWishList', function () {
            var id = $(this).data('id');
            $this = $(this);

            $.ajax({
                method: "POST",
                url: "{{ route('add_to_wishlist') }}",
                data: { id: id }
            }).done(function( data ) {
                toastr.success('Added to Wish List.');

                $this.removeClass('btnAddWishList');
                $this.removeClass('btn-default');
                $this.addClass('btnRemoveWishList');
                $this.addClass('btn-danger');
            });
        });

        $(document).on('click', '.btnRemoveWishList', function () {
            var id = $(this).data('id');
            $this = $(this);

            $.ajax({
                method: "POST",
                url: "{{ route('remove_from_wishlist') }}",
                data: { id: id }
            }).done(function( data ) {
                toastr.success('Remove from Wish List.');

                $this.removeClass('btnRemoveWishList');
                $this.removeClass('btn-danger');
                $this.addClass('btnAddWishList');
                $this.addClass('btn-default');
            });
        });
    </script>
@stop