<?php
    use App\Enumeration\Role;
    use App\Enumeration\Availability;
?>


@extends('layouts.app')

@section('content')
<div class="divLoading vendorLoading">
    <img src="{{ asset('images/loading.gif') }}">
</div>

<div class="container-fluid">
    <div class="row">
        <div class="col-md-12">
            <div class="row">
                <div class="col-lg-8 col-md-12 vendorbanner">
                    @if ($bannerPath != '')
                        <img src="{{ $bannerPath }}">
                    @endif
                </div>
                <div class="col-lg-4 hidden-md-down">
                    <div class="vendorabout">
                        <h3>About Us</h3>{!! nl2br($vendor->company_info)  !!}
                    </div>
                </div>
            </div>
        </div>
    </div>

    <div class="row">
        <div class="col-md-12 padding-top-1x">
            <div class="row">
                <div class="col-md-8">
                    <h4>{{ $category->name or 'All Category' }}</h4>
                    <div class="title-desc">
                        <span id="totalItem">0</span>
                        <span>Items Found</span>
                    </div>
                </div>

                <div class="col-md-4 text-right">
                    <div class="column">
                        <div class="shop-sorting">
                            <select class="form-control" id="sorting">
                                <option value="1">New</option>
                                <option value="2">Price Low-High </option>
                                <option value="3">Price High-Low</option>
                                <option value="4">Style No.</option>
                            </select>
                        </div>
                    </div>

                    {{--<div class="column">
                        <div class="shop-sorting">
                            <label for="sorting">ITEM PER PAGE</label>
                            <select class="form-control" id="sorting">
                                <option selected>20</option>
                                <option>40</option>
                                <option>60</option>
                                <option>80</option>
                                <option>100</option>
                            </select>
                        </div>
                    </div>--}}
                </div>
            </div>
        </div>
    </div>
    @if (isset($category))
    {{ Breadcrumbs::render('vendor_category_page', $category) }}
    @else
    {{ Breadcrumbs::render('vendor_category_all_page', $vendor) }}
    @endif
</div>
<div class="container-fluid">
    <div class="row">
        <div class="col-xl-2 col-lg-3 order-lg-1">
            <button class="sidebar-toggle position-left" data-toggle="modal" data-target="#modalShopFilters"><i class="icon-layout"></i></button>
            <aside class="sidebar-offcanvas">
                @if ($logoPath != '')
                    <a href="{{ route('vendor_or_parent_category', ['text' => changeSpecialChar($vendor->company_name)]) }}">
                        <img src="{{ $logoPath }}">
                    </a>
                @endif

                <!-- Widget Categories-->
                <section class="widget widget-categories">
                    <h3 class="widget-title">VENDOR CATEGORY</h3>
                    <ul>
                        <li><a href="{{ route('vendor_category_all_page', ['vendor' => $vendor->id]) }}">All Category</a></li>
                        @foreach($categories as $cat)
                            @if (sizeof($cat->subCategories) > 0)
                                <li class="has-children expanded"><a href="{{ route('vendor_category_page', ['category' => $cat->id]) }}">{{ $cat->name }}</a></span>
                                    <ul>
                                        @foreach($cat->subCategories as $sub)
                                            <li>
                                                <a href="{{ route('vendor_category_page', ['category' => $sub->id]) }}">{{ $sub->name }}</a>
                                            </li>
                                        @endforeach
                                    </ul>
                                </li>
                            @else
                                <li><a href="{{ route('vendor_category_page', ['category' => $cat->id]) }}">{{ $cat->name }}</a></li>
                            @endif
                        @endforeach
                    </ul>
                </section>

                <section class="widget widget-categories">
                    <h3 class="widget-title">SEARCH</h3>

                    <div class="custom-control custom-radio custom-control-inline">
                        <input class="custom-control-input" type="radio" id="search-style-no" name="search-component" value="1" checked>
                        <label class="custom-control-label" for="search-style-no">Style No.</label>
                    </div>

                    <div class="custom-control custom-radio custom-control-inline">
                        <input class="custom-control-input" type="radio" id="search-description" name="search-component" value="2">
                        <label class="custom-control-label" for="search-description">Description</label>
                    </div>

                    <div class="form-group">
                        <input class="form-control" id="search-input" type="text" placeholder="Search">
                    </div>

                    <div class="row">
                        <div class="col-md-6">
                            <input class="form-control" id="search-price-min" type="text" placeholder="Price Min">
                        </div>

                        <div class="col-md-6">
                            <input class="form-control" id="search-price-max" type="text" placeholder="Price Max">
                        </div>
                    </div>

                    <button class="btn btn-primary" id="btn-search">SEARCH</button>
                </section>
            </aside>
        </div>
        <div class="col-xl-10 col-lg-9 order-lg-2">
            <ul class="product-container-6x" id="product-container">

            </ul>

            <div class="pagination">
                {{--{{ $items->links('others.pagination') }}--}}
            </div>
        </div>
    </div>
</div>

<template id="template-product">
    <li>
        <div class="product-card">
            <a class="product-thumb">
                <img class="product-image" src="{{ cdn('images/no-image.png') }}">
            </a>

            <h3 class="product-title">
                <a class="style-no"></a><span class="price"></span>
            </h3>

            <div class="product-extra-info">
                <img class="multi-color d-none" src="{{ cdn('images/multi-color.png') }}" title="Multi Color Available">

                <span class="available-on d-none" title="Available On">
                        <img class="calendar-icon" src="{{ cdn('images/calendar-icon.png') }}"> <span class="available-date"></span>
                    </span>
            </div>
        </div>
    </li>
</template>
@stop

@section('mobile_filter')
    <div class="modal fade" id="modalShopFilters" tabindex="-1">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <h4 class="modal-title">Filters</h4>
                    <button class="close" type="button" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                </div>
                <div class="modal-body">
                    <!-- Widget Categories-->
                    <section class="widget widget-categories">
                        <h3 class="widget-title">VENDOR CATEGORY</h3>
                        <ul>
                            <li><a href="{{ route('vendor_category_all_page', ['vendor' => $vendor->id]) }}">All Category</a></li>
                            @foreach($categories as $cat)
                                @if (sizeof($cat->subCategories) > 0 && ((isset($category) && $category->id == $cat->id) || (isset($category) && $cat->subCategories->contains('id', $category->id))))
                                    <li class="has-children {{ $category->id == $cat->id || $cat->subCategories->contains('id', $category->id) ? 'expanded' : '' }}"><a href="{{ route('vendor_category_page', ['category' => $cat->id]) }}">{{ $cat->name }}</a></span>
                                        <ul>
                                            @foreach($cat->subCategories as $sub)
                                                <li>
                                                    <a href="{{ route('vendor_category_page', ['category' => $sub->id]) }}">{{ $sub->name }}</a>
                                                </li>
                                            @endforeach
                                        </ul>
                                    </li>
                                @else
                                    <li><a href="{{ route('vendor_category_page', ['category' => $cat->id]) }}">{{ $cat->name }}</a></li>
                                @endif
                            @endforeach
                        </ul>
                    </section>

                    <section class="widget widget-categories">
                        <h3 class="widget-title">SEARCH</h3>

                        <div class="custom-control custom-radio custom-control-inline">
                            <input class="custom-control-input" type="radio" id="search-style-no" name="search-component" value="1" checked>
                            <label class="custom-control-label" for="search-style-no">Style No.</label>
                        </div>

                        <div class="custom-control custom-radio custom-control-inline">
                            <input class="custom-control-input" type="radio" id="search-description" name="search-component" value="2">
                            <label class="custom-control-label" for="search-description">Description</label>
                        </div>

                        <div class="form-group">
                            <input class="form-control" id="search-input" type="text" placeholder="Search">
                        </div>

                        <div class="row">
                            <div class="col-md-6">
                                <input class="form-control" id="search-price-min" type="text" placeholder="Price Min">
                            </div>

                            <div class="col-md-6">
                                <input class="form-control" id="search-price-max" type="text" placeholder="Price Max">
                            </div>
                        </div>

                        <button class="btn btn-primary" id="btn-search">SEARCH</button>
                    </section>
                </div>
            </div>
        </div>
    </div>
@stop

@section('additionalJS')
    <script>
        $(function () {
            $.ajaxSetup({
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                }
            });

            var page = 1;
            var search_text = '';
            var search_option = '';
            var search_price_min = '';
            var search_price_max = '';

            $('#sorting').change(function () {
                filterItem();
            });

            $('#btn-search').click(function () {
                search_text = $('#search-input').val();
                search_option = $('input[name=search-component]:checked').val();
                search_price_min = $('#search-price-min').val();
                search_price_max = $('#search-price-max').val();

                filterItem();
            });

            function filterItem(page) {
                $(".divLoading").addClass('show');
                page = typeof page !== 'undefined' ? page : 1;
                var category = '{{ $category->id or '' }}';
                var vendor = '{{ $vendor->id }}';
                var sorting = $('#sorting').val();

                var pos = 0;
                var changePos = localStorage['change_pos'];
                if (changePos) {
                    localStorage.removeItem('change_pos');

                    pos = parseInt(localStorage.getItem('previous_position'));
                }

                $("html, body").animate({ scrollTop: pos }, "fast");

                $.ajax({
                    method: "POST",
                    url: "{{ route('vendor_category_get_items') }}",
                    data: { category: category, sorting: sorting, searchText: search_text, searchOption: search_option,
                        priceMin: search_price_min, priceMax: search_price_max, page: page, vendor: vendor
                    }
                }).done(function( data ) {
                    var products = data.items;
                    $('.pagination').html(data.pagination);
                    $(".divLoading").removeClass('show');
                    $('#totalItem').html(data.total);

                    $('#product-container').html('');
                    var backOrder = '{{ Availability::$ARRIVES_SOON }}';

                    $.each(products, function (index, product) {
                        var html = $('#template-product').html();
                        var row = $(html);

                        //row.find('.product-image').attr('src', product.imagePath);
                        row.find('.product-thumb').attr('href', product.detailsUrl);
                        row.find('.vendor-name').html(product.company_name);
                        row.find('.vendor-name').attr('href', product.vendorUrl);
                        row.find('.style-no').html(product.style_no);
                        row.find('.style-no').attr('href', product.detailsUrl);
                        row.find('.price').html(product.price);

                        if (product.multiColor == true)
                            row.find('.multi-color').removeClass('d-none');

                        /*if (product.colors.length == 0)
                            row.find('.product-thumb').removeAttr("href");*/

                        if (product.availability == backOrder && product.available_on != '') {
                            row.find('.available-on').removeClass('d-none');
                            var date = new Date(product.available_on);
                            row.find('.available-date').html((date.getMonth()+1) + '/' + date.getDate() + '/' + date.getFullYear());
                        }

                        row.find('.wishlist-button-container').html(product.wishListButton);

                        $('#product-container').append(row);
                        loadImage(row.find('.product-image'), product.imagePath);
                        updatePrice(row, product.id);
                    });
                });
            }

            // Pagination
            $(document).on('click', '.page-link', function (e) {
                e.preventDefault();
                var url = $(this).attr('href');
                page = getURLParameter(url, 'page');

                filterItem(page);
            });

            function getURLParameter(url, name) {
                return (RegExp(name + '=' + '(.+?)(&|$)').exec(url)||[,null])[1];
            }

            // Hold Position
            $(window).bind('beforeunload', function(){
                localStorage['previous_page'] = page;
                localStorage['previous_position'] = $(document).scrollTop()+'';
            });

            var changePage = localStorage['change_page'];
            if (changePage) {
                localStorage.removeItem('change_page');

                page = parseInt(localStorage.getItem('previous_page'));
            }

            filterItem(page);

            function updatePrice(row, id) {
                $.ajax({
                    method: "POST",
                    url: "{{ route('get_item_price') }}",
                    data: { id: id }
                }).done(function( data ) {
                    row.find('.price').html(data);
                });
            }

            function loadImage(img, url) {
                var downloadingImage = new Image();

                downloadingImage.onload = function(){
                    img.attr('src', this.src);
                };
                downloadingImage.src = url;
            }
        });
    </script>
@stop