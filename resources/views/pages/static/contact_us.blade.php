@extends('layouts.app')

@section('additionalCSS')
    <link href="{{ asset('plugins/toastr/toastr.min.css') }}" rel="stylesheet">
@stop

@section('content')
    <div class="container padding-top-3x padding-bottom-3x">
        <div class="row">
            <div class="col-md-6">
                <div class="maps margin-bottom-1x"><iframe src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d3306.1956132552536!2d-118.25782306944103!3d34.03885297527784!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x0%3A0xd011876f89dcf3ae!2sStylepick!5e0!3m2!1sen!2sus!4v1533117682785" width="100%" height="450" frameborder="0" style="border:0" allowfullscreen></iframe></div>
                <h2>Contact Us</h2>
                <p>We are here to answer any questions you may have about our STYLEPICK.NET experiences. Reach out to us and we’ll respond as soon as we can.</p>
                <p>Even If there is something you have always wanted to experience and can’t find it on STYLEPICK.NET, let us know and we promise we’ll do our best to find it for you and send you there.</p>
                <p>Business Hour <br>Mon-fri : 9:00am to 5:30pm (PST)<br>Sat-Sun : closed (PST)</p>
                <br>
            </div>
            <div class="col-md-6">
                <div class="contact-title">
                    <h2>Have Question???</h2></div>
                <form action="{{ route('contact_us_post') }}" method="POST">
                    @csrf

                    <div class="form-group{{ $errors->has('name') ? ' has-danger' : '' }}">
                        <label class="col-form-label">Name *</label>
                        <input class="form-control" type="text" name="name">
                    </div>

                    <div class="form-group{{ $errors->has('email') ? ' has-danger' : '' }}">
                        <label class="col-form-label">Email Address *</label>
                        <input class="form-control" type="text" name="email">
                    </div>

                    <div class="form-group{{ $errors->has('phone') ? ' has-danger' : '' }}">
                        <label class="col-form-label">Phone</label>
                        <input class="form-control" type="text" name="phone">
                    </div>

                    <div class="form-group{{ $errors->has('comment') ? ' has-danger' : '' }}">
                        <label class="col-form-label">Comment *</label>
                        <textarea class="form-control" name="comment"></textarea>
                    </div>

                    <div class="form-group">
                        <div class="text-right">
                            <input type="submit" class="btn btn-primary" value="SEND">
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </div>
@stop

@section('additionalJS')
    <script type="text/javascript" src="{{ asset('plugins/toastr/toastr.min.js') }}"></script>
    <script>
        $(function () {
            var message = '{{ session('message') }}';

            if (message != '')
                toastr.success(message);
        });
    </script>
@stop