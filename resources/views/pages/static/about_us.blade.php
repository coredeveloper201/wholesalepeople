@extends('layouts.app')

@section('content')
<div class="container-fluid">
<div class="container padding-bottom-2x mb-2">
        <div class="row align-items-center padding-bottom-2x">
          <div class="col-md-5"><img class="d-block w-270 m-auto" src="images/pgimg/01.jpg" alt="Online Shopping"></div>
          <div class="col-md-7 text-md-left text-center">
            <div class="mt-30 hidden-md-up"></div>
            <h2>Who We Are</h2>
            <p>StylePick.net is a online wholesale marketplace dedicated to inspiring manufacturers and vendors through combination of trendy styles and user friendly web interface. It is a virtual fashion district for every vendors and retailers globally. Based and found in Los Angeles, StylePick.net offers experiential wholesale environments and a mix of latest and on-trend women's clothes, from boho styles, denims, and basics, graphics, accessories, swim and intimates.</p><a class="text-medium text-decoration-none" href="https://stylepick.net/contact_us">Contact Us&nbsp;<i class="icon-arrow-right"></i></a>
          </div>
        </div>
        <hr>
        <div class="row align-items-center padding-top-2x padding-bottom-2x">
          <div class="col-md-5 order-md-2"><img class="d-block w-270 m-auto" src="images/pgimg/02.jpg" alt="Delivery"></div>
          <div class="col-md-7 order-md-1 text-md-left text-center">
            <div class="mt-30 hidden-md-up"></div>
            <h2>B2B Wholesale Fashion Marketplace</h2>
            <p>In a time of advanced internet and technology, it is our mission to keep the vendors and fashion retailers to connect at the heart of commerce. StylePick.net, built with heart of Los Angeles, connects creative and trendy leading fashion manufactures with a leading boutique owners around the globe.</p><a class="text-medium text-decoration-none" href="https://stylepick.net/contact_us">Contact Us&nbsp;<i class="icon-arrow-right"></i></a>
          </div>
        </div>
</div>
@stop