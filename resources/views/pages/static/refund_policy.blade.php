@extends('layouts.app')

@section('content')
<div class="container margin-top-2x margin-bottom-2x">
    <h2>Refunds Policy</h2>
    <hr>

    <p>
    	If you are not satisfied with your purchase and would like to request a refund/return for exchange or store credit (requirements must be met), please follow these guidelines below.
    </p>
    <h3>Sales</h3>
    <p>All sales are final and only qualify for a return if your item(s) is damaged/defective from the manufacturer or you were sent the wrong item. There are no refunds. Shipping and handling fees are also non-refundable, including all refused and unaccepted packages. Items must be in their original condition (unwashed, unworn) with the original packaging to receive store credit or an exchange.</p>

    <h3>Damaged/Defective Item(s)</h3>
    <p>Damaged or defective items may be returned for store credit or an exchange may be provided.</p>
    <p>Please contact us to notify us within 5 business days from the delivered date if any items are damaged or defective.</p>
    <p>When contacting us regarding a damaged or defective item(s), please provide to us pictures of the damage (close-up and full shot), your invoice number, style number, color, size, and number of damaged or defective item(s). It can take 1-3 business days to investigate. However, it may take a bit longer as the investigation time depends on the vendor.The final RA approval will be done by the vendor.</p>

    <h3>Wrong Item(s)</h3>
    <p>Please contact vendor within 5 business days from the delivered date.</p>
    <p>Store credit: If vendor approves, vendor will send you a return label and process a store credit for the wrong item once it has been received.</p>
    <p>Correct item to be reshipped: vendor will arrange a reshipment for you. This will be based on product availability. If the product is no longer available, store credit will be applied. Items that are sent outside of its original condition/packaging will not be accepted by vendors and vendr will not be eligible for store credit.</p>

    <p>If you have any other questions about the return policy, you can contact us directly.</p>
    </div>
@stop