@extends('layouts.app')

@section('content')
    <div class="container margin-top-2x margin-bottom-2x">
        <h2>Shipping information</h2>
        <hr>

        <p class="padding-top-1x">All sales orders are fulfilled by the vendors. Please confirm with individual vendors regarding details about the shipping fee, courier, and delivery time.
        </p>
        <h3>Shipping Fee</h3>
        <p>Shipping fee is determined by courier, destination, dimension, and weight of the package. Your final invoice includes the purchase order amount and the total shipping fee. Please confirm with individual vendors.</p> 

        <h3>Customized Delivery Service</h3>
        <p>Please email: info@stylepick.net for customized special delivery request so we can notify or leave a comment to the vendor.</p>

        <h3>Payment</h3>
        <p>Final invoice amount that includes shipping fee will charge to your credit card when the vendor is ready to ship your order. Once the charge complete, your order will ship out via the shipping method you’ve selected. You may find the tracking number for your order in buyer dashboard.</p>

        <h3>Delivery</h3>
        <p>Vendors are required to ship out all orders with insurance or with a signature confirmation. Please inquire and confirm with individual vendors regarding return policies. You can also email us info@stylepick.net for assistance.</p>
    </div>
@stop