@extends('layouts.app')

@section('content')
<div class="container margin-top-2x margin-bottom-2x">
	<h2>Terms and Conditions</h2>
	<hr>

	<p class="padding-top-1x">Please read the following carefully</p>
	<h3>Customers Account information and Registration</h3>
	<p>
		Customers have to give the correct information about their business. In order to register for stylepick, the customers must be a retailer whom is in possession of a Seller's Permit. If not, stylepick has the right to prohibit the customer to visit stylepick.net. When stylepick requests proof, such as a copy of the retail license, the retailer is responsible to fax or e-mail the requested information to stylepick.
	</p>
	<h3>Offering Service</h3>
	<p>
		stylepick is simple. We make everything easy for the retailers. You log-in at stylepick.net, choose the merchandise of interest, and check out. When the order is completed, we put your merchandise in one box and ship it to you. It's easy! You only deal with us when you have damaged items, questions about specific items, or have any other favors to ask.
	</p>
	<h3>Payment Method: Credit card</h3>
	<p>We process your credit card here at stylepick.net. and we do not share any information with the third party. Your safety is really important to us so we don't keep retailer's credit card information. We ask for the credit card authorization form after completing the order so we can minimize on credit card fraud. Once the transaction is completed through authorize.net, they will keep the information regarding the transaction and stylepick uses that information when needed.</p>
	<h3>Product Descriptions</h3>
	<p>stylepick works hard to be as accurate as possible with the products and merchandise we carry. However, stylepick cannot guarantee 100% the product descriptions or other contents related to the products. It is possible that there may be some errors. If you have any specific questions or found a mistake or error on our website, please do not hesitate to call our customer service team for details.</p>
	<h3>Copy Rights</h3>
	<p>All contents included on stylepick, such as texts, graphics, logos, images, digital downloads, data compilations, and software is the property of stylepick. Also, it is protected by United States copyright laws.</p>
	<h3>Applicable Law</h3>
	<p>Online customers visiting stylepick agrees that these terms of use will be governed by the laws of the State of California if, or when, any dispute occurs between the customer and stylepick.</p>
	<div id="accordion">
		<div class="card">
			<div class="card-header" id="headingOne">
				<h5 class="mb-0">
					<button class="btn btn-link" data-toggle="collapse" data-target="#collapseOne" aria-expanded="true" aria-controls="collapseOne">
						Rule 1
					</button>
				</h5>
			</div>

			<div id="collapseOne" class="collapse show" aria-labelledby="headingOne" data-parent="#accordion">
				<div class="card-body">
					Please acknowledge that the charge of shipping will be applied to the order upon your requested shipping method. The VAT and shipping charges will be applied to overseas orders..
				</div>
			</div>
		</div>
		<div class="card">
			<div class="card-header" id="headingTwo">
				<h5 class="mb-0">
					<button class="btn btn-link collapsed" data-toggle="collapse" data-target="#collapseTwo" aria-expanded="false" aria-controls="collapseTwo">
						Rule 2
					</button>
				</h5>
			</div>
			<div id="collapseTwo" class="collapse" aria-labelledby="headingTwo" data-parent="#accordion">
				<div class="card-body">
					We ship out the merchandise in the order received. Please allow up to 3 business days for processing standard shipping, 2 business days for express shipping and 1 business day for overnight shipping. However, we may not have all the merchandise in stock; we would only ship partially upon your request.
				</div>
			</div>
		</div>
		<div class="card">
			<div class="card-header" id="headingThree">
				<h5 class="mb-0">
					<button class="btn btn-link collapsed" data-toggle="collapse" data-target="#collapseThree" aria-expanded="false" aria-controls="collapseThree">Rule 3</button>
				</h5>
			</div>
			<div id="collapseThree" class="collapse" aria-labelledby="headingThree" data-parent="#accordion">
				<div class="card-body">We do not share or utilize any personal information. The sole purpose of using the personal information is for shipping, billing and contacting.</div>
			</div>
		</div>
		<div class="card">
			<div class="card-header" id="headingFour">
				<h5 class="mb-0">
					<button class="btn btn-link collapsed" data-toggle="collapse" data-target="#collapseFour" aria-expanded="false" aria-controls="collapseFour">Rule 4</button>
				</h5>
			</div>
			<div id="collapseFour" class="collapse" aria-labelledby="headingFour" data-parent="#accordion">
				<div class="card-body">By clicking on Ship entire order together, you acknowledge that the order won't be shipped unless we have all the merchandise in our warehouse. The order may be cancelled after three days of waiting for the rest of the merchandise.</div>
			</div>
		</div>
		<div class="card">
			<div class="card-header" id="headingFive">
				<h5 class="mb-0">
					<button class="btn btn-link collapsed" data-toggle="collapse" data-target="#collapseFive" aria-expanded="false" aria-controls="collapseFive">Rule 5</button>
				</h5>
			</div>
			<div id="collapseFive" class="collapse" aria-labelledby="headingFive" data-parent="#accordion">
				<div class="card-body">All pre-ordered mechandise will be ship out to you once it is received and ready in the store, unless notified. However, we may not have all the merchandise in stock because we do first come, first serve' by orders received.</div>
			</div>
		</div>
		<div class="card">
			<div class="card-header" id="headingSix">
				<h5 class="mb-0">
					<button class="btn btn-link collapsed" data-toggle="collapse" data-target="#collapseSix" aria-expanded="false" aria-controls="collapseSix">Rule 6</button>
				</h5>
			</div>
			<div id="collapseSix" class="collapse" data-parent="#accordion">
				<div class="card-body">The order can be cancelled by the provider at any time for any reason without notice. But stylepick.net will work our best to avoid any such circumstances..</div>
			</div>
		</div>
		<div class="card">
			<div class="card-header" id="headingSeven">
				<h5 class="mb-0">
					<button class="btn btn-link collapsed" data-toggle="collapse" data-target="#collapseSeven" aria-expanded="false" aria-controls="collapseSeven">Rule 7</button>
				</h5>
			</div>
			<div id="collapseSeven" class="collapse" data-parent="#accordion">
				<div class="card-body">We have done our best to present all the colors of the products as accurately as possible on stylepick.net.</div>
			</div>
		</div>
		<div class="card">
			<div class="card-header" id="headingEight">
				<h5 class="mb-0">
					<button class="btn btn-link collapsed" data-toggle="collapse" data-target="#collapseEight" aria-expanded="false" aria-controls="collapseEight">Rule 8</button>
				</h5>
			</div>
			<div id="collapseEight" class="collapse" data-parent="#accordion">
				<div class="card-body">The information on stylepick.net may contain typographical errors and/or inaccuracies and may not be complete or current. We therefore reserve the right to correct any errors, inaccuracies or omissions and to change and update information at any time without prior notice (even after orders have been submitted). Please note that such errors, inaccuracies or omissions may relate to product descriptions, pricing, and availability. We apologize for any inconvenience this may cause you..</div>
			</div>
		</div>
		<div class="card">
			<div class="card-header" id="headingNine">
				<h5 class="mb-0">
					<button class="btn btn-link collapsed" data-toggle="collapse" data-target="#collapseNine" aria-expanded="false" aria-controls="collapseNine">Rule 9</button>
				</h5>
			</div>
			<div id="collapseNine" class="collapse" data-parent="#accordion">
				<div class="card-body">Termination terms and conditions are applicable to you upon accessing the site, completion of registration, or process of shopping. These terms and conditions, or any of them, may be terminated by stylepick.net without notice at any time for any reason..</div>
			</div>
		</div>
		<div class="card">
			<div class="card-header" id="headingTen">
				<h5 class="mb-0">
					<button class="btn btn-link collapsed" data-toggle="collapse" data-target="#collapseTen" aria-expanded="false" aria-controls="collapseTen">Rule 10</button>
				</h5>
			</div>
			<div id="collapseTen" class="collapse" data-parent="#accordion">
				<div class="card-body">At stylepick.net, our commitment is to offer convenience, service, and product availability on-line at compelling prices every day, with certain limited time offerings of merchandise at promotional prices. Merchandise offered on-line at stylepick.net will be priced the same as merchandise offered at our Showroom. However, the Showroom may have different prices or promotional events at different.</div>
			</div>
		</div>
	</div>	
</div>
@stop