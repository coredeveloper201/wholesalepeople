<!DOCTYPE html>
<html lang="{{ app()->getLocale() }}">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <!-- CSRF Token -->
    <meta name="csrf-token" content="{{ csrf_token() }}">

    <title>{{ config('app.name', 'Wholesalepeople') }}</title>


    <link rel="stylesheet" media="screen" href="{{ asset('themes/unishop/css/vendor.min.css') }}">
    <link id="mainStyles" rel="stylesheet" media="screen" href="{{ asset('themes/unishop/css/styles.css') }}">
    <script src="{{ asset('themes/unishop/js/modernizr.min.js') }}"></script>
    @yield('additionalCSS')
</head>

<!-- Body-->
<body>
<!-- Off-Canvas Wrapper-->
<div class="">
    <!-- Page Content-->
    <div class="row no-gutters">
        <div class="col-md-12 fh-section"><span class="overlay"></span>
            <div class="d-flex flex-column fh-section py-5 px-3 justify-content-between">
                <div class="w-100 text-center">
                    <div class="d-inline-block mb-5"><img class="d-block" src="{{ asset('/images/logo.png') }}" alt="Unishop"></div>
                    <h1 class="text-white text-normal mb-2">Coming Soon...</h1>
                    <h5 class="text-white text-normal opacity-80 mb-4">Our website is currently under construction. It goes live in:</h5>
                    <div class="countdown countdown-inverse" data-date-time="{{ $openingDate->value }} 00:00:00">
                        <div class="item">
                            <div class="days">0</div><span class="days_ref">Days</span>
                        </div>
                        <div class="item">
                            <div class="hours">00</div><span class="hours_ref">Hours</span>
                        </div>
                        <div class="item">
                            <div class="minutes">00</div><span class="minutes_ref">Mins</span>
                        </div>
                        <div class="item">
                            <div class="seconds">00</div><span class="seconds_ref">Secs</span>
                        </div>
                    </div>
                </div>
                <div class="w-100 text-center">
                    <p class="text-white mb-2">Phone: 714.512.0305</p><a class="navi-link-light" href="mailto:support@unishop.com">info@stylepick.net</a>
                </div>
            </div>
        </div>
    </div>
</div>
<!-- Back To Top Button--><a class="scroll-to-top-btn" href="#"><i class="icon-arrow-up"></i></a>
<!-- Backdrop-->
<div class="site-backdrop"></div>

<script src="{{ asset('themes/unishop/js/vendor.min.js') }}"></script>
<script src="{{ asset('themes/unishop/js/scripts.min.js') }}"></script>
</body>
</html>