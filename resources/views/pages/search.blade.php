@extends('layouts.app')

@section('content')
    <div class="container-fluid">
        <h2>Search result for '{{ request()->get('s') }}'</h2>

        <div class="row">
            <div class="col-md-12">
                @if (sizeof($items) > 0)
                <div class="isotope-grid cols-6 mb-1">
                    <div class="gutter-sizer"></div>
                    <div class="grid-sizer"></div>

                    @foreach($items as $item)
                        <div class="grid-item">
                            <div class="product-card">
                                <a class="product-thumb" href="{{ route('item_details_page', ['item' => $item->id]) }}">
                                    @if (sizeof($item->images) > 0)
                                        <img src="{{ cdn($item->images[0]->list_image_path) }}" alt="{{ $item->name }}">
                                    @else
                                        <img src="{{ cdn('images/no-image.png') }}" alt="{{ $item->name }}">
                                    @endif
                                </a>
                                <div class="product-title"><b><a href="{{ route('vendor_or_parent_category', ['text' => changeSpecialChar($item->vendor->company_name)]) }}">{{ $item->vendor->company_name }}</a></b></div>
                                <h3 class="product-title">
                                    <a href="{{ route('item_details_page', ['item' => $item->id]) }}">{{ $item->style_no }}</a>
                                </h3>
                            </div>
                        </div>
                    @endforeach
                </div>

                <div class="pagination">
                    {{ $items->appends(['s' => request()->get('s')])->links('others.pagination') }}
                </div>
                @else
                    <b>No Item(s) Found.</b>
                @endif
            </div>
        </div>
    </div>
@stop