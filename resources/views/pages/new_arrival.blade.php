@extends('layouts.app')

<?php
use App\Enumeration\Role;
use App\Enumeration\Availability;
?>

@section('additionalCSS')
    <style>
        .color-selected {
            border: 1px solid black !important;
        }
    </style>
@stop

@section('content')
    <div class="divLoading">
        <img src="{{ asset('images/loading.gif') }}">
    </div>

    <div class="container-fluid">
        <div class="content col-md-12">
            <div class="row">
                <div class="col-md-8">
                    <h4>New Arrivals</h4>
                    <div class="title-desc">
                        <span id="totalItem"></span>
                        <span>Items Found</span>
                    </div>
                </div>
                <div class="col-md-4 text-right">
                    <div class="column">
                        <div class="shop-sorting">
                            <select class="form-control" id="sorting">
                                <option value="1">New</option>
                                <option value="2">Price Low-High </option>
                                <option value="3">Price High-Low</option>
                                <option value="4">Style No.</option>
                            </select>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        {{ Breadcrumbs::render('new_arrival_home') }}
    </div>
    <div class="container-fluid">
        <div class="row">
            <div class="col-xl-2 col-lg-2 order-lg-1">
                <button class="sidebar-toggle position-left" data-toggle="modal" data-target="#modalShopFilters"><i class="icon-layout"></i></button>
                <aside class="sidebar-offcanvas">
                    <section class="widget widget-categories">
                        <h3 class="widget-title">SEARCH BY DATE</h3>
                        <ul>
                            @foreach($byArrivalDate as $item)
                                <li><a href="{{ route('new_arrival_page', ['D' => $item['day']]) }}">{{ $item['name'] }}</a><span>({{ $item['count'] }})</span></li>
                            @endforeach
                        </ul>
                    </section>

                    <section class="widget">
                        <h3 class="widget-title">CATEGORY</h3>

                        @foreach($categories as $category)
                            <div class="custom-control custom-checkbox">
                                <input class="custom-control-input checkbox-category"
                                       type="checkbox" id="category-checkbox-{{$category->id}}"
                                       data-id="{{ $category->id }}" {{ request()->get('C') == $category->id ? 'checked' : '' }}>
                                <label class="custom-control-label" for="category-checkbox-{{$category->id}}">{{ $category->name }}&nbsp;<span class="text-muted">({{ $category->count }})</span></label>
                            </div>
                        @endforeach
                    </section>

                    <section class="widget widget-categories">
                        <h3 class="widget-title">SEARCH</h3>

                        <div class="custom-control custom-radio custom-control-inline">
                            <input class="custom-control-input" type="radio" id="search-style-no" name="search-component" value="1"
                                    {{ (request()->get('search-by') == '') ? 'checked' : (request()->get('search-by') == 1 ? 'checked' : '') }}>
                            <label class="custom-control-label" for="search-style-no">Style No.</label>
                        </div>

                        <div class="custom-control custom-radio custom-control-inline">
                            <input class="custom-control-input" type="radio" id="search-description" name="search-component" value="2"
                                    {{ request()->get('search-by') == 2 ? 'checked' : '' }}>
                            <label class="custom-control-label" for="search-description">Description</label>
                        </div>

                        <div class="form-group">
                            <input class="form-control" id="search-input" type="text" placeholder="Search"
                                   value="{{ request()->get('search') }}">
                        </div>

                        <div class="row">
                            <div class="col-md-6">
                                <input class="form-control" id="search-price-min" type="text" placeholder="Price Min"
                                       value="{{ request()->get('price_min') }}">
                            </div>

                            <div class="col-md-6">
                                <input class="form-control" id="search-price-max" type="text" placeholder="Price Max"
                                       value="{{ request()->get('price_max') }}">
                            </div>
                        </div>

                        <button class="btn btn-primary" id="btn-search">SEARCH</button>
                    </section>

                    <section class="widget">
                        <h3 class="widget-title">VENDORS</h3>

                        @foreach($vendors as $vendor)
                            <div class="custom-control custom-checkbox">
                                <input class="custom-control-input vendor-checkbox" type="checkbox" id="vendor_{{ $vendor->id }}" data-id="{{ $vendor->id }}">
                                <label class="custom-control-label" for="vendor_{{ $vendor->id }}">{{ $vendor->company_name }}</label>
                            </div>
                        @endforeach
                    </section>

                    <section class="widget">
                        <h3 class="widget-title">COLORS</h3>

                        <ul class="sidecolor">
                            @foreach($masterColors as $mc)
                                <li class="item-color {{ request()->get('color') == $mc->id ? 'color-selected' : '' }}"
                                    style="position: relative; float: left; display: list-item; padding: 3px; border: 1px solid white"
                                    data-id="{{ $mc->id }}" title="{{ $mc->name }}">
                                    <img src="{{ cdn($mc->image_path) }}" width="30px" height="20px">
                                </li>
                            @endforeach
                        </ul>
                    </section>
                </aside>
            </div>
            <div class="col-xl-10 col-lg-10 order-lg-2">
                <div class="wrap">
                    {{--<div class="shop-toolbar padding-bottom-1x mb-2">


                        <div class="column">
                            <div class="shop-sorting">
                                <label for="sorting">ITEM PER PAGE</label>
                                <select class="form-control" id="sorting">
                                    <option selected>20</option>
                                    <option>40</option>
                                    <option>60</option>
                                    <option>80</option>
                                    <option>100</option>
                                </select>
                            </div>
                        </div>
                    </div>--}}


                    <ul class="plist product-container-6x" id="product-container">

                    </ul>

                    <div class="pagination">
                        {{--{{ $items->links('others.pagination') }}--}}
                    </div>

                    {{--<nav class="pagination">
                        <div class="column hidden-xs-down"><a class="btn btn-outline-secondary btn-sm" href="#"><i class="icon-arrow-left"></i>Previous&nbsp;</a></div>
                        <div class="column">
                            <ul class="pages">
                                <li class="active"><a href="#">1</a></li>
                                <li><a href="#">2</a></li>
                                <li><a href="#">3</a></li>
                                <li><a href="#">4</a></li>
                                <li>...</li>
                                <li><a href="#">12</a></li>
                            </ul>
                        </div>
                        <div class="column text-right hidden-xs-down"><a class="btn btn-outline-secondary btn-sm" href="#">Next&nbsp;<i class="icon-arrow-right"></i></a></div>
                    </nav>--}}
                </div>
            </div>
        </div>
    </div>


    {{--<template id="template-product">
        <li>
            <div class="product-card">
                <a class="product-thumb">
                    <img src="{{ cdn('images/no-image.png') }}">
                </a>
                <div class="item-detail">
                    <div class="product-title"><b><a href="#" class="vendor-name"></a></b></div>
                    <h3 class="product-sku"><a class="style-no"></a></h3>

                    <span class="price"></span>
                </div>
            </div>
        </li>
    </template>--}}

    <template id="template-product">
        <li>
            <div class="product-card">
                <a class="product-thumb">
                    <img class="product-image" src="{{ cdn('images/no-image.png') }}">
                </a>
                <div class="product-title"><b><a href="#" class="vendor-name"></a></b></div>
                <h3 class="product-title"><a class="style-no"></a><span class="price"></span></h3>
                <div class="availability">
                    <span class="available-on d-none" title="Available On">
                        PRE ORDER : <span class="available-date"></span>
                    </span>
                </div>

                <div class="product-extra-info">
                    <img class="multi-color d-none" src="{{ cdn('images/multi-color.png') }}" title="Multi Color Available">
                </div>
            </div>
        </li>
    </template>
@stop

@section('mobile_filter')
    <div class="modal fade" id="modalShopFilters" tabindex="-1">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <h4 class="modal-title">Filters</h4>
                    <button class="close" type="button" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                </div>
                <div class="modal-body">
                    <section class="widget widget-categories">
                        <h3 class="widget-title">SEARCH BY DATE</h3>
                        <ul>
                            @foreach($byArrivalDate as $item)
                                <li><a href="{{ route('new_arrival_page', ['D' => $item['day']]) }}">{{ $item['name'] }}</a><span>({{ $item['count'] }})</span></li>
                            @endforeach
                        </ul>
                    </section>

                    <section class="widget">
                        <h3 class="widget-title">CATEGORY</h3>

                        @foreach($categories as $category)
                            <div class="custom-control custom-checkbox">
                                <input class="custom-control-input checkbox-category"
                                       type="checkbox" id="mobile-category-checkbox-{{$category->id}}"
                                       data-id="{{ $category->id }}" {{ request()->get('C') == $category->id ? 'checked' : '' }}>
                                <label class="custom-control-label" for="mobile-category-checkbox-{{$category->id}}">{{ $category->name }}&nbsp;<span class="text-muted">({{ $category->count }})</span></label>
                            </div>
                        @endforeach
                    </section>

                    <section class="widget widget-categories">
                        <h3 class="widget-title">SEARCH</h3>

                        <div class="custom-control custom-radio custom-control-inline">
                            <input class="custom-control-input" type="radio" id="mobile-search-style-no" name="search-component" value="1"
                                    {{ (request()->get('search-by') == '') ? 'checked' : (request()->get('search-by') == 1 ? 'checked' : '') }}>
                            <label class="custom-control-label" for="mobile-search-style-no">Style No.</label>
                        </div>

                        <div class="custom-control custom-radio custom-control-inline">
                            <input class="custom-control-input" type="radio" id="mobile-search-description" name="search-component" value="2"
                                    {{ request()->get('search-by') == 2 ? 'checked' : '' }}>
                            <label class="custom-control-label" for="mobile-search-description">Description</label>
                        </div>

                        <div class="form-group">
                            <input class="form-control" id="search-input" type="text" placeholder="Search"
                                   value="{{ request()->get('search') }}">
                        </div>

                        <div class="row">
                            <div class="col-md-6">
                                <input class="form-control" id="search-price-min" type="text" placeholder="Price Min"
                                       value="{{ request()->get('price_min') }}">
                            </div>

                            <div class="col-md-6">
                                <input class="form-control" id="search-price-max" type="text" placeholder="Price Max"
                                       value="{{ request()->get('price_max') }}">
                            </div>
                        </div>

                        <button class="btn btn-primary" id="btn-search">SEARCH</button>
                    </section>

                    <section class="widget">
                        <h3 class="widget-title">VENDORS</h3>

                        @foreach($vendors as $vendor)
                            <div class="custom-control custom-checkbox">
                                <input class="custom-control-input vendor-checkbox" type="checkbox" id="vendor_mobile_{{ $vendor->id }}" data-id="{{ $vendor->id }}">
                                <label class="custom-control-label" for="vendor_mobile_{{ $vendor->id }}">{{ $vendor->company_name }}</label>
                            </div>
                        @endforeach
                    </section>

                    <section class="widget">
                        <h3 class="widget-title">COLORS</h3>

                        <ul class="sidecolor">
                            @foreach($masterColors as $mc)
                                <li class="item-color {{ request()->get('color') == $mc->id ? 'color-selected' : '' }}" data-id="{{ $mc->id }}" title="{{ $mc->name }}">
                                    <img src="{{ cdn($mc->image_path) }}" width="30px" height="20px">
                                </li>
                            @endforeach
                        </ul>
                    </section>
                </div>
            </div>
        </div>
    </div>
@stop

@section('additionalJS')
    <script>
        $(function () {
            $.ajaxSetup({
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                }
            });

            var page = 1;
            var search_text = '';
            var search_option = '';
            var search_price_min = '';
            var search_price_max = '';


            $('.checkbox-category, .vendor-checkbox').change(function () {
                filterItem();
            });

            $('#sorting').change(function () {
                filterItem();
            });

            $('.item-color').click(function () {
                if ($(this).hasClass('color-selected'))
                    $(this).removeClass('color-selected');
                else
                    $(this).addClass('color-selected');

                filterItem();
            });

            $('#btn-search').click(function () {
                search_text = $('#search-input').val();
                search_option = $('input[name=search-component]:checked').val();
                search_price_min = $('#search-price-min').val();
                search_price_max = $('#search-price-max').val();

                filterItem(page);
            });

            function filterItem(page) {
                $(".divLoading").addClass('show');

                page = typeof page !== 'undefined' ? page : 1;
                var categories = [];
                var vendors = [];
                var masterColors = [];
                var sorting = $('#sorting').val();
                var D = '{{ request()->get('D') }}';
                var C = '{{ request()->get('C') }}';

                var pos = 0;
                var changePos = localStorage['change_pos'];
                if (changePos) {
                    localStorage.removeItem('change_pos');

                    pos = parseInt(localStorage.getItem('previous_position'));
                }

                $("html, body").animate({ scrollTop: pos }, "fast");

                // Category
                $('.checkbox-category').each(function () {
                    if ($(this).is(':checked'))
                        categories.push($(this).data('id'));
                });

                // Vendor
                $('.vendor-checkbox').each(function () {
                    if ($(this).is(':checked'))
                        vendors.push($(this).data('id'));
                });

                // Master Color
                $('.item-color').each(function () {
                    if ($(this).hasClass('color-selected'))
                        masterColors.push($(this).data('id'));
                });


                $.ajax({
                    method: "POST",
                    url: "{{ route('get_new_arrival_items') }}",
                    data: { categories: categories, vendors: vendors, masterColors: masterColors, sorting: sorting, searchText: search_text, searchOption: search_option,
                        priceMin: search_price_min, priceMax: search_price_max, D: D, page: page
                    }
                }).done(function( data ) {
                    $('#product-container').html('');
                    $(".divLoading").removeClass('show');
                    var products = data.items;
                    $('.pagination').html(data.pagination);
                    $('#totalItem').html(data.total);
                    var backOrder = '{{ Availability::$ARRIVES_SOON }}';

                    $.each(products, function (index, product) {
                        var html = $('#template-product').html();
                        var row = $(html);

                        //row.find('.product-image').attr('src', product.imagePath);
                        row.find('.product-thumb').attr('href', product.detailsUrl);
                        row.find('.vendor-name').html(product.company_name);
                        row.find('.vendor-name').attr('href', product.vendorUrl);
                        row.find('.style-no').html(product.style_no);
                        row.find('.style-no').attr('href', product.detailsUrl);
                        row.find('.price').html(product.price);

                        if (product.multiColor == true)
                            row.find('.multi-color').removeClass('d-none');

                        /*f (product.colors.length == 0)
                            row.find('.product-thumb').removeAttr("href");*/

                        if (product.availability == backOrder && product.available_on != '') {
                            row.find('.available-on').removeClass('d-none');
                            var date = new Date(product.available_on);
                            row.find('.available-date').html((date.getMonth()+1) + '/' + date.getDate() + '/' + date.getFullYear());
                        }

                        $('#product-container').append(row);
                        loadImage(row.find('.product-image'), product.imagePath);
                        updatePrice(row, product.id);
                    });
                });
            }

            //filterItem(1);

            // Pagination
            $(document).on('click', '.page-link', function (e) {
                e.preventDefault();
                var url = $(this).attr('href');
                page = getURLParameter(url, 'page');

                filterItem(page);
            });

            function getURLParameter(url, name) {
                return (RegExp(name + '=' + '(.+?)(&|$)').exec(url)||[,null])[1];
            }

            // Hold Position
            $(window).bind('beforeunload', function(){
                localStorage['previous_page'] = page;
                localStorage['previous_position'] = $(document).scrollTop()+'';
            });

            var changePage = localStorage['change_page'];
            if (changePage) {
                localStorage.removeItem('change_page');

                page = parseInt(localStorage.getItem('previous_page'));
            }

            $('#btn-search').trigger('click');
            
            function updatePrice(row, id) {
                $.ajax({
                    method: "POST",
                    url: "{{ route('get_item_price') }}",
                    data: { id: id }
                }).done(function( data ) {
                    row.find('.price').html(data);
                });
            }

            function loadImage(img, url) {
                var downloadingImage = new Image();

                downloadingImage.onload = function(){
                    img.attr('src', this.src);
                };
                downloadingImage.src = url;
            }
        });
    </script>
@stop