@extends('layouts.app')

@section('content')

<div class="container-fluid">
    @if ($topBannerUrl != '')
        <div class="row">
            <div class="col-md-12">
                <a href="{{ route('vendor_or_parent_category', ['text' => changeSpecialChar($topBannerVendor)]) }}">
                    <img src="{{ $topBannerUrl }}" width="100%">
                </a>
            </div>
        </div>
    @endif
    {{ Breadcrumbs::render('all_vendor_page') }}

    <div class="row">
        <div class="col-md-3">
            <form class="input-group form-group" method="get" action="{{ route('all_vendor_page') }}">
                <span class="input-group-btn">
                <button type="submit"><i class="icon-search"></i></button></span>
                <input class="form-control" type="search" placeholder="Search vendor" name="search" value="{{ request()->get('search') }}">
            </form>
        </div>

        <div class="col-md-6"></div>

        <div class="col-md-3">
            <select class="form-control" id="select_vendor_start">
                <option value="">All</option>
                <option value="1" {{ request()->get('s') == '1' ? 'selected' : '' }}>#</option>
                <option value="A" {{ request()->get('s') == 'A' ? 'selected' : '' }}>A</option>
                <option value="B" {{ request()->get('s') == 'B' ? 'selected' : '' }}>B</option>
                <option value="C" {{ request()->get('s') == 'C' ? 'selected' : '' }}>C</option>
                <option value="D" {{ request()->get('s') == 'D' ? 'selected' : '' }}>D</option>
                <option value="E" {{ request()->get('s') == 'E' ? 'selected' : '' }}>E</option>
                <option value="F" {{ request()->get('s') == 'F' ? 'selected' : '' }}>F</option>
                <option value="G" {{ request()->get('s') == 'G' ? 'selected' : '' }}>G</option>
                <option value="H" {{ request()->get('s') == 'H' ? 'selected' : '' }}>H</option>
                <option value="I" {{ request()->get('s') == 'I' ? 'selected' : '' }}>I</option>
                <option value="J" {{ request()->get('s') == 'J' ? 'selected' : '' }}>J</option>
                <option value="K" {{ request()->get('s') == 'K' ? 'selected' : '' }}>K</option>
                <option value="L" {{ request()->get('s') == 'L' ? 'selected' : '' }}>L</option>
                <option value="M" {{ request()->get('s') == 'M' ? 'selected' : '' }}>M</option>
                <option value="N" {{ request()->get('s') == 'N' ? 'selected' : '' }}>N</option>
                <option value="O" {{ request()->get('s') == 'O' ? 'selected' : '' }}>O</option>
                <option value="P" {{ request()->get('s') == 'P' ? 'selected' : '' }}>P</option>
                <option value="Q" {{ request()->get('s') == 'Q' ? 'selected' : '' }}>Q</option>
                <option value="R" {{ request()->get('s') == 'R' ? 'selected' : '' }}>R</option>
                <option value="S" {{ request()->get('s') == 'S' ? 'selected' : '' }}>S</option>
                <option value="T" {{ request()->get('s') == 'T' ? 'selected' : '' }}>T</option>
                <option value="U" {{ request()->get('s') == 'U' ? 'selected' : '' }}>U</option>
                <option value="V" {{ request()->get('s') == 'V' ? 'selected' : '' }}>V</option>
                <option value="W" {{ request()->get('s') == 'W' ? 'selected' : '' }}>W</option>
                <option value="X" {{ request()->get('s') == 'X' ? 'selected' : '' }}>X</option>
                <option value="Y" {{ request()->get('s') == 'Y' ? 'selected' : '' }}>Y</option>
                <option value="Z" {{ request()->get('s') == 'Z' ? 'selected' : '' }}>Z</option>
            </select>
        </div>
    </div>
    <div class="row">
        <div class="col-md-12">
            <table class="table">
                <thead>
                <tr>
                    <th class="vcimage">Vendor</th>
                    <th class="hidden-md-down">Company</th>
                    <th class="hidden-md-down">Coupon</th>
                    <th class="hidden-md-down">Link</th>
                </tr>
                </thead>

                <tbody class="vtbody">
                @foreach($vendors as $vendor)
                    <tr>
                        <td>
                            @if (sizeof($vendor->images) > 0)
                                <img src="{{ cdn($vendor->images[0]->image_path) }}" alt="{{ $vendor->company_name }}">
                            @endif
                        </td>

                        <td>
                            <a href="{{ route('vendor_or_parent_category', ['text' => changeSpecialChar($vendor->company_name)]) }}">{{ $vendor->company_name }}</a><br>
                            <b>Address: </b>{{ $vendor->billing_address. ', ' }}
                            {{ ($vendor->billingState == null) ? $vendor->billing_state : $vendor->billingState->name }}
                            {{ ', '.$vendor->billing_city.', '.$vendor->billingCountry->name.' - '.$vendor->billing_zip }}<br>
                            <b>Tel: </b>{{ $vendor->billing_phone }}<br>
                            <b>Contact: </b> {{ $vendor->user->first_name.' '.$vendor->user->last_name }} <br>
                            <b>Created Date: </b> {{ date('F d, Y', strtotime($vendor->activated_at)) }}
                        </td>
                        <td width="20%">
                            @foreach($vendor->coupons as $coupon)
                                <span class="tag">Code : {{ $coupon->name }}</span>
                                <span>{{ $coupon->description }}<span>
                            @endforeach
                        </td>
                        <td class="hidden-md-down">
                            <a class="goto" href="{{ route('vendor_or_parent_category', ['text' => changeSpecialChar($vendor->company_name)]) }}">Goto Store</a>
                        </td>
                    </tr>
                @endforeach
                </tbody>
            </table>

            <div class="pagination">
                {{ $vendors->appends($appends)->links('others.pagination') }}
            </div>
        </div>
    </div>
</div>
@stop

@section('additionalJS')
    <script>
        $('#select_vendor_start').change(function () {
            var val = $(this).val();

            window.location.replace('{{ route('all_vendor_page') }}?s=' + val);
        });
    </script>
@stop