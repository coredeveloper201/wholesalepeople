<?php
    use App\Enumeration\Availability;
    use App\Enumeration\Role;
?>

@extends('layouts.app')

@section('additionalCSS')
    <link href="{{ asset('plugins/toastr/toastr.min.css') }}" rel="stylesheet">
@stop

@section('slider')
    @if (sizeof($mainSliderVendors) > 0)
        <section class="padding-top-1x margin-bottom-1x hidden-xs-down">
            <div class="main-slider">
                <div class="slider container-fluid">
                    <ul>
                        @foreach($mainSliderVendors as $vendor)
                            <li class="ms-banner" id="ms_banner_{{ $vendor->id }}">
                                <a href="{{ route('vendor_or_parent_category', ['text' => changeSpecialChar($vendor->company_name)]) }}">
                                    <img src="{{ cdn($vendor->images[0]->image_path) }}" alt="{{ $vendor->company_name }}">
                                </a>
                            </li>
                        @endforeach
                    </ul>
                    <a class="ms-previous"></a>
                    <a class="ms-next"></a>
                </div>

                <ul class="nav nav-pills justify-content-center">
                    @foreach($mainSliderVendors as $vendor)
                        <li class="nav-item">
                            <a class="nav-link ms-vendor" data-id="{{ $vendor->id }}" href="{{ route('vendor_or_parent_category', ['text' => changeSpecialChar($vendor->company_name)]) }}">{{ $vendor->company_name }}</a>
                        </li>
                    @endforeach
                </ul>

                <div class="container-fluid products">
                    @foreach($mainSliderVendors as $vendor)
                        <ul class="ms-products" id="ms_products_{{ $vendor->id }}">
                            @foreach($vendor->items as $item)
                                <li class="ms-product">
                                    <div class="product-card">
                                        <a class="product-thumb" href="{{ route('item_details_page', ['item' => $item->id]) }}">
                                            @if (sizeof($item->images) > 0)
                                                <img src="{{ cdn($item->images[0]->list_image_path) }}" alt="{{ $item->name }} - Stylepick.net">
                                            @else
                                                <img src="{{ cdn('images/no-image.png') }}" alt="No Image Found">
                                            @endif
                                        </a>
                                    </div>
                                </li>
                            @endforeach
                        </ul>
                    @endforeach
                </div>
            </div>
        </section>
    @endif

    @if (sizeof($mobileMainSliderVendors) > 0)
        <div class="mobile-slider content hidden-sm-up">
            @foreach($mobileMainSliderVendors as $vendor)
                <a href="{{ route('vendor_or_parent_category', ['text' => changeSpecialChar($vendor->company_name)]) }}">
                    <img class="d-none mobile-main-slider-item" src="{{ cdn($vendor->images[0]->image_path) }}" alt="{{ $vendor->company_name }}">
                </a>
            @endforeach
        </div>
    @endif
@stop

@section('content')
    <section class="container-fluid margin-bottom-2x hidden-md-down">
        <div class="block-title">
            <h3>New Vendors</h3>
        </div>

        <div class="new-vendor-slider">
            <ul class="nvs-tab">
                @foreach($newVendors as $vendor)
                    <li class="nvs-tab-item" data-id="{{ $vendor->id }}"><a href="{{ route('vendor_or_parent_category', ['text' => changeSpecialChar($vendor->company_name)]) }}">{{ $vendor->company_name }}</a></li>
                @endforeach
            </ul>

            @foreach($newVendors as $vendor)
                <div class="nvs-items-container d-none" id="nvs-items-container-{{ $vendor->id }}">
                    <div class="nvs-banner">
                        <a href="{{ route('vendor_or_parent_category', ['text' => changeSpecialChar($vendor->company_name)]) }}">
                            <img src="{{ cdn($vendor->images[0]->image_path) }}" alt="{{ $vendor->company_name }}">
                        </a>
                    </div>

                    <ul class="nvs-items">
                        @foreach($vendor->items as $item)
                            <li>
                                <a href="{{ route('item_details_page', ['item' => $item->id]) }}">
                                    @if (sizeof($item->images) > 0)
                                        <img src="{{ cdn($item->images[0]->list_image_path) }}" alt="{{ $item->name }} - Stylepick.net">
                                    @else
                                        <img src="{{ cdn('images/no-image.png') }}" alt="No Image Found">
                                    @endif
                                </a>
                            </li>
                        @endforeach
                    </ul>
                </div>
            @endforeach
        </div>
    </section>

    <section class="container-fluid margin-bottom-2x">
        <div class="block-title">
            <h3>New Items</h3>
            <span><a href="{{ route('new_arrival_home') }}">+ More</a></span>
        </div>

        <ul class="product-container-8x newfrom">
            @foreach($newArrivals as $item)
                <li>
                    <div class="product-card">
                        <a class="product-thumb" href="{{ route('item_details_page', ['item' => $item->id]) }}">
                                @if (sizeof($item->images) > 0)
                                    <img src="{{ cdn($item->images[0]->list_image_path) }}" alt="{{ $item->name }} - Stylepick.net">
                            @else
                                <img src="{{ cdn('images/no-image.gif') }}" alt="No Image Found">
                            @endif
                        </a>
                            <div class="product-title">
                                <b><a href="{{ route('vendor_or_parent_category', ['text' => changeSpecialChar($item->vendor->company_name)]) }}" class="vendor-name">{{ $item->vendor->company_name }}</a></b>
                            </div>
                            <h3 class="product-title">
                                <a class="style-no" href="{{ route('item_details_page', ['item' => $item->id]) }}">{{ $item->style_no }}</a>
                            </h3>
                        <div class="product-extra-info">
                            @if (sizeof($item->colors) > 1)
                                <img class="multi-color" src="{{ cdn('images/multi-color.png') }}" title="Multi Color Available" alt="multi-color">
                            @endif
                                @if ($item->availability == Availability::$ARRIVES_SOON && $item->available_on != null)
                                        <img class="calendar-icon" src="{{ cdn('images/calendar-icon.png') }}" alt="calender-icon"> {{ date('m/d/Y', strtotime($item->available_on)) }}
                                @endif
                        </div>
                    </div>
                </li>
            @endforeach
        </ul>
    </section>

    <section class="container-fluid margin-bottom-2x">
        <div class="block-title">
            <h3>New Accessories</h3>
            <span><a href="/accessories">+ More</a></span>
        </div>

        <ul class="product-container-8x">
            @foreach($newArrivalsAccessories as $item)
                <li>
                    <div class="product-card">
                        <a class="product-thumb" href="{{ route('item_details_page', ['item' => $item->id]) }}">
                            @if (sizeof($item->images) > 0)
                                <img src="{{ cdn($item->images[0]->list_image_path) }}" alt="{{ $item->name }} - Stylepick.net">
                            @else
                                <img src="{{ cdn('images/no-image.gif') }}" alt="No Image Found">
                            @endif
                        </a>
                        <div class="product-title">
                            <b><a href="{{ route('vendor_or_parent_category', ['text' => changeSpecialChar($item->vendor->company_name)]) }}" class="vendor-name">{{ $item->vendor->company_name }}</a></b>
                        </div>
                        <h3 class="product-title">
                            <a class="style-no" href="{{ route('item_details_page', ['item' => $item->id]) }}">{{ $item->style_no }}</a>
                        </h3>
                        <div class="product-extra-info">
                            @if (sizeof($item->colors) > 1)
                                <img class="multi-color" src="{{ cdn('images/multi-color.png') }}" title="Multi Color Available" alt="multi-color">
                            @endif
                            @if ($item->availability == Availability::$ARRIVES_SOON && $item->available_on != null)
                                <img class="calendar-icon" src="{{ cdn('images/calendar-icon.png') }}" alt="calender-icon"> {{ date('m/d/Y', strtotime($item->available_on)) }}
                            @endif
                        </div>
                    </div>
                </li>
            @endforeach
        </ul>
    </section>

    @if (sizeof($osItems) > 0)
        <section class="container-fluid margin-bottom-2x">
            <div class="block-title">
                <h3>Opening Soon</h3>
            </div>

            <div class="card-group">
                @foreach($osItems as $osItem)
                    <div class="card margin-bottom-1x">
                        @if ($osItem->image_path != null)
                            <img class="card-img-top opening-soon-image" src="{{ cdn($osItem->image_path) }}" alt="{{ $osItem->name }}">
                        @endif
                        <div class="card-body">
                            <h4 class="card-title">{{ $osItem->name }}</h4>
                            <p class="card-text">{{ $osItem->description }}</p>
                        </div>
                    </div>
                @endforeach

                @if (sizeof($osItems) < 4)
                    @for($i=1; $i <= 4-sizeof($osItems); $i++)
                        <div class="card margin-bottom-1x"></div>
                    @endfor
                @endif
            </div>
        </section>
    @endif

    <section class="container-fluid margin-bottom-2x">
        <div class="block-title">
            <h3>Best Selling Items</h3>
            <span><a href="#">+ More</a></span>
        </div>

        <ul class="product-container-8x">
            @foreach($bestItems as $item)
                <li>
                    <div class="product-card">
                        <a class="product-thumb" href="{{ route('item_details_page', ['item' => $item->id]) }}">
                            @if (sizeof($item->images) > 0)
                            <img src="{{ cdn($item->images[0]->list_image_path) }}" alt="{{ $item->name }} - Stylepick.net">
                            @else
                            <img src="{{ cdn('images/no-image.jpg') }}" alt="No Image Found">
                            @endif
                        </a>

                        <div class="product-title">
                            <a href="{{ route('vendor_or_parent_category', ['text' => changeSpecialChar($item->vendor->company_name)]) }}" class="vendor-name">{{ $item->vendor->company_name }}</a>
                        </div>
                        <h3 class="product-title">
                            <a class="style-no" href="{{ route('item_details_page', ['item' => $item->id]) }}">{{ $item->style_no }}</a>
                        </h3>

                        <div class="product-extra-info">
                            @if (sizeof($item->colors) > 1)
                                <img class="multi-color" src="{{ cdn('images/multi-color.png') }}" title="Multi Color Available" alt="multi-color">
                            @endif

                            @if ($item->availability == Availability::$ARRIVES_SOON && $item->available_on != null)
                                <img class="calendar-icon" src="{{ cdn('images/calendar-icon.png') }}" alt="calender-icon"> {{ date('m/d/Y', strtotime($item->available_on)) }}
                            @endif
                        </div>
                    </div>
                </li>
            @endforeach
        </ul>
    </section>
    <section class="container-fluid margin-bottom-2x">
        <div class="block-title">
            <h3>Who We Are</h3>
        </div>
        <div>
            <p>
                STYLEPICK is the fastest growing Online Wholesale Marketplace.
                It is a B2B eCommerce solution empowering wholesalers and retailers
                to streamline their business operations. We are a disruptive technology
                revolutionizing how wholesale business is conducted.
                Showcase your products to thousands of buyers with new buyers
                joining every day!
            </p>
            <div class="block-title">
                <h3>What We Do</h3>
            </div>
                <p>
                    We’ve digitized the B2B sales and buying process and it’s innovative, easy to use and aesthetically beautiful. We invite you to decrease costs and increase orders by joining the B2B eCommerce revolution today. We have reduced ordering time allowing wholesalers to respond more quickly to buyer requests and store inventory needs. Also, we provide a full service custom website with automated product upload, payment processing, domain and storage. We are here to unify your troops and strengthen your resolve. We are here to lighten your workload and make your sales soar. We are here to transport to a place of efficiency.
                </p>
        </div>
    </section>
@stop

@section('mobile_filter')
    <!-- Default Modal-->
    <div class="modal fade" id="modalWelcome" tabindex="-1" role="dialog">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <button class="close" type="button" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                </div>
                <div class="modal-body">
                    {!! $welcome_msg !!}
                </div>
            </div>
        </div>
    </div>
@stop

@section('additionalJS')
    <script type="text/javascript" src="{{ cdn('js/slider.js') }}"></script>
    <script>
        $.ajaxSetup({
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            }
        });

        // WishList
        $(document).on('click', '.btnAddWishList', function () {
            var id = $(this).data('id');
            $this = $(this);

            $.ajax({
                method: "POST",
                url: "{{ route('add_to_wishlist') }}",
                data: { id: id }
            }).done(function( data ) {
                toastr.success('Added to Wish List.');

                $this.removeClass('btnAddWishList');
                $this.removeClass('btn-default');
                $this.addClass('btnRemoveWishList');
                $this.addClass('btn-danger');
            });
        });

        $(document).on('click', '.btnRemoveWishList', function () {
            var id = $(this).data('id');
            $this = $(this);

            $.ajax({
                method: "POST",
                url: "{{ route('remove_from_wishlist') }}",
                data: { id: id }
            }).done(function( data ) {
                toastr.success('Remove from Wish List.');

                $this.removeClass('btnRemoveWishList');
                $this.removeClass('btn-danger');
                $this.addClass('btnAddWishList');
                $this.addClass('btn-default');
            });
        });

        // Show Notification
        setTimeout(function(){
            var msg = '{{ $welcome_msg }}';

            if (msg != '')
                $('#modalWelcome').modal('show');
        }, 5000);
    </script>
@stop