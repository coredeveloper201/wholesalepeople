<?php
    use App\Enumeration\Availability;
    use App\Enumeration\Role;
?>

@extends('layouts.app')

@section('additionalCSS')
    <link href="{{ cdn('plugins/toastr/toastr.min.css') }}" rel="stylesheet">
    <style>
        @import url('//maxcdn.bootstrapcdn.com/font-awesome/4.1.0/css/font-awesome.min.css')
    </style>

    <style>
        .modal-dialog{
            overflow-y: initial !important
        }
        .modal-body{
            height: 600px;
            overflow-y: auto;
        }
    </style>
@stop

@section('content')
<div class="container-fluid">
    <div class="row">
        <div class="col-md-12">
            <div class="row">
                <div class="col-lg-8 col-md-12 vendorbanner">
                    @if ($bannerPath != '')
                    <img src="{{ $bannerPath }}">
                    @endif
                </div>
                <div class="col-lg-4 hidden-md-down">
                    <div class="vendorabout">
                        <h3>About Us</h3>{!! nl2br($vendor->company_info)  !!}
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="row">
        <div class="col-md-12 text-right">
            {{ Breadcrumbs::render('vendor_page', $vendor) }}
        </div>
    </div>
    <div class="row">
        <div class="col-xl-2 col-lg-2 order-lg-1">
            <button class="sidebar-toggle position-left" data-toggle="modal" data-target="#modalShopFilters"><i class="icon-layout"></i></button>
            <aside class="sidebar-offcanvas">
                @if ($smallBannerPath != '')
                    <img src="{{ $smallBannerPath }}">
                @endif

                <section class="vendorinfo">
                    <ul>
                        <li><b>COMPANY: </b> {{ $vendor->company_name }}</li>
                        <li><b>ADDRESS: </b> {{ $vendor->billing_address }}</li>
                        <li><b>CITY:</b> {{ $vendor->billing_city }}</li>
                        <li><b>ZipCode:</b> {{ $vendor->billing_zip }}</li>
                        <li><b>Phone:</b> {{ $vendor->billing_phone }}</li>
                        <li><b>Rating:</b> {{ number_format($rating, 2, '.', '') }}</li>
                        <li><b>Reviews:</b> <a href="#" id="btnViewReviews">{{ $totalReviews }}</a></li>
                    </ul>
                </section>
                <section class="widget widget-categories">
                    <h3 class="widget-title">VENDOR CATEGORY</h3>
                    <ul>
                        <li><a href="{{ route('vendor_category_all_page', ['vendor' => $vendor->id]) }}">All Category</a></li>
                        @foreach($vendor->parentCategories as $cat)
                            @if (sizeof($cat->subCategories) > 0)
                                <li class="has-children expanded"><a href="{{ route('vendor_category_page', ['category' => $cat->id]) }}">{{ $cat->name }}</a></span>
                                    <ul>
                                        @foreach($cat->subCategories as $sub)
                                            <li>
                                                <a href="{{ route('vendor_category_page', ['category' => $sub->id]) }}">{{ $sub->name }}</a>
                                            </li>
                                        @endforeach
                                    </ul>
                                </li>
                            @else
                                <li><a href="{{ route('vendor_category_page', ['category' => $cat->id]) }}">{{ $cat->name }}</a></li>
                            @endif
                        @endforeach
                    </ul>
                </section>
            </aside>
        </div>

        <div class="col-xl-10 col-lg-10 order-lg-2">
            @if ((Auth::check() && Auth::user()->role == Role::$BUYER) || (Auth::check() && (Auth::user()->role == Role::$VENDOR || Auth::user()->role == Role::$VENDOR_EMPLOYEE) && Auth::user()->vendor_meta_id == $vendor->id))
            <div class="row">
                <h3 class="vendortitle">Best Selling Items</h3>
            </div>
            <div id="product-container">
                <ul class="product-container-6x">
                    @foreach($bestItems as $item)
                    <li>
                        <div class="product-card">
                            <a class="product-thumb" href="{{ route('item_details_page', ['item' => $item->id]) }}">
                                    @if (sizeof($item->images) > 0)
                                        <img src="{{ cdn($item->images[0]->list_image_path) }}" alt="{{ $item->style_no }}">
                                    @else
                                        <img src="{{ cdn('images/no-image.png') }}" alt="{{ $item->style_no }}">
                                    @endif
                            </a>
                            <div class="item-detail">
                                <h3 class="product-sku">
                                    <a class="style-no" href="{{ route('item_details_page', ['item' => $item->id]) }}">{{ $item->style_no }}</a>

                                    @if (Auth::check() && Auth::user()->role == Role::$BUYER)
                                        <span class="price">
                                            @if ($item->orig_price != null)
                                                <del>${{ number_format($item->orig_price, 2, '.', '') }}</del>
                                            @endif
                                            ${{ number_format($item->price, 2, '.', '') }}
                                        </span>
                                    @endif
                                </h3>
                            </div>

                            <div class="product-extra-info">

                                @if (sizeof($item->colors) > 1)
                                    <img class="multi-color" src="{{ cdn('images/multi-color.png') }}" title="Multi Color Available">
                                @endif
                                @if ($item->availability == Availability::$ARRIVES_SOON && $item->available_on != null)
                                    <img class="calendar-icon" src="{{ cdn('images/calendar-icon.png') }}"> {{ date('m/d/Y', strtotime($item->available_on)) }}
                                @endif
                            </div>
                        </div>
                    </li>
                    @endforeach
                </ul>
            </div>

            <div class="row"><h3 class="vendortitle">New Items</h3></div>
            <div id="product-container">
                <ul class="product-container-6x">
                    @foreach($newestItems as $item)
                    <li>
                        <div class="product-card">
                            <a class="product-thumb" href="{{ route('item_details_page', ['item' => $item->id]) }}">
                                    @if (sizeof($item->images) > 0)
                                        <img src="{{ cdn($item->images[0]->list_image_path) }}" alt="{{ $item->style_no }}">
                                    @else
                                        <img src="{{ cdn('images/no-image.png') }}" alt="{{ $item->style_no }}">
                                    @endif
                            </a>
                            <div class="item-detail">
                                <h3 class="product-sku">
                                    <a class="style-no" href="{{ route('item_details_page', ['item' => $item->id]) }}">{{ $item->style_no }}</a>

                                    @if (Auth::check() && Auth::user()->role == Role::$BUYER)
                                        <span class="price">
                                            @if ($item->orig_price != null)
                                                <del>${{ number_format($item->orig_price, 2, '.', '') }}</del>
                                            @endif

                                            ${{ sprintf('%0.2f', $item->price) }}
                                        </span>
                                    @endif
                                </h3>
                            </div>

                            <div class="product-extra-info">
                                @if (sizeof($item->colors) > 1)
                                    <img class="multi-color" src="{{ cdn('images/multi-color.png') }}" title="Multi Color Available">
                                @endif
                                @if ($item->availability == Availability::$ARRIVES_SOON && $item->available_on != null)
                                        <img class="calendar-icon" src="{{ cdn('images/calendar-icon.png') }}">{{ date('m/d/Y', strtotime($item->available_on)) }}
                                @endif
                            </div>
                        </div>
                    </li>
                    @endforeach
                </ul>
            </div>
            @else
                <div class="vendorlogin padding-top-10x padding-bottom-10x">
                    <h2>Sign in Needed</h2>
                    <div class="requiredlogin">You Must <a href="{{ route('buyer_login') }}">Signin</a> to view this vendor's information</div>
                </div>
            @endif
        </div>
    </div>
</div>
@stop

@section('mobile_filter')
<div class="modal fade" id="modalShopFilters" tabindex="-1">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <h4 class="modal-title">Filters</h4>
                <button class="close" type="button" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
            </div>
            <div class="modal-body">
                @if ($smallBannerPath != '')
                    <img src="{{ $smallBannerPath }}">
                @endif

                <section class="vendorinfo">
                    <ul>
                        <li><b>COMPANY: </b> {{ $vendor->company_name }}</li>
                        <li><b>ADDRESS: </b> {{ $vendor->billing_address }}</li>
                        <li><b>CITY:</b> {{ $vendor->billing_city }}</li>
                        <li><b>ZipCode:</b> {{ $vendor->billing_zip }}</li>
                        <li><b>Phone:</b> {{ $vendor->billing_phone }}</li>
                    </ul>
                </section>
                <section class="widget widget-categories">
                    <h3 class="widget-title">VENDOR CATEGORY</h3>
                    <ul>
                        @foreach($vendor->parentCategories as $cat)
                            <li><a href="{{ route('vendor_category_page', ['category' => $cat->id]) }}">{{ $cat->name }}</a></li>
                        @endforeach
                    </ul>
                </section>
            </div>
        </div>
    </div>
</div>

<div class="modal fade" id="modalReviews" tabindex="-1" role="dialog">
    <div class="modal-dialog modal-lg" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h4 class="modal-title">Reviews</h4>
                <button class="close" type="button" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
            </div>
            <div class="modal-body">
                <div class="row">
                    <div class="col-md-6">
                        <label class="text-medium">5 star - {{ $star[4] }}</label>
                        <div class="progress margin-bottom-1x">
                            <div class="progress-bar bg-warning" role="progressbar" style="width: {{ $totalReviews == 0 ? 0 : ($star[4] / $totalReviews) * 100 }}%; height: 2px;" aria-valuenow="{{ $totalReviews == 0 ? 0 : ($star[4] / $totalReviews) * 100 }}" aria-valuemin="0" aria-valuemax="100"></div>
                        </div>

                        <label class="text-medium">4 star - {{ $star[3] }}</label>
                        <div class="progress margin-bottom-1x">
                            <div class="progress-bar bg-warning" role="progressbar" style="width: {{ $totalReviews == 0 ? 0 : ($star[3] / $totalReviews) * 100 }}%; height: 2px;" aria-valuenow="{{ $totalReviews == 0 ? 0 : ($star[3] / $totalReviews) * 100 }}" aria-valuemin="0" aria-valuemax="100"></div>
                        </div>

                        <label class="text-medium">3 star - {{ $star[2] }}</label>
                        <div class="progress margin-bottom-1x">
                            <div class="progress-bar bg-warning" role="progressbar" style="width: {{ $totalReviews == 0 ? 0 : ($star[2] / $totalReviews) * 100 }}%; height: 2px;" aria-valuenow="{{ $totalReviews == 0 ? 0 : ($star[2] / $totalReviews) * 100 }}" aria-valuemin="0" aria-valuemax="100"></div>
                        </div>

                        <label class="text-medium">2 star - {{ $star[1] }}</label>
                        <div class="progress margin-bottom-1x">
                            <div class="progress-bar bg-warning" role="progressbar" style="width: {{ $totalReviews == 0 ? 0 : ($star[1] / $totalReviews) * 100 }}%; height: 2px;" aria-valuenow="{{ $totalReviews == 0 ? 0 : ($star[1] / $totalReviews) * 100 }}" aria-valuemin="0" aria-valuemax="100"></div>
                        </div>

                        <label class="text-medium">1 star - {{ $star[0] }}</label>
                        <div class="progress margin-bottom-1x">
                            <div class="progress-bar bg-warning" role="progressbar" style="width: {{ $totalReviews == 0 ? 0 : ($star[0] / $totalReviews) * 100 }}%; height: 2px;" aria-valuenow="{{ $totalReviews == 0 ? 0 : ($star[0] / $totalReviews) * 100 }}" aria-valuemin="0" aria-valuemax="100"></div>
                        </div>
                    </div>

                    <div class="col-md-6 text-right">
                        @if (Auth::check() && Auth::user()->role == Role::$BUYER)
                            <button class="btn btn-primary" id="btnWriteReview">Write a Review</button>
                        @endif
                    </div>
                </div>

                <div class="row">
                    <div class="col-md-12">
                        <table class="table table-bordered">
                            <thead>
                                <tr>
                                    <th>Rating</th>
                                    <th>Name</th>
                                    <th>Review</th>
                                    <th>Date</th>
                                </tr>
                            </thead>

                            <tbody>
                                @foreach($reviews as $review)
                                    <tr>
                                        <td>
                                            @for($i=0; $i<$review->star; $i++)
                                                <i class='fa fa-star'></i>
                                            @endfor
                                        </td>

                                        <td>{{ $review->user->first_name.' '.$review->user->last_name }}</td>
                                        <td>{{ $review->review }}</td>
                                        <td>{{ date('F d, Y', strtotime($review->created_at)) }}</td>
                                    </tr>
                                @endforeach
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

<div class="modal fade" id="modalWriteReview" tabindex="-1" role="dialog">
    <div class="modal-dialog modal-lg" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h4 class="modal-title">Write Reviews</h4>
                <button class="close" type="button" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
            </div>

            <form action="{{ route('buyer_feedback_post') }}" method="POST">
                @csrf
                <div class="modal-body">
                    <table class="table table-bordered">
                        <thead>
                        <tr>
                            <th></th>
                            <th>Date</th>
                            <th>Order Number</th>
                            <th>Rating</th>
                            <th>Comments</th>
                        </tr>
                        </thead>

                        <tbody>
                        @foreach($orders as $order)
                            <tr>
                                <td>
                                    <div class="custom-control custom-checkbox d-block">
                                        <input class="custom-control-input order_checkbox" type="checkbox" id="checkbox_{{ $order->id }}" name="ids[]" value="{{ $order->id }}">
                                        <label class="custom-control-label" for="checkbox_{{ $order->id }}"></label>
                                    </div>
                                </td>
                                <td>{{ date('m/d/Y', strtotime($order->created_at)) }}</td>
                                <td>
                                    <a href="{{ route('show_order_details', ['order' => $order->id]) }}" target="_blank">{{ $order->order_number }}</a>
                                </td>
                                <td>
                                    <div class="lead">
                                        <div class="starrr" data-rating='{{ $order->star }}'></div>
                                        <input type="hidden" name="star_{{ $order->id }}" class="star_input" value="{{ $order->star }}">
                                    </div>
                                </td>
                                <td>
                                    <input type="text" class="form-control form-control-rounded form-control-sm comment" name="comment_{{ $order->id }}" value="{{ $order->review }}">
                                </td>
                            </tr>
                        @endforeach
                        </tbody>
                    </table>
                </div>

                <div class="modal-footer">
                    <input class="btn btn-primary btn-sm" type="submit" value="SAVE">
                </div>
            </form>
        </div>
    </div>
</div>
@stop

@section('additionalJS')
    <script type="text/javascript" src="{{ cdn('plugins/toastr/toastr.min.js') }}"></script>
    <script type="text/javascript" src="{{ cdn('js/slider.js') }}"></script>
    <script type="text/javascript" src="{{ cdn('js/star.js') }}"></script>
    <script>
        $.ajaxSetup({
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            }
        });

        $(document).on('click', '.btnRemoveWishList', function () {
            var id = $(this).data('id');
            $this = $(this);

            $.ajax({
                method: "POST",
                url: "{{ route('remove_from_wishlist') }}",
                data: { id: id }
            }).done(function( data ) {
                toastr.success('Remove from Wish List.');

                $this.removeClass('btnRemoveWishList');
                $this.removeClass('btn-danger');
                $this.addClass('btnAddWishList');
                $this.addClass('btn-default');
            });
        });

        $('#btnViewReviews').click(function () {
            $('#modalReviews').modal('show');
        });

        $('#btnWriteReview').click(function () {
            $('#modalReviews').modal('hide');
            $('#modalWriteReview').modal('show');
        });

        $('.starrr').on('starrr:change', function(e, value){
            var index = $('.starrr').index($(this));
            $('.order_checkbox:eq('+index+')').prop('checked', true);
            $('.star_input:eq('+index+')').val(value);

            $('#count').html(value);
        });

        $('.comment').keyup(function () {
            var index = $('.comment').index($(this));
            $('.order_checkbox:eq('+index+')').prop('checked', true);
        });
    </script>
@stop