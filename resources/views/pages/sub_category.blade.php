<?php
    use App\Enumeration\Role;
    use App\Enumeration\Availability;
?>

@extends('layouts.app')

@section('additionalCSS')
    <link href="{{ cdn('plugins/toastr/toastr.min.css') }}" rel="stylesheet">
    <style>
        .color-selected {
            border: 1px solid black !important;
        }
    </style>
@stop

@section('content')
<div class="divLoading">
    <img src="{{ asset('images/loading.gif') }}">
</div>

<div class="container-fluid">
    <div class="content col-md-12">
        <div class="row">
            <div class="col-md-8">
                <h4>{{ $category->name }}</h4>
                <div class="title-desc">
                    <span id="totalItem"></span>
                    <span>Items Found</span> 
                </div>
            </div>
            <div class="col-md-4 text-right">
                <div class="column">
                    <div class="shop-sorting">
                        <label for="sorting">SORT BY</label>
                        <select class="form-control" id="sorting">
                            <option value="1">New</option>
                            <option value="2">Price Low-High </option>
                            <option value="3">Price High-Low</option>
                            <option value="4">Style No.</option>
                        </select>
                    </div>
                </div>

                {{--<div class="column">
                    <div class="shop-sorting">
                        <label for="sorting">ITEM PER PAGE</label>
                        <select class="form-control" id="sorting">
                            <option selected>20</option>
                            <option>40</option>
                            <option>60</option>
                            <option>80</option>
                            <option>100</option>
                        </select>
                    </div>
                </div>--}}
            </div>
        </div>
    </div>
    {{ Breadcrumbs::render('second_parent_category_page', $category) }}
</div>
<div class="container-fluid">
    <div class="row">
        <div class="col-xl-10 col-lg-8 order-lg-2">
            <ul class="product-container-6x" id="product-container">
                @foreach($items as $item)
                <li>
                    <div class="product-card">
                        <a class="product-thumb" href="{{ route('item_details_page', ['item' => $item->id]) }}">
                            @if (Auth::check() && Auth::user()->role == Role::$BUYER)
                                @if (sizeof($item->images) > 0)
                                    <img src="{{ cdn($item->images[0]->list_image_path) }}" alt="{{ $item->style_no }}">
                                @else
                                    <img src="{{ cdn('images/no-image.png') }}" alt="{{ $item->style_no }}">
                                @endif
                            @else
                                <img src="{{ cdn('images/no-image.jpg') }}">
                            @endif
                        </a>
                        <div class="product-title"><b><a href="#">{{ $item->vendor->company_name }}</a></b></div>
                        <h3 class="product-title">
                            <a href="{{ route('item_details_page', ['item' => $item->id]) }}">{{ $item->style_no }}</a>
                            @if (Auth::check() && Auth::user()->role == Role::$BUYER)
                                <span class="price">
                                    @if ($item->orig_price != null)
                                        <del>${{ number_format($item->orig_price, 2, '.', '') }}</del>
                                    @endif

                                    ${{ sprintf('%0.2f', $item->price) }}
                                </span>
                            @endif
                        </h3>

                        <div class="product-extra-info">
                            @if (sizeof($item->colors) > 1)
                                <img class="multi-color" src="{{ cdn('images/multi-color.png') }}" title="Multi Color Available">
                            @endif

                            @if ($item->availability == Availability::$ARRIVES_SOON && $item->available_on != null)
                                <span title="Available On">
                                    <img class="calendar-icon" src="{{ cdn('images/calendar-icon.png') }}"> {{ date('m/d/Y', strtotime($item->available_on)) }}
                                </span>
                            @endif
                        </div>
                    </div>
                </li>
                @endforeach
            </ul>

            <div class="pagination">
                {{--{{ $items->links('others.pagination') }}--}}
            </div>
            {{--<nav class="pagination">
                <div class="column hidden-xs-down"><a class="btn btn-outline-secondary btn-sm" href="#"><i class="icon-arrow-left"></i>Previous&nbsp;</a></div>
                <div class="column">
                    <ul class="pages">
                        <li class="active"><a href="#">1</a></li>
                        <li><a href="#">2</a></li>
                        <li><a href="#">3</a></li>
                        <li><a href="#">4</a></li>
                        <li>...</li>
                        <li><a href="#">12</a></li>
                    </ul>
                </div>
                <div class="column text-right hidden-xs-down"><a class="btn btn-outline-secondary btn-sm" href="#">Next&nbsp;<i class="icon-arrow-right"></i></a></div>
            </nav>--}}
        </div>

        <div class="col-xl-2 col-lg-4 order-lg-1">
            <button class="sidebar-toggle position-left" data-toggle="modal" data-target="#modalShopFilters"><i class="icon-layout"></i></button>
            <aside class="sidebar-offcanvas">
                <!-- Widget Categories-->
                <section class="widget widget-categories">
                    <h3 class="widget-title">CATEGORY</h3>
                    <ul>
                        @foreach($category->parentCategory->subCategories as $sub)
                            @if ($sub->id == $category->id)
                                <li class="has-children expanded"><a href="#">{{ $sub->name }}</a></span>
                                    <ul>
                                        @foreach($category->subCategories as $sub2)
                                            <li>
                                                <div class="custom-control custom-checkbox">
                                                    <input class="custom-control-input checkbox-category" type="checkbox"
                                                           data-id="{{ $sub2->id }}" id="category_{{ $sub2->id }}">
                                                    <label class="custom-control-label" for="category_{{ $sub2->id }}">{{ $sub2->name }} ({{ $sub2->totalItem }})</label>
                                                </div>
                                            </li>
                                        @endforeach
                                    </ul>
                                </li>
                            @else
                                <li><a href="{{ route('second_category', ['category' => changeSpecialChar($sub->name), 'parent' => changeSpecialChar($sub->parentCategory->name)]) }}">{{ $sub->name }}</a></li>
                            @endif
                        @endforeach
                    </ul>
                </section>

                <!-- Widget Brand Filter-->
                <section class="widget widget-categories">
                    <h3 class="widget-title">SEARCH</h3>

                    <div class="custom-control custom-radio custom-control-inline">
                        <input class="custom-control-input" type="radio" id="search-style-no" name="search-component" value="1" checked>
                        <label class="custom-control-label" for="search-style-no">Style No.</label>
                    </div>

                    <div class="custom-control custom-radio custom-control-inline">
                        <input class="custom-control-input" type="radio" id="search-description" name="search-component" value="2">
                        <label class="custom-control-label" for="search-description">Description</label>
                    </div>

                    <div class="form-group">
                        <input class="form-control" id="search-input" type="text" placeholder="Search">
                    </div>

                    <div class="row">
                        <div class="col-md-6">
                            <input class="form-control" id="search-price-min" type="text" placeholder="Price Min">
                        </div>

                        <div class="col-md-6">
                            <input class="form-control" id="search-price-max" type="text" placeholder="Price Max">
                        </div>
                    </div>

                    <button class="btn btn-primary" id="btn-search">SEARCH</button>
                </section>

                <!-- Widget Brand Filter-->
                <section class="widget">
                    <h3 class="widget-title">VENDORS</h3>

                    @foreach($vendors as $vendor)
                        <div class="custom-control custom-checkbox">
                            <input class="custom-control-input vendor-checkbox" type="checkbox" id="vendor_{{ $vendor->id }}" data-id="{{ $vendor->id }}">
                            <label class="custom-control-label" for="vendor_{{ $vendor->id }}">{{ $vendor->company_name }} ({{ $vendor->totalItem }})</label>
                        </div>
                    @endforeach
                </section>

                <!-- Widget Brand Filter-->
                <section class="widget">
                    <h3 class="widget-title">COLORS</h3>

                    <ul class="sidecolor">
                        @foreach($masterColors as $mc)
                            <li class="item-color" data-id="{{ $mc->id }}" title="{{ $mc->name }}">
                                <img src="{{ cdn($mc->image_path) }}" width="30px" height="20px">
                            </li>
                        @endforeach
                    </ul>
                </section>

                <section class="widget widget-categories" style="clear: both;">
                    <h3 class="widget-title"></h3>
                    <ul>
                        <li class="has-children expanded"><a href="#">BODY SIZE</a></span>
                            <ul>
                                @foreach($bodySizes as $bz)
                                    <li>
                                        <div class="custom-control custom-checkbox">
                                            <input class="custom-control-input checkbox-body-size" type="checkbox" id="body_size_{{ $bz->id }}" data-id="{{ $bz->id }}">
                                            <label class="custom-control-label" for="body_size_{{ $bz->id }}">{{ $bz->name }}</label>
                                        </div>
                                    </li>
                                @endforeach
                            </ul>
                        </li>

                        <li class="has-children"><a href="#">PATTERN</a></span>
                            <ul>
                                @foreach($patterns as $p)
                                    <li>
                                        <div class="custom-control custom-checkbox">
                                            <input class="custom-control-input checkbox-pattern" type="checkbox" id="pattern_{{ $p->id }}" data-id="{{ $p->id }}">
                                            <label class="custom-control-label" for="pattern_{{ $p->id }}">{{ $p->name }}</label>
                                        </div>
                                    </li>
                                @endforeach
                            </ul>
                        </li>

                        <li class="has-children"><a href="#">LENGTH</a></span>
                            <ul>
                                @foreach($category->lengths as $l)
                                    <li>
                                        <div class="custom-control custom-checkbox">
                                            <input class="custom-control-input checkbox-length" type="checkbox" id="length_{{ $l->id }}" data-id="{{ $l->id }}">
                                            <label class="custom-control-label" for="length_{{ $l->id }}">{{ $l->name }}</label>
                                        </div>
                                    </li>
                                @endforeach
                            </ul>
                        </li>

                        <li class="has-children"><a href="#">STYLE</a></span>
                            <ul>
                                @foreach($styles as $s)
                                    <li>
                                        <div class="custom-control custom-checkbox">
                                            <input class="custom-control-input checkbox-style" type="checkbox" id="style_{{ $s->id }}" data-id="{{ $s->id }}">
                                            <label class="custom-control-label" for="style_{{ $s->id }}">{{ $s->name }}</label>
                                        </div>
                                    </li>
                                @endforeach
                            </ul>
                        </li>

                        <li class="has-children"><a href="#">FABRIC</a></span>
                            <ul>
                                @foreach($masterFabrics as $mf)
                                    <li>
                                        <div class="custom-control custom-checkbox">
                                            <input class="custom-control-input checkbox-fabric" type="checkbox" id="fabric_{{ $mf->id }}" data-id="{{ $mf->id }}">
                                            <label class="custom-control-label" for="fabric_{{ $mf->id }}">{{ $mf->name }}</label>
                                        </div>
                                    </li>
                                @endforeach
                            </ul>
                        </li>
                    </ul>
                </section>
            </aside>
        </div>
    </div>
</div>
    <template id="template-product">
        <li>
            <div class="product-card">
                <a class="product-thumb">
                    <img class="product-image" src="{{ cdn('images/no-image.png') }}">
                </a>
                <div class="product-title"><b><a href="#" class="vendor-name"></a></b></div>

                <h3 class="product-title">
                    <a class="style-no"></a><span class="price"></span>
                </h3>

                <div class="product-extra-info">
                    <img class="multi-color d-none" src="{{ cdn('images/multi-color.png') }}" title="Multi Color Available">

                    <span class="available-on d-none" title="Available On">
                        <img class="calendar-icon" src="{{ cdn('images/calendar-icon.png') }}"> <span class="available-date"></span>
                    </span>
                </div>
            </div>
        </li>
    </template>
@stop

@section('mobile_filter')
<div class="modal fade" id="modalShopFilters" tabindex="-1">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <h4 class="modal-title">Filters</h4>
                <button class="close" type="button" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
            </div>
            <div class="modal-body">
                <section class="widget widget-categories">
                    <h3 class="widget-title">CATEGORY</h3>
                    <ul>
                        @foreach($category->parentCategory->subCategories as $sub)
                            @if ($sub->id == $category->id)
                                <li class="has-children expanded"><a href="#">{{ $sub->name }}</a></span>
                                    <ul>
                                        @foreach($category->subCategories as $sub2)
                                            <li>
                                                <div class="custom-control custom-checkbox">
                                                    <input class="custom-control-input checkbox-category" type="checkbox"
                                                           data-id="{{ $sub2->id }}" id="category_mobile_{{ $sub2->id }}">
                                                    <label class="custom-control-label" for="category_mobile_{{ $sub2->id }}">{{ $sub2->name }} ({{ $sub2->totalItem }})</label>
                                                </div>
                                            </li>
                                        @endforeach
                                    </ul>
                                </li>
                            @else
                                <li><a href="{{ route('second_category', ['category' => changeSpecialChar($sub->name), 'parent' => changeSpecialChar($sub->parentCategory->name)]) }}">{{ $sub->name }}</a></li>
                            @endif
                        @endforeach
                    </ul>
                </section>

                <!-- Widget Brand Filter-->
                <section class="widget widget-categories">
                    <h3 class="widget-title">SEARCH</h3>

                    <div class="custom-control custom-radio custom-control-inline">
                        <input class="custom-control-input" type="radio" id="mobile-search-style-no" name="search-component" value="1" checked>
                        <label class="custom-control-label" for="mobile-search-style-no">Style No.</label>
                    </div>

                    <div class="custom-control custom-radio custom-control-inline">
                        <input class="custom-control-input" type="radio" id="mobile-search-description" name="search-component" value="2">
                        <label class="custom-control-label" for="mobile-search-description">Description</label>
                    </div>

                    <div class="form-group">
                        <input class="form-control" id="search-input" type="text" placeholder="Search">
                    </div>

                    <div class="row">
                        <div class="col-md-6">
                            <input class="form-control" id="search-price-min" type="text" placeholder="Price Min">
                        </div>

                        <div class="col-md-6">
                            <input class="form-control" id="search-price-max" type="text" placeholder="Price Max">
                        </div>
                    </div>

                    <button class="btn btn-primary" id="btn-search">SEARCH</button>
                </section>

                <!-- Widget Brand Filter-->
                <section class="widget">
                    <h3 class="widget-title">VENDORS</h3>

                    @foreach($vendors as $vendor)
                        <div class="custom-control custom-checkbox">
                            <input class="custom-control-input vendor-checkbox" type="checkbox" id="vendor_mobile_{{ $vendor->id }}" data-id="{{ $vendor->id }}">
                            <label class="custom-control-label" for="vendor_mobile_{{ $vendor->id }}">{{ $vendor->company_name }} ({{ $vendor->totalItem }})</label>
                        </div>
                    @endforeach
                </section>

                <!-- Widget Brand Filter-->
                <section class="widget">
                    <h3 class="widget-title">COLORS</h3>

                    <ul style="padding-left: 0px; list-style: none; display: block; clear:both; margin-bottom: 30px">
                        @foreach($masterColors as $mc)
                            <li class="item-color" style="position: relative; float: left; display: list-item; padding: 3px; border: 1px solid white"
                                data-id="{{ $mc->id }}" title="{{ $mc->name }}">
                                <img src="{{ cdn($mc->image_path) }}" width="30px" height="20px">
                            </li>
                        @endforeach
                    </ul>
                </section>

                <section class="widget widget-categories" style="clear: both;">
                    <h3 class="widget-title"></h3>
                    <ul>
                        <li class="has-children expanded"><a href="#">BODY SIZE</a></span>
                            <ul>
                                @foreach($bodySizes as $bz)
                                    <li>
                                        <div class="custom-control custom-checkbox">
                                            <input class="custom-control-input checkbox-body-size" type="checkbox" id="body_size_mobile_{{ $bz->id }}" data-id="{{ $bz->id }}">
                                            <label class="custom-control-label" for="body_size_mobile_{{ $bz->id }}">{{ $bz->name }}</label>
                                        </div>
                                    </li>
                                @endforeach
                            </ul>
                        </li>

                        <li class="has-children"><a href="#">PATTERN</a></span>
                            <ul>
                                @foreach($patterns as $p)
                                    <li>
                                        <div class="custom-control custom-checkbox">
                                            <input class="custom-control-input checkbox-pattern" type="checkbox" id="pattern_mobile_{{ $p->id }}" data-id="{{ $p->id }}">
                                            <label class="custom-control-label" for="pattern_mobile_{{ $p->id }}">{{ $p->name }}</label>
                                        </div>
                                    </li>
                                @endforeach
                            </ul>
                        </li>

                        <li class="has-children"><a href="#">LENGTH</a></span>
                            <ul>
                                @foreach($category->lengths as $l)
                                    <li>
                                        <div class="custom-control custom-checkbox">
                                            <input class="custom-control-input checkbox-length" type="checkbox" id="length_mobile_{{ $l->id }}" data-id="{{ $l->id }}">
                                            <label class="custom-control-label" for="length_mobile_{{ $l->id }}">{{ $l->name }}</label>
                                        </div>
                                    </li>
                                @endforeach
                            </ul>
                        </li>

                        <li class="has-children"><a href="#">STYLE</a></span>
                            <ul>
                                @foreach($styles as $s)
                                    <li>
                                        <div class="custom-control custom-checkbox">
                                            <input class="custom-control-input checkbox-style" type="checkbox" id="style_mobile_{{ $s->id }}" data-id="{{ $s->id }}">
                                            <label class="custom-control-label" for="style_mobile_{{ $s->id }}">{{ $s->name }}</label>
                                        </div>
                                    </li>
                                @endforeach
                            </ul>
                        </li>

                        <li class="has-children"><a href="#">FABRIC</a></span>
                            <ul>
                                @foreach($masterFabrics as $mf)
                                    <li>
                                        <div class="custom-control custom-checkbox">
                                            <input class="custom-control-input checkbox-fabric" type="checkbox" id="fabric_mobile_{{ $mf->id }}" data-id="{{ $mf->id }}">
                                            <label class="custom-control-label" for="fabric_mobile_{{ $mf->id }}">{{ $mf->name }}</label>
                                        </div>
                                    </li>
                                @endforeach
                            </ul>
                        </li>
                    </ul>
                </section>
            </div>
        </div>
    </div>
</div>
@stop

@section('additionalJS')
    <script type="text/javascript" src="{{ cdn('plugins/toastr/toastr.min.js') }}"></script>
    <script>
        $(function () {
            $.ajaxSetup({
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                }
            });

            var page = 1;
            var search_text = '';
            var search_option = '';
            var search_price_min = '';
            var search_price_max = '';

            
            $('.checkbox-category, .vendor-checkbox, .checkbox-body-size, .checkbox-pattern, .checkbox-length, .checkbox-style, .checkbox-fabric').change(function () {
                filterItem();
            });

            $('#sorting').change(function () {
                filterItem();
            });

            $('.item-color').click(function () {
                if ($(this).hasClass('color-selected'))
                    $(this).removeClass('color-selected');
                else
                    $(this).addClass('color-selected');

                filterItem();
            });
            
            $('#btn-search').click(function () {
                search_text = $('#search-input').val();
                search_option = $('input[name=search-component]:checked').val();
                search_price_min = $('#search-price-min').val();
                search_price_max = $('#search-price-max').val();

                filterItem();
            });

            function filterItem(page) {
                $(".divLoading").addClass('show');
                page = typeof page !== 'undefined' ? page : 1;
                var secondCategory = '{{ $category->id }}';
                var categories = [];
                var vendors = [];
                var masterColors = [];
                var bodySizes = [];
                var patterns = [];
                var lengths = [];
                var styles = [];
                var fabrics = [];
                var sorting = $('#sorting').val();

                var pos = 0;
                var changePos = localStorage['change_pos'];
                if (changePos) {
                    localStorage.removeItem('change_pos');

                    pos = parseInt(localStorage.getItem('previous_position'));
                }

                $("html, body").animate({ scrollTop: pos }, "fast");

                // Category
                $('.checkbox-category').each(function () {
                    if ($(this).is(':checked'))
                        categories.push($(this).data('id'));
                });

                // Vendor
                $('.vendor-checkbox').each(function () {
                    if ($(this).is(':checked'))
                        vendors.push($(this).data('id'));
                });
                
                // Master Color
                $('.item-color').each(function () {
                    if ($(this).hasClass('color-selected'))
                        masterColors.push($(this).data('id'));
                });

                // Body Size
                $('.checkbox-body-size').each(function () {
                    if ($(this).is(':checked'))
                        bodySizes.push($(this).data('id'));
                });

                // Pattern
                $('.checkbox-pattern').each(function () {
                    if ($(this).is(':checked'))
                        patterns.push($(this).data('id'));
                });

                // Length
                $('.checkbox-length').each(function () {
                    if ($(this).is(':checked'))
                        lengths.push($(this).data('id'));
                });

                // Style
                $('.checkbox-style').each(function () {
                    if ($(this).is(':checked'))
                        styles.push($(this).data('id'));
                });

                // Master Fabric
                $('.checkbox-fabric').each(function () {
                    if ($(this).is(':checked'))
                        fabrics.push($(this).data('id'));
                });


                $.ajax({
                    method: "POST",
                    url: "{{ route('get_items_sub_category') }}",
                    data: { categories: categories, secondCategory: secondCategory, vendors: vendors, masterColors: masterColors, bodySizes: bodySizes, patterns: patterns,
                        lengths: lengths, styles: styles, fabrics: fabrics, sorting: sorting, searchText: search_text, searchOption: search_option,
                        priceMin: search_price_min, priceMax: search_price_max, page: page
                    }
                }).done(function( data ) {
                    var products = data.items;
                    $('.pagination').html(data.pagination);
                    $(".divLoading").removeClass('show');
                    $('#totalItem').html(data.total);

                    $('#product-container').html('');
                    var backOrder = '{{ Availability::$ARRIVES_SOON }}';

                    $.each(products, function (index, product) {
                        var html = $('#template-product').html();
                        var row = $(html);

                        //row.find('.product-image').attr('src', product.imagePath);
                        row.find('.product-thumb').attr('href', product.detailsUrl);
                        row.find('.vendor-name').html(product.company_name);
                        row.find('.vendor-name').attr('href', product.vendorUrl);
                        row.find('.style-no').html(product.style_no);
                        row.find('.style-no').attr('href', product.detailsUrl);
                        row.find('.price').html(product.price);

                        if (product.multiColor == true)
                            row.find('.multi-color').removeClass('d-none');

                        /*if (product.colors.length == 0)
                            row.find('.product-thumb').removeAttr("href");*/

                        if (product.availability == backOrder && product.available_on != '') {
                            row.find('.available-on').removeClass('d-none');
                            var date = new Date(product.available_on);
                            row.find('.available-date').html((date.getMonth()+1) + '/' + date.getDate() + '/' + date.getFullYear());
                        }

                        row.find('.wishlist-button-container').html(product.wishListButton);

                        $('#product-container').append(row);
                        loadImage(row.find('.product-image'), product.imagePath);
                        updatePrice(row, product.id);
                    });
                });
            }

            // WishList
            $(document).on('click', '.btnAddWishList', function () {
                var id = $(this).data('id');
                $this = $(this);

                $.ajax({
                    method: "POST",
                    url: "{{ route('add_to_wishlist') }}",
                    data: { id: id }
                }).done(function( data ) {
                    toastr.success('Added to Wish List.');

                    $this.removeClass('btnAddWishList');
                    $this.removeClass('btn-default');
                    $this.addClass('btnRemoveWishList');
                    $this.addClass('btn-danger');
                });
            });

            $(document).on('click', '.btnRemoveWishList', function () {
                var id = $(this).data('id');
                $this = $(this);

                $.ajax({
                    method: "POST",
                    url: "{{ route('remove_from_wishlist') }}",
                    data: { id: id }
                }).done(function( data ) {
                    toastr.success('Remove from Wish List.');

                    $this.removeClass('btnRemoveWishList');
                    $this.removeClass('btn-danger');
                    $this.addClass('btnAddWishList');
                    $this.addClass('btn-default');
                });
            });

            // Pagination
            $(document).on('click', '.page-link', function (e) {
                e.preventDefault();
                var url = $(this).attr('href');
                page = getURLParameter(url, 'page');

                filterItem(page);
            });

            function getURLParameter(url, name) {
                return (RegExp(name + '=' + '(.+?)(&|$)').exec(url)||[,null])[1];
            }

            // Hold Position
            $(window).bind('beforeunload', function(){
                localStorage['previous_page'] = page;
                localStorage['previous_position'] = $(document).scrollTop()+'';
            });

            var changePage = localStorage['change_page'];
            if (changePage) {
                localStorage.removeItem('change_page');

                page = parseInt(localStorage.getItem('previous_page'));
            }

            filterItem(page);

            function updatePrice(row, id) {
                $.ajax({
                    method: "POST",
                    url: "{{ route('get_item_price') }}",
                    data: { id: id }
                }).done(function( data ) {
                    row.find('.price').html(data);
                });
            }

            function loadImage(img, url) {
                var downloadingImage = new Image();

                downloadingImage.onload = function(){
                    img.attr('src', this.src);
                };
                downloadingImage.src = url;
            }
        });
    </script>
@stop