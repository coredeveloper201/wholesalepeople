<?php use App\Enumeration\Availability; ?>
@extends('admin.layouts.admin')

@section('additionalCSS')
    <link href="{{ asset('plugins/select2/css/select2.min.css') }}" rel="stylesheet">
    <link href="{{ asset('plugins/bootstrap-datepicker/css/bootstrap-datepicker.min.css') }}" rel="stylesheet">
    <link href="{{ asset('plugins/toastr/toastr.min.css') }}" rel="stylesheet">
    <link href="{{ asset('plugins/jquery-ui/jquery-ui.min.css') }}" rel="stylesheet">
@stop

@section('content')
    <div class="row">
        <div class="col-md-4">
            <a class="btn btn-primary btnBack" href="{{ session('back_url') }}">Back To List</a>
        </div>

        <div class="col-md-8 text-right">
            <b>Modified: </b>{{ date('m/d/Y g:i:s a', strtotime($item->updated_at)) }} |
            <b>Activated: </b>{{ $item->activated_at == null ? '' : date('m/d/Y g:i:s a', strtotime($item->activated_at)) }} |
            <b>Created: </b>{{ date('m/d/Y g:i:s a', strtotime($item->created_at)) }}
        </div>
    </div>

    <br>

    <form class="form-horizontal" method="post" action="{{ route('vendor_edit_item_post', ['item' => $item->id]) }}" id="form">
        @csrf

        <input type="hidden" name="ChangeInfo[color__]">
        <input type="hidden" name="ChangeInfo[category__]">
        <input type="hidden" name="ChangeInfo[imagesId__]">

        <h4>Item Info</h4>

        <div class="row">
            <div class="col-md-6">
                <div class="form-group row">
                    <div class="col-lg-2">
                        <label for="status" class="col-form-label">Status</label>
                    </div>

                    <div class="col-lg-6">
                        <label for="statusActive" class="custom-control custom-radio">
                            <input id="statusActive" name="status" type="radio" class="custom-control-input"
                                   value="1" {{ empty(old('status')) ? ($item->status == 1 ? 'checked' : '') : (old('status') == 1 ? 'checked' : '') }}>
                            <span class="custom-control-indicator"></span>
                            <span class="custom-control-description">Active</span>
                        </label>
                        <label for="statusInactive" class="custom-control custom-radio signin_radio4">
                            <input id="statusInactive" name="status" type="radio" class="custom-control-input" value="0"
                                    {{ empty(old('status')) ? ($item->status == 0 ? 'checked' : '') : (old('status') == 0 ? 'checked' : '') }}>
                            <span class="custom-control-indicator"></span>
                            <span class="custom-control-description">Inactive</span>
                        </label>
                    </div>
                </div>

                <div class="form-group row">
                    <div class="col-lg-2">
                        <label for="style_no" class="col-form-label">Style No.*</label>
                    </div>

                    <div class="col-lg-8">
                        <input type="text" id="style_no" class="form-control{{ $errors->has('style_no') ? ' is-invalid' : '' }}"
                               name="style_no" value="{{ empty(old('style_no')) ? ($errors->has('style_no') ? '' : $item->style_no) : old('style_no') }}">
                    </div>
                </div>

                <div class="form-group row">
                    <div class="col-lg-2">
                        <label for="category" class="col-form-label">My Category *</label>
                    </div>

                    <div class="col-lg-8">
                        <select name="category[]" multiple="multiple" id="category" style="width: 100%" onchange="editStatusUp('category');">
                            @foreach($categories as $category)
                                <option value="{{ $category->id }}"
                                        {{ empty(old('category')) ? ($errors->has('category') ? '' : (in_array($category->id, $item->categories()->pluck('id')->toArray()) ? 'selected' : '')) :
                                            (in_array($category->id, old('category')) ? 'selected' : '') }}>{{ $category->name }}</option>

                                @if (sizeof($category->subCategories) > 0)
                                    @foreach($category->subCategories as $sub)
                                        <option value="{{ $sub->id }}"
                                                {{ empty(old('category')) ? ($errors->has('category') ? '' : (in_array($sub->id, $item->categories()->pluck('id')->toArray()) ? 'selected' : '')) :
                                                    (in_array($sub->id, old('category')) ? 'selected' : '') }}>- {{ $sub->name }}</option>
                                    @endforeach
                                @endif
                            @endforeach
                        </select>

                        @if ($errors->has('category'))
                            <span class="">
                                <strong>{{ $errors->first('category') }}</strong>
                            </span>
                        @endif
                    </div>
                </div>

                <div class="form-group row">
                    <div class="col-lg-2">
                        <label for="price" class="col-form-label">Price *</label>
                    </div>

                    <div class="col-lg-3">
                        <input type="text" id="price" class="form-control{{ $errors->has('price') ? ' is-invalid' : '' }}"
                               placeholder="$" name="price" value="{{ empty(old('price')) ? ($errors->has('price') ? '' : $item->getOriginal('price')) : old('price') }}">
                    </div>

                    <div class="col-lg-2">
                        <label for="orig_price" class="col-form-label">Orig. Price</label>
                    </div>

                    <div class="col-lg-3">
                        <input type="text" id="orig_price" class="form-control{{ $errors->has('orig_price') ? ' is-invalid' : '' }}"
                               placeholder="$" name="orig_price" value="{{ empty(old('orig_price')) ? ($errors->has('orig_price') ? '' : $item->orig_price) : old('orig_price') }}">
                    </div>
                </div>

                <div class="form-group row">
                    <div class="col-lg-2">
                        <label for="size" class="col-form-label">Size *</label>
                    </div>

                    <div class="col-lg-4">
                        <select class="form-control{{ $errors->has('size') ? ' is-invalid' : '' }}" name="size" id="size">
                            <option value="">Select Size</option>

                            @foreach($packs as $pack)
                                <option value="{{ $pack->id }}" data-index="{{ $loop->index }}"
                                        {{ empty(old('size')) ? ($errors->has('size') ? '' : ($item->pack_id == $pack->id ? 'selected' : '')) :
                                            (old('size') == $pack->id ? 'selected' : '') }}>{{ $pack->name }}</option>
                            @endforeach
                        </select>
                    </div>

                    <div class="col-lg-4">
                        <span id="pack_description"></span>
                    </div>
                </div>

                <div class="form-group row">
                    <div class="col-lg-2">
                        <label for="sorting" class="col-form-label">Sorting #</label>
                    </div>

                    <div class="col-lg-8">
                        <input type="text" id="sorting" class="form-control{{ $errors->has('sorting') ? ' is-invalid' : '' }}"
                               name="sorting" value="{{ empty(old('sorting')) ? ($errors->has('sorting') ? '' : $item->sorting) : old('sorting') }}">
                    </div>
                </div>

                <div class="form-group row">
                    <div class="col-lg-2">
                        <label for="description" class="col-form-label">Description</label>
                    </div>

                    <div class="col-lg-8">
                        <textarea class="form-control{{ $errors->has('description') ? ' is-invalid' : '' }}"
                                  name="description" rows="4">{{ empty(old('description')) ? ($errors->has('description') ? '' : $item->description) : old('description') }}</textarea>
                    </div>
                </div>
            </div>

            <div class="col-md-6">
                <div class="form-group row">
                    <div class="col-lg-2">
                        <label class="col-form-label">Available On</label>
                    </div>

                    <div class="col-lg-4">
                        <input type="text" id="available_on" class="form-control{{ $errors->has('available_on') ? ' is-invalid' : '' }}"
                               name="available_on"
                               value="{{ empty(old('description')) ? ($errors->has('description') ? '' : ($item->available_on != null) ? date('m/d/Y', strtotime($item->available_on)) : '') : old('description') }}">
                    </div>

                    <div class="col-lg-4">
                        <select class="form-control" name="availability" id="availability">
                            <option value="{{ Availability::$IN_STOCK }}"
                                    {{ empty(old('availability')) ? ($errors->has('availability') ? '' : ($item->availability == Availability::$IN_STOCK ? 'selected' : '')) :
                                            (old('availability') == Availability::$IN_STOCK ? 'selected' : '') }}>In Stock</option>
                            <option value="{{ Availability::$ARRIVES_SOON }}"
                                    {{ empty(old('availability')) ? ($errors->has('availability') ? '' : ($item->availability == Availability::$ARRIVES_SOON ? 'selected' : '')) :
                                            (old('availability') == Availability::$ARRIVES_SOON ? 'selected' : '') }}>Arrives Soon / Back Order</option>
                        </select>
                    </div>
                </div>

                <div class="form-group row{{ $errors->has('item_name') ? ' has-danger' : '' }}">
                    <div class="col-lg-2">
                        <label for="item_name" class="col-form-label">Item Name</label>
                    </div>

                    <div class="col-lg-8">
                        <input type="text" id="item_name" class="form-control{{ $errors->has('item_name') ? ' is-invalid' : '' }}"
                               name="item_name" value="{{ empty(old('item_name')) ? ($errors->has('item_name') ? '' : $item->name) : old('item_name') }}">
                    </div>
                </div>

                <div class="form-group row">
                    <div class="col-lg-2">
                        <label for="d_parent_category" class="col-form-label">SP Category*</label>
                    </div>

                    <div class="col-lg-3">
                        <select class="form-control{{ $errors->has('d_parent_category') ? ' is-invalid' : '' }}" name="d_parent_category" id="d_parent_category">
                            <option value="">Select Category</option>
                            @foreach($defaultCategories as $cat)
                                <option value="{{ $cat['id'] }}" data-index="{{ $loop->index }}"
                                        {{ empty(old('d_parent_category')) ? ($errors->has('d_parent_category') ? '' : ($item->default_parent_category == $cat['id'] ? 'selected' : '')) :
                                                    (old('d_parent_category') == $cat['id'] ? 'selected' : '') }}>{{ $cat['name'] }}</option>
                            @endforeach
                        </select>
                    </div>

                    <div class="col-lg-3">
                        <select class="form-control{{ $errors->has('d_second_parent_category') ? ' is-invalid' : '' }}" name="d_second_parent_category" id="d_second_parent_category">
                            <option value="">Sub Category</option>
                        </select>
                    </div>

                    <div class="col-lg-2">
                        <select class="form-control{{ $errors->has('d_third_parent_category') ? ' is-invalid' : '' }}" name="d_third_parent_category" id="d_third_parent_category">
                            <option value="">Sub Category</option>
                        </select>
                    </div>
                </div>

                <div class="form-group row">
                    <div class="col-lg-2">

                    </div>

                    <div class="col-lg-8">
                        <label class="custom-control custom-checkbox">
                            <input type="checkbox" class="custom-control-input" id="exclusive" value="1" name="exclusive" {{ $item->exclusive == 1 ? 'checked' : '' }}>
                            <span class="custom-control-indicator"></span>
                            <span class="custom-control-description">Exclusive</span>
                        </label>
                    </div>
                </div>

                <div class="form-group row">
                    <div class="col-lg-2">
                        <label class="col-form-label">Pack</label>
                    </div>

                    <div class="col-lg-8">
                        <label class="col-form-label" id="pack_details">Pack Details</label>
                    </div>
                </div>

                <div class="form-group row">
                    <div class="col-lg-2">
                        <label class="col-form-label">Min. Qty</label>
                    </div>

                    <div class="col-lg-2">
                        <input type="text" id="min_qty" class="form-control{{ $errors->has('min_qty') ? ' is-invalid' : '' }}"
                               name="min_qty" value="{{ empty(old('min_qty')) ? ($errors->has('min_qty') ? '' : $item->min_qty) : old('min_qty') }}">
                    </div>

                    <div class="col-lg-4 text-right">
                        <label class="custom-control custom-checkbox">
                            <input type="checkbox" class="custom-control-input" id="even_color" value="1" name="even_color" {{ $item->even_color == 1 ? 'checked' : '' }}>
                            <span class="custom-control-indicator"></span>
                            <span class="custom-control-description">Even Color</span>
                        </label>
                    </div>
                </div>

                <div class="form-group row">
                    <div class="col-lg-2">
                        <label for="fabric" class="col-form-label">Fabric</label>
                    </div>

                    <div class="col-lg-8">
                        <input type="text" class="form-control" name="fabric" value="{{ empty(old('fabric')) ? ($errors->has('fabric') ? '' : $item->fabric) : old('fabric') }}">
                    </div>
                </div>

                <div class="form-group row">
                    <div class="col-lg-2">
                        <label class="col-form-label">Details</label>
                    </div>

                    <div class="col-lg-4">
                        <select class="form-control" name="made_n" id="made_n">
                            <option value="">Select Made In</option>

                            @foreach($madeInCountries as $country)
                                <option value="{{ $country->id }}"
                                        {{ empty(old('made_n')) ? ($errors->has('made_n') ? '' : ($item->made_in_id == $country->id ? 'selected' : '')) :
                                                    (old('made_n') == $country->id ? 'selected' : '') }}>{{ $country->name }}</option>
                            @endforeach
                        </select>
                    </div>

                    <div class="col-lg-4">
                        <select class="form-control" name="labeled" id="labeled">
                            <option value="">Select Labeled</option>
                            <option value="labeled" {{ empty(old('labeled')) ? ($errors->has('labeled') ? '' : ($item->labeled == 'labeled' ? 'selected' : '')) :
                                                    (old('labeled') == 'labeled' ? 'selected' : '') }}>Labeled</option>
                            <option value="printed" {{ empty(old('labeled')) ? ($errors->has('labeled') ? '' : ($item->labeled == 'printed' ? 'selected' : '')) :
                                                    (old('labeled') == 'printed' ? 'selected' : '') }}>Printed</option>
                        </select>
                    </div>
                </div>
            </div>
        </div>
        <br>
        <h4>SP Filter & Search Term</h4>
        <br>
        <div class="row">
            <div class="col-md-12">
                <div class="form-group row">
                    <div class="col-lg-1">
                        <label for="keywords" class="col-form-label">Keywords</label>
                    </div>

                    <div class="col-lg-7">
                        <input type="text" id="keywords" class="form-control{{ $errors->has('keywords') ? ' is-invalid' : '' }}"
                               placeholder="Max 200 characters allowed. Separate using a comma." name="keywords"
                               value="{{ empty(old('keywords')) ? ($errors->has('keywords') ? '' : $item->keywords) : old('keywords') }}">
                    </div>
                </div>

                <div class="form-group row">
                    <div class="col-lg-1">
                        <label class="col-form-label">Details</label>
                    </div>

                    <div class="col-lg-2">
                        <select class="form-control" name="body_size" id="body_size">
                            <option value="">Select Body Size</option>
                        </select>
                    </div>

                    <div class="col-lg-2">
                        <select class="form-control" name="pattern" id="pattern">
                            <option value="">Select Pattern</option>
                        </select>
                    </div>

                    <div class="col-lg-2">
                        <select class="form-control" name="length" id="length">
                            <option value="">Select Length</option>
                        </select>
                    </div>

                    <div class="col-lg-2">
                        <select class="form-control" name="style" id="style">
                            <option value="">Select Style</option>
                        </select>
                    </div>

                    <div class="col-lg-2">
                        <select class="form-control" name="master_fabric" id="master_fabric">
                            <option value="">Select Fabric</option>

                            @foreach($masterFabrics as $fabric)
                                <option value="{{ $fabric->id }}" {{ empty(old('master_fabric')) ? ($errors->has('master_fabric') ? '' : ($item->master_fabric_id == $fabric->id ? 'selected' : '')) :
                                                    (old('master_fabric') == $fabric->id ? 'selected' : '') }}>{{ $fabric->name }}</option>
                            @endforeach
                        </select>
                    </div>
                </div>
            </div>
        </div>
        <br>
        <h4>In-house section</h4>
        <br>
        <div class="row">
            <div class="col-md-6">
                <div class="form-group row">
                    <div class="col-lg-2">
                        <label for="memo" class="col-form-label">Memo</label>
                    </div>

                    <div class="col-lg-8">
                        <input type="text" id="memo" class="form-control{{ $errors->has('memo') ? ' is-invalid' : '' }}"
                               placeholder="Internal use only" name="memo" value="{{ empty(old('memo')) ? ($errors->has('memo') ? '' : $item->memo) : old('memo') }}">
                    </div>
                </div>
            </div>

            <div class="col-md-6">
                <div class="form-group row">
                    <div class="col-lg-2">
                        <label for="vendor_style_no" class="col-form-label">Vendor Style No.</label>
                    </div>

                    <div class="col-lg-8">
                        <input type="text" id="vendor_style_no" class="form-control{{ $errors->has('vendor_style_no') ? ' is-invalid' : '' }}"
                               placeholder="Enter Vendor Style No." name="vendor_style_no"
                               value="{{ empty(old('vendor_style_no')) ? ($errors->has('vendor_style_no') ? '' : $item->vendor_style_no) : old('vendor_style_no') }}">
                    </div>
                </div>
            </div>
        </div>

        <h4>Colors</h4>
        <div class="row">
            <div class="col-md-12">
                <div class="form-group row">
                    <div class="col-lg-1">
                        <label for="keywords" class="col-form-label">Colors</label>
                    </div>

                    <div class="col-lg-2">
                        <input class="form-control" type="text" id="color_search">

                        @if ($errors->has('colors'))
                            <span class="text-danger">Color(s) is required.</span>
                        @endif
                    </div>

                    <select class="col-lg-2 form-control d-none" id="select_master_color">
                        <option value="">Select Master Color</option>
                        @foreach($masterColors as $mc)
                            <option value="{{ $mc->id }}">{{ $mc->name }}</option>
                        @endforeach
                    </select>

                    <div class="col-lg-6">
                        <a class="btn btn-primary" role="button" id="btnAddColor">Add Color</a>
                    </div>
                </div>
            </div>
        </div>

        <div class="row">
            <div class="col-md-12">
                <div class="row">
                    <div class="col-lg-1">

                    </div>

                    <div class="col-lg-11">
                        <ul class="colors-ul">
                            <?php
                            $previous_color_ids = [];

                            if (empty(old('colors'))) {
                                foreach($item->colors as $c)
                                    $previous_color_ids[] = $c->id;
                            } else {
                                $previous_color_ids = old('colors');
                            }
                            ?>

                            @foreach($colors as $color)
                                @if (in_array($color->id, $previous_color_ids))
                                    <li>{{ $color->name }}<a class="color-remove">X</a><input type="hidden" name="colors[]" value="{{ $color->id }}"></li>
                                @endif
                            @endforeach
                        </ul>
                    </div>
                </div>
            </div>
        </div>

        <h4>Images</h4>
        <div class="row">
            <div class="col-md-12">
                <button class="btn btn-primary" id="btnUploadImages">Upload Images</button>
                <input type="file" class="d-none" multiple id="inputImages">
            </div>
        </div>

        <br>

        <div class="row">
            <div class="col-md-12">
                <div style="width: 100%;height: auto;border: 3px dotted black;padding: 20px;">
                    <div class="row" id="images" style="padding: 20px;">
                        Drag & Drop Images from your computer
                        <ul id="image-container" class="block__list block__list_tags">
                            @if (old('imagesId') != null && sizeof(old('imagesId')) > 0)
                                @foreach(old('imagesId') as $img)
                                    <li>
                                        <div class="image-item">
                                            <img class="img-thumbnail img" style="margin-bottom: 10px"
                                                 src="{{ asset(old('imagesSrc.'.$loop->index)) }}"><br>
                                            <select class="image-color" name="imageColor[]" onchange="editStatusUp('imagesId')">
                                                <option value="">Color [Default]</option>
                                            </select><br>
                                            <a class="btnRemoveImage">Remove</a>

                                            <input class="inputImageId" type="hidden" name="imagesId[]" value="{{ $img }}">
                                            <input class="inputImageSrc" type="hidden" name="imagesSrc[]" value="{{ old('imagesSrc.'.$loop->index) }}">
                                        </div>
                                    </li>
                                @endforeach
                            @else
                                @if (sizeof($item->images) > 0)
                                    @foreach($item->images as $img)
                                        <li>
                                            <div class="image-item">
                                                <img class="img-thumbnail img" style="margin-bottom: 10px"
                                                     src="{{ asset($img->image_path) }}"><br>
                                                <select class="image-color" name="imageColor[]" onchange="editStatusUp('imagesId')">
                                                    <option value="">Color [Default]</option>
                                                </select><br>
                                                <a class="btnRemoveImage">Remove</a>

                                                <input class="inputImageId" type="hidden" name="imagesId[]" value="{{ $img->id }}">
                                                <input class="inputImageSrc" type="hidden" name="imagesSrc[]" value="{{ $img->image_path }}">
                                            </div>
                                        </li>
                                    @endforeach
                                @endif
                            @endif
                        </ul>
                    </div>
                </div>
            </div>
        </div>

        <br>

        <div class="row">
            <div class="col-md-6">
                {{--<a href="{{ route('vendor_clone_item', ['item' => $item->id]) }}" class="btn btn-primary" style="color: white">Clone</a>--}}
            </div>
            <div class="col-md-6 text-right">
                <button class="btn btn-primary" id="btnSave">Save</button>
            </div>
        </div>
    </form>

    <template id="imageTemplate">
        <li>
            <div class="image-item">
                <img class="img-thumbnail img" style="margin-bottom: 10px"><br>
                <select class="image-color" name="imageColor[]" onchange="editStatusUp('imagesId')">
                    <option value="">Color [Default]</option>
                </select><br>
                <a class="btnRemoveImage">Remove</a>

                <input class="inputImageId" type="hidden" name="imagesId[]">
                <input class="inputImageSrc" type="hidden" name="imagesSrc[]">
            </div>
        </li>
    </template>
@stop

@section('additionalJS')
    <script type="text/javascript" src="{{ asset('plugins/toastr/toastr.min.js') }}"></script>
    <script type="text/javascript" src="{{ asset('plugins/select2/js/select2.min.js') }}"></script>
    <script type="text/javascript" src="{{ asset('plugins/bootstrap-datepicker/js/bootstrap-datepicker.min.js') }}"></script>
    <script type="text/javascript" src="{{ asset('plugins/sortable/js/Sortable.min.js') }}"></script>
    <script type="text/javascript" src="{{ asset('plugins/jquery-ui/jquery-ui.min.js') }}"></script>
    <script type="text/javascript" src="https://momentjs.com/downloads/moment.js"></script>
    <script>
        function editStatusUp(type){
            alert(type);
            if(type === 'colors'){
                $('input[name="ChangeInfo[color__]"]').val(1);
            }
            if(type === 'category'){
                $('input[name="ChangeInfo[category__]"]').val(1);
            }
            if(type === 'imagesId'){
                $('input[name="ChangeInfo[imagesId__]"]').val(1);
            }
        }
        $(function () {
            var defaultCategories = <?php echo json_encode($defaultCategories); ?>;
            var bodySizes = <?php echo json_encode($bodySizes->toArray()); ?>;
            var patterns = <?php echo json_encode($patterns->toArray()); ?>;
            var lengths = <?php echo json_encode($lengths->toArray()); ?>;
            var styles = <?php echo json_encode($styles->toArray()); ?>;
            var packs = <?php echo json_encode($packs->toArray()); ?>;
            var colors = <?php echo json_encode($colors->toArray()); ?>;

            $.ajaxSetup({
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                }
            });

            $('#form').bind('submit', function () {
                $(this).find(':input').prop('disabled', false);
            });

            // Category
            $('#category').select2({
                maximumSelectionLength: 3
            });

            // Color select
            var availableColors = [];

            $.each(colors, function (i, color) {
                availableColors.push(color.name);
            });

            $('#color_search').autocomplete({
                source: function (request, response) {
                    var results = $.map(availableColors, function (tag) {
                        if (tag.toUpperCase().indexOf(request.term.toUpperCase()) === 0) {
                            return tag;
                        }
                    });
                    response(results);
                },
                response: function(event, ui) {
                    if (ui.content.length === 0) {
                        $('#select_master_color').val('');
                        $('#select_master_color').removeClass('d-none');
                    } else {
                        $('#select_master_color').addClass('d-none');
                    }
                }
            });

            $('#color_search').keydown(function (e){
                if(e.keyCode == 13){
                    e.preventDefault();
                    addColor();
                }
            });

            $('#btnAddColor').click(function () {
                if ($('#select_master_color').hasClass('d-none')) {
                    addColor();
                } else {
                    var id = $('#select_master_color').val();
                    var name = $('#color_search').val();
                    var vendor_id = '{{ $item->vendor->id }}';

                    if (id == '')
                        return alert('Select Master Color.');

                    if (name == '')
                        return alert('Enter color name.');

                    $.ajax({
                        method: "POST",
                        url: "{{ route('vendor_item_add_color') }}",
                        data: { id: id, name: name, vendorId: vendor_id }
                    }).done(function( data ) {
                        if (data.success) {
                            availableColors.push(data.color.name);
                            colors.push(data.color);

                            $('#select_master_color').addClass('d-none');
                            $('#color_search').val(data.color.name);
                            addColor();
                        } else {
                            alert(data.message);
                        }
                    });
                }
            });

            function addColor() {
                editStatusUp('colors');
                var text = $('#color_search').val();

                if (text != '') {
                    var color = '';

                    $.each(colors, function (i, c) {
                        if (c.name == text)
                            color = c;
                    });

                    if (color != '') {
                        var found = false;
                        $( "input[name*='colors']" ).each(function () {
                            if ($(this).val() == color.id)
                                found = true;
                        });

                        if (!found) {
                            $('.colors-ul').append('<li>' + color.name + '<a class="color-remove">X</a><input type="hidden" name="colors[]" value="' + color.id + '"></li>');
                            updateImageColors();
                        }
                        $('#color_search').val('');
                    } else {
                        $('#select_master_color').removeClass('d-none');
                    }
                }
            }

            var old_colors = <?php echo json_encode(old('imageColor')); ?>;
            if (old_colors == null)
                old_colors = <?php echo json_encode($imagesColorIds); ?>;

            function updateImageColors() {
                var ids = [];

                $( "input[name*='colors']" ).each(function () {
                    ids.push($(this).val());
                });

                $('.image-color').each(function (i) {
                    var selected = $(this).val();

                    $(this).html('<option value="">Color [Default]</option>');
                    $this = $(this);

                    $.each(ids, function (index, id) {
                        var color = colors.filter(function( obj ) {
                            return obj.id == id;
                        });
                        color = color[0];

                        if ((old_colors.length > i && old_colors[i] == color.id) || color.id == selected)
                            $this.append('<option value="'+color.id+'" selected>'+color.name+'</option>');
                        else
                            $this.append('<option value="'+color.id+'">'+color.name+'</option>');
                    });
                });
            }
            updateImageColors();

            $(document).on('click', '.color-remove', function () {
                editStatusUp('colors');
                $(this).closest('li').remove();
            });

            // Available on
            $('#available_on').datepicker({
                autoclose: true,
                format: 'mm/dd/yyyy',
                orientation: "bottom left"
            });

            var message = '{{ session('message') }}';

            if (message != '')
                toastr.success(message);

            // Images
            var el = document.getElementById('image-container');
            Sortable.create(el, {
                group: "words",
                animation: 150,
            });

            $('#images').on({
                'dragover dragenter': function(e) {
                    e.preventDefault();
                    e.stopPropagation();
                },
                'drop': function(e) {
                    var dataTransfer =  e.originalEvent.dataTransfer;
                    if( dataTransfer && dataTransfer.files.length) {
                        e.preventDefault();
                        e.stopPropagation();
                        $.each( dataTransfer.files, function(i, file) {
                            if (file.size > 614400) {
                                alert('Max allowed image size is 600Kb per image.')
                            } else if (file.type != 'image/jpeg' && file.type != 'image/png') {
                                alert('Only jpg and png file allowed.');
                            } else if ($(".image-container").length > 2) {
                                alert('Maximum 20 photos allows');
                            } else {
                                var xmlHttpRequest = new XMLHttpRequest();
                                xmlHttpRequest.open("POST", '{{ route('vendor_item_upload_image') }}', true);
                                var formData = new FormData();
                                formData.append("file", file);
                                xmlHttpRequest.send(formData);

                                xmlHttpRequest.onreadystatechange = function() {
                                    if (xmlHttpRequest.readyState == XMLHttpRequest.DONE) {
                                        var response = JSON.parse(xmlHttpRequest.responseText);

                                        if (response.success) {
                                            var html = $('#imageTemplate').html();
                                            var item = $(html);
                                            item.find('.img').attr('src', response.data.fullPath);
                                            item.find('.inputImageId').val(response.data.id);
                                            item.find('.inputImageSrc').val(response.data.image_path);

                                            $('#image-container').append(item);
                                            updateImageColors();
                                        }
                                    }
                                }
                            }
                        });
                    }
                }
            });

            $('body').on('click', '.btnRemoveImage', function () {
                editStatusUp('imagesId');
                var index = $('.btnRemoveImage').index($(this));
                old_colors.splice(index, 1);
                $(this).closest('li').remove();
            });

            var d_parent_index;
            var d_second_id = '{{ empty(old('d_second_parent_category')) ? ($errors->has('d_second_parent_category') ? '' : $item->default_second_category) : old('d_second_parent_category') }}';
            var d_third_id = '{{ empty(old('d_third_parent_category')) ? ($errors->has('d_third_parent_category') ? '' : $item->default_third_category) : old('d_third_parent_category') }}';
            var body_size_id = '{{ empty(old('body_size')) ? ($errors->has('body_size') ? '' : $item->body_size_id) : old('body_size') }}';
            var pattern_id = '{{ empty(old('pattern')) ? ($errors->has('pattern') ? '' : $item->pattern_id) : old('pattern') }}';
            var length_id = '{{ empty(old('length')) ? ($errors->has('length') ? '' : $item->length_id) : old('length') }}';
            var style_id = '{{ empty(old('style')) ? ($errors->has('style') ? '' : $item->style_id) : old('style') }}';

            $('#d_parent_category').change(function () {
                $('#d_second_parent_category').html('<option value="">Sub Category</option>');
                $('#d_third_parent_category').html('<option value="">Sub Category</option>');
                var parent_id = $(this).val();

                if ($(this).val() != '') {
                    var index = $(this).find(':selected').data('index');
                    d_parent_index = index;

                    var childrens = defaultCategories[index].subCategories;

                    $.each(childrens, function (index, value) {
                        if (value.id == d_second_id)
                            $('#d_second_parent_category').append('<option data-index="' + index + '" value="' + value.id + '" selected>' + value.name + '</option>');
                        else
                            $('#d_second_parent_category').append('<option data-index="' + index + '" value="' + value.id + '">' + value.name + '</option>');
                    });
                }

                $('#d_second_parent_category').trigger('change');

                // Body Size
                $('#body_size').html('<option value="">Select Body Size</option>');

                $.each(bodySizes, function (index, value) {
                    if (value.parent_category_id == parent_id) {
                        if (value.id == body_size_id)
                            $('#body_size').append('<option value="'+value.id+'" selected>'+value.name+'</option>');
                        else
                            $('#body_size').append('<option value="'+value.id+'">'+value.name+'</option>');
                    }
                });

                // Pattern
                $('#pattern').html('<option value="">Select Pattern</option>');

                $.each(patterns, function (index, value) {
                    if (value.parent_category_id == parent_id) {
                        if (value.id == pattern_id)
                            $('#pattern').append('<option value="' + value.id + '" selected>' + value.name + '</option>');
                        else
                            $('#pattern').append('<option value="' + value.id + '">' + value.name + '</option>');
                    }
                });

                // Style
                $('#style').html('<option value="">Select Style</option>');

                $.each(styles, function (index, value) {
                    if (value.parent_category_id == parent_id) {
                        if (value.id == style_id)
                            $('#style').append('<option value="' + value.id + '" selected>' + value.name + '</option>');
                        else
                            $('#style').append('<option value="' + value.id + '">' + value.name + '</option>');
                    }
                });
            });

            $('#d_parent_category').trigger('change');

            $('#d_second_parent_category').change(function () {
                $('#d_third_parent_category').html('<option value="">Sub Category</option>');

                if ($(this).val() != '') {
                    var index = $(this).find(':selected').attr('data-index');

                    var childrens = defaultCategories[d_parent_index].subCategories[index].subCategories;

                    $.each(childrens, function (index, value) {
                        if (value.id == d_third_id)
                            $('#d_third_parent_category').append('<option data-index="' + index + '" value="' + value.id + '" selected>' + value.name + '</option>');
                        else
                            $('#d_third_parent_category').append('<option data-index="' + index + '" value="' + value.id + '">' + value.name + '</option>');
                    });
                }

                var id = $(this).val();

                // Length
                $('#length').html('<option value="">Select Length</option>');

                $.each(lengths, function (index, value) {
                    if (value.sub_category_id == id) {
                        if (value.id == length_id)
                            $('#length').append('<option value="'+value.id+'" selected>'+value.name+'</option>');
                        else
                            $('#length').append('<option value="'+value.id+'">'+value.name+'</option>');
                    }
                });
            });

            // Size
            $('#size').change(function () {
                var index = $(this).find(':selected').data('index');

                if (typeof index !== "undefined") {
                    var pack = packs[index];
                    var packDetails = pack.pack1;

                    $('#pack_description').html(pack.description);

                    if (pack.pack2 != null)
                        packDetails += '-' + pack.pack2;

                    if (pack.pack3 != null)
                        packDetails += '-' + pack.pack3;

                    if (pack.pack4 != null)
                        packDetails += '-' + pack.pack4;

                    if (pack.pack5 != null)
                        packDetails += '-' + pack.pack5;

                    if (pack.pack6 != null)
                        packDetails += '-' + pack.pack6;

                    if (pack.pack7 != null)
                        packDetails += '-' + pack.pack7;

                    if (pack.pack8 != null)
                        packDetails += '-' + pack.pack8;

                    if (pack.pack9 != null)
                        packDetails += '-' + pack.pack9;

                    if (pack.pack10 != null)
                        packDetails += '-' + pack.pack10;

                    $('#pack_details').html(packDetails);
                } else {
                    $('#pack_details').html('Pack Details');
                    $('#pack_description').html('');
                }
            });

            $('#size').trigger('change');
            $('#d_second_parent_category').trigger('change');

            // Upload images button
            $('#btnUploadImages').click(function (e) {
                e.preventDefault();

                $('#inputImages').click();
            });

            $('#inputImages').change(function (e) {
                //console.log(e.target.files);
                $.each(e.target.files, function (index, file) {
                    if (file.size > 614400) {
                        alert('Max allowed image size is 600Kb per image.')
                    } else if (file.type != 'image/jpeg' && file.type != 'image/png') {
                        alert('Only jpg and png file allowed.');
                    } else if ($(".image-container").length > 2) {
                        alert('Maximum 20 photos allows');
                    } else {
                        editStatusUp('imagesId');
                        var xmlHttpRequest = new XMLHttpRequest();
                        xmlHttpRequest.open("POST", '{{ route('vendor_item_upload_image') }}', true);
                        var formData = new FormData();
                        formData.append("file", file);
                        xmlHttpRequest.send(formData);

                        xmlHttpRequest.onreadystatechange = function() {
                            if (xmlHttpRequest.readyState == XMLHttpRequest.DONE) {
                                var response = JSON.parse(xmlHttpRequest.responseText);

                                if (response.success) {
                                    /*if ($(".image-container").length == 0)
                                        $('#image-container').html('');*/

                                    var html = $('#imageTemplate').html();
                                    var item = $(html);
                                    item.find('.img').attr('src', response.data.fullPath);
                                    item.find('.inputImageId').val(response.data.id);
                                    item.find('.inputImageSrc').val(response.data.image_path);

                                    $('#image-container').append(item);
                                    updateImageColors();
                                }
                            }
                        }
                    }
                });

                $(this).val('');
            });

            $('#btnSave').click(function (e) {
                e.preventDefault();
                var style_no = $('#style_no').val();
                var current_item_id = '{{ $item->id }}';

                $.ajax({
                    method: "POST",
                    url: "{{ route('vendor_check_style_no') }}",
                    data: { style_no: style_no, except: current_item_id }
                }).done(function( data ) {
                    if (data.success)
                        $('#form').submit();
                    else
                        alert(data.message);
                });
            });

            // Available
            $('#available_on').change(function () {
                var val = $(this).val();
                $('#availability').prop('disabled', false);

                if (val != '') {
                    if (moment(val, 'MM/DD/YYYY').isBefore(moment())) {
                        //console.log('before');
                    }else {
                        console.log('after');
                        $('#availability').val('{{ Availability::$ARRIVES_SOON }}');
                        $('#availability').prop('disabled', true);
                    }
                }
            });

            $('#available_on').trigger('change');

            window.addEventListener("dragover",function(e){
                e = e || event;
                e.preventDefault();
            },false);
            window.addEventListener("drop",function(e){
                e = e || event;
                e.preventDefault();
            },false);

            $('.btnBack').click(function () {
                localStorage['change_position'] = 1;
            });
        });
    </script>
@stop