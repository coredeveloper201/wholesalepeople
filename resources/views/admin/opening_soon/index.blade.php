@extends('admin.layouts.admin')

@section('additionalCSS')
    <link href="{{ asset('plugins/toastr/toastr.min.css') }}" rel="stylesheet">
@stop

@section('content')
    <div class="row {{ ($errors && sizeof($errors) > 0) ? 'd-none' : '' }}" id="addBtnRow">
        <div class="col-md-12">
            <button class="btn btn-primary" id="btnAddNew">Add New</button>
        </div>
    </div>

    <div class="row {{ ($errors && sizeof($errors) > 0) ? '' : 'd-none' }}" id="addEditRow">
        <div class="col-md-12" style="border: 1px solid black">
            <h3><span id="addEditTitle">{{ old('inputAdd') == '0' ? 'Edit' : 'Add' }}</span></h3>

            <form class="form-horizontal" enctype="multipart/form-data" id="form"
                  method="post" action="{{ (old('inputAdd') == '1') ? route('admin_opening_soon_add_post') : route('admin_opening_soon_edit_post') }}">
                @csrf

                <input type="hidden" name="inputAdd" id="inputAdd" value="{{ old('inputAdd') }}">
                <input type="hidden" name="osId" id="osId" value="{{ old('osId') }}">

                <div class="form-group row{{ $errors->has('name') ? ' has-danger' : '' }}">
                    <div class="col-lg-2">
                        <label for="name" class="col-form-label">Name *</label>
                    </div>

                    <div class="col-lg-5">
                        <input type="text" id="name" class="form-control{{ $errors->has('name') ? ' is-invalid' : '' }}"
                               placeholder="Name" name="name" value="{{ old('name') }}">
                    </div>
                </div>

                <div class="form-group row{{ $errors->has('sort') ? ' has-danger' : '' }}">
                    <div class="col-lg-2">
                        <label for="sort" class="col-form-label">Sort</label>
                    </div>

                    <div class="col-lg-5">
                        <input type="text" id="sort" class="form-control{{ $errors->has('sort') ? ' is-invalid' : '' }}"
                               placeholder="Sort" name="sort" value="{{ old('sort') }}">
                    </div>
                </div>

                <div class="form-group row{{ $errors->has('description') ? ' has-danger' : '' }}">
                    <div class="col-lg-2">
                        <label for="description" class="col-form-label">description</label>
                    </div>

                    <div class="col-lg-5">
                        <textarea id="description" class="form-control{{ $errors->has('description') ? ' is-invalid' : '' }}"
                                  placeholder="Description" name="description" rows="10">{{ old('description') }}</textarea>
                    </div>
                </div>

                <div class="form-group row{{ $errors->has('photo') ? ' has-danger' : '' }}">
                    <div class="col-lg-2">
                        <label for="code" class="col-form-label">Image</label>
                    </div>

                    <div class="col-lg-5">
                        <input class="form-control{{ $errors->has('photo') ? ' is-invalid' : '' }}"
                               type="file" id="photo" name="photo" accept="image/*">
                    </div>
                </div>

                <div class="form-group row">
                    <div class="col-lg-12 text-right">
                        <button class="btn btn-default" id="btnCancel">Cancel</button>
                        <input type="submit" id="btnSubmit" class="btn btn-primary" value="{{ old('inputAdd') == '0' ? 'Update' : 'Add' }}">
                    </div>
                </div>
            </form>
        </div>
    </div>

    <br>

    <div class="row">
        <div class="col-md-12">
            <table class="table table-striped">
                <thead>
                <tr>
                    <th></th>
                    <th>Name</th>
                    <th>Sort</th>
                    <th>Description</th>
                    <th>Action</th>
                </tr>
                </thead>
                    @foreach($os as $item)
                        <tr>
                            <td>
                                @if ($item->image_path != null)
                                    <img src="{{ asset($item->image_path) }}" width="200px" height="150px">
                                @endif
                            </td>
                            <td>{{ $item->name }}</td>
                            <td>{{ $item->sort }}</td>
                            <td>{{ $item->description }}</td>
                            <td>
                                <a class="btnEdit" data-id="{{ $item->id }}" data-index="{{ $loop->index }}" role="button" style="color: blue">Edit</a> |
                                <a class="btnDelete" data-id="{{ $item->id }}" role="button" style="color: red">Delete</a>
                            </td>
                        </tr>
                    @endforeach
                <tbody>
                </tbody>
            </table>

            <div class="pagination">
                {{ $os->links() }}
            </div>
        </div>
    </div>

    <div class="modal fade" id="deleteModal" role="dialog" aria-labelledby="deleteModal">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-header bg-danger">
                    <h4 class="modal-title text-white" id="deleteModal">Delete</h4>
                </div>
                <div class="modal-body">
                    <p>
                        Are you sure want to delete?
                    </p>
                </div>
                <div class="modal-footer">
                    <button class="btn  btn-default" data-dismiss="modal">Close</button>
                    <button class="btn  btn-danger" id="modalBtnDelete">Delete</button>
                </div>
            </div>
        </div>
        <!--- end modals-->
    </div>
@stop

@section('additionalJS')
    <script type="text/javascript" src="{{ asset('plugins/toastr/toastr.min.js') }}"></script>
    <script>
        $(function () {
            $.ajaxSetup({
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                }
            });

            var items = <?php echo json_encode($os->toArray()); ?>;
            items = items.data;
            var selectedId;
            var message = '{{ session('message') }}';

            if (message != '')
                toastr.success(message);

            $('#btnAddNew').click(function () {
                $('#addEditRow').removeClass('d-none');
                $('#addBtnRow').addClass('d-none');
                $('#addEditTitle').html('Add');
                $('#btnSubmit').val('Add');
                $('#inputAdd').val('1');
                $('#form').attr('action', '{{ route('admin_opening_soon_add_post') }}');
            });

            $('#btnCancel').click(function (e) {
                e.preventDefault();

                $('#addEditRow').addClass('d-none');
                $('#addBtnRow').removeClass('d-none');

                // Clear form
                $('#statusActive').prop('checked', true);
                $('#name').val('');
                $('#master_color').val('');

                $('input').removeClass('is-invalid');
                $('.form-group').removeClass('has-danger');
            });

            $('.btnEdit').click(function () {
                var id = $(this).data('id');
                var index = $(this).data('index');

                $('#addEditRow').removeClass('d-none');
                $('#addBtnRow').addClass('d-none');
                $('#addEditTitle').html('Edit');
                $('#btnSubmit').val('Update');
                $('#inputAdd').val('0');
                $('#form').attr('action', '{{ route('admin_opening_soon_edit_post') }}');
                $('#osId').val(id);

                var item = items[index];

                $('#name').val(item.name);
                $('#description').val(item.description);
                $('#sort').val(item.sort);
            });

            $('.btnDelete').click(function () {
                $('#deleteModal').modal('show');
                selectedId = $(this).data('id');
            });

            $('#modalBtnDelete').click(function () {
                $.ajax({
                    method: "POST",
                    url: "{{ route('admin_opening_soon_delete') }}",
                    data: { id: selectedId }
                }).done(function( msg ) {
                    location.reload();
                });
            });
        })
    </script>
@stop