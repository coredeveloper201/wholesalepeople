@extends('admin.layouts.admin')

@section('additionalCSS')
    <link href="{{ asset('plugins/toastr/toastr.min.css') }}" rel="stylesheet">
@stop

@section('content')
    <div class="row">
        <div class="col-md-12">
            <button class="btn btn-primary" id="btnAddNew">Add New Body Size</button>
        </div>
    </div>

    <br>

    <div class="row">
        <div class="col-md-12">
            <table class="table table-striped">
                <thead>
                <tr>
                    <th>#</th>
                    <th>Name</th>
                    <th>Category</th>
                    <th>Action</th>
                </tr>
                </thead>

                <tbody>
                @foreach($sizes as $size)
                    <tr>
                        <td>{{ $loop->iteration }}</td>
                        <td>{{ $size->name }}</td>
                        <td>{{ $size->category->name }}</td>

                        <td>
                            <a class="btnEdit" data-id="{{ $size->id }}" data-index="{{ $loop->index }}" role="button" style="color: blue">Edit</a> |
                            <a class="btnDelete" data-id="{{ $size->id }}" role="button" style="color: red">Delete</a>
                        </td>
                    </tr>
                @endforeach
                </tbody>
            </table>
        </div>
    </div>

    <div class="modal fade" id="addEditModal" role="dialog" aria-labelledby="addEditModal">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <form class="form-horizontal" id="form" method="POST">
                    @csrf
                    <input type="hidden" name="id" value="" id="modalInputId">

                    <div class="modal-header bg-primary">
                        <h4 class="modal-title text-white" id="addEditModal">Body Size</h4>
                    </div>

                    <div class="modal-body">
                        <fieldset>
                            <div class="form-group row">
                                <div class="col-lg-5 text-lg-right">
                                    <label for="modalName" class="col-form-label">Name</label>
                                </div>
                                <div class="col-lg-6">
                                    <input type="text" id="modalName" class="form-control" placeholder="" name="name">
                                </div>
                            </div>

                            <div class="form-group row">
                                <div class="col-lg-5 text-lg-right">
                                    <label class="col-form-label">Category</label>
                                </div>
                                <div class="col-lg-6">
                                    <select class="form-control" name="d_parent_category" id="d_parent_category">
                                        <option value="">Select Category</option>

                                        @foreach($defaultCategories as $item)
                                            <option value="{{ $item['id'] }}" data-index="{{ $loop->index }}" {{ old('d_parent_category') == $item['id'] ? 'selected' : '' }}>{{ $item['name'] }}</option>
                                        @endforeach
                                    </select>
                                </div>
                            </div>
                        </fieldset>
                    </div>

                    <div class="modal-footer">
                        <button class="btn  btn-default" data-dismiss="modal">Close</button>
                        <input class="btn  btn-primary" type="submit" id="modalBtnAdd" value="Add">
                        <input class="btn  btn-primary" type="submit" id="modalBtnUpdate" value="Update">
                    </div>
                </form>
            </div>
        </div>
    </div>

    <div class="modal fade" id="deleteModal" role="dialog" aria-labelledby="deleteModal">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-header bg-danger">
                    <h4 class="modal-title text-white" id="deleteModal">Delete</h4>
                </div>
                <div class="modal-body">
                    <p>
                        Are you sure want to delete?
                    </p>
                </div>
                <div class="modal-footer">
                    <button class="btn  btn-default" data-dismiss="modal">Close</button>
                    <button class="btn  btn-danger" id="modalBtnDelete">Delete</button>
                </div>
            </div>
        </div>
        <!--- end modals-->
    </div>
@stop

@section('additionalJS')
    <script type="text/javascript" src="{{ asset('plugins/toastr/toastr.min.js') }}"></script>
    <script>
        $(function () {
            $.ajaxSetup({
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                }
            });

            var sizes = <?php echo json_encode($sizes); ?>;
            var defaultCategories = <?php echo json_encode($defaultCategories); ?>;

            var selectedID;
            var message = '{{ session('message') }}';

            if (message != '')
                toastr.success(message);

            $('#btnAddNew').click(function () {
                $('#modalName').removeClass('is-invalid');
                $('#modalName').val('');
                $('#d_parent_category').val('');
                $('#modalBtnAdd').show();
                $('#modalBtnUpdate').hide();
                $('#form').attr('action', '{{ route('admin_body_size_add_post') }}');

                $('#addEditModal').modal('show');
            });

            $('#modalBtnAdd, #modalBtnUpdate').click(function (e) {
                e.preventDefault();

                $('#modalName').removeClass('is-invalid');
                $('#d_parent_category').removeClass('is-invalid');

                var error = false;
                var name = $('#modalName').val();
                var category = $('#d_parent_category').val();

                if (name == '') {
                    $('#modalName').addClass('is-invalid');
                    error = true;
                }

                if (category == '') {
                    $('#d_parent_category').addClass('is-invalid');
                    error = true;
                }

                if (!error)
                    $('#form').submit();
            });

            $('.btnEdit').click(function () {
                selectedID = $(this).data('id');
                var index = $(this).data('index');
                var size = sizes[index];

                $('#modalName').removeClass('is-invalid');
                $('#d_parent_category').removeClass('is-invalid');

                $('#modalName').val(size.name);
                $('#d_parent_category').val(size.parent_category_id);

                $('#modalBtnAdd').hide();
                $('#modalBtnUpdate').show();

                $('#form').attr('action', '{{ route('admin_body_size_edit_post') }}');
                $('#modalInputId').val(selectedID);
                $('#addEditModal').modal('show');
            });

            $('.btnDelete').click(function () {
                selectedID = $(this).data('id');
                $('#deleteModal').modal('show');
            });

            $('#modalBtnDelete').click(function () {
                $.ajax({
                    method: "POST",
                    url: "{{ route('admin_body_size_delete') }}",
                    data: { id: selectedID }
                }).done(function( msg ) {
                    location.reload();
                });
            });
        });
    </script>
@stop