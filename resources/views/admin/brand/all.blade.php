@extends('admin.layouts.admin')

@section('additionalCSS')
    <link href="{{ asset('plugins/toastr/toastr.min.css') }}" rel="stylesheet">
@stop

@section('content')
    <div class="row">
        <div class="col-md-12">
            <table class="table">
                <thead>
                    <tr>
                        <th>Company Name</th>
                        <th>Active</th>
                        <th>Verified</th>
                        <th>Live</th>
                        <th>Show In Main Slider</th>
                        <th>Show In Mobile Main Slider</th>
                        <th>Feature Vendor</th>
                        <th>Created At</th>
                        <th>Action</th>
                    </tr>
                </thead>

                <tbody>
                    @foreach($vendors as $vendor)
                        <tr>
                            <td>{{ $vendor->company_name }}</td>
                            <td>
                                <label class="custom-control custom-checkbox">
                                    <input type="checkbox" data-id="{{ $vendor->id }}" class="custom-control-input status" value="1" {{ $vendor->active == 1 ? 'checked' : '' }}>
                                    <span class="custom-control-indicator"></span>
                                </label>
                            </td>
                            <td>
                                <label class="custom-control custom-checkbox">
                                    <input type="checkbox" data-id="{{ $vendor->id }}" class="custom-control-input verified" value="1" {{ $vendor->verified == 1 ? 'checked' : '' }}>
                                    <span class="custom-control-indicator"></span>
                                </label>
                            </td>
                            <td>
                                <label class="custom-control custom-checkbox">
                                    <input type="checkbox" data-id="{{ $vendor->id }}" class="custom-control-input live" id="live" value="1" name="live" {{ $vendor->live == 1 ? 'checked' : '' }}>
                                    <span class="custom-control-indicator"></span>
                                </label>
                            </td>
                            <td>
                                <label class="custom-control custom-checkbox">
                                    <input type="checkbox" data-id="{{ $vendor->id }}" class="custom-control-input mainSlider" value="1" {{ $vendor->show_in_main_slider == 1 ? 'checked' : '' }}>
                                    <span class="custom-control-indicator"></span>
                                </label>
                            </td>
                            <td>
                                <label class="custom-control custom-checkbox">
                                    <input type="checkbox" data-id="{{ $vendor->id }}" class="custom-control-input mobileMainSlider" value="1" {{ $vendor->show_in_mobile_main_slider == 1 ? 'checked' : '' }}>
                                    <span class="custom-control-indicator"></span>
                                </label>
                            </td>
                            <td>
                                <label class="custom-control custom-checkbox">
                                    <input type="checkbox" data-id="{{ $vendor->id }}" class="custom-control-input featureVendor" value="1" {{ $vendor->feature_vendor == 1 ? 'checked' : '' }}>
                                    <span class="custom-control-indicator"></span>
                                </label>
                            </td>
                            <td>{{ date('m/d/Y g:i:s a', strtotime($vendor->created_at)) }}</td>
                            <td>
                                <a class="btnEdit" href="{{ route('admin_brand_edit', ['vendor'=> $vendor->id]) }}" style="color: blue">Edit</a> |
                                <a class="btnDelete" data-id="{{ $vendor->id }}" role="button" style="color: red">Delete</a>
                            </td>
                        </tr>
                    @endforeach
                </tbody>
            </table>
        </div>
    </div>

    <div class="modal fade" id="deleteModal" role="dialog" aria-labelledby="deleteModal">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-header bg-danger">
                    <h4 class="modal-title text-white" id="deleteModal">Delete</h4>
                </div>
                <div class="modal-body">
                    <p>
                        Are you sure want to delete?
                    </p>
                </div>
                <div class="modal-footer">
                    <button class="btn  btn-default" data-dismiss="modal">Close</button>
                    <button class="btn  btn-danger" id="modalBtnDelete">Delete</button>
                </div>
            </div>
        </div>
        <!--- end modals-->
    </div>
@stop

@section('additionalJS')
    <script type="text/javascript" src="{{ asset('plugins/toastr/toastr.min.js') }}"></script>
    <script>
        $(function () {
            $.ajaxSetup({
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                }
            });

            $('.status').change(function () {
                var status = 0;
                var id = $(this).data('id');

                if ($(this).is(':checked'))
                    status = 1;

                $.ajax({
                    method: "POST",
                    url: "{{ route('admin_brand_change_status') }}",
                    data: { id: id, status: status }
                }).done(function( msg ) {
                    toastr.success('Status Updated!');
                });
            });

            $('.verified').change(function () {
                var status = 0;
                var id = $(this).data('id');

                if ($(this).is(':checked'))
                    status = 1;

                $.ajax({
                    method: "POST",
                    url: "{{ route('admin_brand_change_verified') }}",
                    data: { id: id, status: status }
                }).done(function( msg ) {
                    toastr.success('Status Updated!');
                });
            });

            $('.live').change(function () {
                var status = 0;
                var id = $(this).data('id');

                if ($(this).is(':checked'))
                    status = 1;

                $.ajax({
                    method: "POST",
                    url: "{{ route('admin_brand_change_live') }}",
                    data: { id: id, status: status }
                }).done(function( msg ) {
                    toastr.success('Live Updated!');
                });
            });

            $('.mainSlider').change(function () {
                var status = 0;
                var id = $(this).data('id');

                if ($(this).is(':checked'))
                    status = 1;

                $.ajax({
                    method: "POST",
                    url: "{{ route('admin_brand_change_main_slider') }}",
                    data: { id: id, status: status }
                }).done(function( msg ) {
                    toastr.success('Slider Setting Updated!');
                });
            });

            $('.mobileMainSlider').change(function () {
                var status = 0;
                var id = $(this).data('id');

                if ($(this).is(':checked'))
                    status = 1;

                $.ajax({
                    method: "POST",
                    url: "{{ route('admin_brand_change_mobile_main_slider') }}",
                    data: { id: id, status: status }
                }).done(function( msg ) {
                    toastr.success('Slider Setting Updated!');
                });
            });

            $('.featureVendor').change(function () {
                var status = 0;
                var id = $(this).data('id');

                if ($(this).is(':checked'))
                    status = 1;

                $.ajax({
                    method: "POST",
                    url: "{{ route('admin_brand_change_feature_vendor') }}",
                    data: { id: id, status: status }
                }).done(function( msg ) {
                    toastr.success('Feature Vendor Setting Updated!');
                });
            });

            // Delete
            $('.btnDelete').click(function () {
                $('#deleteModal').modal('show');
                selectedId = $(this).data('id');
            });

            $('#modalBtnDelete').click(function () {
                $.ajax({
                    method: "POST",
                    url: "{{ route('admin_brand_delete') }}",
                    data: { id: selectedId }
                }).done(function( msg ) {
                    location.reload();
                });
            });
        });
    </script>
@stop