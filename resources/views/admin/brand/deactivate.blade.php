@extends('admin.layouts.admin')

@section('content')
    <div class="row">
        <div class="col-md-12">
            <table class="table">
                <thead>
                <tr>
                    <th>Company Name</th>
                    <th>Business Name</th>
                    <th>Created At</th>
                </tr>
                </thead>

                <tbody>
                @foreach($vendors as $vendor)
                    <tr>
                        <td>{{ $vendor->company_name }}</td>
                        <td>{{ $vendor->business_name }}</td>
                        <td>{{ date('m/d/Y g:i:s a') }}</td>
                    </tr>
                @endforeach
                </tbody>
            </table>
        </div>
    </div>
@stop