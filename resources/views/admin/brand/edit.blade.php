@extends('admin.layouts.admin')

@section('additionalCSS')
    <style>
        .required {
            color: red;
        }

        .form-group label {
            padding-left: 0px !important;
        }
    </style>
@stop

@section('content')
    <form action="{{ route('admin_brand_edit_post', ['vendor' => $vendor->id]) }}" method="POST">
        @csrf
        <h6 class="text-muted text-normal">Your Information</h6>
        <hr class="margin-bottom-1x">

        <div class="row">
            <div class="col-md-6">
                <div class="form-group">
                    <label for="small-rounded-input">Company Name <span class="required">*</span></label>
                    <input class="form-control form-control-rounded form-control-sm{{ $errors->has('companyName') ? ' is-invalid' : '' }}"
                           type="text" id="companyName" name="companyName"
                           value="{{ empty(old('companyName')) ? ($errors->has('companyName') ? '' : $vendor->company_name) : old('companyName') }}">

                    @if ($errors->has('companyName'))
                        <div class="form-control-feedback">{{ $errors->first('companyName') }}</div>
                    @endif
                </div>

                <div class="form-group">
                    <label for="small-rounded-input">First Name <span class="required">*</span></label>
                    <input class="form-control form-control-rounded form-control-sm{{ $errors->has('firstName') ? ' is-invalid' : '' }}"
                           type="text" id="firstName" name="firstName"
                           value="{{ empty(old('firstName')) ? ($errors->has('firstName') ? '' : $vendor->user->first_name) : old('firstName') }}">

                    @if ($errors->has('firstName'))
                        <div class="form-control-feedback">{{ $errors->first('firstName') }}</div>
                    @endif
                </div>

                <div class="form-group">
                    <label for="small-rounded-input">User ID <span class="required">*</span></label>
                    <input class="form-control form-control-rounded form-control-sm{{ $errors->has('userId') ? ' is-invalid' : '' }}"
                           type="text" id="userId" name="userId"
                           value="{{ empty(old('userId')) ? ($errors->has('userId') ? '' : $vendor->user->user_id) : old('userId') }}">

                    @if ($errors->has('userId'))
                        <div class="form-control-feedback">{{ $errors->first('userId') }}</div>
                    @endif
                </div>

                <div class="form-group">
                    <label for="small-rounded-input">Website</label>
                    <input class="form-control form-control-rounded form-control-sm{{ $errors->has('website') ? ' is-invalid' : '' }}"
                           type="text" id="website" name="website"
                           value="{{ empty(old('website')) ? ($errors->has('website') ? '' : $vendor->website) : old('website') }}">

                    @if ($errors->has('website'))
                        <div class="form-control-feedback">{{ $errors->first('website') }}</div>
                    @endif
                </div>
            </div>

            <div class="col-md-6">
                <div class="form-group">
                    <label for="small-rounded-input">Business (DBA) Name <span class="required">*</span></label>
                    <input class="form-control form-control-rounded form-control-sm{{ $errors->has('businessName') ? ' is-invalid' : '' }}"
                           type="text" id="businessName" name="businessName"
                           value="{{ empty(old('businessName')) ? ($errors->has('businessName') ? '' : $vendor->business_name) : old('businessName') }}">

                    @if ($errors->has('businessName'))
                        <div class="form-control-feedback">{{ $errors->first('businessName') }}</div>
                    @endif
                </div>

                <div class="form-group">
                    <label for="small-rounded-input">Last Name <span class="required">*</span></label>
                    <input class="form-control form-control-rounded form-control-sm{{ $errors->has('lastName') ? ' is-invalid' : '' }}"
                           type="text" id="lastName" name="lastName"
                           value="{{ empty(old('lastName')) ? ($errors->has('lastName') ? '' : $vendor->user->last_name) : old('lastName') }}">

                    @if ($errors->has('lastName'))
                        <div class="form-control-feedback">{{ $errors->first('lastName') }}</div>
                    @endif
                </div>

                <div class="form-group">
                    <label for="small-rounded-input">Password</label>
                    <input class="form-control form-control-rounded form-control-sm{{ $errors->has('password') ? ' is-invalid' : '' }}"
                           type="password" id="password" name="password">

                    @if ($errors->has('password'))
                        <div class="form-control-feedback">{{ $errors->first('password') }}</div>
                    @endif
                </div>

                <div class="form-group">
                    <label for="small-rounded-input">Email <span class="required">*</span></label>
                    <input class="form-control form-control-rounded form-control-sm{{ $errors->has('email') ? ' is-invalid' : '' }}"
                           type="email" id="email" name="email"
                           value="{{ empty(old('email')) ? ($errors->has('email') ? '' : $vendor->user->email) : old('email') }}">

                    @if ($errors->has('email'))
                        <div class="form-control-feedback">{{ $errors->first('email') }}</div>
                    @endif
                </div>
            </div>
        </div>

        <h6 class="text-muted text-normal">Showroom/Billing Address</h6>
        <hr class="margin-bottom-1x">

        <div class="row">
            <div class="col-md-12">
                <label for="small-rounded-input">Location</label><br>
            </div>
        </div>

        <div class="row">
            <div class="col-md-12">
                <label for="locationUS" class="custom-control custom-radio">
                    <input class="custom-control-input location" type="radio" id="locationUS" name="location" value="US"
                            {{ empty(old('location')) ? ($vendor->billing_location == "US" ? 'checked' : '') :
                                    (old('location') == 'US' ? 'checked' : '') }}>
                    <span class="custom-control-indicator"></span>
                    <span class="custom-control-description">United States</span>
                </label>

                <label for="locationCA" class="custom-control custom-radio">
                    <input class="custom-control-input location" type="radio" id="locationCA" name="location" value="CA"
                            {{ empty(old('location')) ? ($vendor->billing_location == "CA" ? 'checked' : '') :
                                    (old('location') == 'CA' ? 'checked' : '') }}>
                    <span class="custom-control-indicator"></span>
                    <span class="custom-control-description">Canada</span>
                </label>

                <label for="locationInt" class="custom-control custom-radio">
                    <input class="custom-control-input location" type="radio" id="locationInt" name="location" value="INT"
                            {{ empty(old('location')) ? ($vendor->billing_location == "INT" ? 'checked' : '') :
                                    (old('location') == 'INT' ? 'checked' : '') }}>
                    <span class="custom-control-indicator"></span>
                    <span class="custom-control-description">International</span>
                </label>
            </div>
        </div>

        <div class="row">
            <div class="col-md-5">
                <div class="form-group">
                    <label for="small-rounded-input">Address <span class="required">*</span></label>
                    <input class="form-control form-control-rounded form-control-sm{{ $errors->has('address') ? ' is-invalid' : '' }}"
                           type="text" id="address" name="address"
                           value="{{ empty(old('address')) ? ($errors->has('address') ? '' : $vendor->billing_address) : old('address') }}">

                    @if ($errors->has('address'))
                        <div class="form-control-feedback">{{ $errors->first('address') }}</div>
                    @endif
                </div>
            </div>

            <div class="col-md-1">
                <div class="form-group">
                    <label for="small-rounded-input">Unit #</label>
                    <input class="form-control form-control-rounded form-control-sm{{ $errors->has('unit') ? ' is-invalid' : '' }}"
                           type="text" id="unit" name="unit"
                           value="{{ empty(old('unit')) ? ($errors->has('unit') ? '' : $vendor->billing_unit) : old('unit') }}">
                </div>
            </div>

            <div class="col-md-6">
                <div class="form-group">
                    <label for="small-rounded-input">City <span class="required">*</span></label>
                    <input class="form-control form-control-rounded form-control-sm{{ $errors->has('city') ? ' is-invalid' : '' }}"
                           type="text" id="city" name="city"
                           value="{{ empty(old('city')) ? ($errors->has('city') ? '' : $vendor->billing_city) : old('city') }}">

                    @if ($errors->has('city'))
                        <div class="form-control-feedback">{{ $errors->first('city') }}</div>
                    @endif
                </div>
            </div>
        </div>

        <div class="row">
            <div class="col-md-6">
                <div class="form-group" id="form-group-state">
                    <label for="small-rounded-input">State <span class="required">*</span></label>
                    <input class="form-control form-control-rounded form-control-sm{{ $errors->has('state') ? ' is-invalid' : '' }}"
                           type="text" id="state" name="state"
                           value="{{ empty(old('state')) ? ($errors->has('state') ? '' : $vendor->billing_state) : old('state') }}">

                    @if ($errors->has('state'))
                        <div class="form-control-feedback">{{ $errors->first('state') }}</div>
                    @endif
                </div>

                <div class="form-group" id="form-group-state-select">
                    <label for="small-rounded-input">State <span class="required">*</span></label>
                    <select class="form-control form-control-rounded form-control-sm{{ $errors->has('stateSelect') ? ' is-invalid' : '' }}"
                            id="stateSelect" name="stateSelect">
                        <option value="">Select State</option>
                    </select>

                    @if ($errors->has('stateSelect'))
                        <div class="form-control-feedback">{{ $errors->first('stateSelect') }}</div>
                    @endif
                </div>
            </div>

            <div class="col-md-6">
                <div class="form-group">
                    <label for="small-rounded-input">Zip Code <span class="required">*</span></label>
                    <input class="form-control form-control-rounded form-control-sm{{ $errors->has('zipCode') ? ' is-invalid' : '' }}"
                           type="text" id="zipCode" name="zipCode"
                           value="{{ empty(old('zipCode')) ? ($errors->has('zipCode') ? '' : $vendor->billing_zip) : old('zipCode') }}">

                    @if ($errors->has('zipCode'))
                        <div class="form-control-feedback">{{ $errors->first('zipCode') }}</div>
                    @endif
                </div>
            </div>
        </div>

        <div class="row">
            <div class="col-md-7">
                <div class="form-group">
                    <label for="small-rounded-input">Country <span class="required">*</span></label>
                    <select class="form-control form-control-rounded form-control-sm{{ $errors->has('country') ? ' is-invalid' : '' }}"
                            id="country" name="country">
                        <option value="">Select Country</option>
                        @foreach($countries as $country)
                            <option data-code="{{ $country->code }}" value="{{ $country->id }}"
                                    {{ empty(old('country')) ? ($errors->has('country') ? '' : ($vendor->billing_country_id == $country->id ? 'selected' : '')) :
                                    (old('country') == $country->id ? 'selected' : '') }}>{{ $country->name }}</option>
                        @endforeach
                    </select>

                    @if ($errors->has('country'))
                        <div class="form-control-feedback">{{ $errors->first('country') }}</div>
                    @endif
                </div>
            </div>
        </div>

        <div class="row">
            <div class="col-md-6">
                <div class="form-group">
                    <label for="small-rounded-input">Phone <span class="required">*</span></label>
                    <input class="form-control form-control-rounded form-control-sm{{ $errors->has('phone') ? ' is-invalid' : '' }}"
                           type="text" id="phone" name="phone"
                           value="{{ empty(old('phone')) ? ($errors->has('phone') ? '' : $vendor->billing_phone) : old('phone') }}">

                    @if ($errors->has('phone'))
                        <div class="form-control-feedback">{{ $errors->first('phone') }}</div>
                    @endif
                </div>
            </div>

            <div class="col-md-6">
                <div class="form-group">
                    <label for="small-rounded-input">Fax</label>
                    <input class="form-control form-control-rounded form-control-sm{{ $errors->has('fax') ? ' is-invalid' : '' }}"
                           type="text" id="fax" name="fax"
                           value="{{ empty(old('fax')) ? ($errors->has('fax') ? '' : $vendor->billing_fax) : old('fax') }}">

                    @if ($errors->has('fax'))
                        <div class="form-control-feedback">{{ $errors->first('fax') }}</div>
                    @endif
                </div>
            </div>
        </div>

        <div class="row">
            <div class="col-md-12">
                <label class="custom-control custom-checkbox">
                    <input class="custom-control-input" type="checkbox" id="showroomCommercial" name="showroomCommercial" value="1"
                            {{ empty(old('showroomCommercial')) ? ($vendor->billing_commercial == 1 ? 'checked' : '') :
                                    (old('showroomCommercial') ? 'checked' : '') }}>
                    <span class="custom-control-indicator"></span>
                    <span class="custom-control-description">This address is commercial.</span>
                </label>
            </div>
        </div>

        <h6 class="text-muted text-normal">Factory Address</h6>
        <hr class="margin-bottom-1x">

        <div class="row">
            <div class="col-md-12">
                <label class="custom-control custom-checkbox">
                    <input class="custom-control-input" type="checkbox" id="sameAsShowroomAddress" name="sameAsShowroomAddress" {{ old('sameAsShowroomAddress') ? 'checked' : '' }}>
                    <span class="custom-control-indicator"></span>
                    <span class="custom-control-description">Check here if same as billing address. PO BOX is NOT allowed as a shipping address.</span>
                </label>
            </div>
        </div>

        <div class="row">
            <div class="col-md-12">
                <label for="factoryLocationUS" class="custom-control custom-radio">
                    <input class="custom-control-input factoryLocation" type="radio" id="factoryLocationUS" name="factoryLocation" value="US"
                            {{ empty(old('factoryLocation')) ? ($vendor->factory_location == "US" ? 'checked' : '') :
                                    (old('factoryLocation') == 'US' ? 'checked' : '') }}>
                    <span class="custom-control-indicator"></span>
                    <span class="custom-control-description">United States</span>
                </label>

                <label for="factoryLocationCA" class="custom-control custom-radio">
                    <input class="custom-control-input factoryLocation" type="radio" id="factoryLocationCA" name="factoryLocation" value="CA"
                            {{ empty(old('factoryLocation')) ? ($vendor->factory_location == "CA" ? 'checked' : '') :
                                    (old('factoryLocation') == 'CA' ? 'checked' : '') }}>
                    <span class="custom-control-indicator"></span>
                    <span class="custom-control-description">Canada</span>
                </label>

                <label for="factoryLocationInt" class="custom-control custom-radio">
                    <input class="custom-control-input factoryLocation" type="radio" id="factoryLocationInt" name="factoryLocation" value="INT"
                            {{ empty(old('factoryLocation')) ? ($vendor->factory_location == "INT" ? 'checked' : '') :
                                    (old('factoryLocation') == 'INT' ? 'checked' : '') }}>
                    <span class="custom-control-indicator"></span>
                    <span class="custom-control-description">International</span>
                </label>
            </div>
        </div>

        <div class="row">
            <div class="col-md-5">
                <div class="form-group">
                    <label for="small-rounded-input">Address <span class="required">*</span></label>
                    <input class="form-control form-control-rounded form-control-sm{{ $errors->has('factoryAddress') ? ' is-invalid' : '' }}"
                           type="text" id="factoryAddress" name="factoryAddress"
                           value="{{ empty(old('factoryAddress')) ? ($errors->has('factoryAddress') ? '' : $vendor->factory_address) : old('factoryAddress') }}">

                    @if ($errors->has('factoryAddress'))
                        <div class="form-control-feedback">{{ $errors->first('factoryAddress') }}</div>
                    @endif
                </div>
            </div>

            <div class="col-md-1">
                <div class="form-group">
                    <label for="small-rounded-input">Unit #</label>
                    <input class="form-control form-control-rounded form-control-sm{{ $errors->has('factoryUnit') ? ' is-invalid' : '' }}"
                           type="text" id="factoryUnit" name="factoryUnit"
                           value="{{ empty(old('factoryUnit')) ? ($errors->has('factoryUnit') ? '' : $vendor->factory_unit) : old('factoryUnit') }}">
                </div>
            </div>

            <div class="col-md-6">
                <div class="form-group">
                    <label for="small-rounded-input">City <span class="required">*</span></label>
                    <input class="form-control form-control-rounded form-control-sm{{ $errors->has('factoryCity') ? ' is-invalid' : '' }}"
                           type="text" id="factoryCity" name="factoryCity"
                           value="{{ empty(old('factoryCity')) ? ($errors->has('factoryCity') ? '' : $vendor->factory_city) : old('factoryCity') }}">

                    @if ($errors->has('factoryCity'))
                        <div class="form-control-feedback">{{ $errors->first('factoryCity') }}</div>
                    @endif
                </div>
            </div>
        </div>

        <div class="row">
            <div class="col-md-6">
                <div class="form-group" id="form-group-factory-state">
                    <label for="small-rounded-input">State <span class="required">*</span></label>
                    <input class="form-control form-control-rounded form-control-sm{{ $errors->has('factory_state') ? ' is-invalid' : '' }}"
                           type="text" id="factoryState" name="factoryState"
                           value="{{ empty(old('factoryState')) ? ($errors->has('factoryState') ? '' : $vendor->factory_city) : old('factoryState') }}">

                    @if ($errors->has('factoryState'))
                        <div class="form-control-feedback">{{ $errors->first('factoryState') }}</div>
                    @endif
                </div>

                <div class="form-group" id="form-group-factory-state-select">
                    <label for="small-rounded-input">State <span class="required">*</span></label>
                    <select class="form-control form-control-rounded form-control-sm{{ $errors->has('factoryStateSelect') ? ' is-invalid' : '' }}"
                            id="factoryStateSelect" name="factoryStateSelect">
                        <option value="">Select State</option>
                    </select>

                    @if ($errors->has('factoryStateSelect'))
                        <div class="form-control-feedback">{{ $errors->first('factoryStateSelect') }}</div>
                    @endif
                </div>
            </div>

            <div class="col-md-6">
                <div class="form-group">
                    <label for="small-rounded-input">Zip Code <span class="required">*</span></label>
                    <input class="form-control form-control-rounded form-control-sm{{ $errors->has('factoryZipCode') ? ' is-invalid' : '' }}"
                           type="text" id="factoryZipCode" name="factoryZipCode"
                           value="{{ empty(old('factoryZipCode')) ? ($errors->has('factoryZipCode') ? '' : $vendor->factory_zip) : old('factoryZipCode') }}">

                    @if ($errors->has('factoryZipCode'))
                        <div class="form-control-feedback">{{ $errors->first('factoryZipCode') }}</div>
                    @endif
                </div>
            </div>
        </div>

        <div class="row">
            <div class="col-md-7">
                <div class="form-group">
                    <label for="small-rounded-input">Country <span class="required">*</span></label>
                    <select class="form-control form-control-rounded form-control-sm{{ $errors->has('factoryCountry') ? ' is-invalid' : '' }}"
                            id="factoryCountry" name="factoryCountry">
                        <option value="">Select Country</option>
                        @foreach($countries as $country)
                            <option data-code="{{ $country->code }}" value="{{ $country->id }}"
                                    {{ empty(old('factoryCountry')) ? ($errors->has('factoryCountry') ? '' : ($vendor->factory_country_id == $country->id ? 'selected' : '')) :
                                    (old('factoryCountry') == $country->id ? 'selected' : '') }}>{{ $country->name }}</option>
                        @endforeach
                    </select>

                    @if ($errors->has('factoryCountry'))
                        <div class="form-control-feedback">{{ $errors->first('factoryCountry') }}</div>
                    @endif
                </div>
            </div>
        </div>

        <div class="row">
            <div class="col-md-6">
                <div class="form-group">
                    <label for="small-rounded-input">Phone <span class="required">*</span></label>
                    <input class="form-control form-control-rounded form-control-sm{{ $errors->has('factoryPhone') ? ' is-invalid' : '' }}"
                           type="text" id="factoryPhone" name="factoryPhone"
                           value="{{ empty(old('factoryPhone')) ? ($errors->has('factoryPhone') ? '' : $vendor->factory_phone) : old('factoryPhone') }}">

                    @if ($errors->has('factoryPhone'))
                        <div class="form-control-feedback">{{ $errors->first('factoryPhone') }}</div>
                    @endif
                </div>

                <div class="form-group">
                    <label for="small-rounded-input">Evening Phone Number</label>
                    <input class="form-control form-control-rounded form-control-sm{{ $errors->has('factoryEveningPhone') ? ' is-invalid' : '' }}"
                           type="text" id="factoryEveningPhone" name="factoryEveningPhone"
                           value="{{ empty(old('factoryEveningPhone')) ? ($errors->has('factoryEveningPhone') ? '' : $vendor->factory_evening_phone) : old('factoryEveningPhone') }}">

                    @if ($errors->has('factoryEveningPhone'))
                        <div class="form-control-feedback">{{ $errors->first('factoryEveningPhone') }}</div>
                    @endif
                </div>
            </div>

            <div class="col-md-6">
                <div class="form-group">
                    <label for="small-rounded-input">Alternate Phone</label>
                    <input class="form-control form-control-rounded form-control-sm{{ $errors->has('factoryAlternatePhone') ? ' is-invalid' : '' }}"
                           type="text" id="factoryAlternatePhone" name="factoryAlternatePhone"
                           value="{{ empty(old('factoryAlternatePhone')) ? ($errors->has('factoryAlternatePhone') ? '' : $vendor->factory_alternate_phone) : old('factoryAlternatePhone') }}">

                    @if ($errors->has('factoryAlternatePhone'))
                        <div class="form-control-feedback">{{ $errors->first('factoryAlternatePhone') }}</div>
                    @endif
                </div>

                <div class="form-group">
                    <label for="small-rounded-input">Fax</label>
                    <input class="form-control form-control-rounded form-control-sm{{ $errors->has('factoryFax') ? ' is-invalid' : '' }}"
                           type="text" id="factoryFax" name="factoryFax"
                           value="{{ empty(old('factoryFax')) ? ($errors->has('factoryFax') ? '' : $vendor->factory_fax) : old('factoryFax') }}">

                    @if ($errors->has('factoryFax'))
                        <div class="form-control-feedback">{{ $errors->first('factoryFax') }}</div>
                    @endif
                </div>
            </div>
        </div>

        <div class="row">
            <div class="col-md-12">
                <label class="custom-control custom-checkbox">
                    <input class="custom-control-input" type="checkbox" id="factoryCommercial" name="factoryCommercial" value="1"
                            {{ empty(old('factoryCommercial')) ? ($vendor->factory_commercial == 1 ? 'checked' : '') :
                                    (old('factoryCommercial') ? 'checked' : '') }}>
                    <span class="custom-control-indicator"></span>
                    <span class="custom-control-description">This address is commercial.</span>
                </label>
            </div>
        </div>

        <h6 class="text-muted text-normal">Additional Information</h6>
        <hr class="margin-bottom-1x">

        <div class="row">
            <div class="col-md-12">
                <div class="form-group">
                    <label for="small-rounded-input">Company Information</label>
                    <textarea class="form-control form-control-rounded{{ $errors->has('companyInformation') ? ' is-invalid' : '' }}"
                              id="companyInformation" name="companyInformation" rows="5">{{ empty(old('companyInformation')) ? ($errors->has('companyInformation') ? '' : $vendor->company_info) : old('companyInformation') }}</textarea>

                    @if ($errors->has('companyInformation'))
                        <div class="form-control-feedback">{{ $errors->first('companyInformation') }}</div>
                    @endif
                </div>

                <label for="small-rounded-input">How did you hear about us? <span class="required">*</span></label><br>
            </div>
        </div>

        <div class="row">
            <div class="col-md-12">
                <label for="hearAboutUsGoogle" class="custom-control custom-radio">
                    <input class="custom-control-input" type="radio" id="hearAboutUsGoogle" name="hearAboutUs" value="google"
                            {{ empty(old('hearAboutUs')) ? ($vendor->hear_about_us == "google" ? 'checked' : '') :
                                    (old('hearAboutUs') == 'google' ? 'checked' : '') }}>
                    <span class="custom-control-indicator"></span>
                    <span class="custom-control-description">Google</span>
                </label>

                <label for="hearAboutUsYahoo" class="custom-control custom-radio">
                    <input class="custom-control-input" type="radio" id="hearAboutUsYahoo" name="hearAboutUs" value="yahoo"
                            {{ empty(old('hearAboutUs')) ? ($vendor->hear_about_us == "yahoo" ? 'checked' : '') :
                                    (old('hearAboutUs') == 'yahoo' ? 'checked' : '') }}>
                    <span class="custom-control-indicator"></span>
                    <span class="custom-control-description">Yahoo</span>
                </label>

                <label for="hearAboutUsBing" class="custom-control custom-radio">
                    <input class="custom-control-input" type="radio" id="hearAboutUsBing" name="hearAboutUs" value="bing"
                            {{ empty(old('hearAboutUs')) ? ($vendor->hear_about_us == "bing" ? 'checked' : '') :
                                    (old('hearAboutUs') == 'bing' ? 'checked' : '') }}>
                    <span class="custom-control-indicator"></span>
                    <span class="custom-control-description">Bing</span>
                </label>

                <label for="hearAboutUsOtherSearch" class="custom-control custom-radio">
                    <input class="custom-control-input" type="radio" id="hearAboutUsOtherSearch" name="hearAboutUs" value="other_search"
                            {{ empty(old('hearAboutUs')) ? ($vendor->hear_about_us == "other_search" ? 'checked' : '') :
                                    (old('hearAboutUs') == 'other_search' ? 'checked' : '') }}>
                    <span class="custom-control-indicator"></span>
                    <span class="custom-control-description">Other search</span>
                </label>

                <label for="hearAboutUsMagicShow" class="custom-control custom-radio">
                    <input class="custom-control-input" type="radio" id="hearAboutUsMagicShow" name="hearAboutUs" value="magic_show"
                            {{ empty(old('hearAboutUs')) ? ($vendor->hear_about_us == "magic_show" ? 'checked' : '') :
                                    (old('hearAboutUs') == 'magic_show' ? 'checked' : '') }}>
                    <span class="custom-control-indicator"></span>
                    <span class="custom-control-description">MAGIC Show</span>
                </label>

                <label for="hearAboutUsVendor" class="custom-control custom-radio">
                    <input class="custom-control-input" type="radio" id="hearAboutUsVendor" name="hearAboutUs" value="vendor"
                            {{ empty(old('hearAboutUs')) ? ($vendor->hear_about_us == "vendor" ? 'checked' : '') :
                                    (old('hearAboutUs') == 'vendor' ? 'checked' : '') }}>
                    <span class="custom-control-indicator"></span>
                    <span class="custom-control-description">Referred by a Vendor</span>
                </label>

                <label for="hearAboutUsFriend" class="custom-control custom-radio">
                    <input class="custom-control-input" type="radio" id="hearAboutUsFriend" name="hearAboutUs" value="friend"
                            {{ empty(old('hearAboutUs')) ? ($vendor->hear_about_us == "friend" ? 'checked' : '') :
                                    (old('hearAboutUs') == 'friend' ? 'checked' : '') }}>
                    <span class="custom-control-indicator"></span>
                    <span class="custom-control-description">Referred by a Friend</span>
                </label>
            </div>
        </div>

        <div class="row">
            <div class="col-md-1">
                <label for="hearAboutUsOther" class="custom-control custom-radio">
                    <input class="custom-control-input" type="radio" id="hearAboutUsOther" name="hearAboutUs" value="other"
                            {{ empty(old('hearAboutUs')) ? ($vendor->hear_about_us == "other" ? 'checked' : '') :
                                    (old('hearAboutUs') == 'other' ? 'checked' : '') }}>
                    <span class="custom-control-indicator"></span>
                    <span class="custom-control-description">Other</span>
                </label>
            </div>

            <div class="col-md-5">
                <div class="form-group">
                    <input class="form-control form-control-rounded form-control-sm{{ $errors->has('hearAboutUsOtherText') ? ' is-invalid' : '' }}"
                           type="text" id="hearAboutUsOtherText" name="hearAboutUsOtherText"
                           value="{{ empty(old('hearAboutUsOtherText')) ? ($errors->has('hearAboutUsOtherText') ? '' : $vendor->hear_about_us_other) : old('hearAboutUsOtherText') }}">

                    @if ($errors->has('hearAboutUsOtherText'))
                        <div class="form-control-feedback">{{ $errors->first('hearAboutUsOtherText') }}</div>
                    @endif
                </div>
            </div>
        </div>

        @if ($errors->has('hearAboutUs'))
            <div class="row">
                <div class="col-md-12">
                    <div class="form-group has-danger">
                        <div class="form-control-feedback">{{ $errors->first('hearAboutUs') }}</div>
                    </div>
                </div>
            </div>
        @endif

        <div class="row">
            <div class="col-md-12">
                <label class="custom-control custom-checkbox">
                    <input class="custom-control-input" type="checkbox" id="receiveSpecialOffers" value="1" name="receiveSpecialOffers"
                            {{ ($vendor->receive_offers == 1) ? 'checked' : '' }}>
                    <span class="custom-control-indicator"></span>
                    <span class="custom-control-description">Sign up to receive special offers and information.</span>
                </label>
            </div>
        </div>

        <div class="row">
            <div class="col-md-12">
                <input class="btn btn-primary" type="submit" value="UPDATE">
            </div>
        </div>
    </form>
@stop

@section('additionalJS')
    <script>
        var usStates = <?php echo json_encode($usStates); ?>;
        var caStates = <?php echo json_encode($caStates); ?>;
        var oldState = '{{ empty(old('stateSelect')) ? ($errors->has('stateSelect') ? '' : $vendor->billing_state_id) : old('stateSelect') }}';
        var oldFactoryState = '{{ empty(old('factoryStateSelect')) ? ($errors->has('factoryStateSelect') ? '' : $vendor->factory_state_id) : old('factoryStateSelect') }}';

        $(function () {
            $('form').bind('submit', function () {
                $(this).find(':input').prop('disabled', false);
            });

            $('#address').keyup(function () {
                var text = $(this).val();
                if ($("#sameAsShowroomAddress").is(':checked'))
                    $('#factoryAddress').val(text);
            });

            $('#unit').keyup(function () {
                var text = $(this).val();
                if ($("#sameAsShowroomAddress").is(':checked'))
                    $('#factoryUnit').val(text);
            });

            $('#city').keyup(function () {
                var text = $(this).val();
                if ($("#sameAsShowroomAddress").is(':checked'))
                    $('#factoryCity').val(text);
            });

            $('#state').keyup(function () {
                var text = $(this).val();
                if ($("#sameAsShowroomAddress").is(':checked'))
                    $('#factoryState').val(text);
            });

            $('#zipCode').keyup(function () {
                var text = $(this).val();
                if ($("#sameAsShowroomAddress").is(':checked'))
                    $('#factoryZipCode').val(text);
            });

            $('#phone').keyup(function () {
                var text = $(this).val();
                if ($("#sameAsShowroomAddress").is(':checked'))
                    $('#factoryPhone').val(text);
            });

            $('#fax').keyup(function () {
                var text = $(this).val();
                if ($("#sameAsShowroomAddress").is(':checked'))
                    $('#factoryFax').val(text);
            });

            $('#sameAsShowroomAddress').change(function () {
                $('#address').trigger('keyup');
                $('#unit').trigger('keyup');
                $('#city').trigger('keyup');
                $('#state').trigger('keyup');
                $('#zipCode').trigger('keyup');
                $('#phone').trigger('keyup');
                $('#fax').trigger('keyup');

                var location = $('.location:checked').val();
                $('.factoryLocation[value=' + location + ']').prop('checked', true);
                $('.factoryLocation').trigger('change');

                $('#factoryCountry').val($('#country').val());
                $('#factoryState').val($('#state').val());
                $('#factoryStateSelect').val($('#stateSelect').val());
            });

            $('.location').change(function () {
                var location = $('.location:checked').val();

                if ($("#sameAsShowroomAddress").is(':checked')) {
                    $('.factoryLocation[value=' + location + ']').prop('checked', true);
                    $('.factoryLocation').trigger('change');
                }

                if (location == 'CA' || location == 'US') {
                    if (location == 'US')
                        $('#country').val('1');
                    else
                        $('#country').val('2');


                    $('#country').prop('disabled', 'disabled');
                    $('#form-group-state-select').show();
                    $('#stateSelect').val('');
                    $('#form-group-state').hide();

                    $('#stateSelect').html('<option value="">Select State</option>');

                    if (location == 'US') {
                        $.each(usStates, function (index, value) {
                            if (value.id == oldState)
                                $('#stateSelect').append('<option value="'+value.id+'" selected>'+value.name+'</option>');
                            else
                                $('#stateSelect').append('<option value="'+value.id+'">'+value.name+'</option>');
                        });
                    }

                    if (location == 'CA') {
                        $.each(caStates, function (index, value) {
                            if (value.id == oldState)
                                $('#stateSelect').append('<option value="'+value.id+'" selected>'+value.name+'</option>');
                            else
                                $('#stateSelect').append('<option value="'+value.id+'">'+value.name+'</option>');
                        });
                    }
                } else {
                    $('#country').prop('disabled', false);
                    $('#form-group-state-select').hide();
                    $('#form-group-state').show();
                }
            });

            $('.factoryLocation').change(function () {
                var location = $('.factoryLocation:checked').val();

                if (location == 'CA' || location == 'US') {
                    if (location == 'US')
                        $('#factoryCountry').val('1');
                    else
                        $('#factoryCountry').val('2');

                    $('#factoryCountry').prop('disabled', 'disabled');
                    $('#form-group-factory-state-select').show();
                    $('#factoryStateSelect').val('');
                    $('#form-group-factory-state').hide();

                    $('#factoryStateSelect').html('<option value="">Select State</option>');

                    if (location == 'US') {
                        $.each(usStates, function (index, value) {
                            if (value.id == oldFactoryState)
                                $('#factoryStateSelect').append('<option value="'+value.id+'" selected>'+value.name+'</option>');
                            else
                                $('#factoryStateSelect').append('<option value="'+value.id+'">'+value.name+'</option>');
                        });
                    }

                    if (location == 'CA') {
                        $.each(caStates, function (index, value) {
                            if (value.id == oldFactoryState)
                                $('#factoryStateSelect').append('<option value="'+value.id+'" selected>'+value.name+'</option>');
                            else
                                $('#factoryStateSelect').append('<option value="'+value.id+'">'+value.name+'</option>');
                        });
                    }
                } else {
                    $('#factoryCountry').prop('disabled', false);
                    $('#form-group-factory-state-select').hide();
                    $('#form-group-factory-state').show();
                }
            });

            $('#country').change(function () {
                var countryId = $(this).val();

                if (countryId == 1) {
                    $("#locationUS").prop("checked", true);
                    $('.location').trigger('change');
                } else if (countryId == 2) {
                    $("#locationCA").prop("checked", true);
                    $('.location').trigger('change');
                }
            });

            $('.location').trigger('change');
            $('.factoryLocation').trigger('change');
        })
    </script>
@stop