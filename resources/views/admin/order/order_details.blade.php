<?php use App\Enumeration\OrderStatus; ?>

@extends('admin.layouts.admin')

@section('additionalCSS')
    <link href="{{ asset('plugins/toastr/toastr.min.css') }}" rel="stylesheet">
    <style>
        .table .table {
            background-color: white;
        }

        .is-invalid{
            border-color: red;
        }
    </style>
@stop

@section('content')
    <form action="{{ route('admin_order_details_post', ['order' => $order->id]) }}" method="post">
        @csrf

        <div class="row">
            <div class="col-md-12 text-right">
                <a class="btn btn-primary" data-toggle="modal" data-target="#print-modal">Print</a>
            </div>
        </div>
        <br>
        <span class="order-title">Order Information</span>

        <div class="row">
            <div class="col-md-4">
                <table class="table table-bordered">
                    <tr>
                        <th>Order No.</th>
                        <td>{{ $order->order_number }}</td>
                    </tr>

                    <tr>
                        <th>Order Date</th>
                        <td>{{ date('F d, Y h:i:s a', strtotime($order->created_at)) }}</td>
                    </tr>

                    <tr>
                        <th>Order Status</th>
                        <td>
                            <select class="form-control" name="order_status">
                                <option value="{{ OrderStatus::$NEW_ORDER }}" {{ $order->status == OrderStatus::$NEW_ORDER ? 'selected' : '' }}>New Orders</option>
                                <option value="{{ OrderStatus::$CONFIRM_ORDER }}" {{ $order->status == OrderStatus::$CONFIRM_ORDER ? 'selected' : '' }}>Confirmed Orders</option>
                                <option value="{{ OrderStatus::$PARTIALLY_SHIPPED_ORDER }}" {{ $order->status == OrderStatus::$PARTIALLY_SHIPPED_ORDER ? 'selected' : '' }}>Partially Shipped Orders</option>
                                <option value="{{ OrderStatus::$FULLY_SHIPPED_ORDER }}" {{ $order->status == OrderStatus::$FULLY_SHIPPED_ORDER ? 'selected' : '' }}>Fully Shipped Orders</option>
                                <option value="{{ OrderStatus::$BACK_ORDER }}" {{ $order->status == OrderStatus::$BACK_ORDER ? 'selected' : '' }}>Back Ordered</option>
                                <option value="{{ OrderStatus::$CANCEL_BY_BUYER }}" {{ $order->status == OrderStatus::$CANCEL_BY_BUYER ? 'selected' : '' }}>Cancelled by Buyer</option>
                                <option value="{{ OrderStatus::$CANCEL_BY_VENDOR }}" {{ $order->status == OrderStatus::$CANCEL_BY_VENDOR ? 'selected' : '' }}>Cancelled by Vendor</option>
                                <option value="{{ OrderStatus::$CANCEL_BY_AGREEMENT }}" {{ $order->status == OrderStatus::$CANCEL_BY_AGREEMENT ? 'selected' : '' }}>Cancelled by Agreement</option>
                                <option value="{{ OrderStatus::$RETURNED }}" {{ $order->status == OrderStatus::$RETURNED ? 'selected' : '' }}>Returned</option>
                            </select>
                        </td>
                    </tr>
                </table>
            </div>

            <div class="col-md-4">
                <table class="table table-bordered d-none" id="card-info-table">
                    <tr>
                        <th>Full Name</th>
                        <td colspan="3"><span id="card-info-fullName"></span></td>
                    </tr>
                    <tr>
                        <th>Card Number</th>
                        <td colspan="3"><span id="card-info-number"></span></td>
                    </tr>
                    <tr>
                        <th>Expire</th>
                        <td><span id="card-info-expire"></span></td>
                        <th>CVC</th>
                        <td><span id="card-info-cvc"></span></td>
                    </tr>
                </table>
                <button class="btn btn-primary" id="btnShowCard">Show Card Info</button>
                <button class="btn btn-primary d-none" id="btnMask">Mask</button>
                <button class="btn btn-default d-none" id="btnHideCard">Hide Card Info</button>
                <br><br>
            </div>

            <div class="col-md-4">
                <table class="table table-bordered">
                    <tr>
                        <th>Created At</th>
                        <td>{{ date('F d, Y h:i:s a', strtotime($order->created_at)) }}</td>
                    </tr>

                    <tr>
                        <th>Modified At</th>
                        <td>{{ date('F d, Y h:i:s a', strtotime($order->updated_at)) }}</td>
                    </tr>
                    <tr>
                        <th># of Orders</th>
                        <td>{{ $countText }}</td>
                    </tr>
                </table>
            </div>
        </div>

        <span class="order-title">Buyer Information</span>

        <div class="row">
            <div class="col-md-6">

                <table class="table table-bordered">
                    <tr>
                        <td colspan="2"><b>Billing Address</b></td>
                    </tr>
                    <tr>
                        <th>Company</th>
                        <td>{{ $order->company_name }}</td>
                    </tr>

                    <tr>
                        <th>Name</th>
                        <td>{{ $order->name }}</td>
                    </tr>

                    <tr>
                        <th>Address</th>
                        <td>{{ $order->billing_address }},

                            @if ($order->billing_unit && $order->billing_unit != '')
                                Unit: {{ $order->billing_unit }},
                            @endif

                            {{ $order->billing_state }},
                            {{ $order->billing_city }},
                            {{ $order->billing_country }} - {{ $order->billing_zip }}
                        </td>
                    </tr>

                    <tr>
                        <th>Phone</th>
                        <td>{{ $order->billing_phone }}</td>
                    </tr>

                    <tr>
                        <th>Email</th>
                        <td>{{ $order->email }}</td>
                    </tr>
                </table>
            </div>
            <div class="col-md-6">
                <table class="table table-bordered">
                    <tr>
                        <td colspan="2"><b>Shipping Address</b></td>
                    </tr>
                    <tr>
                        <th>Company</th>
                        <td>{{ $order->company_name }}</td>
                    </tr>

                    <tr>
                        <th>Name</th>
                        <td>{{ $order->name }}</td>
                    </tr>

                    <tr>
                        <th>Address</th>
                        <td>{{ $order->shipping_address }},

                            @if ($order->shipping_unit && $order->shipping_unit != '')
                                Unit - {{ $order->shipping_unit }},
                            @endif

                            {{ $order->shipping_state }},
                            {{ $order->shipping_city }},
                            {{ $order->shipping_country }} - {{ $order->shipping_zip }}</td>
                    </tr>

                    <tr>
                        <th>Phone</th>
                        <td>{{ $order->shipping_phone }}</td>
                    </tr>

                    <tr>
                        <th>Email</th>
                        <td>{{ $order->email }}</td>
                    </tr>
                </table>
            </div>
        </div>

        <span class="order-title">Shipping Method</span>
        <div class="row">
            <div class="col-md-4">
                <table class="table table-bordered">
                    <tr>
                        <th width="30%">Shipping Method</th>
                        <td>
                            <select class="form-control{{ $errors->has('shipping_method_id') ? ' is-invalid' : '' }}" name="shipping_method_id">
                                <option value="">Select Shipping Method</option>
                                @foreach($shippingMethods as $method)
                                    <option value="{{ $method->id }}" {{ $method->id == $order->shipping_method_id ? 'selected' : '' }}>{{ $method->courier_id == 0 ? $method->ship_method_text : $method->shipMethod->name }}</option>
                                @endforeach
                            </select>
                        </td>
                    </tr>

                    <tr>
                        <th>Tracking Number</th>
                        <td>
                            <input type="text" class="form-control{{ $errors->has('tracking_number') ? ' is-invalid' : '' }}" name="tracking_number"
                                   value="{{ empty(old('tracking_number')) ? ($errors->has('tracking_number') ? '' : $order->tracking_number) : old('tracking_number') }}">
                        </td>
                    </tr>
                </table>
            </div>
        </div>


        <span class="order-title">Items</span>

        <div class="row">
            <div class="col-md-12 text-right">
                <button class="btn btn-primary" id="btnBackOrder">Back Order</button>
                <button class="btn btn-warning" id="btnOutOfStock">Out of Stock</button>
                <button class="btn btn-danger" id="btnDeleteItem">Delete</button>
            </div>
        </div>

        <br>

        <div class="table-responsive">
            <table class="table table-bordered">
                <thead>
                <tr>
                    <th>Image</th>
                    <th>Style No.</th>
                    <th class="text-center">Color</th>
                    <th class="text-center">Size</th>
                    <th class="text-center">Pack</th>
                    <th class="text-center">Total Qty</th>
                    <th class="text-center">Unit Price</th>
                    <th class="text-center">Amount</th>
                    <th></th>
                </tr>
                </thead>

                <tbody>
                @foreach($allItems as $item_id => $items)
                    <tr>
                        <td>
                            @if (sizeof($items[0]->item->images) > 0)
                                <img src="{{ asset($items[0]->item->images[0]->image_path) }}" alt="Product" style="height: 100px">
                            @else
                                <img src="{{ asset('images/no-image.png') }}" alt="Product">
                            @endif
                        </td>

                        <td>
                            {{ $items[0]->item->style_no }}
                        </td>

                        <td>
                            <table class="table table-bordered">
                                <thead>
                                <tr>
                                    <td style="border-top: none">&nbsp;</td>
                                </tr>
                                </thead>

                                <tbody>
                                @foreach($items as $item)
                                    <tr>
                                        <td>{{ $item->color }}</td>
                                    </tr>
                                @endforeach
                                </tbody>
                            </table>
                        </td>

                        <td>
                            <?php
                            $sizes = explode("-", $items[0]->size);
                            ?>

                            <table class="table table-bordered">
                                <thead>
                                <tr>
                                    @foreach($sizes as $size)
                                        <th style="border-top: none" class="text-center">{{ $size }}</th>
                                    @endforeach
                                </tr>
                                </thead>

                                <tbody>
                                @foreach($items as $item)
                                    <?php $packs = explode("-", $item->pack); ?>
                                    <tr>
                                        @for($i=0; $i < sizeof($sizes); $i++)
                                            {{--<td>{{ $packs[$i] }}</td>--}}
                                            <td class="text-center">
                                                <input class="input_size input_size_{{ $item->id }} {{ $errors->has('size_'.$item->id.'.'.$i) ? ' is-invalid' : '' }}"
                                                       data-id="{{ $item->id }}"
                                                       type="text" name="size_{{ $item->id }}[{{ $i }}]"
                                                       value="{{ empty(old('size_'.$item->id.'.'.$i)) ? ($errors->has('size_'.$item->id.'.'.$i) ? '' : $packs[$i]) : old('size_'.$item->id.'.'.$i) }}" style="width: 50px; text-align: center">
                                            </td>
                                        @endfor
                                    </tr>
                                @endforeach
                                </tbody>
                            </table>
                        </td>

                        <td class="text-center">
                            <table class="table table-bordered">
                                <thead>
                                <tr>
                                    <th style="border-top: none">&nbsp;</th>
                                </tr>
                                </thead>

                                <tbody>
                                @foreach($items as $item)
                                    <tr>
                                        {{--<td>
                                            {{ $item->qty }}
                                        </td>--}}
                                        <td class="text-center">
                                            <input class="input_pack input_pack_{{ $item->id }} {{ $errors->has('pack_'.$item->id) ? ' is-invalid' : '' }}"
                                                   data-id="{{ $item->id }}" type="text" name="pack_{{ $item->id }}"
                                                   value="{{ empty(old('pack_'.$item->id)) ? ($errors->has('pack_'.$item->id) ? '' : $item->qty) : old('pack_'.$item->id) }}" style="width: 50px; text-align: center">
                                        </td>

                                    </tr>
                                @endforeach
                                </tbody>
                            </table>
                        </td>

                        <td class="text-center">
                            <table class="table table-bordered">
                                <thead>
                                <tr>
                                    <th style="border-top: none">&nbsp;</th>
                                </tr>
                                </thead>

                                <tbody>
                                @foreach($items as $item)
                                    <tr>
                                        <td>
                                            <span class="total_qty" id="total_qty_{{ $item->id }}">{{ $item->total_qty }}</span>
                                        </td>
                                    </tr>
                                @endforeach
                                </tbody>
                            </table>
                        </td>

                        <td class="text-center">
                            <table class="table table-bordered">
                                <thead>
                                <tr>
                                    <th style="border-top: none">&nbsp;</th>
                                </tr>
                                </thead>

                                <tbody>
                                @foreach($items as $item)
                                    <tr>
                                        {{--<td>
                                            ${{ sprintf('%0.2f', $item->per_unit_price) }}
                                        </td>--}}
                                        <td class="text-center">
                                            $ <input class="input_unit_price {{ $errors->has('unit_price_'.$item->id) ? ' is-invalid' : '' }}"
                                                     id="input_unit_price_{{ $item->id }}"
                                                     type="text" data-id="{{ $item->id }}"
                                                     name="unit_price_{{ $item->id }}"
                                                     value="{{ empty(old('unit_price_'.$item->id)) ? ($errors->has('unit_price_'.$item->id) ? '' : sprintf('%0.2f', $item->per_unit_price)) : old('unit_price_'.$item->id) }}" style="width: 50px; text-align: center">
                                        </td>
                                    </tr>
                                @endforeach
                                </tbody>
                            </table>
                        </td>

                        <td class="text-center">
                            <table class="table table-bordered">
                                <thead>
                                <tr>
                                    <th style="border-top: none">&nbsp;</th>
                                </tr>
                                </thead>

                                <tbody>
                                @foreach($items as $item)
                                    <tr>
                                        <td>
                                            <span id="amount_{{ $item->id }}">${{ sprintf('%0.2f', $item->amount) }}</span>
                                            <input type="hidden" class="input_amount" id="input_amount_{{ $item->id }}" value="{{ $item->amount }}">
                                        </td>
                                    </tr>
                                @endforeach
                                </tbody>
                            </table>
                        </td>

                        <td class="text-center">
                            <table class="table table-bordered">
                                <thead>
                                <tr>
                                    <th style="border-top: none">&nbsp;</th>
                                </tr>
                                </thead>

                                <tbody>
                                @foreach($items as $item)
                                    <tr>
                                        <td>
                                            <label for="checkbox_{{ $item->id }}" class="custom-control custom-checkbox">
                                                <input type="checkbox" id="checkbox_{{ $item->id }}" class="custom-control-input item_checkbox" name="checkbox_{{ $item->id }}" value="{{ $item->id }}">
                                                <span class="custom-control-indicator"></span>
                                            </label>
                                        </td>
                                    </tr>
                                @endforeach
                                </tbody>
                            </table>
                        </td>
                    </tr>
                @endforeach
                </tbody>
            </table>
        </div>

        <div class="row">
            <div class="col-md-8"></div>
            <div class="col-md-4">
                <table class="table table-bordered">
                    <tr>
                        <th>Subtotal</th>
                        <td>
                            <span id="span_subtotal">${{ sprintf('%0.2f', $order->subtotal) }}</span>
                        </td>
                    </tr>

                    <tr>
                        <th>Discount ($)</th>
                        <td>
                            <input name="input_discount" id="input_discount" type="text"
                                   class="form-control{{ $errors->has('input_discount') ? ' is-invalid' : '' }}"
                                   value="{{ empty(old('input_discount')) ? ($errors->has('input_discount') ? '' : $order->discount) : old('input_discount') }}"
                                   placeholder="$">
                        </td>
                    </tr>

                    <tr>
                        <th>Shipping Cost ($)</th>
                        <td>
                            <input name="input_shipping_cost" id="input_shipping_cost" type="text"
                                   class="form-control{{ $errors->has('name') ? ' is-invalid' : '' }}"
                                   value="{{ empty(old('input_shipping_cost')) ? ($errors->has('input_shipping_cost') ? '' : $order->shipping_cost) : old('input_shipping_cost') }}"
                                   placeholder="$">
                        </td>
                    </tr>

                    <tr>
                        <th>Total</th>
                        <th><span id="span_total">${{ sprintf('%0.2f', $order->total) }}</span></th>
                    </tr>
                </table>
            </div>
        </div>

        <div class="row">
            <div class="col-md-12">
                <p>
                    <b>Note :</b>
                    {{ $order->note }}
                </p>
            </div>
        </div>

        <div class="row">
            <div class="col-md-12 text-right">
                <label for="notify_user" class="custom-control custom-checkbox">
                    <input type="checkbox" id="notify_user" class="custom-control-input" name="notify_user" value="1">
                    <span class="custom-control-indicator"></span>
                    <span class="custom-control-description">Notify Buyer</span>
                </label>

                <button class="btn btn-primary">Save</button>
            </div>
        </div>
    </form>

    <div class="modal fade" id="print-modal" tabindex="-1" role="dialog" aria-labelledby="modalLabelSmall"
         aria-hidden="true">
        <div class="modal-dialog modal-sm">
            <div class="modal-content">
                <div class="modal-header">
                    <h4 class="modal-title" id="modalLabelSmall">Print</h4>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">×</span>
                    </button>
                </div>

                <div class="modal-body">
                    <a class="btn btn-primary" href="{{ route('vendor_print_pdf', ['order' => $order->id]) }}" target="_blank">Print with Images</a><br><br>
                    <a class="btn btn-primary" href="{{ route('vendor_print_pdf_without_image', ['order' => $order->id]) }}" target="_blank">Print without Images</a>
                </div>
            </div>
        </div>
    </div>

    <div class="modal fade" id="password-modal" tabindex="-1" role="dialog" aria-labelledby="modalLabelSmall"
         aria-hidden="true">
        <div class="modal-dialog modal-sm">
            <div class="modal-content">
                <div class="modal-header">
                    <h4 class="modal-title" id="modalLabelSmall">Enter Password</h4>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">×</span>
                    </button>
                </div>

                <div class="modal-body">
                    <input type="password" class="form-control" id="input-password">
                </div>
                <div class="modal-footer">
                    <button class="btn  btn-primary" id="btnCheckPassword">Ok</button>
                </div>
            </div>
        </div>
    </div>
@stop

@section('additionalJS')
    <script type="text/javascript" src="{{ asset('plugins/toastr/toastr.min.js') }}"></script>
    <script>
        $(function () {
            $.ajaxSetup({
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                }
            });

            var message = '{{ session('message') }}';

            if (message != '')
                toastr.success(message);

            $('#input_discount, #input_shipping_cost').keyup(function () {
                calculate();
            });

            function calculate() {
                var discount = 0;
                var shippingCost = 0;
                var subTotal = 0;

                $('.input_amount').each(function () {
                    subTotal += parseFloat($(this).val());
                });

                var discountInput = parseFloat($('#input_discount').val());
                var shippingInput = parseFloat($('#input_shipping_cost').val());

                if (!isNaN(discountInput))
                    discount = discountInput;

                if (!isNaN(shippingInput))
                    shippingCost = shippingInput;

                var total = subTotal + shippingCost - discount;

                $('#span_total').html('$' + total.toFixed(2));
                $('#span_subtotal').html('$' + subTotal.toFixed(2));
            }

            function isFloat(n){
                return Number(n) === n && n % 1 !== 0;
            }

            $('#btnBackOrder').click(function (e) {
                e.preventDefault();
                var ids = [];

                $('.item_checkbox').each(function (i) {
                    if ($(this).is(':checked')) {
                        ids.push($(this).val());
                    }
                });

                if (ids.length > 0) {
                    $.ajax({
                        method: "POST",
                        url: "{{ route('vendor_create_back_order') }}",
                        data: {ids: ids}
                    }).done(function (data) {
                        if (data.success)
                            window.location.replace(data.url);
                    });
                }
            });

            $('#btnOutOfStock').click(function (e) {
                e.preventDefault();
                var ids = [];

                $('.item_checkbox').each(function (i) {
                    if ($(this).is(':checked')) {
                        ids.push($(this).val());
                    }
                });

                if (ids.length > 0) {
                    $.ajax({
                        method: "POST",
                        url: "{{ route('vendor_out_of_stock') }}",
                        data: {ids: ids}
                    }).done(function (msg) {
                        location.reload();
                    });
                }
            });

            $('#btnDeleteItem').click(function (e) {
                e.preventDefault();
                var ids = [];

                $('.item_checkbox').each(function (i) {
                    if ($(this).is(':checked')) {
                        ids.push($(this).val());
                    }
                });

                if (ids.length > 0) {
                    $.ajax({
                        method: "POST",
                        url: "{{ route('vendor_delete_order_item') }}",
                        data: {ids: ids}
                    }).done(function (msg) {
                        location.reload();
                    });
                }
            });

            $('#btnShowCard').click(function (e) {
                e.preventDefault();

                $('#input-password').val('');
                $('#password-modal').modal('show');
            });

            $('#btnCheckPassword').click(function () {
                var password = $('#input-password').val();
                var orderID = '{{ $order->id }}';

                $.ajax({
                    method: "POST",
                    url: "{{ route('vendor_order_check_password') }}",
                    data: {password: password, orderID: orderID}
                }).done(function (data) {
                    if (data.success) {
                        $('#password-modal').modal('hide');

                        $('#card-info-fullName').html(data.fullName);
                        $('#card-info-number').html(data.number);
                        $('#card-info-cvc').html(data.cvc);
                        $('#card-info-expire').html(data.expire);
                        $('#card-info-table').removeClass('d-none');
                        $('#btnShowCard').addClass('d-none');
                        $('#btnMask').removeClass('d-none');
                        $('#btnHideCard').removeClass('d-none');
                    } else {
                        alert('Invalid Password.');
                    }
                });
            });

            $('#btnMask').click(function (e) {
                e.preventDefault();

                var orderID = '{{ $order->id }}';

                $.ajax({
                    method: "POST",
                    url: "{{ route('vendor_order_mask_card_number') }}",
                    data: {id: orderID}
                }).done(function (data) {
                    if (data.success) {
                        $('#card-info-number').html(data.mask);
                    }
                });
            });

            $('#btnHideCard').click(function (e) {
                e.preventDefault();

                $('#card-info-fullName').html('');
                $('#card-info-number').html('');
                $('#card-info-cvc').html('');
                $('#card-info-expire').html('');
                $('#card-info-table').addClass('d-none');

                $('#btnShowCard').removeClass('d-none');
                $('#btnMask').addClass('d-none');
                $('#btnHideCard').addClass('d-none');
            });

            // Update Calculation
            $('.input_size, .input_pack, .input_unit_price').keyup(function () {
                var id = $(this).data('id');

                allSizeCalculation(id);
            });

            function allSizeCalculation(id) {
                var totalSize = 0;
                var pack = 0;
                var unitPrice = 0;

                // Size calculation
                $('.input_size_'+id).each(function () {
                    var i = 0;
                    var val = $(this).val();

                    if (isInt(val)) {
                        i = parseInt(val);

                        if (i < 0)
                            i = 0;
                    }

                    totalSize += i;
                });

                // Pack
                var val = $('.input_pack_'+id).val();
                if (isInt(val))
                    pack = parseInt(val);

                // Price
                var val = $('#input_unit_price_'+id).val();

                if (!isNaN(val) && val != '')
                    unitPrice = parseFloat(val);

                $('#total_qty_'+id).html(totalSize * pack);
                $('#amount_'+id).html('$' + (totalSize * pack * unitPrice).toFixed(2));
                $('#input_amount_'+id).val((totalSize * pack * unitPrice).toFixed(2));

                calculate();
            }

            $('.input_size').trigger('keyup');

            function isInt(value) {
                return !isNaN(value) && (function(x) { return (x | 0) === x; })(parseFloat(value))
            }
        });
    </script>
@stop