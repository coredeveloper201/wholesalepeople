@extends('admin.layouts.admin')

@section('content')
    <div class="row">
        <div class="col-md-12">
            <div class="table-responsive">
                <table class="table table-bordered">
                    <thead>
                    <tr>
                        <th>Company Name</th>
                        <th>Name</th>
                        <th>Phone</th>
                        <th># of Product</th>
                        <th># of Order</th>
                        <th>Total Sale</th>
                        <th>Action</th>
                    </tr>
                    </thead>

                    <tbody>
                    @foreach($vendors as $vendor)
                        <tr>
                            <td>
                                <a class="text-primary" href="{{ route('admin_vendor_orders', ['vendor' => $vendor->id]) }}">
                                    {{ $vendor->company_name }}
                                </a>
                            </td>
                            <td>{{ $vendor->user->first_name.' '.$vendor->user->last_name }}</td>
                            <td>{{ $vendor->billing_phone }}</td>
                            <td>{{ $vendor->items_count }}</td>
                            <td>{{ $vendor->orders_count }}</td>
                            <td>${{ number_format($vendor->sales) }}</td>
                            <td><a class="text-primary" href="{{ route('admin_vendor_orders', ['vendor' => $vendor->id]) }}">Edit</a></td>
                        </tr>
                    @endforeach
                    </tbody>
                </table>
            </div>
        </div>
    </div>
@stop