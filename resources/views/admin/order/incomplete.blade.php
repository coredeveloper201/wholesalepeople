@extends('admin.layouts.admin')

@section('content')
    <div class="row">
        <div class="col-md-3">
            <select class="form-control" id="selectBrand">
                <option value="">All Brand</option>

                @foreach($vendors as $vendor)
                    <option value="{{ $vendor->id }}" {{ request()->get('brand') == $vendor->id ? 'selected' : '' }}>{{ $vendor->company_name }}</option>
                @endforeach
            </select>
        </div>
    </div>

    <br>

    <div class="row">
        <div class="col-md-12">
            <div class="table-responsive">
                <table class="table">
                    <thead>
                    <tr>
                        <th>Last Updated</th>
                        <th>Brand</th>
                        <th>Company Name</th>
                        <th>Details</th>
                        <th>Amount</th>
                    </tr>
                    </thead>

                    <tbody>
                    @foreach($orders as $order)
                        <tr>
                            <td>{{ date('F d, Y g:i:s a', strtotime($order['updated_at'])) }}</td>
                            <td>{{ $order['vendor'] }}</td>
                            <td>{{ $order['company'] }}</td>
                            <td>
                                <a class="text-primary" href="{{ route('admin_incomplete_order_detail', ['user' => $order['user_id'], 'vendor' => $order['vendor_id']]) }}">View Detail</a>
                            </td>
                            <td>${{ sprintf('%0.2f', $order['total']) }}</td>
                        </tr>
                    @endforeach
                    </tbody>
                </table>
            </div>

            <div class="pagination">
                {{ $result->appends($appends)->links() }}
            </div>
        </div>
    </div>
@stop

@section('additionalJS')
    <script>
        $(function () {
            $('#selectBrand').change(function () {
                var id = $(this).val();

                var url = '{{ route('admin_incomplete_orders') }}';

                if (id != '')
                    url += '?brand=' + id;

                location.replace(url);
            });
        });
    </script>
@stop