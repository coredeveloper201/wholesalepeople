<?php
    use App\Enumeration\MessageTopic;
?>

@extends('admin.layouts.admin')

@section('additionalCSS')
    <link href="{{ asset('plugins/select2/css/select2.min.css') }}" rel="stylesheet">
@stop

@section('content')
    <div class="row">
        <div class="col-md-12 text-right">
            <a class="btn btn-primary" id="btnNewMsg">New Message</a>
        </div>
    </div>

    <div class="row">
        <div class="col-md-12">
            <ul class="nav nav-tabs" id="myTab" role="tablist">
                <li class="nav-item">
                    <a class="nav-link active" id="inbox-tab" data-toggle="tab" href="#inbox" role="tab" aria-controls="inbox" aria-selected="true">Inbox</a>
                </li>
                <li class="nav-item">
                    <a class="nav-link" id="outbox-tab" data-toggle="tab" href="#outbox" role="tab" aria-controls="outbox" aria-selected="false">Outbox</a>
                </li>
            </ul>

            <div class="tab-content" id="myTabContent">
                <div class="tab-pane fade show active" id="inbox" role="tabpanel" aria-labelledby="inbox-tab">
                    <br>
                    <div class="table-responsive">
                        <table class="table table-bordered">
                            <thead>
                            <tr>
                                <th>Sender</th>
                                <th>Topic</th>
                                <th>Title</th>
                                <th>Received Date</th>
                            </tr>
                            </thead>

                            <tbody>
                            @foreach($inboxes as $inbox)
                                <tr class="{{ (($inbox->isSender() == true && $inbox->sender_seen_at == null) || ($inbox->isSender() == false && $inbox->receiver_seen_at == null)) ? 'font-weight-bold' : '' }}">
                                    <td>{{ $inbox->isSender() == true ? 'Me' : $inbox->senderName() }}</td>

                                    <td>
                                        {{ $inbox->topicName() }}
                                    </td>

                                    <td>
                                        <a class="text-primary" href="{{ route('admin_view_chat', ['id' => $inbox->id]) }}">{{ $inbox->title }}</a>
                                    </td>
                                    <td>{{ date('F j, Y H:i A', strtotime($inbox->updated_at)) }}</td>
                                </tr>
                            @endforeach
                            </tbody>
                        </table>
                    </div>
                </div>

                <div class="tab-pane fade" id="outbox" role="tabpanel" aria-labelledby="outbox-tab">
                    <div class="table-responsive">
                        <table class="table table-bordered">
                            <thead>
                            <tr>
                                <th>To</th>
                                <th>Topic</th>
                                <th>Title</th>
                                <th>Send Date</th>
                            </tr>
                            </thead>

                            <tbody>
                            @foreach($outboxes as $outbox)
                                <tr>
                                    <td>
                                        {{ $outbox->receiverName() }}
                                    </td>

                                    <td>
                                        {{ $outbox->topicName() }}
                                    </td>

                                    <td><a href="{{ route('admin_view_chat', ['id' => $outbox->id]) }}">{{ $outbox->title }}</a></td>
                                    <td>{{ date('F j, Y H:i A', strtotime($outbox->updated_at)) }}</td>
                                </tr>
                            @endforeach
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <div class="modal fade" id="modalNewMsg" role="dialog" aria-labelledby="modalNewMsg">
        <div class="modal-dialog modal-lg" role="document">
            <div class="modal-content">
                <div class="modal-header bg-primary">
                    <h4 class="modal-title text-white">New Message</h4>
                </div>
                <div class="modal-body">
                    <form id="form-msg">
                        <div class="form-group row">
                            <label class="col-2 col-form-label" for="text-input">To</label>
                            <div class="col-3">
                                <select name="type" class="form-control" id="select-type">
                                    <option value="1">Customer</option>
                                    <option value="2">Vendor</option>
                                </select>
                            </div>

                            <div class="col-7">
                                <select name="customers[]" class="form-control" id="select-customer" multiple="multiple" style="width: 100%">
                                    @foreach($customers as $customer)
                                        <option value="{{ $customer->id }}">{{ $customer->name }}</option>
                                    @endforeach
                                </select>

                                <select name="vendors[]" class="form-control" id="select-vendor" multiple="multiple" style="width: 100%">
                                    @foreach($vendors as $vendor)
                                        <option value="{{ $vendor->id }}">{{ $vendor->company_name }}</option>
                                    @endforeach
                                </select>
                            </div>
                        </div>

                        <div class="form-group row">
                            <label class="col-2 col-form-label" for="text-input">Topics</label>
                            <div class="col-3">
                                <select name="topic" class="form-control" id="select-topic">
                                    <option value="{{ MessageTopic::$GENERAL }}">General</option>
                                    <option value="{{ MessageTopic::$PRODUCT }}">Product</option>
                                    <option value="{{ MessageTopic::$ORDER }}">Order</option>
                                    <option value="{{ MessageTopic::$PAYMENT }}">Payment</option>
                                    <option value="{{ MessageTopic::$SHIPMENT }}">Shipment</option>
                                    <option value="{{ MessageTopic::$RETURN }}">Return</option>
                                    <option value="{{ MessageTopic::$OTHER }}">Other</option>
                                </select>
                            </div>
                        </div>

                        <div class="form-group row">
                            <label class="col-2 col-form-label" for="text-input">Title</label>
                            <div class="col-10">
                                <input name="title" class="form-control" type="text" id="input-title">
                            </div>
                        </div>

                        <div class="form-group row">
                            <label class="col-2 col-form-label" for="text-input">Message</label>
                            <div class="col-10">
                                <textarea name="message" class="form-control" id="input-message"></textarea>
                            </div>
                        </div>

                        <div class="form-group row">
                            <label class="col-2 col-form-label" for="text-input">Attach Files</label>
                            <div class="col-9" id="input-files">
                                <input name="attachments[]" class="input-file form-control" type="file">
                            </div>

                            <div class="col-1" id="input-files-delete">
                                <a class="text-danger btn-delete-file input-file form-control">X</a>
                            </div>
                        </div>
                    </form>
                </div>
                <div class="modal-footer">
                    <button class="btn  btn-default" data-dismiss="modal">Close</button>
                    <button class="btn  btn-primary" id="btn-send-message">Send</button>
                </div>
            </div>
        </div>
        <!--- end modals-->
    </div>
@stop

@section('additionalJS')
    <script type="text/javascript" src="{{ asset('plugins/select2/js/select2.min.js') }}"></script>
    <script>
        $(function () {
            $.ajaxSetup({
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                }
            });

            var $customerSelect = $('#select-customer').select2({
                dropdownParent: $("#modalNewMsg")
            });

            var $vendorSelect = $('#select-vendor').select2({
                dropdownParent: $("#modalNewMsg")
            });

            $('#btnNewMsg').click(function () {
                $('#modalNewMsg').modal('show');
            });

            $('#select-type').change(function () {
                var val = $(this).val();

                if (val == 2) {
                    $customerSelect.select2("destroy");
                    $('#select-customer').hide();

                    $vendorSelect.select2({
                        dropdownParent: $("#modalNewMsg")
                    });
                    $('#select-vendor').show();
                } else {
                    $vendorSelect.select2("destroy");
                    $('#select-vendor').hide();

                    $customerSelect.select2({
                        dropdownParent: $("#modalNewMsg")
                    });
                    $('#select-customer').show();
                }
            });

            $('#select-type').trigger('change');

            $(document).on('change', '.input-file', function () {
                if ($(this).val() != '') {
                    $('#input-files').append('<input name="attachments[]" class="input-file form-control" type="file">');
                    $('#input-files-delete').append('<a class="text-danger btn-delete-file input-file form-control">X</a>');
                }
            });

            $(document).on('click', '.btn-delete-file', function () {
                var index = $('.btn-delete-file').index($(this));

                $('.input-file:eq('+index+')').val('');
            });

            $('#btn-send-message').click(function () {
                if ($('#select-type').val() == '1' && $('#select-customer').val().length == 0) {
                    alert('Select Customer');
                    return;
                }

                if ($('#select-type').val() == '2' && $('#select-vendor').val().length == 0) {
                    alert('Select Vendor');
                    return;
                }

                if ($('#input-title').val() == '') {
                    alert('Enter title');
                    return;
                }

                if ($('#input-message').val() == '') {
                    alert('Enter message');
                    return;
                }

                $('#form-msg').submit();
            });

            $("#form-msg").submit(function(e) {
                e.preventDefault();
                var formData = new FormData(this);

                $.ajax({
                    url: "{{ route('admin_add_message_parent') }}",
                    type: 'POST',
                    data: formData,
                    success: function (data) {
                        location.reload();
                    },
                    cache: false,
                    contentType: false,
                    processData: false
                });
            });
        })
    </script>
@stop