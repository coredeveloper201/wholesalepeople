@extends('admin.layouts.admin')

@section('additionalCSS')
    <link href="{{ asset('themes/admire/css/widgets.css') }}" rel="stylesheet">
@stop

@section('content')
    <div class="col-md-6">
        <a href="{{ route('admin_view_messages') }}" class="btn btn-primary">Back</a>
    </div>

    <div class="row">
        <div class="col-md-8">
            <div class="chat-conversation p-d-10 m-t-25">
                <ul class="conversation-list">
                    @foreach($message->items as $item)
                        <li class="clearfix {{ (($isSender == true && $item->sender == 1) || ($isSender == false && $item->sender == 0)) ? 'odd' : '' }} m-b-20">
                            <div class="chat-avatar">
                                <img src="{{ asset('images/default-avatar.png') }}">
                            </div>
                            <div class="conversation-text">
                                <div class="ctext-wrap">
                                    <i>{{ (($isSender == true && $item->sender == 1) || ($isSender == false && $item->sender == 0)) ? 'Me' : $personName }}</i>
                                    <p>
                                        {!! nl2br($item->message) !!}

                                        @if (sizeof($item->files) > 0)
                                            <br>
                                            @foreach($item->files as $file)
                                                <a class="text-primary" href="{{ route('get_message_file', ['name' => $file->filename]) }}" target="_blank">{{ $file->original_filename }}</a>

                                                @if (!$loop->last)
                                                    <br>
                                                @endif
                                            @endforeach
                                        @endif
                                    </p>

                                    <span class="text-muted">{{ date('F j, Y H:i A', strtotime($item->created_at)) }}</span>
                                </div>
                            </div>
                        </li>
                    @endforeach
                </ul>


                <div class="row">
                    <div class="col-12 m-b-15">
                        <form id="form-msg">
                            <input type="hidden" name="id" value="{{ $message->id }}">
                            <div class="input-group chat_btn">
                                <textarea class="form-control chat-input custom_textbox"
                                          id="input-msg" placeholder="Enter your text" name="msg"></textarea>

                                <span class="input-group-btn">
                                    <button class="btn btn-primary waves-effect waves-light"
                                            id="btn-send"><i class="fa fa-paper-plane text-white"
                                                             aria-hidden="true"></i></button>
                                </span>
                            </div>


                            <br>

                            <div class="form-group row">
                                <label class="col-2 col-form-label" for="text-input">Attach Files</label>
                                <div class="col-9" id="input-files">
                                    <input name="attachments[]" class="input-file form-control" type="file">
                                </div>

                                <div class="col-1" id="input-files-delete">
                                    <a class="text-danger btn-delete-file input-file form-control">X</a>
                                </div>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>

        <div class="col-md-4">
            <table class="table table-bordered">
                <tr>
                    <th>Sender</th>
                    <td>{{ $message->isSender() == true ? 'Me' : $message->senderName() }}</td>
                </tr>

                <tr>
                    <th>Receiver</th>
                    <td>{{ $message->isSender() == true ? $message->receiverName() : 'Me' }}</td>
                </tr>

                <tr>
                    <th>Title</th>
                    <td>{{ $message->title }}</td>
                </tr>

                <tr>
                    <th>Topic</th>
                    <td>{{ $message->topicName() }}</td>
                </tr>
            </table>
        </div>
    </div>
@stop

@section('additionalJS')
    <script type="text/javascript" src="{{ asset('plugins/slimscroll/js/jquery.slimscroll.min.js') }}"></script>
    <script>
        $(function () {
            $.ajaxSetup({
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                }
            });

            $(".conversation-list").slimscroll({
                height: "500px",
                size: '5px',
                opacity: 0.2
            });

            $(document).on('change', '.input-file', function () {
                if ($(this).val() != '') {
                    $('#input-files').append('<input name="attachments[]" class="input-file form-control" type="file">');
                    $('#input-files-delete').append('<a class="text-danger btn-delete-file input-file form-control">X</a>');
                }
            });

            $(document).on('click', '.btn-delete-file', function () {
                var index = $('.btn-delete-file').index($(this));

                $('.input-file:eq('+index+')').val('');
            });

            $('#btn-send').click(function (e) {
                e.preventDefault();
                if ($('#input-msg').val() != '') {
                    var text = $('#input-msg').val();

                    $('#form-msg').submit();
                    /* $.ajax({
                         method: "POST",
                         url: "{{ route('vendor_add_message') }}",
                        data: { id: '{{ $message->id }}', msg: text }
                    }).done(function( data ) {
                        if (data.success) {
                            location.reload();
                            /!*$('#input-msg').val('');
                            var scrollTo_int = $(".conversation-list").prop('scrollHeight') + 'px';
                            $(".conversation-list").append('<li class="clearfix odd m-t-25"><div class="chat-avatar"><img src="{{ asset('images/default-avatar.png') }}"></div><div class="conversation-text"><div class="ctext-wrap"><i>Me</i><p>' + text + '</p><span class="text-muted">'+data.message+'</span></div></div></li>');
                            $(".conversation-list").slimscroll({ scrollTo: scrollTo_int });*!/
                        } else {
                            alert('Cannot post message');
                        }
                    });*/
                }
            });

            $("#form-msg").submit(function(e) {
                e.preventDefault();
                var formData = new FormData(this);

                $.ajax({
                    url: "{{ route('admin_add_message') }}",
                    type: 'POST',
                    data: formData,
                    success: function (data) {
                        location.reload();
                    },
                    cache: false,
                    contentType: false,
                    processData: false
                });
            });

            var scrollTo_int = $(".conversation-list").prop('scrollHeight') + 'px';
            $(".conversation-list").slimscroll({ scrollTo: scrollTo_int });
        });
    </script>
@stop