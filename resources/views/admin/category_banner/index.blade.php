<?php use App\Enumeration\CategoryBannerType; ?>
@extends('admin.layouts.admin')

@section('content')
    <h4><b>{{ $title }}</b></h4>
    <hr>
    <br>

    <div class="row">
        <div class="col-md-6">
            <h4>Top Banner</h4>
        </div>

        <div class="col-md-6 text-right">
            <button class="btn btn-primary btnAdd" data-type="{{ CategoryBannerType::$TOP_BANNER }}">Add Brand</button>
        </div>
    </div>

    <div class="table-responsive">
        <table class="table table-bordered">
            <thead>
            <tr>
                <th>Brand Name</th>
                <th>Action</th>
            </tr>
            </thead>

            <tbody>
            @foreach($topBanners as $item)
                <tr>
                    <td>{{ $item->vendor->company_name }}</td>
                    <td>
                        <a class="btnDelete" data-id="{{ $item->id }}" role="button" style="color: red">Delete</a>
                    </td>
                </tr>
            @endforeach
            </tbody>
        </table>
    </div>

    @if ($title != 'Vendor All')
        <br>

        <div class="row">
            <div class="col-md-6">
                <h4>Top Slider</h4>
            </div>

            <div class="col-md-6 text-right">
                <button class="btn btn-primary btnAdd" data-type="{{ CategoryBannerType::$TOP_SLIDER }}">Add Brand</button>
            </div>
        </div>

        <div class="table-responsive">
            <table class="table table-bordered">
                <thead>
                <tr>
                    <th>Brand Name</th>
                    <th>Action</th>
                </tr>
                </thead>

                <tbody>
                @foreach($topItems as $item)
                    <tr>
                        <td>{{ $item->vendor->company_name }}</td>
                        <td>
                            <a class="btnDelete" data-id="{{ $item->id }}" role="button" style="color: red">Delete</a>
                        </td>
                    </tr>
                @endforeach
                </tbody>
            </table>
        </div>

        <br>

        <div class="row">
            <div class="col-md-6">
                <h4>Small Banner</h4>
            </div>

            <div class="col-md-6 text-right">
                <button class="btn btn-primary btnAdd" data-type="{{ CategoryBannerType::$SMALL_BANNER }}">Add Brand</button>
            </div>
        </div>

        <div class="table-responsive">
            <table class="table table-bordered">
                <thead>
                <tr>
                    <th>Brand Name</th>
                    <th>Action</th>
                </tr>
                </thead>

                <tbody>
                @foreach($bannerItems as $item)
                    <tr>
                        <td>{{ $item->vendor->company_name }}</td>
                        <td>
                            <a class="btnDelete" data-id="{{ $item->id }}" role="button" style="color: red">Delete</a>
                        </td>
                    </tr>
                @endforeach
                </tbody>
            </table>
        </div>

        <br>

        <div class="row">
            <div class="col-md-6">
                <h4>2nd Slider</h4>
            </div>

            <div class="col-md-6 text-right">
                <button class="btn btn-primary btnAdd" data-type="{{ CategoryBannerType::$SECOND_SLIDER }}">Add Brand</button>
            </div>
        </div>

        <div class="table-responsive">
            <table class="table table-bordered">
                <thead>
                <tr>
                    <th>Brand Name</th>
                    <th>Action</th>
                </tr>
                </thead>

                <tbody>
                @foreach($secondSliderItems as $item)
                    <tr>
                        <td>{{ $item->vendor->company_name }}</td>
                        <td>
                            <a class="btnDelete" data-id="{{ $item->id }}" role="button" style="color: red">Delete</a>
                        </td>
                    </tr>
                @endforeach
                </tbody>
            </table>
        </div>
    @endif

    <div class="modal fade" id="vendor-modal" tabindex="-1" role="dialog" aria-labelledby="modalLabelSmall"
         aria-hidden="true">
        <div class="modal-dialog modal-sm">
            <div class="modal-content">
                <div class="modal-header">
                    <h4 class="modal-title" id="modalLabelSmall">Select Brand</h4>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">×</span>
                    </button>
                </div>

                <div class="modal-body">
                    <select class="form-control" id="select_vendor">
                        <option value="">Select Brand</option>
                        @foreach($vendors as $vendor)
                            <option value="{{ $vendor->id }}">{{ $vendor->company_name }}</option>
                        @endforeach
                    </select>
                </div>

                <div class="modal-footer">
                    <button class="btn btn-primary" id="btnSave">Add</button>
                </div>
            </div>
        </div>
    </div>

    <div class="modal fade" id="deleteModal" role="dialog" aria-labelledby="deleteModal">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-header bg-danger">
                    <h4 class="modal-title text-white" id="deleteModal">Delete</h4>
                </div>
                <div class="modal-body">
                    <p>
                        Are you sure want to delete?
                    </p>
                </div>
                <div class="modal-footer">
                    <button class="btn  btn-default" data-dismiss="modal">Close</button>
                    <button class="btn  btn-danger" id="modalBtnDelete">Delete</button>
                </div>
            </div>
        </div>
        <!--- end modals-->
    </div>
@stop

@section('additionalJS')
    <script>
        $(function () {
            $.ajaxSetup({
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                }
            });

            var type, selectedID = '';

            $('.btnAdd').click(function () {
                type = $(this).data('type');
                $('#select_vendor').val('');
                $('#vendor-modal').modal('show');
            });

            $('#btnSave').click(function () {
                var vendorId = $('#select_vendor').val();
                var categoryId = '{{ $category->id or 0 }}';

                @if ($title == 'Vendor All')
                    categoryId = '-1';
                @endif

                if (vendorId == '') {
                    alert('Select a brand');
                    return;
                }

                $.ajax({
                    method: "POST",
                    url: "{{ route('admin_category_banner_add') }}",
                    data: { type: type, categoryId: categoryId, vendorId: vendorId }
                }).done(function( data ) {
                    if (data.success)
                        location.reload();
                    else
                        alert(data.message);
                });
            });

            $('.btnDelete').click(function () {
                selectedID = $(this).data('id');
                $('#deleteModal').modal('show');
            });

            $('#modalBtnDelete').click(function () {
                $.ajax({
                    method: "POST",
                    url: "{{ route('admin_category_banner_delete') }}",
                    data: { id: selectedID }
                }).done(function( msg ) {
                    location.reload();
                });
            });
        });
    </script>
@stop