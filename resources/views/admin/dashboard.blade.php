@extends('admin.layouts.admin')

@section('additionalCSS')
    <link href="{{ asset('themes/admire/css/widgets.css') }}" rel="stylesheet">
@stop

@section('content')
    <div class="row">
        <div class="col-md-8">
            <h4>My Performance</h4>
        </div>

        <div class="col-md-4 text-right">
            <span class="text-muted">Last 30 days</span>
        </div>
    </div>

    <br>
    <hr>

    <div class="row sales_section">
        <div class="col-xl col-sm-6 col-12">
            <div class="card p-d-15">
                <div class="sales_icons">
                    <span class="bg-info"></span>
                    <i class="fa fa-eye"></i>
                </div>
                <div>
                    <h5 class="sales_orders text-right m-t-5">Vendor Home Page Visitor</h5>
                    <h1 class="sales_number m-t-15 text-right">{{ number_format($data['homePageVisitor']) }}</h1>
                </div>
            </div>
        </div>

        <div class="col-xl col-sm-6 col-12 media_max_573">
            <div class="card p-d-15">
                <div class="sales_icons">
                    <span class="bg-danger"></span>
                    <i class="fa fa-eye"></i>
                </div>
                <div>
                    <h5 class="sales_orders text-right m-t-5">Vendor Home Page Visitor (Unique)</h5>
                    <h1 class="sales_number m-t-15 text-right">{{ number_format($data['homePageVisitorUnique']) }}</h1>
                </div>
            </div>
        </div>
        <div class="col-xl col-sm-6 col-12 media_max_1199">
            <div class="card p-d-15">
                <div class="sales_icons">
                    <span class="bg-primary"></span>
                    <i class="fa fa-cart-plus"></i>
                </div>
                <div>
                    <h5 class="sales_orders text-right m-t-5">Total Sale Amount</h5>
                    <h1 class="sales_number m-t-15 text-right">${{ number_format($data['totalSaleAmount'], 2, '.', ',') }}</h1>
                </div>
            </div>
        </div>
        <div class="col-xl col-sm-6 col-12 media_max_1199">
            <div class="card p-d-15">
                <div class="sales_icons">
                    <span class="bg-warning"></span>
                    <i class="fa fa-dollar"></i>
                </div>
                <div>
                    <h5 class="sales_orders text-right m-t-5">Total Pending Order</h5>
                    <h1 class="sales_number m-t-15 text-right">${{ number_format($data['totalPendingOrder'], 2, '.', ',') }}</h1>
                </div>
            </div>
        </div>
    </div>

    <br>

    <h4>Additional Stats</h4>
    <br>
    <hr>

    <div class="row">
        <div class="col-sm-2 col-12 col-lg">
            <div class="widget_icon_bgclr icon_align bg-white section_border">
                <div class="bg_icon float-left">
                    <i class="fa fa-dollar" aria-hidden="true"></i>
                </div>
                <div class="text-right">
                    <h3 id="widget_count1">${{ number_format($data['todayOrderAmount'], 2, '.', ',') }}</h3>
                    <p>Today Order Amount</p>
                </div>
            </div>
        </div>

        <div class="col-sm-2 col-12 col-lg media_max_991">
            <div class="widget_icon_bgclr icon_align bg-white section_border">
                <div class="bg_icon float-left">
                    <i class="fa fa-eye" aria-hidden="true"></i>
                </div>
                <div class="text-right">
                    <h3 id="widget_count3">{{ number_format($data['yesterdayVisitor']) }}</h3>
                    <p>Yesterday Vendor Home Visitor</p>
                </div>
            </div>
        </div>

        <div class="col-sm-2 col-12 col-lg media_max_991">
            <div class="widget_icon_bgclr icon_align bg-white section_border">
                <div class="bg_icon float-left">
                    <i class="fa fa-dollar" aria-hidden="true"></i>
                </div>
                <div class="text-right">
                    <h3 id="widget_count4">${{ number_format($data['yesterdayOrderAmount'], 2, '.', ',') }}</h3>
                    <p>Yesterday Order Amount</p>
                </div>
            </div>
        </div>

        <div class="col-sm-2 col-12 col-lg media_max_991">
            <div class="widget_icon_bgclr icon_align bg-white section_border">
                <div class="bg_icon float-left">
                    <i class="fa fa-upload" aria-hidden="true"></i>
                </div>
                <div class="text-right">
                    <h3 id="widget_count4">{{ number_format($data['itemUploadedThisMonth']) }}</h3>
                    <p>Item Uploaded (This Month)</p>
                </div>
            </div>
        </div>

        <div class="col-sm-2 col-12 col-lg media_max_991">
            <div class="widget_icon_bgclr icon_align bg-white section_border">
                <div class="bg_icon float-left">
                    <i class="fa fa-upload" aria-hidden="true"></i>
                </div>
                <div class="text-right">
                    <h3 id="widget_count4">{{ number_format($data['itemUploadedLastMonth']) }}</h3>
                    <p>Item Uploaded (Last Month)</p>
                </div>
            </div>
        </div>
    </div>

    <br>
    <h4>Top 6 Styles</h4>
    <br>
    <hr>

    <div class="row">
        <div class="col-md-12">
            <ul class="dashboard-top-6">
                @foreach($bestItems as $item)
                    <li style="">
                        <a href="{{ route('vendor_edit_item', ['item' => $item->id]) }}">
                            <img src="{{ $item->image_path }}" alt="">
                        </a>
                        <div class="text-center">
                            <a href="{{ route('vendor_edit_item', ['item' => $item->id]) }}" class="text-primary">{{ $item->style_no }}</a>
                        </div>
                    </li>
                @endforeach
            </ul>
        </div>
    </div>

    <br>
    <div class="row">
        <div class="col-md-6">
            <h4>Total Order Count by Month</h4>
            <br>
            <hr>

            <canvas id="orderCount" height="200"></canvas>
        </div>

        <div class="col-md-6">
            <h4>Item Uploaded by Month</h4>
            <br>
            <hr>

            <canvas id="myChart2" height="200"></canvas>
        </div>
    </div>

    <br>
    <div class="row">
        <div class="col-md-12">
            <br>
            <h4>Today's Orders</h4>
            <br>
            <hr>
        </div>

        <div class="col-md-12">
            <table class="table table-bordered">
                <thead>
                <tr>
                    <th>Order Date</th>
                    <th>Vendor Name</th>
                    <th class="text-center">Phone</th>
                    <th class="text-center">Total Order</th>
                    <th class="text-right">Total Amount</th>
                </tr>
                </thead>

                <tbody>
                @foreach($todayOrders as $vendor)
                    <tr>
                        <td>{{date('m/d/Y')}}</td>
                        <td>
                            <a class="text-primary" href="{{ route('admin_vendor_orders', ['vendor' => $vendor['id']]) }}">
                                {{ $vendor['company_name'] }}
                            </a>
                        </td>
                        <td class="text-center">{{ $vendor['phone'] }}</td>
                        <td class="text-center">{{ $vendor['totalOrder'] }}</td>
                        <td class="text-right"><strong>${{ $vendor['totalOrderAmount'] }}</strong></td>
                    </tr>
                @endforeach
                </tbody>
            </table>
        </div>
    </div>
@stop


@section('additionalJS')
    <script src="{{ asset('plugins/Chart.js/Chart.min.js') }}"></script>
    <script>
        var orderCountLabel = <?php echo $data['orderCountLabel']; ?>;
        var orderCount = <?php echo $data['orderCount']; ?>;
        var returnOrder = <?php echo $data['returnOrder']; ?>;
        var uploadCount = <?php echo $data['uploadCount']; ?>;

        var chartOrderCount = document.getElementById("orderCount").getContext('2d');
        var chartOrderCount = new Chart(chartOrderCount, {
            type: 'bar',
            data: {
                labels: orderCountLabel,
                datasets: [{
                    label: 'New',
                    data: orderCount,
                    backgroundColor: 'rgba(255, 99, 132, 0.2)',
                    borderColor:  'rgba(255,99,132,1)',
                    borderWidth: 1
                },
                    {
                        label: 'Return',
                        data: returnOrder,
                        backgroundColor: 'rgba(255, 206, 86, 0.2)',
                        borderColor: 'rgba(255, 206, 86, 1)',
                        borderWidth: 1
                    }]
            },
            options: {
                scales: {
                    yAxes: [{
                        ticks: {
                            beginAtZero:true,
                            max: Math.max(Math.max(...orderCount) + 5, Math.max(...returnOrder) + 5),
        }
        }]
        }
        }
        });

        var ctx = document.getElementById("myChart2").getContext('2d');
        var myChart = new Chart(ctx, {
            type: 'bar',
            data: {
                labels: orderCountLabel,
                datasets: [{
                    data: uploadCount,
                    backgroundColor: 'rgba(255, 99, 132, 0.2)',
                    borderColor:  'rgba(255,99,132,1)',
                    borderWidth: 1
                }]
            },
            options: {
                legend: {
                    display: false
                },
                scales: {
                    yAxes: [{
                        ticks: {
                            beginAtZero:true,
                            max: Math.max(...uploadCount) + 5
        }
        }]
        }
        }
        });
    </script>
@stop