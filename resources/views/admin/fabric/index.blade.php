@extends('admin.layouts.admin')

@section('additionalCSS')
    <link href="{{ asset('plugins/toastr/toastr.min.css') }}" rel="stylesheet">
@stop

@section('content')
    <div class="row {{ ($errors && sizeof($errors) > 0) ? 'd-none' : '' }}" id="addBtnRow">
        <div class="col-md-2">
            <select class="form-control" id="select_brand">
                <option value="">All Brand</option>
                @foreach($vendors as $vendor)
                    <option value="{{ $vendor->id }}" {{ (request()->get('id') == $vendor->id) ? 'selected' : '' }}>{{ $vendor->company_name }}</option>
                @endforeach
            </select>
        </div>

        <div class="col-md-10">
            <button class="btn btn-primary" id="btnAddNewFabric">Add New Fabric</button>
        </div>
    </div>

    <div class="row {{ ($errors && sizeof($errors) > 0) ? '' : 'd-none' }}" id="addEditRowFabric">
        <div class="col-md-12" style="border: 1px solid black">
            <h3><span id="addEditTitleFabric"></span></h3>

            <form class="form-horizontal" id="form" method="post" action="{{ (old('inputAdd') == '1') ? route('admin_fabric_add') : route('admin_fabric_update') }}">
                @csrf

                <input type="hidden" name="inputAdd" id="inputAdd" value="{{ old('inputAdd') }}">
                <input type="hidden" name="fabricId" id="fabricId" value="{{ old('fabricId') }}">

                <div class="form-group row">
                    <div class="col-lg-2">
                        <label for="status" class="col-form-label">Status</label>
                    </div>

                    <div class="col-lg-5">
                        <label for="statusActiveFabric" class="custom-control custom-radio">
                            <input id="statusActiveFabric" name="statusFabric" type="radio" class="custom-control-input"
                                   value="1" {{ (old('statusFabric') == '1' || empty(old('statusFabric'))) ? 'checked' : '' }}>
                            <span class="custom-control-indicator"></span>
                            <span class="custom-control-description">Active</span>
                        </label>
                        <label for="statusInactiveFabric" class="custom-control custom-radio signin_radio4">
                            <input id="statusInactiveFabric" name="statusFabric"
                                   type="radio" class="custom-control-input" value="0" {{ old('statusFabric') == '0' ? 'checked' : '' }}>
                            <span class="custom-control-indicator"></span>
                            <span class="custom-control-description">Inactive</span>
                        </label>
                    </div>
                </div>

                <div class="form-group row">
                    <div class="col-lg-2">
                        <label for="vendor_id" class="col-form-label">Brand *</label>
                    </div>

                    <div class="col-lg-5">
                        <select class="form-control{{ $errors->has('vendor_id') ? ' is-invalid' : '' }}" name="vendor_id" id="vendor_id">
                            <option value="">Select Brand</option>

                            @foreach($vendors as $vendor)
                                <option value="{{ $vendor->id }}" {{ old('vendor_id') == $vendor->id ? 'selected' : '' }}>{{ $vendor->company_name }}</option>
                            @endforeach
                        </select>
                    </div>
                </div>

                <div class="form-group row">
                    <div class="col-lg-2">
                        <label for="master_fabric" class="col-form-label">Master Fabric *</label>
                    </div>

                    <div class="col-lg-5">
                        <select id="master_fabric" class="form-control{{ $errors->has('master_fabric') ? ' is-invalid' : '' }}" name="master_fabric">
                            <option value="">Select Master Fabric</option>

                            @foreach($masterFabrics as $fabric)
                                <option value="{{ $fabric->id }}" {{ old('master_fabric') == $fabric->id ? 'selected' : '' }}>{{ $fabric->name }}</option>
                            @endforeach
                        </select>
                    </div>
                </div>

                <div class="form-group row">
                    <div class="col-lg-2">
                        <label for="fabric_description" class="col-form-label">Fabric Description *</label>
                    </div>

                    <div class="col-lg-5">
                        <input type="text" id="fabric_description" class="form-control{{ $errors->has('fabric_description') ? ' is-invalid' : '' }}"
                               name="fabric_description" value="{{ old('fabric_description') }}">
                    </div>
                </div>

                <div class="form-group row">
                    <div class="col-lg-1">
                        <label for="defaultFabric" class="col-form-label">Default</label>
                    </div>

                    <div class="col-lg-5">
                        <label class="custom-control custom-checkbox">
                            <input type="checkbox" class="custom-control-input" id="defaultFabric" value="1" name="defaultFabric" {{ old('defaultFabric') ? 'checked' : '' }}>
                            <span class="custom-control-indicator"></span>
                        </label>
                    </div>
                </div>

                <div class="form-group row">
                    <div class="col-lg-12 text-right">
                        <button class="btn btn-default" id="btnCancelFabric">Cancel</button>
                        <input type="submit" id="btnSubmit" class="btn btn-primary" value="{{ old('inputAdd') == '0' ? 'Update' : 'Add' }}">
                    </div>
                </div>
            </form>
        </div>
    </div>

    <br>

    <table class="table table-striped">
        <thead>
        <tr>
            <th>Brand</th>
            <th>Fabric Description</th>
            <th>Master Fabric</th>
            <th>Active</th>
            <th>Action</th>
        </tr>
        </thead>

        <tbody id="fabricTbody">
        @foreach($fabrics as $fabric)
            <tr>
                <td>{{ $fabric->vendor->company_name }}</td>
                <td><span class="fabricName">{{ $fabric->name }}</span></td>
                <td><span class="masterFabricName">{{ $fabric->masterFabric->name }}</span></td>
                <td>
                    <label class="custom-control custom-checkbox">
                        <input type="checkbox" data-id="{{ $fabric->id }}" class="custom-control-input statusFabric"
                               value="1" {{ $fabric->status == 1 ? 'checked' : '' }}>
                        <span class="custom-control-indicator"></span>
                    </label>
                </td>
                <td>
                    <a class="btnEdit" data-id="{{ $fabric->id }}" data-index="{{ $loop->index }}" role="button" style="color: blue">Edit</a> |
                    <a class="btnDelete" data-id="{{ $fabric->id }}" data-index="{{ $loop->index }}" role="button" style="color: red">Delete</a>
                </td>
            </tr>
        @endforeach
        </tbody>
    </table>

    {{ $fabrics->appends(['id' => request()->get('id')])->links() }}

    <template id="fabricTrTemplate">
        <tr>
            <td><span class="fabricIndex"></span></td>
            <td><span class="fabricName"></span></td>
            <td><span class="masterFabricName"></span></td>
            <td>
                <label class="custom-control custom-checkbox">
                    <input type="checkbox" class="custom-control-input statusFabric"
                           value="1">
                    <span class="custom-control-indicator"></span>
                </label>
            </td>
            <td>
                <label class="custom-control custom-radio">
                    <input type="radio" name="defaultFabricTable" class="custom-control-input defaultFabric"
                           value="1">
                    <span class="custom-control-indicator"></span>
                </label>
            </td>
            <td>
                <a class="btnEditFabric" role="button" style="color: blue">Edit</a> |
                <a class="btnDeleteFabric" role="button" style="color: red">Delete</a>
            </td>
        </tr>
    </template>

    <div class="modal fade" id="deleteModalFabric" role="dialog" aria-labelledby="deleteModalFabric">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-header bg-danger">
                    <h4 class="modal-title text-white" id="deleteModalFabric">Delete</h4>
                </div>
                <div class="modal-body">
                    <p>
                        Are you sure want to delete?
                    </p>
                </div>
                <div class="modal-footer">
                    <button class="btn  btn-default" data-dismiss="modal">Close</button>
                    <button class="btn  btn-danger" id="modalBtnDeleteFabric">Delete</button>
                </div>
            </div>
        </div>
        <!--- end modals-->
    </div>
@stop

@section('additionalJS')
    <script type="text/javascript" src="{{ asset('plugins/toastr/toastr.min.js') }}"></script>
    <script>
        $(function () {
            $.ajaxSetup({
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                }
            });

            var fabrics = <?php echo json_encode($fabrics->toArray()); ?>;
            fabrics = fabrics.data;
            var selectedFabricId;

            var message = '{{ session('message') }}';

            if (message != '')
                toastr.success(message);

            $('#select_brand').change(function () {
                var tmp = '';

                if ($(this).val() != '')
                    tmp = '?id='+$(this).val();

                window.location.replace('{{ route('admin_fabric') }}'+tmp);
            });

            $('#btnAddNewFabric').click(function () {
                $('#addEditRowFabric').removeClass('d-none');
                $('#addBtnRow').addClass('d-none');
                $('#addEditTitleFabric').html('Add a New Fabric');

                $('#btnSubmit').val('Add');
                $('#inputAdd').val('1');
                $('#form').attr('action', '{{ route('admin_fabric_add') }}');
            });

            $('#btnCancelFabric').click(function (e) {
                e.preventDefault();

                $('#addEditRowFabric').addClass('d-none');
                $('#addBtnRow').removeClass('d-none');

                // Clear form
                $('#statusActiveFabric').prop('checked', true);
                $('#master_fabric').val('');
                $('#fabric_description').val('');
                $('#vendor_id').val('');
                $('#defaultFabric').prop('checked', false);

                $('#fabric_description').removeClass('is-invalid');
                $('#master_fabric').removeClass('is-invalid');
                $('#vendor_id').removeClass('is-invalid');
            });

            $('.btnEdit').click(function () {
                var id = $(this).data('id');
                var index = $(this).data('index');

                $('#addEditRowFabric').removeClass('d-none');
                $('#addBtnRow').addClass('d-none');
                $('#addEditTitleFabric').html('Edit Fabric');
                $('#btnSubmit').val('Update');
                $('#inputAdd').val('0');
                $('#form').attr('action', '{{ route('admin_fabric_update') }}');
                $('#fabricId').val(id);

                var fabric = fabrics[index];

                if (fabric.status == 1)
                    $('#statusActiveFabric').prop('checked', true);
                else
                    $('#statusInactiveFabric').prop('checked', true);

                if (fabric.default == 1)
                    $('#defaultFabric').prop('checked', true);
                else
                    $('#defaultFabric').prop('checked', false);

                $('#fabric_description').val(fabric.name);
                $('#master_fabric').val(fabric.master_fabric_id);
                $('#vendor_id').val(fabric.vendor_meta_id);
            });

            $('.btnDelete').click(function () {
                $('#deleteModalFabric').modal('show');
                selectedFabricId = $(this).data('id');
            });

            $('#modalBtnDeleteFabric').click(function () {
                $.ajax({
                    method: "POST",
                    url: "{{ route('admin_fabric_delete') }}",
                    data: { id: selectedFabricId }
                }).done(function( msg ) {
                    location.reload();
                });
            });

            $('.statusFabric').change(function () {
                var status = 0;
                var id = $(this).data('id');

                if ($(this).is(':checked'))
                    status = 1;

                $.ajax({
                    method: "POST",
                    url: "{{ route('admin_fabric_change_status') }}",
                    data: { id: id, status: status }
                }).done(function( msg ) {
                    toastr.success('Status Updated!');
                });
            });
        });
    </script>
@stop