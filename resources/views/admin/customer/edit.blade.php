@extends('admin.layouts.admin')

@section('additionalCSS')
    <style>
        .required {
            color: red;
        }

        .form-group label {
            padding-left: 0px !important;
        }
    </style>
@stop

@section('content')
    <form action="{{ route('admin_buyer_edit_post', ['buyer' => $buyer->id]) }}" method="POST">
        @csrf
        <h6 class="text-muted text-normal">Customer Information</h6>
        <hr class="margin-bottom-1x">

        <div class="row">
            <div class="col-md-6">
                <div class="form-group">
                    <label for="small-rounded-input">First Name <span class="required">*</span></label>
                    <input class="form-control{{ $errors->has('firstName') ? ' is-invalid' : '' }}"
                           type="text" id="firstName" name="firstName"
                           value="{{ empty(old('firstName')) ? ($errors->has('firstName') ? '' : $buyer->user->first_name) : old('firstName') }}">

                    @if ($errors->has('firstName'))
                        <div class="form-control-feedback">{{ $errors->first('firstName') }}</div>
                    @endif
                </div>

                <div class="form-group">
                    <label for="small-rounded-input">Email <span class="required">*</span></label>
                    <input class="form-control{{ $errors->has('email') ? ' is-invalid' : '' }}"
                           type="email" id="email" name="email"
                           value="{{ empty(old('email')) ? ($errors->has('email') ? '' : $buyer->user->email) : old('email') }}">

                    @if ($errors->has('email'))
                        <div class="form-control-feedback">{{ $errors->first('email') }}</div>
                    @endif
                </div>
            </div>

            <div class="col-md-6">
                <div class="form-group">
                    <label for="small-rounded-input">Last Name <span class="required">*</span></label>
                    <input class="form-control{{ $errors->has('lastName') ? ' is-invalid' : '' }}"
                           type="text" id="lastName" name="lastName"
                           value="{{ empty(old('lastName')) ? ($errors->has('lastName') ? '' : $buyer->user->last_name) : old('lastName') }}">

                    @if ($errors->has('lastName'))
                        <div class="form-control-feedback">{{ $errors->first('lastName') }}</div>
                    @endif
                </div>

                <div class="form-group">
                    <label for="small-rounded-input">Password <span class="required">*</span></label>
                    <input class="form-control{{ $errors->has('password') ? ' is-invalid' : '' }}"
                           type="password" id="password" name="password">

                    @if ($errors->has('password'))
                        <div class="form-control-feedback">{{ $errors->first('password') }}</div>
                    @endif
                </div>
            </div>
        </div>

        <h6 class="text-muted text-normal">Customer Company Information</h6>
        <hr class="margin-bottom-1x">

        <div class="row">
            <div class="col-md-6">
                <div class="form-group">
                    <label for="small-rounded-input">Company Name <span class="required">*</span></label>
                    <input class="form-control{{ $errors->has('companyName') ? ' is-invalid' : '' }}"
                           type="text" id="companyName" name="companyName"
                           value="{{ empty(old('companyName')) ? ($errors->has('companyName') ? '' : $buyer->company_name) : old('companyName') }}">

                    @if ($errors->has('companyName'))
                        <div class="form-control-feedback">{{ $errors->first('companyName') }}</div>
                    @endif
                </div>
            </div>

            <div class="col-md-6">
                <div class="form-group">
                    <label>Primary Customer Market <span class="required">*</span></label>

                    <select class="form-control" id="primaryCustomerMarket" name="primaryCustomerMarket">
                        <option value="1"
                                {{ empty(old('primaryCustomerMarket')) ? ($buyer->primary_customer_market == '1' ? 'selected' : '') :
                                    (old('primaryCustomerMarket') == '1' ? 'selected' : '') }}>All</option>
                        <option value="2"
                                {{ empty(old('primaryCustomerMarket')) ? ($buyer->primary_customer_market == '2' ? 'selected' : '') :
                                    (old('primaryCustomerMarket') == '2' ? 'selected' : '') }}>African</option>
                        <option value="3"
                                {{ empty(old('primaryCustomerMarket')) ? ($buyer->primary_customer_market == '3' ? 'selected' : '') :
                                    (old('primaryCustomerMarket') == '3' ? 'selected' : '') }}>Asian</option>
                        <option value="4"
                                {{ empty(old('primaryCustomerMarket')) ? ($buyer->primary_customer_market == '4' ? 'selected' : '') :
                                    (old('primaryCustomerMarket') == '4' ? 'selected' : '') }}>Caucasian</option>
                        <option value="5"
                                {{ empty(old('primaryCustomerMarket')) ? ($buyer->primary_customer_market == '5' ? 'selected' : '') :
                                    (old('primaryCustomerMarket') == '5' ? 'selected' : '') }}>Latino/Hispanic</option>
                        <option value="6"
                                {{ empty(old('primaryCustomerMarket')) ? ($buyer->primary_customer_market == '6' ? 'selected' : '') :
                                    (old('primaryCustomerMarket') == '6' ? 'selected' : '') }}>Middle Eastern</option>
                        <option value="7"
                                {{ empty(old('primaryCustomerMarket')) ? ($buyer->primary_customer_market == '7' ? 'selected' : '') :
                                    (old('primaryCustomerMarket') == '7' ? 'selected' : '') }}>Native American</option>
                        <option value="8"
                                {{ empty(old('primaryCustomerMarket')) ? ($buyer->primary_customer_market == '8' ? 'selected' : '') :
                                    (old('primaryCustomerMarket') == '8' ? 'selected' : '') }}>Pacific Islander</option>
                        <option value="9"
                                {{ empty(old('primaryCustomerMarket')) ? ($buyer->primary_customer_market == '9' ? 'selected' : '') :
                                    (old('primaryCustomerMarket') == '9' ? 'selected' : '') }}>Other</option>
                    </select>
                </div>
            </div>
        </div>

        <div class="row">
            <div class="col-md-6">
                <div class="form-group{{ $errors->has('sellerPermitNumber') ? ' has-danger' : '' }}">
                    <label for="small-rounded-input">Seller Permit Number <span class="required">*</span></label>
                    <input class="form-control{{ $errors->has('sellerPermitNumber') ? ' is-invalid' : '' }}"
                           type="text" id="sellerPermitNumber" name="sellerPermitNumber"
                           value="{{ empty(old('sellerPermitNumber')) ? ($errors->has('sellerPermitNumber') ? '' : $buyer->seller_permit_number) : old('sellerPermitNumber') }}">

                    @if ($errors->has('sellerPermitNumber'))
                        <div class="form-control-feedback">{{ $errors->first('sellerPermitNumber') }}</div>
                    @endif
                </div>
            </div>

            <div class="col-md-6">
                <label for="small-rounded-input">Do you sell online ?</label> &nbsp;

                <label for="sellOnlineYes" class="custom-control custom-radio">
                    <input class="custom-control-input sellOnline" type="radio" id="sellOnlineYes" name="sellOnline" value="1"
                            {{ empty(old('sellOnline')) ? ($buyer->sell_online == "1" ? 'checked' : '') :
                                    (old('sellOnline') == '1' ? 'checked' : '') }}>
                    <span class="custom-control-indicator"></span>
                    <span class="custom-control-description">Yes</span>
                </label>

                <label for="sellOnlineNo" class="custom-control custom-radio">
                    <input class="custom-control-input sellOnline" type="radio" id="sellOnlineNo" name="sellOnline" value="0"
                            {{ empty(old('sellOnline')) ? ($buyer->sell_online == "0" ? 'checked' : '') :
                                    (old('sellOnline') == '0' ? 'checked' : '') }}>
                    <span class="custom-control-indicator"></span>
                    <span class="custom-control-description">No</span>
                </label>

                <input class="form-control" type="text" id="website"
                       name="website" placeholder="http://www.mywebsite.com"
                       value="{{ empty(old('website')) ? ($errors->has('website') ? '' : $buyer->website) : old('website') }}">

                @if ($errors->has('website'))
                    <div class="form-control-feedback">{{ $errors->first('website') }}</div>
                @endif
            </div>
        </div>

        <div class="row">
            <div class="col-md-12">
                <label class="custom-control custom-checkbox">
                    <input class="custom-control-input" type="checkbox" id="receiveSpecialOffers" value="1" name="receiveSpecialOffers"
                            {{ ($buyer->receive_offers == 1) ? 'checked' : '' }}>
                    <span class="custom-control-indicator"></span>
                    <span class="custom-control-description">Sign up to receive special offers and information.</span>
                </label>
            </div>
        </div>

        <div class="row">
            <div class="col-md-12">
                <input class="btn btn-primary" type="submit" value="UPDATE">
            </div>
        </div>
    </form>
@stop

@section('additionalJS')
    <script>
        var usStates = <?php echo json_encode($usStates); ?>;
        var caStates = <?php echo json_encode($caStates); ?>;
        var oldState = '{{ empty(old('stateSelect')) ? ($errors->has('stateSelect') ? '' : $buyer->shipping_state_id) : old('stateSelect') }}';
        var oldFactoryState = '{{ empty(old('factoryStateSelect')) ? ($errors->has('factoryStateSelect') ? '' : $buyer->billing_state_id) : old('factoryStateSelect') }}';

        $(function () {
            $('form').bind('submit', function () {
                $(this).find(':input').prop('disabled', false);
            });

            $('#address').keyup(function () {
                var text = $(this).val();
                if ($("#sameAsShowroomAddress").is(':checked'))
                    $('#factoryAddress').val(text);
            });

            $('#unit').keyup(function () {
                var text = $(this).val();
                if ($("#sameAsShowroomAddress").is(':checked'))
                    $('#factoryUnit').val(text);
            });

            $('#city').keyup(function () {
                var text = $(this).val();
                if ($("#sameAsShowroomAddress").is(':checked'))
                    $('#factoryCity').val(text);
            });

            $('#state').keyup(function () {
                var text = $(this).val();
                if ($("#sameAsShowroomAddress").is(':checked'))
                    $('#factoryState').val(text);
            });

            $('#zipCode').keyup(function () {
                var text = $(this).val();
                if ($("#sameAsShowroomAddress").is(':checked'))
                    $('#factoryZipCode').val(text);
            });

            $('#phone').keyup(function () {
                var text = $(this).val();
                if ($("#sameAsShowroomAddress").is(':checked'))
                    $('#factoryPhone').val(text);
            });

            $('#fax').keyup(function () {
                var text = $(this).val();
                if ($("#sameAsShowroomAddress").is(':checked'))
                    $('#factoryFax').val(text);
            });

            $('#sameAsShowroomAddress').change(function () {
                $('#address').trigger('keyup');
                $('#unit').trigger('keyup');
                $('#city').trigger('keyup');
                $('#state').trigger('keyup');
                $('#zipCode').trigger('keyup');
                $('#phone').trigger('keyup');
                $('#fax').trigger('keyup');

                var location = $('.location:checked').val();
                $('.factoryLocation[value=' + location + ']').prop('checked', true);
                $('.factoryLocation').trigger('change');

                $('#factoryCountry').val($('#country').val());
                $('#factoryState').val($('#state').val());
                $('#factoryStateSelect').val($('#stateSelect').val());
            });

            $('.location').change(function () {
                var location = $('.location:checked').val();

                if ($("#sameAsShowroomAddress").is(':checked')) {
                    $('.factoryLocation[value=' + location + ']').prop('checked', true);
                    $('.factoryLocation').trigger('change');
                }

                if (location == 'CA' || location == 'US') {
                    if (location == 'US')
                        $('#country').val('1');
                    else
                        $('#country').val('2');


                    $('#country').prop('disabled', 'disabled');
                    $('#form-group-state-select').show();
                    $('#stateSelect').val('');
                    $('#form-group-state').hide();

                    $('#stateSelect').html('<option value="">Select State</option>');

                    if (location == 'US') {
                        $.each(usStates, function (index, value) {
                            if (value.id == oldState)
                                $('#stateSelect').append('<option value="'+value.id+'" selected>'+value.name+'</option>');
                            else
                                $('#stateSelect').append('<option value="'+value.id+'">'+value.name+'</option>');
                        });
                    }

                    if (location == 'CA') {
                        $.each(caStates, function (index, value) {
                            if (value.id == oldState)
                                $('#stateSelect').append('<option value="'+value.id+'" selected>'+value.name+'</option>');
                            else
                                $('#stateSelect').append('<option value="'+value.id+'">'+value.name+'</option>');
                        });
                    }
                } else {
                    $('#country').prop('disabled', false);
                    $('#form-group-state-select').hide();
                    $('#form-group-state').show();
                }
            });

            $('.factoryLocation').change(function () {
                var location = $('.factoryLocation:checked').val();

                if (location == 'CA' || location == 'US') {
                    if (location == 'US')
                        $('#factoryCountry').val('1');
                    else
                        $('#factoryCountry').val('2');

                    $('#factoryCountry').prop('disabled', 'disabled');
                    $('#form-group-factory-state-select').show();
                    $('#factoryStateSelect').val('');
                    $('#form-group-factory-state').hide();

                    $('#factoryStateSelect').html('<option value="">Select State</option>');

                    if (location == 'US') {
                        $.each(usStates, function (index, value) {
                            if (value.id == oldFactoryState)
                                $('#factoryStateSelect').append('<option value="'+value.id+'" selected>'+value.name+'</option>');
                            else
                                $('#factoryStateSelect').append('<option value="'+value.id+'">'+value.name+'</option>');
                        });
                    }

                    if (location == 'CA') {
                        $.each(caStates, function (index, value) {
                            if (value.id == oldFactoryState)
                                $('#factoryStateSelect').append('<option value="'+value.id+'" selected>'+value.name+'</option>');
                            else
                                $('#factoryStateSelect').append('<option value="'+value.id+'">'+value.name+'</option>');
                        });
                    }
                } else {
                    $('#factoryCountry').prop('disabled', false);
                    $('#form-group-factory-state-select').hide();
                    $('#form-group-factory-state').show();
                }
            });

            $('#country').change(function () {
                var countryId = $(this).val();

                if (countryId == 1) {
                    $("#locationUS").prop("checked", true);
                    $('.location').trigger('change');
                } else if (countryId == 2) {
                    $("#locationCA").prop("checked", true);
                    $('.location').trigger('change');
                }
            });

            $('.sellOnline').change(function () {
                if ($('#sellOnlineYes').is(':checked')) {
                    $('#website').show();
                } else {
                    $('#website').hide();
                }
            });

            $('.location').trigger('change');
            $('.factoryLocation').trigger('change');
            $('.sellOnline').trigger('change');
        })
    </script>
@stop