@extends('admin.layouts.admin')

@section('additionalCSS')
    <link href="{{ asset('plugins/toastr/toastr.min.css') }}" rel="stylesheet">
    <link href="{{ asset('plugins/bootstrap-datepicker/css/bootstrap-datepicker.min.css') }}" rel="stylesheet">
@stop

@section('content')
    <form action="{{ route('admin_welcome_notification_save') }}" method="POST">
        @csrf

        <div class="row">
            <div class="col-md-2">
                Welcome Notification
            </div>
            <div class="col-md-10">
                <textarea name="data" id="buyer_home" rows="10" cols="80">{{ $setting->value or '' }}</textarea>
            </div>
        </div>

        <div class="row">
            <div class="col-md-2">
                Opening Soon
            </div>

            <div class="col-md-10">
                <label class="custom-control custom-checkbox">
                    <input type="checkbox" class="custom-control-input" id="default" value="1"
                           name="default" {{ $os->value == '1' ? 'checked' : '' }}>
                    <span class="custom-control-indicator"></span>
                </label>
            </div>
        </div>

        <div class="row">
            <div class="col-md-2">
                Opening Date - Time
            </div>

            <div class="col-md-4">
                <input type="text" id="date" class="form-control"
                       name="date" value="{{ $openingDate->value }}" required>
            </div>
        </div>

        <div class="row">
            <div class="col-md-12 text-right">
                <button class="btn btn-primary" id="btnOrderNoticeSubmit">Save</button>
            </div>
        </div>
    </form>
@stop

@section('additionalJS')
    <script type="text/javascript" src="{{ asset('plugins/toastr/toastr.min.js') }}"></script>
    <script type="text/javascript" src="{{ asset('plugins/bootstrap-datepicker/js/bootstrap-datepicker.min.js') }}"></script>
    <script type="text/javascript" src="{{ asset('plugins/ckeditor/ckeditor.js') }}"></script>
    <script>
        $(function () {
            var message = '{{ session('message') }}';

            if (message != '')
                toastr.success(message);

            var buyer_home = CKEDITOR.replace('buyer_home');

            $('#date').datepicker({
                autoclose: true,
                format: 'mm/dd/yyyy',
                orientation: "bottom left"
            });
        });
    </script>
@stop