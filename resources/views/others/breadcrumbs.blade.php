@if (count($breadcrumbs))
    <ul class="breadcrumbs">
        @foreach ($breadcrumbs as $breadcrumb)

            @if ($breadcrumb->url && !$loop->last)
                <li><a href="{{ $breadcrumb->url }}">{{ ucwords(strtolower($breadcrumb->title)) }}</a></li>
                <li class="separator">&nbsp;</li>
            @else
                <li>{{ ucwords(strtolower($breadcrumb->title)) }}</li>
            @endif
        @endforeach
    </ul>
@endif