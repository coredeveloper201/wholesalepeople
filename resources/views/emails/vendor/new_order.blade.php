<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
    <title>New Order</title>
    <meta name="viewport" content="width=device-width, initial-scale=1.0"/>
</head>

<body style="margin: 0; padding: 0;">
<table border="0" cellpadding="0" cellspacing="0" width="100%">
    <tr>
        <td style="padding: 20px 0 30px 0;">
            <table align="center" border="0" cellpadding="0" cellspacing="0" width="600" style="border: 1px solid #cccccc;">
                <tr>
                    <td align="center" bgcolor="black" style="padding: 40px 0 30px 0;">
                        <img src="https://stylepick.net/images/logo.png" alt="Creating Email Magic" width="200" height="40" style="display: block;" />
                    </td>
                </tr>
                <tr>
                    <td bgcolor="#ffffff" style="padding: 40px 30px 40px 30px;">
                        <table border="0" cellpadding="0" cellspacing="0" width="100%">
                            <tr>
                                <td style="color: #153643; font-family: Arial, sans-serif; font-size: 24px;">
                                    <b>New Order</b>
                                </td>
                            </tr>
                            <tr>
                                <td style="padding: 2px 0 20px 0; font-family: Arial, sans-serif;">
                                    You have received order from <b>{{ $order->company_name }}</b>
                                </td>
                            </tr>
                            <tr>
                                <td>
                                    <table cellpadding="0" cellspacing="0" width="100%" style="font-family: Arial, sans-serif; border: 1px solid black">
                                        <tr bgcolor="black" style="color: white;">
                                            <td style="padding: 15px">Order Number</td>
                                            <td style="padding: 15px">Total</td>
                                            <td style="padding: 15px"></td>
                                        </tr>

                                        <tr>
                                            <td style="padding: 15px; border-right: 1px solid black">
                                                {{ $order->order_number }}
                                            </td>

                                            <td style="padding: 15px; border-right: 1px solid black">
                                                ${{ number_format($order->total, 2, '.', ',') }}
                                            </td>

                                            <td style="padding: 15px; text-align: center">
                                                <a style="color: #F01318; text-decoration: none;" href="{{ route('vendor_order_details', ['order' => $order->id]) }}">View Details</a>
                                            </td>
                                        </tr>
                                    </table>
                                </td>
                            </tr>

                            <tr>
                                <td style="padding: 20px 0 20px 0; font-family: Arial, sans-serif;">
                                    You can always check your orders by going into 'New Orders' under the 'Orders' section.
                                    Please contact us with any questions or concerns. Thank you for being a vendor on StylePick.
                                    <br><br>

                                    Thank You,<br>
                                    StylePick Support Team
                                </td>
                            </tr>
                        </table>
                    </td>
                </tr>
                <tr>
                    <td bgcolor="#F01318" style="padding: 30px 30px 30px 30px;">
                        <table border="0" cellpadding="0" cellspacing="0" width="100%">
                            <tr>
                                <td style="color: #ffffff; font-family: Arial, sans-serif; font-size: 14px;">
                                    &reg; 2018 All rights reserved. <br/>
                                </td>
                            </tr>
                        </table>
                    </td>
                </tr>
            </table>
        </td>
    </tr>
</table>
</body>
</html>