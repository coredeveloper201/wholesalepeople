<!DOCTYPE html>
<html lang="{{ app()->getLocale() }}">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <!-- CSRF Token -->
    <meta name="csrf-token" content="{{ csrf_token() }}">
    <title>{{ config('app.name', 'Wholesalepeople') }}</title>
    <!-- Styles -->
    <link rel="icon" href="/images/stylepick_blue.ico" type="image/x-icon">
    <link href="{{ asset('themes/admire/css/components.css') }}" rel="stylesheet">
    <link href="{{ asset('themes/admire/css/custom.css') }}" rel="stylesheet">
    @yield('additionalCSS')
</head>

<body class="body">
<!-- /#top -->

<div class="wrapper">
    <div id="left">
        <div class="menu_scroll">
            <div class="left_media text-center">
                @if ($logo_path != '')
                    <a href="{{ route('vendor_dashboard') }}">
                        <img src="{{ $logo_path }}" class="admin_img" alt="logo">
                    </a>
                @endif
            </div>

            <ul id="menu">
                <?php
                    $menu_items = ['Create a New Item', 'Category', 'Color', 'Pack', 'Other: Fabric, Made In, Supplying Vendor, Default Item Setting', 'Item Edit',
                    'Edit All Items', 'Data Import', 'Sort Items'];
                    foreach($categories as $category)
                        $menu_items[] = $category->name;
                ?>

                <li class="collapse active">
                    <a href="javascript:;">
                        <span class="link-title menu_hide">&nbsp; PRODUCTS</span>
                        <span class="fa arrow menu_hide"></span>
                    </a>
                    <ul class="collapse show">
                        <li class="{{ (isset($page_title) && $page_title == 'Data Import') ? 'active' : '' }}">
                            <a href="{{ route('vendor_data_import') }}">
                                <i class="fa fa-angle-right"></i>
                                &nbsp; Data Import
                            </a>
                        </li>

                        <li class="{{ (isset($page_title) && $page_title == 'Create a New Item') ? 'active' : '' }}">
                            <a href="{{ route('vendor_create_new_item') }}">
                                <i class="fa fa-angle-right"></i>
                                &nbsp; New Products
                            </a>
                        </li>

                        <li class="{{ (isset($page_title) && $page_title == 'Sort Items') ? 'active' : '' }}">
                            <a href="{{ route('vendor_sort_items_view') }}">
                                <i class="fa fa-angle-right"></i>
                                &nbsp; Sort Items
                            </a>
                        </li>

                        <?php
                            $sub_menu_items = ['Item Edit', 'Edit All Items'];

                            foreach($categories as $category) {
                                $sub_menu_items[] = $category->name;

                                if (sizeof($category->subCategories) > 0) {
                                    foreach ($category->subCategories as $sub) {
                                        $sub_menu_items[] = $sub->name;
                                    }
                                }
                            }

                            $title = isset($page_title) ? $page_title : '';

                            /*if (Route::currentRouteName() == 'vendor_edit_item') {
                                $title = request()->route()->parameters['item']->category->name;
                            }*/
                        ?>

                        <li class="{{ (isset($page_title) && in_array($page_title, $sub_menu_items)) ? 'active' : '' }}">
                            <a href="#">
                                <span id="btnMenuItemList">
                                    <i class="fa fa-angle-right"></i>
                                    &nbsp;List Products
                                </span>
                                <span class="fa arrow"></span>
                            </a>
                            <ul class="sub-menu sub-submenu {{ (isset($page_title) && in_array($page_title, $sub_menu_items)) ? '' : 'collapse' }}">
                                @foreach($categories as $category)
                                    <?php
                                        $subCat = [];

                                        foreach ($category->subCategories as $sub)
                                            $subCat[] = $sub->name;
                                    ?>

                                    <li class="{{ ((isset($page_title) && $title == $category->name) || in_array($title, $subCat)) ? 'active' : '' }} {{ sizeof($category->subCategories) > 0 ? 'has-sub-categories' : '' }}" data-id="{{ $category->id }}">
                                        <a href="{{ (sizeof($category->subCategories) > 0) ? 'javascript:;' : route('vendor_item_list_by_category', ['category' => $category->id]) }}">
                                            <span class="menu-category-item" data-id="{{ $category->id }}">
                                                <i class="fa fa-angle-right"></i>
                                                &nbsp;{{ $category->name }}
                                            </span>

                                            @if (sizeof($category->subCategories) > 0)
                                                <span class="fa arrow"></span>
                                            @endif
                                        </a>

                                        @if (sizeof($category->subCategories) > 0)
                                            <ul class="sub-menu sub-submenu collapse">
                                                @foreach($category->subCategories as $sub)
                                                    <li class="{{ (isset($page_title) && $title == $sub->name) ? 'active' : '' }}">
                                                        <a href="{{ route('vendor_item_list_by_category', ['category' => $sub->id]) }}">
                                                            <i class="fa fa-angle-right"></i>
                                                            &nbsp;{{ $sub->name }}
                                                        </a>
                                                    </li>
                                                @endforeach
                                            </ul>
                                        @endif
                                    </li>
                                @endforeach
                            </ul>
                        </li>


                        <?php $sub_menu_items = ['Category', 'Color', 'Pack', 'Other: Fabric, Made In, Supplying Vendor, Default Item Setting'] ?>
                        <li class="{{ (isset($page_title) && in_array($page_title, $sub_menu_items)) ? 'active' : '' }}">
                            <a href="javascript:;">
                                <i class="fa fa-angle-right"></i>
                                &nbsp;Product Settings
                                <span class="fa arrow"></span>
                            </a>
                            <ul class="sub-menu sub-submenu {{ (isset($page_title) && in_array($page_title, $sub_menu_items)) ? '' : 'collapse' }}">
                                <li class="{{ (isset($page_title) && $page_title == 'Category') ? 'active' : '' }}">
                                    <a href="{{ route('vendor_category') }}">
                                        <i class="fa fa-angle-right"></i>
                                        &nbsp;Category
                                    </a>
                                </li>
                                <li class="{{ (isset($page_title) && $page_title == 'Color') ? 'active' : '' }}">
                                    <a href="{{ route('vendor_color') }}">
                                        <i class="fa fa-angle-right"></i>
                                        &nbsp;Color
                                    </a>
                                </li>
                                {{--<li>
                                    <a href="{{ route('vendor_size') }}">
                                        <i class="fa fa-angle-right"></i>
                                        &nbsp;Size
                                    </a>
                                </li>--}}
                                <li class="{{ (isset($page_title) && $page_title == 'Pack') ? 'active' : '' }}">
                                    <a href="{{ route('vendor_pack') }}">
                                        <i class="fa fa-angle-right"></i>
                                        &nbsp;Pack
                                    </a>
                                </li>

                                <li class="{{ (isset($page_title) && $page_title == 'Other: Fabric, Made In, Supplying Vendor, Default Item Setting') ? 'active' : '' }}">
                                    <a href="{{ route('vendor_item_settings_others') }}">
                                        <i class="fa fa-angle-right"></i>
                                        &nbsp;Others
                                    </a>
                                </li>
                            </ul>
                        </li>
                    </ul>
                </li>

                <?php
                    $menu_items = ['New Orders', 'Order Details', 'Confirmed Orders', 'Back Orders', 'Shipped Orders', 'Cancel Orders',
                        'Return Orders', 'All Orders', 'Incomplete Checkouts'];
                ?>

                <li class="collapse active">
                    <a href="javascript:;">
                        <span class="link-title menu_hide">&nbsp; ORDERS</span>
                        <span class="fa arrow menu_hide"></span>
                    </a>
                    <ul class="collapse show">
                        <?php
                            $sub_menu_items = ['New Orders', 'Order Details', 'Confirmed Orders', 'Back Orders', 'Shipped Orders',
                                'Cancel Orders', 'Return Orders', 'All Orders'];
                        ?>

                        <li class="{{ (isset($page_title) && in_array($page_title, $sub_menu_items)) ? 'active' : '' }}">
                            <a role="button" style="cursor: pointer">
                                <span id="btnMenuAllOrders">
                                    <i class="fa fa-angle-right"></i>
                                    &nbsp;All Orders
                                </span>
                                <span class="fa arrow"></span>
                            </a>
                            <ul class="sub-menu sub-submenu {{ (isset($page_title) && in_array($page_title, $sub_menu_items)) ? '' : 'collapse' }}">
                                <li class="{{ (isset($page_title) && $page_title == 'New Orders') ? 'active' : '' }}">
                                    <a href="{{ route('vendor_new_orders') }}">
                                        <i class="fa fa-angle-right"></i>
                                        &nbsp;New Orders
                                    </a>
                                </li>

                                <li class="{{ (isset($page_title) && $page_title == 'Confirmed Orders') ? 'active' : '' }}">
                                    <a href="{{ route('vendor_confirmed_orders') }}">
                                        <i class="fa fa-angle-right"></i>
                                        &nbsp;Confirmed
                                    </a>
                                </li>

                                <li class="{{ (isset($page_title) && $page_title == 'Back Orders') ? 'active' : '' }}">
                                    <a href="{{ route('vendor_backed_orders') }}">
                                        <i class="fa fa-angle-right"></i>
                                        &nbsp;Back Ordered
                                    </a>
                                </li>

                                <li class="{{ (isset($page_title) && $page_title == 'Shipped Orders') ? 'active' : '' }}">
                                    <a href="{{ route('vendor_shipped_orders') }}">
                                        <i class="fa fa-angle-right"></i>
                                        &nbsp;Shipped
                                    </a>
                                </li>

                                <li class="{{ (isset($page_title) && $page_title == 'Cancel Orders') ? 'active' : '' }}">
                                    <a href="{{ route('vendor_cancelled_orders') }}">
                                        <i class="fa fa-angle-right"></i>
                                        &nbsp;Cancelled
                                    </a>
                                </li>

                                <li class="{{ (isset($page_title) && $page_title == 'Return Orders') ? 'active' : '' }}">
                                    <a href="{{ route('vendor_returned_orders') }}">
                                        <i class="fa fa-angle-right"></i>
                                        &nbsp;Returned
                                    </a>
                                </li>
                            </ul>
                        </li>

                        <li class="{{ (isset($page_title) && $page_title == 'Incomplete Checkouts') ? 'active' : '' }}">
                            <a href="{{ route('vendor_incomplete_orders') }}">
                                <i class="fa fa-angle-right"></i>
                                &nbsp;Incomplete Checkouts
                            </a>
                        </li>
                    </ul>
                </li>

                <?php $menu_items = ['Banner Manager', 'Banner Items', 'Best Items'] ?>
                <li class="collapse {{ (isset($page_title) && in_array($page_title, $menu_items)) ? 'active' : '' }}">
                    <a href="javascript:;">
                        <span class="link-title menu_hide">&nbsp; BANNER MANAGER</span>
                        <span class="fa arrow menu_hide"></span>
                    </a>
                    <ul class="collapse {{ (isset($page_title) && in_array($page_title, $menu_items)) ? 'show' : '' }}">
                        <li class="{{ (isset($page_title) && $page_title == 'Banner Manager') ? 'active' : '' }}">
                            <a href="{{ route('vendor_banner') }}">
                                <i class="fa fa-angle-right"></i>
                                &nbsp;Banners
                            </a>
                        </li>

                        <li class="{{ (isset($page_title) && $page_title == 'Banner Items') ? 'active' : '' }}">
                            <a href="{{ route('vendor_banner_items') }}">
                                <i class="fa fa-angle-right"></i>
                                &nbsp;Banner Items
                            </a>
                        </li>

                        <li class="{{ (isset($page_title) && $page_title == 'Best Items') ? 'active' : '' }}">
                            <a href="{{ route('vendor_best_items') }}">
                                <i class="fa fa-angle-right"></i>
                                &nbsp; Best Items
                            </a>
                        </li>
                    </ul>
                </li>

                <?php $menu_items = ['All Customers', 'Block Customers', 'Store Credit'] ?>
                <li class="collapse {{ (isset($page_title) && in_array($page_title, $menu_items)) ? 'active' : '' }}">
                    <a href="javascript:;">
                        <span class="link-title menu_hide">&nbsp; CUSTOMERS</span>
                        <span class="fa arrow menu_hide"></span>
                    </a>
                    <ul class="collapse {{ (isset($page_title) && in_array($page_title, $menu_items)) ? 'show' : '' }}">
                        <li class="{{ (isset($page_title) && $page_title == 'All Customers') ? 'active' : '' }}">
                            <a href="{{ route('vendor_all_customer') }}">
                                <i class="fa fa-angle-right"></i>
                                &nbsp;All Customers
                            </a>
                        </li>

                        <li class="{{ (isset($page_title) && $page_title == 'Store Credit') ? 'active' : '' }}">
                            <a href="{{ route('vendor_store_credit_view') }}">
                                <i class="fa fa-angle-right"></i>
                                &nbsp;Store Credit
                            </a>
                        </li>
                    </ul>
                </li>

                <?php $menu_items = ['Vendor Information', 'Account Setting', 'Shipping Methods', 'Coupon'] ?>
                <li class="collapse {{ (isset($page_title) && in_array($page_title, $menu_items)) ? 'active' : '' }}">
                    <a href="javascript:;">
                        <span class="link-title menu_hide">&nbsp; ADMINISTRATION</span>
                        <span class="fa arrow menu_hide"></span>
                    </a>
                    <ul class="collapse {{ (isset($page_title) && in_array($page_title, $menu_items)) ? 'show' : '' }}">
                        <li class="{{ (isset($page_title) && $page_title == 'Vendor Information') ? 'active' : '' }}">
                            <a href="{{ route('vendor_vendor_information') }}">
                                <i class="fa fa-angle-right"></i>
                                &nbsp;Vendor Information
                            </a>
                        </li>

                        <li class="{{ (isset($page_title) && $page_title == 'Account Setting') ? 'active' : '' }}">
                            <a href="{{ route('vendor_account_setting') }}">
                                <i class="fa fa-angle-right"></i>
                                &nbsp;Account Setting
                            </a>
                        </li>

                        <li class="{{ (isset($page_title) && $page_title == 'Shipping Methods') ? 'active' : '' }}">
                            <a href="{{ route('vendor_shipping_method') }}">
                                <i class="fa fa-angle-right"></i>
                                &nbsp;Shipping Methods
                            </a>
                        </li>

                        <li class="{{ (isset($page_title) && $page_title == 'Coupon') ? 'active' : '' }}">
                            <a href="{{ route('vendor_coupon') }}">
                                <i class="fa fa-angle-right"></i>
                                &nbsp;Coupon
                            </a>
                        </li>
                    </ul>
                </li>

                <?php $menu_items = ['View & Leave Feedback'] ?>
                <li class="collapse {{ (isset($page_title) && in_array($page_title, $menu_items)) ? 'active' : '' }}">
                    <a href="javascript:;">
                        <span class="link-title menu_hide">&nbsp; FEEDBACK</span>
                        <span class="fa arrow menu_hide"></span>
                    </a>
                    <ul class="collapse {{ (isset($page_title) && in_array($page_title, $menu_items)) ? 'show' : '' }}">
                        <li class="{{ (isset($page_title) && $page_title == 'View & Leave Feedback') ? 'active' : '' }}">
                            <a href="{{ route('vendor_feedback') }}">
                                <i class="fa fa-angle-right"></i>
                                &nbsp;View/Leave Feedback
                            </a>
                        </li>
                    </ul>
                </li>
            </ul>
            <!-- /#menu -->
        </div>
    </div>
    <!-- /#left -->

    <div id="content" class="bg-container">
        <div id="top">
            <!-- .navbar -->
            <nav class="navbar navbar-static-top">
                <div class="container-fluid m-0">
                    <!--<a class="navbar-brand" href="index.html"><h4>{{ Auth::user()->vendor->company_name }}</h4></a>-->
                    <div class="menu mr-sm-auto">
                        <span class="toggle-left" id="menu-toggle">
                            <i class="fa fa-bars"></i>
                        </span>
                    </div>
                    <div class="topnav dropdown-menu-right">
                        <a class="btn btn-default btn-sm messages btn-message" href="{{ route('vendor_view_messages') }}">
                            <i class="fa fa-envelope-o fa-1x"></i>

                            @if ($unread_msg > 0)
                                <span class="badge badge-pill badge-warning notifications_badge_top">{{ $unread_msg }}</span>
                            @endif
                        </a>

                        <div class="btn-group">
                            <div class="user-settings no-bg">
                                <button type="button" class="btn btn-default no-bg micheal_btn" data-toggle="dropdown">
                                    <img src="{{ asset('images/default-avatar.png') }}" class="admin_img2 img-thumbnail rounded-circle avatar-img"
                                         alt="avatar"> <strong>{{ Auth::user()->first_name.' '.Auth::user()->last_name }}</strong>
                                    <span class="fa fa-sort-down white_bg"></span>
                                </button>
                                <div class="dropdown-menu admire_admin">
                                    <a class="dropdown-item title" href="#">
                                        Vendor Admin</a>
                                    <a class="dropdown-item" href="{{ route('vendor_or_parent_category', ['text' => changeSpecialChar(Auth::user()->vendor->company_name)]) }}" target="_blank"><i class="fa fa-eye"></i>
                                        Preview Home</a>
                                    <a id="btnLogOut" class="dropdown-item" href="#"><i class="fa fa-sign-out"></i>
                                        Log Out</a>

                                    <form id="logoutForm" class="" action="{{ route('logout_vendor') }}" method="post">
                                        {{ csrf_field() }}
                                    </form>
                                </div>
                            </div>
                        </div>

                    </div>
                </div>
                <!-- /.container-fluid -->
            </nav>
            <!-- /.navbar -->
        <!-- /.head -->
        </div>
        <header class="head">
            <div class="main-bar">
                <div class="row no-gutters">
                    <div class="col-6">
                        <h4 class="m-t-5">
                            {{ $page_title or '' }}
                        </h4>
                    </div>
                </div>
            </div>
        </header>

        <div class="outer">
            <div class="inner bg-container">
                @yield('content')
            </div>
            <!-- /.inner -->
        </div>
        <!-- /.outer -->
    </div>
    <!-- /#content -->
</div>
<!--wrapper-->


<!-- global scripts-->
<script type="text/javascript" src="{{ asset('themes/admire/js/components.js') }}"></script>
<script type="text/javascript" src="{{ asset('themes/admire/js/custom.js') }}"></script>
<!--end of global scripts-->
<script>
    $(function () {
        $('#btnLogOut').click(function () {
            $('#logoutForm').submit();
        });

        $('#btnMenuItemList').click(function (event) {
            event.stopImmediatePropagation();
            window.location.replace("{{ route('vendor_item_list_all') }}");
        });

        /*$('#btnMenuItemList').click(function () {
            if ($(this).closest('li').hasClass('active'))
                window.location.replace("{{ route('vendor_item_list_all') }}");
        }).on('touchstart', function () {
            if (!$(this).closest('li').hasClass('active'))
                window.location.replace("{{ route('vendor_item_list_all') }}");
        });*/

        /*$('#btnMenuAllOrders').on('click', function () {
            if ($(this).closest('li').hasClass('active'))
                window.location.replace("{{ route('vendor_all_orders') }}");
        }).on('touchstart', function () {
            if (!$(this).closest('li').hasClass('active'))
                window.location.replace("{{ route('vendor_all_orders') }}");
        });
*/
        $('#btnMenuAllOrders').click(function (event) {
            event.stopImmediatePropagation();
            window.location.replace("{{ route('vendor_all_orders') }}");
        });

        /*$('.has-sub-categories').click(function (event) {
            event.stopImmediatePropagation();
            var id = $(this).data('id');
            var url = '{{ route('vendor_item_list_by_category', ['category' => '']) }}';

            //if ($(this).hasClass('active'))
            window.location.replace(url + "/" + id);
        });*/

        $('.menu-category-item').click(function (event) {
            event.stopImmediatePropagation();
            var id = $(this).data('id');
            var url = '{{ route('vendor_item_list_by_category', ['category' => '']) }}';
            window.location.replace(url + '/' + id);
        });
    })
</script>
@yield('additionalJS')
</body>
</html>
