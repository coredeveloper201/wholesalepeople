@extends('vendor.layouts.dashboard')

@section('additionalCSS')
    <link href="{{ asset('plugins/toastr/toastr.min.css') }}" rel="stylesheet">
@stop

@section('content')
    <div class="row">
        <div class="col-md-12">
            <button class="btn btn-primary" id="btnAddProduct">Add Product</button>
        </div>
    </div>

    <div class="row">
        <div class="col-md-12">
            <form action="{{ route('vendor_best_item_sort') }}" method="POST" id="form-sort">
                @csrf
                <ul class="il-product-container sl-product-container" id="SortItems">
                    @foreach($items as $item)
                        <li>
                            <div class="il-item-container">
                                <div class="il-item-top">
                                    <div class="il-item-img">
                                        <a href="{{ route('vendor_edit_item', ['item' => $item->id]) }}" class="text-primary il-item-style-no">
                                            @if (sizeof($item->images) > 0)
                                                <img src="{{ asset($item->images[0]->list_image_path) }}" alt="{{ $item->style_no }}">
                                            @else
                                                <img src="{{ asset('images/no-image.png') }}" alt="{{ $item->style_no }}">
                                            @endif
                                        </a>
                                    </div>
                                </div>

                                <div class="il-item-footer text-center">
                                    <a href="{{ route('vendor_edit_item', ['item' => $item->id]) }}" class="text-primary il-item-style-no">{{ $item->style_no }}</a>
                                    ${{ number_format($item->price, 2, '.', '') }} <br>

                                    <input type="hidden" name="sort[]" class="form-control input_sort" value="{{ $item->sorting }}">
                                    <input type="hidden" name="ids[]" value="{{ $item->id }}">

                                    <a href="" class="text-danger btnRemove" data-id="{{ $item->id }}">Remove</a>
                                </div>
                            </div>
                        </li>
                    @endforeach
                </ul>
            </form>
        </div>
    </div>

    <div class="modal fade" id="item-modal" tabindex="-1" role="dialog" aria-labelledby="modalLabelLarge"
         aria-hidden="true">
        <div class="modal-dialog modal-lg">
            <div class="modal-content">
                <div class="modal-header">
                    <h4 class="modal-title" id="modalLabelLarge">Select Item</h4>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">×</span>
                    </button>
                </div>

                <div class="modal-body">
                    <div class="row">
                        <div class="col-md-4">
                            <input type="text" class="form-control" placeholder="Search" id="modal-search">
                        </div>

                        <div class="col-md-4">
                            <select class="form-control" id="modal-category">
                                <option value="">All Category</option>

                                @foreach($categories as $category)
                                    <option value="{{ $category->id }}">{{ $category->name }}</option>
                                @endforeach
                            </select>
                        </div>

                        <div class="col-md-4">
                            <button class="btn btn-primary" id="modal-btn-search">SEARCH</button>
                        </div>
                    </div>

                    <br>
                    <hr>

                    <ul class="modal-items">
                        @foreach($products as $item)
                            <li class="modal-item" data-id="{{ $item->id }}">
                                @if (sizeof($item->images) > 0)
                                    <img src="{{ asset($item->images[0]->list_image_path) }}" alt="{{ $item->style_no }}">
                                @else
                                    <img src="{{ asset('images/no-image.png') }}" alt="{{ $item->style_no }}">
                                @endif

                                <div class="style_no">
                                    {{ $item->style_no }}
                                </div>
                            </li>
                        @endforeach
                    </ul>

                    <div id="modal-pagination">
                        {{ $products->links() }}
                    </div>
                </div>
            </div>
        </div>
    </div>

    <template id="template-modal-item">
        <li class="modal-item">
            <img src="">

            <div class="style-no">

            </div>
        </li>
    </template>

    <div class="modal fade" id="deleteModal" role="dialog" aria-labelledby="deleteModal">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-header bg-danger">
                    <h4 class="modal-title text-white">Delete</h4>
                </div>
                <div class="modal-body">
                    <p>
                        Are you sure want to delete?
                    </p>
                </div>
                <div class="modal-footer">
                    <button class="btn  btn-default" data-dismiss="modal">Close</button>
                    <button class="btn  btn-danger" id="modalBtnDelete">Delete</button>
                </div>
            </div>
        </div>
        <!--- end modals-->
    </div>
@stop

@section('additionalJS')
    <script type="text/javascript" src="{{ asset('plugins/toastr/toastr.min.js') }}"></script>
    <script type="text/javascript" src="{{ asset('plugins/sortable/js/Sortable.min.js') }}"></script>
    <script>
        $(function () {
            $.ajaxSetup({
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                }
            });

            var message = '{{ session('message') }}';

            if (message != '')
                toastr.success(message);

            var el = document.getElementById('SortItems');
            Sortable.create(el, {
                animation: 150,
                dataIdAttr: 'data-id',
                onEnd: function () {
                    updateSort();
                },
            });


            // Add product
            $('#btnAddProduct').click(function (e) {
                e.preventDefault();

                $('#item-modal').modal('show');
            });

            $(document).on('click', '.modal-item', function () {
                var id = $(this).data('id');

                $.ajax({
                    method: "POST",
                    url: "{{ route('vendor_best_item_add') }}",
                    data: { id: id }
                }).done(function( data ) {
                    window.location.reload();
                });
            });

            function updateSort() {
                var page = '{{ request()->get('page') }}';

                if (page == '')
                    page = 1;
                else
                    page = parseInt(page);

                var perPage = 50;

                if ($('#showPerPage').val() == '2')
                    perPage = 100;
                else if ($('#showPerPage').val() == '3')
                    perPage = 150;

                var t = (page-1) * perPage + 1;

                $('#SortItems li').each(function (i, item) {
                    $(item).find('.input_sort').val(t+i);
                });

                $('#form-sort').submit();
            }

            var selectedId;

            $('.btnRemove').click(function (e) {
                e.preventDefault();

                $('#deleteModal').modal('show');
                selectedId = $(this).data('id');
            });

            $('#modalBtnDelete').click(function () {
                $.ajax({
                    method: "POST",
                    url: "{{ route('vendor_best_item_delete') }}",
                    data: { id: selectedId }
                }).done(function( msg ) {
                    location.reload();
                });
            });

            // Pagination
            $(document).on('click', '.page-link', function (e) {
                e.preventDefault();
                var url = $(this).attr('href');
                var page = getURLParameter(url, 'page');

                filterItem(page);
            });

            function getURLParameter(url, name) {
                return (RegExp(name + '=' + '(.+?)(&|$)').exec(url)||[,null])[1];
            }

            function filterItem(page) {
                page = typeof page !== 'undefined' ? page : 1;
                var search = $('#modal-search').val();
                var category = $('#modal-category').val();

                $.ajax({
                    method: "POST",
                    url: "{{ route('vendor_get_items_for_modal') }}",
                    data: { search: search, category: category, page: page }
                }).done(function( data ) {
                    var products = data.items.data;
                    $('#modal-pagination').html(data.pagination);

                    $('.modal-items').html('');

                    $.each(products, function (index, product) {
                        var html = $('#template-modal-item').html();
                        var row = $(html);

                        row.attr('data-id', product.id);
                        row.find('img').attr('src', product.imagePath);
                        row.find('.style-no').html(product.style_no);

                        $('.modal-items').append(row);
                    });
                });
            }

            $('#modal-btn-search').click(function () {
                filterItem();
            });
        });
    </script>
@stop