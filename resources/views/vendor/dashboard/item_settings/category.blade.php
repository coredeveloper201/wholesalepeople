@extends('vendor.layouts.dashboard')

@section('additionalCSS')
    <link href="{{ asset('plugins/toastr/toastr.min.css') }}" rel="stylesheet">
    <link href="{{ asset('plugins/Nestable/style.css') }}" rel="stylesheet">
@stop

@section('content')
    <div class="row">
        <div class="col-md-12">
            <button class="btn btn-primary" id="btnAddNew">Add New Category</button>
        </div>
        <div class="col-md-12 dragnotice">Drag & drop to change sort order</div>
    </div>
    <div class="row">
        <div class="col-md-6">
            <div class="addcategory">
                <div class="dd" id="nestable">
                    <ol class="dd-list">
                        @foreach($categories as $category)
                            <li class="dd-item" data-id="{{ $category['id'] }}">
                                <div class="dd-handle dd3-handle">{{ $category['name'] }}</div>
                                <div class="editdelete">
                                    <span style="float: right">
                                    &nbsp;<a style="color: red" class="btnDelete" data-id="{{ $category['id'] }}">Delete</a></span>
                                    <span style="float: right"><a style="color: blue" class="btnEdit" data-id="{{ $category['id'] }}" data-index="{{ $loop->index }}">Edit</a> |
                                    </span>
                                </div>

                                @if (sizeof($category['subCategories']) > 0)
                                    <ol class="dd-list">
                                        @foreach($category['subCategories'] as $sub)
                                            <li class="dd-item" data-id="{{ $sub['id'] }}">
                                                <div class="dd-handle dd3-handle">{{ $sub['name'] }}</div>
                                                <div class="editdelete">
                                                    <span style="float: right">
                                                    &nbsp;<a style="color: red" class="btnDelete" data-id="{{ $sub['id'] }}">Delete</a></span>

                                                    <span style="float: right"><a style="color: blue" class="btnEdit" data-id="{{ $sub['id'] }}" data-index="{{ $loop->index }}" data-parent="{{ $loop->parent->index }}">Edit
                                                    </a> |</span>
                                                </div>
                                            </li>
                                        @endforeach
                                    </ol>
                                @endif
                            </li>
                        @endforeach
                    </ol>
                </div>
            </div>
        </div>

        <div class="col-md-6 {{ ($errors && sizeof($errors) > 0) ? '' : 'd-none' }}" id="addEditRow">
            <div class="addcategory">
                <h3><span id="addEditTitle">{{ old('inputAdd') == '0' ? 'Edit Category' : 'Add Category' }}</span></h3>

                <form class="form-horizontal" enctype="multipart/form-data" id="form"
                        method="post" action="{{ (old('inputAdd') == '1') ? route('vendor_category_add_post') : route('vendor_category_edit_post') }}">
                    @csrf

                    <input type="hidden" name="inputAdd" id="inputAdd" value="{{ old('inputAdd') }}">
                    <input type="hidden" name="categoryId" id="categoryId" value="{{ old('categoryId') }}">

                    <div class="form-group row">
                        <div class="col-lg-3">
                            <label for="status" class="col-form-label">Status</label>
                        </div>

                        <div class="col-lg-5">
                            <label for="statusActive" class="custom-control custom-radio">
                                <input id="statusActive" name="status" type="radio" class="custom-control-input"
                                       value="1" {{ (old('status') == '1' || empty(old('status'))) ? 'checked' : '' }}>
                                <span class="custom-control-indicator"></span>
                                <span class="custom-control-description">Active</span>
                            </label>
                            <label for="statusInactive" class="custom-control custom-radio signin_radio4">
                                <input id="statusInactive" name="status" type="radio" class="custom-control-input" value="0" {{ old('status') == '0' ? 'checked' : '' }}>
                                <span class="custom-control-indicator"></span>
                                <span class="custom-control-description">Inactive</span>
                            </label>
                        </div>
                    </div>

                    <div class="form-group row">
                        <div class="col-lg-3">
                            <label for="parent_category" class="col-form-label">Parent Category</label>
                        </div>

                        <div class="col-lg-5">
                            <select class="form-control" name="parent_category" id="parent_category">
                                <option value="">Select Parent</option>

                                @foreach($parentCategories as $item)
                                    <option value="{{ $item->id }}" {{ old('parent_category') == $item->id ? 'selected' : '' }}>{{ $item->name }}</option>
                                @endforeach
                            </select>
                        </div>
                    </div>

                    <div class="form-group row{{ $errors->has('category_name') ? ' has-danger' : '' }}">
                        <div class="col-lg-3">
                            <label for="category_name" class="col-form-label">My Category Name *</label>
                        </div>

                        <div class="col-lg-5">
                            <input type="text" id="category_name" class="form-control{{ $errors->has('category_name') ? ' is-invalid' : '' }}"
                                   name="category_name" value="{{ old('category_name') }}">
                        </div>
                    </div>

                    <div class="form-group row">
                        <div class="col-lg-3">
                            <label for="master_color" class="col-form-label">Default Category *</label>
                        </div>

                        <div class="col-lg-3">
                            <select class="form-control{{ $errors->has('d_parent_category') ? ' is-invalid' : '' }}" name="d_parent_category" id="d_parent_category">
                                <option value="">Select Category</option>

                                @foreach($defaultCategories as $item)
                                    <option value="{{ $item['id'] }}" data-index="{{ $loop->index }}" {{ old('d_parent_category') == $item['id'] ? 'selected' : '' }}>{{ $item['name'] }}</option>
                                @endforeach
                            </select>
                        </div>

                        <div class="col-lg-3">
                            <select class="form-control" name="d_second_parent_category" id="d_second_parent_category">
                                <option value="">Select Category</option>
                            </select>
                        </div>

                        <div class="col-lg-3">
                            <select class="form-control" name="d_third_parent_category" id="d_third_parent_category">
                                <option value="">Select Category</option>
                            </select>
                        </div>
                    </div>

                    <div class="form-group row{{ $errors->has('sort_no') ? ' has-danger' : '' }}">
                        <div class="col-lg-3">
                            <label for="sort_no" class="col-form-label">Sorting No. *</label>
                        </div>

                        <div class="col-lg-5">
                            <input type="text" id="sort_no" class="form-control{{ $errors->has('sort_no') ? ' is-invalid' : '' }}"
                                   name="sort_no" value="{{ old('sort_no') }}">
                        </div>
                    </div>

                    <div class="form-group row{{ $errors->has('discount') ? ' has-danger' : '' }}">
                        <div class="col-lg-3">
                            <label for="discount" class="col-form-label">Discount (%)</label>
                        </div>

                        <div class="col-lg-5">
                            <input type="text" id="discount" class="form-control{{ $errors->has('discount') ? ' is-invalid' : '' }}"
                                   name="discount" value="{{ old('discount') }}">
                        </div>
                    </div>

                    <div class="form-group row">
                        <div class="col-lg-12 text-right">
                            <button class="btn btn-default" id="btnCancel">Cancel</button>
                            <input type="submit" id="btnSubmit" class="btn btn-primary" value="{{ old('inputAdd') == '0' ? 'Update' : 'Add' }}">
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </div>

    <div class="modal fade" id="deleteModal" role="dialog" aria-labelledby="deleteModal">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-header bg-danger">
                    <h4 class="modal-title text-white" id="deleteModal">Delete</h4>
                </div>
                <div class="modal-body">
                    <p>
                        Are you sure want to delete?
                    </p>
                </div>
                <div class="modal-footer">
                    <button class="btn  btn-default" data-dismiss="modal">Close</button>
                    <button class="btn  btn-danger" id="modalBtnDelete">Delete</button>
                </div>
            </div>
        </div>
        <!--- end modals-->
    </div>
@stop

@section('additionalJS')
    <script type="text/javascript" src="{{ asset('plugins/Nestable/jquery.nestable.js') }}"></script>
    <script type="text/javascript" src="{{ asset('plugins/toastr/toastr.min.js') }}"></script>
    <script>
        $(function () {
            $.ajaxSetup({
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                }
            });

            var message = '{{ session('message') }}';

            if (message != '')
                toastr.success(message);

            var defaultCategories = <?php echo json_encode($defaultCategories); ?>;
            var d_parent_index, selectedId;
            var d_second_id = '{{ old('d_second_parent_category') }}';
            var d_third_id = '{{ old('d_third_parent_category') }}';

            $('#nestable').nestable({
                maxDepth: 2
            }).on('change', '.dd-item', function(e) {
                e.stopPropagation();

                var itemArray = $('.dd').nestable('serialize');

                var id = $(this).data('id'),
                    parentId = $(this).parents('.dd-item').data('id');

                if (parentId == undefined)
                    parentId = 0;

                $.ajax({
                    method: "POST",
                    url: "{{ route('vendor_sort_category') }}",
                    data: { itemArray: itemArray }
                }).done(function( msg ) {
                    toastr.success('Category Updated!');
                });
            });

            $('#btnAddNew').click(function () {
                $('#addEditRow').removeClass('d-none');
                $('#addEditTitle').html('Add Category');
                $('#btnSubmit').val('Add');
                $('#inputAdd').val('1');
                $('#form').attr('action', '{{ route('vendor_category_add_post') }}');
            });

            $('#btnCancel').click(function (e) {
                e.preventDefault();

                $('#addEditRow').addClass('d-none');

                // Clear form
                $('#statusActive').prop('checked', true);
                $('#parent_category').val('');
                $('#category_name').val('');
                $('#d_parent_category').val('');
                $('#d_second_parent_category').val('');
                $('#d_third_parent_category').val('');
                $('#sort_no').val('');
                $('#discount').val('');

                $('input').removeClass('is-invalid');
                $('.form-group').removeClass('has-danger');
            });
            
            $('#d_parent_category').change(function () {
                $('#d_second_parent_category').html('<option value="">Select Category</option>');
                $('#d_third_parent_category').html('<option value="">Select Category</option>');

                if ($(this).val() != '') {
                    var index = $(this).find(':selected').data('index');
                    d_parent_index = index;

                    var childrens = defaultCategories[index].subCategories;

                    $.each(childrens, function (index, value) {
                        if (value.id == d_second_id)
                            $('#d_second_parent_category').append('<option data-index="' + index + '" value="' + value.id + '" selected>' + value.name + '</option>');
                        else
                            $('#d_second_parent_category').append('<option data-index="' + index + '" value="' + value.id + '">' + value.name + '</option>');
                    });
                }
            });

            $('#d_parent_category').trigger('change');

            $('#d_second_parent_category').change(function () {
                $('#d_third_parent_category').html('<option value="">Select Category</option>');

                if ($(this).val() != '') {
                    var index = $(this).find(':selected').attr('data-index');

                    var childrens = defaultCategories[d_parent_index].subCategories[index].subCategories;

                    $.each(childrens, function (index, value) {
                        if (value.id == d_third_id)
                            $('#d_third_parent_category').append('<option data-index="' + index + '" value="' + value.id + '" selected>' + value.name + '</option>');
                        else
                            $('#d_third_parent_category').append('<option data-index="' + index + '" value="' + value.id + '">' + value.name + '</option>');
                    });
                }
            });

            $('#d_second_parent_category').trigger('change');

            $('.btnEdit').click(function () {
                var id = $(this).data('id');

                $.ajax({
                    method: "POST",
                    url: "{{ route('vendor_category_detail') }}",
                    data: { id: id }
                }).done(function( data ) {
                    $('#addEditRow').removeClass('d-none');
                    $('#addEditTitle').html('Edit Category');
                    $('#btnSubmit').val('Update');
                    $('#inputAdd').val('0');
                    $('#form').attr('action', '{{ route('vendor_category_edit_post') }}');
                    $('#categoryId').val(id);

                    if (data.status == 1)
                        $('#statusActive').prop('checked', true);
                    else
                        $('#statusInactive').prop('checked', true);

                    if (data.parent != 0)
                        $('#parent_category').val(data.parent);
                    else
                        $('#parent_category').val('');

                    $('#category_name').val(data.name);
                    $('#d_parent_category').val(data.d_category_id);
                    d_second_id = data.d_category_second_id;
                    d_third_id = data.d_category_third_id;
                    $('#d_parent_category').trigger('change');
                    $('#d_second_parent_category').trigger('change');
                    $('#sort_no').val(data.sort);
                    $('#discount').val(data.discount);
                });
            });

            $('.btnDelete').click(function () {
                $('#deleteModal').modal('show');
                selectedId = $(this).data('id');
            });

            $('#modalBtnDelete').click(function () {
                $.ajax({
                    method: "POST",
                    url: "{{ route('vendor_category_delete') }}",
                    data: { id: selectedId }
                }).done(function( msg ) {
                    location.reload();
                });
            });
        });
    </script>
@stop