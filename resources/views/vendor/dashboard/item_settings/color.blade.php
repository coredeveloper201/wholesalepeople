@extends('vendor.layouts.dashboard')

@section('additionalCSS')
    <link href="{{ asset('plugins/toastr/toastr.min.css') }}" rel="stylesheet">
@stop

@section('content')
    <form action="{{ route('vendor_color') }}">
        <div class="row">
            <div class="col-md-4">
                <input type="text" class="form-control" placeholder="Search" name="s" value="{{ request()->get('s') }}">
            </div>

            <div class="col-md-2">
                <input type="submit" class="btn btn-primary" value="Search">
            </div>
        </div>
    </form>

    <br>

    <div class="row {{ ($errors && sizeof($errors) > 0) ? 'd-none' : '' }}" id="addBtnRow">
        <div class="col-md-12">
            <button class="btn btn-primary" id="btnAddNew">Add New Color</button>
        </div>
    </div>

    <div class="row {{ ($errors && sizeof($errors) > 0) ? '' : 'd-none' }}" id="addEditRow">
        <div class="col-md-12" style="border: 1px solid black">
            <h3><span id="addEditTitle">{{ old('inputAdd') == '0' ? 'Edit Color' : 'Add Color' }}</span></h3>

            <form class="form-horizontal" enctype="multipart/form-data" id="form"
                  method="post" action="{{ (old('inputAdd') == '1') ? route('vendor_color_add_post') : route('vendor_color_edit_post') }}">
                @csrf

                <input type="hidden" name="inputAdd" id="inputAdd" value="{{ old('inputAdd') }}">
                <input type="hidden" name="colorId" id="colorId" value="{{ old('colorId') }}">

                <div class="form-group row">
                    <div class="col-lg-2">
                        <label for="status" class="col-form-label">Status</label>
                    </div>

                    <div class="col-lg-5">
                        <label for="statusActive" class="custom-control custom-radio">
                            <input id="statusActive" name="status" type="radio" class="custom-control-input"
                                   value="1" {{ (old('status') == '1' || empty(old('status'))) ? 'checked' : '' }}>
                            <span class="custom-control-indicator"></span>
                            <span class="custom-control-description">Active</span>
                        </label>
                        <label for="statusInactive" class="custom-control custom-radio signin_radio4">
                            <input id="statusInactive" name="status" type="radio" class="custom-control-input" value="0" {{ old('status') == '0' ? 'checked' : '' }}>
                            <span class="custom-control-indicator"></span>
                            <span class="custom-control-description">Inactive</span>
                        </label>
                    </div>
                </div>

                <div class="form-group row{{ $errors->has('color_name') ? ' has-danger' : '' }}">
                    <div class="col-lg-2">
                        <label for="color_name" class="col-form-label">Color Name *</label>
                    </div>

                    <div class="col-lg-5">
                        <input type="text" id="color_name" class="form-control{{ $errors->has('color_name') ? ' is-invalid' : '' }}"
                               placeholder="Color Name" name="color_name" value="{{ old('color_name') }}">
                    </div>
                </div>

                <div class="form-group row{{ $errors->has('master_color') ? ' has-danger' : '' }}">
                    <div class="col-lg-2">
                        <label for="master_color" class="col-form-label">Master Color *</label>
                    </div>

                    <div class="col-lg-5">
                        <select class="form-control{{ $errors->has('master_color') ? ' is-invalid' : '' }}" name="master_color" id="master_color">
                            <option value="">Select Master Color</option>

                            @foreach($masterColors as $color)
                                <option value="{{ $color->id }}" {{ old('master_color') == $color->id ? 'selected' : '' }}>{{ $color->name }}</option>
                            @endforeach
                        </select>
                    </div>
                </div>

                <div class="form-group row{{ $errors->has('photo') ? ' has-danger' : '' }}">
                    <div class="col-lg-2">
                        <label for="code" class="col-form-label">Image (20px x 20px)</label>
                    </div>

                    <div class="col-lg-5">
                        <input class="form-control{{ $errors->has('photo') ? ' is-invalid' : '' }}"
                               type="file" id="photo" name="photo" accept="image/*">
                    </div>
                </div>

                <div class="form-group row">
                    <div class="col-lg-12 text-right">
                        <button class="btn btn-default" id="btnCancel">Cancel</button>
                        <input type="submit" id="btnSubmit" class="btn btn-primary" value="{{ old('inputAdd') == '0' ? 'Update' : 'Add' }}">
                    </div>
                </div>
            </form>
        </div>
    </div>

    <br>

    <div class="row">
        <div class="col-md-12">
            <table class="table table-striped">
                <thead>
                    <tr>
                        <th></th>
                        <th>Name</th>
                        <th>Action</th>
                    </tr>
                </thead>

                <tbody>
                    @foreach($colors as $color)
                        <tr>
                            <td>
                                <img src="{{ ($color->thumbs_image_path) ? asset($color->thumbs_image_path) : asset('images/no-image.png') }}" height="50px" width="50px">
                            </td>

                            <td>{{ $color->name }}</td>
                            <td>
                                <a class="btnEdit" data-id="{{ $color->id }}" data-index="{{ $loop->index }}" role="button" style="color: blue">Edit</a> |
                                <a class="btnDelete" data-id="{{ $color->id }}" role="button" style="color: red">Delete</a>
                            </td>
                        </tr>
                    @endforeach
                </tbody>
            </table>

            <div class="pagination">
                {{ $colors->appends($appends)->links() }}
            </div>
        </div>
    </div>

    <div class="modal fade" id="deleteModal" role="dialog" aria-labelledby="deleteModal">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-header bg-danger">
                    <h4 class="modal-title text-white" id="deleteModal">Delete</h4>
                </div>
                <div class="modal-body">
                    <p>
                        Are you sure want to delete?
                    </p>
                </div>
                <div class="modal-footer">
                    <button class="btn  btn-default" data-dismiss="modal">Close</button>
                    <button class="btn  btn-danger" id="modalBtnDelete">Delete</button>
                </div>
            </div>
        </div>
        <!--- end modals-->
    </div>
@stop

@section('additionalJS')
    <script type="text/javascript" src="{{ asset('plugins/toastr/toastr.min.js') }}"></script>
    <script>
        $(function () {
            $.ajaxSetup({
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                }
            });

            var colors = <?php echo json_encode($colors->toArray()); ?>;
            colors = colors.data;
            var selectedId;
            var message = '{{ session('message') }}';

            if (message != '')
                toastr.success(message);

            $('#btnAddNew').click(function () {
                $('#addEditRow').removeClass('d-none');
                $('#addBtnRow').addClass('d-none');
                $('#addEditTitle').html('Add Color');
                $('#btnSubmit').val('Add');
                $('#inputAdd').val('1');
                $('#form').attr('action', '{{ route('vendor_color_add_post') }}');
            });

            $('#btnCancel').click(function (e) {
                e.preventDefault();

                $('#addEditRow').addClass('d-none');
                $('#addBtnRow').removeClass('d-none');

                // Clear form
                $('#statusActive').prop('checked', true);
                $('#color_name').val('');
                $('#master_color').val('');

                $('input').removeClass('is-invalid');
                $('.form-group').removeClass('has-danger');
                $('#master_color').removeClass('is-invalid');
            });

            $('.btnEdit').click(function () {
                var id = $(this).data('id');
                var index = $(this).data('index');

                $('#addEditRow').removeClass('d-none');
                $('#addBtnRow').addClass('d-none');
                $('#addEditTitle').html('Edit Color');
                $('#btnSubmit').val('Update');
                $('#inputAdd').val('0');
                $('#form').attr('action', '{{ route('vendor_color_edit_post') }}');
                $('#colorId').val(id);

                var color = colors[index];

                if (color.status == 1)
                    $('#statusActive').prop('checked', true);
                else
                    $('#statusInactive').prop('checked', true);

                $('#color_name').val(color.name);
                $('#master_color').val(color.master_color_id);
            });

            $('.btnDelete').click(function () {
                $('#deleteModal').modal('show');
                selectedId = $(this).data('id');
            });

            $('#modalBtnDelete').click(function () {
                $.ajax({
                    method: "POST",
                    url: "{{ route('vendor_color_delete') }}",
                    data: { id: selectedId }
                }).done(function( msg ) {
                    location.reload();
                });
            });
        })
    </script>
@stop