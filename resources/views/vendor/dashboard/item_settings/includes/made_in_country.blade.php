<div class="row">
    <div class="col-md-12">
        <button class="btn btn-primary" id="btnAddNewMadeInCountry">Add New Made In Country</button>
    </div>
</div>

<div class="row d-none" id="addEditRowMadeInCountry">
    <div class="col-md-12">
        <h3><span id="addEditTitleMadeInCountry"></span></h3>

        <div class="form-group row">
            <div class="col-lg-2">
                <label for="status" class="col-form-label">Status</label>
            </div>

            <div class="col-lg-5">
                <label for="statusActiveMadeInCountry" class="custom-control custom-radio">
                    <input id="statusActiveMadeInCountry" name="statusMadeInCountry" type="radio" class="custom-control-input"
                           value="1" checked>
                    <span class="custom-control-indicator"></span>
                    <span class="custom-control-description">Active</span>
                </label>
                <label for="statusInactiveMadeInCountry" class="custom-control custom-radio signin_radio4">
                    <input id="statusInactiveMadeInCountry" name="statusMadeInCountry" type="radio" class="custom-control-input" value="0">
                    <span class="custom-control-indicator"></span>
                    <span class="custom-control-description">Inactive</span>
                </label>
            </div>
        </div>

        <div class="form-group row">
            <div class="col-lg-2">
                <label for="color_name" class="col-form-label">Made In Country *</label>
            </div>

            <div class="col-lg-5">
                <input type="text" id="madeInCountryName" class="form-control">
            </div>
        </div>

        <div class="form-group row">
            <div class="col-lg-1">
                <label for="defaultMadeInCountry" class="col-form-label">Default</label>
            </div>

            <div class="col-lg-5">
                <label class="custom-control custom-checkbox">
                    <input type="checkbox" class="custom-control-input" id="defaultMadeInCountry" value="1" name="defaultMadeInCountry">
                    <span class="custom-control-indicator"></span>
                </label>
            </div>
        </div>

        <div class="form-group row">
            <div class="col-lg-12 text-right">
                <button class="btn btn-default" id="btnCancelMadeInCountry">Cancel</button>
                <button id="btnAddMadeInCountry" class="btn btn-primary">Add</button>
                <button id="btnUpdateMadeInCountry" class="btn btn-primary">Update</button>
            </div>
        </div>
    </div>
</div>

<br>

<table class="table table-striped">
    <thead>
        <tr>
            <th>#</th>
            <th>Made In Country</th>
            <th>Active</th>
            <th>Default</th>
            <th>Action</th>
        </tr>
    </thead>

    <tbody id="madeInCountryTbody">
        @foreach($madeInCountries as $country)
            <tr>
                <td>{{ $loop->iteration }}</td>
                <td><span class="madeInCountryName">{{ $country->name }}</span></td>
                <td>
                    <label class="custom-control custom-checkbox">
                        <input type="checkbox" data-id="{{ $country->id }}" class="custom-control-input statusMadeInCountry"
                               value="1" {{ $country->status == 1 ? 'checked' : '' }}>
                        <span class="custom-control-indicator"></span>
                    </label>
                </td>
                <td>
                    <label class="custom-control custom-radio">
                        <input type="radio" name="defaultMadeInCountryTable" class="custom-control-input defaultMadeInCountry" data-id="{{ $country->id }}"
                               value="1" {{ $country->default == 1 ? 'checked' : '' }}>
                        <span class="custom-control-indicator"></span>
                    </label>
                </td>
                <td>
                    <a class="btnEditMadeInCountry" data-id="{{ $country->id }}" data-index="{{ $loop->index }}" role="button" style="color: blue">Edit</a> |
                    <a class="btnDeleteMadeInCountry" data-id="{{ $country->id }}" data-index="{{ $loop->index }}" role="button" style="color: red">Delete</a>
                </td>
            </tr>
        @endforeach
    </tbody>
</table>

<template id="madeInCountryTrTemplate">
    <tr>
        <td><span class="madeInCountryIndex"></span></td>
        <td><span class="madeInCountryName"></span></td>
        <td>
            <label class="custom-control custom-checkbox">
                <input type="checkbox" class="custom-control-input statusMadeInCountry"
                       value="1">
                <span class="custom-control-indicator"></span>
            </label>
        </td>
        <td>
            <label class="custom-control custom-radio">
                <input type="radio" name="defaultMadeInCountryTable" class="custom-control-input defaultMadeInCountry"
                       value="1">
                <span class="custom-control-indicator"></span>
            </label>
        </td>
        <td>
            <a class="btnEditMadeInCountry" role="button" style="color: blue">Edit</a> |
            <a class="btnDeleteMadeInCountry" role="button" style="color: red">Delete</a>
        </td>
    </tr>
</template>

<div class="modal fade" id="deleteModalMadeInCountry" role="dialog" aria-labelledby="deleteModalMadeInCountry">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header bg-danger">
                <h4 class="modal-title text-white" id="deleteModalMadeInCountry">Delete</h4>
            </div>
            <div class="modal-body">
                <p>
                    Are you sure want to delete?
                </p>
            </div>
            <div class="modal-footer">
                <button class="btn  btn-default" data-dismiss="modal">Close</button>
                <button class="btn  btn-danger" id="modalBtnDeleteMadeInCountry">Delete</button>
            </div>
        </div>
    </div>
    <!--- end modals-->
</div>