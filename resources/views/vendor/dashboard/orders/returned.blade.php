@extends('vendor.layouts.dashboard')

@section('content')
    <div class="table-responsive">
        <table class="table">
            <thead>
            <tr>
                <th>Order #</th>
                <th>Company Name</th>
                <th>Order Date</th>
                <th>Total</th>
                <th>Shipping Method</th>
                <th># of Orders</th>
                <th>Action</th>
            </tr>
            </thead>

            <tbody>
            @foreach($orders as $order)
                <tr>
                    <td><a class="text-primary" href="{{ route('vendor_order_details', ['order' => $order->id]) }}">{{ $order->order_number }}</a></td>
                    <td>{{ $order->company_name }}</td>
                    <td>{{ date('F d, Y', strtotime($order->created_at)) }}</td>
                    <td>${{ sprintf('%0.2f', $order->total) }}</td>
                    <td>{{ $order->shipping }}</td>
                    <td>{{ $order->count }}</td>
                    <td>
                        <a class="btnDelete" data-id="{{ $order->id }}" role="button" style="color: red">Delete</a>
                    </td>
                </tr>
            @endforeach
            </tbody>
        </table>
    </div>

    <div class="modal fade" id="deleteModal" role="dialog" aria-labelledby="deleteModal">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-header bg-danger">
                    <h4 class="modal-title text-white" id="deleteModal">Delete</h4>
                </div>
                <div class="modal-body">
                    <p>
                        Are you sure want to delete?
                    </p>
                </div>
                <div class="modal-footer">
                    <button class="btn  btn-default" data-dismiss="modal">Close</button>
                    <button class="btn  btn-danger" id="modalBtnDelete">Delete</button>
                </div>
            </div>
        </div>
        <!--- end modals-->
    </div>
@stop

@section('additionalJS')
    <script>
        $(function () {
            $.ajaxSetup({
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                }
            });

            var selectedId;

            $('.btnDelete').click(function () {
                $('#deleteModal').modal('show');
                selectedId = $(this).data('id');
            });

            $('#modalBtnDelete').click(function () {
                $.ajax({
                    method: "POST",
                    url: "{{ route('vendor_delete_order') }}",
                    data: { id: selectedId }
                }).done(function( msg ) {
                    location.reload();
                });
            });
        });
    </script>
@stop