<?php use App\Enumeration\OrderStatus; ?>

@extends('vendor.layouts.dashboard')

@section('additionalCSS')
    <link href="{{ asset('plugins/daterangepicker/css/daterangepicker.css') }}" rel="stylesheet">
@stop

@section('content')
<div id="accordionSearch" role="tablist">
    <div class="card">
        <div class="card-header" role="tab" id="headingSearch">
            <h5 class="mb-0">
                <a data-toggle="collapse" href="#collapseSearch" role="button" aria-expanded="true" aria-controls="collapseSearch" class="">Search</a>
            </h5>
        </div>
        <div id="collapseSearch" class="collapse show" role="tabpanel" aria-labelledby="headingSearch" data-parent="#accordionSearch" style="">
            <div class="card-body">
                <div class="search-container">
                    <div class="row">
                        <div class="col-md-2">
                            <select class="form-control" id="selectOrderDate">
                                <option value="0" {{ (request()->get('date') == '0') ? 'selected' : '' }}>Input Period</option>
                                <option value="1" {{ (request()->get('date') == '1') ? 'selected' : '' }}>Today</option>
                                <option value="2" {{ (request()->get('date') == '2') ? 'selected' : '' }}>This Week</option>
                                <option value="3" {{ (request()->get('date') == '3') ? 'selected' : '' }}>This Month</option>
                                <option value="5" {{ (request()->get('date') == '5') ? 'selected' : '' }}>This Year</option>
                                <option value="6" {{ (request()->get('date') == '6') ? 'selected' : '' }}>Yesterday</option>
                                <option value="8" {{ (request()->get('date') == '8') ? 'selected' : '' }}>Last Month</option>
                                <option value="10" {{ (request()->get('date') == '10') ? 'selected' : '' }}>Last Year</option>
                                <option value="13" {{ (request()->get('date') == '13') ? 'selected' : '' }}>Last 7 Days</option>
                                <option value="14" {{ (request()->get('date') == '14') ? 'selected' : '' }}>Last 30 Days</option>
                                <option value="15" {{ (request()->get('date') == '15' || request()->get('date') == null) ? 'selected' : '' }}>Last 90 Days</option>
                                <option value="16" {{ (request()->get('date') == '16') ? 'selected' : '' }}>Last 365 Days</option>
                            </select>
                        </div>

                        <div class="col-md-2 d-none" id="date-range">
                            <div class="input-group">
                                <span class="input-group-addon">
                                    <i class="fa fa-calendar"></i>
                                </span>
                                <input type="text" id="dateRange" class="form-control"
                                       value="{{ (request()->get('startDate') != null ) ? request()->get('startDate').' - '.request()->get('endDate') : '' }}">
                            </div>
                        </div>

                        <div class="col-md-2">
                            <select class="form-control" id="searchItem">
                                <option value="1" {{ request()->get('search') == '1' ? 'selected' : '' }}>Company Name</option>
                                <option value="2" {{ request()->get('search') == '2' ? 'selected' : '' }}>Order Number</option>
                                <option value="3" {{ request()->get('search') == '3' ? 'selected' : '' }}>Tracking No.</option>
                            </select>
                        </div>

                        <div class="col-md-3">
                            <div class="input-group">
                            <span class="input-group-addon">
                                <i class="fa fa-search"></i>
                            </span>
                                <input type="text" class="form-control" id="inputText" value="{{ request()->get('text') }}">
                            </div>
                        </div>
                    </div>

                    <br>

                    <div class="row">
                        <div class="col-md-2">
                            <select class="form-control" id="selectShipStatus">
                                <option value="">Shipped Status</option>
                                <option value="1" {{ request()->get('ship') == '1' ? 'selected' : '' }}>Partially Shipped</option>
                                <option value="2" {{ request()->get('ship') == '2' ? 'selected' : '' }}>Fully Shipped</option>
                            </select>
                        </div>

                        <div class="col-md-10">
                            <button class="btn btn-primary" id="btnApply">Apply</button>
                            <button class="btn btn-default" id="btnReset">Reset All</button>
                        </div>
                    </div>
                </div>

            </div>
        </div>
    </div>
</div><br>

<div id="accordion" role="tablist">
    <div class="card">
        <div class="card-header" role="tab" id="headingOne">
            <h5 class="mb-0">
                <a data-toggle="collapse" href="#collapseOne" role="button" aria-expanded="true" aria-controls="collapseOne" class="">New Orders</a>
            </h5>
        </div>
        <div id="collapseOne" class="collapse show" role="tabpanel" aria-labelledby="headingOne" data-parent="#accordion" style="">
            <div class="card-body">
                <table class="table table-bordered">
                    <thead>
                        <tr>
                            <th></th>
                            <th>Date</th>
                            <th>Order #</th>
                            <th>Company Name</th>
                            <th>Amount</th>
                            <th>Shipping Method</th>
                            <th># of Orders</th>
                            <th>Status</th>
                            <th>Action</th>
                        </tr>
                    </thead>

                    <tbody>
                        <?php $total = 0; ?>
                        @foreach($newOrders as $order)
                            <tr>
                                <td>
                                    <label class="custom-control custom-checkbox">
                                        <input type="checkbox" class="custom-control-input checkbox-order" data-id="{{ $order->id }}">
                                        <span class="custom-control-indicator"></span>
                                    </label>
                                </td>
                                <td>{{ date('m/d/Y', strtotime($order->created_at)) }}</td>
                                <td><a class="text-primary" href="{{ route('vendor_order_details', ['order' => $order->id]) }}">{{ $order->order_number }}</a></td>
                                <td>{{ $order->company_name }}</td>
                                <td>${{ sprintf('%0.2f', $order->total) }}</td>
                                <td>{{ $order->shipping }}</td>
                                <td>{{ $order->count }}</td>
                                <td>
                                    @if ($order->status == OrderStatus::$NEW_ORDER)
                                        New Order
                                    @elseif ($order->status == OrderStatus::$CONFIRM_ORDER)
                                        Confirmed Orders
                                    @elseif ($order->status == OrderStatus::$PARTIALLY_SHIPPED_ORDER)
                                        Partially Shipped Orders
                                    @elseif ($order->status == OrderStatus::$FULLY_SHIPPED_ORDER)
                                        Fully Shipped Orders
                                    @elseif ($order->status == OrderStatus::$BACK_ORDER)
                                        Back Ordered
                                    @elseif ($order->status == OrderStatus::$CANCEL_BY_BUYER)
                                        Cancelled by Buyer
                                    @elseif ($order->status == OrderStatus::$CANCEL_BY_VENDOR)
                                        Cancelled by Vendor
                                    @elseif ($order->status == OrderStatus::$CANCEL_BY_AGREEMENT)
                                        Cancelled by Agreement
                                    @elseif ($order->status == OrderStatus::$RETURNED)
                                        Returned
                                    @endif
                                </td>
                                <td>
                                    <a class="btnDelete" data-id="{{ $order->id }}" role="button" style="color: red">Delete</a>
                                </td>
                            </tr>

                            <?php $total += $order->total; ?>
                        @endforeach

                        @if (sizeof($newOrders) > 0)
                            <tr>
                                <td colspan="4">
                                    <button class="btn btn-primary btnPacklistOrder">Packlist</button>
                                    <button class="btn btn-primary btnPrintInvoiceOrder">Print Invoice</button>
                                </td>

                                <th>${{ number_format($totals['new'], 2, '.', '') }}</th>

                                <td colspan="4"></td>
                            </tr>
                        @endif
                    </tbody>
                </table>
                {{ $newOrders->appends(array_merge($appends, ['c' => 'new']))->links() }}
            </div>
        </div>
    </div>
</div><br>
<div id="accordiontwo" role="tablist">
    <div class="card">
        <div class="card-header" role="tab" id="headingTwo">
            <h5 class="mb-0">
                <a data-toggle="collapse" href="#collapseTwo" role="button" aria-expanded="true" aria-controls="collapseTwo" class="">Confirmed Order</a>
            </h5>
        </div>
        <div id="collapseTwo" class="collapse show" role="tabpanel" aria-labelledby="headingOne" data-parent="#accordiontwo" style="">
            <div class="card-body">
                <table class="table table-bordered">
                    <thead>
                        <tr>
                            <th></th>
                            <th>Date</th>
                            <th>Order #</th>
                            <th>Company Name</th>
                            <th>Amount</th>
                            <th>Shipping Method</th>
                            <th># of Orders</th>
                            <th>Status</th>
                            <th>Action</th>
                        </tr>
                    </thead>

                    <tbody>
                        @foreach($confirmOrders as $order)
                            <tr>
                                <td>
                                    <label class="custom-control custom-checkbox">
                                        <input type="checkbox" class="custom-control-input checkbox-order" data-id="{{ $order->id }}">
                                        <span class="custom-control-indicator"></span>
                                    </label>
                                </td>
                                <td>{{ date('m/d/Y', strtotime($order->created_at)) }}</td>
                                <td><a class="text-primary" href="{{ route('vendor_order_details', ['order' => $order->id]) }}">{{ $order->order_number }}</a></td>
                                <td>{{ $order->company_name }}</td>
                                <td>${{ sprintf('%0.2f', $order->total) }}</td>
                                <td>{{ $order->shipping }}</td>
                                <td>{{ $order->count }}</td>
                                <td>
                                    @if ($order->status == OrderStatus::$NEW_ORDER)
                                        New Order
                                    @elseif ($order->status == OrderStatus::$CONFIRM_ORDER)
                                        Confirmed Orders
                                    @elseif ($order->status == OrderStatus::$PARTIALLY_SHIPPED_ORDER)
                                        Partially Shipped Orders
                                    @elseif ($order->status == OrderStatus::$FULLY_SHIPPED_ORDER)
                                        Fully Shipped Orders
                                    @elseif ($order->status == OrderStatus::$BACK_ORDER)
                                        Back Ordered
                                    @elseif ($order->status == OrderStatus::$CANCEL_BY_BUYER)
                                        Cancelled by Buyer
                                    @elseif ($order->status == OrderStatus::$CANCEL_BY_VENDOR)
                                        Cancelled by Vendor
                                    @elseif ($order->status == OrderStatus::$CANCEL_BY_AGREEMENT)
                                        Cancelled by Agreement
                                    @elseif ($order->status == OrderStatus::$RETURNED)
                                        Returned
                                    @endif
                                </td>
                                <td>
                                    <a class="btnDelete" data-id="{{ $order->id }}" role="button" style="color: red">Delete</a>
                                </td>
                            </tr>
                        @endforeach

                        @if (sizeof($confirmOrders) > 0)
                            <tr>
                                <td colspan="4">
                                    <button class="btn btn-primary btnPacklistOrder">Packlist</button>
                                    <button class="btn btn-primary btnPrintInvoiceOrder">Print Invoice</button>
                                </td>

                                <th>${{ number_format($totals['confirm'], 2, '.', '') }}</th>

                                <td colspan="4"></td>
                            </tr>
                        @endif
                    </tbody>
                </table>
                {{ $confirmOrders->appends(array_merge($appends, ['c' => 'confirm']))->links() }}
            </div>
        </div>
    </div>
</div><br>
<div id="accordionthree" role="tablist">
    <div class="card">
        <div class="card-header" role="tab" id="headingThree">
            <h5 class="mb-0">
                <a data-toggle="collapse" href="#collapseThree" role="button" aria-expanded="true" aria-controls="collapseTwo" class="">Back Ordered</a>
            </h5>
        </div>
        <div id="collapseThree" class="collapse show" role="tabpanel" aria-labelledby="headingOne" data-parent="#accordionthree" style="">
            <div class="card-body">
                <table class="table table-bordered">
                    <thead>
                        <tr>
                            <th></th>
                            <th>Date</th>
                            <th>Order #</th>
                            <th>Company Name</th>
                            <th>Amount</th>
                            <th>Shipping Method</th>
                            <th># of Orders</th>
                            <th>Status</th>
                            <th>Action</th>
                        </tr>
                    </thead>

                    <tbody>
                         @foreach($backOrders as $order)
                            <tr>
                                <td>
                                    <label class="custom-control custom-checkbox">
                                        <input type="checkbox" class="custom-control-input checkbox-order" data-id="{{ $order->id }}">
                                        <span class="custom-control-indicator"></span>
                                    </label>
                                </td>
                                <td>{{ date('m/d/Y', strtotime($order->created_at)) }}</td>
                                <td><a class="text-primary" href="{{ route('vendor_order_details', ['order' => $order->id]) }}">{{ $order->order_number }}</a></td>
                                <td>{{ $order->company_name }}</td>
                                <td>${{ sprintf('%0.2f', $order->total) }}</td>
                                <td>{{ $order->shipping }}</td>
                                <td>{{ $order->count }}</td>
                                <td>
                                    @if ($order->status == OrderStatus::$NEW_ORDER)
                                        New Order
                                    @elseif ($order->status == OrderStatus::$CONFIRM_ORDER)
                                        Confirmed Orders
                                    @elseif ($order->status == OrderStatus::$PARTIALLY_SHIPPED_ORDER)
                                        Partially Shipped Orders
                                    @elseif ($order->status == OrderStatus::$FULLY_SHIPPED_ORDER)
                                        Fully Shipped Orders
                                    @elseif ($order->status == OrderStatus::$BACK_ORDER)
                                        Back Ordered
                                    @elseif ($order->status == OrderStatus::$CANCEL_BY_BUYER)
                                        Cancelled by Buyer
                                    @elseif ($order->status == OrderStatus::$CANCEL_BY_VENDOR)
                                        Cancelled by Vendor
                                    @elseif ($order->status == OrderStatus::$CANCEL_BY_AGREEMENT)
                                        Cancelled by Agreement
                                    @elseif ($order->status == OrderStatus::$RETURNED)
                                        Returned
                                    @endif
                                </td>
                                <td>
                                    <a class="btnDelete" data-id="{{ $order->id }}" role="button" style="color: red">Delete</a>
                                </td>
                            </tr>
                        @endforeach

                         @if (sizeof($backOrders) > 0)
                             <tr>
                                 <td colspan="4">
                                     <button class="btn btn-primary btnPacklistOrder">Packlist</button>
                                     <button class="btn btn-primary btnPrintInvoiceOrder">Print Invoice</button>
                                 </td>

                                 <th>${{ number_format($totals['back'], 2, '.', '') }}</th>

                                 <td colspan="4"></td>
                             </tr>
                         @endif
                    </tbody>
                </table>
                {{ $backOrders->appends(array_merge($appends, ['c' => 'back']))->links() }}
            </div>
        </div>
    </div>
</div><br>
<div id="accordionfour" role="tablist">
    <div class="card">
        <div class="card-header" role="tab" id="headingFour">
            <h5 class="mb-0">
                <a data-toggle="collapse" href="#collapseFour" role="button" aria-expanded="true" aria-controls="collapseFour" class="">Shipped</a>
            </h5>
        </div>
        <div id="collapseFour" class="collapse show" role="tabpanel" aria-labelledby="headingOne" data-parent="#accordionfour" style="">
            <div class="card-body">
                <table class="table table-bordered">
                    <thead>
                        <tr>
                            <th></th>
                            <th>Date</th>
                            <th>Order #</th>
                            <th>Company Name</th>
                            <th>Amount</th>
                            <th>Shipping Method</th>
                            <th># of Orders</th>
                            <th>Status</th>
                            <th>Action</th>
                        </tr>
                    </thead>

                    <tbody>
                         @foreach($shippedOrders as $order)
                            <tr>
                                <td>
                                    <label class="custom-control custom-checkbox">
                                        <input type="checkbox" class="custom-control-input checkbox-order" data-id="{{ $order->id }}">
                                        <span class="custom-control-indicator"></span>
                                    </label>
                                </td>
                                <td>{{ date('m/d/Y', strtotime($order->created_at)) }}</td>
                                <td><a class="text-primary" href="{{ route('vendor_order_details', ['order' => $order->id]) }}">{{ $order->order_number }}</a></td>
                                <td>{{ $order->company_name }}</td>
                                <td>${{ sprintf('%0.2f', $order->total) }}</td>
                                <td>{{ $order->shipping }}</td>
                                <td>{{ $order->count }}</td>
                                <td>
                                    @if ($order->status == OrderStatus::$NEW_ORDER)
                                        New Order
                                    @elseif ($order->status == OrderStatus::$CONFIRM_ORDER)
                                        Confirmed Orders
                                    @elseif ($order->status == OrderStatus::$PARTIALLY_SHIPPED_ORDER)
                                        Partially Shipped Orders
                                    @elseif ($order->status == OrderStatus::$FULLY_SHIPPED_ORDER)
                                        Fully Shipped Orders
                                    @elseif ($order->status == OrderStatus::$BACK_ORDER)
                                        Back Ordered
                                    @elseif ($order->status == OrderStatus::$CANCEL_BY_BUYER)
                                        Cancelled by Buyer
                                    @elseif ($order->status == OrderStatus::$CANCEL_BY_VENDOR)
                                        Cancelled by Vendor
                                    @elseif ($order->status == OrderStatus::$CANCEL_BY_AGREEMENT)
                                        Cancelled by Agreement
                                    @elseif ($order->status == OrderStatus::$RETURNED)
                                        Returned
                                    @endif
                                </td>
                                <td>
                                    <a class="btnDelete" data-id="{{ $order->id }}" role="button" style="color: red">Delete</a>
                                </td>
                            </tr>
                        @endforeach

                         @if (sizeof($shippedOrders) > 0)
                             <tr>
                                 <td colspan="4">
                                     <button class="btn btn-primary btnPacklistOrder">Packlist</button>
                                     <button class="btn btn-primary btnPrintInvoiceOrder">Print Invoice</button>
                                 </td>

                                 <th>${{ number_format($totals['shipped'], 2, '.', '') }}</th>

                                 <td colspan="4"></td>
                             </tr>
                         @endif
                    </tbody>
                </table>
                {{ $shippedOrders->appends(array_merge($appends, ['c' => 'ship']))->links() }}
            </div>
        </div>
    </div>
</div><br>
<div id="accordionfive" role="tablist">
    <div class="card">
        <div class="card-header" role="tab" id="headingFive">
            <h5 class="mb-0">
                <a data-toggle="collapse" href="#collapseFive" role="button" aria-expanded="true" aria-controls="collapseFive" class="">Canceled</a>
            </h5>
        </div>
        <div id="collapseFive" class="collapse show" role="tabpanel" aria-labelledby="headingOne" data-parent="#accordionfive" style="">
            <div class="card-body">
                <table class="table table-bordered">
                    <thead>
                        <tr>
                            <th></th>
                            <th>Date</th>
                            <th>Order #</th>
                            <th>Company Name</th>
                            <th>Amount</th>
                            <th>Shipping Method</th>
                            <th># of Orders</th>
                            <th>Status</th>
                            <th>Action</th>
                        </tr>
                    </thead>

                    <tbody>
                         @foreach($cancelOrders as $order)
                            <tr>
                                <td>
                                    <label class="custom-control custom-checkbox">
                                        <input type="checkbox" class="custom-control-input checkbox-order" data-id="{{ $order->id }}">
                                        <span class="custom-control-indicator"></span>
                                    </label>
                                </td>
                                <td>{{ date('m/d/Y', strtotime($order->created_at)) }}</td>
                                <td><a class="text-primary" href="{{ route('vendor_order_details', ['order' => $order->id]) }}">{{ $order->order_number }}</a></td>
                                <td>{{ $order->company_name }}</td>
                                <td>${{ sprintf('%0.2f', $order->total) }}</td>
                                <td>{{ $order->shipping }}</td>
                                <td>{{ $order->count }}</td>
                                <td>
                                    @if ($order->status == OrderStatus::$NEW_ORDER)
                                        New Order
                                    @elseif ($order->status == OrderStatus::$CONFIRM_ORDER)
                                        Confirmed Orders
                                    @elseif ($order->status == OrderStatus::$PARTIALLY_SHIPPED_ORDER)
                                        Partially Shipped Orders
                                    @elseif ($order->status == OrderStatus::$FULLY_SHIPPED_ORDER)
                                        Fully Shipped Orders
                                    @elseif ($order->status == OrderStatus::$BACK_ORDER)
                                        Back Ordered
                                    @elseif ($order->status == OrderStatus::$CANCEL_BY_BUYER)
                                        Cancelled by Buyer
                                    @elseif ($order->status == OrderStatus::$CANCEL_BY_VENDOR)
                                        Cancelled by Vendor
                                    @elseif ($order->status == OrderStatus::$CANCEL_BY_AGREEMENT)
                                        Cancelled by Agreement
                                    @elseif ($order->status == OrderStatus::$RETURNED)
                                        Returned
                                    @endif
                                </td>
                                <td>
                                    <a class="btnDelete" data-id="{{ $order->id }}" role="button" style="color: red">Delete</a>
                                </td>
                            </tr>
                        @endforeach

                         @if (sizeof($cancelOrders) > 0)
                             <tr>
                                 <td colspan="4">
                                     <button class="btn btn-primary btnPacklistOrder">Packlist</button>
                                     <button class="btn btn-primary btnPrintInvoiceOrder">Print Invoice</button>
                                 </td>

                                 <th>${{ number_format($totals['cancel'], 2, '.', '') }}</th>

                                 <td colspan="4"></td>
                             </tr>
                         @endif
                    </tbody>
                </table>
                {{ $cancelOrders->appends(array_merge($appends, ['c' => 'cancel']))->links() }}
            </div>
        </div>
    </div>
</div>
<br>

<div class="modal fade" id="deleteModal" role="dialog" aria-labelledby="deleteModal">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header bg-danger">
                <h4 class="modal-title text-white" id="deleteModal">Delete</h4>
            </div>
            <div class="modal-body">
                <p>
                    Are you sure want to delete?
                </p>
            </div>
            <div class="modal-footer">
                <button class="btn  btn-default" data-dismiss="modal">Close</button>
                <button class="btn  btn-danger" id="modalBtnDelete">Delete</button>
            </div>
        </div>
    </div>
    <!--- end modals-->
</div>

<div class="modal fade" id="print-modal" tabindex="-1" role="dialog" aria-labelledby="modalLabelSmall"
     aria-hidden="true">
    <div class="modal-dialog modal-sm">
        <div class="modal-content">
            <div class="modal-header">
                <h4 class="modal-title" id="modalLabelSmall">Print</h4>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">×</span>
                </button>
            </div>

            <div class="modal-body">
                <a class="btn btn-primary" href="" target="_blank" id="btnPrintWithImage">Print with Images</a><br><br>
                <a class="btn btn-primary" href="" target="_blank" id="btnPrintWithoutImage">Print without Images</a>
            </div>
        </div>
    </div>
</div>
@stop

@section('additionalJS')
    <script type="text/javascript" src="{{ asset('plugins/moment/js/moment.min.js') }}"></script>
    <script type="text/javascript" src="{{ asset('plugins/daterangepicker/js/daterangepicker.js') }}"></script>
    <script>
        $(function () {
            $.ajaxSetup({
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                }
            });

            $('#dateRange').daterangepicker({
                locale: {
                    cancelLabel: 'Clear',
                    format: 'MM/DD/YYYY'
                }
            });

            var selectedId;

            $('.btnDelete').click(function () {
                $('#deleteModal').modal('show');
                selectedId = $(this).data('id');
            });

            $('#modalBtnDelete').click(function () {
                $.ajax({
                    method: "POST",
                    url: "{{ route('vendor_delete_order') }}",
                    data: { id: selectedId }
                }).done(function( msg ) {
                    location.reload();
                });
            });

            $('#selectOrderDate').change(function () {
                if ($(this).val() == '0') {
                    $('#date-range').removeClass('d-none');
                } else {
                    $('#date-range').addClass('d-none');
                }
            });

            $('#selectOrderDate').trigger('change');

            $('#btnApply').click(function () {
                var text = $('#inputText').val();
                var search = $('#searchItem').val();
                var ship = $('#selectShipStatus').val();
                var date = $('#selectOrderDate').val();
                var startDate = $('#dateRange').data('daterangepicker').startDate.format('MM/DD/YYYY');
                var endDate = $('#dateRange').data('daterangepicker').endDate.format('MM/DD/YYYY');

                var url = '{{ route('vendor_all_orders') }}' + '?text=' + text + '&search=' + search + '&ship=' + ship +
                    '&date=' + date + '&startDate=' + startDate + '&endDate=' + endDate;
                window.location.replace(url);
            });

            $('#btnReset').click(function () {
                var url = '{{ route('vendor_all_orders') }}';
                window.location.replace(url);
            });

            // Multiple Print
            var ids = [];
            $('.btnPrintInvoiceOrder').click(function () {
                ids = [];

                $(this).closest('tbody').find('.checkbox-order').each(function () {
                    if ($(this).is(':checked')) {
                        ids.push($(this).data('id'));
                    }
                });

                if (ids.length > 0) {
                    createOrderPdfUrl();
                    $('#print-modal').modal('show');
                }
            });

            $('.btnPacklistOrder').click(function () {
                ids = [];

                $(this).closest('tbody').find('.checkbox-order').each(function () {
                    if ($(this).is(':checked')) {
                        ids.push($(this).data('id'));
                    }
                });

                if (ids.length > 0) {
                    var url = '{{ route('vendor_print_packlist') }}' + '?order=' + ids.join(',');
                    window.open(url, '_blank');
                }
            });

            function createOrderPdfUrl() {
                var url = '{{ route('vendor_print_pdf') }}' + '?order=' + ids.join(',');
                var urlWithoutImage = '{{ route('vendor_print_pdf_without_image') }}' + '?order=' + ids.join(',');

                $('#btnPrintWithImage').attr('href', url);
                $('#btnPrintWithoutImage').attr('href', urlWithoutImage);
            }
        });
    </script>
@stop