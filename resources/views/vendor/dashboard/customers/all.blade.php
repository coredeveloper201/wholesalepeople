@extends('vendor.layouts.dashboard')

@section('additionalCSS')
    <link href="{{ asset('plugins/toastr/toastr.min.css') }}" rel="stylesheet">
@stop

@section('content')
    <div class="table-responsive">
        <table class="table">
            <thead>
            <tr>
                <th>Name</th>
                <th>Company Name</th>
                <th>Email</th>
                <th>Visitor Count</th>
                <th>Total Orders</th>
                <th>Blocked</th>
            </tr>
            </thead>

            <tbody>
            @foreach($users as $user)
                <tr>
                    <td>{{ $user->first_name.' '.$user->last_name }}</td>
                    <td>{{ $user->buyer->company_name }}</td>
                    <td>xxxxxxxxx</td>
                    <td>{{ $user->visitCount }}</td>
                    <td>
                        @if (isset($ordersCount[$user->id]))
                            {{ ($ordersCount[$user->id] == '' || $ordersCount[$user->id] == NULL) ? 0 : $ordersCount[$user->id] }}
                        @else
                        0
                        @endif
                    </td>
                    <td>
                        <label class="custom-control custom-checkbox">
                            <input type="checkbox" class="custom-control-input checkbox-blocked" value="1" data-id="{{ $user->id }}"
                                {{ in_array($user->id, $blockedIds) ? 'checked' : '' }}>
                            <span class="custom-control-indicator"></span>
                        </label>
                    </td>
                </tr>
            @endforeach
            </tbody>
        </table>

        <div class="pagination">
            @if (sizeof($users) > 0)
                {{ $users->links() }}
            @endif
        </div>
    </div>
@stop

@section('additionalJS')
    <script type="text/javascript" src="{{ asset('plugins/toastr/toastr.min.js') }}"></script>
    <script>
        $(function () {
            $.ajaxSetup({
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                }
            });

            $('.checkbox-blocked').change(function () {
                var status = 0;
                var id = $(this).data('id');

                if ($(this).is(':checked'))
                    status = 1;

                $.ajax({
                    method: "POST",
                    url: "{{ route('vendor_block_customer') }}",
                    data: { id: id, status: status }
                }).done(function( msg ) {
                    toastr.success('Status Updated!');
                });
            });
        });
    </script>
@stop