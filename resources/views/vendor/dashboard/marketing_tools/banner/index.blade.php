<?php use App\Enumeration\VendorImageType; ?>
@extends('vendor.layouts.dashboard')

@section('content')
    <div class="row">
        <div class="col-md-12">
            <form action="{{ route('vendor_banner_add_post') }}" method="POST" class="form-horizontal" enctype="multipart/form-data">
                @csrf

                <div class="form-group row">
                    <div class="col-lg-3">
                        <label class="col-form-label">Logo (230 x 54 Pixels):</label>
                    </div>
                    <div class="col-lg-4">
                        <input type="file" class="form-control{{ $errors->has('logo') ? ' is-invalid' : '' }}" name="logo" accept="image/*">

                        @if ($errors->has('logo'))
                            <div class="form-control-feedback">{{ $errors->first('logo') }}</div>
                        @endif
                    </div>
                </div>

                <div class="form-group row">
                    <div class="col-lg-3">
                        <label class="col-form-label">SP Main Slider (1400 x 400 Pixels):</label>
                    </div>
                    <div class="col-lg-4">
                        <input type="file" class="form-control{{ $errors->has('bidding_big_banner') ? ' is-invalid' : '' }}" name="bidding_big_banner" accept="image/*">

                        @if ($errors->has('bidding_big_banner'))
                            <div class="form-control-feedback">{{ $errors->first('bidding_big_banner') }}</div>
                        @endif
                    </div>
                </div>

                <div class="form-group row">
                    <div class="col-lg-3">
                        <label class="col-form-label">Category Top Banner (1200 x 250 Pixels):</label>
                    </div>
                    <div class="col-lg-4">
                        <input type="file" class="form-control{{ $errors->has('top_banner') ? ' is-invalid' : '' }}" name="top_banner" accept="image/*">

                        @if ($errors->has('top_banner'))
                            <div class="form-control-feedback">{{ $errors->first('top_banner') }}</div>
                        @endif
                    </div>
                </div>

                <div class="form-group row">
                    <div class="col-lg-3">
                        <label class="col-form-label">Category Small Banner (364 x 364 Pixels):</label>
                    </div>
                    <div class="col-lg-4">
                        <input type="file" class="form-control{{ $errors->has('small_ad_banner') ? ' is-invalid' : '' }}" name="small_ad_banner" accept="image/*">

                        @if ($errors->has('small_ad_banner'))
                            <div class="form-control-feedback">{{ $errors->first('small_ad_banner') }}</div>
                        @endif
                    </div>
                </div>

                <div class="form-group row">
                    <div class="col-lg-3">
                        <label class="col-form-label">Vendor Homepage Banner (910 x 326 Pixels):</label>
                    </div>
                    <div class="col-lg-4">
                        <input type="file" class="form-control{{ $errors->has('homepage_banner') ? ' is-invalid' : '' }}" name="homepage_banner" accept="image/*">

                        @if ($errors->has('homepage_banner'))
                            <div class="form-control-feedback">{{ $errors->first('homepage_banner') }}</div>
                        @endif
                    </div>
                </div>

                <div class="form-group row">
                    <div class="col-lg-3">
                        <label class="col-form-label">Vendor Home Logo (593 x 400 Pixels):</label>
                    </div>
                    <div class="col-lg-4">
                        <input type="file" class="form-control{{ $errors->has('mobile_main_banner') ? ' is-invalid' : '' }}" name="mobile_main_banner" accept="image/*">

                        @if ($errors->has('mobile_main_banner'))
                            <div class="form-control-feedback">{{ $errors->first('mobile_main_banner') }}</div>
                        @endif
                    </div>
                </div>

                <div class="form-group row">
                    <div class="col-lg-3">
                        <label class="col-form-label">Menu Feature Banner (376 x 270 Pixels):</label>
                    </div>
                    <div class="col-lg-4">
                        <input type="file" class="form-control{{ $errors->has('bidding_small_banner') ? ' is-invalid' : '' }}" name="bidding_small_banner" accept="image/*">

                        @if ($errors->has('bidding_small_banner'))
                            <div class="form-control-feedback">{{ $errors->first('bidding_small_banner') }}</div>
                        @endif
                    </div>
                </div>

                <div class="form-group row">
                    <div class="col-lg-12 text-right">
                        <input class="btn btn-primary" type="submit" value="Submit">
                    </div>
                </div>
            </form>
        </div>
    </div>

    <hr>

    <div class="row">
        <div class="col-md-1 text-right">
            Image Type:
        </div>
        
        <div class="col-md-2">
            <select class="form-control" id="selectType">
                <option value="">All</option>
                <option value="{{ VendorImageType::$LOGO }}" {{ request()->get('type') == VendorImageType::$LOGO ? 'selected' : '' }}>Logo</option>
                <option value="{{ VendorImageType::$HOME_PAGE_BANNER }}" {{ request()->get('type') == VendorImageType::$HOME_PAGE_BANNER ? 'selected' : '' }}>Vendor Main</option>
                <option value="{{ VendorImageType::$SMALL_AD_BANNER }}" {{ request()->get('type') == VendorImageType::$SMALL_AD_BANNER ? 'selected' : '' }}>Small Ad</option>
                <option value="{{ VendorImageType::$MOBILE_MAIN_BANNER }}" {{ request()->get('type') == VendorImageType::$MOBILE_MAIN_BANNER ? 'selected' : '' }}>Mobile</option>
                <option value="{{ VendorImageType::$BIDDING_BIG_BANNER }}" {{ request()->get('type') == VendorImageType::$BIDDING_BIG_BANNER ? 'selected' : '' }}>Bidding Big</option>
                <option value="{{ VendorImageType::$BIDDING_SMALL_BANNER }}" {{ request()->get('type') == VendorImageType::$BIDDING_SMALL_BANNER ? 'selected' : '' }}>Bidding Small</option>
                <option value="{{ VendorImageType::$TOP_BANNER }}" {{ request()->get('type') == VendorImageType::$TOP_BANNER ? 'selected' : '' }}>Top Banner</option>
            </select>
        </div>

        <div class="col-md-1 text-right">
            Status:
        </div>

        <div class="col-md-2">
            <select class="form-control" id="selectStatus">
                <option value="">All</option>
                <option value="1" {{ request()->get('status') == '1' ? 'selected' : '' }}>Active</option>
                <option value="2" {{ request()->get('status') == '2' ? 'selected' : '' }}>InActive</option>
            </select>
        </div>
    </div>

    <br>

    <div class="row">
        <div class="col-md-12">
            <table class="table">
                <thead>
                    <tr>
                        <th>Type</th>
                        <th>Preview</th>
                        <th>Upload Date</th>
                        <th>Status</th>
                        <th>Action</th>
                    </tr>
                </thead>

                <tbody>
                    @foreach($images as $image)
                        <tr>
                            <td>
                                @if ($image->type == VendorImageType::$LOGO)
                                    Logo
                                @elseif ($image->type == VendorImageType::$HOME_PAGE_BANNER)
                                    Vendor Homepage Banner
                                @elseif ($image->type == VendorImageType::$SMALL_AD_BANNER)
                                    Category Small Banner
                                @elseif ($image->type == VendorImageType::$MOBILE_MAIN_BANNER)
                                    Mobile Main Banner
                                @elseif ($image->type == VendorImageType::$BIDDING_BIG_BANNER)
                                    SP Main Slider
                                @elseif ($image->type == VendorImageType::$BIDDING_SMALL_BANNER)
                                    Menu Feature Banner
                                @elseif ($image->type == VendorImageType::$TOP_BANNER)
                                    Top Banner
                                @endif
                            </td>

                            <td>
                                <img src="{{ asset($image->image_path) }}" height="50px">
                            </td>

                            <td>{{ date('m/d/Y g:h:i a', strtotime($image->created_at)) }}</td>

                            <td>
                                @if ($image->status == 1)
                                    Active
                                @else
                                    Inactive
                                @endif
                            </td>

                            <td>
                                @if ($image->status == 0)
                                    <a class="btnActive" data-id="{{ $image->id }}" data-type="{{ $image->type }}" role="button" style="color: blue">Active</a> |
                                    <a class="btnDelete" data-id="{{ $image->id }}" role="button" style="color: red">Delete</a>
                                @endif
                            </td>
                        </tr>
                    @endforeach
                </tbody>
            </table>

            {{ $images->appends($appends)->links() }}
        </div>
    </div>

    <div class="modal fade" id="deleteModal" role="dialog" aria-labelledby="deleteModal">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-header bg-danger">
                    <h4 class="modal-title text-white" id="deleteModal">Delete</h4>
                </div>
                <div class="modal-body">
                    <p>
                        Are you sure want to delete?
                    </p>
                </div>
                <div class="modal-footer">
                    <button class="btn  btn-default" data-dismiss="modal">Close</button>
                    <button class="btn  btn-danger" id="modalBtnDelete">Delete</button>
                </div>
            </div>
        </div>
        <!--- end modals-->
    </div>
@stop

@section('additionalJS')
    <script>
        $(function () {
            $.ajaxSetup({
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                }
            });

            var selectedId;

            $('#selectType, #selectStatus').change(function () {
                var current_url = '{{ Request::url() }}' + '?';

                if ($('#selectType').val() != "")
                    current_url += 'type=' + $('#selectType').val() + '&';

                if ($('#selectStatus').val() != "")
                    current_url += 'status=' + $('#selectStatus').val() + '&';

                window.location.replace(current_url);
            });

            $('.btnDelete').click(function () {
                $('#deleteModal').modal('show');
                selectedId = $(this).data('id');
            });

            $('#modalBtnDelete').click(function () {
                $.ajax({
                    method: "POST",
                    url: "{{ route('vendor_banner_delete') }}",
                    data: { id: selectedId }
                }).done(function( msg ) {
                    location.reload();
                });
            });

            $('.btnActive').click(function () {
                var type = $(this).data('type');
                var id = $(this).data('id');

                $.ajax({
                    method: "POST",
                    url: "{{ route('vendor_banner_active') }}",
                    data: { id: id, type: type }
                }).done(function( msg ) {
                    location.reload();
                });
            });
        });
    </script>
@stop