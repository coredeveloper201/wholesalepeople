@extends('vendor.layouts.dashboard')

@section('additionalCSS')
    <link href="{{ asset('plugins/toastr/toastr.min.css') }}" rel="stylesheet">
@stop

@section('content')
    <button class="btn btn-primary" id="btnImport">Import from Excel File</button>
    <a class="btn btn-info" href="{{ asset('files/sample-file.xlsx') }}" target="_blank">Download Sample File</a>

    <form action="{{ route('vendor_data_import_read_file') }}" id="form" method="post" enctype="multipart/form-data">
        <input class="d-none" type="file" id="file" name="file">
    </form>

@stop

@section('additionalJS')
    <script type="text/javascript" src="{{ asset('plugins/toastr/toastr.min.js') }}"></script>
    <script>
        $(function () {
            var message = '{{ session('message') }}';
            var error = '{{ session('error') }}';

            if (message != '')
                toastr.success(message);

            if (error != '')
                toastr.error(error);

            $('#btnImport').click(function () {
                $('#file').click();
            });

            $('#file').change(function () {
                file = $(this).prop('files')[0];

                if (typeof file !== "undefined") {
                    $('#form').submit();
                }
            });
        });
    </script>
@stop