<form class="form-horizontal" method="POST" id="form-settings">
    <div class="form-group row">
        <div class="col-lg-2">
            <label for="company_description" class="col-form-label">Minimum Order: </label>
        </div>

        <div class="col-lg-4">
            <input type="text" class="form-control" name="min_order" value="{{ $user->vendor->min_order }}" id="min_order" placeholder="$">
        </div>
    </div>

    <div class="row">
        <div class="col-md-12 text-right">
            <button id="btnSaveSettings" class="btn btn-primary">SAVE</button>
        </div>
    </div>
</form>