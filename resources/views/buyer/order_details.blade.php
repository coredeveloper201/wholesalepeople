<?php
    use App\Enumeration\OrderStatus;
    use App\Enumeration\MessageTopic;
?>

@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row content margin-bottom-1x">
        <div class=" col-md-8">
            <h4>Vendor Order Details - {{ $order->order_number }}</h4>
        </div>

        <div class="col-md-4 text-right">
            <button class="btn btn-primary" id="btnSendMsg">Send Message</button>
        </div>
    </div>
</div>
<div class="container content">
    <div class="row">
        <div class="col-md-4">
            @if ($vendor_logo_path != '')
                <img src="{{ $vendor_logo_path }}">
            @endif
            <p>
                <b>{{ $order->vendor->company_name }}</b><br>
                {{ $order->vendor->billing_address }},<br>
                {{ $order->vendor->billing_city }},
                @if ($order->vendor->billingState == null)
                    {{ $order->vendor->billing_state }},
                @else
                    {{ $order->vendor->billingState->name }},
                @endif
                {{ $order->vendor->billingCountry->name }} - {{ $order->vendor->billing_zip }}
            </p>
        </div>

        <div class="col-md-4"></div>
        <div class="col-md-4 text-right">
            <table class="table table-bordered">
                <tr>
                    <th>Order No.</th>
                    <td>{{ $order->order_number }}</td>
                </tr>

                <tr>
                    <th>Order Date</th>
                    <td>{{ date('F d, Y', strtotime($order->created_at)) }}</td>
                </tr>

                <tr>
                    <th>Status</th>
                    <td>
                        @if ($order->status == OrderStatus::$NEW_ORDER)
                            New Order
                        @elseif ($order->status == OrderStatus::$CONFIRM_ORDER)
                            Confirmed Orders
                        @elseif ($order->status == OrderStatus::$PARTIALLY_SHIPPED_ORDER)
                            Partially Shipped Orders
                        @elseif ($order->status == OrderStatus::$FULLY_SHIPPED_ORDER)
                            Fully Shipped Orders
                        @elseif ($order->status == OrderStatus::$BACK_ORDER)
                            Back Ordered
                        @elseif ($order->status == OrderStatus::$CANCEL_BY_BUYER)
                            Cancelled by Buyer
                        @elseif ($order->status == OrderStatus::$CANCEL_BY_VENDOR)
                            Cancelled by Vendor
                        @elseif ($order->status == OrderStatus::$CANCEL_BY_AGREEMENT)
                            Cancelled by Agreement
                        @elseif ($order->status == OrderStatus::$RETURNED)
                            Returned
                        @endif
                    </td>
                </tr>
            </table>
        </div>
    </div>

    <div class=" padding-bottom-1x">
        <table class="table table-bordered">
            <thead>
            <tr>
                <th>Shipping Address</th>
                <th>Billing Address</th>
            </tr>
            </thead>
            <tbody>
                <tr>
                    <td>{{ $order->shipping_address }}<br>
                        {{ $order->shipping_state }}, {{ $order->shipping_city }}, {{ $order->shipping_country }} - {{ $order->shipping_zip }}</td>
                    <td>{{ $order->billing_address }}<br>
                        {{ $order->billing_state }}, {{ $order->billing_city }}, {{ $order->billing_country }} - {{ $order->billing_zip }}</td>
                </tr>
                <tr>
                    <td><b>Phone: </b>{{ $order->shipping_phone }}</td>
                    <td><b>Phone: </b>{{ $order->billing_phone }}</td>
                </tr>
            </tbody>
        </table>
        <table class="table table-bordered">
            <tr>
                <th width="25%">Shipping Method</th>
                <td width="25%">{{ $order->shipping }}</td>
                <th width="25%">Tracking Number</th>
                <td width="25%">{{ $order->tracking_number }}</td>
            </tr>
        </table>
    </div>

    <h4>Items</h4>
    <hr class="padding-bottom-1x">
    <div class="table-responsive">
        <table class="table table-bordered">
            <thead>
            <tr>
                <th>Image</th>
                <th>Style No.</th>
                <th class="text-center">Color</th>
                <th class="text-center" colspan="10">Size</th>
                <th class="text-center">Pack</th>
                <th class="text-center">Total Qty</th>
                <th class="text-center">Unit Price</th>
                <th class="text-center">Amount</th>
            </tr>
            </thead>

            <tbody>
            @foreach($allItems as $item_id => $items)
                <tr>
                    <td rowspan="{{ sizeof($items)+1  }}">
                        @if (sizeof($items[0]->item->images) > 0)
                            <img src="{{ asset($items[0]->item->images[0]->image_path) }}" alt="Product" style="height: 100px">
                        @else
                            <img src="{{ asset('images/no-image.png') }}" alt="Product">
                        @endif
                    </td>

                    <td rowspan="{{ sizeof($items)+1  }}">
                        {{ $items[0]->item->style_no }}
                    </td>

                    <td>
                        &nbsp;
                    </td>

                    <?php
                    $sizes = explode("-", $items[0]->size);
                    ?>

                    @foreach($sizes as $size)
                        <th colspan="{{ $loop->last ? 10-sizeof($sizes) +1 : '' }}">{{ $size }}</th>
                    @endforeach

                    <td class="text-center">
                        &nbsp;
                    </td>

                    <td class="text-center">
                        &nbsp;
                    </td>

                    <td class="text-center">
                        &nbsp;
                    </td>

                    <td class="text-center">
                        &nbsp;
                    </td>
                </tr>

                @foreach($items as $item)
                    <tr>
                        <td>
                            {{ $item->color }}
                        </td>

                        <?php $packs = explode("-", $item->pack); ?>

                        @for($i=0; $i < sizeof($sizes); $i++)
                            <td colspan="{{ $i == sizeof($sizes) -1 ? 10-$i : '' }}">{{ $packs[$i] }}</td>
                        @endfor

                        <td>
                            {{ $item->qty }}
                        </td>

                        <td>
                            {{ $item->total_qty }}
                        </td>

                        <td>
                            ${{ sprintf('%0.2f', $item->per_unit_price) }}
                        </td>

                        <td>
                            <span>${{ sprintf('%0.2f', $item->amount) }}</span>
                        </td>
                    </tr>
                @endforeach
            @endforeach
            </tbody>
        </table>
    </div>

    <div class="row">
        <div class="col-md-9"></div>
        <div class="col-md-3">
            <table class="table table-bordered">
                <tr>
                    <th>Sub Total</th>
                    <td>${{ sprintf('%0.2f', $order->subtotal) }}</td>
                </tr>

                <tr>
                    <th>Shipping Cost</th>
                    <td>${{ sprintf('%0.2f', $order->shipping_cost) }}</td>
                </tr>

                <tr>
                    <th>Store Credit</th>
                    <td>${{ sprintf('%0.2f', $order->store_credit) }}</td>
                </tr>

                <tr>
                    <th>Discount</th>
                    <td>${{ sprintf('%0.2f', $order->discount) }}</td>
                </tr>

                <tr>
                    <th>Total</th>
                    <td><b>${{ sprintf('%0.2f', $order->total) }}</b></td>
                </tr>
            </table>
        </div>
    </div>

    <div class="row">
        <div class="col-md-12">
            <p>
                <b>Note: </b>
                {{ $order->note }}
            </p>
        </div>
    </div>
</div>
<div class="container">
    <div class="row">
        <div class="content col-md-12 margin-top-1x">
            <a class="btn btn-primary" href="{{ route('buyer_show_orders') }}">Back To Order List</a>
        </div>
    </div>
</div>
@stop

@section('mobile_filter')
    <div class="modal fade" id="modalNewMessage" role="dialog">
        <div class="modal-dialog modal-lg" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h4 class="modal-title">New Message</h4>
                    <button class="close" type="button" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                </div>

                <div class="modal-body">
                    <form id="form-msg">
                        <input type="hidden" name="orderID" value="{{ $order->id }}">
                        <div class="form-group row">
                            <label class="col-2 col-form-label" for="text-input">To</label>
                            <input type="hidden" name="type" value="1">

                            <div class="col-7">
                                {{ $order->vendor->company_name }}
                                <input type="hidden" name="vendor" value="{{ $order->vendor_meta_id }}">
                            </div>
                        </div>

                        <div class="form-group row">
                            <label class="col-2 col-form-label" for="text-input">Topics</label>
                            <div class="col-3">
                                <select name="topic" class="form-control form-control-rounded form-control-sm" id="select-topic">
                                    <option value="{{ MessageTopic::$GENERAL }}">General</option>
                                    <option value="{{ MessageTopic::$PRODUCT }}">Product</option>
                                    <option value="{{ MessageTopic::$ORDER }}" selected>Order</option>
                                    <option value="{{ MessageTopic::$PAYMENT }}">Payment</option>
                                    <option value="{{ MessageTopic::$SHIPMENT }}">Shipment</option>
                                    <option value="{{ MessageTopic::$RETURN }}">Return</option>
                                    <option value="{{ MessageTopic::$OTHER }}">Other</option>
                                </select>
                            </div>
                        </div>

                        <div class="form-group row">
                            <label class="col-2 col-form-label" for="text-input">Title</label>
                            <div class="col-10">
                                <input name="title" class="form-control form-control-rounded form-control-sm" type="text" id="input-title" value="Regarding My Order {{ $order->order_number }}">
                            </div>
                        </div>

                        <div class="form-group row">
                            <label class="col-2 col-form-label" for="text-input">Message</label>
                            <div class="col-10">
                                <textarea name="message" class="form-control form-control-rounded form-control-sm" id="input-message"></textarea>
                            </div>
                        </div>

                        <div class="form-group row">
                            <label class="col-2 col-form-label" for="text-input">Attach Files</label>
                            <div class="col-9" id="input-files">
                                <input name="attachments[]" class="input-file form-control form-control-rounded form-control-sm" type="file">
                            </div>

                            <div class="col-1" id="input-files-delete">
                                <a class="text-danger btn-delete-file input-file form-control form-control-rounded form-control-sm">X</a>
                            </div>
                        </div>
                    </form>
                </div>

                <div class="modal-footer">
                    <button class="btn btn-outline-secondary btn-sm" type="button" data-dismiss="modal">Close</button>
                    <button class="btn btn-primary btn-sm" type="button" id="btn-send-message">Send</button>
                </div>
            </div>
        </div>
    </div>
@stop

@section('additionalJS')
    <script>
        $(function () {
            $.ajaxSetup({
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                }
            });

            $('#btnSendMsg').click(function () {
                $('#modalNewMessage').modal('show');
            });

            $('#select-type').change(function () {
                var val = $(this).val();

                if (val == 2)
                    $('#select-vendor').hide();
                else
                    $('#select-vendor').show();
            });

            $(document).on('change', '.input-file', function () {
                if ($(this).val() != '') {
                    $('#input-files').append('<input name="attachments[]" class="input-file form-control form-control-rounded form-control-sm" type="file">');
                    $('#input-files-delete').append('<a class="text-danger btn-delete-file input-file form-control form-control-rounded form-control-sm">X</a>');
                }
            });

            $(document).on('click', '.btn-delete-file', function () {
                var index = $('.btn-delete-file').index($(this));

                $('.input-file:eq('+index+')').val('');
            });

            $('#btn-send-message').click(function () {
                if ($('#select-type').val() == '1' && $('#select-vendor').val() == '') {
                    alert('Select Vendor');
                    return;
                }

                if ($('#input-title').val() == '') {
                    alert('Enter title');
                    return;
                }

                if ($('#input-message').val() == '') {
                    alert('Enter message');
                    return;
                }

                $('#form-msg').submit();
            });

            $("#form-msg").submit(function(e) {
                e.preventDefault();
                var formData = new FormData(this);

                $.ajax({
                    url: "{{ route('buyer_add_messsage') }}",
                    type: 'POST',
                    data: formData,
                    success: function (data) {
                        location.reload();
                    },
                    cache: false,
                    contentType: false,
                    processData: false
                });
            });
        });
    </script>
@stop