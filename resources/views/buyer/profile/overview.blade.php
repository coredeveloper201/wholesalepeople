<?php use App\Enumeration\OrderStatus; ?>
@extends('buyer.layouts.profile')

@section('profile_content')
    <h2>Hello {{ $user->first_name.' '.$user->last_name }},</h2>

    <div class="row margin-bottom-1x">
        <div class="col-md-12">
            {!! $buyer_home !!}
        </div>
    </div>
    @if (sizeof($vendors) > 0)
        <h2>Recommended By Stylepick.net</h2>

        <div class="row margin-bottom-1x">
            @foreach($vendors as $vendor)
                <div class="col-md-3 margin-bottom-1x">
                    <a href="{{ route('vendor_or_parent_category', ['text' => changeSpecialChar($vendor->company_name)]) }}">
                        <img src="{{ asset($vendor->images[0]->image_path) }}">
                    </a>
                </div>
            @endforeach
        </div>
    @endif

    @if (sizeof($orders) > 0)
    <div class="recentorder">
        <h2>Your Recent order</h2>
    </div>
    <div class="row">
        <div class="col-md-12 margin-bottom-2x">
            <div class="table-responsive">
                <table class="table table-hover margin-bottom-none">
                    <thead>
                    <tr>
                        <th>Order #</th>
                        <th>Vendor</th>
                        <th>Date Purchased</th>
                        <th>Status</th>
                        <th>Total</th>
                    </tr>
                    </thead>

                    <tbody>
                    @foreach($orders as $order)
                        <tr>
                            <td><a class="text-medium navi-link" href="{{ route('show_order_details', ['order' => $order->id]) }}">{{ $order->order_number }}</a></td>
                            <td>{{ $order->vendor->company_name }}</td>
                            <td>{{ date('F d, Y', strtotime($order->created_at)) }}</td>
                            <td>
                                @if ($order->status == OrderStatus::$NEW_ORDER)
                                    New Order
                                @elseif ($order->status == OrderStatus::$CONFIRM_ORDER)
                                    Confirmed Orders
                                @elseif ($order->status == OrderStatus::$PARTIALLY_SHIPPED_ORDER)
                                    Partially Shipped Orders
                                @elseif ($order->status == OrderStatus::$FULLY_SHIPPED_ORDER)
                                    Fully Shipped Orders
                                @elseif ($order->status == OrderStatus::$BACK_ORDER)
                                    Back Ordered
                                @elseif ($order->status == OrderStatus::$CANCEL_BY_BUYER)
                                    Cancelled by Buyer
                                @elseif ($order->status == OrderStatus::$CANCEL_BY_VENDOR)
                                    Cancelled by Vendor
                                @elseif ($order->status == OrderStatus::$CANCEL_BY_AGREEMENT)
                                    Cancelled by Agreement
                                @elseif ($order->status == OrderStatus::$RETURNED)
                                    Returned
                                @endif
                            </td>
                            <td><span class="text-medium">${{ sprintf('%0.2f', $order->total) }}</span></td>
                        </tr>
                    @endforeach
                    </tbody>
                </table>
            </div>
        </div>
    </div>
    @endif
@stop