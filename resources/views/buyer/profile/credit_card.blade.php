@extends('buyer.layouts.profile')

@section('additionalCSS')
    <link href="{{ asset('plugins/toastr/toastr.min.css') }}" rel="stylesheet">
@stop

@section('profile_content')
    <a href="{{ route('buyer_add_credit_card') }}" class="btn btn-primary">Add New Card</a>

    <div class="table-responsive">
        <table class="table table-bordered">
            <thead>
            <tr>
                <th>Card Type</th>
                <th>Card Number</th>
                <th>Address</th>
                <th>Default</th>
                <th>Action</th>
            </tr>
            </thead>

            <tbody>
            @foreach($cards as $card)
                <tr>
                    <td class="text-center">
                        @if ($card->card_type == 'visa')
                            <img src="{{ asset('images/cards/visa.png') }}" alt="visa_logo" width="25px">
                        @elseif ($card->card_type == 'mastercard')
                            <img src="{{ asset('images/cards/master_card.png') }}" alt="master_card_logo" width="25px">
                        @elseif ($card->card_type == 'amex')
                            <img src="{{ asset('images/cards/amex.png') }}" alt="amex_logo" width="25px">
                        @else
                            {{ $card->card_type }}
                        @endif
                    </td>
                    <td>{{ $card->mask }}</td>
                    <td>
                        {{ $card->billing_address }}, {{ $card->billing_city }},
                        {{ ($card->billingState == null) ? $card->billing_state : $card->billingState->name }},
                        {{ $card->billingCountry->name }} - {{ $card->billing_zip }}
                    </td>

                    <td class="text-center">
                        <div class="custom-control custom-radio custom-control-inline">
                            <input class="custom-control-input defaultAddress" type="radio" id="default_address_{{ $card->id }}"
                                   name="defaultAddress" {{ ($card->default == 1) ? 'checked' : '' }} value="{{ $card->id }}">
                            <label class="custom-control-label" for="default_address_{{ $card->id }}"></label>
                        </div>
                    </td>

                    <td class="text-center">
                        <a class="text-info btnEdit" href="{{ route('buyer_edit_credit_card', ['card' => $card->id]) }}">Edit</a> |
                        <a class="text-danger btnDelete" role="button" data-id="{{ $card->id }}">Delete</a>
                    </td>
                </tr>
            @endforeach
            </tbody>
        </table>
    </div>
@stop

@section('mobile_filter')
    <div class="modal fade" id="deleteModal" tabindex="-1" role="dialog">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h4 class="modal-title">Delete</h4>
                    <button class="close" type="button" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                </div>
                <div class="modal-body">
                    <p>
                        Are you sure want to delete?
                    </p>
                </div>
                <div class="modal-footer">
                    <button class="btn btn-outline-secondary btn-sm" type="button" data-dismiss="modal">Close</button>
                    <button class="btn btn-primary btn-sm" type="button" id="modalBtnDelete">Delete</button>
                </div>
            </div>
        </div>
    </div>
@stop

@section('additionalJS')
    <script type="text/javascript" src="{{ asset('plugins/toastr/toastr.min.js') }}"></script>
    <script>
        $(function () {
            $.ajaxSetup({
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                }
            });

            // Default Address
            $('.defaultAddress').change(function () {
                var id = $('.defaultAddress:checked').val();

                $.ajax({
                    method: "POST",
                    url: "{{ route('buyer_default_credit_card') }}",
                    data: { id: id },
                }).done(function( data ) {
                    toastr.success('Default Credit Card Changed!');
                });
            });

            // Delete Address
            var selectedId = '';

            $('.btnDelete').click(function () {
                $('#deleteModal').modal('show');
                selectedId = $(this).data('id');
            });

            $('#modalBtnDelete').click(function () {
                $.ajax({
                    method: "POST",
                    url: "{{ route('buyer_delete_credit_card') }}",
                    data: { id: selectedId }
                }).done(function( msg ) {
                    window.location.reload(true);
                });
            });
        });
    </script>
@stop