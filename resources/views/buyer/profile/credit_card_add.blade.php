@extends('buyer.layouts.profile')

@section('profile_content')
<form action="{{ route('buyer_add_credit_card_post') }}" method="post">
    @csrf

    <h4>Credit Card Information</h4>
    <hr class="padding-bottom-1x">

    <div class="row">
        <div class="col-md-6">
            <div class="form-group{{ $errors->has('card_number') ? ' has-danger' : '' }}">
                <label for="small-rounded-input">Card Number <span class="required">*</span></label>
                <input class="form-control" type="text" id="card_number" name="card_number"
                       value="{{ old('card_number') }}">
            </div>

            <div class="form-group{{ $errors->has('expiration_date') ? ' has-danger' : '' }}">
                <label for="small-rounded-input">Expiration Date <span class="required">*</span></label>
                <input class="form-control" type="text" id="expiration_date" name="expiration_date"
                       value="{{ old('expiration_date') }}" placeholder="MM/YY"
                       data-inputmask="'mask': '99/99'">
            </div>
        </div>

        <div class="col-md-6">
            <div class="form-group{{ $errors->has('card_name') ? ' has-danger' : '' }}">
                <label for="small-rounded-input">Name  <span class="required">*</span></label>
                <input class="form-control" type="text" id="card_name" name="card_name"
                       value="{{ old('card_name') }}">
            </div>

            <div class="form-group{{ $errors->has('cvc') ? ' has-danger' : '' }}">
                <label for="small-rounded-input">CVC  <span class="required">*</span></label>
                <input class="form-control" type="text" id="cvc" name="cvc"
                       value="{{ old('cvc') }}">
            </div>
        </div>
    </div>

    <div class="row">
        <div class="col-md-12">
            <div class="form-group">
                <div class="custom-control custom-checkbox">
                    <input class="custom-control-input" type="checkbox" id="default" name="default" value="1"
                            {{ old('default') ? 'checked' : '' }}>
                    <label class="custom-control-label" for="default">This is default card</label>
                </div>
            </div>
        </div>
    </div>

    <h4>Billing Address</h4>
    <hr class="padding-bottom-1x">

    <div class="row">
        <div class="col-md-12">
            <div class="form-group">
                <label for="small-rounded-input">Location</label><br>
                <div class="custom-control custom-radio custom-control-inline">
                    <input class="custom-control-input factoryLocation" type="radio" id="factoryLocationUS" name="factoryLocation" value="US" {{ (old('factoryLocation') == 'US' || empty(old('factoryLocation'))) ? 'checked' : '' }}>
                    <label class="custom-control-label" for="factoryLocationUS">United States</label>
                </div>

                <div class="custom-control custom-radio custom-control-inline">
                    <input class="custom-control-input factoryLocation" type="radio" id="factoryLocationCA" name="factoryLocation" value="CA" {{ old('factoryLocation') == 'CA' ? 'checked' : '' }}>
                    <label class="custom-control-label" for="factoryLocationCA">Canada</label>
                </div>

                <div class="custom-control custom-radio custom-control-inline">
                    <input class="custom-control-input factoryLocation" type="radio" id="factoryLocationInt" name="factoryLocation" value="INT" {{ old('factoryLocation') == 'INT' ? 'checked' : '' }}>
                    <label class="custom-control-label" for="factoryLocationInt">International</label>
                </div>
            </div>
        </div>
    </div>

    <div class="row">
        <div class="col-md-6">
            <div class="form-group{{ $errors->has('factoryAddress') ? ' has-danger' : '' }}">
                <label for="small-rounded-input">Address <span class="required">*</span></label>
                <input class="form-control form-control-rounded form-control-sm" type="text" id="factoryAddress" name="factoryAddress" value="{{ old('factoryAddress') }}">
            </div>
        </div>

        <div class="col-md-6">
            <div class="form-group{{ $errors->has('factoryCity') ? ' has-danger' : '' }}">
                <label for="small-rounded-input">City <span class="required">*</span></label>
                <input class="form-control form-control-rounded form-control-sm" type="text" id="factoryCity" name="factoryCity" value="{{ old('factoryCity') }}">
            </div>
        </div>
    </div>

    <div class="row">
        <div class="col-md-6">
            <div class="form-group{{ $errors->has('factoryState') ? ' has-danger' : '' }}" id="form-group-factory-state">
                <label for="small-rounded-input">State <span class="required">*</span></label>
                <input class="form-control form-control-rounded form-control-sm" type="text" id="factoryState" name="factoryState" value="{{ old('factoryState') }}">
            </div>

            <div class="form-group{{ $errors->has('factoryStateSelect') ? ' has-danger' : '' }}" id="form-group-factory-state-select">
                <label for="small-rounded-input">State <span class="required">*</span></label>
                <select class="form-control form-control-rounded form-control-sm" id="factoryStateSelect" name="factoryStateSelect">
                    <option value="">Select State</option>
                </select>
            </div>
        </div>

        <div class="col-md-6">
            <div class="form-group{{ $errors->has('factoryZipCode') ? ' has-danger' : '' }}">
                <label for="small-rounded-input">Zip Code <span class="required">*</span></label>
                <input class="form-control form-control-rounded form-control-sm" type="text" id="factoryZipCode" name="factoryZipCode" value="{{ old('factoryZipCode') }}">
            </div>
        </div>
    </div>

    <div class="row">
        <div class="col-md-6">
            <div class="form-group{{ $errors->has('factoryCountry') ? ' has-danger' : '' }}">
                <label for="small-rounded-input">Country <span class="required">*</span></label>
                <select class="form-control form-control-rounded form-control-sm" id="factoryCountry" name="factoryCountry">
                    <option value="">Select Country</option>
                    @foreach($countries as $country)
                        <option data-code="{{ $country->code }}" value="{{ $country->id }}" {{ old('factoryCountry') == $country->id ? 'selected' : '' }}>{{ $country->name }}</option>
                    @endforeach
                </select>
            </div>
        </div>
    </div>

    <div class="row">
        <div class="col-md-12 text-right">
            <input class="btn btn-primary margin-bottom-none" type="submit" value="ADD CARD">
        </div>
    </div>
</form>
@stop

@section('additionalJS')
    <script type="text/javascript" src="{{ asset('plugins/inputmask/js/inputmask.js') }}"></script>
    <script type="text/javascript" src="{{ asset('plugins/inputmask/js/jquery.inputmask.js') }}"></script>
    <script>
        $(function () {
            var usStates = <?php echo json_encode($usStates); ?>;
            var caStates = <?php echo json_encode($caStates); ?>;
            var oldFactoryState = '{{ old('factoryStateSelect') }}';

            $('#expiration_date').inputmask();

            $('form').bind('submit', function () {
                $(this).find(':input').prop('disabled', false);
            });

            $('.factoryLocation').change(function () {
                var location = $('.factoryLocation:checked').val();

                if (location == 'CA' || location == 'US') {
                    if (location == 'US')
                        $('#factoryCountry').val('1');
                    else
                        $('#factoryCountry').val('2');

                    $('#factoryCountry').prop('disabled', 'disabled');
                    $('#form-group-factory-state-select').show();
                    $('#factoryStateSelect').val('');
                    $('#form-group-factory-state').hide();

                    $('#factoryStateSelect').html('<option value="">Select State</option>');

                    if (location == 'US') {
                        $.each(usStates, function (index, value) {
                            if (value.id == oldFactoryState)
                                $('#factoryStateSelect').append('<option value="'+value.id+'" selected>'+value.name+'</option>');
                            else
                                $('#factoryStateSelect').append('<option value="'+value.id+'">'+value.name+'</option>');
                        });
                    }

                    if (location == 'CA') {
                        $.each(caStates, function (index, value) {
                            if (value.id == oldFactoryState)
                                $('#factoryStateSelect').append('<option value="'+value.id+'" selected>'+value.name+'</option>');
                            else
                                $('#factoryStateSelect').append('<option value="'+value.id+'">'+value.name+'</option>');
                        });
                    }
                } else {
                    $('#factoryCountry').prop('disabled', false);
                    $('#form-group-factory-state-select').hide();
                    $('#form-group-factory-state').show();
                }
            });

            $('.factoryLocation').trigger('change');
        });
    </script>
@stop