<?php
    use App\Enumeration\MessageTopic;
?>

@extends('buyer.layouts.profile')

@section('profile_content')
    <div class="row">
        <div class="col-md-12 text-right">
            <button class="btn btn-primary" id="btnNewMsg">Compose</button>
        </div>
    </div>

    <div class="row">
        <div class="col-md-12">
            <ul class="nav nav-tabs" role="tablist">
                <li class="nav-item"><a class="nav-link active show" href="#inbox" data-toggle="tab" role="tab" aria-selected="true">Inbox</a></li>
                <li class="nav-item"><a class="nav-link" href="#outbox" data-toggle="tab" role="tab" aria-selected="false">Outbox</a></li>
             </ul>

            <div class="tab-content">
                <div class="tab-pane fade active show" id="inbox" role="tabpanel">
                    <div class="table-responsive">
                        <table class="table table-bordered">
                            <thead>
                            <tr>
                                <th>Sender</th>
                                <th>Topic</th>
                                <th>Title</th>
                                <th>Received</th>
                            </tr>
                            </thead>

                            <tbody>
                            @foreach($inboxes as $inbox)
                                <tr class="{{ (($inbox->isSender() == true && $inbox->sender_seen_at == null) || ($inbox->isSender() == false && $inbox->receiver_seen_at == null)) ? 'text-bold' : '' }}">
                                    <td>{{ $inbox->senderName() == Auth::user()->name ? 'Me' : $inbox->senderName() }}</td>
                                    <td>{{ $inbox->topicName() }}</td>
                                    <td><a href="{{ route('buyer_view_chats', ['id' => $inbox->id]) }}">{{ $inbox->title }}</a></td>
                                    <td>{{ date('F j, Y H:i A', strtotime($inbox->items->last()->created_at)) }}</td>
                                </tr>
                            @endforeach
                            </tbody>
                        </table>
                    </div>
                </div>

                <div class="tab-pane fade" id="outbox" role="tabpanel">
                    <div class="table-responsive">
                        <table class="table table-bordered">
                            <thead>
                            <tr>
                                <th>To</th>
                                <th>Topic</th>
                                <th>Title</th>
                                <th>Send Date</th>
                            </tr>
                            </thead>

                            <tbody>
                            @foreach($outboxes as $outbox)
                                <tr>
                                    <td>
                                        {{ $outbox->receiverName() }}
                                    </td>

                                    <td>
                                        {{ $outbox->topicName() }}
                                    </td>

                                    <td><a href="{{ route('buyer_view_chats', ['id' => $outbox->id]) }}">{{ $outbox->title }}</a></td>
                                    <td>{{ date('F j, Y H:i A', strtotime($outbox->updated_at)) }}</td>
                                </tr>
                            @endforeach
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>
@stop

@section('mobile_filter')
    <div class="modal fade" id="modalNewMessage" role="dialog">
        <div class="modal-dialog modal-lg" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h4 class="modal-title">New Message</h4>
                    <button class="close" type="button" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                </div>

                <div class="modal-body">
                    <form id="form-msg">
                        <div class="form-group row">
                            <label class="col-2 col-form-label" for="text-input">To</label>
                            <div class="col-3">
                                <select name="type" class="form-control form-control-rounded form-control-sm" id="select-type">
                                    <option value="1">Vendor</option>
                                    <option value="2">StylePick</option>
                                </select>
                            </div>

                            <div class="col-7">
                                <select name="vendor" class="form-control form-control-rounded form-control-sm" id="select-vendor">
                                    <option value="">Select Vendor</option>

                                    @foreach($vendors as $vendor)
                                        <option value="{{ $vendor->id }}">{{ $vendor->company_name }}</option>
                                    @endforeach
                                </select>
                            </div>
                        </div>

                        <div class="form-group row">
                            <label class="col-2 col-form-label" for="text-input">Topics</label>
                            <div class="col-3">
                                <select name="topic" class="form-control form-control-rounded form-control-sm" id="select-topic">
                                    <option value="{{ MessageTopic::$GENERAL }}">General</option>
                                    <option value="{{ MessageTopic::$PRODUCT }}">Product</option>
                                    <option value="{{ MessageTopic::$ORDER }}">Order</option>
                                    <option value="{{ MessageTopic::$PAYMENT }}">Payment</option>
                                    <option value="{{ MessageTopic::$SHIPMENT }}">Shipment</option>
                                    <option value="{{ MessageTopic::$RETURN }}">Return</option>
                                    <option value="{{ MessageTopic::$OTHER }}">Other</option>
                                </select>
                            </div>
                        </div>

                        <div class="form-group row">
                            <label class="col-2 col-form-label" for="text-input">Title</label>
                            <div class="col-10">
                                <input name="title" class="form-control form-control-rounded form-control-sm" type="text" id="input-title">
                            </div>
                        </div>

                        <div class="form-group row">
                            <label class="col-2 col-form-label" for="text-input">Message</label>
                            <div class="col-10">
                                <textarea name="message" class="form-control form-control-rounded form-control-sm" id="input-message"></textarea>
                            </div>
                        </div>

                        <div class="form-group row">
                            <label class="col-2 col-form-label" for="text-input">Attach Files</label>
                            <div class="col-9" id="input-files">
                                <input name="attachments[]" class="input-file form-control form-control-rounded form-control-sm" type="file">
                            </div>

                            <div class="col-1" id="input-files-delete">
                                <a class="text-danger btn-delete-file input-file form-control form-control-rounded form-control-sm">X</a>
                            </div>
                        </div>
                    </form>
                </div>

                <div class="modal-footer">
                    <button class="btn btn-outline-secondary btn-sm" type="button" data-dismiss="modal">Close</button>
                    <button class="btn btn-primary btn-sm" type="button" id="btn-send-message">Send</button>
                </div>
            </div>
        </div>
    </div>
@stop

@section('additionalJS')
    <script>
        $(function () {
            $.ajaxSetup({
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                }
            });

            $('#btnNewMsg').click(function () {
                $('#modalNewMessage').modal('show');
            });

            $('#select-type').change(function () {
                var val = $(this).val();

                if (val == 2)
                    $('#select-vendor').hide();
                else
                    $('#select-vendor').show();
            });

            $(document).on('change', '.input-file', function () {
                if ($(this).val() != '') {
                    $('#input-files').append('<input name="attachments[]" class="input-file form-control form-control-rounded form-control-sm" type="file">');
                    $('#input-files-delete').append('<a class="text-danger btn-delete-file input-file form-control form-control-rounded form-control-sm">X</a>');
                }
            });

            $(document).on('click', '.btn-delete-file', function () {
                var index = $('.btn-delete-file').index($(this));

                $('.input-file:eq('+index+')').val('');
            });
            
            $('#btn-send-message').click(function () {
                if ($('#select-type').val() == '1' && $('#select-vendor').val() == '') {
                    alert('Select Vendor');
                    return;
                }

                if ($('#input-title').val() == '') {
                    alert('Enter title');
                    return;
                }

                if ($('#input-message').val() == '') {
                    alert('Enter message');
                    return;
                }

                $('#form-msg').submit();
            });

            $("#form-msg").submit(function(e) {
                e.preventDefault();
                var formData = new FormData(this);

                $.ajax({
                    url: "{{ route('buyer_add_messsage') }}",
                    type: 'POST',
                    data: formData,
                    success: function (data) {
                        location.reload();
                    },
                    cache: false,
                    contentType: false,
                    processData: false
                });
            });
        });
    </script>
@stop