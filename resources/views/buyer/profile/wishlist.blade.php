@extends('buyer.layouts.profile')

@section('additionalCSS')
    <link href="{{ asset('plugins/toastr/toastr.min.css') }}" rel="stylesheet">
@stop

@section('profile_content')
    <div class="table-responsive wishlist-table margin-bottom-none">
        <table class="table">
            <thead>
            <tr>
                <th colspan="2">Product Name</th>
            </tr>
            </thead>
            <tbody>
            @foreach($items as $item)
                <tr>
                    <td>
                        <div class="product-item">
                            <a class="product-thumb" href="{{ route('item_details_page', ['item' => $item->id]) }}">
                                @if (sizeof($item->images) > 0)
                                    <img src="{{ asset($item->images[0]->list_image_path) }}" alt="{{ $item->style_no }}">
                                @else
                                    <img src="{{ asset('images/no-image.png') }}">
                                @endif
                            </a>
                            <div class="product-info">
                                <h4 class="product-title"><a href="{{ route('item_details_page', ['item' => $item->id]) }}">{{ $item->style_no }}</a></h4>
                                <div><b>Vendor:</b>
                                    <div class="d-inline">{{ $item->vendor->company_name }}</div>
                                </div>
                                <div class="text-lg text-medium text-muted">
                                    @if ($item->orig_price != null)
                                        <del>${{ number_format($item->orig_price, 2, '.', '') }}</del>
                                    @endif

                                    ${{ number_format($item->price, 2, '.', '') }}
                                </div>
                            </div>
                        </div>
                    </td>
                    <td class="text-center">
                        <a class="btnAddToCart" data-id="{{ $item->id }}" title="Add To Cart"><i class="icon-bag"></i></a>
                        <a class="remove-from-cart btnRemove" data-id="{{ $item->id }}"><i class="icon-cross"></i></a>
                    </td>
                </tr>
            @endforeach
            </tbody>
        </table>
    </div>


@stop
<div class="modal fade" id="color-modal" tabindex="-1" role="dialog" aria-labelledby="modalLabelLarge"
     aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <h4 class="modal-title" id="modalLabelLarge">Add Item</h4>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">×</span>
                </button>
            </div>

            <div class="modal-body">
                <form id="form_new_item">
                    @csrf

                    <input type="hidden" name="item_id" value="" id="input_item_id">

                    <table class="table table-bordered" id="item_colors_table">

                    </table>
                </form>
            </div>

            <div class="modal-footer">
                <button class="btn btn-primary" id="btnItemAdd">Add</button>
            </div>
        </div>
    </div>
</div>

@section('additionalJS')
    <script type="text/javascript" src="{{ asset('plugins/toastr/toastr.min.js') }}"></script>
    <script>
        $(function () {
            $.ajaxSetup({
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                }
            });

            $('.btnRemove').click(function () {
                var id = $(this).data('id');
                $this = $(this);

                $.ajax({
                    method: "POST",
                    url: "{{ route('remove_from_wishlist') }}",
                    data: { id: id }
                }).done(function( data ) {
                    $this.closest('tr').remove();
                    toastr.success('Removed from Wish List.');
                });
            });

            $('.btnAddToCart').click(function () {
                var id = $(this).data('id');

                $.ajax({
                    method: "POST",
                    url: "{{ route('wishlist_item_details') }}",
                    data: { id: id }
                }).done(function( product ) {
                    $('#item_colors_table').html('');

                    $.each(product.colors, function (i, color) {
                        $('#item_colors_table').append('<tr><th>'+color.name+'</th><td><input name="colors['+color.id+']" class="input_color" type="text"></td></tr>');
                    });

                    $('#input_item_id').val(product.id);

                    $('#color-modal').modal('show');
                });
            });

            $('#btnItemAdd').click(function () {
                var error = false;

                $('.input_color').each(function () {
                    if (error)
                        return;

                    var count = $(this).val();

                    if (count != '' && !isInt(count)) {
                        error = true;
                        return alert('Invalid input.');
                    }
                });

                if (!error) {
                    $.ajax({
                        method: "POST",
                        url: "{{ route('wishlist_add_to_cart') }}",
                        data: $('#form_new_item').serialize()
                    }).done(function( data ) {
                        location.reload();
                    });
                }
            });

            function isInt(value) {
                return !isNaN(value) && (function(x) { return (x | 0) === x; })(parseFloat(value))
            }
        });
    </script>
@stop