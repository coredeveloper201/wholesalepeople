@extends('buyer.layouts.profile')

@section('profile_content')
    <div class="row">
        <div class="col-md-4">
            <a href="{{ route('view_messsage') }}" class="btn btn-primary">Back</a>
        </div>

        <div class="col-md-8">
            <table class="table table-bordered">
                <tr>
                    <th>Sender</th>
                    <td>{{ $message->isSender() == true ? 'Me' : $message->senderName() }}</td>

                    <th>Title</th>
                    <td>
                        @if ($message->order != null)
                            <a href="{{ route('show_order_details', ['order' => $message->order->id]) }}" target="_blank">{{ $message->title }}</a>
                        @else
                            {{ $message->title }}
                        @endif
                    </td>
                </tr>

                <tr>
                    <th>Receiver</th>
                    <td>{{ $message->isSender() == true ? $message->receiverName() : 'Me' }}</td>

                    <th>Topic</th>
                    <td>{{ $message->topicName() }}</td>
                </tr>
            </table>
        </div>
    </div>

    <hr><br>

    <div class="row">
        <div class="col-md-12">
            <div class="chat-conversation">
                <ul class="conversation-list">
                    @foreach($message->items as $item)
                        <li class="clearfix {{ (($isSender == true && $item->sender == 1) || ($isSender == false && $item->sender == 0)) ? 'odd' : '' }} mb-2">
                            <div class="chat-avatar">
                                <img src="{{ asset('images/default-avatar.png') }}">
                            </div>
                            <div class="conversation-text">
                                <div class="ctext-wrap">
                                    <i>{{ (($isSender == true && $item->sender == 1) || ($isSender == false && $item->sender == 0)) ? 'Me' : $personName }}</i>
                                    <p>
                                        {!! nl2br($item->message) !!}

                                        @if (sizeof($item->files) > 0)
                                            <br>
                                            @foreach($item->files as $file)
                                                <a class="text-info" href="{{ route('get_message_file', ['name' => $file->filename]) }}" target="_blank">{{ $file->original_filename }}</a>

                                                @if (!$loop->last)
                                                    <br>
                                                @endif
                                            @endforeach
                                        @endif
                                    </p>

                                    <span class="text-muted">{{ date('F j, Y H:i A', strtotime($item->created_at)) }}</span>
                                </div>
                            </div>
                        </li>
                    @endforeach
                </ul>


                <form id="form-msg">
                    <div class="row">
                        <div class="col-md-11">
                            <div class="input-group chat_btn">
                                <textarea class="form-control chat-input"
                                          id="input-msg" placeholder="Enter your text" name="msg"></textarea>
                            </div>
                        </div>

                        <div class="col-md-1">
                            <button class="btn btn-primary"
                                    id="btn-send"><i class="icon-play"></i></button>
                        </div>
                    </div>

                    <br>

                    <div class="row">
                        <div class="col-md-12">
                            <input type="hidden" name="id" value="{{ $message->id }}">

                            <div class="row">
                                <label class="col-md-2 col-form-label" for="text-input">Attach Files</label>
                                <div class="col-md-9" id="input-files">
                                    <input name="attachments[]" class="input-file form-control" type="file">
                                </div>

                                <div class="col-md-1" id="input-files-delete">
                                    <a class="text-danger btn-delete-file input-file form-control">X</a>
                                </div>
                            </div>
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </div>
@stop

@section('additionalJS')
    <script type="text/javascript" src="{{ asset('plugins/slimscroll/js/jquery.slimscroll.min.js') }}"></script>
    <script>
        $(function () {
            $.ajaxSetup({
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                }
            });

            $(".conversation-list").slimscroll({
                height: "500px",
                size: '5px',
                opacity: 0.2
            });

            $(document).on('change', '.input-file', function () {
                if ($(this).val() != '') {
                    $('#input-files').append('<input name="attachments[]" class="input-file form-control" type="file">');
                    $('#input-files-delete').append('<a class="text-danger btn-delete-file input-file form-control">X</a>');
                }
            });

            $(document).on('click', '.btn-delete-file', function () {
                var index = $('.btn-delete-file').index($(this));

                $('.input-file:eq('+index+')').val('');
            });

            $('#btn-send').click(function (e) {
                e.preventDefault();
                if ($('#input-msg').val() != '') {
                    var text = $('#input-msg').val();

                    $('#form-msg').submit();
                }
            });

            $("#form-msg").submit(function(e) {
                e.preventDefault();
                var formData = new FormData(this);

                $.ajax({
                    url: "{{ route('buyer_add_chat') }}",
                    type: 'POST',
                    data: formData,
                    success: function (data) {
                        location.reload();
                    },
                    cache: false,
                    contentType: false,
                    processData: false
                });
            });

            var scrollTo_int = $(".conversation-list").prop('scrollHeight') + 'px';
            $(".conversation-list").slimscroll({ scrollTo: scrollTo_int });
        });
    </script>
@stop