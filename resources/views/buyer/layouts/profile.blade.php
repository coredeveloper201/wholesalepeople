@extends('layouts.app')

@section('content')
<div class="container-fluid content">
    <div class="row">
        <div class="col-lg-3 hidden-md-down">
            <aside class="user-info-wrapper">
                <div class="user-info">
                    <div class="user-data">
                        <h4>{{ Auth::user()->first_name.' '.Auth::user()->last_name }}</h4><span>Joined {{ date('F d, Y', strtotime(Auth::user()->created_at)) }}</span>
                    </div>
                </div>
            </aside>
            <nav class="list-group">
                <a class="list-group-item {{ (Request::route()->getName() == 'buyer_show_orders') ? 'active' : '' }}"
                   href="{{ route('buyer_show_orders') }}"><i class="icon-bag"></i>Orders</a>
                <a class="list-group-item {{ (in_array(Request::route()->getName(), ['view_messsage', 'buyer_view_chats'])) ? 'active' : '' }}"
                   href="{{ route('view_messsage') }}"><i class="icon-mail"></i>Message {{ $unread_msg > 0 ? '('.$unread_msg.')' : '' }}</a>
                <a class="list-group-item {{ (Request::route()->getName() == 'view_wishlist') ? 'active' : '' }}"
                   href="{{ route('view_wishlist') }}"><i class="icon-heart"></i>Wishlist</a>
                <a class="list-group-item {{ (Request::route()->getName() == 'buyer_show_profile') ? 'active' : '' }}"
                   href="{{ route('buyer_show_profile') }}"><i class="icon-head"></i>Profile</a>
                <a class="list-group-item {{ (Request::route()->getName() == 'buyer_show_address') ? 'active' : '' }}"
                   href="{{ route('buyer_show_address') }}"><i class="icon-map"></i>Addresses</a>
                <a class="list-group-item {{ (Request::route()->getName() == 'buyer_show_feedback') ? 'active' : '' }}"
                   href="{{ route('buyer_show_feedback') }}"><i class="icon-paper"></i>Feedback</a>
                <a class="list-group-item {{ (Request::route()->getName() == 'buyer_show_credit_card') ? 'active' : '' }}"
                   href="{{ route('buyer_show_credit_card') }}"><i class="icon-ribbon"></i>Credit Card</a>
            </nav>
        </div>

        <div class="col-lg-9">
            @yield('profile_content')
        </div>
    </div>
</div>
@stop