@extends('layouts.app')

@section('content')
<div class="container content">
    <div class="row">
        <div class="col-md-12">
            <div class="checkout-steps hidden-xs-down">
                <a>5. Complete</a>
                <a><span class="angle"></span>4. Review</a>
                <a><span class="angle"></span>3. Payment</a>
                <a class="active"><span class="angle"></span>2. Shipping Method</a>
                <a class="completed"><span class="angle"></span><span class="step-indicator icon-circle-check"></span>1. Address</a>
            </div>

            <div class="checkout-steps d-block d-sm-none">
                <a class="completed"><span class="angle"></span><span class="step-indicator icon-circle-check"></span>1. Address</a>
                <a class="active"><span class="angle"></span>2. Shipping Method</a>
                <a><span class="angle"></span>3. Payment</a>
                <a><span class="angle"></span>4. Review</a>
                <a>5. Complete</a>
            </div>
        </div>
    </div>

    <form method="post" action="{{ route('show_shipping_method_post') }}">
        @csrf

        <input type="hidden" name="orderIds" value="{{ request()->get('id') }}">

        @foreach ($orders as $order)
            <input type="hidden" name="orderId[]" value="{{ $order->id }}">

            <h4>Shipping Method - {{ $order->vendor->company_name }}</h4>
            <hr class="padding-bottom-1x">
            <div class="table-responsive">
                <table class="table table-hover">
                    <thead class="thead-default">
                    <tr>
                        <th></th>
                        <th>Shipping method</th>
                    </tr>
                    </thead>
                    <tbody>
                    @foreach($order->vendor->shippingMethods as $shipping_method)
                        <tr>
                            <td class="align-middle">
                                <div class="custom-control custom-radio mb-0">
                                    <input class="custom-control-input" type="radio" id="{{ $shipping_method->id }}" name="shipping_method[{{ $loop->parent->index }}]"
                                           value="{{ $shipping_method->id }}"
                                           {{ $errors->any() ? (old('shipping_method.'.$loop->index) == $shipping_method->id ? 'checked' : '') :
                                            (($order->shipping_method_id == $shipping_method->id) ? 'checked' : '')}}>
                                    <label class="custom-control-label" for="{{ $shipping_method->id }}"></label>
                                </div>
                            </td>

                            <td class="align-middle">
                                <span class="text-medium">{{ $shipping_method->courier_id == 0 ? 'Other' : $shipping_method->courier->name }}</span><br>
                                <span class="text-muted text-sm">{{ $shipping_method->courier_id == 0 ? $shipping_method->ship_method_text : $shipping_method->shipMethod->name }}</span>
                            </td>

                        </tr>
                    @endforeach
                </table>

                @if ($errors->has('shipping_method.'.$loop->index))
                    <div class="form-control-feedback text-danger">Select a shipping method</div>
                @endif
            </div>
        @endforeach

        <div class="checkout-footer">
            <div class="column"><a class="btn btn-outline-secondary" href="{{ route('show_checkout') }}?id={{ request()->get('id') }}"><i class="icon-arrow-left"></i><span class="hidden-xs-down">&nbsp;Back</span></a></div>
            <div class="column"><button class="btn btn-primary" type="submit"><span class="hidden-xs-down">Continue&nbsp;</span><i class="icon-arrow-right"></i></button></div>
        </div>
    </form>
</div>
@stop