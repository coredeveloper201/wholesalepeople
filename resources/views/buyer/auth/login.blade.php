@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row">
        <div class="content col-md-6 margin-bottom-3x margin-top-3x">
            <h3>RETURNING BUYER</h3>
            <p>Please enter your e-mail address and password to log in</p>
            <form class="login-box" method="post" action="{{ route('buyer_login_post') }}">
                @csrf

                <div class="form-group input-group">
                    <input class="form-control" type="email" placeholder="Email" name="email" value="{{ old('email') }}" required><span class="input-group-addon"><i class="icon-mail"></i></span>
                </div>
                <div class="form-group input-group">
                    <input class="form-control" type="password" placeholder="Password" name="password" required><span class="input-group-addon"><i class="icon-lock"></i></span>
                </div>
                <div class="d-flex flex-wrap justify-content-between padding-bottom-1x">
                    <div class="custom-control custom-checkbox">
                        <input class="custom-control-input" type="checkbox" id="remember_me" name="remember_me" checked>
                        <label class="custom-control-label" for="remember_me">Remember me</label>
                    </div>

                    <a href="{{ route('password_reset_buyer') }}">Forget Password</a>
                </div>

                <div class="form-group has-danger">
                    <div class="form-control-feedback">{{ session('message') }}</div>
                </div>

                <div class="text-center text-sm-right">
                    <button class="btn btn-primary margin-bottom-none" type="submit">Login</button>
                </div>
            </form>
        </div>
        <div class="content col-md-6 margin-bottom-3x margin-top-3x">
            <div class="padding-top-3x hidden-md-up"></div>
            <h3>NEW BUYER</h3>
            <p>Welcome to Stylepick.net. You are about to enter our entire website featuring thousands of items, updated daily with new styles. We are for wholesale buyers only, and documentation is required for membership.</p>
            <a href="{{ route('buyer_register') }}" class="btn btn-primary">Create an account</a>
        </div>
    </div>
</div>
@stop