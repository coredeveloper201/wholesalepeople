@extends('layouts.app')

@section('content')
    <div class="container">
        <div class="content">
        	<div class="complete-title mb-1"><h3>Registration complete</h3><hr></div>
            <p>Thank you for registering a customer account. Your profile has been sumbitted and your account is now activated</p>
            <p>Please use your e-mail address and the specified password every time you place an order at stylepick.net</p>
            <p>Thank you, and have a wonderlad day</p>
            <p><b>Best Regards,</b></p>
            <p>TEAM - STYLEPICK </p>
        </div>
    </div>
    <!-- Event snippet for adwords leads conversion conversion page -->
<script>
  gtag('event', 'conversion', {
      'send_to': 'AW-794646121/NERHCJeq14cBEOms9foC',
      'transaction_id': ''
  });
</script>

@stop