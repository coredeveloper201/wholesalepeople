<?php
    use App\Enumeration\Role;
?>
<!DOCTYPE html>
<html lang="{{ app()->getLocale() }}">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1,user-scalable=0"/>

    <!-- CSRF Token -->
    <meta name="csrf-token" content="{{ csrf_token() }}">

    <title>{{ $meta_title }}</title>
    <meta name="description" content="{{ $meta_description }}" />
    <meta property="og:title" content="{{ $meta_title }}" />
    <meta property="og:og:description" content="{{ $meta_description }}" />
    <link rel="canonical" href="{{ url()->current() }}">

    <meta name="theme-color" content="#db2d2d" />

    <meta name="google-site-verification" content="2goRg__0GEXJEuClTf5YzwoyD_xYjR9MGndCOjvmLw8" />
    <meta name="msvalidate.01" content="FD911D23AE2B7CAF0EFBBEF6DE5E21C5" />
    <link rel="icon" href="{{ asset('images/stylepick.ico') }}" type="image/x-icon">
    <link rel="stylesheet" media="screen" href="{{ asset('themes/unishop/css/vendor.min.css') }}">
    <link id="mainStyles" rel="stylesheet" media="screen" href="{{ asset('themes/unishop/css/styles.css') }}?id={{ rand() }}">
    <script src="{{ asset('themes/unishop/js/modernizr.min.js') }}"></script>
    <!-- Global site tag (gtag.js) - Google Analytics -->
    <script async src="https://www.googletagmanager.com/gtag/js?id=UA-122475877-1"></script>
    <script>
      window.dataLayer = window.dataLayer || [];
      function gtag(){dataLayer.push(arguments);}
      gtag('js', new Date());

      gtag('config', 'UA-122475877-1');
      gtag('config', 'AW-794646121');
    </script>

    <script type="application/ld+json">
    {
      "@context": "http://schema.org",
      "@type": "Organization",
      "url": "https://stylepick.net/",
      "logo": "https://stylepick.net/images/logo_558x112.png"
    }
    </script>

    <script type="application/ld+json">
    {
      "@context": "http://schema.org",
      "@type": "Organization",
      "address": {
        "@type": "PostalAddress",
        "addressLocality": "Los Angeles,",
        "postalCode": "CA 90015",
        "streetAddress": "210 E. Olympic blvd #330",
        "addressRegion": "California"
      },
    }
    </script>

    <script type="application/ld+json">
    {
      "@context": "http://schema.org",
      "@type": "WebSite",
      "url": "https://stylepick.net/",
      "potentialAction": {
        "@type": "SearchAction",
        "target": "https://stylepick.net/search?s={search_term_string}",
        "query-input": "required name=search_term_string"
      }
    }
    </script>

    <style>
        .notification-has {
            background-color: rgba(255,82,82,0.09);
        }

        body.modal-open {
            overflow: hidden;
        }
    </style>
    @yield('additionalCSS')
</head>

<!-- Body-->
<body>
<!-- Off-Canvas Mobile Menu-->
<div class="offcanvas-container" id="mobile-menu">
    <nav class="offcanvas-menu">
        <ul class="menu">
            @if (!Auth::check() || (Auth::check() && Auth::user()->role != Role::$BUYER))
                <li><span><a href="{{ route('buyer_login') }}"><span>LOG IN</span></a></span></li>
                <li><span><a href="{{ route('buyer_register') }}"><span>REGISTER</span></a></span></li>
            @else
                <li class="has-children"><span><a href="{{ route('buyer_show_overview') }}"><span>My Account</span></a><span class="sub-menu-toggle"></span></span>
                    <ul class="offcanvas-submenu">
                        <li><a href="{{ route('buyer_show_orders') }}">ORDERS</a></li>
                        <li><a href="{{ route('view_wishlist') }}">WISHLIST</a></li>
                        <li><a href="{{ route('buyer_show_profile') }}">PROFILE</a></li>
                        <li><a href="{{ route('logout_buyer') }}">Logout</a></li>
                    </ul>
                </li>
            @endif

            <li class="has-children"><span><a href="{{ route('all_vendor_page') }}"><span>Vendors</span></a><span class="sub-menu-toggle"></span></span>
                <ul class="offcanvas-submenu">
                    @foreach($vendors as $vendor)
                        <li><a href="{{ route('vendor_or_parent_category', ['text' => changeSpecialChar($vendor->company_name)]) }}">{{ $vendor->company_name }}</a></li>
                    @endforeach
                </ul>
            </li>

            <li class="has-children"><span><a href="{{ route('new_arrival_home') }}"><span>New Arrivals</span></a><span class="sub-menu-toggle"></span></span>
                <ul class="offcanvas-submenu">
                    @foreach($by_arrival_dates as $by_arrival_date)
                        <li><a href="{{ route('new_arrival_page', ['D' => $by_arrival_date['day']]) }}">{{ $by_arrival_date['name'] }} <span class="text-muted">({{ $by_arrival_date['count'] }})</span></a></li>
                    @endforeach
                </ul>
            </li>

            @foreach($default_categories as $d_cat)
                <li class="has-children"><span><a href="{{ route('vendor_or_parent_category', ['category' => changeSpecialChar($d_cat['name'])]) }}"><span>{{ $d_cat['name'] }}</span></a><span class="sub-menu-toggle"></span></span>
                    @if (isset($d_cat['subCategories']) && sizeof($d_cat['subCategories']) > 0)
                        <ul class="offcanvas-submenu">
                            @if (sizeof($d_cat['subCategories']) == 1)
                                @if (isset($d_cat['subCategories'][0]['subCategories']) && sizeof($d_cat['subCategories'][0]['subCategories']) > 0)
                                    @foreach($d_cat['subCategories'][0]['subCategories'] as $d_sub2)
                                        <li><a href="{{ route('third_category', ['category' => changeSpecialChar($d_sub2['name']), 'second' => changeSpecialChar($d_cat['subCategories'][0]['name']), 'parent' => changeSpecialChar($d_cat['name'])]) }}">{{ $d_sub2['name'] }}</a></li>
                                    @endforeach
                                @endif
                            @else
                                @foreach($d_cat['subCategories'] as $d_sub)
                                    <li class="has-children"><span><a href="{{ route('second_category', ['category' => changeSpecialChar($d_sub['name']), 'parent' => changeSpecialChar($d_cat['name'])]) }}"><span>{{ $d_sub['name'] }}</span></a><span class="sub-menu-toggle"></span></span>
                                        @if (isset($d_sub['subCategories']) && sizeof($d_sub['subCategories']) > 0)
                                            <ul class="offcanvas-submenu">
                                                @foreach($d_sub['subCategories'] as $d_sub2)
                                                    <li><a href="{{ route('third_category', ['category' => changeSpecialChar($d_sub2['name']), 'second' => changeSpecialChar($d_sub['name']), 'parent' => changeSpecialChar($d_cat['name'])]) }}">{{ $d_sub2['name'] }}</a></li>
                                                @endforeach
                                            </ul>
                                        @endif
                                    </li>
                                @endforeach
                            @endif
                        </ul>
                    @endif
                </li>
            @endforeach
        </ul>
    </nav>
</div>
<header class="navbar">
    <div class="container-fluid">
        <div class="row top-menu">
            <!-- Search-->
            <form class="site-search" method="get" action="{{ route('search') }}">
                <input type="text" name="s" placeholder="Type to search..." autocomplete="off">
                <div class="search-tools"><span class="clear-search">Clear</span><span class="close-search"><i class="icon-cross"></i></span></div>
            </form>
            <div class="site-branding">
                <div class="inner">
                    <!-- Off-Canvas Toggle (#mobile-menu)--><a class="offcanvas-toggle menu-toggle" href="#mobile-menu" data-toggle="offcanvas"></a>
                    <!-- Site Logo--><a class="site-logo" href="{{ route('home') }}"><img src="{{ asset('images/logo.png') }}" alt="{{ config('app.name', 'Wholesalepeople') }}"></a>
                </div>
            </div>

            <!-- Main Navigation-->
            <nav class="site-menu">
                <ul>
                    <li class="has-megamenu"><a href="{{ route('all_vendor_page') }}"><span>Vendors</span></a>
                        <ul class="mega-menu">
                            <li><span class="mega-menu-title">Find Vendors</span>
                                <ul class="sub-menu" style="margin-bottom: 50px; overflow: hidden">
                                    <li style="float: left; margin-right: 10px;"><a href="{{ route('all_vendor_page') }}">All</a></li>
                                    <li style="float: left; margin-right: 10px;"><a href="{{ route('all_vendor_page', ['s' => 'A']) }}">A</a></li>
                                    <li style="float: left; margin-right: 10px;"><a href="{{ route('all_vendor_page', ['s' => 'B']) }}">B</a></li>
                                    <li style="float: left; margin-right: 10px;"><a href="{{ route('all_vendor_page', ['s' => 'C']) }}">C</a></li>
                                    <li style="float: left; margin-right: 10px;"><a href="{{ route('all_vendor_page', ['s' => 'D']) }}">D</a></li>
                                    <li style="float: left; margin-right: 10px;"><a href="{{ route('all_vendor_page', ['s' => 'E']) }}">E</a></li>
                                    <li style="float: left; margin-right: 10px;"><a href="{{ route('all_vendor_page', ['s' => 'F']) }}">F</a></li>
                                    <li style="float: left; margin-right: 10px;"><a href="{{ route('all_vendor_page', ['s' => 'G']) }}">G</a></li>
                                    <li style="float: left; margin-right: 10px;"><a href="{{ route('all_vendor_page', ['s' => 'H']) }}">H</a></li>
                                    <li style="float: left; margin-right: 10px;"><a href="{{ route('all_vendor_page', ['s' => 'I']) }}">I</a></li>
                                    <li style="float: left; margin-right: 10px;"><a href="{{ route('all_vendor_page', ['s' => 'J']) }}">J</a></li>
                                    <li style="float: left; margin-right: 10px;"><a href="{{ route('all_vendor_page', ['s' => 'K']) }}">K</a></li>
                                    <li style="float: left; margin-right: 10px;"><a href="{{ route('all_vendor_page', ['s' => 'L']) }}">L</a></li>
                                    <li style="float: left; margin-right: 10px;"><a href="{{ route('all_vendor_page', ['s' => 'M']) }}">M</a></li>
                                    <li style="float: left; margin-right: 10px;"><a href="{{ route('all_vendor_page', ['s' => 'N']) }}">N</a></li>
                                    <li style="float: left; margin-right: 10px;"><a href="{{ route('all_vendor_page', ['s' => 'O']) }}">O</a></li>
                                    <li style="float: left; margin-right: 10px;"><a href="{{ route('all_vendor_page', ['s' => 'P']) }}">P</a></li>
                                    <li style="float: left; margin-right: 10px;"><a href="{{ route('all_vendor_page', ['s' => 'Q']) }}">Q</a></li>
                                    <li style="float: left; margin-right: 10px;"><a href="{{ route('all_vendor_page', ['s' => 'R']) }}">R</a></li>
                                    <li style="float: left; margin-right: 10px;"><a href="{{ route('all_vendor_page', ['s' => 'S']) }}">S</a></li>
                                    <li style="float: left; margin-right: 10px;"><a href="{{ route('all_vendor_page', ['s' => 'T']) }}">T</a></li>
                                    <li style="float: left; margin-right: 10px;"><a href="{{ route('all_vendor_page', ['s' => 'U']) }}">U</a></li>
                                    <li style="float: left; margin-right: 10px;"><a href="{{ route('all_vendor_page', ['s' => 'V']) }}">V</a></li>
                                    <li style="float: left; margin-right: 10px;"><a href="{{ route('all_vendor_page', ['s' => 'W']) }}">W</a></li>
                                    <li style="float: left; margin-right: 10px;"><a href="{{ route('all_vendor_page', ['s' => 'X']) }}">X</a></li>
                                    <li style="float: left; margin-right: 10px;"><a href="{{ route('all_vendor_page', ['s' => 'Y']) }}">Y</a></li>
                                    <li style="float: left; margin-right: 10px;"><a href="{{ route('all_vendor_page', ['s' => 'Z']) }}">Z</a></li>
                                    <li style="float: left; margin-right: 10px;"><a href="{{ route('all_vendor_page', ['s' => '1']) }}">#</a></li>
                                </ul>

                                <select class="form-control" id="mm_select_vendor">
                                    <option value="">Select Vendor</option>

                                    @foreach($vendors as $vendor)
                                        <option value="{{ changeSpecialChar($vendor->company_name) }}">{{ $vendor->company_name }}</option>
                                    @endforeach
                                </select>
                            </li>

                            <li><span class="mega-menu-title">Category</span>
                                <ul class="sub-menu">
                                    @foreach($default_categories as $d_cat)
                                        <li><a href="{{ route('vendor_or_parent_category', ['category' => changeSpecialChar($d_cat['name'])]) }}">{{ $d_cat['name'] }}</a></li>
                                    @endforeach
                                </ul>
                            </li>

                            @foreach($menu_vendor_banners as $banner)
                            <li>
                                <span class="mega-menu-title">Feature Vendor</span>
                                <a href="{{ route('vendor_or_parent_category', ['text' => changeSpecialChar($banner['vendor_name'])]) }}">
                                    <img src="{{ asset($banner['image_path']) }}" style="width: 90%" alt="{{ $banner['vendor_name'] }}">
                                </a>

                                {{--<section class="promo-box" style="background-image: url(img/banners/02.jpg);"><span class="overlay-dark" style="opacity: .4;"></span>
                                    <div class="promo-box-content text-center padding-top-2x padding-bottom-2x">
                                        <h4 class="text-light text-thin text-shadow">New Collection of</h4>
                                        <h3 class="text-bold text-light text-shadow">Sunglasses</h3><a class="btn btn-sm btn-primary" href="#">Shop Now</a>
                                    </div>
                                </section>--}}
                            </li>
                            @endforeach
                        </ul>
                    </li>

                    <li><a href="{{ route('new_arrival_home') }}"><span>New Arrivals</span></a>
                        <ul class="sub-menu">
                            <li><a href="#" class="text-muted">By Arrival Date</a></li>

                            @foreach($by_arrival_dates as $by_arrival_date)
                                <li><a href="{{ route('new_arrival_page', ['D' => $by_arrival_date['day']]) }}">{{ $by_arrival_date['name'] }} <span class="text-muted">({{ $by_arrival_date['count'] }})</span></a></li>
                            @endforeach

                            <li class="li-margin-top-10"><a href="#" class="text-muted">By Category</a></li>

                            @foreach($by_category as $item)
                                <li><a href="{{ route('new_arrival_page', ['C' => $item['id']]) }}">{{ $item['name'] }} <span class="text-muted">({{ $item['count'] }})</span></a></li>
                            @endforeach
                        </ul>
                    </li>

                    @foreach($default_categories as $d_cat)
                        <li><a href="{{ route('vendor_or_parent_category', ['category' => changeSpecialChar($d_cat['name'])]) }}"><span>{{ $d_cat['name'] }}</span></a>
                            @if (isset($d_cat['subCategories']) && sizeof($d_cat['subCategories']) > 0)
                                <ul class="sub-menu">
                                    @if (sizeof($d_cat['subCategories']) == 1)
                                        @if (isset($d_cat['subCategories'][0]['subCategories']) && sizeof($d_cat['subCategories'][0]['subCategories']) > 0)
                                            @foreach($d_cat['subCategories'][0]['subCategories'] as $d_sub2)
                                                <li><a href="{{ route('third_category', ['category' => changeSpecialChar($d_sub2['name']), 'second' => changeSpecialChar($d_cat['subCategories'][0]['name']), 'parent' => changeSpecialChar($d_cat['name'])]) }}">{{ $d_sub2['name'] }}</a></li>
                                            @endforeach
                                        @endif
                                    @else
                                        @foreach($d_cat['subCategories'] as $d_sub)
                                            <li class="{{ (isset($d_sub['subCategories']) && sizeof($d_sub['subCategories']) > 0) ? 'has-children' : '' }}"><a href="{{ route('second_category', ['category' => changeSpecialChar($d_sub['name']), 'parent' => changeSpecialChar($d_cat['name'])]) }}"><span>{{ $d_sub['name'] }}</span></a>
                                                @if (isset($d_sub['subCategories']) && sizeof($d_sub['subCategories']) > 0)
                                                    <ul class="sub-menu">
                                                        @foreach($d_sub['subCategories'] as $d_sub2)
                                                            <li><a href="{{ route('third_category', ['category' => changeSpecialChar($d_sub2['name']), 'second' => changeSpecialChar($d_sub['name']), 'parent' => changeSpecialChar($d_cat['name'])]) }}">{{ $d_sub2['name'] }}</a></li>
                                                        @endforeach
                                                    </ul>
                                                @endif
                                            </li>
                                        @endforeach
                                    @endif
                                </ul>
                            @endif
                        </li>
                    @endforeach
                </ul>
            </nav>
            <!-- Toolbar-->
            <div class="toolbar">
                <div class="inner">
                    <div class="tools">
                            @if (!Auth::check() || (Auth::check() && Auth::user()->role != Role::$BUYER))
                            <div class="login"><a href="{{ route('buyer_login') }}">Sign In</a></div>
                            <div class="register register hidden-xs-down"><a href="{{ route('buyer_register') }}">Register</a></div>
                            @endif

                        @if (Auth::check() && Auth::user()->role == Role::$BUYER)
                            <div class="search"><i class="icon-search"></i></div>
                            <div class="account" id="btnShowProfileOverview"><a href="{{ route('buyer_show_overview') }}"></a><i class="icon-head"></i>
                                <ul class="toolbar-dropdown">
                                    <li class="sub-menu-user">
                                        <div class="user-ava"><img src="{{ asset('images/default-avatar.png') }}" alt="Daniel Adams">
                                        </div>
                                        <div class="user-info">
                                            <h6 class="user-name">{{ Auth::user()->first_name.' '.Auth::user()->last_name }}</h6>
                                        </div>
                                    </li>
                                    <li><a href="{{ route('buyer_show_orders') }}">Orders List</a></li>
                                    <li><a href="{{ route('view_wishlist') }}">Wishlist</a></li>
                                    <li><a href="{{ route('buyer_show_profile') }}">My Profile</a></li>
                                    <li class="sub-menu-separator"></li>
                                    <li><a href="{{ route('logout_buyer') }}" id="btnLogOut"> <i class="icon-unlock"></i>Logout</a></li>
                                </ul>
                            </div>

                            <div class="hidden-sm-down cart {{ (sizeof($notifications) > 0) ? 'notification-has' : '' }}" id="btnShowNotifications"><a href="{{ route('view_all_notification') }}"></a><i class="icon-bell"></i>
                                @if (sizeof($notifications) > 0)
                                    <span class="count">{{ sizeof($notifications) }}</span>
                                @endif

                                <div class="toolbar-dropdown">
                                    @foreach($notifications as $notification)
                                        <div class="dropdown-product-item">
                                            <div class="dropdown-product-info">
                                                <a class="dropdown-product-title"
                                                   href="{{ route('view_notification', ['id' => $notification->id, 'link' => $notification->link]) }}">
                                                    {{ $notification->text }}
                                                </a>
                                                <span class="dropdown-product-details">{{ date('F d, Y g:i a', strtotime($notification->created_at)) }}</span>
                                            </div>
                                        </div>
                                    @endforeach

                                    <div class="toolbar-dropdown-group">
                                        <div class="column"><a class="btn btn-sm btn-block btn-secondary" href="{{ route('view_all_notification') }}">View All Notifications</a></div>
                                    </div>
                                </div>
                            </div>

                            <div class="cart" id="btnShowCart"><a href="{{ route('show_cart') }}"></a><i class="icon-bag"></i><span class="count">{{ $cart_items['total']['total_qty'] }}</span><span class="subtotal">${{ sprintf('%0.2f', $cart_items['total']['total_price']) }}</span>
                                @if (sizeof($cart_items) > 1)
                                    <div class="toolbar-dropdown">
                                        @foreach($cart_items as $key => $ci)
                                            @if ($key != 'total')
                                                <div class="dropdown-product-item">
                                                    <a class="dropdown-product-thumb" href="{{ $ci['details_url'] }}">
                                                        <img src="{{ $ci['image_path'] }}" alt="Product" style="height: 55px !important;">
                                                    </a>

                                                    <div class="dropdown-product-info">
                                                        <a class="dropdown-product-title" href="{{ $ci['details_url'] }}">{{ $ci['name'] }}</a>
                                                        <span class="dropdown-product-details">{{ $ci['qty'] }} x ${{ sprintf('%0.2f', $ci['price']) }}</span>
                                                    </div>
                                                </div>
                                            @endif
                                        @endforeach
                                        <div class="toolbar-dropdown-group">
                                            <div class="column"><span class="text-lg">Total:</span></div>
                                            <div class="column text-right"><span class="text-lg text-medium">${{ sprintf('%0.2f', $cart_items['total']['total_price']) }}&nbsp;</span></div>
                                        </div>
                                        <div class="toolbar-dropdown-group">
                                            <div class="column"><a class="btn btn-sm btn-block btn-secondary" href="{{ route('show_cart') }}">View Cart</a></div>
                                        </div>
                                    </div>
                                @endif
                            </div>
                        @endif
                    </div>
                </div>
            </div>
        </div>
    </div>
</header>

@yield('mobile_filter')

<!-- Off-Canvas Wrapper-->
<div class="offcanvas-wrapper">
    <!-- Page Content-->

    {{--@if (isset($page_title) && $page_title != '')
        <div class="page-title">
            <div class="container">
                <div class="column">
                    <h1>{{ $page_title or '' }}</h1>
                </div>

                <div class="column">
                    <ul class="breadcrumbs">
                        <li><a href="index.html">Home</a>
                        </li>
                        <li class="separator">&nbsp;</li>
                        <li>Shop Grid Left Sidebar</li>
                    </ul>
                </div>
            </div>
        </div>
    @endif--}}

    @yield('slider')

    <div class="padding-bottom-1x padding-top-1x mb-1">
        @yield('content')
    </div>

    <!-- Site Footer-->
    <footer class="site-footer">
        <div class="container-fluid">
            <div class="row">
                <div class="col-lg-3 col-md-6 col-sm-6">
                    <!-- About Us-->
                    <section class="widget widget-links widget-light-skin">
                        <h3 class="widget-title">Company</h3>
                        <ul>
                            <li><a href="{{ route('about_us') }}">About Us</a></li>
                            <li><a href="{{ route('contact_us') }}">Contact Us</a></li>
                            <li><a href="{{ route('return_exchange') }}">Returns & Exchange</a></li>
                            <li><a href="{{ route('show_schedule') }}">Show Schedule</a></li>
                        </ul>
                    </section>
                </div>
                <div class="col-lg-3 col-md-6 col-sm-6">
                    <!-- Mobile App Buttons-->
                    <section class="widget widget-links widget-light-skin">
                        <h3 class="widget-title">Quick Links</h3>
                        <ul>
                            <li><a href="{{ route('buyer_register') }}">Be a Buyer</a></li>
                            <li><a href="{{ route('vendor_register') }}">Be a Vendor</a></li>
                            <li><a href="{{ route('photoshoot') }}">Photo Shoot</a></li>
                        </ul>
                    </section>
                </div>
                <div class="col-lg-3 col-md-6 col-sm-6">
                    <!-- Account / Shipping Info-->
                    <section class="widget widget-links widget-light-skin">
                        <h3 class="widget-title">Support</h3>
                        <ul>
                            <li><a href="{{ route('buyer_show_profile') }}">Your Account</a></li>
                            <li><a href="{{ route('shipping_information') }}">Shipping information</a></li>
                            <li><a href="{{ route('refund_policy') }}">Refunds Policy</a></li>
                            <li><a href="{{ route('privacy_policy') }}">Privacy Policy</a></li>
                            <li><a href="{{ route('terms_conditions') }}">Terms and Conditions</a></li>
                        </ul>
                    </section>
                </div>
                <div class="col-lg-3 col-md-6 col-sm-6">
                    <!-- Contact Info-->
                    <section class="widget widget-light-skin">
                        <h3 class="widget-title">Get In Touch With Us</h3>
                        <ul class="list-unstyled text-sm text-black">
                            <li><span class="opacity-80">Monday-Friday: </span>9.00 am - 5.30 pm</li>
                            <li><span class="opacity-80">Saturday: </span>Closed</li>
                        </ul>
                        <p>Email: <a class="navi-link-dark" href="#">info@stylepick.net</a></p>

                        <a class="social-button shape-circle sb-facebook sb-light-skin" target="_blank" href="https://www.facebook.com/stylepickusa"><i class="socicon-facebook"></i></a>
                        <a class="social-button shape-circle sb-twitter sb-light-skin" target="_blank" href="https://twitter.com/style_pick"><i class="socicon-twitter"></i></a>
                        <a class="social-button shape-circle sb-instagram sb-light-skin" target="_blank" href="https://www.instagram.com/stylepickusa"><i class="socicon-instagram"></i></a>
                        <a class="social-button shape-circle sb-google-plus sb-light-skin" target="_blank" href="https://plus.google.com/u/0/115710768340353536502"><i class="socicon-googleplus"></i></a>
                    </section>
                </div>
            </div>
            <div class="row">
                <div class="col-md-5 padding-bottom-1x">
                    <div class="margin-top-1x hidden-md-up"></div>
                    <!--Subscription-->
                    {{--<form class="subscribe-form" action="//rokaux.us12.list-manage.com/subscribe/post?u=c7103e2c981361a6639545bd5&amp;amp;id=1194bb7544" method="post" target="_blank" novalidate>
                        <div class="clearfix">
                            <div class="input-group input-light">
                                <input class="form-control" type="email" name="EMAIL" placeholder="Your e-mail"><span class="input-group-addon"><i class="icon-mail"></i></span>
                            </div>
                            <!-- real people should not fill this in and expect good things - do not remove this or risk form bot signups-->
                            <div style="position: absolute; left: -5000px;" aria-hidden="true">
                                <input type="text" name="b_c7103e2c981361a6639545bd5_1194bb7544" tabindex="-1">
                            </div>
                            <button class="btn btn-primary" type="submit"><i class="icon-check"></i></button>
                        </div><span class="form-text text-sm text-white opacity-50">Subscribe to our Newsletter to receive early discount offers, latest news, sales and promo information.</span>
                    </form>--}}
                </div>
            </div>
        </div>
        <!-- Copyright-->
            <p class="footer-copyright">© 2018 All rights reserved. <i class="icon-heart text-danger"></i></p>
    </footer>
</div>

<!-- Back To Top Button-->
<a class="scroll-to-top-btn" href="#"><i class="icon-arrow-up"></i></a>
<!-- Backdrop-->
<div class="site-backdrop"></div>

<!-- JavaScript (jQuery) libraries, plugins and custom scripts-->
<script src="{{ asset('themes/unishop/js/vendor.min.js') }}"></script>
<script src="{{ asset('themes/unishop/js/scripts.min.js') }}"></script>
<script>
    $(function () {
        $('#mm_select_vendor').change(function () {
            var id = $(this).val();

            if (id != '') {
                var url = '{{ route('vendor_or_parent_category', ['text' => '']) }}';

                window.location.replace(url+'/'+id);
            }
        });

        $('#btnShowCart').click(function () {
            location.replace('{{ route('show_cart') }}');
        });

        $('#btnShowNotifications').click(function () {
            location.replace('{{ route('view_all_notification') }}');
        });

        $('#btnShowProfileOverview').click(function () {
            location.replace('{{ route('buyer_show_overview') }}');
        });
    });
</script>
@yield('additionalJS')
</body>
</html>