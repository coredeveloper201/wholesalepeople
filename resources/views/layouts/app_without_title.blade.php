<?php use App\Enumeration\Role; ?>
<!DOCTYPE html>
<html lang="{{ app()->getLocale() }}">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1,user-scalable=0"/>

    <!-- CSRF Token -->
    <meta name="csrf-token" content="{{ csrf_token() }}">

    <title>{{ config('app.name', 'Wholesalepeople') }}</title>


    <link rel="stylesheet" media="screen" href="{{ asset('themes/unishop/css/vendor.min.css') }}">
    <link id="mainStyles" rel="stylesheet" media="screen" href="{{ asset('themes/unishop/css/styles.css') }}">
    <script src="{{ asset('themes/unishop/js/modernizr.min.js') }}"></script>
    @yield('additionalCSS')
</head>

<!-- Body-->
<body>
<!-- Off-Canvas Category Menu-->
<div class="offcanvas-container" id="shop-categories">
    <div class="offcanvas-header">
        <h3 class="offcanvas-title">Shop Categories</h3>
    </div>
    <nav class="offcanvas-menu">
        <ul class="menu">
            <li class="has-children"><span><a href="#">Men's Shoes</a><span class="sub-menu-toggle"></span></span>
                <ul class="offcanvas-submenu">
                    <li><a href="#">Sneakers</a></li>
                    <li><a href="#">Loafers</a></li>
                    <li><a href="#">Boat Shoes</a></li>
                    <li><a href="#">Sandals</a></li>
                    <li><a href="#">View All</a></li>
                </ul>
            </li>
        </ul>
    </nav>
</div>

<!-- Off-Canvas Mobile Menu-->
<div class="offcanvas-container" id="mobile-menu">
    <nav class="offcanvas-menu">
        <ul class="menu">
            <li class="has-children active"><span><a href="index.html"><span>Home</span></a><span class="sub-menu-toggle"></span></span>
                <ul class="offcanvas-submenu">
                    <li class="active"><a href="index.html">Featured Products Slider</a></li>
                    <li><a href="home-featured-categories.html">Featured Categories</a></li>
                    <li><a href="home-collection-showcase.html">Products Collection Showcase</a></li>
                </ul>
            </li>
        </ul>
    </nav>
</div>

<!-- Topbar-->
<div class="topbar">
    <div class="topbar-column"><a class="hidden-md-down" href="mailto:support@unishop.com"><i class="icon-mail"></i>&nbsp; support@unishop.com</a><a class="hidden-md-down" href="tel:00331697720"><i class="icon-bell"></i>&nbsp; 00 33 169 7720</a><a class="social-button sb-facebook shape-none sb-dark" href="#" target="_blank"><i class="socicon-facebook"></i></a><a class="social-button sb-twitter shape-none sb-dark" href="#" target="_blank"><i class="socicon-twitter"></i></a><a class="social-button sb-instagram shape-none sb-dark" href="#" target="_blank"><i class="socicon-instagram"></i></a><a class="social-button sb-pinterest shape-none sb-dark" href="#" target="_blank"><i class="socicon-pinterest"></i></a>
    </div>
</div>
<!-- Navbar-->

<!-- Remove "navbar-sticky" class to make navigation bar scrollable with the page.-->
<header class="navbar navbar-sticky">
    <!-- Search-->
    <form class="site-search" method="get">
        <input type="text" name="site_search" placeholder="Type to search...">
        <div class="search-tools"><span class="clear-search">Clear</span><span class="close-search"><i class="icon-cross"></i></span></div>
    </form>
    <div class="site-branding">
        <div class="inner">
            <!-- Off-Canvas Toggle (#shop-categories)--><a class="offcanvas-toggle cats-toggle" href="#shop-categories" data-toggle="offcanvas"></a>
            <!-- Off-Canvas Toggle (#mobile-menu)--><a class="offcanvas-toggle menu-toggle" href="#mobile-menu" data-toggle="offcanvas"></a>
            <!-- Site Logo--><a class="site-logo" href="{{ url('/') }}"><img src="{{ asset('images/logo.png') }}" alt="Wholesalepeople"></a>
        </div>
    </div>

    <!-- Main Navigation-->
    <nav class="site-menu">
        <ul>
            @foreach($default_categories as $d_cat)
                <li><a href="{{ route('parent_category_page', ['category' => $d_cat['id']]) }}"><span>{{ $d_cat['name'] }}</span></a>
                    @if (isset($d_cat['subCategories']) && sizeof($d_cat['subCategories']) > 0)
                        <ul class="sub-menu">
                            @if (sizeof($d_cat['subCategories']) == 1)
                                @if (isset($d_cat['subCategories'][0]['subCategories']) && sizeof($d_cat['subCategories'][0]['subCategories']) > 0)
                                    @foreach($d_cat['subCategories'][0]['subCategories'] as $d_sub2)
                                        <li><a href="{{ route('catalog_page', ['category' => $d_sub2['id']]) }}">{{ $d_sub2['name'] }}</a></li>
                                    @endforeach
                                @endif
                            @else
                                @foreach($d_cat['subCategories'] as $d_sub)
                                    <li class="{{ (isset($d_sub['subCategories']) && sizeof($d_sub['subCategories']) > 0) ? 'has-children' : '' }}"><a href="{{ route('second_parent_category_page', ['category' => $d_sub['id']]) }}"><span>{{ $d_sub['name'] }}</span></a>
                                        @if (isset($d_sub['subCategories']) && sizeof($d_sub['subCategories']) > 0)
                                            <ul class="sub-menu">
                                                @foreach($d_sub['subCategories'] as $d_sub2)
                                                    <li><a href="{{ route('catalog_page', ['category' => $d_sub2['id']]) }}">{{ $d_sub2['name'] }}</a></li>
                                                @endforeach
                                            </ul>
                                        @endif
                                    </li>
                                @endforeach
                            @endif
                        </ul>
                    @endif
                </li>
            @endforeach
        </ul>
    </nav>
    <!-- Toolbar-->
    <div class="toolbar">
        <div class="inner">
            <div class="tools">
                @if (!Auth::check() || (Auth::check() && Auth::user()->role != Role::$BUYER))
                    <a class="btn btn-link-secondary" href="{{ route('buyer_login') }}">Sign In</a>
                    <a class="btn btn-link-secondary" href="{{ route('buyer_register') }}">Register</a>
                @endif
                <div class="search"><i class="icon-search"></i></div>

                @if (Auth::check() && Auth::user()->role == Role::$BUYER)
                    <div class="account"><a href="#"></a><i class="icon-head"></i>
                        <ul class="toolbar-dropdown">
                            <li class="sub-menu-user">
                                <div class="user-ava"><img src="{{ asset('images/default-avatar.png') }}" alt="Daniel Adams">
                                </div>
                                <div class="user-info">
                                    <h6 class="user-name">{{ Auth::user()->first_name.' '.Auth::user()->last_name }}</h6>
                                </div>
                            </li>
                            <li><a href="{{ route('buyer_show_orders') }}">Orders List</a></li>
                            <li><a href="{{ route('buyer_show_profile') }}">My Profile</a></li>
                            <li class="sub-menu-separator"></li>
                            <li><a href="#" id="btnLogOut"> <i class="icon-unlock"></i>Logout</a></li>
                        </ul>
                    </div>

                    <div class="cart"><a href="#"></a><i class="icon-bag"></i><span class="count">{{ $cart_items['total']['total_qty'] }}</span><span class="subtotal">${{ sprintf('%0.2f', $cart_items['total']['total_price']) }}</span>
                        @if (sizeof($cart_items) > 1)
                            <div class="toolbar-dropdown">
                                @foreach($cart_items as $key => $ci)
                                    @if ($key != 'total')
                                        <div class="dropdown-product-item">
                                            <a class="dropdown-product-thumb" href="{{ $ci['details_url'] }}">
                                                <img src="{{ $ci['image_path'] }}" alt="Product" style="height: 55px !important;">
                                            </a>

                                            <div class="dropdown-product-info">
                                                <a class="dropdown-product-title" href="{{ $ci['details_url'] }}">{{ $ci['name'] }}</a>
                                                <span class="dropdown-product-details">{{ $ci['qty'] }} x ${{ sprintf('%0.2f', $ci['price']) }}</span>
                                            </div>
                                        </div>
                                    @endif
                                @endforeach
                                <div class="toolbar-dropdown-group">
                                    <div class="column"><span class="text-lg">Total:</span></div>
                                    <div class="column text-right"><span class="text-lg text-medium">${{ sprintf('%0.2f', $cart_items['total']['total_price']) }}&nbsp;</span></div>
                                </div>
                                <div class="toolbar-dropdown-group">
                                    <div class="column"><a class="btn btn-sm btn-block btn-secondary" href="{{ route('show_cart') }}">View Cart</a></div>
                                </div>
                            </div>
                        @endif
                    </div>

                    <form id="logoutForm" class="" action="{{ route('logout_buyer') }}" method="post">
                        {{ csrf_field() }}
                    </form>
                @endif
            </div>
        </div>
    </div>
</header>

<!-- Off-Canvas Wrapper-->
<div class="offcanvas-wrapper">
    <!-- Page Content-->
    @yield('slider')
    <div class="padding-bottom-3x mb-1 padding-top-1x">
        @yield('content')
    </div>

    <!-- Site Footer-->
    <footer class="site-footer">
        <div class="container">
            <div class="row">
                <div class="col-lg-3 col-md-6">
                    <!-- Contact Info-->
                    <section class="widget widget-light-skin">
                        <h3 class="widget-title">Get In Touch With Us</h3>
                        <p class="text-white">Phone: 00 33 169 7720</p>
                        <ul class="list-unstyled text-sm text-white">
                            <li><span class="opacity-50">Monday-Friday:</span>9.00 am - 8.00 pm</li>
                            <li><span class="opacity-50">Saturday:</span>10.00 am - 6.00 pm</li>
                        </ul>
                        <p><a class="navi-link-light" href="#">support@unishop.com</a></p><a class="social-button shape-circle sb-facebook sb-light-skin" href="#"><i class="socicon-facebook"></i></a><a class="social-button shape-circle sb-twitter sb-light-skin" href="#"><i class="socicon-twitter"></i></a><a class="social-button shape-circle sb-instagram sb-light-skin" href="#"><i class="socicon-instagram"></i></a><a class="social-button shape-circle sb-google-plus sb-light-skin" href="#"><i class="socicon-googleplus"></i></a>
                    </section>
                </div>
                <div class="col-lg-3 col-md-6">
                    <!-- Mobile App Buttons-->
                    <section class="widget widget-light-skin">
                        <h3 class="widget-title">Our Mobile App</h3><a class="market-button apple-button mb-light-skin" href="#"><span class="mb-subtitle">Download on the</span><span class="mb-title">App Store</span></a><a class="market-button google-button mb-light-skin" href="#"><span class="mb-subtitle">Download on the</span><span class="mb-title">Google Play</span></a><a class="market-button windows-button mb-light-skin" href="#"><span class="mb-subtitle">Download on the</span><span class="mb-title">Windows Store</span></a>
                    </section>
                </div>
                <div class="col-lg-3 col-md-6">
                    <!-- About Us-->
                    <section class="widget widget-links widget-light-skin">
                        <h3 class="widget-title">About Us</h3>
                        <ul>
                            <li><a href="#">Careers</a></li>
                            <li><a href="#">About Unishop</a></li>
                            <li><a href="#">Our Story</a></li>
                            <li><a href="#">Services</a></li>
                            <li><a href="#">Our Blog</a></li>
                        </ul>
                    </section>
                </div>
                <div class="col-lg-3 col-md-6">
                    <!-- Account / Shipping Info-->
                    <section class="widget widget-links widget-light-skin">
                        <h3 class="widget-title">Account &amp; Shipping Info</h3>
                        <ul>
                            <li><a href="#">Your Account</a></li>
                            <li><a href="#">Shipping Rates & Policies</a></li>
                            <li><a href="#">Refunds & Replacements</a></li>
                            <li><a href="#">Taxes</a></li>
                            <li><a href="#">Delivery Info</a></li>
                            <li><a href="#">Affiliate Program</a></li>
                        </ul>
                    </section>
                </div>
            </div>
            <hr class="hr-light mt-2 margin-bottom-2x">
            <div class="row">
                <div class="col-md-7 padding-bottom-1x">
                    <!-- Payment Methods-->
                    <div class="margin-bottom-1x" style="max-width: 615px;"><img src="img/payment_methods.png" alt="Payment Methods">
                    </div>
                </div>
                <div class="col-md-5 padding-bottom-1x">
                    <div class="margin-top-1x hidden-md-up"></div>
                    <!--Subscription-->
                    <form class="subscribe-form" action="//rokaux.us12.list-manage.com/subscribe/post?u=c7103e2c981361a6639545bd5&amp;amp;id=1194bb7544" method="post" target="_blank" novalidate>
                        <div class="clearfix">
                            <div class="input-group input-light">
                                <input class="form-control" type="email" name="EMAIL" placeholder="Your e-mail"><span class="input-group-addon"><i class="icon-mail"></i></span>
                            </div>
                            <!-- real people should not fill this in and expect good things - do not remove this or risk form bot signups-->
                            <div style="position: absolute; left: -5000px;" aria-hidden="true">
                                <input type="text" name="b_c7103e2c981361a6639545bd5_1194bb7544" tabindex="-1">
                            </div>
                            <button class="btn btn-primary" type="submit"><i class="icon-check"></i></button>
                        </div><span class="form-text text-sm text-white opacity-50">Subscribe to our Newsletter to receive early discount offers, latest news, sales and promo information.</span>
                    </form>
                </div>
            </div>
            <!-- Copyright-->
            <p class="footer-copyright">© All rights reserved. Made with &nbsp;<i class="icon-heart text-danger"></i><a href="http://rokaux.com/" target="_blank"> &nbsp;by rokaux</a></p>
        </div>
    </footer>
</div>

<!-- Back To Top Button--><a class="scroll-to-top-btn" href="#"><i class="icon-arrow-up"></i></a>
<!-- Backdrop-->
<div class="site-backdrop"></div>

<!-- JavaScript (jQuery) libraries, plugins and custom scripts-->
<script src="{{ asset('themes/unishop/js/vendor.min.js') }}"></script>
<script src="{{ asset('themes/unishop/js/scripts.min.js') }}"></script>
<script>
    $(function () {
        $('#btnLogOut').click(function () {
            $('#logoutForm').submit();
        });
    })
</script>
@yield('additionalJS')
</body>
</html>