<?php

use Illuminate\Http\Request;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

/*Route::middleware('auth:api')->get('/user', function (Request $request) {
    return $request->user();
});*/

// Item
Route::post('items', 'Api\ItemController@getItems');
Route::post('create/item', 'Api\ItemController@createItem');
Route::post('item/change_status', 'Api\ItemController@changeStatus');

// Color
Route::post('color/add', 'Api\ColorController@addColor');

// Pack
Route::post('pack/add', 'Api\PackController@addPack');
Route::post('vendor/packs', 'Api\PackController@getPacks');
Route::post('vendor/pack/details', 'Api\PackController@getPackDetails');

// Default Categories
Route::get('categories', 'Api\CategoryController@defaultCategories');
Route::post('vendor/categories', 'Api\CategoryController@vendorCategories');