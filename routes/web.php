<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/
//Auth::routes();
//Route::get('/home', 'HomeController@index')->name('home');
Route::redirect('/home', '/');

// Sitemap
Route::get('/sitemap', 'SitemapController@index');
Route::get('/sitemap/items', 'SitemapController@items')->name('sitemap_items');
Route::get('/sitemap/vendors', 'SitemapController@vendors')->name('sitemap_vendors');
Route::get('/sitemap/categories', 'SitemapController@categories')->name('sitemap_categories');
Route::get('/sitemap/static', 'SitemapController@staticPages')->name('sitemap_static');

// Testing purpose
//Route::get('create_folder', 'TestController@createFolder');
//Route::get('/test/time', 'TestController@time');
//Route::get('/test/item', 'TestController@itemCategory');
//Route::get('/test', 'TestController@test');
//Route::get('121212', 'TestController@froya');
//Route::get('img', 'TestController@img');
//Route::get('ttt', 'TestController@ttt');
//Route::get('mail', 'TestController@mailView');

// Pages
//Route::get('/', 'HomeController@comingSoon');
Route::get('/', 'HomeController@index')->name('home');
//Route::get('/category/{category}', 'HomeController@categoryPage')->name('parent_category_page');
Route::get('/new-arrival', 'HomeController@newArrivalHomePage')->name('new_arrival_home');
//Route::get('/category/sub/{category}', 'HomeController@subCategoryPage')->name('second_parent_category_page')->middleware('buyer');
//Route::get('/catalog/{category}', 'HomeController@catalogPage')->name('catalog_page')->middleware('buyer');
Route::get('/item/details/{item}', 'HomeController@itemDetailsPage')->name('item_details_page')->middleware('buyer');
Route::post('/items/get/sub_category', 'HomeController@getItemsSubCategory')->name('get_items_sub_category');
//Route::get('/vendor/home/{vendor}', 'HomeController@vendorPage')->name('vendor_page');
Route::get('/vendor/category/item/{category}', 'HomeController@vendorCategoryPage')->name('vendor_category_page')->middleware('buyer');
Route::get('/vendor/category/all/{vendor}', 'HomeController@vendorCategoryAllPage')->name('vendor_category_all_page')->middleware('buyer');
Route::post('/vendor/category/get/items', 'HomeController@vendorCategoryGetItems')->name('vendor_category_get_items')->middleware('buyer');
Route::get('/vendor', 'HomeController@allVendorPage')->name('all_vendor_page');
Route::get('/search', 'HomeController@searchPage')->name('search');

// Static Pages
Route::get('aboutUs', 'StaticPagesController@aboutUs')->name('about_us');
Route::get('contactUs', 'StaticPagesController@contactUs')->name('contact_us');
Route::post('contactUs', 'StaticPagesController@contactUsPost')->name('contact_us_post');
Route::get('returnExchange', 'StaticPagesController@returnExchange')->name('return_exchange');
Route::get('showSchedule', 'StaticPagesController@showSchedule')->name('show_schedule');
Route::get('photoshoot', 'StaticPagesController@photoshoot')->name('photoshoot');
Route::get('shippingInformation', 'StaticPagesController@shippingInformation')->name('shipping_information');
Route::get('refundPolicy', 'StaticPagesController@refundPolicy')->name('refund_policy');
Route::get('privacyPolicy', 'StaticPagesController@privacyPolicy')->name('privacy_policy');
Route::get('termsConditions', 'StaticPagesController@termsConditions')->name('terms_conditions');

// New Arrivals
Route::get('/new_arrival/category', 'NewArrivalController@showItems')->name('new_arrival_page')->middleware('buyer');
Route::post('/new_arrival/items', 'NewArrivalController@getNewArrivalItems')->name('get_new_arrival_items');

Route::post('/price', 'HomeController@getItemPrice')->name('get_item_price');

// Message
Route::get('message/file/{name}', 'HomeController@getMessageFile')->name('get_message_file');

Route::prefix('admin')->group(function () {
    Route::get('login', 'Admin\AuthController@login')->name('login_admin');
    Route::post('login/post', 'Admin\AuthController@loginPost')->name('login_admin_post');
    Route::post('logout', 'Admin\AuthController@logout')->name('logout_admin');

    // Dashboard
    Route::get('dashboad', 'Admin\DashboardController@index')->name('admin_dashboard')->middleware('admin');

    // Default Category
    Route::get('category', 'Admin\CategoryController@index')->name('admin_category')->middleware('admin');
    Route::post('category/add', 'Admin\CategoryController@addCategory')->name('admin_category_add')->middleware('admin');
    Route::post('category/delete', 'Admin\CategoryController@deleteCategory')->name('admin_category_delete')->middleware('admin');
    Route::post('category/update', 'Admin\CategoryController@updateCategory')->name('admin_category_update')->middleware('admin');
    Route::post('category/update/parent', 'Admin\CategoryController@updateCategoryParent')->name('admin_category_parent_update')->middleware('admin');
    Route::post('category/update/sort', 'Admin\CategoryController@sortCategory')->name('admin_sort_category')->middleware('admin');

    // Master Color
    Route::get('master_color', 'Admin\MasterColorController@index')->name('admin_master_color')->middleware('admin');
    Route::post('master_color/add/post', 'Admin\MasterColorController@addPost')->name('admin_master_color_add')->middleware('admin');
    Route::post('master_color/delete', 'Admin\MasterColorController@delete')->name('admin_master_color_delete')->middleware('admin');
    Route::post('master_color/update', 'Admin\MasterColorController@update')->name('admin_master_color_update')->middleware('admin');

    // Master Fabric
    Route::get('master_fabric', 'Admin\MasterFabricController@index')->name('admin_master_fabric')->middleware('admin');
    Route::post('master_fabric/add/post', 'Admin\MasterFabricController@addPost')->name('admin_master_fabric_add')->middleware('admin');
    Route::post('master_fabric/delete', 'Admin\MasterFabricController@delete')->name('admin_master_fabric_delete')->middleware('admin');
    Route::post('master_fabric/update', 'Admin\MasterFabricController@update')->name('admin_master_fabric_update')->middleware('admin');

    // Industry
    Route::get('industry', 'Admin\IndustryController@index')->name('admin_industry')->middleware('admin');
    Route::post('industry/add/post', 'Admin\IndustryController@addPost')->name('admin_industry_add')->middleware('admin');
    Route::post('industry/delete', 'Admin\IndustryController@delete')->name('admin_industry_delete')->middleware('admin');
    Route::post('industry/update', 'Admin\IndustryController@update')->name('admin_industry_update')->middleware('admin');

    // Body Size
    Route::get('body_size', 'Admin\BodySizeController@index')->name('admin_body_size')->middleware('admin');
    Route::post('body_size/add/post', 'Admin\BodySizeController@addPost')->name('admin_body_size_add_post')->middleware('admin');
    Route::post('body_size/update/post', 'Admin\BodySizeController@editPost')->name('admin_body_size_edit_post')->middleware('admin');
    Route::post('body_size/delete/post', 'Admin\BodySizeController@delete')->name('admin_body_size_delete')->middleware('admin');

    // Pattern
    Route::get('pattern', 'Admin\PatternController@index')->name('admin_pattern')->middleware('admin');
    Route::post('pattern/add/post', 'Admin\PatternController@addPost')->name('admin_pattern_add_post')->middleware('admin');
    Route::post('pattern/update/post', 'Admin\PatternController@editPost')->name('admin_pattern_edit_post')->middleware('admin');
    Route::post('pattern/delete/post', 'Admin\PatternController@delete')->name('admin_pattern_delete')->middleware('admin');

    // Length
    Route::get('length', 'Admin\LengthController@index')->name('admin_length')->middleware('admin');
    Route::post('length/add/post', 'Admin\LengthController@addPost')->name('admin_length_add_post')->middleware('admin');
    Route::post('length/update/post', 'Admin\LengthController@editPost')->name('admin_length_edit_post')->middleware('admin');
    Route::post('length/delete/post', 'Admin\LengthController@delete')->name('admin_length_delete')->middleware('admin');

    // Style
    Route::get('style', 'Admin\StyleController@index')->name('admin_style')->middleware('admin');
    Route::post('style/add/post', 'Admin\StyleController@addPost')->name('admin_style_add_post')->middleware('admin');
    Route::post('style/update/post', 'Admin\StyleController@editPost')->name('admin_style_edit_post')->middleware('admin');
    Route::post('style/delete/post', 'Admin\StyleController@delete')->name('admin_style_delete')->middleware('admin');

    // Brand
    Route::get('brand/all', 'Admin\BrandController@all')->name('admin_brand_all')->middleware('admin');
    Route::post('brand/change/status', 'Admin\BrandController@changeStatus')->name('admin_brand_change_status')->middleware('admin');
    Route::post('brand/change/verified', 'Admin\BrandController@changeVerified')->name('admin_brand_change_verified')->middleware('admin');
    Route::post('brand/change/live', 'Admin\BrandController@changeLive')->name('admin_brand_change_live')->middleware('admin');
    Route::post('brand/change/mainSlider', 'Admin\BrandController@changeMainSlider')->name('admin_brand_change_main_slider')->middleware('admin');
    Route::post('brand/change/mobileMainSlider', 'Admin\BrandController@changeMobileMainSlider')->name('admin_brand_change_mobile_main_slider')->middleware('admin');
    Route::post('brand/change/featureVendor', 'Admin\BrandController@changeFeatureVendor')->name('admin_brand_change_feature_vendor')->middleware('admin');
    Route::get('brand/add', 'Admin\BrandController@addBrand')->name('admin_create_brand')->middleware('admin');
    Route::post('brand/add/post', 'Admin\BrandController@addBrandPost')->name('admin_create_brand_post')->middleware('admin');
    Route::get('brand/active', 'Admin\BrandController@activeBrand')->name('admin_active_brand')->middleware('admin');
    Route::get('brand/deactivate', 'Admin\BrandController@deactivateBrand')->name('admin_deactivate_brand')->middleware('admin');
    Route::get('brand/pending', 'Admin\BrandController@pendingBrand')->name('admin_pending_brand')->middleware('admin');
    Route::post('brand/change/verified_status', 'Admin\BrandController@changeVerifiedStatus')->name('admin_brand_change_verified_status')->middleware('admin');
    Route::get('brand/edit/{vendor}', 'Admin\BrandController@edit')->name('admin_brand_edit')->middleware('admin');
    Route::post('brand/edit/{vendor}', 'Admin\BrandController@editPost')->name('admin_brand_edit_post')->middleware('admin');
    Route::post('brand/delete', 'Admin\BrandController@delete')->name('admin_brand_delete')->middleware('admin');

    // Color
    Route::get('color/all', 'Admin\ColorController@index')->name('admin_color')->middleware('admin');
    Route::post('color/add/post', 'Admin\ColorController@addPost')->name('admin_color_add_post')->middleware('admin');
    Route::post('color/edit/post', 'Admin\ColorController@editPost')->name('admin_color_edit_post')->middleware('admin');
    Route::post('color/delete', 'Admin\ColorController@delete')->name('admin_color_delete')->middleware('admin');

    // Pack
    Route::get('pack/all', 'Admin\PackController@index')->name('admin_pack')->middleware('admin');
    Route::post('pack/add/post', 'Admin\PackController@addPost')->name('admin_pack_add_post')->middleware('admin');
    Route::post('pack/edit/post', 'Admin\PackController@editPost')->name('admin_pack_edit_post')->middleware('admin');
    Route::post('pack/delete', 'Admin\PackController@delete')->name('admin_pack_delete')->middleware('admin');
    Route::post('pack/change_status', 'Admin\PackController@changeStatus')->name('admin_pack_change_status')->middleware('admin');

    // Fabric
    Route::get('fabric/all', 'Admin\FabricController@index')->name('admin_fabric')->middleware('admin');
    Route::post('fabric/add/post', 'Admin\FabricController@fabricAdd')->name('admin_fabric_add')->middleware('admin');
    Route::post('fabric/update/post', 'Admin\FabricController@fabricUpdate')->name('admin_fabric_update')->middleware('admin');
    Route::post('fabric/delete/post', 'Admin\FabricController@fabricDelete')->name('admin_fabric_delete')->middleware('admin');
    Route::post('fabric/change_status/post', 'Admin\FabricController@fabricChangeStatus')->name('admin_fabric_change_status')->middleware('admin');

    // Customer
    Route::get('customer/all', 'Admin\BuyerController@allBuyer')->name('admin_all_buyer')->middleware('admin');
    Route::post('customer/change/status', 'Admin\BuyerController@changeStatus')->name('admin_buyer_change_status')->middleware('admin');
    Route::post('customer/change/verified', 'Admin\BuyerController@changeVerified')->name('admin_buyer_change_verified')->middleware('admin');
    Route::post('customer/change/block', 'Admin\BuyerController@changeBlock')->name('admin_buyer_change_block')->middleware('admin');
    Route::get('customer/edit/{buyer}', 'Admin\BuyerController@edit')->name('admin_buyer_edit')->middleware('admin');
    Route::post('customer/edit/post/{buyer}', 'Admin\BuyerController@editPost')->name('admin_buyer_edit_post')->middleware('admin');
    Route::post('customer/delete', 'Admin\BuyerController@delete')->name('admin_buyer_delete')->middleware('admin');

    // Courier
    Route::get('courier', 'Admin\CourierController@index')->name('admin_courier')->middleware('admin');
    Route::post('courier/add/post', 'Admin\CourierController@addPost')->name('admin_courier_add')->middleware('admin');
    Route::post('courier/delete', 'Admin\CourierController@delete')->name('admin_courier_delete')->middleware('admin');
    Route::post('courier/update', 'Admin\CourierController@update')->name('admin_courier_update')->middleware('admin');

    // Ship Method
    Route::get('ship_method', 'Admin\ShipMethodController@index')->name('admin_ship_method')->middleware('admin');
    Route::post('ship_method/add/post', 'Admin\ShipMethodController@addPost')->name('admin_ship_method_add')->middleware('admin');
    Route::post('ship_method/delete', 'Admin\ShipMethodController@delete')->name('admin_ship_method_delete')->middleware('admin');
    Route::post('ship_method/update', 'Admin\ShipMethodController@update')->name('admin_ship_method_update')->middleware('admin');

    // Category Banner
    Route::get('category_banner/home/{category}', 'Admin\CategoryBannerController@index')->name('admin_category_banner')->middleware('admin');
    Route::get('category_banner/new_arrival', 'Admin\CategoryBannerController@newArrival')->name('admin_category_banner_new_arrivals')->middleware('admin');
    Route::get('category_banner/vendor_all', 'Admin\CategoryBannerController@vendorAll')->name('admin_category_banner_vendor_all')->middleware('admin');
    Route::post('category_banner/add', 'Admin\CategoryBannerController@add')->name('admin_category_banner_add')->middleware('admin');
    Route::post('category_banner/delete', 'Admin\CategoryBannerController@delete')->name('admin_category_banner_delete')->middleware('admin');

    // Order
    Route::get('orders/all', 'Admin\OrderController@allOrders')->name('admin_all_orders')->middleware('admin');
    Route::get('orders/new', 'Admin\OrderController@allNewOrders')->name('admin_new_orders')->middleware('admin');
    Route::get('orders/vendor/{vendor}', 'Admin\OrderController@vendorOrders')->name('admin_vendor_orders')->middleware('admin');
    Route::get('orders/details/{order}', 'Admin\OrderController@orderDetails')->name('admin_order_details')->middleware('admin');
    Route::post('orders/details/post/{order}', 'Admin\OrderController@orderDetailsPost')->name('admin_order_details_post')->middleware('admin');
    Route::get('orders/incomplete', 'Admin\OrderController@incompleteOrders')->name('admin_incomplete_orders')->middleware('admin');
    Route::get('orders/incomplete/{user}/{vendor}', 'Admin\OrderController@incompleteOrderDetails')->name('admin_incomplete_order_detail')->middleware('admin');

    // Meta
    Route::get('meta/page/{page}', 'Admin\MetaController@page')->name('admin_meta_page')->middleware('admin');
    Route::get('meta/category/{category}', 'Admin\MetaController@category')->name('admin_meta_category')->middleware('admin');
    Route::get('meta/vendor/{vendor}', 'Admin\MetaController@vendor')->name('admin_meta_vendor')->middleware('admin');
    Route::post('meta/save', 'Admin\MetaController@save')->name('admin_meta_save')->middleware('admin');

    // Buyer Home
    Route::get('buyer_home', 'Admin\OtherController@buyerHome')->name('admin_buyer_home')->middleware('admin');
    Route::post('buyer_home/save', 'Admin\OtherController@buyerHomeSave')->name('admin_buyer_home_save')->middleware('admin');

    // Welcome Notification
    Route::get('welcome_notification', 'Admin\OtherController@welcomeNotification')->name('admin_welcome_notification')->middleware('admin');
    Route::post('welcome_notification/save', 'Admin\OtherController@welcomeNotificationSave')->name('admin_welcome_notification_save')->middleware('admin');

    // Openging Soon
    Route::get('opening_soon', 'Admin\OpeningSoonController@index')->name('admin_opening_soon')->middleware('admin');
    Route::post('opening_soon/add/post', 'Admin\OpeningSoonController@addPost')->name('admin_opening_soon_add_post')->middleware('admin');
    Route::post('opening_soon/edit/post', 'Admin\OpeningSoonController@editPost')->name('admin_opening_soon_edit_post')->middleware('admin');
    Route::post('opening_soon/delete', 'Admin\OpeningSoonController@delete')->name('admin_opening_soon_delete')->middleware('admin');

    // Item
    Route::get('item_list', 'Admin\ItemController@itemList')->name('admin_item_list')->middleware('admin');
    Route::get('item/edit/{item}', 'Admin\ItemController@itemEdit')->name('admin_item_edit')->middleware('admin');

    // Message
    Route::get('messages', 'Admin\MessageController@index')->name('admin_view_messages')->middleware('admin');
    Route::get('messages/{id}', 'Admin\MessageController@chatbox')->name('admin_view_chat')->middleware('admin');
    Route::post('messages/add', 'Admin\MessageController@addMessage')->name('admin_add_message')->middleware('admin');
    Route::post('message/add/parent', 'Admin\MessageController@addMessageParent')->name('admin_add_message_parent')->middleware('admin');
});

Route::prefix('vendor')->group(function () {
    Route::get('register', 'Vendor\AuthController@register')->name('vendor_register');
    Route::post('register/post', 'Vendor\AuthController@registerPost')->name('register_vendor_post');
    Route::get('register/complete', 'Vendor\AuthController@registerComplete')->name('register_vendor_complete');

    Route::get('login', 'Vendor\AuthController@login')->name('login_vendor');
    Route::post('login/post', 'Vendor\AuthController@loginPost')->name('login_vendor_post');
    Route::post('logout', 'Vendor\AuthController@logout')->name('logout_vendor');

    Route::get('dashboard', 'Vendor\DashboardController@index')->name('vendor_dashboard')->middleware('vendor_employee');

    // Item Size
    Route::get('item_settings/size', 'Vendor\SizeController@index')->name('vendor_size')->middleware('vendor_employee');
    Route::post('item_settings/size/add/post', 'Vendor\SizeController@addPost')->name('vendor_size_add_post')->middleware('vendor_employee');
    Route::post('item_settings/size/edit/post', 'Vendor\SizeController@editPost')->name('vendor_size_edit_post')->middleware('vendor_employee');
    Route::post('item_settings/size/delete', 'Vendor\SizeController@delete')->name('vendor_size_delete')->middleware('vendor_employee');
    Route::post('item_settings/size/change_status', 'Vendor\SizeController@changeStatus')->name('vendor_size_change_status')->middleware('vendor_employee');
    Route::post('item_settings/size/change_default', 'Vendor\SizeController@changeDefault')->name('vendor_size_change_default')->middleware('vendor_employee');

    // Item Pack
    Route::get('item_settings/pack', 'Vendor\PackController@index')->name('vendor_pack')->middleware('vendor_employee');
    Route::post('item_settings/pack/add/post', 'Vendor\PackController@addPost')->name('vendor_pack_add_post')->middleware('vendor_employee');
    Route::post('item_settings/pack/edit/post', 'Vendor\PackController@editPost')->name('vendor_pack_edit_post')->middleware(['vendor_employee', 'valid_vendor']);
    Route::post('item_settings/pack/delete', 'Vendor\PackController@delete')->name('vendor_pack_delete')->middleware(['vendor_employee', 'valid_vendor']);
    Route::post('item_settings/pack/change_status', 'Vendor\PackController@changeStatus')->name('vendor_pack_change_status')->middleware(['vendor_employee', 'valid_vendor']);
    Route::post('item_settings/pack/change_default', 'Vendor\PackController@changeDefault')->name('vendor_pack_change_default')->middleware(['vendor_employee', 'valid_vendor']);

    // Color
    Route::get('item_settings/color', 'Vendor\ColorController@index')->name('vendor_color')->middleware('vendor_employee');
    Route::post('item_settings/color/add/post', 'Vendor\ColorController@addPost')->name('vendor_color_add_post')->middleware('vendor_employee');
    Route::post('item_settings/color/edit/post', 'Vendor\ColorController@editPost')->name('vendor_color_edit_post')->middleware(['vendor_employee', 'valid_vendor']);
    Route::post('item_settings/color/delete', 'Vendor\ColorController@delete')->name('vendor_color_delete')->middleware(['vendor_employee', 'valid_vendor']);

    // Items settings other
    Route::get('item_settings/others', 'Vendor\ItemSettingsOthersController@index')->name('vendor_item_settings_others')->middleware('vendor_employee');

    // Made In Country
    Route::post('item_settings/made_in_country/add/post', 'Vendor\ItemSettingsOthersController@madeInCountryAdd')->name('vendor_made_in_country_add')->middleware('vendor_employee');
    Route::post('item_settings/made_in_country/update/post', 'Vendor\ItemSettingsOthersController@madeInCountryUpdate')->name('vendor_made_in_country_update')->middleware(['vendor_employee', 'valid_vendor']);
    Route::post('item_settings/made_in_country/delete/post', 'Vendor\ItemSettingsOthersController@madeInCountryDelete')->name('vendor_made_in_country_delete')->middleware(['vendor_employee', 'valid_vendor']);
    Route::post('item_settings/made_in_country/change_status/post', 'Vendor\ItemSettingsOthersController@madeInCountryChangeStatus')->name('vendor_made_in_country_change_status')->middleware(['vendor_employee', 'valid_vendor']);
    Route::post('item_settings/made_in_country/change_default/post', 'Vendor\ItemSettingsOthersController@madeInCountryChangeDefault')->name('vendor_made_in_country_change_default')->middleware(['vendor_employee', 'valid_vendor']);

    // Fabric
    Route::post('item_settings/fabric/add/post', 'Vendor\ItemSettingsOthersController@fabricAdd')->name('vendor_fabric_add')->middleware('vendor_employee');
    Route::post('item_settings/fabric/update/post', 'Vendor\ItemSettingsOthersController@fabricUpdate')->name('vendor_fabric_update')->middleware(['vendor_employee', 'valid_vendor']);
    Route::post('item_settings/fabric/delete/post', 'Vendor\ItemSettingsOthersController@fabricDelete')->name('vendor_fabric_delete')->middleware(['vendor_employee', 'valid_vendor']);
    Route::post('item_settings/fabric/change_status/post', 'Vendor\ItemSettingsOthersController@fabricChangeStatus')->name('vendor_fabric_change_status')->middleware(['vendor_employee', 'valid_vendor']);
    Route::post('item_settings/fabric/change_default/post', 'Vendor\ItemSettingsOthersController@fabricChangeDefault')->name('vendor_fabric_change_default')->middleware(['vendor_employee', 'valid_vendor']);


    // Administration -> Vendor Information
    Route::get('administration/vendor_information', 'Vendor\VendorInformationController@index')->name('vendor_vendor_information')->middleware('vendor_employee');
    Route::post('administration/company_information/post', 'Vendor\VendorInformationController@companyInformationPost')->name('vendor_company_information_post')->middleware('vendor_employee');
    Route::post('administration/size_chart/post', 'Vendor\VendorInformationController@sizeChartPost')->name('vendor_size_chart_post')->middleware('vendor_employee');
    Route::post('administration/order_notice/post', 'Vendor\VendorInformationController@orderNoticePost')->name('vendor_order_notice_post')->middleware('vendor_employee');

    // Account Setting
    Route::get('administration/account_setting', 'Vendor\AccountSettingController@index')->name('vendor_account_setting')->middleware('vendor_employee');
    Route::post('administration/admin_id/post', 'Vendor\AccountSettingController@adminIdPost')->name('vendor_admin_id_post')->middleware('vendor_employee');
    Route::post('administration/manage_account/add/post', 'Vendor\AccountSettingController@addAccountPost')->name('vendor_add_account_post')->middleware('vendor_employee');
    Route::post('administration/manage_account/delete/post', 'Vendor\AccountSettingController@deleteAccountPost')->name('vendor_delete_account_post')->middleware(['vendor_employee', 'valid_vendor']);
    Route::post('administration/manage_account/update/post', 'Vendor\AccountSettingController@updateAccountPost')->name('vendor_update_account_post')->middleware(['vendor_employee', 'valid_vendor']);
    Route::post('administration/manage_account/status_update/post', 'Vendor\AccountSettingController@statusUpdateAccountPost')->name('vendor_status_update_account_post')->middleware(['vendor_employee', 'valid_vendor']);

    Route::post('administration/store_setting/save/post', 'Vendor\AccountSettingController@saveStoreSetting')->name('vendor_save_store_setting_post')->middleware('vendor_employee');
    Route::post('administration/save/settings', 'Vendor\AccountSettingController@saveSetting')->name('vendor_save_setting_post')->middleware('vendor_employee');

    // Category
    Route::get('item_settings/category', 'Vendor\CategoryController@index')->name('vendor_category')->middleware('vendor_employee');
    Route::post('item_settings/category/detail', 'Vendor\CategoryController@categoryDetail')->name('vendor_category_detail')->middleware(['vendor_employee', 'valid_vendor']);
    Route::post('item_settings/category/sort', 'Vendor\CategoryController@sortCategory')->name('vendor_sort_category')->middleware(['vendor_employee', 'valid_vendor']);
    Route::post('item_settings/category/add/post', 'Vendor\CategoryController@addPostCategory')->name('vendor_category_add_post')->middleware('vendor_employee');
    Route::post('item_settings/category/edit/post', 'Vendor\CategoryController@editPostCategory')->name('vendor_category_edit_post')->middleware(['vendor_employee', 'valid_vendor']);
    Route::post('item_settings/category/delete', 'Vendor\CategoryController@deleteCategory')->name('vendor_category_delete')->middleware(['vendor_employee', 'valid_vendor']);

    // Create a new item
    Route::get('create_new_item', 'Vendor\ItemController@createNewItemIndex')->name('vendor_create_new_item')->middleware('vendor_employee');
    Route::post('create_new_item/post', 'Vendor\ItemController@createNewItemPost')->name('vendor_create_new_item_post')->middleware('vendor_employee');
    Route::post('create_new_item/upload/image', 'Vendor\ItemController@uploadImage')->name('vendor_item_upload_image')->middleware('admin_or_vendor_employee');
    Route::post('create_new_item/add/color', 'Vendor\ItemController@addColor')->name('vendor_item_add_color')->middleware('admin_or_vendor_employee');

    // Edit Item
    Route::get('item/edit/{item}', 'Vendor\ItemController@editItem')->name('vendor_edit_item')->middleware(['vendor_employee', 'valid_vendor']);
    Route::post('item/edit/{item}', 'Vendor\ItemController@editItemPost')->name('vendor_edit_item_post')->middleware(['admin_or_vendor_employee', 'valid_vendor']);

    // Clone Item
    Route::get('item/clone/{item}', 'Vendor\ItemController@cloneItem')->name('vendor_clone_item')->middleware(['vendor_employee', 'valid_vendor']);
    Route::post('item/clone/{old_item}', 'Vendor\ItemController@cloneItemPost')->name('vendor_clone_item_post')->middleware('vendor_employee');

    // Item List
    //Route::get('items/category/{category}', 'Vendor\ItemController@itemListByCategory')->name('vendor_item_list_by_category')->middleware(['vendor_employee', 'valid_vendor']);
    Route::get('items/all', 'Vendor\ItemController@itemListAll')->name('vendor_item_list_all')->middleware('vendor_employee');
    Route::post('item_list/change/category', 'Vendor\ItemController@itemsChangeCategory')->name('vendor_item_list_change_category')->middleware(['vendor_employee', 'valid_vendor']);
    Route::post('item_list/change_to_inactive', 'Vendor\ItemController@itemsChangeToInactive')->name('vendor_item_list_change_to_inactive')->middleware(['admin_or_vendor_employee', 'valid_vendor']);
    Route::post('item_list/change_to_active', 'Vendor\ItemController@itemsChangeToActive')->name('vendor_item_list_change_to_active')->middleware(['admin_or_vendor_employee', 'valid_vendor']);
    Route::post('item_list/delete', 'Vendor\ItemController@itemsDelete')->name('vendor_item_list_delete')->middleware(['admin_or_vendor_employee', 'valid_vendor']);
    Route::post('item_list/check_style_no', 'Vendor\ItemController@checkStyleNo')->name('vendor_check_style_no')->middleware('admin_or_vendor_employee');
    Route::get('item/category/{category}', 'Vendor\ItemController@itemListByCategory')->name('vendor_item_list_by_category')->middleware(['vendor_employee', 'valid_vendor']);
    Route::post('item/category/save', 'Vendor\ItemController@itemCategorySave')->name('vendor_item_category_save')->middleware(['vendor_employee', 'valid_vendor']);

    // Banner
    Route::get('banner', 'Vendor\BannerController@index')->name('vendor_banner')->middleware('vendor_employee');
    Route::post('banner/add/post', 'Vendor\BannerController@addPost')->name('vendor_banner_add_post')->middleware('vendor_employee');
    Route::post('banner/delete', 'Vendor\BannerController@delete')->name('vendor_banner_delete')->middleware(['vendor_employee', 'valid_vendor']);
    Route::post('banner/active', 'Vendor\BannerController@active')->name('vendor_banner_active')->middleware(['vendor_employee', 'valid_vendor']);

    // Banner Items
    Route::get('banner/items', 'Vendor\BannerController@bannerItems')->name('vendor_banner_items')->middleware('vendor_employee');
    Route::post('banner/item/add', 'Vendor\BannerController@bannerItemAdd')->name('vendor_banner_item_add')->middleware('vendor_employee');
    Route::post('banner/item/remove', 'Vendor\BannerController@bannerItemRemove')->name('vendor_banner_item_remove')->middleware(['vendor_employee', 'valid_vendor']);
    Route::post('banner/item/sort', 'Vendor\BannerController@bannerItemsSort')->name('vendor_banner_item_sort')->middleware(['vendor_employee', 'valid_vendor']);

    // Orders
    Route::get('orders/all', 'Vendor\OrderController@allOrders')->name('vendor_all_orders')->middleware('vendor_employee');
    Route::get('orders/new', 'Vendor\OrderController@newOrders')->name('vendor_new_orders')->middleware('vendor_employee');
    Route::get('orders/confirmed', 'Vendor\OrderController@confirmOrders')->name('vendor_confirmed_orders')->middleware('vendor_employee');
    Route::get('orders/backed', 'Vendor\OrderController@backedOrders')->name('vendor_backed_orders')->middleware('vendor_employee');
    Route::get('orders/shipped', 'Vendor\OrderController@shippedOrders')->name('vendor_shipped_orders')->middleware('vendor_employee');
    Route::get('orders/cancelled', 'Vendor\OrderController@cancelledOrders')->name('vendor_cancelled_orders')->middleware('vendor_employee');
    Route::get('orders/returned', 'Vendor\OrderController@returnedOrders')->name('vendor_returned_orders')->middleware('vendor_employee');
    Route::get('orders/details/{order}', 'Vendor\OrderController@orderDetails')->name('vendor_order_details')->middleware('vendor_employee');
    Route::post('orders/details/post/{order}', 'Vendor\OrderController@orderDetailsPost')->name('vendor_order_details_post')->middleware(['vendor_employee', 'valid_vendor']);
    Route::get('orders/incomplete', 'Vendor\OrderController@incompleteOrders')->name('vendor_incomplete_orders')->middleware('vendor_employee');
    Route::get('orders/incomplete/{order}', 'Vendor\OrderController@incompleteOrderDetails')->name('vendor_incomplete_order_detail')->middleware('vendor_employee');
    Route::post('orders/backorder/create', 'Vendor\OrderController@createBackorder')->name('vendor_create_back_order')->middleware(['admin_or_vendor_employee', 'valid_vendor']);
    Route::post('orders/out_of_stock', 'Vendor\OrderController@outOfStock')->name('vendor_out_of_stock')->middleware(['admin_or_vendor_employee', 'valid_vendor']);
    Route::post('orders/delete_item', 'Vendor\OrderController@deleteOrderItem')->name('vendor_delete_order_item')->middleware(['admin_or_vendor_employee', 'valid_vendor']);
    Route::post('order/delete', 'Vendor\OrderController@deleteOrder')->name('vendor_delete_order')->middleware(['admin_or_vendor_employee', 'valid_vendor']);
    Route::post('order/item_details', 'Vendor\OrderController@itemDetails')->name('vendor_get_item_details')->middleware(['admin_or_vendor_employee', 'valid_vendor']);
    Route::post('order/add/item', 'Vendor\OrderController@addItem')->name('vendor_order_add_item')->middleware(['admin_or_vendor_employee', 'valid_vendor']);

    Route::get('orders/print/pdf', 'Vendor\OrderController@printPdf')->name('vendor_print_pdf')->middleware(['admin_or_vendor_employee', 'valid_vendor']);
    Route::get('orders/print/pdf/without_image', 'Vendor\OrderController@printPdfWithOutImage')->name('vendor_print_pdf_without_image')->middleware(['admin_or_vendor_employee', 'valid_vendor']);
    Route::get('orders/print/packlist', 'Vendor\OrderController@printPacklist')->name('vendor_print_packlist')->middleware(['admin_or_vendor_employee', 'valid_vendor']);

    Route::post('orders/check_password', 'Vendor\OrderController@checkPassword')->name('vendor_order_check_password')->middleware(['admin_or_vendor_employee', 'valid_vendor']);
    Route::post('orders/mask/card_number', 'Vendor\OrderController@maskCardNumber')->name('vendor_order_mask_card_number')->middleware(['admin_or_vendor_employee', 'valid_vendor']);

    // Shipping Method
    Route::get('administration/shipping_method', 'Vendor\ShippingMethodController@index')->name('vendor_shipping_method')->middleware('vendor_employee');
    Route::post('administration/shipping_method/add/post', 'Vendor\ShippingMethodController@addPost')->name('vendor_shipping_method_add_post')->middleware('vendor_employee');
    Route::post('administration/shipping_method/edit/post', 'Vendor\ShippingMethodController@editPost')->name('vendor_shipping_method_edit_post')->middleware(['vendor_employee', 'valid_vendor']);
    Route::post('administration/shipping_method/delete', 'Vendor\ShippingMethodController@delete')->name('vendor_shipping_method_delete')->middleware(['vendor_employee', 'valid_vendor']);
    Route::post('administration/shipping_method/change_status', 'Vendor\ShippingMethodController@changeStatus')->name('vendor_shipping_method_change_status')->middleware(['vendor_employee', 'valid_vendor']);
    Route::post('administration/shipping_method/change_default', 'Vendor\ShippingMethodController@changeDefault')->name('vendor_shipping_method_change_default')->middleware(['vendor_employee', 'valid_vendor']);

    // Customers
    Route::get('customer/all', 'Vendor\CustomerController@allCustomer')->name('vendor_all_customer')->middleware('vendor_employee');
    Route::post('customer/block/add', 'Vendor\CustomerController@blockCustomer')->name('vendor_block_customer')->middleware('vendor_employee');
    /*Route::get('customer/block', 'Vendor\CustomerController@blockCustomer')->name('vendor_block_customers')->middleware('vendor_employee');
    Route::post('customer/block/add', 'Vendor\CustomerController@addBlockCustomer')->name('vendor_block_customer_add')->middleware('vendor_employee');
    Route::post('customer/block/delete', 'Vendor\CustomerController@deleteBlockCustomer')->name('vendor_block_customer_delete')->middleware('vendor_employee');*/


    // Item Upload
    Route::get('items/import', 'Vendor\ItemController@dataImportView')->name('vendor_data_import')->middleware('vendor_employee');
    Route::post('items/import/read_file', 'Vendor\ItemController@dataImportReadFile')->name('vendor_data_import_read_file');
    Route::post('items/import/upload', 'Vendor\ItemController@dataImportUpload')->name('vendor_data_import_upload')->middleware('vendor_employee');
    Route::post('items/import/images', 'Vendor\ItemController@dataImportImage')->name('vendor_data_import_image')->middleware('vendor_employee');

    // Feedback
    Route::get('feedback', 'Vendor\ReviewController@index')->name('vendor_feedback')->middleware('vendor_employee');
    Route::post('feedback/save', 'Vendor\ReviewController@saveFeedback')->name('vendor_save_feedback')->middleware('vendor_employee');

    // Coupon
    Route::get('coupon', 'Vendor\CouponController@index')->name('vendor_coupon')->middleware('vendor_employee');
    Route::post('coupon/add/post', 'Vendor\CouponController@addPost')->name('vendor_coupon_add_post')->middleware('vendor_employee');
    Route::post('coupon/edit/post', 'Vendor\CouponController@editPost')->name('vendor_coupon_edit_post')->middleware('vendor_employee');
    Route::post('coupon/delete', 'Vendor\CouponController@delete')->name('vendor_coupon_delete')->middleware('vendor_employee');

    // Store Credit
    Route::post('/store_credit/add', 'Vendor\StoreCreditController@add')->name('vendor_add_store_credit')->middleware('admin_or_vendor_employee');
    Route::get('/store_credit', 'Vendor\StoreCreditController@view')->name('vendor_store_credit_view')->middleware('vendor_employee');

    // Sort Items
    Route::get('sort/items', 'Vendor\SortController@index')->name('vendor_sort_items_view')->middleware('vendor_employee');
    Route::post('sort/items/save', 'Vendor\SortController@save')->name('vendor_sort_items_save')->middleware('vendor_employee');

    // Message
    Route::get('messages', 'Vendor\MessageController@index')->name('vendor_view_messages')->middleware('vendor_employee');
    Route::get('messages/{id}', 'Vendor\MessageController@chatbox')->name('vendor_view_chat')->middleware('vendor_employee');
    Route::post('messages/add', 'Vendor\MessageController@addMessage')->name('vendor_add_message')->middleware('vendor_employee');
    Route::post('message/add/parent', 'Vendor\MessageController@addMessageParent')->name('vendor_add_message_parent')->middleware('vendor_employee');

    // Best Items
    Route::get('item/best', 'Vendor\BestItemController@index')->name('vendor_best_items')->middleware('vendor_employee');
    Route::post('item/best/add', 'Vendor\BestItemController@add')->name('vendor_best_item_add')->middleware('vendor_employee');
    Route::post('item/best/sort', 'Vendor\BestItemController@sort')->name('vendor_best_item_sort')->middleware('vendor_employee');
    Route::post('item/best/delete', 'Vendor\BestItemController@delete')->name('vendor_best_item_delete')->middleware('vendor_employee');

    // Others
    Route::post('modal/items', 'Vendor\OtherController@getItems')->name('vendor_get_items_for_modal')->middleware('vendor_employee');
});

Route::prefix('buyer')->group(function () {
    // Auth
    Route::get('register', 'Buyer\AuthController@register')->name('buyer_register');
    Route::post('register/post', 'Buyer\AuthController@registerPost')->name('buyer_register_post');
    Route::get('register/complete', 'Buyer\AuthController@registerComplete')->name('buyer_register_complete');
    Route::get('login', 'Buyer\AuthController@login')->name('buyer_login');
    Route::post('login/post', 'Buyer\AuthController@loginPost')->name('buyer_login_post');
    Route::get('logout', 'Buyer\AuthController@logout')->name('logout_buyer');

    Route::get('reset', 'Buyer\AuthController@resetPassword')->name('password_reset_buyer');
    Route::post('reset/post', 'Buyer\AuthController@resetPasswordPost')->name('password_reset__buyer_post');
    Route::get('reset/new', 'Buyer\AuthController@newPassword')->name('new_password_buyer');
    Route::post('reset/new/post', 'Buyer\AuthController@newPasswordPost')->name('new_password_post_buyer');

    // Cart
    Route::get('cart', 'Buyer\CartController@showCart')->name('show_cart')->middleware('buyer');
    Route::post('cart/add', 'Buyer\CartController@addToCart')->name('add_to_cart')->middleware('buyer');
    Route::get('cart/add/success', 'Buyer\CartController@addToCartSuccess')->name('add_to_cart_success')->middleware('buyer');
    Route::post('cart/update', 'Buyer\CartController@updateCart')->name('update_cart')->middleware('buyer');
    Route::get('cart/update/success', 'Buyer\CartController@updateCartSuccess')->name('update_cart_success')->middleware('buyer');
    Route::post('cart/delete', 'Buyer\CartController@deleteCart')->name('delete_cart')->middleware('buyer');

    // Checkout
    Route::get('checkout', 'Buyer\CheckoutController@index')->name('show_checkout')->middleware('buyer');
    Route::post('checkout/address/post', 'Buyer\CheckoutController@addressPost')->name('checkout_address_post')->middleware('buyer');
    Route::post('checkout/address/select', 'Buyer\CheckoutController@addressSelect')->name('checkout_address_select')->middleware('buyer');
    //Route::get('checkout/shipping', 'Buyer\CheckoutController@shipping')->name('show_shipping_method')->middleware('buyer');
    //Route::post('checkout/shipping/post', 'Buyer\CheckoutController@shippingPost')->name('show_shipping_method_post')->middleware('buyer');
    //Route::get('checkout/payment', 'Buyer\CheckoutController@payment')->name('show_payment')->middleware('buyer');
    //Route::post('checkout/payment/post', 'Buyer\CheckoutController@paymentPost')->name('checkout_payment_post')->middleware('buyer');
    Route::post('checkout/create', 'Buyer\CheckoutController@create')->name('create_checkout')->middleware('buyer');
    //Route::get('checkout/review', 'Buyer\CheckoutController@review')->name('show_checkout_review')->middleware('buyer');
    Route::post('checkout/apply_coupon', 'Buyer\CheckoutController@applyCoupon')->name('buyer_apply_coupon')->middleware('buyer');
    Route::get('checkout/complete', 'Buyer\CheckoutController@completeView')->name('checkout_complete_view')->middleware('buyer');
    //Route::post('checkout/complete', 'Buyer\CheckoutController@complete')->name('checkout_complete')->middleware('buyer');
    Route::post('checkout/post', 'Buyer\CheckoutController@singleCheckoutPost')->name('buyer_single_checkout_post')->middleware('buyer');
    Route::post('checkout/add/card', 'Buyer\OtherController@addCreditCard')->name('buyer_single_checkout_add_card_post')->middleware('buyer');

    // Profile
    Route::get('profile', 'Buyer\ProfileController@index')->name('buyer_show_profile')->middleware('buyer');
    Route::post('profile/update', 'Buyer\ProfileController@updateProfile')->name('buyer_update_profile')->middleware('buyer');
    Route::get('profile/orders', 'Buyer\ProfileController@orders')->name('buyer_show_orders')->middleware('buyer');
    Route::get('profile/address', 'Buyer\ProfileController@address')->name('buyer_show_address')->middleware('buyer');
    Route::post('profile/address', 'Buyer\ProfileController@addressPost')->name('buyer_update_address')->middleware('buyer');
    Route::post('profile/add/shipping_address', 'Buyer\ProfileController@addShippingAddress')->name('buyer_add_shipping_address')->middleware('buyer');
    Route::post('profile/edit/shipping_address', 'Buyer\ProfileController@editShippingAddress')->name('buyer_edit_shipping_address')->middleware('buyer');
    Route::post('profile/change/default_shipping_address', 'Buyer\ProfileController@defaultShippingAddress')->name('buyer_default_shipping_address')->middleware('buyer');
    Route::post('profile/delete/shipping_address', 'Buyer\ProfileController@deleteShippingAddress')->name('buyer_delete_shipping_address')->middleware('buyer');
    Route::get('profile/feedback', 'Buyer\ProfileController@feedback')->name('buyer_show_feedback')->middleware('buyer');
    Route::post('profile/feedback/post', 'Buyer\ProfileController@feedbackPost')->name('buyer_feedback_post')->middleware('buyer');
    Route::get('profile/overview', 'Buyer\ProfileController@overview')->name('buyer_show_overview')->middleware('buyer');

    // Credit Card
    Route::get('credit_card', 'Buyer\ProfileController@creditCard')->name('buyer_show_credit_card')->middleware('buyer');
    Route::get('credit_card/add', 'Buyer\ProfileController@creditCardAdd')->name('buyer_add_credit_card')->middleware('buyer');
    Route::post('credit_card/add/post', 'Buyer\ProfileController@creditCardAddPost')->name('buyer_add_credit_card_post')->middleware('buyer');
    Route::post('credit_card/default', 'Buyer\ProfileController@creditCardDefault')->name('buyer_default_credit_card')->middleware('buyer');
    Route::post('credit_card/delete', 'Buyer\ProfileController@creditCardDelete')->name('buyer_delete_credit_card')->middleware('buyer');
    Route::get('credit_card/edit/{card}', 'Buyer\ProfileController@creditCardEdit')->name('buyer_edit_credit_card')->middleware('buyer');
    Route::post('credit_card/edit/{card}', 'Buyer\ProfileController@creditCardEditPost')->name('buyer_edit_credit_card_post')->middleware('buyer');

    // Order
    Route::get('order/{order}', 'Buyer\OtherController@showOrderDetails')->name('show_order_details')->middleware('buyer');

    // Notification
    Route::get('notification/all', 'Buyer\OtherController@allNotification')->name('view_all_notification')->middleware('buyer');
    Route::get('notification/view', 'Buyer\OtherController@viewNotification')->name('view_notification')->middleware('buyer');

    // Wishlist
    Route::get('wishlist', 'Buyer\WishListController@index')->name('view_wishlist')->middleware('buyer');
    Route::post('wishlist/add', 'Buyer\WishListController@addToWishList')->name('add_to_wishlist')->middleware('buyer');
    Route::post('wishlist/remove', 'Buyer\WishListController@removeWishListItem')->name('remove_from_wishlist')->middleware('buyer');
    Route::post('wishlist/item/details', 'Buyer\WishListController@itemDetails')->name('wishlist_item_details')->middleware('buyer');
    Route::post('wishlist/addToCart', 'Buyer\WishListController@addToCart')->name('wishlist_add_to_cart')->middleware('buyer');

    // Message
    Route::get('message', 'Buyer\MessageController@index')->name('view_messsage')->middleware('buyer');
    Route::post('message/add', 'Buyer\MessageController@add')->name('buyer_add_messsage')->middleware('buyer');
    Route::get('message/{id}', 'Buyer\MessageController@chats')->name('buyer_view_chats')->middleware('buyer');
    Route::post('message/chat/add', 'Buyer\MessageController@addChat')->name('buyer_add_chat')->middleware('buyer');
});

Route::get('/{text}', 'HomeController@checkVendorOrCategory')->name('vendor_or_parent_category');
Route::get('/{parent}/{category}', 'HomeController@secondCategory')->name('second_category')->middleware('buyer');
Route::get('/{parent}/{second}/{category}', 'HomeController@thirdCategory')->name('third_category')->middleware('buyer');


