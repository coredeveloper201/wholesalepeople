<?php
    use App\Model\DefaultCategory;

    // Home
    Breadcrumbs::for('home', function ($trail) {
        $trail->push('Home', route('home'));
    });

    // Vendor All
    Breadcrumbs::for('all_vendor_page', function ($trail) {
        $trail->parent('home');
        $trail->push('Vendors', route('all_vendor_page'));
    });

    // Vendor Home
    Breadcrumbs::for('vendor_page', function ($trail, $vendor) {
        $trail->parent('all_vendor_page');
        $trail->push($vendor->company_name, route('vendor_or_parent_category', changeSpecialChar($vendor->company_name)));
    });

    // Vendor Category
    Breadcrumbs::for('vendor_category_page', function ($trail, $category) {
        $trail->parent('vendor_page', $category->vendor);
        $trail->push($category->name, route('vendor_category_page', $category->id));
    });

    // Vendor Category All
    Breadcrumbs::for('vendor_category_all_page', function ($trail, $vendor) {
        $trail->parent('vendor_page', $vendor);
        $trail->push('All Category', route('vendor_category_all_page', $vendor->id));
    });

    // New Arrival Home
    Breadcrumbs::for('new_arrival_home', function ($trail) {
        $trail->parent('home');
        $trail->push('New Arrivals', route('new_arrival_home'));
    });

    // Parent Category
    Breadcrumbs::for('parent_category_page', function ($trail, $category) {
        $trail->parent('home');
        $trail->push($category->name, route('vendor_or_parent_category', changeSpecialChar($category->name)));
    });

    // Sub Category
    Breadcrumbs::for('second_parent_category_page', function ($trail, $category) {
        $trail->parent('parent_category_page', $category->parentCategory);
        $trail->push($category->name, route('second_category', ['parent' => changeSpecialChar($category->parentCategory->name), 'category' => changeSpecialChar($category->name)]));
    });

    // Catalog
    Breadcrumbs::for('catalog_page', function ($trail, $category) {
        $trail->parent('second_parent_category_page', $category->parentCategory);
        $trail->push($category->name, route('third_category', ['category' => changeSpecialChar($category->name), 'second' => changeSpecialChar($category->parentCategory->name), 'parent' => changeSpecialChar($category->parentCategory->parentCategory->name)]));
    });

    // Item Details
    Breadcrumbs::for('item_details', function ($trail, $item) {
        if ($item->default_third_category != null && $item->default_third_category != '') {
            $category = DefaultCategory::where('id', $item->default_third_category)->first();
            $trail->parent('catalog_page', $category);
        } else if ($item->default_second_category != null && $item->default_second_category != '') {
            $category = DefaultCategory::where('id', $item->default_second_category)->first();
            $trail->parent('second_parent_category_page', $category);
        } else {
            $category = DefaultCategory::where('id', $item->default_parent_category)->first();
            $trail->parent('parent_category_page', $category);
        }

        $trail->push($item->style_no, route('item_details_page', $item->id));
    });


