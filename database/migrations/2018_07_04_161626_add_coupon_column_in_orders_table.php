<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddCouponColumnInOrdersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('orders', function (Blueprint $table) {
            $table->string('coupon')->after('card_cvc')->nullable();
            $table->integer('coupon_type')->after('card_cvc')->nullable();
            $table->float('coupon_amount')->after('card_cvc')->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('orders', function (Blueprint $table) {
            $table->dropColumn('coupon');
            $table->dropColumn('coupon_type');
            $table->dropColumn('coupon_amount');
        });
    }
}
