<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddImagePathColumnInColorsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('colors', function (Blueprint $table) {
            $table->string('thumbs_image_path')->after('vendor_meta_id')->nullable();
            $table->string('image_path')->after('vendor_meta_id')->nullable();
            $table->dropColumn('code');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('colors', function (Blueprint $table) {
            $table->string('code')->after('master_color_id')->nullable();
            $table->dropColumn('thumbs_image_path');
            $table->dropColumn('image_path');
        });
    }
}
