<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class ChangeUserIdToMetaVendorIdInMadeInCountriesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('made_in_countries', function (Blueprint $table) {
            $table->renameColumn('user_id', 'vendor_meta_id');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('made_in_countries', function (Blueprint $table) {
            $table->renameColumn('vendor_meta_id', 'user_id');
        });
    }
}
