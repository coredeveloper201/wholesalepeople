<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddShippingLocationColumnInMetaBuyersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('meta_buyers', function (Blueprint $table) {
            $table->string('shipping_location')->after('website');
            $table->string('billing_location')->after('shipping_commercial');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('meta_buyers', function (Blueprint $table) {
            $table->dropColumn('shipping_location');
            $table->dropColumn('billing_location');
        });
    }
}
