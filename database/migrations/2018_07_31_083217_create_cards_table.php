<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateCardsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('cards', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('user_id');
            $table->boolean('default')->default(0);
            $table->string('card_number', 1000);
            $table->string('mask');
            $table->string('card_name', 1000);
            $table->string('card_expiry');
            $table->string('card_cvc', 1000);
            $table->string('card_type');
            $table->string('billing_address');
            $table->string('billing_city');
            $table->integer('billing_state_id')->nullable();
            $table->string('billing_state')->nullable();
            $table->string('billing_zip')->nullable();
            $table->integer('billing_country_id')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('cards');
    }
}
