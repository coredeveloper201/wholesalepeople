<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateBuyerShippingAddressesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('buyer_shipping_addresses', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('user_id');
            $table->boolean('default')->default(0);
            $table->string('store_no')->nullable();
            $table->string('location');
            $table->string('address');
            $table->string('unit')->nullable();
            $table->string('city');
            $table->integer('state_id')->nullable();
            $table->string('state')->nullable();
            $table->string('zip');
            $table->integer('country_id');
            $table->string('phone');
            $table->string('fax')->nullable();
            $table->boolean('commercial');
            $table->timestamps();
            $table->softDeletes();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('buyer_shipping_addresses');
    }
}
