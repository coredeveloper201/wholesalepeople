<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class RemoveShippingAddressFromMetaBuyersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('meta_buyers', function (Blueprint $table) {
            $table->dropColumn('store_no');
            $table->dropColumn('shipping_location');
            $table->dropColumn('shipping_address');
            $table->dropColumn('shipping_unit');
            $table->dropColumn('shipping_city');
            $table->dropColumn('shipping_state_id');
            $table->dropColumn('shipping_state');
            $table->dropColumn('shipping_zip');
            $table->dropColumn('shipping_country_id');
            $table->dropColumn('shipping_phone');
            $table->dropColumn('shipping_fax');
            $table->dropColumn('shipping_commercial');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('meta_buyers', function (Blueprint $table) {
            $table->string('store_no')->after('attention');
            $table->string('shipping_location')->after('attention');
            $table->string('shipping_address')->after('attention');
            $table->string('shipping_unit')->after('attention')->nullable();
            $table->string('shipping_city')->after('attention');
            $table->integer('shipping_state_id')->after('attention')->nullable();
            $table->string('shipping_state')->after('attention')->nullable();
            $table->string('shipping_zip')->after('attention');
            $table->integer('shipping_country_id')->after('attention');
            $table->string('shipping_phone')->after('attention');
            $table->string('shipping_fax')->after('attention')->nullable();
            $table->boolean('shipping_commercial')->after('attention');
        });
    }
}
