<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateMetaBuyersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('meta_buyers', function (Blueprint $table) {
            $table->increments('id');
            $table->string('company_name');
            $table->integer('primary_customer_market');
            $table->string('seller_permit_number');
            $table->boolean('sell_online')->nullable();
            $table->string('website')->nullable();
            $table->string('shipping_address');
            $table->string('shipping_unit')->nullable();
            $table->string('shipping_city');
            $table->integer('shipping_state_id')->nullable();
            $table->string('shipping_state')->nullable();
            $table->string('shipping_zip');
            $table->integer('shipping_country_id');
            $table->string('shipping_phone');
            $table->string('shipping_fax')->nullable();
            $table->boolean('shipping_commercial');
            $table->string('billing_address');
            $table->string('billing_unit')->nullable();
            $table->string('billing_city');
            $table->integer('billing_state_id')->nullable();
            $table->string('billing_state')->nullable();
            $table->string('billing_zip');
            $table->integer('billing_country_id');
            $table->string('billing_phone');
            $table->string('billing_fax')->nullable();
            $table->boolean('billing_commercial')->nullable();
            $table->string('hear_about_us');
            $table->string('hear_about_us_other')->nullable();
            $table->boolean('receive_offers');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('meta_buyers');
    }
}
