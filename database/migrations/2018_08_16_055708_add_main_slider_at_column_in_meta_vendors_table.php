<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddMainSliderAtColumnInMetaVendorsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('meta_vendors', function (Blueprint $table) {
            $table->timestamp('main_slider_at')->after('show_in_mobile_main_slider')->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('meta_vendors', function (Blueprint $table) {
            $table->dropColumn('main_slider_at');
        });
    }
}
