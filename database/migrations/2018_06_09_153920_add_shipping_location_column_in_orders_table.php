<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddShippingLocationColumnInOrdersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('orders', function (Blueprint $table) {
            $table->string('shipping_location')->after('shipping_method_id')->nullable();
            $table->integer('shipping_state_id')->after('shipping_state')->nullable();
            $table->string('shipping_state_text')->after('shipping_state')->nullable();
            $table->string('billing_location')->after('shipping_phone')->nullable();
            $table->integer('billing_state_id')->after('billing_state')->nullable();
            $table->string('billing_state_text')->after('billing_state')->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('orders', function (Blueprint $table) {
            $table->dropColumn('shipping_location');
            $table->dropColumn('shipping_state_id');
            $table->dropColumn('billing_location');
            $table->dropColumn('billing_state_id');
            $table->dropColumn('shipping_state_text');
            $table->dropColumn('billing_state_text');
        });
    }
}
