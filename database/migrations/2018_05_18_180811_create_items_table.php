<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateItemsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('items', function (Blueprint $table) {
            $table->increments('id');
            $table->boolean('status');
            $table->string('style_no');
            $table->integer('category_id');
            $table->float('price');
            $table->float('orig_price')->nullable();
            $table->integer('pack_id');
            $table->integer('sorting')->nullable();
            $table->string('description', 500);
            $table->date('available_on')->nullable();
            $table->integer('availability')->nulllable();
            $table->string('name')->nullable();
            $table->integer('default_parent_category');
            $table->integer('default_second_category')->nullable();
            $table->integer('default_third_category')->nullable();
            $table->boolean('exclusive')->default(0);
            $table->integer('min_qty')->nullable();
            $table->boolean('even_color')->default(0);
            $table->integer('fabric_id')->nullable();
            $table->integer('made_in_id')->nullable();
            $table->string('labeled')->nullable();
            $table->string('keywords', 200)->nullable();
            $table->integer('body_size_id')->nullable();
            $table->integer('pattern_id')->nullable();
            $table->integer('length_id')->nullable();
            $table->integer('style_id')->nullable();
            $table->integer('master_fabric_id')->nullable();
            $table->string('memo')->nullable();
            $table->string('vendor_style_no')->nullable();
            $table->timestamps();
            $table->softDeletes();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('items');
    }
}
