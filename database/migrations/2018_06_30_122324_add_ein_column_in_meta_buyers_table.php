<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddEinColumnInMetaBuyersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('meta_buyers', function (Blueprint $table) {
            $table->string('ein_path')->after('receive_offers')->nullable();
            $table->string('sales1_path')->after('receive_offers')->nullable();
            $table->string('sales2_path')->after('receive_offers')->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('meta_buyers', function (Blueprint $table) {
            $table->dropColumn('ein_path');
            $table->dropColumn('sales1_path');
            $table->dropColumn('sales2_path');
        });
    }
}
