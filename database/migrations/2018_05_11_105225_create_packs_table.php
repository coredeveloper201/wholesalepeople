<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreatePacksTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('packs', function (Blueprint $table) {
            $table->increments('id');
            $table->string('name');
            $table->boolean('status');
            $table->boolean('default');
            $table->integer('user_id');
            $table->string('pack1');
            $table->string('pack2')->nullable();
            $table->string('pack3')->nullable();
            $table->string('pack4')->nullable();
            $table->string('pack5')->nullable();
            $table->string('pack6')->nullable();
            $table->string('pack7')->nullable();
            $table->string('pack8')->nullable();
            $table->string('pack9')->nullable();
            $table->string('pack10')->nullable();
            $table->timestamps();
            $table->softDeletes();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('packs');
    }
}
