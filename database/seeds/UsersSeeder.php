<?php

use Illuminate\Database\Seeder;
use App\Model\User;
use Illuminate\Support\Facades\Hash;
use App\Enumeration\Role;

class UsersSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        User::create([
            'first_name' => 'Super',
            'last_name' => 'Admin',
            'email' => 'admin@gmail.com',
            'password' => Hash::make('123456'),
            'role' => Role::$ADMIN,
        ]);

        /*User::create([
            'first_name' => 'Super',
            'last_name' => 'Admin',
            'email' => 'info@stylepick.net',
            'password' => Hash::make('enet$$tokyo102$'),
            'role' => Role::$ADMIN,
        ]);

        User::create([
            'first_name' => 'Super',
            'last_name' => 'Admin',
            'email' => 'info@whatef.com',
            'password' => Hash::make('skarldnjs102'),
            'role' => Role::$ADMIN,
        ]);*/
    }
}
