<?php

use Illuminate\Database\Seeder;
use App\Model\State;

class StatesTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('states')->truncate();

        State::create(['country_id' => 1, 'name' => 'Alaska', 'code' => 'AK']);
        State::create(['country_id' => 1, 'name' => 'Alabama', 'code' => 'AL']);
        State::create(['country_id' => 1, 'name' => 'American Samoa', 'code' => 'AS']);
        State::create(['country_id' => 1, 'name' => 'Arizona', 'code' => 'AZ']);
        State::create(['country_id' => 1, 'name' => 'Arkansas', 'code' => 'AR']);
        State::create(['country_id' => 1, 'name' => 'California', 'code' => 'CA']);
        State::create(['country_id' => 1, 'name' => 'Colorado', 'code' => 'CO']);
        State::create(['country_id' => 1, 'name' => 'Connecticut', 'code' => 'CT']);
        State::create(['country_id' => 1, 'name' => 'Delaware', 'code' => 'DE']);
        State::create(['country_id' => 1, 'name' => 'District of Columbia', 'code' => 'DC']);
        State::create(['country_id' => 1, 'name' => 'Federated States of Micronesia', 'code' => 'FM']);
        State::create(['country_id' => 1, 'name' => 'Florida', 'code' => 'FL']);
        State::create(['country_id' => 1, 'name' => 'Georgia', 'code' => 'GA']);
        State::create(['country_id' => 1, 'name' => 'Guam', 'code' => 'GU']);
        State::create(['country_id' => 1, 'name' => 'Hawaii', 'code' => 'HI']);
        State::create(['country_id' => 1, 'name' => 'Idaho', 'code' => 'ID']);
        State::create(['country_id' => 1, 'name' => 'Illinois', 'code' => 'IL']);
        State::create(['country_id' => 1, 'name' => 'Indiana', 'code' => 'IN']);
        State::create(['country_id' => 1, 'name' => 'Iowa', 'code' => 'IA']);
        State::create(['country_id' => 1, 'name' => 'Kansas', 'code' => 'KS']);
        State::create(['country_id' => 1, 'name' => 'Kentucky', 'code' => 'KY']);
        State::create(['country_id' => 1, 'name' => 'Louisiana', 'code' => 'LA']);
        State::create(['country_id' => 1, 'name' => 'Maine', 'code' => 'ME']);
        State::create(['country_id' => 1, 'name' => 'Marshall Islands', 'code' => 'MH']);
        State::create(['country_id' => 1, 'name' => 'Maryland', 'code' => 'MD']);
        State::create(['country_id' => 1, 'name' => 'Massachusetts', 'code' => 'MA']);
        State::create(['country_id' => 1, 'name' => 'Michigan', 'code' => 'MI']);
        State::create(['country_id' => 1, 'name' => 'Minnesota', 'code' => 'MN']);
        State::create(['country_id' => 1, 'name' => 'Mississippi', 'code' => 'MS']);
        State::create(['country_id' => 1, 'name' => 'Missouri', 'code' => 'MO']);
        State::create(['country_id' => 1, 'name' => 'Montana', 'code' => 'MT']);
        State::create(['country_id' => 1, 'name' => 'Nebraska', 'code' => 'NE']);
        State::create(['country_id' => 1, 'name' => 'Nevada', 'code' => 'NV']);
        State::create(['country_id' => 1, 'name' => 'New Hampshire', 'code' => 'NH']);
        State::create(['country_id' => 1, 'name' => 'New Jersey', 'code' => 'NJ']);
        State::create(['country_id' => 1, 'name' => 'New Mexico', 'code' => 'NM']);
        State::create(['country_id' => 1, 'name' => 'New York', 'code' => 'NY']);
        State::create(['country_id' => 1, 'name' => 'North Carolina', 'code' => 'NC']);
        State::create(['country_id' => 1, 'name' => 'North Dakota', 'code' => 'ND']);
        State::create(['country_id' => 1, 'name' => 'Northern Mariana Islands', 'code' => 'MP']);
        State::create(['country_id' => 1, 'name' => 'Ohio', 'code' => 'OH']);
        State::create(['country_id' => 1, 'name' => 'Oklahoma', 'code' => 'OK']);
        State::create(['country_id' => 1, 'name' => 'Oregon', 'code' => 'OR']);
        State::create(['country_id' => 1, 'name' => 'Palau', 'code' => 'PW']);
        State::create(['country_id' => 1, 'name' => 'Pennsylvania', 'code' => 'PA']);
        State::create(['country_id' => 1, 'name' => 'Puerto Rico', 'code' => 'PR']);
        State::create(['country_id' => 1, 'name' => 'Rhode Island', 'code' => 'RI']);
        State::create(['country_id' => 1, 'name' => 'South Carolina', 'code' => 'SC']);
        State::create(['country_id' => 1, 'name' => 'South Dakota', 'code' => 'SD']);
        State::create(['country_id' => 1, 'name' => 'Tennessee', 'code' => 'TN']);
        State::create(['country_id' => 1, 'name' => 'Texas', 'code' => 'TX']);
        State::create(['country_id' => 1, 'name' => 'Utah', 'code' => 'UT']);
        State::create(['country_id' => 1, 'name' => 'Vermont', 'code' => 'VT']);
        State::create(['country_id' => 1, 'name' => 'Virgin Islands', 'code' => 'VI']);
        State::create(['country_id' => 1, 'name' => 'Virginia', 'code' => 'VA']);
        State::create(['country_id' => 1, 'name' => 'Washington', 'code' => 'WA']);
        State::create(['country_id' => 1, 'name' => 'West Virginia', 'code' => 'WV']);
        State::create(['country_id' => 1, 'name' => 'Wisconsin', 'code' => 'WI']);
        State::create(['country_id' => 1, 'name' => 'Wyoming', 'code' => 'WY']);
        State::create(['country_id' => 1, 'name' => 'Armed Forces Africa', 'code' => 'AE']);
        State::create(['country_id' => 1, 'name' => 'Armed Forces Americas (except Canada)', 'code' => 'AA']);
        State::create(['country_id' => 1, 'name' => 'Armed Forces Canada', 'code' => 'AE']);
        State::create(['country_id' => 1, 'name' => 'Armed Forces Europe', 'code' => 'AE']);
        State::create(['country_id' => 1, 'name' => 'Armed Forces Middle East', 'code' => 'AE']);
        State::create(['country_id' => 1, 'name' => 'Armed Forces Pacific', 'code' => 'AP']);

        State::create(['country_id' => 2, 'code' => 'AB', 'name' => 'Alberta']);
        State::create(['country_id' => 2, 'code' => 'BC', 'name' => 'British Columbia']);
        State::create(['country_id' => 2, 'code' => 'MB', 'name' => 'Manitoba']);
        State::create(['country_id' => 2, 'code' => 'NB', 'name' => 'New Brunswick']);
        State::create(['country_id' => 2, 'code' => 'NL', 'name' => 'Newfoundland and Labrador']);
        State::create(['country_id' => 2, 'code' => 'NT', 'name' => 'Northwest Territories']);
        State::create(['country_id' => 2, 'code' => 'NS', 'name' => 'Nova Scotia']);
        State::create(['country_id' => 2, 'code' => 'NU', 'name' => 'Nunavut']);
        State::create(['country_id' => 2, 'code' => 'ON', 'name' => 'Ontario']);
        State::create(['country_id' => 2, 'code' => 'PE', 'name' => 'Prince Edward Island']);
        State::create(['country_id' => 2, 'code' => 'QC', 'name' => 'Quebec']);
        State::create(['country_id' => 2, 'code' => 'SK', 'name' => 'Saskatchewan']);
        State::create(['country_id' => 2, 'code' => 'YT', 'name' => 'Yukon']);
    }
}
