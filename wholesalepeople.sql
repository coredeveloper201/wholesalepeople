-- phpMyAdmin SQL Dump
-- version 4.7.9
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1:3306
-- Generation Time: Jan 11, 2019 at 04:08 PM
-- Server version: 5.7.21
-- PHP Version: 7.2.4

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `wholesalepeople`
--

-- --------------------------------------------------------

--
-- Table structure for table `admin_ship_methods`
--

DROP TABLE IF EXISTS `admin_ship_methods`;
CREATE TABLE IF NOT EXISTS `admin_ship_methods` (
  `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT,
  `name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `courier_id` int(11) NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=7 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `admin_ship_methods`
--

INSERT INTO `admin_ship_methods` (`id`, `name`, `courier_id`, `created_at`, `updated_at`, `deleted_at`) VALUES
(1, 'UPS Ground', 1, '2018-06-01 03:13:50', '2018-06-01 03:13:50', NULL),
(2, 'UPS 23', 1, '2018-06-01 03:15:23', '2018-06-01 03:20:10', NULL),
(3, 'UPS 3', 1, '2018-06-01 03:15:34', '2018-06-01 03:15:34', NULL),
(4, 'erw', 1, '2018-06-01 03:20:58', '2018-06-01 03:21:00', '2018-06-01 03:21:00'),
(5, 'Truck', 3, '2018-06-01 03:38:12', '2018-06-01 03:38:12', NULL),
(6, 'dsf', 3, '2018-07-02 09:33:32', '2018-07-02 11:38:24', '2018-07-02 11:38:24');

-- --------------------------------------------------------

--
-- Table structure for table `best_items`
--

DROP TABLE IF EXISTS `best_items`;
CREATE TABLE IF NOT EXISTS `best_items` (
  `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT,
  `vendor_meta_id` int(11) NOT NULL,
  `item_id` int(11) NOT NULL,
  `sort` int(11) DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=6 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `best_items`
--

INSERT INTO `best_items` (`id`, `vendor_meta_id`, `item_id`, `sort`, `created_at`, `updated_at`) VALUES
(1, 1, 123, 3, '2018-10-17 00:10:08', '2018-11-20 19:57:37'),
(4, 1, 77, 4, '2018-10-17 00:36:38', '2018-11-20 19:57:37'),
(3, 1, 16, 1, '2018-10-17 00:13:43', '2018-11-20 19:57:37'),
(5, 1, 17, 2, '2018-10-25 21:25:10', '2018-11-20 19:57:37');

-- --------------------------------------------------------

--
-- Table structure for table `block_users`
--

DROP TABLE IF EXISTS `block_users`;
CREATE TABLE IF NOT EXISTS `block_users` (
  `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT,
  `user_id` int(11) NOT NULL,
  `vendor_meta_id` int(11) NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=12 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `block_users`
--

INSERT INTO `block_users` (`id`, `user_id`, `vendor_meta_id`, `created_at`, `updated_at`) VALUES
(7, 19, 1, '2018-06-30 05:45:43', '2018-06-30 05:45:43');

-- --------------------------------------------------------

--
-- Table structure for table `body_sizes`
--

DROP TABLE IF EXISTS `body_sizes`;
CREATE TABLE IF NOT EXISTS `body_sizes` (
  `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT,
  `name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `parent_category_id` int(11) NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=9 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `body_sizes`
--

INSERT INTO `body_sizes` (`id`, `name`, `parent_category_id`, `created_at`, `updated_at`, `deleted_at`) VALUES
(1, 'Junior', 1, '2018-05-17 04:13:39', '2018-05-22 14:18:18', NULL),
(2, 'sdf', 1, '2018-05-17 05:00:37', '2018-05-17 05:00:40', '2018-05-17 05:00:40'),
(3, 'Young Contemporary', 1, '2018-05-17 06:39:49', '2018-05-22 14:18:31', NULL),
(4, '222', 0, '2018-05-17 06:42:29', '2018-05-17 06:42:35', '2018-05-17 06:42:35'),
(5, 'Missy', 1, '2018-05-22 14:18:43', '2018-05-22 14:18:43', NULL),
(6, 'Plus Size', 1, '2018-05-22 14:18:51', '2018-05-22 14:18:51', NULL),
(7, 'Maternity', 1, '2018-05-22 14:18:59', '2018-05-22 14:18:59', NULL),
(8, 'tt', 3, '2018-06-22 03:13:35', '2018-06-22 03:36:26', '2018-06-22 03:36:26');

-- --------------------------------------------------------

--
-- Table structure for table `buyer_shipping_addresses`
--

DROP TABLE IF EXISTS `buyer_shipping_addresses`;
CREATE TABLE IF NOT EXISTS `buyer_shipping_addresses` (
  `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT,
  `user_id` int(11) NOT NULL,
  `default` tinyint(1) NOT NULL DEFAULT '0',
  `store_no` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `location` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `address` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `unit` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `city` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `state_id` int(11) DEFAULT NULL,
  `state_text` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `zip` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `country_id` int(11) NOT NULL,
  `phone` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `fax` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `commercial` tinyint(1) NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=17 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `buyer_shipping_addresses`
--

INSERT INTO `buyer_shipping_addresses` (`id`, `user_id`, `default`, `store_no`, `location`, `address`, `unit`, `city`, `state_id`, `state_text`, `zip`, `country_id`, `phone`, `fax`, `commercial`, `created_at`, `updated_at`, `deleted_at`) VALUES
(1, 14, 1, 'fd', 'US', 'Raj', NULL, 'gsdfgfga', 10, NULL, '5656', 1, '4576767', '4545', 0, '2018-06-14 18:00:00', '2018-06-22 10:58:40', NULL),
(2, 19, 1, 'erwer', 'US', '45345', NULL, '45', 4, NULL, '34535', 1, '4545', NULL, 0, '2018-06-22 07:18:27', '2018-06-22 07:18:27', NULL),
(7, 14, 0, 'sadfa', 'US', 'asdfa', NULL, 'asdf', 4, NULL, 'asdf', 1, 'rfer', NULL, 0, '2018-06-22 09:49:57', '2018-06-22 10:58:40', NULL),
(6, 14, 0, 'bad 2', 'US', 'asdf', '2', 'dfadf', 1, NULL, '3er', 1, '3434', NULL, 1, '2018-06-22 09:49:27', '2018-06-22 10:58:40', NULL),
(8, 14, 0, 'sadfa', 'US', 'asdfa', NULL, 'asdf', 4, NULL, 'asdf', 1, 'rfer', NULL, 0, '2018-06-22 09:49:58', '2018-06-22 10:58:40', NULL),
(9, 14, 0, 'asdf', 'US', 'asdf', NULL, 'asdf', 5, NULL, 'sdf', 1, '34', NULL, 0, '2018-06-22 09:50:44', '2018-06-22 10:11:47', '2018-06-22 10:11:47'),
(10, 14, 0, NULL, 'US', 'fdaf', 'd', 'dfads', 4, NULL, 'asdfad', 1, 'asdf', NULL, 0, '2018-06-22 10:58:27', '2018-06-22 10:58:40', NULL),
(11, 14, 0, 'BD', 'INT', 'sdfdfdf', '45', 'rtrtr', NULL, 'erer', 'ter', 20, 'erer', NULL, 0, '2018-06-22 11:31:30', '2018-06-22 11:31:30', NULL),
(13, 14, 0, 'dd', 'US', 'jhaka naka', NULL, 'rer', 3, NULL, 'rer', 1, '3434', NULL, 0, '2018-06-23 10:15:59', '2018-06-23 10:15:59', NULL),
(14, 20, 1, NULL, 'US', 'eqrwer', NULL, 'qewrqew', 8, NULL, 'weqrqwe', 1, '342423', NULL, 0, '2018-06-30 06:30:24', '2018-06-30 06:30:24', NULL),
(15, 22, 1, NULL, 'US', 'adfad', NULL, 'asdf', 11, NULL, 'asfd', 1, 'qwerq', NULL, 0, '2018-07-17 01:10:13', '2018-07-17 01:10:13', NULL),
(16, 14, 0, '343', 'US', 'with unit', '566', 'wer', 1, NULL, '3434', 1, '23423', NULL, 0, '2018-07-27 20:43:06', '2018-07-27 20:43:06', NULL);

-- --------------------------------------------------------

--
-- Table structure for table `cards`
--

DROP TABLE IF EXISTS `cards`;
CREATE TABLE IF NOT EXISTS `cards` (
  `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT,
  `user_id` int(11) NOT NULL,
  `default` tinyint(1) NOT NULL DEFAULT '0',
  `card_number` varchar(1000) COLLATE utf8mb4_unicode_ci NOT NULL,
  `mask` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `card_name` varchar(1000) COLLATE utf8mb4_unicode_ci NOT NULL,
  `card_expiry` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `card_cvc` varchar(1000) COLLATE utf8mb4_unicode_ci NOT NULL,
  `card_type` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `billing_location` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `billing_address` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `billing_city` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `billing_state_id` int(11) DEFAULT NULL,
  `billing_state` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `billing_zip` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `billing_country_id` int(11) DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=11 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `cards`
--

INSERT INTO `cards` (`id`, `user_id`, `default`, `card_number`, `mask`, `card_name`, `card_expiry`, `card_cvc`, `card_type`, `billing_location`, `billing_address`, `billing_city`, `billing_state_id`, `billing_state`, `billing_zip`, `billing_country_id`, `created_at`, `updated_at`) VALUES
(3, 14, 0, 'eyJpdiI6ImNLZzJMaHExdGg0Y3JrTzA2S2NWeEE9PSIsInZhbHVlIjoiaXVIdWJ4aFg4STVPcVVkbTNvdDA2bXZoeVJUcjI1N0JHZmRTUVQ5SGwwWT0iLCJtYWMiOiI5N2UwMTI3ODM2YzAxN2IwZDI5OGU4OGY4NWU3YjIyMDU0NDI3NmE4NmE3NGNlMzc2ZTJlMTk2NjhlMzExYmE1In0=', '***********0007', 'eyJpdiI6IkhEMGhIa0l0YzlTZzVKQ2FzNGRvYUE9PSIsInZhbHVlIjoibHZMY2NvUk1rdFlwMlRTMVVJdDdEQT09IiwibWFjIjoiZDI4YzU1YTZjOGJjN2JlMDdmZWFlNWY0YWIwMTEwZmEyOWFiNTQ1MTQ2M2Q0NmM4ODA3OTM1ZTQxYjBjOTM5OSJ9', '12/19', 'eyJpdiI6Ilp0R1VSMEV1OElFSlFFcHVIYVJFXC9BPT0iLCJ2YWx1ZSI6InV5WnN0VEVrUHhYZGZOcWFaQlJFeXc9PSIsIm1hYyI6ImU3MTlhZmVhYmU0NTAwYzgwMTUyZDE4NzVkNzUwZjUwMzM2ODZkNmFmOTFiYjU5YTE1ZWVlMWZhN2IwNjYwNmEifQ==', 'amex', 'US', 'adfa', 'asdf', 9, NULL, '4334', 1, '2018-07-31 03:11:26', '2018-07-31 22:02:03'),
(4, 14, 0, 'eyJpdiI6ImZVOGJYcm16S2NWR250M1wvNlwvd1Fsdz09IiwidmFsdWUiOiJNNkEwRncxVUpGSUhtSjRBYU5tRVA4WDdNZ1M1WldVZjlUdDF1UkRcL0Q4UT0iLCJtYWMiOiI4NTFiNjZmZjJmNzhhZjVjODQxYTgxOWEwOTNkMmZlYmYzNmJkYWI3NDI1OTJiOTBiODE4M2IzMDEzMGYyM2JlIn0=', '***********0007', 'eyJpdiI6InQ4OXNRZkpHOW9OTUdhZDJFdzRMVXc9PSIsInZhbHVlIjoiQTJrK2V2RkZmZEhvWWFIUHM4bVNwZz09IiwibWFjIjoiNGQ1ZWI2Y2RmODFjZThmZjYzZTI1ZGVkODc3NWJlZGQ5OGY3YjVhNjViNzBjNDdjY2RhYTg3OTA5Y2Q5YWE4OCJ9', '12/19', 'eyJpdiI6InhRM0FwblJ1MlBCRzFtZGR6NDNqUUE9PSIsInZhbHVlIjoiRkxSTElUSDFBTkYybHh0dUx5MDBkZz09IiwibWFjIjoiMzQ3YzVlY2U5OTE2ZDM1NzRhNDc4YzZlNjE0MzU2NmVkNmIyZTI5NjQ1YWU5NTcyYzcyZWUzYjkzOGMwMTIzZSJ9', 'amex', 'US', '3423', '234234', 9, NULL, '234', 1, '2018-07-31 03:12:17', '2018-07-31 22:02:03'),
(5, 14, 1, 'eyJpdiI6Ik5jMngwMkpLM1ZlSUNXc1p5XC93SHNRPT0iLCJ2YWx1ZSI6IlVBZjFhdjROMGhwd3ZROUtHWWd6eXV3VEFoc2dEMEo5WXAxSEk5dkdcL2ZzPSIsIm1hYyI6IjM2N2IzMzYzOGM2NDNmZDhjOTM3MTNiYTY2MTRlYzU1MDk4YzE3ZTk4M2ZhZDk3MTlkNzhkYThiMjVlMWNiNWEifQ==', '***********0007', 'eyJpdiI6InZOekxHUTI4bEJ6emNKbW9rUytXTmc9PSIsInZhbHVlIjoiWjR0TzU1RHJnZmVPd1hqNFh3UmUyUT09IiwibWFjIjoiZjViNjA3MDRmY2Y1Y2JlZWU1ODVkOGVkNmNiYzMxMGNlZmU1ODhhYmE0ZWM5MmRiYjY1YzkxZmUyODU1ODMyYSJ9', '12/19', 'eyJpdiI6InlOcTkwSU9QZWdjSFdCdGRiRVAzXC9RPT0iLCJ2YWx1ZSI6IkZBV3B5Q3craGZqODVnSUNwYnJPUkE9PSIsIm1hYyI6IjA3OTU0M2Y4Y2UyYjgwZGNlM2VlNjZjNmNjNWFiMWVmZWY0MDMxODEwNTA5MWQ3MGVlZTU5MWQ3NmI2NjNkODkifQ==', 'amex', 'CA', '2323', '2323', 67, NULL, '2323', 2, '2018-07-31 21:41:36', '2018-07-31 22:02:03'),
(6, 14, 0, 'eyJpdiI6IlZFMTlzOGpMRzl0d2JIbHFXR1BNSlE9PSIsInZhbHVlIjoielwvTkwrR1dlMGpqVDlzYjJYZUxSMzl3bUVHOUhKbEJrWWF6SHRGZVN4WWc9IiwibWFjIjoiYzk3Y2QyYjhhMTFhNzViMzFhNWQ1MWJkOWMxYjZiYWZhOTE4MTc5YTE0MTUxZmM0MGYyYmU1OTdmZGYxOWMzMyJ9', '***********0007', 'eyJpdiI6Ik1QMm9nRFcxVHpWUmRXWk5zTG56R1E9PSIsInZhbHVlIjoiM2Q3VDdRNm1mY0d5WGRDbzdtU0twdz09IiwibWFjIjoiNWJiMTVjYTE2Y2UwMzc3ZTY2ZDVkM2NhZDZjN2Y1ZjU3MTJmYjU0ZTMyNzUwYmI3NDYxZWUwNGQ1MzIwODQyZCJ9', '12/19', 'eyJpdiI6IkorMENuenVOS3Y3SFBYcHJNQXJZM1E9PSIsInZhbHVlIjoiRUIzSGQ1bTBDd1dYWFhGbGJmZkZuUT09IiwibWFjIjoiZDMyMTk0OTJiZjQ4ZmRiM2VhNjA3MTBlZDg0YzI0ZTQ5ZjZhNjhhNTQ0MzJkYjk2NjJmYWI2MWZlZTkyOWZlNCJ9', 'amex', 'US', '12312', '123123', 3, NULL, '123', 1, '2018-08-01 02:28:23', '2018-08-01 02:28:23'),
(7, 14, 0, 'eyJpdiI6IlBRWGtSYjFXR1JlQnpZWWcrYkphYnc9PSIsInZhbHVlIjoiUFhnYnllYW5hU1RYMWJBUEFxUGRuWmN0VFNSU1dFK1h6b0U2emludmFMRT0iLCJtYWMiOiJkNTZhYThlMDFhOTg3YTlmNGYxMjUxM2RmMzk0OTFiNmY3MGQ2MWQ5YmMwY2M4NGQyYjYxYmYyM2Q5OWZjNjk0In0=', '***********0007', 'eyJpdiI6IlNtQmNpUlpKMzN6RTBGbDBqN29XYVE9PSIsInZhbHVlIjoiYUhsS2xKMUM1QmdUQzRJTFJlb3ljUT09IiwibWFjIjoiYmY2YzZmMmEwMWFkNDQzMDBlNmMyODE5NWE0NzFiYzFlZGZlY2YzNjYyMjc5OTk5NDY2Y2M2MzBiZWZlODZiNiJ9', '12/19', 'eyJpdiI6InZjNThscGhoOVF3T0FsQm85UUxZeEE9PSIsInZhbHVlIjoiYm1aSitSMkxNYUxlY1M5MEQzb0NLZz09IiwibWFjIjoiY2M4M2RmYzM4OTNlMzhjNTY0ZDM4NjYwNjQ2M2MyMTRkZjZjZDNhZjZhYjhmMTQ5NmMyNTc2Zjc4OWViZmZhOCJ9', 'amex', 'US', '12312', '123123', 3, NULL, '123', 1, '2018-08-01 02:28:36', '2018-08-08 21:39:49'),
(9, 14, 0, 'eyJpdiI6IllJWlp6ekZhRExWc291elJjVFpWQmc9PSIsInZhbHVlIjoicE1QWG9EN3M2WDRhbFBDR0ZEaWRQaFwvTlwvbTl6UklUdFNkYkpUbFN5UHNZPSIsIm1hYyI6IjU5MDRlNzkwYTdhOWZiMmNhYTIzOTYwNmZlMGY3N2YzZWQ1ZGI2OGRhMzI2NDYxN2RjNjA2MDEzNjQxZjVjNTMifQ==', '************0016', 'eyJpdiI6IjJ0a2grRVNwdGsrUUZNNlNTb25ST3c9PSIsInZhbHVlIjoidzRzYUhjZXg4QUxaU1c5dTF6dTJhQT09IiwibWFjIjoiZDNiZmJmYmZjODVhZjk4MmQ5ZGRmYWRiZTQxYzg2ODVlODhhOWIwZTBhYzI0OWZmZTgxZGM1Yjk4OWFiZDFiZCJ9', '12/19', 'eyJpdiI6Im5BSHlIbFd3eGhlTThYZ3NxNnkwaFE9PSIsInZhbHVlIjoiU0x4NTN6XC92Q1NcL1NIMVNKejVPWTlnPT0iLCJtYWMiOiI5OTI1YTM0YzIyNmZjYzY5YWViNDAyMzVhY2Q0OTJiODc5NTM4MzYyZDQyY2I4N2E3M2MwNTY3ZDU5ZjJhOWQ5In0=', 'mastercard', 'US', 'sdfa', 'asdf', 8, NULL, '3434', 1, '2018-08-08 21:56:16', '2018-08-08 21:56:16'),
(10, 14, 0, 'eyJpdiI6IjVhYzNrd0JEWkpoU01MWjMwQ3hucFE9PSIsInZhbHVlIjoiTENWbmkzRlA1T1cyUGRMK2RNdHM0dkUzNzlGMkN0dGV6Z1NRaU1BNkRrZz0iLCJtYWMiOiJmMTk0YWY3MmU5ZDYwYTgyYzExNmYxNjVjOGZmNjZlZDNlNTIzOTNlYTZmM2YyZGIyMjE4OGFlNDU3Y2I2YTEyIn0=', '************0003', 'eyJpdiI6Imd0Rzlsa2FMQldSbnkwdkVudktKZ2c9PSIsInZhbHVlIjoibm9sd2RWUTN6VjBHSE9HQUFFWkE0Zz09IiwibWFjIjoiMTAyYWQ2NmRkZDk2MTEwYzM0MDRjNjdiYWQ2MDZkYzk3ZDI4ZDE1NzUwZTNlMjU3ODlmMTM3ZGE4ZDJhZTMzMCJ9', '12/19', 'eyJpdiI6InFJU2s5SDZQVGVtTUhFSWxBZDVJOVE9PSIsInZhbHVlIjoiQW9SSm9RY2RWN3dRcnRBVEt5S29GQT09IiwibWFjIjoiYTNkMzU1MjY4MGQzY2M4MjI0NDJjMjI4YTUzOGU0ZTIzNTgyM2MzZGI1MjQ1ZTAzYjZkODJkOTM5ZDViMmIxZCJ9', 'visa', 'US', 'wer', 'wer', 10, NULL, '2323', 1, '2018-08-08 21:56:44', '2018-08-08 21:56:44');

-- --------------------------------------------------------

--
-- Table structure for table `cart_items`
--

DROP TABLE IF EXISTS `cart_items`;
CREATE TABLE IF NOT EXISTS `cart_items` (
  `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT,
  `user_id` int(11) NOT NULL,
  `vendor_meta_id` int(11) NOT NULL,
  `item_id` int(11) NOT NULL,
  `color_id` int(11) NOT NULL,
  `quantity` int(11) NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=149 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `cart_items`
--

INSERT INTO `cart_items` (`id`, `user_id`, `vendor_meta_id`, `item_id`, `color_id`, `quantity`, `created_at`, `updated_at`) VALUES
(148, 14, 1, 123, 17, 2, '2018-11-23 19:30:52', '2018-11-23 19:30:52'),
(147, 14, 1, 123, 16, 2, '2018-11-23 19:30:52', '2018-11-23 19:30:52'),
(146, 14, 1, 123, 15, 2, '2018-11-23 19:30:52', '2018-11-23 19:30:52');

-- --------------------------------------------------------

--
-- Table structure for table `categories`
--

DROP TABLE IF EXISTS `categories`;
CREATE TABLE IF NOT EXISTS `categories` (
  `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT,
  `name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `parent` int(11) NOT NULL,
  `sort` int(11) NOT NULL,
  `d_category_id` int(11) NOT NULL,
  `d_category_second_id` int(11) DEFAULT NULL,
  `d_category_third_id` int(11) DEFAULT NULL,
  `status` tinyint(1) NOT NULL,
  `vendor_meta_id` int(11) NOT NULL,
  `discount` double(8,2) DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=13 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `categories`
--

INSERT INTO `categories` (`id`, `name`, `parent`, `sort`, `d_category_id`, `d_category_second_id`, `d_category_third_id`, `status`, `vendor_meta_id`, `discount`, `created_at`, `updated_at`, `deleted_at`) VALUES
(1, 'SALES', 0, 1, 3, NULL, NULL, 1, 1, 20.00, '2018-05-16 08:55:07', '2018-11-20 19:47:16', NULL),
(2, 's22', 1, 1, 9, NULL, NULL, 1, 1, NULL, '2018-05-16 08:55:25', '2018-05-16 09:37:13', '2018-05-16 09:37:13'),
(3, 'IN STOCK', 0, 2, 1, 10, 18, 1, 1, NULL, '2018-05-16 09:38:02', '2018-11-20 19:47:16', NULL),
(4, 'ARRIVAL SOON', 0, 3, 1, 10, 18, 1, 1, NULL, '2018-05-22 14:36:47', '2018-11-20 19:47:16', NULL),
(5, 'PRE ORDER', 0, 4, 1, 10, 18, 1, 1, NULL, '2018-05-22 14:37:10', '2018-11-20 19:47:16', NULL),
(6, 'YELLOW INSTOCK', 0, 1, 1, 10, 18, 1, 2, NULL, '2018-05-23 15:20:50', '2018-05-23 15:20:50', NULL),
(7, 'sdf', 0, 6, 1, NULL, NULL, 1, 1, NULL, '2018-06-25 09:03:39', '2018-06-25 09:03:42', '2018-06-25 09:03:42'),
(8, 'PANT', 5, 1, 1, 10, 13, 1, 1, 10.00, '2018-07-10 03:04:53', '2018-11-20 19:47:16', NULL),
(9, '20% Off', 0, 9, 1, 10, 12, 1, 1, NULL, '2018-07-13 19:50:41', '2018-07-13 19:51:58', '2018-07-13 19:51:58'),
(10, '20% OFF', 0, 6, 1, 10, 13, 1, 1, 20.00, '2018-07-13 19:52:15', '2018-11-20 19:47:16', NULL),
(11, 'DEMO', 10, 1, 1, 10, 12, 1, 1, NULL, '2018-07-20 05:07:28', '2018-11-20 19:47:16', NULL),
(12, 'Shrugs & Cardigans', 0, 5, 1, 10, 18, 1, 1, NULL, '2018-08-28 19:25:25', '2018-11-20 19:47:16', NULL);

-- --------------------------------------------------------

--
-- Table structure for table `category_banners`
--

DROP TABLE IF EXISTS `category_banners`;
CREATE TABLE IF NOT EXISTS `category_banners` (
  `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT,
  `type` int(11) NOT NULL,
  `vendor_id` int(11) NOT NULL,
  `category_id` int(11) NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=23 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `category_banners`
--

INSERT INTO `category_banners` (`id`, `type`, `vendor_id`, `category_id`, `created_at`, `updated_at`) VALUES
(1, 1, 1, 1, '2018-06-07 15:19:59', '2018-06-07 15:19:59'),
(2, 2, 2, 1, '2018-06-07 15:25:45', '2018-06-07 15:25:45'),
(3, 3, 1, 1, '2018-06-07 15:25:49', '2018-06-07 15:25:49'),
(6, 3, 2, 1, '2018-06-07 15:30:23', '2018-06-07 15:30:23'),
(8, 2, 1, 1, '2018-06-08 10:28:20', '2018-06-08 10:28:20'),
(7, 1, 2, 1, '2018-06-08 09:09:28', '2018-06-08 09:09:28'),
(10, 1, 1, 0, '2018-06-11 05:49:31', '2018-06-11 05:49:31'),
(11, 1, 2, 0, '2018-06-11 12:52:37', '2018-06-11 12:52:37'),
(12, 2, 1, 0, '2018-06-11 12:52:40', '2018-06-11 12:52:40'),
(13, 2, 2, 0, '2018-06-11 12:52:44', '2018-06-11 12:52:44'),
(14, 3, 1, 0, '2018-06-11 12:52:48', '2018-06-11 12:52:48'),
(15, 3, 2, 0, '2018-06-11 12:52:51', '2018-06-11 12:52:51'),
(18, 4, 1, 0, '2018-07-03 12:08:59', '2018-07-03 12:08:59'),
(22, 4, 1, 1, '2018-07-04 11:11:24', '2018-07-04 11:11:24'),
(21, 4, 2, -1, '2018-07-03 12:32:23', '2018-07-03 12:32:23');

-- --------------------------------------------------------

--
-- Table structure for table `category_item`
--

DROP TABLE IF EXISTS `category_item`;
CREATE TABLE IF NOT EXISTS `category_item` (
  `item_id` int(11) NOT NULL,
  `category_id` int(11) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `category_item`
--

INSERT INTO `category_item` (`item_id`, `category_id`) VALUES
(17, 3),
(16, 3),
(15, 3),
(78, 5),
(18, 6),
(19, 6),
(67, 3),
(69, 4),
(72, 4),
(74, 3),
(75, 3),
(76, 3),
(77, 8),
(78, 4),
(81, 8),
(82, 10),
(83, 10),
(90, 4),
(91, 4),
(91, 8),
(91, 1),
(92, 4),
(92, 8),
(97, 3),
(93, 4),
(94, 4),
(95, 4),
(96, 4),
(97, 3),
(98, 3),
(99, 4),
(100, 1),
(101, 1),
(102, 1),
(103, 1),
(104, 10),
(105, 1),
(106, 10),
(107, 1),
(108, 1),
(109, 1),
(110, 1),
(112, 11),
(111, 12),
(113, 11),
(114, 11),
(14, 4),
(115, 10),
(116, 4),
(117, 11),
(118, 11),
(119, 8),
(120, 4),
(121, 4),
(122, 4),
(123, 3);

-- --------------------------------------------------------

--
-- Table structure for table `colors`
--

DROP TABLE IF EXISTS `colors`;
CREATE TABLE IF NOT EXISTS `colors` (
  `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT,
  `name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `status` tinyint(1) NOT NULL,
  `master_color_id` int(11) DEFAULT NULL,
  `vendor_meta_id` int(11) NOT NULL,
  `image_path` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `thumbs_image_path` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=52 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `colors`
--

INSERT INTO `colors` (`id`, `name`, `status`, `master_color_id`, `vendor_meta_id`, `image_path`, `thumbs_image_path`, `created_at`, `updated_at`, `deleted_at`) VALUES
(1, 'BEIGE', 1, 14, 1, NULL, NULL, '2018-05-11 09:11:15', '2018-06-25 07:55:30', NULL),
(5, 'BLACK MULTI', 1, 8, 1, NULL, NULL, '2018-05-22 14:38:46', '2018-06-10 03:53:16', NULL),
(2, 'tet2', 1, 1, 1, NULL, NULL, '2018-05-12 02:29:35', '2018-05-12 03:15:09', '2018-05-12 03:15:09'),
(3, 'DeepBlue2', 1, 4, 1, NULL, NULL, '2018-05-17 10:36:52', '2018-05-17 10:36:59', '2018-05-17 10:36:59'),
(4, 'BLACK', 1, 8, 1, NULL, NULL, '2018-05-18 10:55:03', '2018-06-10 03:34:39', NULL),
(6, 'BLUE', 1, 9, 1, NULL, NULL, '2018-05-22 14:38:59', '2018-06-10 03:53:20', NULL),
(7, 'BLUE MULTI', 1, 9, 1, NULL, NULL, '2018-05-22 14:39:12', '2018-06-10 03:53:25', NULL),
(8, 'BLUE/YELLOW', 1, 15, 1, NULL, NULL, '2018-05-22 14:39:26', '2018-06-10 03:53:30', NULL),
(9, 'BLUSH', 1, 17, 1, NULL, NULL, '2018-05-22 14:39:36', '2018-05-22 14:39:36', NULL),
(10, 'BRICK', 1, 10, 1, NULL, NULL, '2018-05-22 14:39:47', '2018-05-22 14:39:47', NULL),
(11, 'BROWN', 1, 10, 1, NULL, NULL, '2018-05-22 14:40:01', '2018-06-10 03:53:39', NULL),
(12, 'CAMEL', 1, 15, 1, NULL, NULL, '2018-05-22 14:40:09', '2018-06-10 03:53:43', NULL),
(13, 'CHARCOAL', 1, 8, 1, NULL, NULL, '2018-05-22 14:40:18', '2018-05-22 14:41:13', NULL),
(14, 'CORAL', 1, 16, 1, NULL, NULL, '2018-05-22 14:40:31', '2018-06-10 03:53:51', NULL),
(15, 'GREEN', 1, 13, 1, NULL, NULL, '2018-05-23 04:14:51', '2018-06-10 03:53:56', NULL),
(16, 'RED', 1, 7, 1, NULL, NULL, '2018-05-23 04:14:57', '2018-06-10 03:54:07', NULL),
(17, 'WHITE', 1, 21, 1, NULL, NULL, '2018-05-23 04:15:04', '2018-05-23 04:15:04', NULL),
(18, 'PINK', 1, 17, 1, NULL, NULL, '2018-05-23 04:24:10', '2018-06-10 03:54:02', NULL),
(19, 'JADE', 1, 14, 2, NULL, NULL, '2018-05-23 15:23:45', '2018-05-23 15:23:45', NULL),
(20, 'WHITE', 1, 21, 2, NULL, NULL, '2018-05-23 15:24:04', '2018-05-23 15:24:04', NULL),
(21, 'YELLOW', 1, 22, 2, NULL, NULL, '2018-05-23 15:24:18', '2018-05-23 15:24:18', NULL),
(22, 'PINK', 1, 17, 2, NULL, NULL, '2018-05-23 15:27:43', '2018-05-23 15:27:43', NULL),
(23, 'MINT', 1, 13, 2, NULL, NULL, '2018-05-23 15:28:02', '2018-05-23 15:28:02', NULL),
(24, '122', 1, 9, 1, NULL, NULL, '2018-05-26 09:47:53', '2018-05-26 09:53:27', '2018-05-26 09:53:27'),
(25, '3', 1, 10, 1, NULL, NULL, '2018-05-26 09:52:58', '2018-05-26 09:53:24', '2018-05-26 09:53:24'),
(26, 'test', 1, 9, 1, NULL, NULL, '2018-06-10 03:15:20', '2018-06-22 05:36:56', '2018-06-22 05:36:56'),
(27, 'tttt', 1, 8, 1, '/images/vendors/1/colors/19af7a90-6f29-11e8-8d9e-99bd1237045b.jpg', '/images/vendors/1/colors/thumbs/19af7a90-6f29-11e8-8d9e-99bd1237045b.jpg', '2018-06-13 10:40:13', '2018-06-13 15:46:47', '2018-06-13 15:46:47'),
(28, 'fdf', 1, 8, 1, NULL, NULL, '2018-06-22 05:37:02', '2018-06-22 05:37:10', '2018-06-22 05:37:10'),
(29, 'sdf', 1, 9, 1, '/images/vendors/1/colors/9fb374f0-7610-11e8-9d3a-cf28bc1263a7.jpg', '/images/vendors/1/colors/thumbs/9fb374f0-7610-11e8-9d3a-cf28bc1263a7.jpg', '2018-06-22 05:37:20', '2018-06-22 05:37:24', '2018-06-22 05:37:24'),
(30, 'rt', 1, 8, 1, NULL, NULL, '2018-06-25 07:56:25', '2018-06-25 07:56:28', '2018-06-25 07:56:28'),
(31, 'trt', 1, 8, 1, NULL, NULL, '2018-06-25 10:52:40', '2018-06-25 10:52:43', '2018-06-25 10:52:43'),
(35, 'sadf', 1, 9, 1, NULL, NULL, '2018-06-28 09:40:47', '2018-06-28 09:40:47', NULL),
(36, 'asdf', 1, 20, 1, NULL, NULL, '2018-06-28 09:43:58', '2018-06-28 09:43:58', NULL),
(37, 'bsdf', 1, 19, 1, NULL, NULL, '2018-06-28 09:55:47', '2018-06-28 09:55:47', NULL),
(38, 'v', 1, 20, 1, NULL, NULL, '2018-06-28 10:42:02', '2018-06-28 10:42:02', NULL),
(39, 'fasd', 1, 8, 1, NULL, NULL, '2018-06-28 12:21:52', '2018-06-28 12:21:52', NULL),
(40, 'bl', 1, 13, 1, NULL, NULL, '2018-06-28 13:26:09', '2018-06-28 13:26:09', NULL),
(41, 'asdfa2', 1, 9, 1, '/images/vendors/1/colors/acca9ae0-7bb5-11e8-b355-8d621d2a63d7.jpg', '/images/vendors/1/colors/thumbs/acca9ae0-7bb5-11e8-b355-8d621d2a63d7.jpg', '2018-06-29 10:00:32', '2018-06-29 10:01:49', '2018-06-29 10:01:49'),
(42, '1212', 1, 8, 1, NULL, NULL, '2018-07-03 05:18:20', '2018-07-03 05:18:20', NULL),
(43, 'Tuk', 1, NULL, 1, NULL, NULL, '2018-07-10 08:03:26', '2018-07-10 08:03:26', NULL),
(44, 'newB', 1, NULL, 1, NULL, NULL, '2018-07-17 20:33:07', '2018-07-17 20:33:07', NULL),
(45, 'GREY', 1, NULL, 1, NULL, NULL, '2018-07-19 04:09:47', '2018-07-19 04:09:47', NULL),
(46, 'SILVER', 1, NULL, 1, NULL, NULL, '2018-07-19 04:09:47', '2018-07-19 04:09:47', NULL),
(47, 'GOLD', 1, NULL, 1, NULL, NULL, '2018-07-25 05:07:12', '2018-07-25 05:07:12', NULL),
(48, 'trr', 1, 20, 1, NULL, NULL, '2018-08-17 22:48:01', '2018-08-17 22:48:01', NULL),
(49, 'tte', 1, 19, 1, NULL, NULL, '2018-08-17 22:48:18', '2018-08-17 22:48:18', NULL),
(50, 'JHAKANAKA', 1, NULL, 1, NULL, NULL, '2018-09-14 22:19:40', '2018-09-14 22:19:40', NULL),
(51, 'asdasdf', 1, 21, 1, NULL, NULL, '2018-12-14 20:48:40', '2018-12-14 20:48:40', NULL);

-- --------------------------------------------------------

--
-- Table structure for table `color_item`
--

DROP TABLE IF EXISTS `color_item`;
CREATE TABLE IF NOT EXISTS `color_item` (
  `item_id` int(11) NOT NULL,
  `color_id` int(11) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `color_item`
--

INSERT INTO `color_item` (`item_id`, `color_id`) VALUES
(2, 1),
(2, 4),
(3, 1),
(3, 4),
(4, 1),
(4, 4),
(5, 1),
(5, 4),
(6, 1),
(6, 4),
(7, 1),
(7, 4),
(14, 15),
(14, 16),
(14, 17),
(15, 4),
(15, 17),
(16, 6),
(16, 18),
(16, 17),
(17, 14),
(18, 19),
(18, 20),
(18, 21),
(19, 23),
(19, 22),
(19, 20),
(20, 14),
(21, 14),
(22, 14),
(23, 14),
(24, 14),
(30, 17),
(30, 4),
(29, 17),
(29, 4),
(31, 4),
(31, 17),
(32, 4),
(32, 17),
(33, 4),
(33, 17),
(34, 4),
(34, 17),
(37, 4),
(37, 17),
(38, 4),
(38, 17),
(39, 4),
(39, 17),
(40, 4),
(40, 17),
(41, 4),
(41, 17),
(42, 4),
(42, 17),
(43, 4),
(43, 6),
(44, 4),
(44, 6),
(45, 4),
(45, 6),
(46, 5),
(46, 6),
(47, 5),
(47, 6),
(48, 5),
(48, 6),
(49, 5),
(49, 6),
(50, 5),
(50, 6),
(51, 4),
(51, 17),
(52, 4),
(52, 17),
(53, 4),
(53, 17),
(54, 4),
(54, 17),
(55, 4),
(55, 17),
(56, 4),
(56, 17),
(57, 4),
(57, 17),
(58, 4),
(58, 17),
(59, 4),
(59, 17),
(60, 4),
(60, 17),
(61, 4),
(61, 17),
(62, 4),
(62, 17),
(63, 4),
(63, 17),
(64, 4),
(64, 17),
(65, 4),
(65, 17),
(66, 4),
(67, 4),
(68, 17),
(68, 4),
(69, 4),
(70, 4),
(71, 4),
(72, 17),
(73, 4),
(73, 17),
(74, 4),
(74, 17),
(75, 4),
(75, 17),
(76, 4),
(76, 17),
(77, 1),
(77, 4),
(78, 4),
(79, 4),
(79, 17),
(80, 4),
(80, 17),
(81, 4),
(81, 17),
(81, 43),
(82, 36),
(83, 36),
(84, 17),
(85, 17),
(85, 44),
(86, 17),
(86, 44),
(87, 17),
(87, 44),
(88, 17),
(88, 44),
(89, 44),
(89, 17),
(90, 17),
(90, 44),
(91, 4),
(92, 4),
(93, 4),
(94, 40),
(95, 4),
(96, 4),
(97, 17),
(97, 4),
(98, 17),
(98, 4),
(99, 44),
(99, 17),
(100, 46),
(100, 45),
(101, 4),
(102, 45),
(102, 46),
(103, 4),
(104, 46),
(104, 45),
(105, 4),
(106, 46),
(106, 47),
(107, 17),
(107, 44),
(108, 17),
(108, 44),
(109, 4),
(110, 4),
(77, 49),
(111, 17),
(111, 16),
(111, 15),
(112, 17),
(112, 16),
(112, 15),
(113, 15),
(113, 6),
(114, 15),
(114, 16),
(114, 17),
(115, 15),
(115, 16),
(115, 17),
(116, 15),
(116, 16),
(116, 17),
(117, 15),
(117, 16),
(117, 17),
(117, 50),
(118, 15),
(118, 16),
(118, 17),
(118, 50),
(119, 15),
(119, 16),
(119, 17),
(119, 50),
(120, 50),
(120, 17),
(120, 16),
(120, 15),
(121, 15),
(121, 16),
(121, 17),
(121, 50),
(122, 15),
(122, 16),
(122, 17),
(122, 50),
(123, 50),
(123, 17),
(123, 16),
(123, 15);

-- --------------------------------------------------------

--
-- Table structure for table `countries`
--

DROP TABLE IF EXISTS `countries`;
CREATE TABLE IF NOT EXISTS `countries` (
  `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT,
  `code` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=243 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `countries`
--

INSERT INTO `countries` (`id`, `code`, `name`) VALUES
(1, 'US', 'United States'),
(2, 'CA', 'Canada'),
(3, 'AF', 'Afghanistan'),
(4, 'AL', 'Albania'),
(5, 'DZ', 'Algeria'),
(6, 'AS', 'American Samoa'),
(7, 'AD', 'Andorra'),
(8, 'AO', 'Angola'),
(9, 'AI', 'Anguilla'),
(10, 'AQ', 'Antarctica'),
(11, 'AG', 'Antigua and/or Barbuda'),
(12, 'AR', 'Argentina'),
(13, 'AM', 'Armenia'),
(14, 'AW', 'Aruba'),
(15, 'AU', 'Australia'),
(16, 'AT', 'Austria'),
(17, 'AZ', 'Azerbaijan'),
(18, 'BS', 'Bahamas'),
(19, 'BH', 'Bahrain'),
(20, 'BD', 'Bangladesh'),
(21, 'BB', 'Barbados'),
(22, 'BY', 'Belarus'),
(23, 'BE', 'Belgium'),
(24, 'BZ', 'Belize'),
(25, 'BJ', 'Benin'),
(26, 'BM', 'Bermuda'),
(27, 'BT', 'Bhutan'),
(28, 'BO', 'Bolivia'),
(29, 'BA', 'Bosnia and Herzegovina'),
(30, 'BW', 'Botswana'),
(31, 'BV', 'Bouvet Island'),
(32, 'BR', 'Brazil'),
(33, 'IO', 'British lndian Ocean Territory'),
(34, 'BN', 'Brunei Darussalam'),
(35, 'BG', 'Bulgaria'),
(36, 'BF', 'Burkina Faso'),
(37, 'BI', 'Burundi'),
(38, 'KH', 'Cambodia'),
(39, 'CM', 'Cameroon'),
(40, 'CV', 'Cape Verde'),
(41, 'KY', 'Cayman Islands'),
(42, 'CF', 'Central African Republic'),
(43, 'TD', 'Chad'),
(44, 'CL', 'Chile'),
(45, 'CN', 'China'),
(46, 'CX', 'Christmas Island'),
(47, 'CC', 'Cocos (Keeling) Islands'),
(48, 'CO', 'Colombia'),
(49, 'KM', 'Comoros'),
(50, 'CG', 'Congo'),
(51, 'CK', 'Cook Islands'),
(52, 'CR', 'Costa Rica'),
(53, 'HR', 'Croatia (Hrvatska)'),
(54, 'CU', 'Cuba'),
(55, 'CY', 'Cyprus'),
(56, 'CZ', 'Czech Republic'),
(57, 'CD', 'Democratic Republic of Congo'),
(58, 'DK', 'Denmark'),
(59, 'DJ', 'Djibouti'),
(60, 'DM', 'Dominica'),
(61, 'DO', 'Dominican Republic'),
(62, 'TP', 'East Timor'),
(63, 'EC', 'Ecudaor'),
(64, 'EG', 'Egypt'),
(65, 'SV', 'El Salvador'),
(66, 'GQ', 'Equatorial Guinea'),
(67, 'ER', 'Eritrea'),
(68, 'EE', 'Estonia'),
(69, 'ET', 'Ethiopia'),
(70, 'FK', 'Falkland Islands (Malvinas)'),
(71, 'FO', 'Faroe Islands'),
(72, 'FJ', 'Fiji'),
(73, 'FI', 'Finland'),
(74, 'FR', 'France'),
(75, 'FX', 'France, Metropolitan'),
(76, 'GF', 'French Guiana'),
(77, 'PF', 'French Polynesia'),
(78, 'TF', 'French Southern Territories'),
(79, 'GA', 'Gabon'),
(80, 'GM', 'Gambia'),
(81, 'GE', 'Georgia'),
(82, 'DE', 'Germany'),
(83, 'GH', 'Ghana'),
(84, 'GI', 'Gibraltar'),
(85, 'GR', 'Greece'),
(86, 'GL', 'Greenland'),
(87, 'GD', 'Grenada'),
(88, 'GP', 'Guadeloupe'),
(89, 'GU', 'Guam'),
(90, 'GT', 'Guatemala'),
(91, 'GN', 'Guinea'),
(92, 'GW', 'Guinea-Bissau'),
(93, 'GY', 'Guyana'),
(94, 'HT', 'Haiti'),
(95, 'HM', 'Heard and Mc Donald Islands'),
(96, 'HN', 'Honduras'),
(97, 'HK', 'Hong Kong'),
(98, 'HU', 'Hungary'),
(99, 'IS', 'Iceland'),
(100, 'IN', 'India'),
(101, 'ID', 'Indonesia'),
(102, 'IR', 'Iran (Islamic Republic of)'),
(103, 'IQ', 'Iraq'),
(104, 'IE', 'Ireland'),
(105, 'IL', 'Israel'),
(106, 'IT', 'Italy'),
(107, 'CI', 'Ivory Coast'),
(108, 'JM', 'Jamaica'),
(109, 'JP', 'Japan'),
(110, 'JO', 'Jordan'),
(111, 'KZ', 'Kazakhstan'),
(112, 'KE', 'Kenya'),
(113, 'KI', 'Kiribati'),
(114, 'KP', 'Korea, Democratic People\'s Republic of'),
(115, 'KR', 'Korea, Republic of'),
(116, 'KW', 'Kuwait'),
(117, 'KG', 'Kyrgyzstan'),
(118, 'LA', 'Lao People\'s Democratic Republic'),
(119, 'LV', 'Latvia'),
(120, 'LB', 'Lebanon'),
(121, 'LS', 'Lesotho'),
(122, 'LR', 'Liberia'),
(123, 'LY', 'Libyan Arab Jamahiriya'),
(124, 'LI', 'Liechtenstein'),
(125, 'LT', 'Lithuania'),
(126, 'LU', 'Luxembourg'),
(127, 'MO', 'Macau'),
(128, 'MK', 'Macedonia'),
(129, 'MG', 'Madagascar'),
(130, 'MW', 'Malawi'),
(131, 'MY', 'Malaysia'),
(132, 'MV', 'Maldives'),
(133, 'ML', 'Mali'),
(134, 'MT', 'Malta'),
(135, 'MH', 'Marshall Islands'),
(136, 'MQ', 'Martinique'),
(137, 'MR', 'Mauritania'),
(138, 'MU', 'Mauritius'),
(139, 'TY', 'Mayotte'),
(140, 'MX', 'Mexico'),
(141, 'FM', 'Micronesia, Federated States of'),
(142, 'MD', 'Moldova, Republic of'),
(143, 'MC', 'Monaco'),
(144, 'MN', 'Mongolia'),
(145, 'MS', 'Montserrat'),
(146, 'MA', 'Morocco'),
(147, 'MZ', 'Mozambique'),
(148, 'MM', 'Myanmar'),
(149, 'NA', 'Namibia'),
(150, 'NR', 'Nauru'),
(151, 'NP', 'Nepal'),
(152, 'NL', 'Netherlands'),
(153, 'AN', 'Netherlands Antilles'),
(154, 'NC', 'New Caledonia'),
(155, 'NZ', 'New Zealand'),
(156, 'NI', 'Nicaragua'),
(157, 'NE', 'Niger'),
(158, 'NG', 'Nigeria'),
(159, 'NU', 'Niue'),
(160, 'NF', 'Norfork Island'),
(161, 'MP', 'Northern Mariana Islands'),
(162, 'NO', 'Norway'),
(163, 'OM', 'Oman'),
(164, 'PK', 'Pakistan'),
(165, 'PW', 'Palau'),
(166, 'PA', 'Panama'),
(167, 'PG', 'Papua New Guinea'),
(168, 'PY', 'Paraguay'),
(169, 'PE', 'Peru'),
(170, 'PH', 'Philippines'),
(171, 'PN', 'Pitcairn'),
(172, 'PL', 'Poland'),
(173, 'PT', 'Portugal'),
(174, 'PR', 'Puerto Rico'),
(175, 'QA', 'Qatar'),
(176, 'SS', 'Republic of South Sudan'),
(177, 'RE', 'Reunion'),
(178, 'RO', 'Romania'),
(179, 'RU', 'Russian Federation'),
(180, 'RW', 'Rwanda'),
(181, 'KN', 'Saint Kitts and Nevis'),
(182, 'LC', 'Saint Lucia'),
(183, 'VC', 'Saint Vincent and the Grenadines'),
(184, 'WS', 'Samoa'),
(185, 'SM', 'San Marino'),
(186, 'ST', 'Sao Tome and Principe'),
(187, 'SA', 'Saudi Arabia'),
(188, 'SN', 'Senegal'),
(189, 'RS', 'Serbia'),
(190, 'SC', 'Seychelles'),
(191, 'SL', 'Sierra Leone'),
(192, 'SG', 'Singapore'),
(193, 'SK', 'Slovakia'),
(194, 'SI', 'Slovenia'),
(195, 'SB', 'Solomon Islands'),
(196, 'SO', 'Somalia'),
(197, 'ZA', 'South Africa'),
(198, 'GS', 'South Georgia South Sandwich Islands'),
(199, 'ES', 'Spain'),
(200, 'LK', 'Sri Lanka'),
(201, 'SH', 'St. Helena'),
(202, 'PM', 'St. Pierre and Miquelon'),
(203, 'SD', 'Sudan'),
(204, 'SR', 'Suriname'),
(205, 'SJ', 'Svalbarn and Jan Mayen Islands'),
(206, 'SZ', 'Swaziland'),
(207, 'SE', 'Sweden'),
(208, 'CH', 'Switzerland'),
(209, 'SY', 'Syrian Arab Republic'),
(210, 'TW', 'Taiwan'),
(211, 'TJ', 'Tajikistan'),
(212, 'TZ', 'Tanzania, United Republic of'),
(213, 'TH', 'Thailand'),
(214, 'TG', 'Togo'),
(215, 'TK', 'Tokelau'),
(216, 'TO', 'Tonga'),
(217, 'TT', 'Trinidad and Tobago'),
(218, 'TN', 'Tunisia'),
(219, 'TR', 'Turkey'),
(220, 'TM', 'Turkmenistan'),
(221, 'TC', 'Turks and Caicos Islands'),
(222, 'TV', 'Tuvalu'),
(223, 'UG', 'Uganda'),
(224, 'UA', 'Ukraine'),
(225, 'AE', 'United Arab Emirates'),
(226, 'GB', 'United Kingdom'),
(227, 'UM', 'United States minor outlying islands'),
(228, 'UY', 'Uruguay'),
(229, 'UZ', 'Uzbekistan'),
(230, 'VU', 'Vanuatu'),
(231, 'VA', 'Vatican City State'),
(232, 'VE', 'Venezuela'),
(233, 'VN', 'Vietnam'),
(234, 'VG', 'Virgin Islands (British)'),
(235, 'VI', 'Virgin Islands (U.S.)'),
(236, 'WF', 'Wallis and Futuna Islands'),
(237, 'EH', 'Western Sahara'),
(238, 'YE', 'Yemen'),
(239, 'YU', 'Yugoslavia'),
(240, 'ZR', 'Zaire'),
(241, 'ZM', 'Zambia'),
(242, 'ZW', 'Zimbabwe');

-- --------------------------------------------------------

--
-- Table structure for table `coupons`
--

DROP TABLE IF EXISTS `coupons`;
CREATE TABLE IF NOT EXISTS `coupons` (
  `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT,
  `vendor_meta_id` int(11) NOT NULL,
  `name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `type` int(11) NOT NULL,
  `amount` double DEFAULT NULL,
  `multiple_use` tinyint(1) NOT NULL,
  `description` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=6 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `coupons`
--

INSERT INTO `coupons` (`id`, `vendor_meta_id`, `name`, `type`, `amount`, `multiple_use`, `description`, `created_at`, `updated_at`) VALUES
(1, 1, 'EID', 2, 4, 1, 'wer', '2018-07-04 09:19:53', '2018-07-06 09:50:06'),
(2, 1, 'KURBANI2', 1, 50, 1, '234234', '2018-07-04 09:20:09', '2018-07-06 09:50:10'),
(4, 1, 'FREE', 3, NULL, 1, '234324', '2018-07-05 03:44:40', '2018-07-06 09:50:13');

-- --------------------------------------------------------

--
-- Table structure for table `couriers`
--

DROP TABLE IF EXISTS `couriers`;
CREATE TABLE IF NOT EXISTS `couriers` (
  `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT,
  `name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=4 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `couriers`
--

INSERT INTO `couriers` (`id`, `name`, `created_at`, `updated_at`, `deleted_at`) VALUES
(1, 'UPS', '2018-05-31 13:43:21', '2018-05-31 13:46:34', NULL),
(2, 'tyty', '2018-05-31 13:48:22', '2018-05-31 13:48:25', '2018-05-31 13:48:25'),
(3, 'Others', '2018-06-01 03:38:03', '2018-06-01 03:38:03', NULL);

-- --------------------------------------------------------

--
-- Table structure for table `default_categories`
--

DROP TABLE IF EXISTS `default_categories`;
CREATE TABLE IF NOT EXISTS `default_categories` (
  `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT,
  `name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `parent` int(11) NOT NULL DEFAULT '0',
  `sort` int(11) NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=64 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `default_categories`
--

INSERT INTO `default_categories` (`id`, `name`, `parent`, `sort`, `created_at`, `updated_at`, `deleted_at`) VALUES
(1, 'WOMEN', 0, 1, '2018-05-15 03:22:30', '2018-06-07 13:55:05', NULL),
(2, 'SHOES', 9, 1, '2018-05-15 03:22:39', '2018-05-22 13:35:29', '2018-05-22 13:35:29'),
(3, 'ACCESSORIES', 0, 3, '2018-05-15 03:22:42', '2018-06-07 13:55:05', NULL),
(4, 'Body Jewelry', 43, 2, '2018-05-15 03:22:44', '2018-06-07 13:55:05', NULL),
(5, 'HANDBAGS', 0, 4, '2018-05-15 03:22:47', '2018-06-28 04:12:25', '2018-06-28 04:12:25'),
(6, 'Bracelets', 43, 3, '2018-05-15 03:22:50', '2018-06-07 13:55:05', NULL),
(7, 'Bagpack', 44, 1, '2018-05-15 03:22:52', '2018-06-07 13:55:05', NULL),
(8, 'Belts', 43, 1, '2018-05-15 03:22:54', '2018-06-07 13:55:05', NULL),
(9, 'SHOES', 0, 2, '2018-05-15 03:22:57', '2018-06-07 13:55:05', NULL),
(10, 'TOPS', 1, 1, '2018-05-15 03:59:50', '2018-06-07 13:55:05', NULL),
(11, 'Bucket', 44, 2, '2018-05-15 04:00:29', '2018-06-07 13:55:05', NULL),
(12, 'Body Suits', 10, 1, '2018-05-22 02:39:30', '2018-06-07 13:55:05', NULL),
(13, 'Casual', 10, 2, '2018-05-22 02:39:47', '2018-06-07 13:55:05', NULL),
(14, 'DRESSES', 1, 2, '2018-05-22 02:40:09', '2018-06-07 13:55:05', NULL),
(15, 'Casual', 14, 1, '2018-05-22 02:40:18', '2018-06-07 13:55:05', NULL),
(16, 'Club Wear', 14, 2, '2018-05-22 02:40:26', '2018-06-07 13:55:05', NULL),
(17, 'Dressy Tops', 10, 3, '2018-05-22 13:17:29', '2018-06-07 13:55:05', NULL),
(18, 'Fashion Tops', 10, 4, '2018-05-22 13:17:42', '2018-06-07 13:55:05', NULL),
(19, 'Graphic Tees', 10, 5, '2018-05-22 13:17:58', '2018-06-07 13:55:05', NULL),
(20, 'Seamless', 10, 6, '2018-05-22 13:18:10', '2018-06-07 13:55:05', NULL),
(21, 'Shirt & Blouse', 10, 7, '2018-05-22 13:18:25', '2018-06-07 13:55:05', NULL),
(22, 'Tank & Tube Tops', 10, 8, '2018-05-22 13:19:07', '2018-06-07 13:55:05', NULL),
(23, 'T-Shirt & Polo', 10, 9, '2018-05-22 13:19:28', '2018-06-07 13:55:05', NULL),
(24, 'Tunics', 10, 10, '2018-05-22 13:19:40', '2018-06-07 13:55:05', NULL),
(25, 'Cocktail', 14, 3, '2018-05-22 13:20:17', '2018-06-07 13:55:05', NULL),
(26, 'Fashion Dresses', 14, 4, '2018-05-22 13:20:30', '2018-06-07 13:55:05', NULL),
(27, 'Maxi Dresses', 14, 5, '2018-05-22 13:20:42', '2018-06-07 13:55:05', NULL),
(28, 'Semiformal', 14, 6, '2018-05-22 13:21:00', '2018-06-07 13:55:05', NULL),
(29, 'JACKETS / OUTWEAR', 1, 3, '2018-05-22 13:30:21', '2018-06-07 13:55:05', NULL),
(30, 'Cape & Poncho', 29, 1, '2018-05-22 13:30:47', '2018-06-07 13:55:05', NULL),
(31, 'Coats', 29, 2, '2018-05-22 13:31:00', '2018-06-07 13:55:05', NULL),
(32, 'Hoodies', 29, 3, '2018-05-22 13:31:10', '2018-06-07 13:55:05', NULL),
(33, 'Jackets & Blazer', 29, 4, '2018-05-22 13:31:29', '2018-06-07 13:55:05', NULL),
(34, 'Shrugs & Cardigans', 29, 5, '2018-05-22 13:31:46', '2018-06-07 13:55:05', NULL),
(35, 'Sweaters', 29, 6, '2018-05-22 13:32:00', '2018-06-07 13:55:05', NULL),
(36, 'Vests', 29, 7, '2018-05-22 13:32:17', '2018-06-07 13:55:05', NULL),
(37, 'Booties', 42, 1, '2018-05-22 13:33:58', '2018-06-07 13:55:05', NULL),
(38, 'Boots', 42, 2, '2018-05-22 13:34:11', '2018-06-07 13:55:05', NULL),
(39, 'Dress Shoes', 42, 3, '2018-05-22 13:34:25', '2018-06-07 13:55:05', NULL),
(40, 'Flats', 42, 4, '2018-05-22 13:34:40', '2018-06-07 13:55:05', NULL),
(41, 'Crossbody Bags', 44, 3, '2018-05-22 13:39:00', '2018-06-07 13:55:05', NULL),
(42, 'SHOES', 9, 1, '2018-05-22 13:45:19', '2018-06-07 13:55:05', NULL),
(43, 'ACCESSORIES', 3, 1, '2018-05-22 13:45:58', '2018-06-07 13:55:05', NULL),
(44, 'HANDBAGS', 5, 1, '2018-05-22 13:46:15', '2018-06-07 13:55:05', NULL),
(45, 'MEN', 0, 5, '2018-05-22 13:46:33', '2018-06-07 13:55:05', NULL),
(46, 'MEN', 45, 1, '2018-05-22 13:46:53', '2018-06-07 13:55:05', NULL),
(47, 'Casual Shirts', 46, 1, '2018-05-22 13:47:15', '2018-06-07 13:55:05', NULL),
(48, 'Denim', 46, 2, '2018-05-22 13:47:36', '2018-06-07 13:55:05', NULL),
(49, 'Dress Shirts', 46, 3, '2018-05-22 13:47:50', '2018-06-07 13:55:05', NULL),
(50, 'Graphic', 46, 4, '2018-05-22 13:48:04', '2018-06-07 13:55:05', NULL),
(51, 'KIDS', 0, 6, '2018-05-22 13:48:21', '2018-06-07 13:55:05', NULL),
(52, 'KIDS', 51, 1, '2018-05-22 13:48:26', '2018-06-07 13:55:05', NULL),
(53, 'Boys', 52, 1, '2018-05-22 13:48:56', '2018-06-07 13:55:05', NULL),
(54, 'Girls', 52, 2, '2018-05-22 13:49:03', '2018-06-07 13:55:05', NULL),
(55, 'Infants', 52, 3, '2018-05-22 13:49:21', '2018-06-07 13:55:05', NULL),
(56, 'MORE', 0, 7, '2018-05-22 13:50:00', '2018-06-07 13:55:05', NULL),
(57, 'MORE', 56, 1, '2018-05-22 13:50:04', '2018-06-07 13:55:05', NULL),
(58, 'Cosmetics', 57, 1, '2018-05-22 13:50:22', '2018-06-07 13:55:05', NULL),
(59, 'Fixtures/Display', 57, 2, '2018-05-22 13:50:33', '2018-06-07 13:55:05', NULL),
(60, 'Key Chains', 57, 3, '2018-05-22 13:50:43', '2018-06-07 13:55:05', NULL),
(61, 'Jewelry', 0, 8, '2018-07-19 03:57:58', '2018-07-19 03:57:58', NULL),
(62, 'Necklaces', 61, 1, '2018-07-19 03:58:23', '2018-07-19 03:58:23', NULL),
(63, 'Made In Korea', 62, 1, '2018-07-19 03:58:41', '2018-07-19 03:58:41', NULL);

-- --------------------------------------------------------

--
-- Table structure for table `fabrics`
--

DROP TABLE IF EXISTS `fabrics`;
CREATE TABLE IF NOT EXISTS `fabrics` (
  `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT,
  `name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `master_fabric_id` int(11) NOT NULL,
  `status` tinyint(1) NOT NULL,
  `default` tinyint(1) NOT NULL,
  `vendor_meta_id` int(11) NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=16 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `fabrics`
--

INSERT INTO `fabrics` (`id`, `name`, `master_fabric_id`, `status`, `default`, `vendor_meta_id`, `created_at`, `updated_at`, `deleted_at`) VALUES
(9, '100% COTTON', 5, 1, 0, 1, '2018-05-12 11:02:30', '2018-06-25 08:07:31', NULL),
(10, 'er', 2, 1, 0, 1, '2018-05-12 11:02:54', '2018-05-12 11:02:57', '2018-05-12 11:02:57'),
(8, '100% POLYAMIDE', 37, 1, 0, 1, '2018-05-12 09:49:52', '2018-06-25 08:07:31', NULL),
(7, '53% COTTON 47% ACETATE', 5, 1, 0, 1, '2018-05-12 09:49:28', '2018-06-25 08:07:31', NULL),
(6, '100% ACRYLIC', 43, 1, 1, 1, '2018-05-12 09:48:02', '2018-06-25 08:07:31', NULL),
(11, 'dd2', 3, 1, 0, 1, '2018-05-17 01:25:56', '2018-05-17 01:26:07', '2018-05-17 01:26:07'),
(12, '59% POLYESTER 35% POLYAMIDE 6% SPANDEX', 28, 1, 0, 2, '2018-05-23 15:22:05', '2018-05-23 15:22:05', NULL),
(13, '60% COTTON 37% POLYAMIDE 3% SPANDEX', 3, 1, 0, 2, '2018-05-23 15:22:16', '2018-05-23 15:22:16', NULL),
(14, '64% COTTON 36% POLYESTER', 5, 1, 0, 2, '2018-05-23 15:22:25', '2018-05-23 15:22:25', NULL),
(15, '122', 3, 1, 1, 6, '2018-05-26 11:42:54', '2018-05-26 12:03:45', '2018-05-26 12:03:45');

-- --------------------------------------------------------

--
-- Table structure for table `industries`
--

DROP TABLE IF EXISTS `industries`;
CREATE TABLE IF NOT EXISTS `industries` (
  `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT,
  `name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=12 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `industries`
--

INSERT INTO `industries` (`id`, `name`, `created_at`, `updated_at`, `deleted_at`) VALUES
(1, 'erd', '2018-05-14 04:26:39', '2018-05-14 04:28:21', '2018-05-14 04:28:21'),
(2, 'df', '2018-05-14 04:27:04', '2018-05-14 05:14:22', '2018-05-14 05:14:22'),
(3, 'Women\'s Clothing', '2018-05-14 05:14:32', '2018-05-14 05:14:32', NULL),
(4, 'Men\'s Clothing', '2018-05-14 05:14:44', '2018-05-14 05:14:44', NULL),
(5, 'Children\'s Clothings', '2018-05-14 05:14:59', '2018-05-14 05:14:59', NULL),
(6, 'Accessories', '2018-05-14 05:15:09', '2018-05-14 05:15:09', NULL),
(7, 'Shoes', '2018-05-14 05:15:19', '2018-05-14 05:15:19', NULL),
(8, 'Fixure', '2018-05-14 05:15:28', '2018-05-14 05:15:28', NULL),
(9, 'Handbag', '2018-05-14 05:15:38', '2018-05-14 05:15:38', NULL),
(10, 'Cosmetic', '2018-05-14 05:15:47', '2018-05-14 05:15:47', NULL),
(11, 'Lingerie', '2018-05-14 05:15:58', '2018-05-14 05:15:58', NULL);

-- --------------------------------------------------------

--
-- Table structure for table `industry_user`
--

DROP TABLE IF EXISTS `industry_user`;
CREATE TABLE IF NOT EXISTS `industry_user` (
  `user_id` int(11) NOT NULL,
  `industry_id` int(11) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `industry_user`
--

INSERT INTO `industry_user` (`user_id`, `industry_id`) VALUES
(1, 3),
(1, 5),
(1, 10);

-- --------------------------------------------------------

--
-- Table structure for table `items`
--

DROP TABLE IF EXISTS `items`;
CREATE TABLE IF NOT EXISTS `items` (
  `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT,
  `vendor_meta_id` int(11) NOT NULL,
  `status` tinyint(1) NOT NULL,
  `style_no` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `price` double(8,2) NOT NULL,
  `orig_price` double(8,2) DEFAULT NULL,
  `pack_id` int(11) NOT NULL,
  `sorting` int(11) DEFAULT NULL,
  `description` varchar(500) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `available_on` date DEFAULT NULL,
  `availability` int(11) DEFAULT NULL,
  `name` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `default_parent_category` int(11) NOT NULL,
  `default_second_category` int(11) DEFAULT NULL,
  `default_third_category` int(11) DEFAULT NULL,
  `exclusive` tinyint(1) NOT NULL DEFAULT '0',
  `min_qty` int(11) DEFAULT NULL,
  `even_color` tinyint(1) NOT NULL DEFAULT '0',
  `fabric_id` int(11) DEFAULT NULL,
  `fabric` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `made_in_id` int(11) DEFAULT NULL,
  `labeled` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `keywords` varchar(200) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `body_size_id` int(11) DEFAULT NULL,
  `pattern_id` int(11) DEFAULT NULL,
  `length_id` int(11) DEFAULT NULL,
  `style_id` int(11) DEFAULT NULL,
  `master_fabric_id` int(11) DEFAULT NULL,
  `memo` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `vendor_style_no` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `category_second_slider` tinyint(1) NOT NULL DEFAULT '0',
  `category_top_slider` tinyint(1) NOT NULL DEFAULT '0',
  `main_slider` tinyint(1) NOT NULL DEFAULT '0',
  `new_second_slider` tinyint(1) NOT NULL DEFAULT '0',
  `new_top_slider` tinyint(1) NOT NULL DEFAULT '0',
  `activated_at` timestamp NULL DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=124 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `items`
--

INSERT INTO `items` (`id`, `vendor_meta_id`, `status`, `style_no`, `price`, `orig_price`, `pack_id`, `sorting`, `description`, `available_on`, `availability`, `name`, `default_parent_category`, `default_second_category`, `default_third_category`, `exclusive`, `min_qty`, `even_color`, `fabric_id`, `fabric`, `made_in_id`, `labeled`, `keywords`, `body_size_id`, `pattern_id`, `length_id`, `style_id`, `master_fabric_id`, `memo`, `vendor_style_no`, `category_second_slider`, `category_top_slider`, `main_slider`, `new_second_slider`, `new_top_slider`, `activated_at`, `created_at`, `updated_at`, `deleted_at`) VALUES
(17, 1, 1, '7437 COR .', 14.50, NULL, 1, 5, 'LACE DETAILED OFF THE SHOULDER TOP WITH PUFF SLEEVES by M2\r\n\r\nPre Order Available ETA : 02/12/2018\r\n\r\nModel is wearing small size.\r\n\r\nIn packs of 2S, 2M and 2L per color only.', '2018-06-14', 1, 'LACE DETAILED OFF THE SHOULDER TOP', 1, 10, 18, 0, 6, 0, 9, NULL, 1, NULL, 'LACE, DETAILED, OFF, THE, SHOULDER, TOP, WITH, PUFF, SLEEVES', 3, 4, 4, NULL, 43, NULL, '7437', 0, 0, 0, 0, 1, '2018-06-22 02:23:19', '2018-06-05 04:30:21', '2018-10-17 00:19:28', NULL),
(16, 1, 1, '7378 WHT', 12.50, NULL, 2, 4, 'Crystal Embellished Lace Halter Top with Stripe Mesh by M2\r\n\r\nModel is wearing small size.\r\n\r\nIn packs of 2S, 2M and 2L per color only.', NULL, 2, 'Crystal Embellished Lace Halter Top Stripe Mesh', 1, 10, 18, 0, NULL, 0, 9, NULL, 1, NULL, 'CRYSTAL, EMBELLISHED, LACE, HALTER, TOP,STRIPE, MESH', 3, 7, 5, NULL, 37, NULL, '7378 WHITE', 1, 1, 1, 1, 0, '2018-06-10 11:20:27', '2018-06-05 04:27:07', '2018-10-17 00:19:28', NULL),
(15, 1, 1, '9403 WHT', 15.00, NULL, 1, 1, 'FLORAL EMBROIDERY LACE TOP by M2\r\n\r\nModel is wearing small size.\r\n\r\nIn packs of 2S, 2M and 2L per color only.', NULL, NULL, 'FLORAL EMBROIDERY LACE TOP', 1, 10, 21, 0, 6, 0, 6, NULL, 1, NULL, NULL, 3, 4, 3, NULL, 43, NULL, '9403', 0, 0, 1, 0, 0, '2018-06-25 09:34:36', '2018-05-23 04:23:20', '2018-10-17 00:19:28', NULL),
(14, 1, 0, '9389 RD  .', 15.50, 18.00, 1, 2, 'Model is wearing small size.\r\n\r\nIn packs of 2S, 2M and 2L per color only.', '2018-08-15', 1, 'FLOWER BEAD EMBROIDERY ROUND NECK BLOUSE', 1, 10, 21, 0, 6, 0, 9, NULL, 1, NULL, 'Flower Bead Embroidery Round Neck Blouse, Fashion', 3, 11, 5, NULL, 5, NULL, '9389', 1, 0, 0, 0, 0, '2018-08-30 01:05:32', '2018-05-23 04:18:19', '2018-10-06 04:08:20', NULL),
(18, 2, 1, '2314. WH  .', 14.50, NULL, 4, 50, 'V-NECK FLORAL EMBROIDERED TOP  by M2\r\n\r\nModel is wearing small size.\r\n\r\nIn packs of 2S, 2M and 2L per color only.', NULL, 2, 'V-NECK FLORAL EMBROIDERED TOP', 1, 10, 21, 0, 6, 0, 12, NULL, 17, 'labeled', 'V NECK, FLORAL, EMBROIDERED ,TOP', 3, 18, 3, NULL, 28, NULL, '2314 WHITE', 0, 1, 1, 0, 0, NULL, '2018-06-05 15:27:02', '2018-06-08 11:36:20', NULL),
(19, 2, 1, '9376 PINK', 14.50, NULL, 4, 138, 'RIBBON DETAIL COLD SHOULDER TOP by M2\r\n\r\nModel is wearing small size.\r\n\r\nIn packs of 2S, 2M and 2L per color only.', NULL, 2, 'RIBBON DETAIL COLD SHOULDER TOP', 1, 10, 18, 0, 6, 0, 12, NULL, 17, 'labeled', 'RIBBON ,DETAIL ,COLD SHOULDER ,TOP', 3, 3, 6, NULL, 5, NULL, '9376 PINK', 1, 1, 0, 0, 0, '2018-07-10 18:00:00', '2018-06-05 15:30:55', '2018-06-08 11:36:18', NULL),
(20, 1, 0, '7437 COR .-Clone', 14.50, NULL, 1, 145, 'LACE DETAILED OFF THE SHOULDER TOP WITH PUFF SLEEVES by M2\r\n\r\nPre Order Available ETA : 02/12/2018\r\n\r\nModel is wearing small size.\r\n\r\nIn packs of 2S, 2M and 2L per color only.', NULL, NULL, 'LACE DETAILED OFF THE SHOULDER TOP', 1, 10, 18, 0, 6, 0, 9, NULL, 1, NULL, 'LACE, DETAILED, OFF, THE, SHOULDER, TOP, WITH, PUFF, SLEEVES', 3, 4, 4, NULL, 43, NULL, '7437', 0, 0, 0, 0, 0, NULL, '2018-06-01 10:12:18', '2018-06-01 10:20:13', '2018-06-01 10:20:13'),
(21, 1, 0, '7437 COR .-Clone-Clone', 14.50, NULL, 1, 145, 'LACE DETAILED OFF THE SHOULDER TOP WITH PUFF SLEEVES by M2\r\n\r\nPre Order Available ETA : 02/12/2018\r\n\r\nModel is wearing small size.\r\n\r\nIn packs of 2S, 2M and 2L per color only.', NULL, NULL, 'LACE DETAILED OFF THE SHOULDER TOP', 1, 10, 18, 0, 6, 0, 9, NULL, 1, NULL, 'LACE, DETAILED, OFF, THE, SHOULDER, TOP, WITH, PUFF, SLEEVES', 3, 4, 4, NULL, 43, NULL, '7437', 0, 0, 0, 0, 0, NULL, '2018-06-01 10:16:37', '2018-06-01 10:20:13', '2018-06-01 10:20:13'),
(22, 1, 0, '7437 COR .-Clone-Clone-Clone', 14.50, NULL, 1, 145, 'LACE DETAILED OFF THE SHOULDER TOP WITH PUFF SLEEVES by M2\r\n\r\nPre Order Available ETA : 02/12/2018\r\n\r\nModel is wearing small size.\r\n\r\nIn packs of 2S, 2M and 2L per color only.', NULL, NULL, 'LACE DETAILED OFF THE SHOULDER TOP', 1, 10, 18, 0, 6, 0, 9, NULL, 1, NULL, 'LACE, DETAILED, OFF, THE, SHOULDER, TOP, WITH, PUFF, SLEEVES', 3, 4, 4, NULL, 43, NULL, '7437', 0, 0, 0, 0, 0, NULL, '2018-06-01 10:18:43', '2018-06-01 10:20:13', '2018-06-01 10:20:13'),
(23, 1, 0, '7437 COR .-Clone-Clone-Clone', 14.50, NULL, 1, 145, 'LACE DETAILED OFF THE SHOULDER TOP WITH PUFF SLEEVES by M2\r\n\r\nPre Order Available ETA : 02/12/2018\r\n\r\nModel is wearing small size.\r\n\r\nIn packs of 2S, 2M and 2L per color only.', NULL, NULL, 'LACE DETAILED OFF THE SHOULDER TOP', 1, 10, 18, 0, 6, 0, 9, NULL, 1, NULL, 'LACE, DETAILED, OFF, THE, SHOULDER, TOP, WITH, PUFF, SLEEVES', 3, 4, 4, NULL, 43, NULL, '7437', 0, 0, 0, 0, 0, NULL, '2018-06-01 10:19:31', '2018-06-01 10:20:13', '2018-06-01 10:20:13'),
(24, 1, 0, '7437 COR .-Clone', 14.50, NULL, 1, 145, 'LACE DETAILED OFF THE SHOULDER TOP WITH PUFF SLEEVES by M2\r\n\r\nPre Order Available ETA : 02/12/2018\r\n\r\nModel is wearing small size.\r\n\r\nIn packs of 2S, 2M and 2L per color only.', NULL, NULL, 'LACE DETAILED OFF THE SHOULDER TOP', 1, 10, 18, 0, 6, 0, 9, NULL, 1, NULL, 'LACE, DETAILED, OFF, THE, SHOULDER, TOP, WITH, PUFF, SLEEVES', 3, 4, 4, NULL, 43, NULL, '7437', 0, 0, 0, 0, 0, NULL, '2018-05-02 10:25:01', '2018-06-01 10:30:58', '2018-06-01 10:30:58'),
(30, 1, 0, 'Test2', 10.99, 12.99, 1, NULL, 'Test', '2017-09-08', NULL, 'Test2', 1, 10, 18, 0, 6, 0, 9, NULL, 16, NULL, 'Fashion Tops,Solid,Blouse,Chiffon', 3, 22, 2, NULL, NULL, 'Test', 'Test', 0, 0, 0, 0, 0, NULL, '2018-06-05 12:31:47', '2018-06-05 12:31:47', '2018-06-10 18:00:00'),
(29, 1, 0, 'Test', 10.99, 12.99, 1, NULL, 'Test', '2017-09-08', NULL, 'Test', 1, 10, 18, 0, 6, 0, 9, NULL, 16, NULL, 'Fashion Tops,Solid,Blouse,Chiffon', 3, 22, 2, NULL, NULL, 'Test', 'Test', 0, 0, 0, 0, 0, NULL, '2018-06-05 12:31:44', '2018-06-05 12:31:44', '2018-06-17 18:00:00'),
(31, 1, 0, 'Test', 10.99, 12.99, 1, NULL, 'Test', '2017-09-08', NULL, 'Test', 1, 10, 18, 0, 6, 0, 9, NULL, 16, NULL, 'Fashion Tops,Solid,Blouse,Chiffon', 3, 22, 2, NULL, NULL, 'Test', 'Test', 0, 0, 0, 0, 0, NULL, '2018-06-05 12:34:03', '2018-06-05 12:34:03', '2018-06-17 18:00:00'),
(32, 1, 0, 'Test2', 10.99, 12.99, 1, NULL, 'Test', '2017-09-08', NULL, 'Test2', 1, 10, 18, 0, 6, 0, 9, NULL, 16, NULL, 'Fashion Tops,Solid,Blouse,Chiffon', 3, 22, 2, NULL, NULL, 'Test', 'Test', 0, 0, 0, 0, 0, NULL, '2018-06-05 12:34:05', '2018-06-05 12:34:05', '2018-06-10 18:00:00'),
(33, 1, 0, 'Test', 10.99, 12.99, 1, NULL, 'Test', '2017-09-08', NULL, 'Test', 1, 10, 18, 0, 6, 0, 9, NULL, 16, NULL, 'Fashion Tops,Solid,Blouse,Chiffon', 3, 22, 2, NULL, NULL, 'Test', 'Test', 0, 0, 0, 0, 0, NULL, '2018-06-05 12:34:49', '2018-06-05 12:34:49', '2018-06-10 18:00:00'),
(34, 1, 0, 'Test2', 10.99, 12.99, 1, NULL, 'Test', '2017-09-08', NULL, 'Test2', 1, 10, 18, 0, 6, 0, 9, NULL, 16, NULL, 'Fashion Tops,Solid,Blouse,Chiffon', 3, 22, 2, NULL, NULL, 'Test', 'Test', 0, 0, 0, 0, 0, NULL, '2018-06-05 12:34:51', '2018-06-05 12:34:51', '2018-06-03 18:00:00'),
(35, 1, 0, 'Test', 10.99, 12.99, 1, NULL, 'Test', '2017-09-08', NULL, 'Test', 1, 10, 18, 0, 6, 0, 9, NULL, 16, NULL, 'Fashion Tops,Solid,Blouse,Chiffon', 3, 22, 2, NULL, NULL, 'Test', 'Test', 0, 0, 0, 0, 0, NULL, '2018-06-05 14:57:12', '2018-06-05 14:57:12', '2018-06-11 18:00:00'),
(36, 1, 0, 'Test2', 10.99, 12.99, 1, NULL, 'Test', '2017-09-08', NULL, 'Test', 1, 10, 18, 0, 6, 0, 9, NULL, 16, NULL, 'Fashion Tops,Solid,Blouse,Chiffon', 3, 22, 2, NULL, NULL, 'Test', 'Test', 0, 0, 0, 0, 0, NULL, '2018-06-05 14:57:12', '2018-06-05 14:57:12', '2018-06-10 18:00:00'),
(37, 1, 0, 'Test', 10.99, 12.99, 1, NULL, 'Test', '2017-09-08', NULL, 'Test', 1, 10, 18, 0, 6, 0, 9, NULL, 16, NULL, 'Fashion Tops,Solid,Blouse,Chiffon', 3, 22, 2, NULL, NULL, 'Test', 'Test', 0, 0, 0, 0, 0, NULL, '2018-06-05 14:58:32', '2018-06-05 14:58:32', '2018-06-10 18:00:00'),
(38, 1, 0, 'Test2', 10.99, 12.99, 1, NULL, 'Test', '2017-09-08', NULL, 'Test', 1, 10, 18, 0, 6, 0, 9, NULL, 16, NULL, 'Fashion Tops,Solid,Blouse,Chiffon', 3, 22, 2, NULL, NULL, 'Test', 'Test', 0, 0, 0, 0, 0, NULL, '2018-06-05 14:58:33', '2018-06-05 14:58:33', '2018-06-10 18:00:00'),
(39, 1, 0, 'Test', 10.99, 12.99, 1, NULL, 'Test', '2017-09-08', NULL, 'Test', 1, 10, 18, 0, 6, 0, 9, NULL, 16, NULL, 'Fashion Tops,Solid,Blouse,Chiffon', 3, 22, 2, NULL, NULL, 'Test', 'Test', 0, 0, 0, 0, 0, NULL, '2018-06-05 15:01:13', '2018-06-05 15:01:13', '2018-06-10 18:00:00'),
(40, 1, 0, 'Test2', 10.99, 12.99, 1, NULL, 'Test', '2017-09-08', NULL, 'Test', 1, 10, 18, 0, 6, 0, 9, NULL, 16, NULL, 'Fashion Tops,Solid,Blouse,Chiffon', 3, 22, 2, NULL, NULL, 'Test', 'Test', 0, 0, 0, 0, 0, NULL, '2018-06-05 15:01:15', '2018-06-05 15:01:15', '2018-06-10 18:00:00'),
(41, 1, 0, 'Test', 10.99, 12.99, 1, NULL, 'Test', '2017-09-08', NULL, 'Test', 1, 10, 18, 0, 6, 0, 9, NULL, 16, NULL, 'Fashion Tops,Solid,Blouse,Chiffon', 3, 22, 2, NULL, NULL, 'Test', 'Test', 0, 0, 0, 0, 0, NULL, '2018-06-05 15:02:14', '2018-06-13 16:54:09', '2018-06-13 16:54:09'),
(42, 1, 0, 'Test2', 10.99, 12.99, 1, NULL, 'Test', '2017-09-08', NULL, 'Test', 1, 10, 18, 0, 6, 0, 9, NULL, 16, NULL, 'Fashion Tops,Solid,Blouse,Chiffon', 3, 22, 2, NULL, NULL, 'Test', 'Test', 0, 0, 0, 0, 0, NULL, '2018-06-05 15:02:16', '2018-06-13 16:54:09', '2018-06-13 16:54:09'),
(43, 1, 0, 'adsfadsf', 12.00, NULL, 2, NULL, NULL, NULL, 2, NULL, 1, NULL, NULL, 0, NULL, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, 0, 0, 0, 0, NULL, '2018-06-12 12:56:50', '2018-06-12 12:56:50', '2018-06-16 18:00:00'),
(44, 1, 0, 'adsfadsf', 12.00, NULL, 2, NULL, NULL, NULL, 2, NULL, 1, NULL, NULL, 0, NULL, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, 0, 0, 0, 0, NULL, '2018-06-12 12:58:05', '2018-06-12 12:58:05', '2018-06-10 18:00:00'),
(45, 1, 0, 'eee', 34.00, NULL, 2, NULL, NULL, NULL, 2, NULL, 9, NULL, NULL, 0, NULL, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, 0, 0, 0, 0, NULL, '2018-06-12 13:00:03', '2018-06-12 13:00:03', '2018-06-17 18:00:00'),
(46, 1, 0, 'ttt', 24.00, NULL, 2, NULL, NULL, NULL, 2, NULL, 1, 10, 18, 0, NULL, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, 0, 0, 0, 0, NULL, '2018-06-12 13:11:44', '2018-06-12 13:11:44', '2018-06-09 18:00:00'),
(47, 1, 0, 'ttt-Clone', 24.00, NULL, 2, NULL, NULL, NULL, NULL, NULL, 1, 10, 18, 0, NULL, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, 0, 0, 0, 0, NULL, '2018-06-12 13:19:49', '2018-06-12 13:19:49', '2018-06-17 18:00:00'),
(48, 1, 0, 'ttt-Clone', 24.00, NULL, 2, NULL, NULL, NULL, NULL, NULL, 1, 10, 18, 0, NULL, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, 0, 0, 0, 0, NULL, '2018-06-12 13:20:57', '2018-06-12 13:20:57', '2018-06-16 18:00:00'),
(49, 1, 0, 'ttt-Clone', 24.00, NULL, 2, NULL, NULL, NULL, NULL, NULL, 1, 10, 18, 0, NULL, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, 0, 0, 0, 0, NULL, '2018-06-12 13:27:59', '2018-06-12 13:27:59', '2018-06-10 18:00:00'),
(50, 1, 0, 'ttt-Clone', 24.00, NULL, 2, NULL, NULL, NULL, NULL, NULL, 1, 10, 18, 0, NULL, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, 0, 0, 0, 0, NULL, '2018-06-12 13:28:41', '2018-06-12 13:28:41', '2018-06-09 18:00:00'),
(51, 1, 0, 'Test6', 10.99, 12.99, 1, NULL, 'Test', '2017-09-08', NULL, 'Test', 1, 10, 18, 0, 6, 0, 9, NULL, 16, NULL, 'Fashion Tops,Solid,Blouse,Chiffon', 3, 22, 2, NULL, NULL, 'Test', 'Test', 0, 0, 0, 0, 0, NULL, '2018-06-13 02:30:34', '2018-06-13 02:30:34', '2018-06-10 18:00:00'),
(52, 1, 0, 'Test6', 10.99, 12.99, 1, NULL, 'Test', '2017-09-08', NULL, 'Test', 1, 10, 18, 0, 6, 0, 9, NULL, 16, NULL, 'Fashion Tops,Solid,Blouse,Chiffon', 3, 22, 2, NULL, NULL, 'Test', 'Test', 0, 0, 0, 0, 0, NULL, '2018-06-13 02:32:13', '2018-06-13 02:32:13', '2018-06-09 18:00:00'),
(53, 1, 0, 'Test6', 10.99, 12.99, 1, NULL, 'Test', '2017-09-08', NULL, 'Test', 1, 10, 18, 0, 6, 0, 9, NULL, 16, NULL, 'Fashion Tops,Solid,Blouse,Chiffon', 3, 22, 2, NULL, NULL, 'Test', 'Test', 0, 0, 0, 0, 0, NULL, '2018-06-13 02:33:56', '2018-06-13 02:33:56', '2018-06-09 18:00:00'),
(54, 1, 0, 'Test6', 10.99, 12.99, 1, NULL, 'Test', '2017-09-08', NULL, 'Test', 1, 10, 18, 0, 6, 0, 9, NULL, 16, NULL, 'Fashion Tops,Solid,Blouse,Chiffon', 3, 22, 2, NULL, NULL, 'Test', 'Test', 0, 0, 0, 0, 0, NULL, '2018-06-13 02:35:08', '2018-06-13 02:35:08', '2018-06-09 18:00:00'),
(55, 1, 0, 'Test6', 10.99, 12.99, 1, NULL, 'Test', '2017-09-08', NULL, 'Test', 1, 10, 18, 0, 6, 0, 9, NULL, 16, NULL, 'Fashion Tops,Solid,Blouse,Chiffon', 3, 22, 2, NULL, NULL, 'Test', 'Test', 0, 0, 0, 0, 0, NULL, '2018-06-13 02:36:06', '2018-06-13 02:36:06', '2018-06-09 18:00:00'),
(56, 1, 0, 'Test6', 10.99, 12.99, 1, NULL, 'Test', '2017-09-08', NULL, 'Test', 1, 10, 18, 0, 6, 0, 9, NULL, 16, NULL, 'Fashion Tops,Solid,Blouse,Chiffon', 3, 22, 2, NULL, NULL, 'Test', 'Test', 0, 0, 0, 0, 0, NULL, '2018-06-13 02:38:13', '2018-06-13 02:38:13', '2018-06-05 18:00:00'),
(57, 1, 0, 'Test6', 10.99, 12.99, 1, NULL, 'Test', '2017-09-08', NULL, 'Test', 1, 10, 18, 0, 6, 0, 9, NULL, 16, NULL, 'Fashion Tops,Solid,Blouse,Chiffon', 3, 22, 2, NULL, NULL, 'Test', 'Test', 0, 0, 0, 0, 0, NULL, '2018-06-13 02:39:09', '2018-06-13 02:39:09', '2018-06-09 18:00:00'),
(58, 1, 0, 'Test', 10.99, 12.99, 1, NULL, 'Test', '2017-09-08', NULL, 'Test', 1, 10, 18, 0, 6, 0, 9, NULL, 16, NULL, 'Fashion Tops,Solid,Blouse,Chiffon', 3, 22, 2, NULL, NULL, 'Test', 'Test', 0, 0, 0, 0, 0, NULL, '2018-06-22 04:51:42', '2018-06-22 04:51:42', '2018-06-02 18:00:00'),
(59, 1, 0, 'Test', 10.99, 12.99, 1, NULL, 'Test', '2017-09-08', NULL, 'Test', 1, 10, 18, 0, 6, 0, 9, NULL, 16, NULL, 'Fashion Tops,Solid,Blouse,Chiffon', 3, 22, 2, NULL, NULL, 'Test', 'Test', 0, 0, 0, 0, 0, NULL, '2018-06-22 04:52:12', '2018-06-22 04:52:12', '2018-06-03 18:00:00'),
(60, 1, 0, 'Test', 10.99, 12.99, 1, NULL, 'Test', '2017-09-08', NULL, 'Test', 1, 10, 18, 0, 6, 0, 9, NULL, 16, NULL, 'Fashion Tops,Solid,Blouse,Chiffon', 3, 22, 2, NULL, NULL, 'Test', 'Test', 0, 0, 0, 0, 0, NULL, '2018-06-22 04:52:42', '2018-06-22 04:52:42', '2018-06-10 18:00:00'),
(61, 1, 0, 'Test', 10.99, 12.99, 1, NULL, 'Test', '2017-09-08', NULL, 'Test', 1, 10, 18, 0, 6, 0, 9, NULL, 16, NULL, 'Fashion Tops,Solid,Blouse,Chiffon', 3, 22, 2, NULL, NULL, 'Test', 'Test', 0, 0, 0, 0, 0, NULL, '2018-06-22 05:11:13', '2018-06-22 05:11:13', '2018-06-10 18:00:00'),
(62, 1, 0, 'Test', 10.99, 12.99, 1, NULL, 'Test', '2017-09-08', 2, 'Test', 1, 10, 18, 0, 6, 0, 9, NULL, 16, NULL, 'Fashion Tops,Solid,Blouse,Chiffon', 3, 22, 2, NULL, NULL, 'Test', 'Test', 0, 0, 0, 0, 0, NULL, '2018-06-23 09:04:22', '2018-06-23 09:04:22', '2018-06-02 18:00:00'),
(63, 1, 0, 'Test', 10.99, 12.99, 1, NULL, 'Test', '2017-09-08', 2, 'Test', 1, 10, 18, 0, 6, 0, 9, NULL, 16, NULL, 'Fashion Tops,Solid,Blouse,Chiffon', 3, 22, 2, NULL, NULL, 'Test', 'Test', 0, 0, 0, 0, 0, NULL, '2018-06-23 09:19:01', '2018-06-23 09:19:01', '2018-06-17 18:00:00'),
(64, 1, 0, 'Test', 10.99, 12.99, 1, NULL, 'Test', '2019-09-08', 1, 'Test', 1, 10, 18, 0, 6, 0, 9, NULL, 16, NULL, 'Fashion Tops,Solid,Blouse,Chiffon', 3, 22, 2, NULL, NULL, 'Test', 'Test', 0, 0, 0, 0, 0, NULL, '2018-06-23 09:19:38', '2018-06-23 09:19:38', '2018-06-10 18:00:00'),
(65, 1, 0, 'Test', 10.99, 12.99, 1, NULL, 'Test', '2019-09-08', 1, 'Test', 1, 10, 18, 0, 6, 0, 9, NULL, 16, NULL, 'Fashion Tops,Solid,Blouse,Chiffon', 3, 22, 2, NULL, NULL, 'Test', 'Test', 0, 0, 0, 0, 0, NULL, '2018-06-23 12:34:12', '2018-06-25 09:35:08', '2018-06-25 09:35:08'),
(66, 1, 0, 'LT3751', 12.75, NULL, 1, NULL, 'Off  Shoulder Roll up Sleeve Top', NULL, 2, 'Off  Shoulder Roll Up Sleeve Top', 1, 10, 18, 0, 6, 0, NULL, NULL, NULL, NULL, 'off shoulder topkimono plus size romper jumpsuit leather jacket maxi dress shorts she  dress rompers bodysuit leopard cardigan pineapple camo sunglasses  leggings jeggings long', 3, 22, 2, NULL, NULL, NULL, 'LT3751', 0, 0, 0, 0, 0, NULL, '2018-06-27 12:13:43', '2018-06-27 12:14:39', '2018-06-27 12:14:39'),
(67, 1, 1, 'LT3751', 12.75, NULL, 1, 3, 'Off  Shoulder Roll up Sleeve Top', NULL, 2, 'Off  Shoulder Roll Up Sleeve Top', 1, 10, 18, 0, 6, 0, NULL, NULL, NULL, NULL, 'off shoulder topkimono plus size romper jumpsuit leather jacket maxi dress shorts she  dress rompers bodysuit leopard cardigan pineapple camo sunglasses  leggings jeggings long', 3, 22, 2, NULL, NULL, NULL, 'LT3751', 0, 0, 0, 0, 0, '2018-06-28 03:45:01', '2018-06-27 12:14:46', '2018-10-17 00:19:28', NULL),
(68, 1, 0, 'Test', 10.99, 12.99, 1, NULL, 'Test', '2017-09-08', 2, 'Test', 1, 10, 18, 0, 6, 0, 9, '100% COTTON', 16, NULL, 'Fashion Tops,Solid,Blouse,Chiffon', 3, 22, 2, NULL, NULL, 'Test', 'Test', 0, 0, 0, 0, 0, '2018-07-03 05:01:27', '2018-06-28 03:00:57', '2018-07-04 04:17:12', '2018-07-04 04:17:12'),
(69, 1, 0, 'asdf', 23.00, NULL, 2, 1, NULL, NULL, 2, NULL, 1, NULL, NULL, 0, NULL, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, 0, 0, 0, 0, NULL, '2018-06-28 10:04:50', '2018-06-28 10:31:27', NULL),
(70, 1, 0, 'asdfee', 23.00, NULL, 2, NULL, NULL, NULL, 2, NULL, 1, 10, 13, 0, NULL, 0, NULL, 'ddd3', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, 0, 0, 0, 0, NULL, '2018-06-28 10:54:00', '2018-07-04 02:59:55', '2018-07-04 02:59:55'),
(71, 1, 0, 'asdfee-Clone', 23.00, NULL, 2, NULL, NULL, NULL, 2, NULL, 1, 10, 13, 0, NULL, 0, NULL, 'ddd3e', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, 0, 0, 0, 0, NULL, '2018-06-28 11:01:21', '2018-07-04 02:13:24', '2018-07-04 02:13:24'),
(72, 1, 0, 'tttt3434', 58.00, NULL, 11, NULL, NULL, '2018-06-12', 2, NULL, 1, 10, 13, 0, NULL, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, 0, 0, 0, 0, NULL, '2018-06-29 05:17:36', '2018-07-17 21:27:39', NULL),
(73, 1, 0, 'Test', 10.99, 12.99, 1, NULL, 'Test', '2017-09-08', 2, 'Test', 1, 10, 18, 0, 6, 0, NULL, '100% COTTON', 16, NULL, 'Fashion Tops,Solid,Blouse,Chiffon', 3, 22, 2, NULL, NULL, 'Test', 'Test', 0, 0, 0, 0, 0, NULL, '2018-07-04 04:18:10', '2018-07-04 04:20:23', '2018-07-04 04:20:23'),
(74, 1, 0, 'Test', 10.99, 12.99, 1, NULL, 'Test', '2017-09-08', 2, 'Test', 1, 10, 18, 0, 6, 0, NULL, '100% COTTON', 16, NULL, 'Fashion Tops,Solid,Blouse,Chiffon', 3, 22, 2, NULL, NULL, 'Test', 'Test', 0, 0, 0, 0, 0, NULL, '2018-07-04 04:20:33', '2018-07-04 04:50:34', NULL),
(75, 1, 0, 'Test-Clone', 10.99, 12.99, 1, NULL, 'Test', '2017-09-08', 2, 'Test', 1, 10, 18, 0, 6, 0, NULL, '100% COTTON', 16, NULL, 'Fashion Tops,Solid,Blouse,Chiffon', 3, 22, 2, NULL, NULL, 'Test', 'Test', 0, 0, 0, 0, 0, NULL, '2018-07-04 04:50:40', '2018-07-04 04:50:56', NULL),
(76, 1, 0, 'Test-Clone-dfdf', 10.99, 12.99, 1, NULL, 'Test', '2017-09-08', 2, 'Test', 1, 10, 18, 0, 6, 0, NULL, '100% COTTON', 16, NULL, 'Fashion Tops,Solid,Blouse,Chiffon', 3, 22, 2, NULL, NULL, 'Test', 'Test', 0, 0, 0, 0, 0, NULL, '2018-07-04 05:05:23', '2018-07-04 05:05:23', NULL),
(77, 1, 1, 'pant', 34.00, 34.00, 2, 8, NULL, NULL, 2, NULL, 1, 14, 16, 0, NULL, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, 0, 0, 0, 0, '2018-07-10 05:10:39', '2018-07-10 03:15:28', '2018-10-17 00:19:28', NULL),
(78, 1, 1, 'adf', 34.00, NULL, 2, 9, NULL, NULL, 2, NULL, 1, 10, 12, 0, NULL, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, 0, 0, 0, 0, '2018-08-17 22:11:35', '2018-07-10 04:38:09', '2018-10-17 00:19:28', NULL),
(79, 1, 0, 'Test-ba', 10.99, 12.99, 1, NULL, 'Test', '2017-09-08', 2, 'Test', 1, 10, 18, 0, 6, 0, NULL, '100% COTTON', 16, NULL, 'Fashion Tops,Solid,Blouse,Chiffon', 3, 22, 2, NULL, NULL, 'Test', 'Test', 0, 0, 0, 0, 0, NULL, '2018-07-10 06:57:56', '2018-07-10 06:57:56', '2018-06-30 18:00:00'),
(80, 1, 0, 'Test-ba', 10.99, 12.99, 1, NULL, 'Test', '2017-09-08', 2, 'Test', 1, 10, 18, 0, 6, 0, NULL, '100% COTTON', 16, NULL, 'Fashion Tops,Solid,Blouse,Chiffon', 3, 22, 2, NULL, NULL, 'Test', 'Test', 0, 0, 0, 0, 0, NULL, '2018-07-10 06:59:11', '2018-07-10 06:59:11', '2018-07-07 18:00:00'),
(81, 1, 0, 'Test-ba', 10.99, 12.99, 1, NULL, 'Test', '2017-09-08', 2, 'Test', 1, 10, 18, 0, 6, 0, NULL, '100% COTTON', 16, NULL, 'Fashion Tops,Solid,Blouse,Chiffon', 3, 22, 2, NULL, NULL, 'Test', 'Test', 0, 0, 0, 0, 0, NULL, '2018-07-10 08:03:26', '2018-07-10 08:03:26', NULL),
(82, 1, 0, 'ete', 34.00, 34.00, 3, 5, NULL, NULL, 2, NULL, 9, 42, 37, 0, NULL, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, 0, 0, 0, 0, NULL, '2018-07-17 19:54:18', '2018-07-17 19:57:43', NULL),
(83, 1, 0, 'ete-Clone', 34.00, 34.00, 3, 5, NULL, NULL, 2, NULL, 9, 42, 37, 0, NULL, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, 0, 0, 0, 0, NULL, '2018-07-17 19:58:19', '2018-08-17 22:16:54', '2018-08-17 22:16:54'),
(84, 1, 0, 'new-api', 58.00, NULL, 1, NULL, NULL, '2018-06-12', 2, NULL, 1, 10, 13, 0, NULL, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, 0, 0, 0, 0, NULL, '2018-07-17 20:28:19', '2018-07-17 20:28:19', '2018-07-02 18:00:00'),
(85, 1, 0, 'new-api', 58.00, NULL, 1, NULL, NULL, '2018-06-12', 2, NULL, 1, 10, 13, 0, NULL, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, 0, 0, 0, 0, NULL, '2018-07-17 20:33:07', '2018-07-17 20:33:07', '2018-06-30 18:00:00'),
(86, 1, 0, 'new-api', 58.00, NULL, 1, NULL, NULL, '2018-06-12', 2, NULL, 1, 10, 13, 0, NULL, 0, NULL, 'asdfasd', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, 0, 0, 0, 0, NULL, '2018-07-17 20:40:38', '2018-07-17 20:40:38', '2018-07-07 18:00:00'),
(87, 1, 0, 'new-api', 58.00, NULL, 1, NULL, NULL, '2018-06-12', 2, NULL, 1, 10, 13, 0, NULL, 0, NULL, 'asdfasd', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, 0, 0, 0, 0, NULL, '2018-07-17 20:47:49', '2018-07-17 20:47:49', '2018-07-07 18:00:00'),
(88, 1, 0, 'new-api', 58.00, NULL, 13, NULL, NULL, '2018-06-12', 2, NULL, 1, 10, 13, 0, NULL, 0, NULL, 'asdfasd', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, 0, 0, 0, 0, NULL, '2018-07-17 22:16:55', '2018-07-17 22:16:55', '2018-07-08 18:00:00'),
(89, 1, 0, 'new-api', 58.00, NULL, 16, NULL, 'asfaasdf', '2018-06-12', 2, NULL, 1, 10, 13, 0, NULL, 0, NULL, 'asdfasd', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, 0, 0, 0, 0, NULL, '2018-07-17 22:18:03', '2018-07-17 22:51:51', '2018-07-07 18:00:00'),
(90, 1, 0, 'new-api', 58.00, NULL, 17, NULL, 'asfaasdf', '2018-06-12', 2, NULL, 1, 10, 13, 0, NULL, 0, NULL, 'asdfasd', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, 0, 0, 0, 0, NULL, '2018-07-17 23:16:21', '2018-08-09 02:30:38', '2018-08-09 02:30:38'),
(91, 1, 1, 'Multi11', 23.00, 23.00, 11, 10, 'asdf', NULL, 2, NULL, 1, 10, 18, 0, NULL, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, 0, 0, 0, 0, '2018-07-09 18:00:00', '2018-07-18 20:37:18', '2018-10-17 00:19:28', NULL),
(92, 1, 1, 'Multi-Clone', 23.00, 23.00, 11, 11, 'asdf', NULL, 2, NULL, 1, 10, 18, 0, NULL, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, 0, 0, 0, 0, '2018-07-03 18:00:00', '2018-07-18 21:20:52', '2018-10-17 00:19:28', NULL),
(93, 1, 1, 'active', 234.00, NULL, 7, NULL, NULL, NULL, 2, NULL, 1, 10, 18, 0, NULL, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, 0, 0, 0, 0, NULL, '2018-07-18 22:18:20', '2018-07-18 22:18:20', '2018-07-14 18:00:00'),
(94, 1, 0, 'activeer', 34.00, NULL, 2, NULL, NULL, NULL, 2, NULL, 1, 10, 17, 0, NULL, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, 0, 0, 0, 0, NULL, '2018-07-18 22:19:20', '2018-07-18 22:19:33', '2018-07-18 22:19:33'),
(95, 1, 1, 'active-bbb', 34.00, NULL, 11, 7, NULL, NULL, 2, NULL, 1, 10, 18, 0, NULL, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, 0, 0, 0, 0, '2018-07-18 22:19:54', '2018-07-18 22:19:54', '2018-10-17 00:19:28', NULL),
(96, 1, 1, 'active-bbb-Clone', 34.00, NULL, 11, 6, NULL, NULL, 2, NULL, 1, 10, 18, 0, NULL, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, 0, 0, 0, 0, '2018-07-18 22:20:59', '2018-07-18 22:20:59', '2018-10-17 00:19:28', NULL),
(97, 1, 0, 'Test-m', 10.99, 12.99, 1, NULL, 'Test', '2017-09-08', 2, 'Test', 1, 10, 18, 0, 6, 0, NULL, '100% COTTON', 16, NULL, 'Fashion Tops,Solid,Blouse,Chiffon', 3, 22, 2, NULL, NULL, 'Test', 'Test', 0, 0, 0, 0, 0, NULL, '2018-07-18 23:00:52', '2018-07-18 23:01:16', '2018-06-30 18:00:00'),
(98, 1, 0, 'Test-m', 10.99, 12.99, 1, NULL, 'Test', '2017-09-08', 2, 'Test', 1, 10, 18, 0, 6, 0, NULL, '100% COTTON', 16, NULL, 'Fashion Tops,Solid,Blouse,Chiffon', 3, 22, 2, NULL, NULL, 'Test', 'Test', 0, 0, 0, 0, 0, NULL, '2018-07-18 23:03:21', '2018-07-18 23:04:17', NULL),
(99, 1, 0, 'new-api-m', 58.00, NULL, 21, NULL, 'asfaasdf', '2018-06-12', 2, NULL, 1, 10, 13, 0, NULL, 0, NULL, 'asdfasd', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, 0, 0, 0, 0, NULL, '2018-07-18 23:05:27', '2018-08-09 02:38:42', '2018-08-09 02:38:42'),
(100, 1, 0, 'N734', 67.00, NULL, 1, NULL, NULL, '2018-07-21', 1, 'Tiered Ring Triangle Rhinestone Charm Necklace', 61, 62, 63, 0, NULL, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, 0, 0, 0, 0, NULL, '2018-07-19 04:09:47', '2018-07-19 04:11:14', '2018-07-08 18:00:00'),
(101, 1, 0, 'N67', 34.00, NULL, 1, NULL, NULL, '2018-07-18', 2, 'Scorpio Pendant Necklace.', 61, 62, 63, 0, NULL, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, 0, 0, 0, 0, NULL, '2018-07-19 04:11:25', '2018-07-19 04:11:25', '2018-07-08 18:00:00'),
(102, 1, 0, 'N734', 67.00, NULL, 1, NULL, NULL, '2018-07-21', 1, 'Tiered Ring Triangle Rhinestone Charm Necklace', 61, 62, 63, 0, NULL, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, 0, 0, 0, 0, NULL, '2018-07-19 04:12:56', '2018-07-19 04:12:56', '2018-07-15 18:00:00'),
(103, 1, 0, 'N67', 34.00, NULL, 1, NULL, NULL, '2018-07-18', 2, 'Scorpio Pendant Necklace.', 61, 62, 63, 0, NULL, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, 0, 0, 0, 0, NULL, '2018-07-19 04:12:56', '2018-07-19 04:12:56', '2018-07-08 18:00:00'),
(104, 1, 0, 'N734', 67.00, NULL, 1, NULL, NULL, '2018-07-21', 2, 'Tiered Ring Triangle Rhinestone Charm Necklace', 1, 10, 12, 0, NULL, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, 0, 0, 0, 0, NULL, '2018-07-19 04:14:11', '2018-07-25 05:07:12', NULL),
(105, 1, 0, 'N67', 34.00, NULL, 1, NULL, NULL, '2018-07-18', 2, 'Scorpio Pendant Necklace.', 61, 62, 63, 0, NULL, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, 0, 0, 0, 0, NULL, '2018-07-19 04:14:54', '2018-07-19 04:32:38', NULL),
(106, 1, 0, 'N01', 12.00, 15.00, 1, NULL, NULL, NULL, 2, 'Flat Metallic Cut Disc Pendant Necklace', 1, 10, 13, 0, NULL, 0, NULL, NULL, 1, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, 0, 0, 0, 0, NULL, '2018-07-25 05:07:12', '2018-07-30 22:57:18', NULL),
(113, 1, 0, '5014 GREEN.', 15.00, NULL, 1, NULL, 'Palm Tree Printed Sleeveless Jumpsuit', NULL, 2, 'Palm Tree Printed Sleeveless Jumpsuit', 1, 10, 12, 0, NULL, 0, NULL, '100% POLYESTER', 1, NULL, 'palm tree,coconut,print,v neck,sleeveless,straight leg,wide leg,full length,onepiece,jumpsuit,romper,spring,summer,fall,vacation fashion,beach fashion,loose fit,shift,casual', 3, 19, NULL, NULL, NULL, NULL, '5014', 0, 0, 0, 0, 0, NULL, '2018-08-29 02:06:55', '2018-08-29 02:06:55', NULL),
(107, 1, 0, 'new-api', 58.00, NULL, 22, NULL, 'asfaasdf', '2018-06-12', 2, NULL, 1, 10, 12, 0, NULL, 0, NULL, 'asdfasd', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, 0, 0, 0, 0, NULL, '2018-08-09 02:38:25', '2018-08-09 02:38:48', '2018-08-09 02:38:48'),
(108, 1, 0, 'new-api', 58.00, NULL, 23, NULL, 'asfaasdf', '2018-06-12', 2, NULL, 1, 10, 12, 0, NULL, 0, NULL, 'asdfasd', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, 0, 0, 0, 0, NULL, '2018-08-09 02:38:52', '2018-08-09 02:39:06', '2018-08-09 02:39:06'),
(109, 1, 0, 'TEST!2', 12.00, NULL, 24, NULL, NULL, '2018-07-04', 2, 'rerer', 1, 10, 12, 0, NULL, 0, NULL, NULL, 1, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, 0, 0, 0, 0, NULL, '2018-08-15 22:10:36', '2018-08-16 03:02:30', '2018-08-16 03:02:30'),
(110, 1, 0, 'TEST!2', 12.00, NULL, 13, NULL, NULL, '2018-07-04', 2, 'rerer', 1, 10, 18, 0, NULL, 0, NULL, NULL, 1, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, 0, 0, 0, 0, NULL, '2018-08-16 03:07:36', '2018-08-16 03:19:36', NULL),
(111, 1, 0, '9383 TEST', 15.50, 18.00, 1, NULL, 'desc220\r\n2\r\nadfasd', '2018-08-31', 1, 'FLOWER BEAD EMBROIDERY ROUND NECK BLOUSE', 1, 10, 12, 0, NULL, 0, NULL, NULL, 1, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, 0, 0, 0, 0, NULL, '2018-08-27 22:18:04', '2018-08-28 22:09:03', '2018-08-28 22:09:03'),
(112, 1, 0, '9383 TEST', 15.50, 18.00, 1, NULL, 'desc220\r\n2\r\nadfasd', '2018-10-18', 1, 'FLOWER BEAD EMBROIDERY ROUND NECK BLOUSE', 1, 29, 34, 0, NULL, 0, NULL, 'asdfad', 16, 'labeled', 'Flower Bead Embroidery Round Neck Blouse, Fashion', 3, 3, NULL, NULL, 5, NULL, '9389', 0, 0, 0, 0, 0, '2018-09-04 20:14:59', '2018-08-28 22:09:05', '2018-09-11 20:47:57', '2018-09-11 20:47:57'),
(114, 1, 0, '9383 TEST', 15.50, 18.00, 1, NULL, 'desc220\r\n2\r\nadfasd', '2018-10-18', 1, 'FLOWER BEAD EMBROIDERY ROUND NECK BLOUSE', 1, 29, 34, 0, NULL, 0, NULL, 'asdfad', 16, 'labeled', 'Flower Bead Embroidery Round Neck Blouse, Fashion', 3, 3, NULL, NULL, 5, NULL, '9389', 0, 0, 0, 0, 0, NULL, '2018-09-11 20:48:08', '2018-09-11 20:48:39', '2018-09-11 20:48:39'),
(115, 1, 0, '9383 TEST', 15.50, 18.00, 1, NULL, 'desc220\r\n2\r\nadfasd', NULL, 2, 'FLOWER BEAD EMBROIDERY ROUND NECK BLOUSE', 1, 29, 34, 0, NULL, 0, NULL, 'asdfad', 16, 'labeled', 'Flower Bead Embroidery Round Neck Blouse, Fashion', 3, 3, NULL, NULL, 5, NULL, '9389', 0, 0, 0, 0, 0, NULL, '2018-09-11 20:49:07', '2018-09-14 22:19:29', '2018-09-14 22:19:29'),
(116, 1, 0, '9389 RD  .-Clone', 15.50, 18.00, 1, 2, 'Model is wearing small size.\r\n\r\nIn packs of 2S, 2M and 2L per color only.', '2018-08-15', 1, 'FLOWER BEAD EMBROIDERY ROUND NECK BLOUSE', 1, 10, 21, 0, 6, 0, NULL, NULL, 1, NULL, 'Flower Bead Embroidery Round Neck Blouse, Fashion', 3, 11, 5, NULL, 5, NULL, '9389', 0, 0, 0, 0, 0, NULL, '2018-09-13 23:30:58', '2018-09-13 23:30:58', NULL),
(117, 1, 0, '9383 TEST', 15.50, 18.00, 1, NULL, 'desc220\r\n2\r\nadfasd', NULL, 2, 'FLOWER BEAD EMBROIDERY ROUND NECK BLOUSE', 1, 29, 34, 0, NULL, 0, NULL, 'asdfad', 16, 'labeled', 'Flower Bead Embroidery Round Neck Blouse, Fashion', 3, 3, NULL, NULL, 5, NULL, '9389', 0, 0, 0, 0, 0, NULL, '2018-09-14 22:19:40', '2018-09-14 22:22:15', '2018-09-14 22:22:15'),
(118, 1, 0, '9383 TEST', 15.50, 18.00, 1, NULL, 'desc220\r\n2\r\nadfasd', NULL, 2, 'FLOWER BEAD EMBROIDERY ROUND NECK BLOUSE', 1, 29, 34, 0, NULL, 0, NULL, 'asdfad', 16, 'labeled', 'Flower Bead Embroidery Round Neck Blouse, Fashion', 3, 3, NULL, NULL, 5, NULL, '9389', 0, 0, 0, 0, 0, NULL, '2018-09-14 22:22:23', '2018-09-14 22:23:08', '2018-09-14 22:23:08'),
(119, 1, 0, '9383 TEST', 15.50, 18.00, 1, NULL, 'desc220\r\n2\r\nadfasd', NULL, 2, 'FLOWER BEAD EMBROIDERY ROUND NECK BLOUSE', 1, 29, 34, 0, NULL, 0, NULL, 'asdfad', 16, 'labeled', 'Flower Bead Embroidery Round Neck Blouse, Fashion', 3, 3, NULL, NULL, 5, NULL, '9389', 0, 0, 0, 0, 0, NULL, '2018-09-14 22:23:27', '2018-09-14 22:23:47', '2018-09-14 22:23:47'),
(120, 1, 0, '9383 TEST', 15.50, 18.00, 1, NULL, 'desc220rr\r\n2\r\nadfasd2222', NULL, 2, 'FLOWER BEAD EMBROIDERY ROUND NECK BLOUSE', 1, 29, 34, 0, NULL, 0, NULL, 'asdfad', 16, 'labeled', 'Flower Bead Embroidery Round Neck Blouse, Fashion', 3, 3, NULL, NULL, 5, NULL, '9389', 0, 0, 0, 0, 0, NULL, '2018-09-30 05:25:18', '2018-10-06 04:09:00', '2018-10-06 04:09:00'),
(121, 1, 0, '9383 TEST', 15.50, 18.00, 1, NULL, 'desc220rr\r\n2\r\nadfasd2222', NULL, 2, 'FLOWER BEAD EMBROIDERY ROUND NECK BLOUSE', 1, 29, 34, 0, NULL, 0, NULL, 'asdfad', 16, 'labeled', 'Flower Bead Embroidery Round Neck Blouse, Fashion', 3, 3, NULL, NULL, 5, NULL, '9389', 0, 0, 0, 0, 0, NULL, '2018-10-06 04:09:15', '2018-10-06 04:09:39', '2018-10-06 04:09:39'),
(122, 1, 0, '9383 TEST', 15.50, 18.00, 1, NULL, 'desc220rr\r\n2\r\nadfasd2222', NULL, 2, 'FLOWER BEAD EMBROIDERY ROUND NECK BLOUSE', 1, 29, 34, 0, NULL, 0, NULL, 'asdfad', 16, 'labeled', 'Flower Bead Embroidery Round Neck Blouse, Fashion', 3, 3, NULL, NULL, 5, NULL, '9389', 0, 0, 0, 0, 0, '2018-10-06 04:17:46', '2018-10-06 04:17:46', '2018-10-06 04:17:59', '2018-10-06 04:17:59'),
(123, 1, 1, '9383 TEST', 15.50, 18.00, 1, 0, 'CAP SLEEVE V NECK BASIC BODYSUIT \r\n\r\nModel is wearing S Size \r\n\r\nIn packs of 2S, 2M and 2L per color.', NULL, 2, 'FLOWER BEAD EMBROIDERY ROUND NECK BLOUSE', 1, 29, 34, 0, NULL, 0, NULL, 'asdfad', 16, 'labeled', 'Flower Bead Embroidery Round Neck Blouse, Fashion', 3, 3, NULL, NULL, 5, NULL, '9389-TEST', 0, 0, 0, 0, 0, '2019-01-07 23:26:56', '2018-10-06 04:20:23', '2019-01-07 23:26:56', NULL);

-- --------------------------------------------------------

--
-- Table structure for table `item_images`
--

DROP TABLE IF EXISTS `item_images`;
CREATE TABLE IF NOT EXISTS `item_images` (
  `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT,
  `item_id` int(11) DEFAULT NULL,
  `color_id` int(11) DEFAULT NULL,
  `sort` int(11) DEFAULT NULL,
  `image_path` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `list_image_path` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `thumbs_image_path` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=607 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `item_images`
--

INSERT INTO `item_images` (`id`, `item_id`, `color_id`, `sort`, `image_path`, `list_image_path`, `thumbs_image_path`, `created_at`, `updated_at`) VALUES
(85, NULL, NULL, NULL, '/images/item/235c5870-65b7-11e8-8d09-57108cde5000.jpg', NULL, NULL, '2018-06-01 10:16:28', '2018-06-01 10:16:28'),
(84, 20, NULL, 3, 'images/item/8e710240-65b6-11e8-a7a8-9d5bd3bc7ffb.jpg', NULL, NULL, '2018-06-01 10:12:18', '2018-06-01 10:18:05'),
(83, 20, 14, 2, 'images/item/8e6fc7a0-65b6-11e8-a41e-89578c45f386.jpg', NULL, NULL, '2018-06-01 10:12:18', '2018-06-01 10:18:05'),
(82, 20, 14, 1, 'images/item/8e6ebfb0-65b6-11e8-8a13-97fc846fb140.jpg', NULL, NULL, '2018-06-01 10:12:18', '2018-06-01 10:18:05'),
(81, NULL, NULL, NULL, '/images/item/9cade980-60d1-11e8-a164-7faea76aeda5.jpg', NULL, NULL, '2018-05-26 04:43:22', '2018-05-26 04:43:22'),
(80, NULL, NULL, NULL, '/images/item/b19e9340-5f2b-11e8-905f-05c733d8baac.jpg', NULL, NULL, '2018-05-24 02:23:10', '2018-05-24 02:23:10'),
(79, NULL, NULL, NULL, '/images/item/90a57b00-5f2b-11e8-a99e-1fef2cc3e6d1.jpg', NULL, NULL, '2018-05-24 02:22:14', '2018-05-24 02:22:14'),
(78, NULL, NULL, NULL, '/images/item/f4d5c4d0-5f2a-11e8-bedb-952c21900279.jpg', NULL, NULL, '2018-05-24 02:17:53', '2018-05-24 02:17:53'),
(77, NULL, NULL, NULL, '/images/item/eb2c6f20-5f2a-11e8-8eca-c941f719c2ba.jpg', NULL, NULL, '2018-05-24 02:17:37', '2018-05-24 02:17:37'),
(76, NULL, NULL, NULL, '/images/item/d47d3b10-5f2a-11e8-9f9b-6d76c9ef4849.jpg', NULL, NULL, '2018-05-24 02:16:59', '2018-05-24 02:16:59'),
(75, NULL, NULL, NULL, '/images/item/13ae6320-5f2a-11e8-adc5-5de8f3569e23.jpg', NULL, NULL, '2018-05-24 02:11:35', '2018-05-24 02:11:35'),
(74, NULL, NULL, NULL, '/images/item/05317e30-5f2a-11e8-a939-b771cb6d9f48.jpg', NULL, NULL, '2018-05-24 02:11:11', '2018-05-24 02:11:11'),
(73, NULL, NULL, NULL, '/images/item/ecc9b0f0-5f29-11e8-9d25-a7a8f6037375.jpg', NULL, NULL, '2018-05-24 02:10:30', '2018-05-24 02:10:30'),
(72, NULL, NULL, NULL, '/images/item/cfa3dfa0-5f29-11e8-97d3-f3ecd5e5765b.jpg', NULL, NULL, '2018-05-24 02:09:41', '2018-05-24 02:09:41'),
(71, 19, 23, 4, 'images/vendors/2/original/658a12e0-6e7a-11e8-8164-fdc42ecc17da.jpg', 'images/vendors/2/list/658a12e0-6e7a-11e8-8164-fdc42ecc17da.jpg', 'images/vendors/2/thumbs/658a12e0-6e7a-11e8-8164-fdc42ecc17da.jpg', '2018-05-23 15:30:42', '2018-06-12 13:54:20'),
(70, 19, 20, 3, 'images/vendors/2/original/657d9fe0-6e7a-11e8-9a1f-a7cfe1f86354.jpg', 'images/vendors/2/list/657d9fe0-6e7a-11e8-9a1f-a7cfe1f86354.jpg', 'images/vendors/2/thumbs/657d9fe0-6e7a-11e8-9a1f-a7cfe1f86354.jpg', '2018-05-23 15:30:38', '2018-06-12 13:54:20'),
(69, 19, 22, 2, 'images/vendors/2/original/65710690-6e7a-11e8-87bd-31d2050ff57d.jpg', 'images/vendors/2/list/65710690-6e7a-11e8-87bd-31d2050ff57d.jpg', 'images/vendors/2/thumbs/65710690-6e7a-11e8-87bd-31d2050ff57d.jpg', '2018-05-23 15:30:34', '2018-06-12 13:54:20'),
(68, 19, 22, 1, 'images/vendors/2/original/65642310-6e7a-11e8-8929-5d5a369e6790.jpg', 'images/vendors/2/list/65642310-6e7a-11e8-8929-5d5a369e6790.jpg', 'images/vendors/2/thumbs/65642310-6e7a-11e8-8929-5d5a369e6790.jpg', '2018-05-23 15:30:30', '2018-06-12 13:54:20'),
(67, 18, 21, 4, 'images/vendors/2/original/655790f0-6e7a-11e8-b86e-3b373010591e.jpg', 'images/vendors/2/list/655790f0-6e7a-11e8-b86e-3b373010591e.jpg', 'images/vendors/2/thumbs/655790f0-6e7a-11e8-b86e-3b373010591e.jpg', '2018-05-23 15:26:54', '2018-06-12 13:54:20'),
(66, 18, 19, 3, 'images/vendors/2/original/654af290-6e7a-11e8-aae2-47657153b3cf.jpg', 'images/vendors/2/list/654af290-6e7a-11e8-aae2-47657153b3cf.jpg', 'images/vendors/2/thumbs/654af290-6e7a-11e8-aae2-47657153b3cf.jpg', '2018-05-23 15:26:50', '2018-06-12 13:54:20'),
(65, 18, 20, 2, 'images/vendors/2/original/653e75a0-6e7a-11e8-98bb-c90d7f13d2a9.jpg', 'images/vendors/2/list/653e75a0-6e7a-11e8-98bb-c90d7f13d2a9.jpg', 'images/vendors/2/thumbs/653e75a0-6e7a-11e8-98bb-c90d7f13d2a9.jpg', '2018-05-23 15:26:46', '2018-06-12 13:54:19'),
(64, 18, 20, 1, 'images/vendors/2/original/65307a10-6e7a-11e8-b535-c313fe2af7f9.jpg', 'images/vendors/2/list/65307a10-6e7a-11e8-b535-c313fe2af7f9.jpg', 'images/vendors/2/thumbs/65307a10-6e7a-11e8-b535-c313fe2af7f9.jpg', '2018-05-23 15:26:41', '2018-06-12 13:54:19'),
(63, 17, NULL, 3, 'images/vendors/1/original/645d42e0-6e7a-11e8-80ae-bf2702de11e8.jpg', 'images/vendors/1/list/645d42e0-6e7a-11e8-80ae-bf2702de11e8.jpg', 'images/vendors/1/thumbs/645d42e0-6e7a-11e8-80ae-bf2702de11e8.jpg', '2018-05-23 04:30:15', '2018-06-28 12:40:23'),
(62, 17, 14, 2, 'images/vendors/1/original/6450c5d0-6e7a-11e8-b75c-03dfb2943d33.jpg', 'images/vendors/1/list/6450c5d0-6e7a-11e8-b75c-03dfb2943d33.jpg', 'images/vendors/1/thumbs/6450c5d0-6e7a-11e8-b75c-03dfb2943d33.jpg', '2018-05-23 04:30:11', '2018-06-28 12:40:23'),
(61, 17, 14, 1, 'images/vendors/1/original/6443df10-6e7a-11e8-a55a-55455946cdb6.jpg', 'images/vendors/1/list/6443df10-6e7a-11e8-a55a-55455946cdb6.jpg', 'images/vendors/1/thumbs/6443df10-6e7a-11e8-a55a-55455946cdb6.jpg', '2018-05-23 04:30:04', '2018-06-28 12:40:23'),
(60, 16, 18, 4, 'images/vendors/1/original/64a60700-6e7a-11e8-9184-61899ed87d63.jpg', 'images/vendors/1/list/64a60700-6e7a-11e8-9184-61899ed87d63.jpg', 'images/vendors/1/thumbs/64a60700-6e7a-11e8-9184-61899ed87d63.jpg', '2018-05-23 04:26:59', '2018-06-28 12:40:17'),
(59, 16, 6, 3, 'images/vendors/1/original/649892d0-6e7a-11e8-85e2-31495522d204.jpg', 'images/vendors/1/list/649892d0-6e7a-11e8-85e2-31495522d204.jpg', 'images/vendors/1/thumbs/649892d0-6e7a-11e8-85e2-31495522d204.jpg', '2018-05-23 04:26:55', '2018-06-28 12:40:17'),
(58, 16, 17, 2, 'images/vendors/1/original/648bead0-6e7a-11e8-9fba-876b01165acf.jpg', 'images/vendors/1/list/648bead0-6e7a-11e8-9fba-876b01165acf.jpg', 'images/vendors/1/thumbs/648bead0-6e7a-11e8-9fba-876b01165acf.jpg', '2018-05-23 04:26:51', '2018-06-28 12:40:17'),
(57, 16, 17, 1, 'images/vendors/1/original/647f59c0-6e7a-11e8-ab11-77767f7dd93a.jpg', 'images/vendors/1/list/647f59c0-6e7a-11e8-ab11-77767f7dd93a.jpg', 'images/vendors/1/thumbs/647f59c0-6e7a-11e8-ab11-77767f7dd93a.jpg', '2018-05-23 04:26:46', '2018-06-28 12:40:17'),
(56, 15, 4, 4, 'images/vendors/1/original/64d88460-6e7a-11e8-8e66-2bd9c7cce9b2.jpg', 'images/vendors/1/list/64d88460-6e7a-11e8-8e66-2bd9c7cce9b2.jpg', 'images/vendors/1/thumbs/64d88460-6e7a-11e8-8e66-2bd9c7cce9b2.jpg', '2018-05-23 04:23:14', '2018-06-12 13:54:19'),
(55, 15, 4, 3, 'images/vendors/1/original/64cbe900-6e7a-11e8-bbba-bdbb3e254ec1.jpg', 'images/vendors/1/list/64cbe900-6e7a-11e8-bbba-bdbb3e254ec1.jpg', 'images/vendors/1/thumbs/64cbe900-6e7a-11e8-bbba-bdbb3e254ec1.jpg', '2018-05-23 04:23:09', '2018-06-12 13:54:19'),
(54, 15, 17, 2, 'images/vendors/1/original/64bf3750-6e7a-11e8-8ce2-e7f8c126e9a6.jpg', 'images/vendors/1/list/64bf3750-6e7a-11e8-8ce2-e7f8c126e9a6.jpg', 'images/vendors/1/thumbs/64bf3750-6e7a-11e8-8ce2-e7f8c126e9a6.jpg', '2018-05-23 04:23:01', '2018-06-12 13:54:19'),
(53, 15, 17, 1, 'images/vendors/1/original/64b2b6a0-6e7a-11e8-a211-bd8b858b9b49.jpg', 'images/vendors/1/list/64b2b6a0-6e7a-11e8-a211-bd8b858b9b49.jpg', 'images/vendors/1/thumbs/64b2b6a0-6e7a-11e8-a211-bd8b858b9b49.jpg', '2018-05-23 04:22:52', '2018-06-12 13:54:19'),
(47, 14, 16, 1, 'images/vendors/1/original/64e50110-6e7a-11e8-8197-5f25d5cc7b76.jpg', 'images/vendors/1/list/64e50110-6e7a-11e8-8197-5f25d5cc7b76.jpg', 'images/vendors/1/thumbs/64e50110-6e7a-11e8-8197-5f25d5cc7b76.jpg', '2018-05-23 04:17:46', '2018-06-23 07:49:47'),
(48, 14, 16, 2, 'images/vendors/1/original/64f192c0-6e7a-11e8-b5fe-e385bf351528.jpg', 'images/vendors/1/list/64f192c0-6e7a-11e8-b5fe-e385bf351528.jpg', 'images/vendors/1/thumbs/64f192c0-6e7a-11e8-b5fe-e385bf351528.jpg', '2018-05-23 04:17:51', '2018-06-23 07:49:47'),
(49, 14, 17, 3, 'images/vendors/1/original/64fe1d40-6e7a-11e8-904c-a1973ce911c3.jpg', 'images/vendors/1/list/64fe1d40-6e7a-11e8-904c-a1973ce911c3.jpg', 'images/vendors/1/thumbs/64fe1d40-6e7a-11e8-904c-a1973ce911c3.jpg', '2018-05-23 04:17:56', '2018-06-23 07:49:47'),
(50, 14, 17, 4, 'images/vendors/1/original/650a9fa0-6e7a-11e8-a020-d15cb220bd8d.jpg', 'images/vendors/1/list/650a9fa0-6e7a-11e8-a020-d15cb220bd8d.jpg', 'images/vendors/1/thumbs/650a9fa0-6e7a-11e8-a020-d15cb220bd8d.jpg', '2018-05-23 04:18:00', '2018-06-23 07:49:47'),
(51, 14, 15, 5, 'images/vendors/1/original/65170a50-6e7a-11e8-a474-9579f7f3a771.jpg', 'images/vendors/1/list/65170a50-6e7a-11e8-a474-9579f7f3a771.jpg', 'images/vendors/1/thumbs/65170a50-6e7a-11e8-a474-9579f7f3a771.jpg', '2018-05-23 04:18:05', '2018-06-23 07:49:47'),
(52, 14, 15, 6, 'images/vendors/1/original/65239020-6e7a-11e8-a7de-2355368bba87.jpg', 'images/vendors/1/list/65239020-6e7a-11e8-a7de-2355368bba87.jpg', 'images/vendors/1/thumbs/65239020-6e7a-11e8-a7de-2355368bba87.jpg', '2018-05-23 04:18:09', '2018-06-23 07:49:47'),
(86, 21, 14, 1, 'images/item/28f7f520-65b7-11e8-85c2-67aed8ec1ccb.jpg', NULL, NULL, '2018-06-01 10:16:37', '2018-06-01 10:16:37'),
(87, 21, 14, 2, 'images/item/28f95c20-65b7-11e8-a0e6-215e9b3ba34c.jpg', NULL, NULL, '2018-06-01 10:16:37', '2018-06-01 10:16:37'),
(88, 21, NULL, 3, 'images/item/28faa0e0-65b7-11e8-9413-4589fca537b3.jpg', NULL, NULL, '2018-06-01 10:16:37', '2018-06-01 10:16:37'),
(89, 20, 14, 4, '/images/item/5c4e6110-65b7-11e8-9739-31a16cad6719.jpg', NULL, NULL, '2018-06-01 10:18:03', '2018-06-01 10:18:05'),
(90, NULL, NULL, NULL, '/images/item/729681c0-65b7-11e8-9419-a93b5f5c3ae4.jpg', NULL, NULL, '2018-06-01 10:18:41', '2018-06-01 10:18:41'),
(91, 23, 14, 1, 'images/item/906c6740-65b7-11e8-9e0e-0bae476013c2.jpg', NULL, NULL, '2018-06-01 10:19:31', '2018-06-01 10:19:31'),
(92, 23, 14, 2, 'images/item/906df000-65b7-11e8-a0a2-5f30aa98057b.jpg', NULL, NULL, '2018-06-01 10:19:31', '2018-06-01 10:19:31'),
(93, 23, NULL, 3, 'images/item/906f0070-65b7-11e8-99b4-15756f86b90e.jpg', NULL, NULL, '2018-06-01 10:19:31', '2018-06-01 10:19:31'),
(94, NULL, NULL, NULL, '/images/item/ff4b0970-65b7-11e8-8298-31663d3eac6b.jpg', NULL, NULL, '2018-06-01 10:22:37', '2018-06-01 10:22:37'),
(96, 24, 14, 4, '/images/item/53a9b6a0-65b8-11e8-bd7c-b78ccaab8d3e.jpg', NULL, NULL, '2018-06-01 10:24:58', '2018-06-01 10:25:01'),
(97, 24, 14, 1, 'images/item/5547a400-65b8-11e8-9220-cd7a1be59c8d.jpg', NULL, NULL, '2018-06-01 10:25:01', '2018-06-01 10:25:01'),
(98, 24, 14, 2, 'images/item/5548de70-65b8-11e8-a3da-ef4b66ea7890.jpg', NULL, NULL, '2018-06-01 10:25:01', '2018-06-01 10:25:01'),
(99, 24, NULL, 3, 'images/item/5549f3e0-65b8-11e8-8754-e50105339a48.jpg', NULL, NULL, '2018-06-01 10:25:01', '2018-06-01 10:25:01'),
(103, 30, NULL, 1, 'images/item/b4600c00-68ee-11e8-b8fc-af13b832e9a2.jpg', NULL, NULL, '2018-06-05 12:31:49', '2018-06-05 12:31:49'),
(102, 29, NULL, 1, 'images/item/b2b1a680-68ee-11e8-b8c7-97aab8cd82fd.jpg', NULL, NULL, '2018-06-05 12:31:47', '2018-06-05 12:31:47'),
(104, 31, NULL, 1, 'images/item/05866bf0-68ef-11e8-8298-5b9568207d66.jpg', NULL, NULL, '2018-06-05 12:34:05', '2018-06-05 12:34:05'),
(105, 32, NULL, 1, 'images/item/06e06b10-68ef-11e8-899f-85b4be5a7797.jpg', NULL, NULL, '2018-06-05 12:34:07', '2018-06-05 12:34:07'),
(106, 33, NULL, 1, 'images/item/20f55920-68ef-11e8-b311-774305919625.jpg', NULL, NULL, '2018-06-05 12:34:51', '2018-06-05 12:34:51'),
(107, 34, NULL, 1, 'images/item/22559500-68ef-11e8-9adb-0f13ae0900b9.jpg', NULL, NULL, '2018-06-05 12:34:53', '2018-06-05 12:34:53'),
(108, 37, NULL, 1, 'images/item/3486ef60-6903-11e8-9c48-fd89e22e9a0e.jpg', NULL, NULL, '2018-06-05 14:58:33', '2018-06-05 14:58:33'),
(109, 38, NULL, 1, 'images/item/358788d0-6903-11e8-af07-5b3c0e0cd825.jpg', NULL, NULL, '2018-06-05 14:58:35', '2018-06-05 14:58:35'),
(110, 39, NULL, 1, 'images/item/94b1baf0-6903-11e8-94d5-25e5c37a654d.jpg', NULL, NULL, '2018-06-05 15:01:15', '2018-06-05 15:01:15'),
(111, 40, NULL, 1, 'images/item/95ab7170-6903-11e8-8e63-350955c928d4.jpg', NULL, NULL, '2018-06-05 15:01:18', '2018-06-05 15:01:18'),
(112, 41, NULL, 1, 'images/item/b939c510-6903-11e8-b02a-a1c9cb9c6f75.jpg', NULL, NULL, '2018-06-05 15:02:16', '2018-06-05 15:02:16'),
(113, 42, NULL, 1, 'images/item/ba587fa0-6903-11e8-8bae-b363a4b3bbcc.jpg', NULL, NULL, '2018-06-05 15:02:18', '2018-06-05 15:02:18'),
(114, NULL, NULL, NULL, '/images/item/f7f27750-6e6c-11e8-b381-f7d56cd43d37.jpg', NULL, NULL, '2018-06-12 12:18:13', '2018-06-12 12:18:13'),
(115, NULL, NULL, NULL, '/images/item/fb2f31d0-6e6c-11e8-80d9-b7692efd51ae.jpg', NULL, NULL, '2018-06-12 12:18:18', '2018-06-12 12:18:18'),
(116, NULL, NULL, NULL, '/images/item/ee201020-6e6e-11e8-a809-ef05fdd5e6d7.jpg', NULL, NULL, '2018-06-12 12:32:15', '2018-06-12 12:32:15'),
(117, NULL, NULL, NULL, '/images/item/ee200ba0-6e6e-11e8-a678-2b565252e00f.jpg', NULL, NULL, '2018-06-12 12:32:15', '2018-06-12 12:32:15'),
(118, 45, 4, 1, 'images/vendors/1/original/d04f2de0-6e72-11e8-8264-ab0d3b3eec1b.jpg', 'images/vendors/1/list/d04f2de0-6e72-11e8-8264-ab0d3b3eec1b.jpg', NULL, '2018-06-12 12:59:45', '2018-06-12 13:00:03'),
(119, 45, 6, 2, 'images/vendors/1/original/d05677f0-6e72-11e8-8b57-df1cf08ff960.jpg', 'images/vendors/1/list/d05677f0-6e72-11e8-8b57-df1cf08ff960.jpg', NULL, '2018-06-12 12:59:45', '2018-06-12 13:00:03'),
(120, 46, 5, 1, 'images/vendors/1/original/721d4740-6e74-11e8-a2ba-fff0e1688b3d.jpg', 'images/vendors/1/list/721d4740-6e74-11e8-a2ba-fff0e1688b3d.jpg', 'images/vendors/1/thumbs/721d4740-6e74-11e8-a2ba-fff0e1688b3d.jpg', '2018-06-12 13:11:19', '2018-06-12 13:11:44'),
(121, 46, 6, 2, 'images/vendors/1/original/7229dfc0-6e74-11e8-afe4-375cb4160061.jpg', 'images/vendors/1/list/7229dfc0-6e74-11e8-afe4-375cb4160061.jpg', 'images/vendors/1/thumbs/7229dfc0-6e74-11e8-afe4-375cb4160061.jpg', '2018-06-12 13:11:19', '2018-06-12 13:11:44'),
(122, 48, 5, 1, 'images/vendors/1/original/bbc13670-6e75-11e8-a981-e5aafe29b370.jpg', 'images/vendors/1/list/bbc13670-6e75-11e8-a981-e5aafe29b370.jpg', 'images/vendors/1/thumbs/bbc13670-6e75-11e8-a981-e5aafe29b370.jpg', '2018-06-12 13:20:57', '2018-06-12 13:20:57'),
(123, 48, 6, 2, 'images/vendors/1/original/bbceeae0-6e75-11e8-88fe-c91114b20b3f.jpg', 'images/vendors/1/list/bbceeae0-6e75-11e8-88fe-c91114b20b3f.jpg', 'images/vendors/1/thumbs/bbceeae0-6e75-11e8-88fe-c91114b20b3f.jpg', '2018-06-12 13:20:57', '2018-06-12 13:20:57'),
(124, NULL, NULL, NULL, '/images/item/d78fe190-6e75-11e8-a8b3-5f6eb9f1d6fc.jpg', NULL, NULL, '2018-06-12 13:21:44', '2018-06-12 13:21:44'),
(131, 61, NULL, 1, 'images/vendors/1/original/f96631b0-760c-11e8-98ac-c1d407388522.jpg', 'images/vendors/1/list/f96631b0-760c-11e8-98ac-c1d407388522.jpg', 'images/vendors/1/thumbs/f96631b0-760c-11e8-98ac-c1d407388522.jpg', '2018-06-22 05:11:18', '2018-06-22 05:11:18'),
(130, 57, NULL, 1, 'images/vendors/1/original/3dc8c5e0-6ee5-11e8-ad8d-d9d568f4df2d.jpg', 'images/vendors/1/list/3dc8c5e0-6ee5-11e8-ad8d-d9d568f4df2d.jpg', 'images/vendors/1/thumbs/3dc8c5e0-6ee5-11e8-ad8d-d9d568f4df2d.jpg', '2018-06-13 02:39:14', '2018-06-13 02:39:14'),
(132, 62, NULL, 1, 'images/vendors/1/original/b62a79f0-76f6-11e8-8f66-b144971abb4c.jpg', 'images/vendors/1/list/b62a79f0-76f6-11e8-8f66-b144971abb4c.jpg', 'images/vendors/1/thumbs/b62a79f0-76f6-11e8-8f66-b144971abb4c.jpg', '2018-06-23 09:04:28', '2018-06-23 09:04:28'),
(133, 63, NULL, 1, 'images/vendors/1/original/c203fc80-76f8-11e8-a89a-1d253cbd0bf9.jpg', 'images/vendors/1/list/c203fc80-76f8-11e8-a89a-1d253cbd0bf9.jpg', 'images/vendors/1/thumbs/c203fc80-76f8-11e8-a89a-1d253cbd0bf9.jpg', '2018-06-23 09:19:07', '2018-06-23 09:19:07'),
(134, 64, NULL, 1, 'images/vendors/1/original/d7d85640-76f8-11e8-ba36-dbe346e8c2d1.jpg', 'images/vendors/1/list/d7d85640-76f8-11e8-ba36-dbe346e8c2d1.jpg', 'images/vendors/1/thumbs/d7d85640-76f8-11e8-ba36-dbe346e8c2d1.jpg', '2018-06-23 09:19:43', '2018-06-23 09:19:43'),
(135, 65, NULL, 1, 'images/vendors/1/original/062414d0-7714-11e8-a3bf-b161c1a7f8f9.jpg', 'images/vendors/1/list/062414d0-7714-11e8-a3bf-b161c1a7f8f9.jpg', 'images/vendors/1/thumbs/062414d0-7714-11e8-a3bf-b161c1a7f8f9.jpg', '2018-06-23 12:34:19', '2018-06-23 12:34:19'),
(136, NULL, NULL, NULL, '/images/item/84b15560-79ee-11e8-8ac7-8708d1d06854.jpg', NULL, NULL, '2018-06-27 03:43:17', '2018-06-27 03:43:17'),
(137, NULL, NULL, NULL, '/images/item/a7058ef0-79ee-11e8-821b-e99dc76b2fc8.jpg', NULL, NULL, '2018-06-27 03:44:14', '2018-06-27 03:44:14'),
(138, NULL, NULL, NULL, '/images/item/2c03aa60-79ef-11e8-b4ce-a7c66cf251d1.jpg', NULL, NULL, '2018-06-27 03:47:57', '2018-06-27 03:47:57'),
(139, 66, NULL, 1, 'images/vendors/1/original/d36d2160-7a35-11e8-b8ef-9fde2340124d.jpg', 'images/vendors/1/list/d36d2160-7a35-11e8-b8ef-9fde2340124d.jpg', 'images/vendors/1/thumbs/d36d2160-7a35-11e8-b8ef-9fde2340124d.jpg', '2018-06-27 12:13:59', '2018-06-27 12:13:59'),
(140, 66, NULL, 2, 'images/vendors/1/original/dd10b910-7a35-11e8-bb69-8b13d1a30df3.jpg', 'images/vendors/1/list/dd10b910-7a35-11e8-bb69-8b13d1a30df3.jpg', 'images/vendors/1/thumbs/dd10b910-7a35-11e8-bb69-8b13d1a30df3.jpg', '2018-06-27 12:14:16', '2018-06-27 12:14:16'),
(141, 66, NULL, 3, 'images/vendors/1/original/e7468200-7a35-11e8-b1ca-59d34869f088.jpg', 'images/vendors/1/list/e7468200-7a35-11e8-b1ca-59d34869f088.jpg', 'images/vendors/1/thumbs/e7468200-7a35-11e8-b1ca-59d34869f088.jpg', '2018-06-27 12:14:33', '2018-06-27 12:14:33'),
(142, 66, NULL, 4, 'images/vendors/1/original/f192e100-7a35-11e8-b79e-23c1cc395651.jpg', 'images/vendors/1/list/f192e100-7a35-11e8-b79e-23c1cc395651.jpg', 'images/vendors/1/thumbs/f192e100-7a35-11e8-b79e-23c1cc395651.jpg', '2018-06-27 12:14:52', '2018-06-27 12:14:52'),
(143, 67, NULL, 1, 'images/vendors/1/original/f8cb6f40-7a35-11e8-8f20-b1974ae98069.jpg', 'images/vendors/1/list/f8cb6f40-7a35-11e8-8f20-b1974ae98069.jpg', 'images/vendors/1/thumbs/f8cb6f40-7a35-11e8-8f20-b1974ae98069.jpg', '2018-06-27 12:15:03', '2018-07-04 02:11:42'),
(144, 66, NULL, 5, 'images/vendors/1/original/fc895ba0-7a35-11e8-8889-8fe3d6e63869.jpg', 'images/vendors/1/list/fc895ba0-7a35-11e8-8889-8fe3d6e63869.jpg', 'images/vendors/1/thumbs/fc895ba0-7a35-11e8-8889-8fe3d6e63869.jpg', '2018-06-27 12:15:08', '2018-06-27 12:15:08'),
(145, 67, NULL, 2, 'images/vendors/1/original/030e7290-7a36-11e8-8c7f-5104b0404da6.jpg', 'images/vendors/1/list/030e7290-7a36-11e8-8c7f-5104b0404da6.jpg', 'images/vendors/1/thumbs/030e7290-7a36-11e8-8c7f-5104b0404da6.jpg', '2018-06-27 12:15:23', '2018-07-04 02:11:42'),
(146, 66, NULL, 6, 'images/vendors/1/original/061bb6c0-7a36-11e8-bf09-6380b91bd614.jpg', 'images/vendors/1/list/061bb6c0-7a36-11e8-bf09-6380b91bd614.jpg', 'images/vendors/1/thumbs/061bb6c0-7a36-11e8-bf09-6380b91bd614.jpg', '2018-06-27 12:15:28', '2018-06-27 12:15:28'),
(147, 67, NULL, 3, 'images/vendors/1/original/0f065d70-7a36-11e8-93a4-8531d08fa95b.jpg', 'images/vendors/1/list/0f065d70-7a36-11e8-93a4-8531d08fa95b.jpg', 'images/vendors/1/thumbs/0f065d70-7a36-11e8-93a4-8531d08fa95b.jpg', '2018-06-27 12:15:41', '2018-07-04 02:11:42'),
(148, 66, NULL, 7, 'images/vendors/1/original/11ddfb60-7a36-11e8-bde7-6128f73dc155.jpg', 'images/vendors/1/list/11ddfb60-7a36-11e8-bde7-6128f73dc155.jpg', 'images/vendors/1/thumbs/11ddfb60-7a36-11e8-bde7-6128f73dc155.jpg', '2018-06-27 12:15:46', '2018-06-27 12:15:46'),
(149, 67, NULL, 4, 'images/vendors/1/original/19bf7ff0-7a36-11e8-8da3-a9ec7fc13182.jpg', 'images/vendors/1/list/19bf7ff0-7a36-11e8-8da3-a9ec7fc13182.jpg', 'images/vendors/1/thumbs/19bf7ff0-7a36-11e8-8da3-a9ec7fc13182.jpg', '2018-06-27 12:15:57', '2018-07-04 02:11:42'),
(150, 67, NULL, 5, 'images/vendors/1/original/23a58d80-7a36-11e8-9f46-9791b6231f5b.jpg', 'images/vendors/1/list/23a58d80-7a36-11e8-9f46-9791b6231f5b.jpg', 'images/vendors/1/thumbs/23a58d80-7a36-11e8-9f46-9791b6231f5b.jpg', '2018-06-27 12:16:16', '2018-07-04 02:11:42'),
(151, 67, NULL, 6, 'images/vendors/1/original/2ed44e00-7a36-11e8-9805-2b83dfab14be.jpg', 'images/vendors/1/list/2ed44e00-7a36-11e8-9805-2b83dfab14be.jpg', 'images/vendors/1/thumbs/2ed44e00-7a36-11e8-9805-2b83dfab14be.jpg', '2018-06-27 12:16:34', '2018-07-04 02:11:42'),
(168, 73, NULL, 1, '/images/item/9a5f3740-7f73-11e8-849e-91f8b1eb6ed6.jpg', NULL, NULL, '2018-07-04 04:18:32', '2018-07-04 04:18:57'),
(166, 72, NULL, 1, 'images/vendors/1/original/88079e20-7b8e-11e8-af81-653342ae9403.jpg', 'images/vendors/1/list/88079e20-7b8e-11e8-af81-653342ae9403.jpg', 'images/vendors/1/thumbs/88079e20-7b8e-11e8-af81-653342ae9403.jpg', '2018-06-29 05:21:18', '2018-06-29 05:21:18'),
(176, 76, NULL, 1, 'images/vendors/1/original/25de4b00-7f7a-11e8-8930-b780e79d51a3.jpg', 'images/vendors/1/list/25de4b00-7f7a-11e8-8930-b780e79d51a3.jpg', 'images/vendors/1/thumbs/25de4b00-7f7a-11e8-8930-b780e79d51a3.jpg', '2018-07-04 05:05:19', '2018-07-04 05:05:23'),
(175, 75, NULL, 1, 'images/vendors/1/original/2123d210-7f78-11e8-ad46-d158b0b4e8b2.jpg', 'images/vendors/1/list/2123d210-7f78-11e8-ad46-d158b0b4e8b2.jpg', 'images/vendors/1/thumbs/2123d210-7f78-11e8-ad46-d158b0b4e8b2.jpg', '2018-07-04 04:50:53', '2018-07-04 04:50:56'),
(157, NULL, NULL, NULL, '/images/item/16782790-7aeb-11e8-bd67-d73c4ff77f42.PNG', NULL, NULL, '2018-06-28 09:51:14', '2018-06-28 09:51:14'),
(158, NULL, NULL, NULL, '/images/item/345b82b0-7aeb-11e8-b432-816175436b58.PNG', NULL, NULL, '2018-06-28 09:52:04', '2018-06-28 09:52:04'),
(159, NULL, NULL, NULL, '/images/item/4e3f3300-7aeb-11e8-b2d9-6d06b4d40a24.PNG', NULL, NULL, '2018-06-28 09:52:48', '2018-06-28 09:52:48'),
(160, NULL, NULL, NULL, '/images/item/68e10a70-7aeb-11e8-90e3-33d9cf6f7315.PNG', NULL, NULL, '2018-06-28 09:53:33', '2018-06-28 09:53:33'),
(161, NULL, NULL, NULL, '/images/item/973320c0-7aeb-11e8-8e59-032e9d8592c8.PNG', NULL, NULL, '2018-06-28 09:54:50', '2018-06-28 09:54:50'),
(162, NULL, NULL, NULL, '/images/item/608b5160-7aec-11e8-8fb8-4b067bfc78a9.PNG', NULL, NULL, '2018-06-28 10:00:28', '2018-06-28 10:00:28'),
(163, NULL, NULL, NULL, '/images/item/94f9c520-7aec-11e8-bf14-b97fee4d7c1b.PNG', NULL, NULL, '2018-06-28 10:01:56', '2018-06-28 10:01:56'),
(164, 69, 4, 1, 'images/vendors/1/original/fc9fc7f0-7aec-11e8-8482-d9cd63201dd5.PNG', 'images/vendors/1/list/fc9fc7f0-7aec-11e8-8482-d9cd63201dd5.PNG', 'images/vendors/1/thumbs/fc9fc7f0-7aec-11e8-8482-d9cd63201dd5.PNG', '2018-06-28 10:04:46', '2018-06-28 10:59:44'),
(173, 74, NULL, 1, 'images/vendors/1/original/142639a0-7f78-11e8-a3a7-17f71508337e.jpg', 'images/vendors/1/list/142639a0-7f78-11e8-a3a7-17f71508337e.jpg', 'images/vendors/1/thumbs/142639a0-7f78-11e8-a3a7-17f71508337e.jpg', '2018-07-04 04:50:33', '2018-07-04 04:50:34'),
(177, 79, NULL, 1, 'images/vendors/1/original/ddaac2b0-8440-11e8-8b6a-6b486d9822c2.jpg', 'images/vendors/1/list/ddaac2b0-8440-11e8-8b6a-6b486d9822c2.jpg', 'images/vendors/1/thumbs/ddaac2b0-8440-11e8-8b6a-6b486d9822c2.jpg', '2018-07-10 06:58:01', '2018-07-10 06:58:01'),
(178, 80, NULL, 1, 'images/vendors/1/original/0a6f4b00-8441-11e8-8ab4-3d650295a243.jpg', 'images/vendors/1/list/0a6f4b00-8441-11e8-8ab4-3d650295a243.jpg', 'images/vendors/1/thumbs/0a6f4b00-8441-11e8-8ab4-3d650295a243.jpg', '2018-07-10 06:59:15', '2018-07-10 06:59:15'),
(179, 81, NULL, 1, 'images/vendors/1/original/04514ee0-844a-11e8-8ece-83d7a943804a.jpg', 'images/vendors/1/list/04514ee0-844a-11e8-8ece-83d7a943804a.jpg', 'images/vendors/1/thumbs/04514ee0-844a-11e8-8ece-83d7a943804a.jpg', '2018-07-10 08:03:31', '2018-07-10 08:03:31'),
(180, NULL, NULL, NULL, '/images/item/81f0c230-8a66-11e8-8dfd-3bb6b1c7455d.jpg', NULL, NULL, '2018-07-17 19:42:30', '2018-07-17 19:42:30'),
(181, NULL, NULL, NULL, '/images/item/85fe9920-8a66-11e8-bb94-8daf42f975e6.jpg', NULL, NULL, '2018-07-17 19:42:37', '2018-07-17 19:42:37'),
(182, NULL, NULL, NULL, '/images/item/88ea32b0-8a66-11e8-87ed-7b7b47e21984.jpg', NULL, NULL, '2018-07-17 19:42:42', '2018-07-17 19:42:42'),
(183, NULL, NULL, NULL, '/images/item/964c70b0-8a66-11e8-a177-4be9296532ab.jpg', NULL, NULL, '2018-07-17 19:43:04', '2018-07-17 19:43:04'),
(184, NULL, NULL, NULL, '/images/item/964c78b0-8a66-11e8-9bc9-19d72149ae95.jpg', NULL, NULL, '2018-07-17 19:43:04', '2018-07-17 19:43:04'),
(185, NULL, NULL, NULL, '/images/item/964c78d0-8a66-11e8-99e5-93af5d540cbe.jpg', NULL, NULL, '2018-07-17 19:43:04', '2018-07-17 19:43:04'),
(186, NULL, NULL, NULL, '/images/item/4e716fe0-8a67-11e8-bf06-1fe39459c639.jpg', NULL, NULL, '2018-07-17 19:48:13', '2018-07-17 19:48:13'),
(187, NULL, NULL, NULL, '/images/item/4f72c5c0-8a67-11e8-850c-cf8816ca0e15.jpg', NULL, NULL, '2018-07-17 19:48:15', '2018-07-17 19:48:15'),
(188, NULL, NULL, NULL, '/images/item/564a4600-8a67-11e8-b328-afd4669777f5.jpg', NULL, NULL, '2018-07-17 19:48:26', '2018-07-17 19:48:26'),
(189, NULL, NULL, NULL, '/images/item/b3e70360-8a67-11e8-9b63-69a73335cc92.jpg', NULL, NULL, '2018-07-17 19:51:03', '2018-07-17 19:51:03'),
(190, NULL, NULL, NULL, '/images/item/bca866d0-8a67-11e8-8571-0f07a318ddc3.jpg', NULL, NULL, '2018-07-17 19:51:18', '2018-07-17 19:51:18'),
(191, NULL, NULL, NULL, '/images/item/c8932f70-8a67-11e8-a0a4-d5169fc9bf85.jpg', NULL, NULL, '2018-07-17 19:51:38', '2018-07-17 19:51:38'),
(192, NULL, NULL, NULL, '/images/item/cc9e8640-8a67-11e8-8dcb-4d71b6649536.jpg', NULL, NULL, '2018-07-17 19:51:45', '2018-07-17 19:51:45'),
(193, NULL, NULL, NULL, '/images/item/d69ca280-8a67-11e8-971d-7177d9597821.jpg', NULL, NULL, '2018-07-17 19:52:02', '2018-07-17 19:52:02'),
(194, NULL, NULL, NULL, '/images/item/d69ca260-8a67-11e8-9b30-85e6b9ef0041.jpg', NULL, NULL, '2018-07-17 19:52:02', '2018-07-17 19:52:02'),
(195, NULL, NULL, NULL, '/images/item/d69ca330-8a67-11e8-88b7-dd5d727a569a.jpg', NULL, NULL, '2018-07-17 19:52:02', '2018-07-17 19:52:02'),
(196, NULL, NULL, NULL, '/images/item/d987ae30-8a67-11e8-a91c-75ae641d4c6d.jpg', NULL, NULL, '2018-07-17 19:52:07', '2018-07-17 19:52:07'),
(197, NULL, NULL, NULL, '/images/item/06c13a00-8a68-11e8-82f2-3310f3f3b643.jpg', NULL, NULL, '2018-07-17 19:53:22', '2018-07-17 19:53:22'),
(198, NULL, NULL, NULL, '/images/item/09e82d30-8a68-11e8-b656-556a32d0db71.jpg', NULL, NULL, '2018-07-17 19:53:28', '2018-07-17 19:53:28'),
(199, 82, NULL, 1, 'images/vendors/1/original/281260f0-8a68-11e8-b03e-add508ed63fb.jpg', 'images/vendors/1/list/281260f0-8a68-11e8-b03e-add508ed63fb.jpg', 'images/vendors/1/thumbs/281260f0-8a68-11e8-b03e-add508ed63fb.jpg', '2018-07-17 19:53:55', '2018-07-17 19:54:19'),
(200, NULL, NULL, NULL, '/images/item/372cdf00-8a68-11e8-b008-ab57ad4ea50e.jpg', NULL, NULL, '2018-07-17 19:54:44', '2018-07-17 19:54:44'),
(201, NULL, NULL, NULL, '/images/item/4180b410-8a68-11e8-9897-37316f8dde81.jpg', NULL, NULL, '2018-07-17 19:55:01', '2018-07-17 19:55:01'),
(202, NULL, NULL, NULL, '/images/item/456966f0-8a68-11e8-89d9-d1cb1ca15bcf.jpg', NULL, NULL, '2018-07-17 19:55:08', '2018-07-17 19:55:08'),
(203, NULL, NULL, NULL, '/images/item/4cc041e0-8a68-11e8-995a-7140e7c6d11b.jpg', NULL, NULL, '2018-07-17 19:55:20', '2018-07-17 19:55:20'),
(204, 82, NULL, 2, 'images/vendors/1/original/a1d4f500-8a68-11e8-9cf9-a9923ab7e6f5.jpg', 'images/vendors/1/list/a1d4f500-8a68-11e8-9cf9-a9923ab7e6f5.jpg', 'images/vendors/1/thumbs/a1d4f500-8a68-11e8-9cf9-a9923ab7e6f5.jpg', '2018-07-17 19:57:40', '2018-07-17 19:57:43'),
(205, 83, NULL, 3, 'images/vendors/1/original/b7e03270-8a68-11e8-a2fe-1977130c7805.jpg', 'images/vendors/1/list/b7e03270-8a68-11e8-a2fe-1977130c7805.jpg', 'images/vendors/1/thumbs/b7e03270-8a68-11e8-a2fe-1977130c7805.jpg', '2018-07-17 19:58:14', '2018-07-17 19:58:20'),
(206, 83, NULL, 1, 'images/vendors/1/original/b7cd05d0-8a68-11e8-a8e6-77c2c560b44c.jpg', 'images/vendors/1/list/b7cd05d0-8a68-11e8-a8e6-77c2c560b44c.jpg', 'images/vendors/1/thumbs/b7cd05d0-8a68-11e8-a8e6-77c2c560b44c.jpg', '2018-07-17 19:58:20', '2018-07-17 19:58:20'),
(207, 83, NULL, 2, 'images/vendors/1/original/b7d74e60-8a68-11e8-a952-83b772e74de2.jpg', 'images/vendors/1/list/b7d74e60-8a68-11e8-a952-83b772e74de2.jpg', 'images/vendors/1/thumbs/b7d74e60-8a68-11e8-a952-83b772e74de2.jpg', '2018-07-17 19:58:20', '2018-07-17 19:58:20'),
(208, 84, NULL, 1, 'images/vendors/1/original/e888e700-8a6c-11e8-bb45-a3032ac63ebb.jpg', 'images/vendors/1/list/e888e700-8a6c-11e8-bb45-a3032ac63ebb.jpg', 'images/vendors/1/thumbs/e888e700-8a6c-11e8-bb45-a3032ac63ebb.jpg', '2018-07-17 20:28:24', '2018-07-17 20:28:24'),
(209, 85, NULL, 1, 'images/vendors/1/original/93e10690-8a6d-11e8-b898-0b91208aa23f.jpg', 'images/vendors/1/list/93e10690-8a6d-11e8-b898-0b91208aa23f.jpg', 'images/vendors/1/thumbs/93e10690-8a6d-11e8-b898-0b91208aa23f.jpg', '2018-07-17 20:33:11', '2018-07-17 20:33:11'),
(210, 86, NULL, 1, 'images/vendors/1/original/a0e63190-8a6e-11e8-a302-01383eb1edc9.jpg', 'images/vendors/1/list/a0e63190-8a6e-11e8-a302-01383eb1edc9.jpg', 'images/vendors/1/thumbs/a0e63190-8a6e-11e8-a302-01383eb1edc9.jpg', '2018-07-17 20:40:43', '2018-07-17 20:40:43'),
(211, 87, NULL, 1, 'images/vendors/1/original/a1e0bd10-8a6f-11e8-bc96-a5cccc250fef.jpg', 'images/vendors/1/list/a1e0bd10-8a6f-11e8-bc96-a5cccc250fef.jpg', 'images/vendors/1/thumbs/a1e0bd10-8a6f-11e8-bc96-a5cccc250fef.jpg', '2018-07-17 20:47:53', '2018-07-17 20:47:53'),
(212, 87, NULL, 2, 'images/vendors/1/original/a45eaaa0-8a6f-11e8-b1ce-ebdad6b7bddb.jpg', 'images/vendors/1/list/a45eaaa0-8a6f-11e8-b1ce-ebdad6b7bddb.jpg', 'images/vendors/1/thumbs/a45eaaa0-8a6f-11e8-b1ce-ebdad6b7bddb.jpg', '2018-07-17 20:47:57', '2018-07-17 20:47:57'),
(213, 88, NULL, 1, 'images/vendors/1/original/145cc740-8a7c-11e8-8376-c9115951e6dd.jpg', 'images/vendors/1/list/145cc740-8a7c-11e8-8376-c9115951e6dd.jpg', 'images/vendors/1/thumbs/145cc740-8a7c-11e8-8376-c9115951e6dd.jpg', '2018-07-17 22:17:00', '2018-07-17 22:17:00'),
(214, 88, NULL, 2, 'images/vendors/1/original/170157c0-8a7c-11e8-a8b4-7fbd4ce8f8ae.jpg', 'images/vendors/1/list/170157c0-8a7c-11e8-a8b4-7fbd4ce8f8ae.jpg', 'images/vendors/1/thumbs/170157c0-8a7c-11e8-a8b4-7fbd4ce8f8ae.jpg', '2018-07-17 22:17:04', '2018-07-17 22:17:04'),
(218, 89, NULL, 2, 'images/vendors/1/original/f8a9c3d0-8a80-11e8-bd45-47cb94dbb165.jpg', 'images/vendors/1/list/f8a9c3d0-8a80-11e8-bd45-47cb94dbb165.jpg', 'images/vendors/1/thumbs/f8a9c3d0-8a80-11e8-bd45-47cb94dbb165.jpg', '2018-07-17 22:52:00', '2018-07-17 22:52:00'),
(217, 89, NULL, 1, 'images/vendors/1/original/f595fcb0-8a80-11e8-aa7e-97eeb60782ab.jpg', 'images/vendors/1/list/f595fcb0-8a80-11e8-aa7e-97eeb60782ab.jpg', 'images/vendors/1/thumbs/f595fcb0-8a80-11e8-aa7e-97eeb60782ab.jpg', '2018-07-17 22:51:56', '2018-07-17 22:51:56'),
(219, 90, 44, 1, 'images/vendors/1/original/61d18e40-8a84-11e8-be5f-0d874ee1c8a1.jpg', 'images/vendors/1/list/61d18e40-8a84-11e8-be5f-0d874ee1c8a1.jpg', 'images/vendors/1/thumbs/61d18e40-8a84-11e8-be5f-0d874ee1c8a1.jpg', '2018-07-17 23:16:25', '2018-07-17 23:16:25'),
(220, 90, 17, 2, 'images/vendors/1/original/644bcf20-8a84-11e8-a4da-e152880749c3.jpg', 'images/vendors/1/list/644bcf20-8a84-11e8-a4da-e152880749c3.jpg', 'images/vendors/1/thumbs/644bcf20-8a84-11e8-a4da-e152880749c3.jpg', '2018-07-17 23:16:29', '2018-07-17 23:16:29'),
(222, 97, NULL, 1, 'images/vendors/1/original/70a62110-8b4b-11e8-b2d3-2d0a38cc7876.jpg', 'images/vendors/1/list/70a62110-8b4b-11e8-b2d3-2d0a38cc7876.jpg', 'images/vendors/1/thumbs/70a62110-8b4b-11e8-b2d3-2d0a38cc7876.jpg', '2018-07-18 23:01:20', '2018-07-18 23:01:20'),
(224, 98, NULL, 1, 'images/vendors/1/original/dc6e74c0-8b4b-11e8-a984-7bf4c63f01e6.jpg', 'images/vendors/1/list/dc6e74c0-8b4b-11e8-a984-7bf4c63f01e6.jpg', 'images/vendors/1/thumbs/dc6e74c0-8b4b-11e8-a984-7bf4c63f01e6.jpg', '2018-07-18 23:04:21', '2018-07-18 23:04:21'),
(236, 99, 17, 2, 'images/vendors/1/original/16411080-8b79-11e8-9cf9-e1cb57cbf92a.jpg', 'images/vendors/1/list/16411080-8b79-11e8-9cf9-e1cb57cbf92a.jpg', 'images/vendors/1/thumbs/16411080-8b79-11e8-9cf9-e1cb57cbf92a.jpg', '2018-07-19 04:28:10', '2018-07-19 04:28:10'),
(235, 99, 44, 1, 'images/vendors/1/original/11d4afe0-8b79-11e8-8e46-b12d9252daaa.jpg', 'images/vendors/1/list/11d4afe0-8b79-11e8-8e46-b12d9252daaa.jpg', 'images/vendors/1/thumbs/11d4afe0-8b79-11e8-8e46-b12d9252daaa.jpg', '2018-07-19 04:28:01', '2018-07-19 04:28:01'),
(244, 104, NULL, 1, 'images/vendors/1/original/8e148450-9035-11e8-9875-fdfcd2c4163b.jpg', 'images/vendors/1/list/8e148450-9035-11e8-9875-fdfcd2c4163b.jpg', 'images/vendors/1/thumbs/8e148450-9035-11e8-9875-fdfcd2c4163b.jpg', '2018-07-25 05:07:12', '2018-07-25 05:07:12'),
(245, 104, NULL, 2, 'images/vendors/1/original/8e1ea960-9035-11e8-9da1-0790e06428b3.jpg', 'images/vendors/1/list/8e1ea960-9035-11e8-9da1-0790e06428b3.jpg', 'images/vendors/1/thumbs/8e1ea960-9035-11e8-9da1-0790e06428b3.jpg', '2018-07-25 05:07:12', '2018-07-25 05:07:12'),
(240, 105, NULL, 2, 'images/vendors/1/original/bb51a960-8b79-11e8-b1b8-e76456fd4234.JPG', 'images/vendors/1/list/bb51a960-8b79-11e8-b1b8-e76456fd4234.JPG', 'images/vendors/1/thumbs/bb51a960-8b79-11e8-b1b8-e76456fd4234.JPG', '2018-07-19 04:32:38', '2018-07-19 04:32:38'),
(239, 105, NULL, 1, 'images/vendors/1/original/bb48a0a0-8b79-11e8-b146-816f350490b3.jpg', 'images/vendors/1/list/bb48a0a0-8b79-11e8-b146-816f350490b3.jpg', 'images/vendors/1/thumbs/bb48a0a0-8b79-11e8-b146-816f350490b3.jpg', '2018-07-19 04:32:38', '2018-07-19 04:32:38'),
(248, 106, 46, 3, 'images/vendors/1/original/dfd666c0-94b8-11e8-a7bc-e571f469b686.jpg', 'images/vendors/1/list/dfd666c0-94b8-11e8-a7bc-e571f469b686.jpg', 'images/vendors/1/thumbs/dfd666c0-94b8-11e8-a7bc-e571f469b686.jpg', '2018-07-30 22:57:18', '2018-07-30 22:57:18'),
(247, 106, 47, 2, 'images/vendors/1/original/dfcce450-94b8-11e8-82b2-f39bf5ed6493.jpg', 'images/vendors/1/list/dfcce450-94b8-11e8-82b2-f39bf5ed6493.jpg', 'images/vendors/1/thumbs/dfcce450-94b8-11e8-82b2-f39bf5ed6493.jpg', '2018-07-30 22:57:18', '2018-07-30 22:57:18'),
(246, 106, 47, 1, 'images/vendors/1/original/dfa913d0-94b8-11e8-bc9f-2da58ca6ad23.jpg', 'images/vendors/1/list/dfa913d0-94b8-11e8-bc9f-2da58ca6ad23.jpg', 'images/vendors/1/thumbs/dfa913d0-94b8-11e8-bc9f-2da58ca6ad23.jpg', '2018-07-30 22:57:18', '2018-07-30 22:57:18'),
(249, 107, 44, 1, 'images/vendors/1/original/4146eb40-9bea-11e8-afe8-2964d9bff4c9.jpg', 'images/vendors/1/list/4146eb40-9bea-11e8-afe8-2964d9bff4c9.jpg', 'images/vendors/1/thumbs/4146eb40-9bea-11e8-afe8-2964d9bff4c9.jpg', '2018-08-09 02:38:25', '2018-08-09 02:38:25'),
(250, 107, 17, 2, 'images/vendors/1/original/416b5c30-9bea-11e8-ba59-4bf4f32d8155.jpg', 'images/vendors/1/list/416b5c30-9bea-11e8-ba59-4bf4f32d8155.jpg', 'images/vendors/1/thumbs/416b5c30-9bea-11e8-ba59-4bf4f32d8155.jpg', '2018-08-09 02:38:25', '2018-08-09 02:38:25'),
(251, 108, 44, 1, 'images/vendors/1/original/51702270-9bea-11e8-b4cc-fb9c6f89ece4.jpg', 'images/vendors/1/list/51702270-9bea-11e8-b4cc-fb9c6f89ece4.jpg', 'images/vendors/1/thumbs/51702270-9bea-11e8-b4cc-fb9c6f89ece4.jpg', '2018-08-09 02:38:52', '2018-08-09 02:38:52'),
(252, 108, 17, 2, 'images/vendors/1/original/517a53a0-9bea-11e8-8da4-b7546a92607e.jpg', 'images/vendors/1/list/517a53a0-9bea-11e8-8da4-b7546a92607e.jpg', 'images/vendors/1/thumbs/517a53a0-9bea-11e8-8da4-b7546a92607e.jpg', '2018-08-09 02:38:52', '2018-08-09 02:38:52'),
(253, 109, NULL, 1, 'images/vendors/1/original/005359e0-a145-11e8-965f-19a8c1b069ae.jpg', 'images/vendors/1/list/005359e0-a145-11e8-965f-19a8c1b069ae.jpg', 'images/vendors/1/thumbs/005359e0-a145-11e8-965f-19a8c1b069ae.jpg', '2018-08-15 22:10:36', '2018-08-15 22:10:36'),
(258, 110, NULL, 1, 'images/vendors/1/original/2acde050-a170-11e8-aabf-93a0dac9d451.jpg', 'images/vendors/1/list/2acde050-a170-11e8-aabf-93a0dac9d451.jpg', 'images/vendors/1/thumbs/2acde050-a170-11e8-aabf-93a0dac9d451.jpg', '2018-08-16 03:19:36', '2018-08-16 03:19:36'),
(259, NULL, NULL, NULL, '/images/item/9f70ae50-a2dd-11e8-8e79-43dc5e0343c5.jpg', NULL, NULL, '2018-08-17 22:55:38', '2018-08-17 22:55:38'),
(260, NULL, NULL, NULL, '/images/item/cce8aa00-a2dd-11e8-8acf-7d9c069f6927.jpg', NULL, NULL, '2018-08-17 22:56:54', '2018-08-17 22:56:54'),
(261, 91, NULL, 1, 'images/vendors/1/original/0dd34920-a2de-11e8-9e8d-35dc321f2188.jpg', 'images/vendors/1/list/0dd34920-a2de-11e8-9e8d-35dc321f2188.jpg', 'images/vendors/1/thumbs/0dd34920-a2de-11e8-9e8d-35dc321f2188.jpg', '2018-08-17 22:57:31', '2018-08-17 22:58:43'),
(262, 77, NULL, 1, 'images/vendors/1/original/190f35e0-a2de-11e8-8662-63e43e8dc18a.jpg', 'images/vendors/1/list/190f35e0-a2de-11e8-8662-63e43e8dc18a.jpg', 'images/vendors/1/thumbs/190f35e0-a2de-11e8-8662-63e43e8dc18a.jpg', '2018-08-17 22:59:00', '2018-08-17 22:59:02'),
(310, 111, 15, 6, 'images/vendors/1/original/ea773b40-ab7b-11e8-a99a-7fe2d64e6ae6.jpg', 'images/vendors/1/list/ea773b40-ab7b-11e8-a99a-7fe2d64e6ae6.jpg', 'images/vendors/1/thumbs/ea773b40-ab7b-11e8-a99a-7fe2d64e6ae6.jpg', '2018-08-28 22:08:53', '2018-08-28 22:08:53'),
(309, 111, 15, 5, 'images/vendors/1/original/ea6d9aa0-ab7b-11e8-a623-27a8be7c83d2.jpg', 'images/vendors/1/list/ea6d9aa0-ab7b-11e8-a623-27a8be7c83d2.jpg', 'images/vendors/1/thumbs/ea6d9aa0-ab7b-11e8-a623-27a8be7c83d2.jpg', '2018-08-28 22:08:53', '2018-08-28 22:08:53'),
(308, 111, 17, 4, 'images/vendors/1/original/ea628190-ab7b-11e8-9da5-3dc21d743991.jpg', 'images/vendors/1/list/ea628190-ab7b-11e8-9da5-3dc21d743991.jpg', 'images/vendors/1/thumbs/ea628190-ab7b-11e8-9da5-3dc21d743991.jpg', '2018-08-28 22:08:53', '2018-08-28 22:08:53'),
(307, 111, 17, 3, 'images/vendors/1/original/ea579280-ab7b-11e8-b464-3780c8402f48.jpg', 'images/vendors/1/list/ea579280-ab7b-11e8-b464-3780c8402f48.jpg', 'images/vendors/1/thumbs/ea579280-ab7b-11e8-b464-3780c8402f48.jpg', '2018-08-28 22:08:53', '2018-08-28 22:08:53'),
(306, 111, 16, 2, 'images/vendors/1/original/ea4cfed0-ab7b-11e8-abde-e10f38524e0c.jpg', 'images/vendors/1/list/ea4cfed0-ab7b-11e8-abde-e10f38524e0c.jpg', 'images/vendors/1/thumbs/ea4cfed0-ab7b-11e8-abde-e10f38524e0c.jpg', '2018-08-28 22:08:53', '2018-08-28 22:08:53'),
(305, 111, 16, 1, 'images/vendors/1/original/ea41bbd0-ab7b-11e8-8f0f-ad75099aa668.jpg', 'images/vendors/1/list/ea41bbd0-ab7b-11e8-8f0f-ad75099aa668.jpg', 'images/vendors/1/thumbs/ea41bbd0-ab7b-11e8-8f0f-ad75099aa668.jpg', '2018-08-28 22:08:53', '2018-08-28 22:08:53'),
(417, 112, 16, 1, 'images/vendors/1/original/c4ea39d0-b4f7-11e8-b7b7-15a6c0f65212.jpg', 'images/vendors/1/list/c4ea39d0-b4f7-11e8-b7b7-15a6c0f65212.jpg', 'images/vendors/1/thumbs/c4ea39d0-b4f7-11e8-b7b7-15a6c0f65212.jpg', '2018-09-09 23:48:09', '2018-09-09 23:48:09'),
(418, 112, 16, 2, 'images/vendors/1/original/c50f14b0-b4f7-11e8-a2d2-71cbca3ea5c0.jpg', 'images/vendors/1/list/c50f14b0-b4f7-11e8-a2d2-71cbca3ea5c0.jpg', 'images/vendors/1/thumbs/c50f14b0-b4f7-11e8-a2d2-71cbca3ea5c0.jpg', '2018-09-09 23:48:09', '2018-09-09 23:48:09'),
(419, 112, 17, 3, 'images/vendors/1/original/c518f9b0-b4f7-11e8-9926-4f608ae81e36.jpg', 'images/vendors/1/list/c518f9b0-b4f7-11e8-9926-4f608ae81e36.jpg', 'images/vendors/1/thumbs/c518f9b0-b4f7-11e8-9926-4f608ae81e36.jpg', '2018-09-09 23:48:09', '2018-09-09 23:48:09'),
(420, 112, 17, 4, 'images/vendors/1/original/c5234f50-b4f7-11e8-8af0-e357aeec5dff.jpg', 'images/vendors/1/list/c5234f50-b4f7-11e8-8af0-e357aeec5dff.jpg', 'images/vendors/1/thumbs/c5234f50-b4f7-11e8-8af0-e357aeec5dff.jpg', '2018-09-09 23:48:09', '2018-09-09 23:48:09'),
(421, 112, 15, 5, 'images/vendors/1/original/c52dca50-b4f7-11e8-ae88-894fb37ca56c.jpg', 'images/vendors/1/list/c52dca50-b4f7-11e8-ae88-894fb37ca56c.jpg', 'images/vendors/1/thumbs/c52dca50-b4f7-11e8-ae88-894fb37ca56c.jpg', '2018-09-09 23:48:09', '2018-09-09 23:48:09'),
(422, 112, 15, 6, 'images/vendors/1/original/c5377e90-b4f7-11e8-bb80-e3308df6561f.jpg', 'images/vendors/1/list/c5377e90-b4f7-11e8-bb80-e3308df6561f.jpg', 'images/vendors/1/thumbs/c5377e90-b4f7-11e8-bb80-e3308df6561f.jpg', '2018-09-09 23:48:09', '2018-09-09 23:48:09'),
(359, 113, 15, 1, 'images/vendors/1/original/2ae75dd0-ab9d-11e8-b6d4-df5d5a62c192.jpg', 'images/vendors/1/list/2ae75dd0-ab9d-11e8-b6d4-df5d5a62c192.jpg', 'images/vendors/1/thumbs/2ae75dd0-ab9d-11e8-b6d4-df5d5a62c192.jpg', '2018-08-29 02:06:55', '2018-08-29 02:06:55'),
(360, 113, 15, 2, 'images/vendors/1/original/2aef9ed0-ab9d-11e8-a17f-85eabfb4ff07.jpg', 'images/vendors/1/list/2aef9ed0-ab9d-11e8-a17f-85eabfb4ff07.jpg', 'images/vendors/1/thumbs/2aef9ed0-ab9d-11e8-a17f-85eabfb4ff07.jpg', '2018-08-29 02:06:55', '2018-08-29 02:06:55'),
(361, 113, 15, 3, 'images/vendors/1/original/2af69060-ab9d-11e8-9c01-1b4927b7c963.jpg', 'images/vendors/1/list/2af69060-ab9d-11e8-9c01-1b4927b7c963.jpg', 'images/vendors/1/thumbs/2af69060-ab9d-11e8-9c01-1b4927b7c963.jpg', '2018-08-29 02:06:55', '2018-08-29 02:06:55'),
(362, 113, 15, 4, 'images/vendors/1/original/2aff99a0-ab9d-11e8-925a-193dc86e6e67.jpg', 'images/vendors/1/list/2aff99a0-ab9d-11e8-925a-193dc86e6e67.jpg', 'images/vendors/1/thumbs/2aff99a0-ab9d-11e8-925a-193dc86e6e67.jpg', '2018-08-29 02:06:55', '2018-08-29 02:06:55'),
(363, 113, 15, 5, 'images/vendors/1/original/2b06b990-ab9d-11e8-b8c2-bde082cff698.jpg', 'images/vendors/1/list/2b06b990-ab9d-11e8-b8c2-bde082cff698.jpg', 'images/vendors/1/thumbs/2b06b990-ab9d-11e8-b8c2-bde082cff698.jpg', '2018-08-29 02:06:55', '2018-08-29 02:06:55'),
(364, 113, 6, 6, 'images/vendors/1/original/2b11fa50-ab9d-11e8-aa2c-2b47bdd4164f.jpg', 'images/vendors/1/list/2b11fa50-ab9d-11e8-aa2c-2b47bdd4164f.jpg', 'images/vendors/1/thumbs/2b11fa50-ab9d-11e8-aa2c-2b47bdd4164f.jpg', '2018-08-29 02:06:55', '2018-08-29 02:06:55'),
(365, 113, 6, 7, 'images/vendors/1/original/2b189fa0-ab9d-11e8-adc9-8b8bc256ac41.jpg', 'images/vendors/1/list/2b189fa0-ab9d-11e8-adc9-8b8bc256ac41.jpg', 'images/vendors/1/thumbs/2b189fa0-ab9d-11e8-adc9-8b8bc256ac41.jpg', '2018-08-29 02:06:55', '2018-08-29 02:06:55'),
(366, 113, 6, 8, 'images/vendors/1/original/2b208790-ab9d-11e8-8ac9-896581051224.jpg', 'images/vendors/1/list/2b208790-ab9d-11e8-8ac9-896581051224.jpg', 'images/vendors/1/thumbs/2b208790-ab9d-11e8-8ac9-896581051224.jpg', '2018-08-29 02:06:55', '2018-08-29 02:06:55'),
(367, 113, 6, 9, 'images/vendors/1/original/2b273320-ab9d-11e8-9211-4d8b1631345f.jpg', 'images/vendors/1/list/2b273320-ab9d-11e8-9211-4d8b1631345f.jpg', 'images/vendors/1/thumbs/2b273320-ab9d-11e8-9211-4d8b1631345f.jpg', '2018-08-29 02:06:55', '2018-08-29 02:06:55'),
(368, 113, 6, 10, 'images/vendors/1/original/2b2daa30-ab9d-11e8-8ab4-876cd5c7422b.jpg', 'images/vendors/1/list/2b2daa30-ab9d-11e8-8ab4-876cd5c7422b.jpg', 'images/vendors/1/thumbs/2b2daa30-ab9d-11e8-8ab4-876cd5c7422b.jpg', '2018-08-29 02:06:55', '2018-08-29 02:06:55'),
(423, 114, 16, 1, 'images/vendors/1/original/f414b790-b670-11e8-8afd-13734d8e9071.jpg', 'images/vendors/1/list/f414b790-b670-11e8-8afd-13734d8e9071.jpg', 'images/vendors/1/thumbs/f414b790-b670-11e8-8afd-13734d8e9071.jpg', '2018-09-11 20:48:08', '2018-09-11 20:48:08'),
(424, 114, 16, 2, 'images/vendors/1/original/f43ee350-b670-11e8-a5fe-51720d494ba0.jpg', 'images/vendors/1/list/f43ee350-b670-11e8-a5fe-51720d494ba0.jpg', 'images/vendors/1/thumbs/f43ee350-b670-11e8-a5fe-51720d494ba0.jpg', '2018-09-11 20:48:08', '2018-09-11 20:48:08'),
(425, 114, 17, 3, 'images/vendors/1/original/f4491c20-b670-11e8-a2d0-ed86dfb130b1.jpg', 'images/vendors/1/list/f4491c20-b670-11e8-a2d0-ed86dfb130b1.jpg', 'images/vendors/1/thumbs/f4491c20-b670-11e8-a2d0-ed86dfb130b1.jpg', '2018-09-11 20:48:08', '2018-09-11 20:48:08'),
(426, 114, 17, 4, 'images/vendors/1/original/f452da50-b670-11e8-b22f-d3ad336ec377.jpg', 'images/vendors/1/list/f452da50-b670-11e8-b22f-d3ad336ec377.jpg', 'images/vendors/1/thumbs/f452da50-b670-11e8-b22f-d3ad336ec377.jpg', '2018-09-11 20:48:08', '2018-09-11 20:48:08'),
(427, 114, 15, 5, 'images/vendors/1/original/f45d8500-b670-11e8-b425-5519c499d622.jpg', 'images/vendors/1/list/f45d8500-b670-11e8-b425-5519c499d622.jpg', 'images/vendors/1/thumbs/f45d8500-b670-11e8-b425-5519c499d622.jpg', '2018-09-11 20:48:08', '2018-09-11 20:48:08'),
(428, 114, 15, 6, 'images/vendors/1/original/f46745d0-b670-11e8-944f-6de3ca5edb19.jpg', 'images/vendors/1/list/f46745d0-b670-11e8-944f-6de3ca5edb19.jpg', 'images/vendors/1/thumbs/f46745d0-b670-11e8-944f-6de3ca5edb19.jpg', '2018-09-11 20:48:08', '2018-09-11 20:48:08'),
(429, 115, 16, 1, 'images/vendors/1/original/17972df0-b671-11e8-a8d3-5f6307bcb28c.jpg', 'images/vendors/1/list/17972df0-b671-11e8-a8d3-5f6307bcb28c.jpg', 'images/vendors/1/thumbs/17972df0-b671-11e8-a8d3-5f6307bcb28c.jpg', '2018-09-11 20:49:07', '2018-09-11 20:49:07'),
(430, 115, 16, 2, 'images/vendors/1/original/17a169f0-b671-11e8-94fd-7f288f9f6547.jpg', 'images/vendors/1/list/17a169f0-b671-11e8-94fd-7f288f9f6547.jpg', 'images/vendors/1/thumbs/17a169f0-b671-11e8-94fd-7f288f9f6547.jpg', '2018-09-11 20:49:07', '2018-09-11 20:49:07'),
(431, 115, 17, 3, 'images/vendors/1/original/17ab6230-b671-11e8-8262-89068a48f6a0.jpg', 'images/vendors/1/list/17ab6230-b671-11e8-8262-89068a48f6a0.jpg', 'images/vendors/1/thumbs/17ab6230-b671-11e8-8262-89068a48f6a0.jpg', '2018-09-11 20:49:08', '2018-09-11 20:49:08'),
(432, 115, 17, 4, 'images/vendors/1/original/17b50a80-b671-11e8-a464-b5fe6d93b621.jpg', 'images/vendors/1/list/17b50a80-b671-11e8-a464-b5fe6d93b621.jpg', 'images/vendors/1/thumbs/17b50a80-b671-11e8-a464-b5fe6d93b621.jpg', '2018-09-11 20:49:08', '2018-09-11 20:49:08'),
(433, 115, 15, 5, 'images/vendors/1/original/17bf09a0-b671-11e8-8062-8744f5c67586.jpg', 'images/vendors/1/list/17bf09a0-b671-11e8-8062-8744f5c67586.jpg', 'images/vendors/1/thumbs/17bf09a0-b671-11e8-8062-8744f5c67586.jpg', '2018-09-11 20:49:08', '2018-09-11 20:49:08'),
(434, 115, 15, 6, 'images/vendors/1/original/17c9fa90-b671-11e8-9e14-9b39c0306cbb.jpg', 'images/vendors/1/list/17c9fa90-b671-11e8-9e14-9b39c0306cbb.jpg', 'images/vendors/1/thumbs/17c9fa90-b671-11e8-9e14-9b39c0306cbb.jpg', '2018-09-11 20:49:08', '2018-09-11 20:49:08'),
(435, 116, 16, 1, 'images/vendors/1/original/0851c5e0-b81a-11e8-9cf0-23bc750008df.jpg', 'images/vendors/1/list/0851c5e0-b81a-11e8-9cf0-23bc750008df.jpg', 'images/vendors/1/thumbs/0851c5e0-b81a-11e8-9cf0-23bc750008df.jpg', '2018-09-13 23:30:58', '2018-09-13 23:30:58'),
(436, 116, 16, 2, 'images/vendors/1/original/087c5720-b81a-11e8-9f9a-eb2c9dffbfe8.jpg', 'images/vendors/1/list/087c5720-b81a-11e8-9f9a-eb2c9dffbfe8.jpg', 'images/vendors/1/thumbs/087c5720-b81a-11e8-9f9a-eb2c9dffbfe8.jpg', '2018-09-13 23:30:58', '2018-09-13 23:30:58'),
(437, 116, 17, 3, 'images/vendors/1/original/088976b0-b81a-11e8-8408-b34afcee9647.jpg', 'images/vendors/1/list/088976b0-b81a-11e8-8408-b34afcee9647.jpg', 'images/vendors/1/thumbs/088976b0-b81a-11e8-8408-b34afcee9647.jpg', '2018-09-13 23:30:58', '2018-09-13 23:30:58'),
(438, 116, 17, 4, 'images/vendors/1/original/08960920-b81a-11e8-8728-633748ed5502.jpg', 'images/vendors/1/list/08960920-b81a-11e8-8728-633748ed5502.jpg', 'images/vendors/1/thumbs/08960920-b81a-11e8-8728-633748ed5502.jpg', '2018-09-13 23:30:58', '2018-09-13 23:30:58'),
(439, 116, 15, 5, 'images/vendors/1/original/08a2b6c0-b81a-11e8-b9ac-d7c9bf7c64ac.jpg', 'images/vendors/1/list/08a2b6c0-b81a-11e8-b9ac-d7c9bf7c64ac.jpg', 'images/vendors/1/thumbs/08a2b6c0-b81a-11e8-b9ac-d7c9bf7c64ac.jpg', '2018-09-13 23:30:58', '2018-09-13 23:30:58'),
(440, 116, 15, 6, 'images/vendors/1/original/08af8210-b81a-11e8-a57e-e9668518db1c.jpg', 'images/vendors/1/list/08af8210-b81a-11e8-a57e-e9668518db1c.jpg', 'images/vendors/1/thumbs/08af8210-b81a-11e8-a57e-e9668518db1c.jpg', '2018-09-13 23:30:58', '2018-09-13 23:30:58'),
(441, 117, 16, 1, 'images/vendors/1/original/3cf66c30-b8d9-11e8-b824-93a1854007c9.jpg', 'images/vendors/1/list/3cf66c30-b8d9-11e8-b824-93a1854007c9.jpg', 'images/vendors/1/thumbs/3cf66c30-b8d9-11e8-b824-93a1854007c9.jpg', '2018-09-14 22:19:40', '2018-09-14 22:19:40'),
(442, 117, 16, 2, 'images/vendors/1/original/3d047a10-b8d9-11e8-bebc-f1f2e3a0aea9.jpg', 'images/vendors/1/list/3d047a10-b8d9-11e8-bebc-f1f2e3a0aea9.jpg', 'images/vendors/1/thumbs/3d047a10-b8d9-11e8-bebc-f1f2e3a0aea9.jpg', '2018-09-14 22:19:40', '2018-09-14 22:19:40'),
(443, 117, 17, 3, 'images/vendors/1/original/3d0ecd80-b8d9-11e8-88bf-73f9f72901ae.jpg', 'images/vendors/1/list/3d0ecd80-b8d9-11e8-88bf-73f9f72901ae.jpg', 'images/vendors/1/thumbs/3d0ecd80-b8d9-11e8-88bf-73f9f72901ae.jpg', '2018-09-14 22:19:40', '2018-09-14 22:19:40'),
(444, 117, 17, 4, 'images/vendors/1/original/3d18b0e0-b8d9-11e8-8982-1bf1efa75a20.jpg', 'images/vendors/1/list/3d18b0e0-b8d9-11e8-8982-1bf1efa75a20.jpg', 'images/vendors/1/thumbs/3d18b0e0-b8d9-11e8-8982-1bf1efa75a20.jpg', '2018-09-14 22:19:40', '2018-09-14 22:19:40'),
(445, 117, 15, 5, 'images/vendors/1/original/3d226ce0-b8d9-11e8-aa2f-d1750f16eef6.jpg', 'images/vendors/1/list/3d226ce0-b8d9-11e8-aa2f-d1750f16eef6.jpg', 'images/vendors/1/thumbs/3d226ce0-b8d9-11e8-aa2f-d1750f16eef6.jpg', '2018-09-14 22:19:40', '2018-09-14 22:19:40'),
(446, 117, 50, 6, 'images/vendors/1/original/3d2c7480-b8d9-11e8-9dd3-dfe001b6def2.jpg', 'images/vendors/1/list/3d2c7480-b8d9-11e8-9dd3-dfe001b6def2.jpg', 'images/vendors/1/thumbs/3d2c7480-b8d9-11e8-9dd3-dfe001b6def2.jpg', '2018-09-14 22:19:40', '2018-09-14 22:19:40'),
(447, 118, 16, 1, 'images/vendors/1/original/9e21bf10-b8d9-11e8-830b-b7388a4ae0c9.jpg', 'images/vendors/1/list/9e21bf10-b8d9-11e8-830b-b7388a4ae0c9.jpg', 'images/vendors/1/thumbs/9e21bf10-b8d9-11e8-830b-b7388a4ae0c9.jpg', '2018-09-14 22:22:23', '2018-09-14 22:22:23'),
(448, 118, 16, 2, 'images/vendors/1/original/9e2c2140-b8d9-11e8-8e11-99951155f32c.jpg', 'images/vendors/1/list/9e2c2140-b8d9-11e8-8e11-99951155f32c.jpg', 'images/vendors/1/thumbs/9e2c2140-b8d9-11e8-8e11-99951155f32c.jpg', '2018-09-14 22:22:23', '2018-09-14 22:22:23'),
(449, 118, 17, 3, 'images/vendors/1/original/9e36fd70-b8d9-11e8-a9c6-7575295a1d21.jpg', 'images/vendors/1/list/9e36fd70-b8d9-11e8-a9c6-7575295a1d21.jpg', 'images/vendors/1/thumbs/9e36fd70-b8d9-11e8-a9c6-7575295a1d21.jpg', '2018-09-14 22:22:23', '2018-09-14 22:22:23'),
(450, 118, 17, 4, 'images/vendors/1/original/9e40cfa0-b8d9-11e8-bde5-b329214d59d3.jpg', 'images/vendors/1/list/9e40cfa0-b8d9-11e8-bde5-b329214d59d3.jpg', 'images/vendors/1/thumbs/9e40cfa0-b8d9-11e8-bde5-b329214d59d3.jpg', '2018-09-14 22:22:23', '2018-09-14 22:22:23'),
(451, 118, 15, 5, 'images/vendors/1/original/9e4b1230-b8d9-11e8-81a3-ebf37f61549c.jpg', 'images/vendors/1/list/9e4b1230-b8d9-11e8-81a3-ebf37f61549c.jpg', 'images/vendors/1/thumbs/9e4b1230-b8d9-11e8-81a3-ebf37f61549c.jpg', '2018-09-14 22:22:23', '2018-09-14 22:22:23'),
(452, 118, 50, 6, 'images/vendors/1/original/9e54e3f0-b8d9-11e8-92ad-73b6f2b78e78.jpg', 'images/vendors/1/list/9e54e3f0-b8d9-11e8-92ad-73b6f2b78e78.jpg', 'images/vendors/1/thumbs/9e54e3f0-b8d9-11e8-92ad-73b6f2b78e78.jpg', '2018-09-14 22:22:23', '2018-09-14 22:22:23'),
(453, 119, 16, 1, 'images/vendors/1/original/c4105970-b8d9-11e8-b433-1bb58999e599.jpg', 'images/vendors/1/list/c4105970-b8d9-11e8-b433-1bb58999e599.jpg', 'images/vendors/1/thumbs/c4105970-b8d9-11e8-b433-1bb58999e599.jpg', '2018-09-14 22:23:27', '2018-09-14 22:23:27');
INSERT INTO `item_images` (`id`, `item_id`, `color_id`, `sort`, `image_path`, `list_image_path`, `thumbs_image_path`, `created_at`, `updated_at`) VALUES
(454, 119, 16, 2, 'images/vendors/1/original/c41cc100-b8d9-11e8-87f9-91a561e8a905.jpg', 'images/vendors/1/list/c41cc100-b8d9-11e8-87f9-91a561e8a905.jpg', 'images/vendors/1/thumbs/c41cc100-b8d9-11e8-87f9-91a561e8a905.jpg', '2018-09-14 22:23:27', '2018-09-14 22:23:27'),
(455, 119, 17, 3, 'images/vendors/1/original/c4276c50-b8d9-11e8-94e1-55d223160fb7.jpg', 'images/vendors/1/list/c4276c50-b8d9-11e8-94e1-55d223160fb7.jpg', 'images/vendors/1/thumbs/c4276c50-b8d9-11e8-94e1-55d223160fb7.jpg', '2018-09-14 22:23:27', '2018-09-14 22:23:27'),
(456, 119, 17, 4, 'images/vendors/1/original/c43152d0-b8d9-11e8-a87c-f767e60a22c0.jpg', 'images/vendors/1/list/c43152d0-b8d9-11e8-a87c-f767e60a22c0.jpg', 'images/vendors/1/thumbs/c43152d0-b8d9-11e8-a87c-f767e60a22c0.jpg', '2018-09-14 22:23:27', '2018-09-14 22:23:27'),
(457, 119, 15, 5, 'images/vendors/1/original/c43b6180-b8d9-11e8-b138-7d7bca83b066.jpg', 'images/vendors/1/list/c43b6180-b8d9-11e8-b138-7d7bca83b066.jpg', 'images/vendors/1/thumbs/c43b6180-b8d9-11e8-b138-7d7bca83b066.jpg', '2018-09-14 22:23:27', '2018-09-14 22:23:27'),
(458, 119, 50, 6, 'images/vendors/1/original/c4451e00-b8d9-11e8-a921-697bf28e5edb.jpg', 'images/vendors/1/list/c4451e00-b8d9-11e8-a921-697bf28e5edb.jpg', 'images/vendors/1/thumbs/c4451e00-b8d9-11e8-a921-697bf28e5edb.jpg', '2018-09-14 22:23:27', '2018-09-14 22:23:27'),
(555, 120, 16, 1, 'images/vendors/1/original/66887790-c98a-11e8-8028-89a66e0e9c8c.jpg', 'images/vendors/1/list/66887790-c98a-11e8-8028-89a66e0e9c8c.jpg', 'images/vendors/1/thumbs/66887790-c98a-11e8-8028-89a66e0e9c8c.jpg', '2018-10-06 04:08:10', '2018-10-06 04:08:10'),
(556, 120, 16, 2, 'images/vendors/1/original/66b01ac0-c98a-11e8-a5f9-8fc8c74d3fbb.jpg', 'images/vendors/1/list/66b01ac0-c98a-11e8-a5f9-8fc8c74d3fbb.jpg', 'images/vendors/1/thumbs/66b01ac0-c98a-11e8-a5f9-8fc8c74d3fbb.jpg', '2018-10-06 04:08:10', '2018-10-06 04:08:10'),
(557, 120, 17, 3, 'images/vendors/1/original/66ba8500-c98a-11e8-b545-3130e688ebce.jpg', 'images/vendors/1/list/66ba8500-c98a-11e8-b545-3130e688ebce.jpg', 'images/vendors/1/thumbs/66ba8500-c98a-11e8-b545-3130e688ebce.jpg', '2018-10-06 04:08:10', '2018-10-06 04:08:10'),
(558, 120, 17, 4, 'images/vendors/1/original/66c450d0-c98a-11e8-821e-337cfdef793e.jpg', 'images/vendors/1/list/66c450d0-c98a-11e8-821e-337cfdef793e.jpg', 'images/vendors/1/thumbs/66c450d0-c98a-11e8-821e-337cfdef793e.jpg', '2018-10-06 04:08:10', '2018-10-06 04:08:10'),
(559, 120, 15, 5, 'images/vendors/1/original/66ce2f40-c98a-11e8-9172-cbd6bd2a11d0.jpg', 'images/vendors/1/list/66ce2f40-c98a-11e8-9172-cbd6bd2a11d0.jpg', 'images/vendors/1/thumbs/66ce2f40-c98a-11e8-9172-cbd6bd2a11d0.jpg', '2018-10-06 04:08:10', '2018-10-06 04:08:10'),
(560, 120, 50, 6, 'images/vendors/1/original/66d7e940-c98a-11e8-8af1-1985c50e4be0.jpg', 'images/vendors/1/list/66d7e940-c98a-11e8-8af1-1985c50e4be0.jpg', 'images/vendors/1/thumbs/66d7e940-c98a-11e8-8af1-1985c50e4be0.jpg', '2018-10-06 04:08:10', '2018-10-06 04:08:10'),
(561, 121, 16, 1, 'images/vendors/1/original/8dc759c0-c98a-11e8-8134-7b5ea99891b7.jpg', 'images/vendors/1/list/8dc759c0-c98a-11e8-8134-7b5ea99891b7.jpg', 'images/vendors/1/thumbs/8dc759c0-c98a-11e8-8134-7b5ea99891b7.jpg', '2018-10-06 04:09:15', '2018-10-06 04:09:15'),
(562, 121, 16, 2, 'images/vendors/1/original/8dd21120-c98a-11e8-8990-574d2860dc8f.jpg', 'images/vendors/1/list/8dd21120-c98a-11e8-8990-574d2860dc8f.jpg', 'images/vendors/1/thumbs/8dd21120-c98a-11e8-8990-574d2860dc8f.jpg', '2018-10-06 04:09:15', '2018-10-06 04:09:15'),
(563, 121, 17, 3, 'images/vendors/1/original/8ddbf2c0-c98a-11e8-b9b5-1d2554617711.jpg', 'images/vendors/1/list/8ddbf2c0-c98a-11e8-b9b5-1d2554617711.jpg', 'images/vendors/1/thumbs/8ddbf2c0-c98a-11e8-b9b5-1d2554617711.jpg', '2018-10-06 04:09:15', '2018-10-06 04:09:15'),
(564, 121, 17, 4, 'images/vendors/1/original/8de57bc0-c98a-11e8-bd22-4d09ca410b45.jpg', 'images/vendors/1/list/8de57bc0-c98a-11e8-bd22-4d09ca410b45.jpg', 'images/vendors/1/thumbs/8de57bc0-c98a-11e8-bd22-4d09ca410b45.jpg', '2018-10-06 04:09:15', '2018-10-06 04:09:15'),
(565, 121, 15, 5, 'images/vendors/1/original/8def0ae0-c98a-11e8-aceb-1955f8cdafd1.jpg', 'images/vendors/1/list/8def0ae0-c98a-11e8-aceb-1955f8cdafd1.jpg', 'images/vendors/1/thumbs/8def0ae0-c98a-11e8-aceb-1955f8cdafd1.jpg', '2018-10-06 04:09:15', '2018-10-06 04:09:15'),
(566, 121, 50, 6, 'images/vendors/1/original/8df8a060-c98a-11e8-814a-a72142a60eeb.jpg', 'images/vendors/1/list/8df8a060-c98a-11e8-814a-a72142a60eeb.jpg', 'images/vendors/1/thumbs/8df8a060-c98a-11e8-814a-a72142a60eeb.jpg', '2018-10-06 04:09:15', '2018-10-06 04:09:15'),
(567, 122, 16, 1, 'images/vendors/1/original/be49a590-c98b-11e8-b200-c98c32557848.jpg', 'images/vendors/1/list/be49a590-c98b-11e8-b200-c98c32557848.jpg', 'images/vendors/1/thumbs/be49a590-c98b-11e8-b200-c98c32557848.jpg', '2018-10-06 04:17:46', '2018-10-06 04:17:46'),
(568, 122, 16, 2, 'images/vendors/1/original/be53b630-c98b-11e8-a947-23b93fec68fc.jpg', 'images/vendors/1/list/be53b630-c98b-11e8-a947-23b93fec68fc.jpg', 'images/vendors/1/thumbs/be53b630-c98b-11e8-a947-23b93fec68fc.jpg', '2018-10-06 04:17:46', '2018-10-06 04:17:46'),
(569, 122, 17, 3, 'images/vendors/1/original/be5dbea0-c98b-11e8-a16f-ddb8f08cda66.jpg', 'images/vendors/1/list/be5dbea0-c98b-11e8-a16f-ddb8f08cda66.jpg', 'images/vendors/1/thumbs/be5dbea0-c98b-11e8-a16f-ddb8f08cda66.jpg', '2018-10-06 04:17:46', '2018-10-06 04:17:46'),
(570, 122, 17, 4, 'images/vendors/1/original/be672d20-c98b-11e8-b396-d3010b390f29.jpg', 'images/vendors/1/list/be672d20-c98b-11e8-b396-d3010b390f29.jpg', 'images/vendors/1/thumbs/be672d20-c98b-11e8-b396-d3010b390f29.jpg', '2018-10-06 04:17:46', '2018-10-06 04:17:46'),
(571, 122, 15, 5, 'images/vendors/1/original/be70a220-c98b-11e8-b012-6b106116d0f5.jpg', 'images/vendors/1/list/be70a220-c98b-11e8-b012-6b106116d0f5.jpg', 'images/vendors/1/thumbs/be70a220-c98b-11e8-b012-6b106116d0f5.jpg', '2018-10-06 04:17:46', '2018-10-06 04:17:46'),
(572, 122, 50, 6, 'images/vendors/1/original/be7a4480-c98b-11e8-83aa-5b64be7dce94.jpg', 'images/vendors/1/list/be7a4480-c98b-11e8-83aa-5b64be7dce94.jpg', 'images/vendors/1/thumbs/be7a4480-c98b-11e8-83aa-5b64be7dce94.jpg', '2018-10-06 04:17:46', '2018-10-06 04:17:46'),
(606, 123, 50, 6, 'images/vendors/1/original/1252d230-1349-11e9-ad7f-29b8f7260e64.jpg', 'images/vendors/1/list/1252d230-1349-11e9-ad7f-29b8f7260e64.jpg', 'images/vendors/1/thumbs/1252d230-1349-11e9-ad7f-29b8f7260e64.jpg', '2019-01-07 23:26:57', '2019-01-07 23:26:57'),
(605, 123, 15, 5, 'images/vendors/1/original/12432d50-1349-11e9-a8a5-b19a878265f2.jpg', 'images/vendors/1/list/12432d50-1349-11e9-a8a5-b19a878265f2.jpg', 'images/vendors/1/thumbs/12432d50-1349-11e9-a8a5-b19a878265f2.jpg', '2019-01-07 23:26:57', '2019-01-07 23:26:57'),
(604, 123, 17, 4, 'images/vendors/1/original/12325ff0-1349-11e9-8a52-7fffeba53669.jpg', 'images/vendors/1/list/12325ff0-1349-11e9-8a52-7fffeba53669.jpg', 'images/vendors/1/thumbs/12325ff0-1349-11e9-8a52-7fffeba53669.jpg', '2019-01-07 23:26:57', '2019-01-07 23:26:57'),
(603, 123, 17, 3, 'images/vendors/1/original/1222a7b0-1349-11e9-b531-73ed2aab7824.jpg', 'images/vendors/1/list/1222a7b0-1349-11e9-b531-73ed2aab7824.jpg', 'images/vendors/1/thumbs/1222a7b0-1349-11e9-b531-73ed2aab7824.jpg', '2019-01-07 23:26:57', '2019-01-07 23:26:57'),
(602, 123, 16, 2, 'images/vendors/1/original/12138dc0-1349-11e9-97dd-b3f82bd57521.jpg', 'images/vendors/1/list/12138dc0-1349-11e9-97dd-b3f82bd57521.jpg', 'images/vendors/1/thumbs/12138dc0-1349-11e9-97dd-b3f82bd57521.jpg', '2019-01-07 23:26:56', '2019-01-07 23:26:56'),
(579, NULL, NULL, NULL, '/images/item/7f202ca0-fdeb-11e8-b158-399355d28c2d.jpg', NULL, NULL, '2018-12-11 18:54:12', '2018-12-11 18:54:12'),
(580, NULL, NULL, NULL, '/images/item/87675ce0-fdeb-11e8-88bd-8d9fb66ec058.png', NULL, NULL, '2018-12-11 18:54:26', '2018-12-11 18:54:26'),
(581, NULL, NULL, NULL, '/images/item/ea543530-fdeb-11e8-bf9f-bf205f3a1503.png', NULL, NULL, '2018-12-11 18:57:12', '2018-12-11 18:57:12'),
(582, NULL, NULL, NULL, '/images/item/0432e090-fdec-11e8-ab09-a5859740e995.png', NULL, NULL, '2018-12-11 18:57:55', '2018-12-11 18:57:55'),
(583, NULL, NULL, NULL, '/images/item/ed93e830-fdec-11e8-b09b-f9aa5f5e90ef.png', NULL, NULL, '2018-12-11 19:04:27', '2018-12-11 19:04:27'),
(584, NULL, NULL, NULL, '/images/item/5db370a0-fded-11e8-bc25-3fe4df0ad2c6.png', NULL, NULL, '2018-12-11 19:07:35', '2018-12-11 19:07:35'),
(585, NULL, NULL, NULL, '/images/item/75a01ca0-fded-11e8-b85c-f14accfc97aa.png', NULL, NULL, '2018-12-11 19:08:15', '2018-12-11 19:08:15'),
(586, NULL, NULL, NULL, '/images/item/a0715a60-fded-11e8-8f5b-ed9eb93e16e4.png', NULL, NULL, '2018-12-11 19:09:27', '2018-12-11 19:09:27'),
(587, NULL, NULL, NULL, '/images/item/a0715ad0-fded-11e8-ba39-0f5189c2e5da.png', NULL, NULL, '2018-12-11 19:09:27', '2018-12-11 19:09:27'),
(588, NULL, NULL, NULL, '/images/item/a0715ff0-fded-11e8-94dd-835cdc52303e.png', NULL, NULL, '2018-12-11 19:09:27', '2018-12-11 19:09:27'),
(601, 123, 16, 1, 'images/vendors/1/original/12046e20-1349-11e9-9dc9-196831b01e6e.jpg', 'images/vendors/1/list/12046e20-1349-11e9-9dc9-196831b01e6e.jpg', 'images/vendors/1/thumbs/12046e20-1349-11e9-9dc9-196831b01e6e.jpg', '2019-01-07 23:26:56', '2019-01-07 23:26:56');

-- --------------------------------------------------------

--
-- Table structure for table `item_views`
--

DROP TABLE IF EXISTS `item_views`;
CREATE TABLE IF NOT EXISTS `item_views` (
  `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT,
  `item_id` int(11) NOT NULL,
  `user_id` int(11) NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=539 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `item_views`
--

INSERT INTO `item_views` (`id`, `item_id`, `user_id`, `created_at`, `updated_at`) VALUES
(538, 123, 14, '2018-11-23 19:30:53', '2018-11-23 19:30:53'),
(510, 92, 14, '2018-09-19 21:43:26', '2018-09-19 21:43:26'),
(525, 91, 14, '2018-10-18 05:43:53', '2018-10-18 05:43:53'),
(517, 78, 14, '2018-09-25 21:09:21', '2018-09-25 21:09:21'),
(503, 18, 14, '2018-09-19 21:19:30', '2018-09-19 21:19:30'),
(533, 19, 14, '2018-11-20 20:05:23', '2018-11-20 20:05:23'),
(435, 77, 14, '2018-09-04 20:04:53', '2018-09-04 20:04:53'),
(535, 15, 14, '2018-11-20 20:06:38', '2018-11-20 20:06:38'),
(511, 67, 14, '2018-09-19 21:45:41', '2018-09-19 21:45:41'),
(536, 17, 14, '2018-11-23 19:30:41', '2018-11-23 19:30:41'),
(527, 16, 14, '2018-10-22 21:12:53', '2018-10-22 21:12:53');

-- --------------------------------------------------------

--
-- Table structure for table `lengths`
--

DROP TABLE IF EXISTS `lengths`;
CREATE TABLE IF NOT EXISTS `lengths` (
  `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT,
  `name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `sub_category_id` int(11) NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=10 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `lengths`
--

INSERT INTO `lengths` (`id`, `name`, `sub_category_id`, `created_at`, `updated_at`, `deleted_at`) VALUES
(1, 'sdf2', 8, '2018-05-17 07:02:23', '2018-05-17 07:02:52', '2018-05-17 07:02:52'),
(2, '1/2 Sleeve', 10, '2018-05-17 07:02:50', '2018-05-22 14:23:02', NULL),
(3, '3/4 Sleeve', 10, '2018-05-22 14:23:10', '2018-05-22 14:23:10', NULL),
(4, 'Asymmetrical', 10, '2018-05-22 14:23:18', '2018-05-22 14:23:18', NULL),
(5, 'Bell Sleeves', 10, '2018-05-22 14:23:24', '2018-05-22 14:23:24', NULL),
(6, 'Bubble Sleeve', 10, '2018-05-22 14:23:32', '2018-05-22 14:23:32', NULL),
(7, 'Cap Sleeve', 10, '2018-05-22 14:23:42', '2018-05-22 14:23:42', NULL),
(8, 'Dolman Sleeve', 10, '2018-05-22 14:23:49', '2018-05-22 14:23:49', NULL),
(9, 'Flyaway Sleeve', 10, '2018-05-22 14:23:57', '2018-05-22 14:23:57', NULL);

-- --------------------------------------------------------

--
-- Table structure for table `login_history`
--

DROP TABLE IF EXISTS `login_history`;
CREATE TABLE IF NOT EXISTS `login_history` (
  `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT,
  `user_id` int(11) NOT NULL,
  `ip` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=614 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `login_history`
--

INSERT INTO `login_history` (`id`, `user_id`, `ip`, `created_at`, `updated_at`) VALUES
(1, 1, '::1', '2018-05-16 03:27:48', '2018-05-16 03:27:48'),
(2, 1, '::1', '2018-05-16 03:29:12', '2018-05-16 03:29:12'),
(3, 1, '::1', '2018-05-16 03:50:05', '2018-05-16 03:50:05'),
(4, 1, '::1', '2018-05-16 04:23:32', '2018-05-16 04:23:32'),
(5, 1, '::1', '2018-05-16 04:45:14', '2018-05-16 04:45:14'),
(6, 1, '::1', '2018-05-17 01:14:36', '2018-05-17 01:14:36'),
(7, 12, '::1', '2018-05-17 02:10:33', '2018-05-17 02:10:33'),
(8, 12, '::1', '2018-05-17 02:18:15', '2018-05-17 02:18:15'),
(9, 12, '::1', '2018-05-17 02:18:43', '2018-05-17 02:18:43'),
(10, 12, '::1', '2018-05-17 02:19:53', '2018-05-17 02:19:53'),
(11, 1, '::1', '2018-05-17 02:26:59', '2018-05-17 02:26:59'),
(12, 1, '::1', '2018-05-17 08:29:42', '2018-05-17 08:29:42'),
(13, 1, '::1', '2018-05-18 02:37:28', '2018-05-18 02:37:28'),
(14, 1, '::1', '2018-05-18 08:25:13', '2018-05-18 08:25:13'),
(15, 1, '::1', '2018-05-19 02:15:46', '2018-05-19 02:15:46'),
(16, 1, '::1', '2018-05-19 08:06:43', '2018-05-19 08:06:43'),
(17, 1, '::1', '2018-05-19 08:20:41', '2018-05-19 08:20:41'),
(18, 1, '::1', '2018-05-21 02:06:31', '2018-05-21 02:06:31'),
(19, 1, '::1', '2018-05-21 08:57:37', '2018-05-21 08:57:37'),
(20, 1, '::1', '2018-05-21 12:03:37', '2018-05-21 12:03:37'),
(21, 1, '::1', '2018-05-22 12:26:50', '2018-05-22 12:26:50'),
(22, 1, '::1', '2018-05-22 14:35:25', '2018-05-22 14:35:25'),
(23, 1, '::1', '2018-05-23 04:10:57', '2018-05-23 04:10:57'),
(24, 1, '::1', '2018-05-23 09:43:26', '2018-05-23 09:43:26'),
(25, 13, '::1', '2018-05-23 15:17:15', '2018-05-23 15:17:15'),
(26, 1, '::1', '2018-05-24 02:09:18', '2018-05-24 02:09:18'),
(27, 14, '::1', '2018-05-24 10:54:29', '2018-05-24 10:54:29'),
(28, 14, '::1', '2018-05-24 11:11:36', '2018-05-24 11:11:36'),
(29, 14, '::1', '2018-05-24 11:11:52', '2018-05-24 11:11:52'),
(30, 14, '::1', '2018-05-24 11:15:18', '2018-05-24 11:15:18'),
(31, 14, '::1', '2018-05-24 11:32:52', '2018-05-24 11:32:52'),
(32, 1, '::1', '2018-05-24 12:10:47', '2018-05-24 12:10:47'),
(33, 1, '::1', '2018-05-24 12:14:48', '2018-05-24 12:14:48'),
(34, 14, '::1', '2018-05-24 12:16:59', '2018-05-24 12:16:59'),
(35, 1, '::1', '2018-05-24 12:33:27', '2018-05-24 12:33:27'),
(36, 14, '::1', '2018-05-24 14:03:02', '2018-05-24 14:03:02'),
(37, 1, '::1', '2018-05-25 03:18:56', '2018-05-25 03:18:56'),
(38, 14, '::1', '2018-05-25 12:42:12', '2018-05-25 12:42:12'),
(39, 14, '::1', '2018-05-25 13:26:50', '2018-05-25 13:26:50'),
(40, 1, '::1', '2018-05-26 02:22:07', '2018-05-26 02:22:07'),
(41, 1, '::1', '2018-05-26 04:37:55', '2018-05-26 04:37:55'),
(42, 1, '::1', '2018-05-26 05:35:17', '2018-05-26 05:35:17'),
(43, 1, '::1', '2018-05-26 08:59:26', '2018-05-26 08:59:26'),
(44, 1, '::1', '2018-05-26 09:00:54', '2018-05-26 09:00:54'),
(45, 14, '::1', '2018-05-26 13:53:18', '2018-05-26 13:53:18'),
(46, 1, '::1', '2018-05-28 02:04:42', '2018-05-28 02:04:42'),
(47, 1, '::1', '2018-05-28 07:53:33', '2018-05-28 07:53:33'),
(48, 14, '::1', '2018-05-28 11:06:13', '2018-05-28 11:06:13'),
(49, 1, '::1', '2018-05-28 15:28:36', '2018-05-28 15:28:36'),
(50, 14, '::1', '2018-05-28 15:29:05', '2018-05-28 15:29:05'),
(51, 1, '::1', '2018-05-29 09:15:48', '2018-05-29 09:15:48'),
(52, 14, '::1', '2018-05-29 09:31:19', '2018-05-29 09:31:19'),
(53, 14, '::1', '2018-05-29 12:35:28', '2018-05-29 12:35:28'),
(54, 1, '::1', '2018-05-29 13:29:02', '2018-05-29 13:29:02'),
(55, 14, '::1', '2018-05-30 02:04:38', '2018-05-30 02:04:38'),
(56, 14, '::1', '2018-05-30 02:59:00', '2018-05-30 02:59:00'),
(57, 1, '::1', '2018-05-30 04:23:33', '2018-05-30 04:23:33'),
(58, 14, '::1', '2018-05-30 04:28:08', '2018-05-30 04:28:08'),
(59, 14, '::1', '2018-05-30 04:36:41', '2018-05-30 04:36:41'),
(60, 1, '::1', '2018-05-30 09:04:34', '2018-05-30 09:04:34'),
(61, 14, '::1', '2018-05-30 09:14:11', '2018-05-30 09:14:11'),
(62, 14, '::1', '2018-05-30 12:37:28', '2018-05-30 12:37:28'),
(63, 14, '::1', '2018-05-30 12:48:18', '2018-05-30 12:48:18'),
(64, 14, '::1', '2018-05-30 13:27:15', '2018-05-30 13:27:15'),
(65, 1, '::1', '2018-05-30 13:29:05', '2018-05-30 13:29:05'),
(66, 14, '::1', '2018-05-31 00:11:42', '2018-05-31 00:11:42'),
(67, 1, '::1', '2018-05-31 00:57:42', '2018-05-31 00:57:42'),
(68, 14, '::1', '2018-05-31 01:00:44', '2018-05-31 01:00:44'),
(69, 1, '::1', '2018-05-31 04:15:34', '2018-05-31 04:15:34'),
(70, 1, '::1', '2018-05-31 04:16:31', '2018-05-31 04:16:31'),
(71, 1, '::1', '2018-05-31 04:18:10', '2018-05-31 04:18:10'),
(72, 1, '::1', '2018-06-01 03:38:39', '2018-06-01 03:38:39'),
(73, 1, '::1', '2018-06-01 08:59:29', '2018-06-01 08:59:29'),
(74, 1, '::1', '2018-06-01 11:33:39', '2018-06-01 11:33:39'),
(75, 13, '::1', '2018-06-01 11:34:53', '2018-06-01 11:34:53'),
(76, 14, '::1', '2018-06-01 12:54:01', '2018-06-01 12:54:01'),
(77, 1, '::1', '2018-06-01 13:01:51', '2018-06-01 13:01:51'),
(78, 14, '::1', '2018-06-01 13:10:20', '2018-06-01 13:10:20'),
(79, 1, '::1', '2018-06-01 13:49:51', '2018-06-01 13:49:51'),
(80, 1, '::1', '2018-06-01 22:09:28', '2018-06-01 22:09:28'),
(81, 14, '::1', '2018-06-01 22:14:09', '2018-06-01 22:14:09'),
(82, 1, '::1', '2018-06-01 23:22:53', '2018-06-01 23:22:53'),
(83, 13, '::1', '2018-06-01 23:32:21', '2018-06-01 23:32:21'),
(84, 14, '::1', '2018-06-02 02:09:22', '2018-06-02 02:09:22'),
(85, 1, '::1', '2018-06-02 02:57:53', '2018-06-02 02:57:53'),
(86, 14, '::1', '2018-06-02 12:25:38', '2018-06-02 12:25:38'),
(87, 1, '::1', '2018-06-02 13:40:05', '2018-06-02 13:40:05'),
(88, 13, '::1', '2018-06-02 13:41:10', '2018-06-02 13:41:10'),
(89, 14, '::1', '2018-06-02 13:59:32', '2018-06-02 13:59:32'),
(90, 1, '::1', '2018-06-02 14:28:08', '2018-06-02 14:28:08'),
(91, 1, '::1', '2018-06-04 02:52:46', '2018-06-04 02:52:46'),
(92, 13, '::1', '2018-06-04 03:01:08', '2018-06-04 03:01:08'),
(93, 1, '::1', '2018-06-04 03:44:48', '2018-06-04 03:44:48'),
(94, 1, '::1', '2018-06-04 04:19:03', '2018-06-04 04:19:03'),
(95, 1, '::1', '2018-06-04 05:09:13', '2018-06-04 05:09:13'),
(96, 14, '::1', '2018-06-04 05:24:00', '2018-06-04 05:24:00'),
(97, 14, '::1', '2018-06-04 05:33:09', '2018-06-04 05:33:09'),
(98, 13, '::1', '2018-06-04 09:22:12', '2018-06-04 09:22:12'),
(99, 1, '::1', '2018-06-04 10:06:17', '2018-06-04 10:06:17'),
(100, 14, '::1', '2018-06-04 10:35:47', '2018-06-04 10:35:47'),
(101, 1, '::1', '2018-06-04 12:12:06', '2018-06-04 12:12:06'),
(102, 14, '::1', '2018-06-04 12:38:54', '2018-06-04 12:38:54'),
(103, 14, '::1', '2018-06-04 14:03:38', '2018-06-04 14:03:38'),
(104, 1, '::1', '2018-06-05 02:09:19', '2018-06-05 02:09:19'),
(105, 1, '::1', '2018-06-05 03:12:01', '2018-06-05 03:12:01'),
(106, 1, '::1', '2018-06-05 09:32:14', '2018-06-05 09:32:14'),
(107, 1, '::1', '2018-06-06 02:23:15', '2018-06-06 02:23:15'),
(108, 1, '::1', '2018-06-06 09:16:26', '2018-06-06 09:16:26'),
(109, 1, '::1', '2018-06-06 11:35:14', '2018-06-06 11:35:14'),
(110, 1, '::1', '2018-06-06 11:35:42', '2018-06-06 11:35:42'),
(111, 1, '::1', '2018-06-06 13:56:53', '2018-06-06 13:56:53'),
(112, 14, '::1', '2018-06-07 04:05:17', '2018-06-07 04:05:17'),
(113, 1, '::1', '2018-06-07 04:07:03', '2018-06-07 04:07:03'),
(114, 1, '::1', '2018-06-07 05:02:15', '2018-06-07 05:02:15'),
(115, 14, '::1', '2018-06-07 05:18:23', '2018-06-07 05:18:23'),
(116, 1, '::1', '2018-06-07 05:39:10', '2018-06-07 05:39:10'),
(117, 1, '::1', '2018-06-07 11:49:32', '2018-06-07 11:49:32'),
(118, 1, '::1', '2018-06-07 15:40:57', '2018-06-07 15:40:57'),
(119, 1, '::1', '2018-06-08 02:05:30', '2018-06-08 02:05:30'),
(120, 13, '::1', '2018-06-08 09:31:12', '2018-06-08 09:31:12'),
(121, 1, '::1', '2018-06-08 10:26:33', '2018-06-08 10:26:33'),
(122, 13, '::1', '2018-06-08 10:26:50', '2018-06-08 10:26:50'),
(123, 1, '::1', '2018-06-08 11:36:30', '2018-06-08 11:36:30'),
(124, 1, '::1', '2018-06-08 14:05:05', '2018-06-08 14:05:05'),
(125, 1, '::1', '2018-06-08 14:12:51', '2018-06-08 14:12:51'),
(126, 14, '::1', '2018-06-09 02:01:14', '2018-06-09 02:01:14'),
(127, 1, '::1', '2018-06-09 02:02:59', '2018-06-09 02:02:59'),
(128, 1, '::1', '2018-06-09 04:54:58', '2018-06-09 04:54:58'),
(129, 14, '::1', '2018-06-09 05:47:16', '2018-06-09 05:47:16'),
(130, 1, '::1', '2018-06-09 10:41:35', '2018-06-09 10:41:35'),
(131, 13, '::1', '2018-06-09 10:42:10', '2018-06-09 10:42:10'),
(132, 14, '::1', '2018-06-09 11:37:16', '2018-06-09 11:37:16'),
(133, 14, '::1', '2018-06-09 12:07:18', '2018-06-09 12:07:18'),
(134, 14, '::1', '2018-06-09 12:16:58', '2018-06-09 12:16:58'),
(135, 14, '::1', '2018-06-09 12:33:53', '2018-06-09 12:33:53'),
(136, 1, '::1', '2018-06-09 13:22:07', '2018-06-09 13:22:07'),
(137, 14, '::1', '2018-06-09 13:22:50', '2018-06-09 13:22:50'),
(138, 14, '::1', '2018-06-10 02:03:55', '2018-06-10 02:03:55'),
(139, 1, '::1', '2018-06-10 02:49:56', '2018-06-10 02:49:56'),
(140, 1, '::1', '2018-06-10 03:53:02', '2018-06-10 03:53:02'),
(141, 14, '::1', '2018-06-10 04:03:28', '2018-06-10 04:03:28'),
(142, 14, '::1', '2018-06-10 04:23:05', '2018-06-10 04:23:05'),
(143, 1, '::1', '2018-06-10 05:15:42', '2018-06-10 05:15:42'),
(144, 1, '::1', '2018-06-10 09:03:14', '2018-06-10 09:03:14'),
(145, 1, '::1', '2018-06-11 02:08:22', '2018-06-11 02:08:22'),
(146, 1, '::1', '2018-06-11 03:14:42', '2018-06-11 03:14:42'),
(147, 14, '::1', '2018-06-11 04:11:12', '2018-06-11 04:11:12'),
(148, 14, '::1', '2018-06-11 08:59:47', '2018-06-11 08:59:47'),
(149, 1, '::1', '2018-06-11 09:53:10', '2018-06-11 09:53:10'),
(150, 1, '::1', '2018-06-11 10:05:28', '2018-06-11 10:05:28'),
(151, 14, '::1', '2018-06-11 10:44:26', '2018-06-11 10:44:26'),
(152, 1, '::1', '2018-06-11 12:03:03', '2018-06-11 12:03:03'),
(153, 1, '::1', '2018-06-11 12:53:09', '2018-06-11 12:53:09'),
(154, 1, '::1', '2018-06-12 03:09:23', '2018-06-12 03:09:23'),
(155, 1, '::1', '2018-06-12 04:45:40', '2018-06-12 04:45:40'),
(156, 1, '::1', '2018-06-12 05:07:48', '2018-06-12 05:07:48'),
(157, 14, '::1', '2018-06-12 09:03:13', '2018-06-12 09:03:13'),
(158, 14, '::1', '2018-06-12 11:17:54', '2018-06-12 11:17:54'),
(159, 1, '::1', '2018-06-12 12:10:36', '2018-06-12 12:10:36'),
(160, 14, '::1', '2018-06-12 13:54:53', '2018-06-12 13:54:53'),
(161, 1, '::1', '2018-06-13 02:29:28', '2018-06-13 02:29:28'),
(162, 1, '::1', '2018-06-13 02:59:10', '2018-06-13 02:59:10'),
(163, 14, '::1', '2018-06-13 03:17:03', '2018-06-13 03:17:03'),
(164, 14, '::1', '2018-06-13 04:45:52', '2018-06-13 04:45:52'),
(165, 1, '::1', '2018-06-13 05:33:07', '2018-06-13 05:33:07'),
(166, 1, '::1', '2018-06-13 10:00:57', '2018-06-13 10:00:57'),
(167, 14, '::1', '2018-06-13 11:17:38', '2018-06-13 11:17:38'),
(168, 1, '::1', '2018-06-13 11:46:23', '2018-06-13 11:46:23'),
(169, 1, '::1', '2018-06-13 15:46:07', '2018-06-13 15:46:07'),
(170, 1, '::1', '2018-06-13 16:41:28', '2018-06-13 16:41:28'),
(171, 14, '::1', '2018-06-22 01:49:03', '2018-06-22 01:49:03'),
(172, 1, '::1', '2018-06-22 01:49:17', '2018-06-22 01:49:17'),
(173, 1, '::1', '2018-06-22 02:18:51', '2018-06-22 02:18:51'),
(174, 14, '::1', '2018-06-22 02:25:38', '2018-06-22 02:25:38'),
(175, 1, '::1', '2018-06-22 02:27:58', '2018-06-22 02:27:58'),
(176, 1, '::1', '2018-06-22 03:37:42', '2018-06-22 03:37:42'),
(177, 1, '::1', '2018-06-22 04:20:20', '2018-06-22 04:20:20'),
(178, 1, '::1', '2018-06-22 04:50:39', '2018-06-22 04:50:39'),
(179, 14, '::1', '2018-06-22 05:26:22', '2018-06-22 05:26:22'),
(180, 14, '::1', '2018-06-22 05:44:41', '2018-06-22 05:44:41'),
(181, 1, '::1', '2018-06-22 05:48:03', '2018-06-22 05:48:03'),
(182, 14, '::1', '2018-06-22 06:21:20', '2018-06-22 06:21:20'),
(183, 1, '::1', '2018-06-22 13:14:10', '2018-06-22 13:14:10'),
(184, 1, '::1', '2018-06-23 03:12:49', '2018-06-23 03:12:49'),
(185, 14, '::1', '2018-06-23 03:17:00', '2018-06-23 03:17:00'),
(186, 1, '::1', '2018-06-23 07:34:24', '2018-06-23 07:34:24'),
(187, 14, '::1', '2018-06-23 07:44:51', '2018-06-23 07:44:51'),
(188, 1, '::1', '2018-06-23 07:49:34', '2018-06-23 07:49:34'),
(189, 1, '::1', '2018-06-23 08:46:15', '2018-06-23 08:46:15'),
(190, 14, '::1', '2018-06-23 08:58:06', '2018-06-23 08:58:06'),
(191, 1, '::1', '2018-06-23 12:34:05', '2018-06-23 12:34:05'),
(192, 1, '::1', '2018-06-25 02:24:59', '2018-06-25 02:24:59'),
(193, 1, '::1', '2018-06-25 03:21:28', '2018-06-25 03:21:28'),
(194, 1, '::1', '2018-06-25 03:46:04', '2018-06-25 03:46:04'),
(195, 1, '::1', '2018-06-25 03:51:07', '2018-06-25 03:51:07'),
(196, 1, '::1', '2018-06-25 03:59:51', '2018-06-25 03:59:51'),
(197, 14, '::1', '2018-06-25 04:09:36', '2018-06-25 04:09:36'),
(198, 1, '::1', '2018-06-25 07:45:43', '2018-06-25 07:45:43'),
(199, 1, '::1', '2018-06-25 10:52:13', '2018-06-25 10:52:13'),
(200, 14, '::1', '2018-06-25 11:14:00', '2018-06-25 11:14:00'),
(201, 1, '::1', '2018-06-25 13:12:33', '2018-06-25 13:12:33'),
(202, 14, '::1', '2018-06-26 02:54:32', '2018-06-26 02:54:32'),
(203, 1, '::1', '2018-06-26 02:59:42', '2018-06-26 02:59:42'),
(204, 14, '::1', '2018-06-26 03:24:56', '2018-06-26 03:24:56'),
(205, 1, '::1', '2018-06-26 03:26:16', '2018-06-26 03:26:16'),
(206, 14, '::1', '2018-06-26 04:17:51', '2018-06-26 04:17:51'),
(207, 1, '::1', '2018-06-26 04:29:07', '2018-06-26 04:29:07'),
(208, 14, '::1', '2018-06-26 05:45:07', '2018-06-26 05:45:07'),
(209, 14, '::1', '2018-06-26 05:54:11', '2018-06-26 05:54:11'),
(210, 14, '::1', '2018-06-26 05:55:55', '2018-06-26 05:55:55'),
(211, 14, '::1', '2018-06-26 07:23:31', '2018-06-26 07:23:31'),
(212, 1, '::1', '2018-06-26 08:12:26', '2018-06-26 08:12:26'),
(213, 14, '::1', '2018-06-26 10:09:06', '2018-06-26 10:09:06'),
(214, 1, '::1', '2018-06-26 10:18:49', '2018-06-26 10:18:49'),
(215, 14, '::1', '2018-06-26 10:59:58', '2018-06-26 10:59:58'),
(216, 1, '::1', '2018-06-26 13:11:30', '2018-06-26 13:11:30'),
(217, 1, '::1', '2018-06-27 03:20:55', '2018-06-27 03:20:55'),
(218, 14, '::1', '2018-06-27 05:54:07', '2018-06-27 05:54:07'),
(219, 1, '::1', '2018-06-27 07:05:31', '2018-06-27 07:05:31'),
(220, 1, '::1', '2018-06-27 09:59:41', '2018-06-27 09:59:41'),
(221, 14, '::1', '2018-06-27 12:07:05', '2018-06-27 12:07:05'),
(222, 1, '::1', '2018-06-27 12:13:00', '2018-06-27 12:13:00'),
(223, 1, '::1', '2018-06-28 02:13:05', '2018-06-28 02:13:05'),
(224, 14, '::1', '2018-06-28 03:19:17', '2018-06-28 03:19:17'),
(225, 1, '::1', '2018-06-28 03:29:35', '2018-06-28 03:29:35'),
(226, 1, '::1', '2018-06-28 03:44:50', '2018-06-28 03:44:50'),
(227, 1, '::1', '2018-06-28 03:54:14', '2018-06-28 03:54:14'),
(228, 1, '::1', '2018-06-28 04:39:36', '2018-06-28 04:39:36'),
(229, 14, '::1', '2018-06-28 06:19:10', '2018-06-28 06:19:10'),
(230, 1, '::1', '2018-06-28 07:08:38', '2018-06-28 07:08:38'),
(231, 14, '::1', '2018-06-28 12:38:20', '2018-06-28 12:38:20'),
(232, 1, '::1', '2018-06-28 12:40:10', '2018-06-28 12:40:10'),
(233, 14, '::1', '2018-06-28 12:43:51', '2018-06-28 12:43:51'),
(234, 1, '::1', '2018-06-28 12:56:48', '2018-06-28 12:56:48'),
(235, 1, '::1', '2018-06-28 13:43:16', '2018-06-28 13:43:16'),
(236, 1, '::1', '2018-06-28 13:59:43', '2018-06-28 13:59:43'),
(237, 1, '::1', '2018-06-29 04:48:34', '2018-06-29 04:48:34'),
(238, 1, '::1', '2018-06-29 09:06:15', '2018-06-29 09:06:15'),
(239, 1, '::1', '2018-06-29 10:48:32', '2018-06-29 10:48:32'),
(240, 14, '::1', '2018-06-29 11:06:47', '2018-06-29 11:06:47'),
(241, 14, '::1', '2018-06-29 11:55:52', '2018-06-29 11:55:52'),
(242, 1, '::1', '2018-06-30 02:56:16', '2018-06-30 02:56:16'),
(243, 1, '::1', '2018-06-30 03:17:33', '2018-06-30 03:17:33'),
(244, 14, '::1', '2018-06-30 03:28:25', '2018-06-30 03:28:25'),
(245, 1, '::1', '2018-06-30 05:04:45', '2018-06-30 05:04:45'),
(246, 1, '::1', '2018-06-30 05:06:30', '2018-06-30 05:06:30'),
(247, 19, '::1', '2018-06-30 05:24:27', '2018-06-30 05:24:27'),
(248, 1, '::1', '2018-06-30 06:51:05', '2018-06-30 06:51:05'),
(249, 14, '::1', '2018-06-30 13:45:17', '2018-06-30 13:45:17'),
(250, 14, '::1', '2018-07-02 04:02:22', '2018-07-02 04:02:22'),
(251, 14, '::1', '2018-07-02 04:12:41', '2018-07-02 04:12:41'),
(252, 1, '::1', '2018-07-02 04:19:34', '2018-07-02 04:19:34'),
(253, 1, '::1', '2018-07-02 05:35:43', '2018-07-02 05:35:43'),
(254, 13, '::1', '2018-07-02 05:45:18', '2018-07-02 05:45:18'),
(255, 14, '::1', '2018-07-02 05:47:44', '2018-07-02 05:47:44'),
(256, 1, '::1', '2018-07-02 06:44:21', '2018-07-02 06:44:21'),
(257, 14, '::1', '2018-07-02 11:46:40', '2018-07-02 11:46:40'),
(258, 14, '::1', '2018-07-03 02:50:46', '2018-07-03 02:50:46'),
(259, 1, '::1', '2018-07-03 05:00:16', '2018-07-03 05:00:16'),
(260, 1, '::1', '2018-07-03 09:08:34', '2018-07-03 09:08:34'),
(261, 14, '::1', '2018-07-03 09:58:59', '2018-07-03 09:58:59'),
(262, 14, '::1', '2018-07-03 12:42:16', '2018-07-03 12:42:16'),
(263, 19, '::1', '2018-07-03 12:57:07', '2018-07-03 12:57:07'),
(264, 14, '::1', '2018-07-03 13:22:52', '2018-07-03 13:22:52'),
(265, 1, '::1', '2018-07-04 02:10:30', '2018-07-04 02:10:30'),
(266, 1, '::1', '2018-07-04 03:46:39', '2018-07-04 03:46:39'),
(267, 14, '::1', '2018-07-04 05:45:22', '2018-07-04 05:45:22'),
(268, 1, '::1', '2018-07-04 05:51:23', '2018-07-04 05:51:23'),
(269, 14, '::1', '2018-07-04 07:59:01', '2018-07-04 07:59:01'),
(270, 1, '::1', '2018-07-04 08:00:10', '2018-07-04 08:00:10'),
(271, 14, '::1', '2018-07-04 08:18:30', '2018-07-04 08:18:30'),
(272, 1, '::1', '2018-07-04 08:31:24', '2018-07-04 08:31:24'),
(273, 14, '::1', '2018-07-04 09:40:04', '2018-07-04 09:40:04'),
(274, 1, '::1', '2018-07-04 11:09:29', '2018-07-04 11:09:29'),
(275, 1, '::1', '2018-07-04 12:17:59', '2018-07-04 12:17:59'),
(276, 14, '::1', '2018-07-04 12:54:38', '2018-07-04 12:54:38'),
(277, 1, '::1', '2018-07-04 12:55:28', '2018-07-04 12:55:28'),
(278, 1, '::1', '2018-07-05 02:05:11', '2018-07-05 02:05:11'),
(279, 14, '::1', '2018-07-05 03:47:36', '2018-07-05 03:47:36'),
(280, 1, '::1', '2018-07-05 03:56:41', '2018-07-05 03:56:41'),
(281, 14, '::1', '2018-07-05 04:41:41', '2018-07-05 04:41:41'),
(282, 1, '::1', '2018-07-05 12:49:03', '2018-07-05 12:49:03'),
(283, 1, '::1', '2018-07-05 13:19:18', '2018-07-05 13:19:18'),
(284, 14, '::1', '2018-07-06 05:44:43', '2018-07-06 05:44:43'),
(285, 14, '::1', '2018-07-06 09:31:26', '2018-07-06 09:31:26'),
(286, 1, '::1', '2018-07-06 09:38:58', '2018-07-06 09:38:58'),
(287, 14, '::1', '2018-07-06 09:51:49', '2018-07-06 09:51:49'),
(288, 1, '::1', '2018-07-06 09:58:32', '2018-07-06 09:58:32'),
(289, 14, '::1', '2018-07-06 10:08:54', '2018-07-06 10:08:54'),
(290, 1, '::1', '2018-07-06 10:29:27', '2018-07-06 10:29:27'),
(291, 1, '::1', '2018-07-06 13:07:23', '2018-07-06 13:07:23'),
(292, 14, '::1', '2018-07-07 02:58:36', '2018-07-07 02:58:36'),
(293, 14, '::1', '2018-07-07 04:17:55', '2018-07-07 04:17:55'),
(294, 1, '::1', '2018-07-07 04:39:41', '2018-07-07 04:39:41'),
(295, 11, '::1', '2018-07-07 04:40:37', '2018-07-07 04:40:37'),
(296, 1, '::1', '2018-07-07 04:41:43', '2018-07-07 04:41:43'),
(297, 1, '::1', '2018-07-07 05:44:22', '2018-07-07 05:44:22'),
(298, 1, '::1', '2018-07-07 11:34:44', '2018-07-07 11:34:44'),
(299, 1, '::1', '2018-07-07 11:35:21', '2018-07-07 11:35:21'),
(300, 14, '::1', '2018-07-07 12:30:24', '2018-07-07 12:30:24'),
(301, 1, '::1', '2018-07-09 04:11:56', '2018-07-09 04:11:56'),
(302, 1, '::1', '2018-07-09 04:45:28', '2018-07-09 04:45:28'),
(303, 14, '::1', '2018-07-09 05:22:14', '2018-07-09 05:22:14'),
(304, 14, '::1', '2018-07-09 06:09:44', '2018-07-09 06:09:44'),
(305, 14, '::1', '2018-07-09 06:49:25', '2018-07-09 06:49:25'),
(306, 14, '::1', '2018-07-09 06:50:10', '2018-07-09 06:50:10'),
(307, 14, '::1', '2018-07-09 12:54:40', '2018-07-09 12:54:40'),
(308, 14, '::1', '2018-07-10 02:39:35', '2018-07-10 02:39:35'),
(309, 1, '::1', '2018-07-10 03:03:40', '2018-07-10 03:03:40'),
(310, 14, '::1', '2018-07-10 05:19:55', '2018-07-10 05:19:55'),
(311, 1, '::1', '2018-07-10 06:21:06', '2018-07-10 06:21:06'),
(312, 1, '::1', '2018-07-10 06:57:13', '2018-07-10 06:57:13'),
(313, 14, '::1', '2018-07-10 10:16:23', '2018-07-10 10:16:23'),
(314, 1, '::1', '2018-07-10 11:53:32', '2018-07-10 11:53:32'),
(315, 14, '::1', '2018-07-10 12:20:48', '2018-07-10 12:20:48'),
(316, 14, '::1', '2018-07-10 12:46:21', '2018-07-10 12:46:21'),
(317, 14, '::1', '2018-07-10 12:58:33', '2018-07-10 12:58:33'),
(318, 14, '::1', '2018-07-11 04:28:16', '2018-07-11 04:28:16'),
(319, 1, '::1', '2018-07-11 19:41:27', '2018-07-11 19:41:27'),
(320, 14, '::1', '2018-07-12 19:39:47', '2018-07-12 19:39:47'),
(321, 1, '::1', '2018-07-12 19:42:38', '2018-07-12 19:42:38'),
(322, 14, '::1', '2018-07-12 19:50:49', '2018-07-12 19:50:49'),
(323, 1, '::1', '2018-07-12 20:31:22', '2018-07-12 20:31:22'),
(324, 14, '::1', '2018-07-12 21:42:06', '2018-07-12 21:42:06'),
(325, 1, '::1', '2018-07-12 22:37:26', '2018-07-12 22:37:26'),
(326, 14, '::1', '2018-07-12 23:01:54', '2018-07-12 23:01:54'),
(327, 1, '::1', '2018-07-12 23:11:08', '2018-07-12 23:11:08'),
(328, 1, '::1', '2018-07-13 03:04:10', '2018-07-13 03:04:10'),
(329, 1, '::1', '2018-07-13 19:28:45', '2018-07-13 19:28:45'),
(330, 14, '::1', '2018-07-13 20:10:59', '2018-07-13 20:10:59'),
(331, 1, '::1', '2018-07-13 20:27:30', '2018-07-13 20:27:30'),
(332, 14, '::1', '2018-07-13 20:40:31', '2018-07-13 20:40:31'),
(333, 1, '::1', '2018-07-13 21:36:02', '2018-07-13 21:36:02'),
(334, 1, '::1', '2018-07-13 21:47:20', '2018-07-13 21:47:20'),
(335, 14, '::1', '2018-07-13 21:47:53', '2018-07-13 21:47:53'),
(336, 1, '::1', '2018-07-13 22:14:25', '2018-07-13 22:14:25'),
(337, 14, '::1', '2018-07-13 22:24:01', '2018-07-13 22:24:01'),
(338, 1, '::1', '2018-07-13 22:34:11', '2018-07-13 22:34:11'),
(339, 14, '::1', '2018-07-13 23:29:26', '2018-07-13 23:29:26'),
(340, 1, '::1', '2018-07-14 00:31:39', '2018-07-14 00:31:39'),
(341, 1, '::1', '2018-07-14 01:12:24', '2018-07-14 01:12:24'),
(342, 1, '::1', '2018-07-14 02:25:07', '2018-07-14 02:25:07'),
(343, 14, '::1', '2018-07-14 02:48:29', '2018-07-14 02:48:29'),
(344, 1, '::1', '2018-07-14 05:15:13', '2018-07-14 05:15:13'),
(345, 1, '::1', '2018-07-14 06:02:49', '2018-07-14 06:02:49'),
(346, 14, '::1', '2018-07-14 19:11:20', '2018-07-14 19:11:20'),
(347, 14, '::1', '2018-07-15 20:16:50', '2018-07-15 20:16:50'),
(348, 1, '::1', '2018-07-15 20:51:21', '2018-07-15 20:51:21'),
(349, 14, '::1', '2018-07-15 20:51:48', '2018-07-15 20:51:48'),
(350, 1, '::1', '2018-07-15 23:07:41', '2018-07-15 23:07:41'),
(351, 14, '::1', '2018-07-15 23:08:02', '2018-07-15 23:08:02'),
(352, 1, '::1', '2018-07-15 23:33:27', '2018-07-15 23:33:27'),
(353, 14, '::1', '2018-07-16 00:20:01', '2018-07-16 00:20:01'),
(354, 14, '::1', '2018-07-16 20:08:44', '2018-07-16 20:08:44'),
(355, 1, '::1', '2018-07-16 22:43:58', '2018-07-16 22:43:58'),
(356, 14, '::1', '2018-07-16 22:49:11', '2018-07-16 22:49:11'),
(357, 14, '::1', '2018-07-16 23:16:05', '2018-07-16 23:16:05'),
(358, 1, '::1', '2018-07-16 23:17:30', '2018-07-16 23:17:30'),
(359, 1, '::1', '2018-07-17 01:06:33', '2018-07-17 01:06:33'),
(360, 22, '::1', '2018-07-17 01:10:27', '2018-07-17 01:10:27'),
(361, 1, '::1', '2018-07-17 02:41:21', '2018-07-17 02:41:21'),
(362, 14, '::1', '2018-07-17 05:23:17', '2018-07-17 05:23:17'),
(363, 14, '::1', '2018-07-17 19:21:50', '2018-07-17 19:21:50'),
(364, 1, '::1', '2018-07-17 19:29:29', '2018-07-17 19:29:29'),
(365, 1, '::1', '2018-07-17 20:16:48', '2018-07-17 20:16:48'),
(366, 1, '::1', '2018-07-18 01:15:46', '2018-07-18 01:15:46'),
(367, 1, '::1', '2018-07-18 01:19:58', '2018-07-18 01:19:58'),
(368, 14, '::1', '2018-07-18 02:03:22', '2018-07-18 02:03:22'),
(369, 1, '::1', '2018-07-18 02:14:36', '2018-07-18 02:14:36'),
(370, 14, '::1', '2018-07-18 02:26:44', '2018-07-18 02:26:44'),
(371, 14, '::1', '2018-07-18 02:52:30', '2018-07-18 02:52:30'),
(372, 14, '::1', '2018-07-18 02:53:06', '2018-07-18 02:53:06'),
(373, 14, '::1', '2018-07-18 02:58:12', '2018-07-18 02:58:12'),
(374, 14, '::1', '2018-07-18 02:58:18', '2018-07-18 02:58:18'),
(375, 14, '::1', '2018-07-18 02:58:49', '2018-07-18 02:58:49'),
(376, 14, '::1', '2018-07-18 03:00:14', '2018-07-18 03:00:14'),
(377, 14, '::1', '2018-07-18 03:01:48', '2018-07-18 03:01:48'),
(378, 14, '::1', '2018-07-18 05:20:14', '2018-07-18 05:20:14'),
(379, 1, '::1', '2018-07-18 06:43:24', '2018-07-18 06:43:24'),
(380, 1, '::1', '2018-07-18 20:06:59', '2018-07-18 20:06:59'),
(381, 14, '::1', '2018-07-18 21:56:24', '2018-07-18 21:56:24'),
(382, 1, '::1', '2018-07-18 22:17:55', '2018-07-18 22:17:55'),
(383, 14, '::1', '2018-07-18 22:21:47', '2018-07-18 22:21:47'),
(384, 1, '::1', '2018-07-18 22:29:25', '2018-07-18 22:29:25'),
(385, 1, '::1', '2018-07-19 04:18:19', '2018-07-19 04:18:19'),
(386, 1, '::1', '2018-07-19 07:44:17', '2018-07-19 07:44:17'),
(387, 1, '::1', '2018-07-19 19:28:57', '2018-07-19 19:28:57'),
(388, 1, '::1', '2018-07-19 19:34:23', '2018-07-19 19:34:23'),
(389, 14, '::1', '2018-07-19 20:04:54', '2018-07-19 20:04:54'),
(390, 1, '::1', '2018-07-19 20:06:46', '2018-07-19 20:06:46'),
(391, 1, '::1', '2018-07-20 05:16:55', '2018-07-20 05:16:55'),
(392, 1, '::1', '2018-07-20 05:32:46', '2018-07-20 05:32:46'),
(393, 1, '::1', '2018-07-19 23:14:59', '2018-07-19 23:14:59'),
(394, 14, '::1', '2018-07-20 04:48:28', '2018-07-20 04:48:28'),
(395, 1, '::1', '2018-07-20 04:53:24', '2018-07-20 04:53:24'),
(396, 1, '::1', '2018-07-20 05:06:56', '2018-07-20 05:06:56'),
(397, 1, '::1', '2018-07-20 05:29:57', '2018-07-20 05:29:57'),
(398, 14, '::1', '2018-07-20 05:40:16', '2018-07-20 05:40:16'),
(399, 1, '::1', '2018-07-22 20:25:45', '2018-07-22 20:25:45'),
(400, 1, '::1', '2018-07-22 22:49:58', '2018-07-22 22:49:58'),
(401, 14, '::1', '2018-07-22 23:18:28', '2018-07-22 23:18:28'),
(402, 14, '::1', '2018-07-24 03:51:23', '2018-07-24 03:51:23'),
(403, 1, '::1', '2018-07-24 04:19:05', '2018-07-24 04:19:05'),
(404, 14, '::1', '2018-07-24 05:27:51', '2018-07-24 05:27:51'),
(405, 14, '::1', '2018-07-24 20:02:45', '2018-07-24 20:02:45'),
(406, 1, '::1', '2018-07-24 21:51:21', '2018-07-24 21:51:21'),
(407, 14, '::1', '2018-07-24 22:03:37', '2018-07-24 22:03:37'),
(408, 14, '::1', '2018-07-24 23:31:05', '2018-07-24 23:31:05'),
(409, 14, '::1', '2018-07-24 23:31:37', '2018-07-24 23:31:37'),
(410, 14, '::1', '2018-07-25 07:50:38', '2018-07-25 07:50:38'),
(411, 1, '::1', '2018-07-25 22:34:09', '2018-07-25 22:34:09'),
(412, 14, '::1', '2018-07-27 20:33:59', '2018-07-27 20:33:59'),
(413, 1, '::1', '2018-07-27 20:45:01', '2018-07-27 20:45:01'),
(414, 14, '::1', '2018-07-28 00:23:17', '2018-07-28 00:23:17'),
(415, 1, '::1', '2018-07-28 02:27:14', '2018-07-28 02:27:14'),
(416, 14, '::1', '2018-07-28 02:27:54', '2018-07-28 02:27:54'),
(417, 1, '::1', '2018-07-28 02:29:50', '2018-07-28 02:29:50'),
(418, 14, '::1', '2018-07-29 19:17:53', '2018-07-29 19:17:53'),
(419, 14, '::1', '2018-07-30 01:27:27', '2018-07-30 01:27:27'),
(420, 1, '::1', '2018-07-30 19:35:11', '2018-07-30 19:35:11'),
(421, 22, '::1', '2018-07-30 19:39:54', '2018-07-30 19:39:54'),
(422, 1, '::1', '2018-07-30 19:40:10', '2018-07-30 19:40:10'),
(423, 14, '::1', '2018-07-30 19:50:20', '2018-07-30 19:50:20'),
(424, 1, '::1', '2018-07-30 20:03:20', '2018-07-30 20:03:20'),
(425, 13, '::1', '2018-07-30 20:05:04', '2018-07-30 20:05:04'),
(426, 1, '::1', '2018-07-30 22:11:31', '2018-07-30 22:11:31'),
(427, 14, '::1', '2018-07-30 22:13:32', '2018-07-30 22:13:32'),
(428, 14, '::1', '2018-07-31 01:01:55', '2018-07-31 01:01:55'),
(429, 1, '::1', '2018-07-31 04:47:24', '2018-07-31 04:47:24'),
(430, 14, '::1', '2018-07-31 04:55:12', '2018-07-31 04:55:12'),
(431, 1, '::1', '2018-07-31 19:34:22', '2018-07-31 19:34:22'),
(432, 14, '::1', '2018-07-31 21:06:27', '2018-07-31 21:06:27'),
(433, 14, '::1', '2018-08-01 01:40:11', '2018-08-01 01:40:11'),
(434, 1, '::1', '2018-08-01 02:59:47', '2018-08-01 02:59:47'),
(435, 14, '::1', '2018-08-01 03:00:55', '2018-08-01 03:00:55'),
(436, 14, '::1', '2018-08-01 19:08:30', '2018-08-01 19:08:30'),
(437, 1, '::1', '2018-08-01 19:12:16', '2018-08-01 19:12:16'),
(438, 14, '::1', '2018-08-01 20:22:15', '2018-08-01 20:22:15'),
(439, 1, '::1', '2018-08-01 23:18:32', '2018-08-01 23:18:32'),
(440, 1, '::1', '2018-08-01 23:42:51', '2018-08-01 23:42:51'),
(441, 14, '::1', '2018-08-02 00:54:41', '2018-08-02 00:54:41'),
(442, 1, '::1', '2018-08-02 00:54:50', '2018-08-02 00:54:50'),
(443, 14, '::1', '2018-08-02 01:08:53', '2018-08-02 01:08:53'),
(444, 14, '::1', '2018-08-02 04:32:49', '2018-08-02 04:32:49'),
(445, 14, '::1', '2018-08-02 19:55:02', '2018-08-02 19:55:02'),
(446, 14, '::1', '2018-08-04 04:40:44', '2018-08-04 04:40:44'),
(447, 14, '::1', '2018-08-06 03:38:38', '2018-08-06 03:38:38'),
(448, 1, '::1', '2018-08-06 06:05:57', '2018-08-06 06:05:57'),
(449, 1, '::1', '2018-08-06 19:30:57', '2018-08-06 19:30:57'),
(450, 14, '::1', '2018-08-06 19:31:30', '2018-08-06 19:31:30'),
(451, 1, '::1', '2018-08-06 19:34:21', '2018-08-06 19:34:21'),
(452, 1, '::1', '2018-08-07 00:38:53', '2018-08-07 00:38:53'),
(453, 14, '::1', '2018-08-07 00:53:47', '2018-08-07 00:53:47'),
(454, 1, '::1', '2018-08-07 01:14:59', '2018-08-07 01:14:59'),
(455, 1, '::1', '2018-08-07 01:24:02', '2018-08-07 01:24:02'),
(456, 1, '::1', '2018-08-07 01:37:47', '2018-08-07 01:37:47'),
(457, 1, '::1', '2018-08-07 03:48:28', '2018-08-07 03:48:28'),
(458, 1, '::1', '2018-08-07 19:27:51', '2018-08-07 19:27:51'),
(459, 1, '::1', '2018-08-07 20:25:30', '2018-08-07 20:25:30'),
(460, 19, '::1', '2018-08-07 20:40:22', '2018-08-07 20:40:22'),
(461, 14, '::1', '2018-08-08 21:19:01', '2018-08-08 21:19:01'),
(462, 1, '::1', '2018-08-09 02:30:30', '2018-08-09 02:30:30'),
(463, 14, '::1', '2018-08-09 19:16:59', '2018-08-09 19:16:59'),
(464, 14, '::1', '2018-08-09 23:55:09', '2018-08-09 23:55:09'),
(465, 1, '::1', '2018-08-13 20:44:08', '2018-08-13 20:44:08'),
(466, 1, '::1', '2018-08-15 19:08:56', '2018-08-15 19:08:56'),
(467, 1, '::1', '2018-08-15 20:00:16', '2018-08-15 20:00:16'),
(468, 14, '::1', '2018-08-15 20:16:33', '2018-08-15 20:16:33'),
(469, 1, '::1', '2018-08-15 22:02:15', '2018-08-15 22:02:15'),
(470, 1, '::1', '2018-08-16 02:42:12', '2018-08-16 02:42:12'),
(471, 1, '::1', '2018-08-16 19:03:07', '2018-08-16 19:03:07'),
(472, 1, '::1', '2018-08-17 03:24:42', '2018-08-17 03:24:42'),
(473, 1, '::1', '2018-08-17 19:38:32', '2018-08-17 19:38:32'),
(474, 1, '::1', '2018-08-17 21:20:16', '2018-08-17 21:20:16'),
(475, 1, '::1', '2018-08-27 21:41:31', '2018-08-27 21:41:31'),
(476, 1, '::1', '2018-08-27 23:16:03', '2018-08-27 23:16:03'),
(477, 14, '::1', '2018-08-28 03:37:27', '2018-08-28 03:37:27'),
(478, 14, '::1', '2018-08-28 03:38:41', '2018-08-28 03:38:41'),
(479, 14, '::1', '2018-08-28 03:39:01', '2018-08-28 03:39:01'),
(480, 1, '::1', '2018-08-28 03:52:12', '2018-08-28 03:52:12'),
(481, 14, '::1', '2018-08-28 03:52:17', '2018-08-28 03:52:17'),
(482, 14, '::1', '2018-08-28 19:23:04', '2018-08-28 19:23:04'),
(483, 1, '::1', '2018-08-28 19:24:35', '2018-08-28 19:24:35'),
(484, 1, '::1', '2018-08-28 19:26:48', '2018-08-28 19:26:48'),
(485, 14, '::1', '2018-08-29 02:12:12', '2018-08-29 02:12:12'),
(486, 1, '::1', '2018-08-29 02:48:22', '2018-08-29 02:48:22'),
(487, 1, '::1', '2018-08-29 04:46:01', '2018-08-29 04:46:01'),
(488, 1, '::1', '2018-08-29 23:38:00', '2018-08-29 23:38:00'),
(489, 1, '::1', '2018-08-30 03:15:51', '2018-08-30 03:15:51'),
(490, 1, '::1', '2018-08-30 23:09:47', '2018-08-30 23:09:47'),
(491, 1, '::1', '2018-09-03 19:11:08', '2018-09-03 19:11:08'),
(492, 1, '::1', '2018-09-04 19:54:52', '2018-09-04 19:54:52'),
(493, 14, '::1', '2018-09-04 20:04:33', '2018-09-04 20:04:33'),
(494, 1, '::1', '2018-09-04 20:05:26', '2018-09-04 20:05:26'),
(495, 1, '::1', '2018-09-04 20:14:18', '2018-09-04 20:14:18'),
(496, 14, '::1', '2018-09-04 20:15:12', '2018-09-04 20:15:12'),
(497, 1, '::1', '2018-09-05 01:50:05', '2018-09-05 01:50:05'),
(498, 1, '::1', '2018-09-05 20:11:07', '2018-09-05 20:11:07'),
(499, 1, '::1', '2018-09-08 01:58:45', '2018-09-08 01:58:45'),
(500, 1, '::1', '2018-09-11 20:47:14', '2018-09-11 20:47:14'),
(501, 14, '::1', '2018-09-11 22:05:37', '2018-09-11 22:05:37'),
(502, 1, '::1', '2018-09-12 04:13:21', '2018-09-12 04:13:21'),
(503, 14, '::1', '2018-09-12 04:53:10', '2018-09-12 04:53:10'),
(504, 1, '::1', '2018-09-12 21:46:01', '2018-09-12 21:46:01'),
(505, 1, '::1', '2018-09-12 22:31:39', '2018-09-12 22:31:39'),
(506, 1, '::1', '2018-09-13 00:40:47', '2018-09-13 00:40:47'),
(507, 14, '::1', '2018-09-13 01:50:03', '2018-09-13 01:50:03'),
(508, 14, '::1', '2018-09-13 02:06:23', '2018-09-13 02:06:23'),
(509, 1, '::1', '2018-09-13 02:06:40', '2018-09-13 02:06:40'),
(510, 14, '::1', '2018-09-13 19:11:04', '2018-09-13 19:11:04'),
(511, 1, '::1', '2018-09-13 20:17:26', '2018-09-13 20:17:26'),
(512, 14, '::1', '2018-09-13 20:26:21', '2018-09-13 20:26:21'),
(513, 1, '::1', '2018-09-13 20:28:13', '2018-09-13 20:28:13'),
(514, 14, '::1', '2018-09-13 20:30:59', '2018-09-13 20:30:59'),
(515, 1, '::1', '2018-09-13 20:36:06', '2018-09-13 20:36:06'),
(516, 14, '::1', '2018-09-14 02:21:00', '2018-09-14 02:21:00'),
(517, 1, '::1', '2018-09-14 02:25:33', '2018-09-14 02:25:33'),
(518, 1, '::1', '2018-09-14 19:07:11', '2018-09-14 19:07:11'),
(519, 1, '::1', '2018-09-14 21:31:26', '2018-09-14 21:31:26'),
(520, 14, '::1', '2018-09-14 21:33:08', '2018-09-14 21:33:08'),
(521, 1, '::1', '2018-09-14 22:05:11', '2018-09-14 22:05:11'),
(522, 1, '::1', '2018-09-14 23:09:53', '2018-09-14 23:09:53'),
(523, 14, '::1', '2018-09-15 19:01:26', '2018-09-15 19:01:26'),
(524, 1, '::1', '2018-09-15 22:38:37', '2018-09-15 22:38:37'),
(525, 14, '::1', '2018-09-16 01:16:02', '2018-09-16 01:16:02'),
(526, 1, '::1', '2018-09-16 02:31:03', '2018-09-16 02:31:03'),
(527, 14, '::1', '2018-09-16 21:39:33', '2018-09-16 21:39:33'),
(528, 1, '::1', '2018-09-16 23:10:23', '2018-09-16 23:10:23'),
(529, 14, '::1', '2018-09-16 23:22:15', '2018-09-16 23:22:15'),
(530, 14, '::1', '2018-09-17 04:33:25', '2018-09-17 04:33:25'),
(531, 1, '::1', '2018-09-17 20:17:04', '2018-09-17 20:17:04'),
(532, 14, '::1', '2018-09-17 20:24:01', '2018-09-17 20:24:01'),
(533, 1, '::1', '2018-09-18 01:43:16', '2018-09-18 01:43:16'),
(534, 1, '::1', '2018-09-18 19:09:55', '2018-09-18 19:09:55'),
(535, 14, '::1', '2018-09-18 23:57:30', '2018-09-18 23:57:30'),
(536, 14, '::1', '2018-09-19 00:21:22', '2018-09-19 00:21:22'),
(537, 14, '::1', '2018-09-19 20:57:45', '2018-09-19 20:57:45'),
(538, 1, '::1', '2018-09-20 21:44:52', '2018-09-20 21:44:52'),
(539, 1, '::1', '2018-09-21 15:07:25', '2018-09-21 15:07:25'),
(540, 14, '::1', '2018-09-24 20:21:08', '2018-09-24 20:21:08'),
(541, 14, '::1', '2018-09-25 20:56:21', '2018-09-25 20:56:21'),
(542, 14, '::1', '2018-09-26 01:00:21', '2018-09-26 01:00:21'),
(543, 14, '::1', '2018-09-27 20:01:42', '2018-09-27 20:01:42'),
(544, 14, '::1', '2018-09-27 23:38:10', '2018-09-27 23:38:10'),
(545, 14, '::1', '2018-09-28 05:36:01', '2018-09-28 05:36:01'),
(546, 1, '::1', '2018-09-29 23:59:26', '2018-09-29 23:59:26'),
(547, 14, '::1', '2018-09-30 15:22:25', '2018-09-30 15:22:25'),
(548, 1, '::1', '2018-09-30 17:23:33', '2018-09-30 17:23:33'),
(549, 1, '::1', '2018-09-30 20:40:40', '2018-09-30 20:40:40'),
(550, 14, '::1', '2018-10-05 20:38:27', '2018-10-05 20:38:27'),
(551, 14, '::1', '2018-10-06 04:04:42', '2018-10-06 04:04:42'),
(552, 1, '::1', '2018-10-06 04:05:15', '2018-10-06 04:05:15'),
(553, 1, '::1', '2018-10-06 04:08:48', '2018-10-06 04:08:48'),
(554, 1, '::1', '2018-10-07 00:14:36', '2018-10-07 00:14:36'),
(555, 1, '::1', '2018-10-07 00:15:13', '2018-10-07 00:15:13'),
(556, 1, '::1', '2018-10-07 01:33:47', '2018-10-07 01:33:47'),
(557, 1, '::1', '2018-10-09 01:51:28', '2018-10-09 01:51:28'),
(558, 1, '::1', '2018-10-16 19:12:04', '2018-10-16 19:12:04'),
(559, 1, '::1', '2018-10-16 23:39:36', '2018-10-16 23:39:36'),
(560, 14, '::1', '2018-10-17 00:37:47', '2018-10-17 00:37:47'),
(561, 1, '::1', '2018-10-17 19:39:18', '2018-10-17 19:39:18'),
(562, 14, '::1', '2018-10-17 20:01:58', '2018-10-17 20:01:58'),
(563, 1, '::1', '2018-10-18 00:44:39', '2018-10-18 00:44:39'),
(564, 14, '::1', '2018-10-18 03:56:14', '2018-10-18 03:56:14'),
(565, 14, '::1', '2018-10-18 05:08:46', '2018-10-18 05:08:46'),
(566, 14, '::1', '2018-10-22 21:06:59', '2018-10-22 21:06:59'),
(567, 1, '::1', '2018-10-25 21:19:06', '2018-10-25 21:19:06'),
(568, 1, '::1', '2018-10-26 22:11:56', '2018-10-26 22:11:56'),
(569, 1, '::1', '2018-10-27 00:41:27', '2018-10-27 00:41:27'),
(570, 14, '::1', '2018-10-29 02:09:20', '2018-10-29 02:09:20'),
(571, 1, '::1', '2018-11-01 22:49:30', '2018-11-01 22:49:30'),
(572, 1, '::1', '2018-11-04 20:16:20', '2018-11-04 20:16:20'),
(573, 1, '::1', '2018-11-04 23:25:35', '2018-11-04 23:25:35'),
(574, 1, '::1', '2018-11-05 21:20:30', '2018-11-05 21:20:30'),
(575, 14, '::1', '2018-11-16 23:29:50', '2018-11-16 23:29:50'),
(576, 1, '::1', '2018-11-18 20:16:31', '2018-11-18 20:16:31'),
(577, 1, '::1', '2018-11-20 19:44:33', '2018-11-20 19:44:33'),
(578, 14, '::1', '2018-11-20 20:02:50', '2018-11-20 20:02:50'),
(579, 1, '::1', '2018-11-20 20:09:31', '2018-11-20 20:09:31'),
(580, 1, '::1', '2018-11-23 02:13:58', '2018-11-23 02:13:58'),
(581, 14, '::1', '2018-11-23 19:30:25', '2018-11-23 19:30:25'),
(582, 1, '::1', '2018-11-26 18:13:58', '2018-11-26 18:13:58'),
(583, 1, '::1', '2018-11-28 20:23:43', '2018-11-28 20:23:43'),
(584, 1, '::1', '2018-11-29 18:45:25', '2018-11-29 18:45:25'),
(585, 1, '::1', '2018-12-05 18:50:12', '2018-12-05 18:50:12'),
(586, 1, '::1', '2018-12-06 22:43:13', '2018-12-06 22:43:13'),
(587, 1, '::1', '2018-12-09 19:32:44', '2018-12-09 19:32:44'),
(588, 1, '::1', '2018-12-09 19:46:51', '2018-12-09 19:46:51'),
(589, 1, '::1', '2018-12-09 21:26:45', '2018-12-09 21:26:45'),
(590, 1, '::1', '2018-12-10 21:21:19', '2018-12-10 21:21:19'),
(591, 1, '::1', '2018-12-10 22:21:04', '2018-12-10 22:21:04'),
(592, 1, '::1', '2018-12-11 02:43:05', '2018-12-11 02:43:05'),
(593, 1, '::1', '2018-12-11 18:30:43', '2018-12-11 18:30:43'),
(594, 1, '::1', '2018-12-11 18:56:53', '2018-12-11 18:56:53'),
(595, 1, '::1', '2018-12-11 23:12:53', '2018-12-11 23:12:53'),
(596, 1, '::1', '2018-12-12 00:03:05', '2018-12-12 00:03:05'),
(597, 1, '::1', '2018-12-12 18:10:53', '2018-12-12 18:10:53'),
(598, 1, '::1', '2018-12-13 01:45:11', '2018-12-13 01:45:11'),
(599, 1, '::1', '2018-12-13 01:57:55', '2018-12-13 01:57:55'),
(600, 1, '::1', '2018-12-13 22:02:25', '2018-12-13 22:02:25'),
(601, 1, '::1', '2018-12-13 22:05:08', '2018-12-13 22:05:08'),
(602, 1, '::1', '2018-12-14 20:26:25', '2018-12-14 20:26:25'),
(603, 1, '::1', '2018-12-14 20:46:28', '2018-12-14 20:46:28'),
(604, 1, '::1', '2018-12-17 19:14:17', '2018-12-17 19:14:17'),
(605, 1, '::1', '2018-12-18 04:48:52', '2018-12-18 04:48:52'),
(606, 1, '::1', '2018-12-18 18:22:47', '2018-12-18 18:22:47'),
(607, 1, '::1', '2018-12-18 18:50:49', '2018-12-18 18:50:49'),
(608, 1, '::1', '2018-12-19 02:00:43', '2018-12-19 02:00:43'),
(609, 1, '::1', '2018-12-19 02:22:31', '2018-12-19 02:22:31'),
(610, 1, '::1', '2018-12-19 02:26:37', '2018-12-19 02:26:37'),
(611, 1, '::1', '2018-12-19 18:08:25', '2018-12-19 18:08:25'),
(612, 1, '::1', '2018-12-19 18:14:50', '2018-12-19 18:14:50'),
(613, 1, '::1', '2019-01-07 23:18:52', '2019-01-07 23:18:52');

-- --------------------------------------------------------

--
-- Table structure for table `made_in_countries`
--

DROP TABLE IF EXISTS `made_in_countries`;
CREATE TABLE IF NOT EXISTS `made_in_countries` (
  `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT,
  `name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `status` tinyint(1) NOT NULL,
  `default` tinyint(1) NOT NULL,
  `vendor_meta_id` int(11) NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=20 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `made_in_countries`
--

INSERT INTO `made_in_countries` (`id`, `name`, `status`, `default`, `vendor_meta_id`, `created_at`, `updated_at`, `deleted_at`) VALUES
(1, 'IMPORT', 1, 1, 1, '2018-05-12 05:03:14', '2018-06-25 08:01:22', NULL),
(11, 'ee', 1, 0, 1, '2018-05-12 06:08:15', '2018-05-12 06:28:46', '2018-05-12 06:28:46'),
(12, 'd', 0, 0, 1, '2018-05-12 06:20:36', '2018-05-22 14:43:53', '2018-05-22 14:43:53'),
(13, '2', 0, 0, 1, '2018-05-12 06:43:58', '2018-05-12 06:44:11', '2018-05-12 06:44:11'),
(8, '4', 1, 0, 1, '2018-05-12 05:42:22', '2018-05-22 14:43:49', '2018-05-22 14:43:49'),
(10, 'aa', 1, 0, 1, '2018-05-12 05:46:40', '2018-05-22 14:43:51', '2018-05-22 14:43:51'),
(14, 'dfd', 1, 0, 1, '2018-05-12 09:27:19', '2018-05-22 14:43:54', '2018-05-22 14:43:54'),
(15, 'dda3', 0, 0, 1, '2018-05-17 01:30:11', '2018-05-17 01:30:28', '2018-05-17 01:30:28'),
(16, 'USA', 1, 0, 1, '2018-05-22 14:44:10', '2018-06-25 08:01:24', NULL),
(17, 'IMPORT', 1, 0, 2, '2018-05-23 15:22:38', '2018-05-23 15:22:38', NULL),
(18, 'USA', 1, 0, 2, '2018-05-23 15:22:47', '2018-05-23 15:22:47', NULL),
(19, 'd', 1, 0, 1, '2018-06-25 08:01:29', '2018-06-25 08:01:33', '2018-06-25 08:01:33');

-- --------------------------------------------------------

--
-- Table structure for table `master_colors`
--

DROP TABLE IF EXISTS `master_colors`;
CREATE TABLE IF NOT EXISTS `master_colors` (
  `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT,
  `name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `image_path` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=23 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `master_colors`
--

INSERT INTO `master_colors` (`id`, `name`, `image_path`, `created_at`, `updated_at`, `deleted_at`) VALUES
(8, 'Black', '/images/master_color/4e77ad10-5df2-11e8-9866-35f0a9aad927.jpg', '2018-05-22 12:59:51', '2018-05-22 12:59:51', NULL),
(7, 'Red', '/images/master_color/3586a0b0-5df2-11e8-8423-fbb0e7848eaf.jpg', '2018-05-22 12:59:09', '2018-05-22 12:59:09', NULL),
(9, 'Blue', '/images/master_color/5e75f940-5df2-11e8-8b31-53e1d9dda377.jpg', '2018-05-22 13:00:18', '2018-05-22 13:00:18', NULL),
(10, 'Brown', '/images/master_color/791e7f20-5df2-11e8-b6ea-f9806acbee22.jpg', '2018-05-22 13:01:03', '2018-05-22 13:01:03', NULL),
(11, 'Gold', '/images/master_color/871c7b10-5df2-11e8-bd06-5d3d96e22977.jpg', '2018-05-22 13:01:26', '2018-05-22 13:01:26', NULL),
(12, 'Gray', '/images/master_color/97251680-5df2-11e8-80d0-0b45ed04fbf9.jpg', '2018-05-22 13:01:53', '2018-05-22 13:01:53', NULL),
(13, 'Green', '/images/master_color/ad0764b0-5df2-11e8-bf69-49778d6fff9a.jpg', '2018-05-22 13:02:30', '2018-05-22 13:02:30', NULL),
(14, 'Ivory', '/images/master_color/c1f2e920-5df2-11e8-8da5-5b7f0944a348.jpg', '2018-05-22 13:03:05', '2018-05-22 13:03:05', NULL),
(15, 'Multi', '/images/master_color/d3727d00-5df2-11e8-8efd-e3a8ede6be2e.jpg', '2018-05-22 13:03:34', '2018-05-22 13:03:34', NULL),
(16, 'Orange', '/images/master_color/e5b44320-5df2-11e8-8331-275f5efbdb75.jpg', '2018-05-22 13:04:05', '2018-05-22 13:04:05', NULL),
(17, 'Pink', '/images/master_color/f9303bb0-5df2-11e8-a50f-230282c65397.jpg', '2018-05-22 13:04:37', '2018-05-22 13:04:37', NULL),
(18, 'Purple', '/images/master_color/12f03b80-5df3-11e8-a134-07210ad6c7cb.jpg', '2018-05-22 13:05:21', '2018-05-22 13:05:21', NULL),
(19, 'Silver', '/images/master_color/283ad410-5df3-11e8-878c-15abac44d755.jpg', '2018-05-22 13:05:56', '2018-05-22 13:05:56', NULL),
(20, 'Beige', '/images/master_color/3bbf2f50-5df3-11e8-861a-ff67af98860e.jpg', '2018-05-22 13:06:29', '2018-05-22 13:06:29', NULL),
(21, 'White', '/images/master_color/4b694870-5df3-11e8-a72e-7702ab347774.jpg', '2018-05-22 13:06:55', '2018-05-22 13:06:55', NULL),
(22, 'Yellow', '/images/master_color/5b5a3520-5df3-11e8-a6bc-bd02725fcab8.jpg', '2018-05-22 13:07:22', '2018-05-22 13:07:22', NULL);

-- --------------------------------------------------------

--
-- Table structure for table `master_fabrics`
--

DROP TABLE IF EXISTS `master_fabrics`;
CREATE TABLE IF NOT EXISTS `master_fabrics` (
  `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT,
  `name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=48 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `master_fabrics`
--

INSERT INTO `master_fabrics` (`id`, `name`, `created_at`, `updated_at`, `deleted_at`) VALUES
(1, 'Canvas', '2018-05-12 08:04:56', '2018-05-22 13:10:03', NULL),
(2, 'Chiffon', '2018-05-12 08:05:13', '2018-05-22 13:10:12', NULL),
(3, 'Acid Wash', '2018-05-12 08:08:01', '2018-05-22 13:09:48', NULL),
(4, 'Corduroy', '2018-05-22 13:10:23', '2018-05-22 13:10:23', NULL),
(5, 'Cotton', '2018-05-22 13:10:32', '2018-05-22 13:10:32', NULL),
(6, 'Crystal', '2018-05-22 13:10:37', '2018-05-22 13:10:37', NULL),
(7, 'Dark Wash', '2018-05-22 13:10:49', '2018-05-22 13:10:49', NULL),
(8, 'Denim', '2018-05-22 13:11:21', '2018-05-22 13:11:21', NULL),
(9, 'Faux Leather', '2018-05-22 13:11:27', '2018-05-22 13:11:27', NULL),
(10, 'Faux Pearl', '2018-05-22 13:11:32', '2018-05-22 13:11:32', NULL),
(11, 'Genuine Stones', '2018-05-22 13:11:38', '2018-05-22 13:11:38', NULL),
(12, 'Jersey', '2018-05-22 13:11:43', '2018-05-22 13:11:43', NULL),
(13, 'Lace', '2018-05-22 13:11:49', '2018-05-22 13:11:49', NULL),
(14, 'Leather', '2018-05-22 13:12:06', '2018-05-22 13:12:06', NULL),
(15, 'Leather/Pleather', '2018-05-22 13:12:11', '2018-05-22 13:12:11', NULL),
(16, 'Light Wash', '2018-05-22 13:12:18', '2018-05-22 13:12:18', NULL),
(17, 'Linen', '2018-05-22 13:12:23', '2018-05-22 13:12:23', NULL),
(18, 'Mesh', '2018-05-22 13:12:28', '2018-05-22 13:12:28', NULL),
(19, 'Metal', '2018-05-22 13:12:34', '2018-05-22 13:12:34', NULL),
(20, 'Patent Leather', '2018-05-22 13:12:38', '2018-05-22 13:12:38', NULL),
(21, 'Pearl', '2018-05-22 13:12:44', '2018-05-22 13:12:44', NULL),
(22, 'Plastic', '2018-05-22 13:12:51', '2018-05-22 13:12:51', NULL),
(23, 'Pleather', '2018-05-22 13:12:55', '2018-05-22 13:12:55', NULL),
(24, 'Polyester', '2018-05-22 13:12:59', '2018-05-22 13:12:59', NULL),
(25, 'Rhinestones', '2018-05-22 13:13:04', '2018-05-22 13:13:04', NULL),
(26, 'Satin', '2018-05-22 13:13:08', '2018-05-22 13:13:08', NULL),
(27, 'Silk', '2018-05-22 13:13:15', '2018-05-22 13:13:15', NULL),
(28, 'Spandex', '2018-05-22 13:13:23', '2018-05-22 13:13:23', NULL),
(29, 'Stones', '2018-05-22 13:13:28', '2018-05-22 13:13:28', NULL),
(30, 'Straw', '2018-05-22 13:13:33', '2018-05-22 13:13:33', NULL),
(31, 'Suede', '2018-05-22 13:13:37', '2018-05-22 13:13:37', NULL),
(32, 'Taffeta', '2018-05-22 13:13:43', '2018-05-22 13:13:43', NULL),
(33, 'Terry/Velour', '2018-05-22 13:13:47', '2018-05-22 13:13:47', NULL),
(34, 'Wood', '2018-05-22 13:13:55', '2018-05-22 13:13:55', NULL),
(35, 'Wool', '2018-05-22 13:14:01', '2018-05-22 13:14:01', NULL),
(36, 'Rayon', '2018-05-22 13:14:06', '2018-05-22 13:14:06', NULL),
(37, 'Cotton/Poly', '2018-05-22 13:14:10', '2018-05-22 13:14:10', NULL),
(38, 'Viscose', '2018-05-22 13:14:17', '2018-05-22 13:14:17', NULL),
(39, 'Nylon', '2018-05-22 13:14:22', '2018-05-22 13:14:22', NULL),
(40, 'Medium Wash', '2018-05-22 13:14:27', '2018-05-22 13:14:27', NULL),
(41, 'White Denim', '2018-05-22 13:14:31', '2018-05-22 13:14:31', NULL),
(42, 'Black and Grey', '2018-05-22 13:14:36', '2018-05-22 13:14:36', NULL),
(43, 'Acrylic', '2018-05-22 13:14:42', '2018-05-22 13:14:42', NULL),
(44, 'Tencel', '2018-05-22 13:14:53', '2018-05-22 13:14:53', NULL),
(45, 'Cashmere', '2018-05-22 13:14:57', '2018-05-22 13:14:57', NULL),
(46, 'Modal', '2018-05-22 13:15:01', '2018-05-22 13:15:01', NULL),
(47, 'Velvet', '2018-05-22 13:15:06', '2018-05-22 13:15:06', NULL);

-- --------------------------------------------------------

--
-- Table structure for table `messages`
--

DROP TABLE IF EXISTS `messages`;
CREATE TABLE IF NOT EXISTS `messages` (
  `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT,
  `sender_id` int(11) NOT NULL,
  `sender_type` int(11) NOT NULL,
  `receiver_id` int(11) NOT NULL,
  `receiver_type` int(11) NOT NULL,
  `title` varchar(1000) COLLATE utf8mb4_unicode_ci NOT NULL,
  `topic` int(11) NOT NULL,
  `order_id` int(11) DEFAULT NULL,
  `receiver_seen_at` timestamp NULL DEFAULT NULL,
  `sender_seen_at` timestamp NULL DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=33 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `messages`
--

INSERT INTO `messages` (`id`, `sender_id`, `sender_type`, `receiver_id`, `receiver_type`, `title`, `topic`, `order_id`, `receiver_seen_at`, `sender_seen_at`, `created_at`, `updated_at`, `deleted_at`) VALUES
(1, 14, 1, 1, 2, 'werwer', 1, NULL, '2018-09-15 00:01:26', '2018-09-15 00:20:51', '2018-09-12 03:54:03', '2018-09-14 23:26:00', NULL),
(2, 1, 2, 14, 1, 'from vendor', 1, NULL, '2018-10-18 04:58:35', '2018-09-15 01:29:08', '2018-09-13 22:43:46', '2018-09-15 01:29:10', NULL),
(3, 1, 2, 14, 1, '2nd', 1, NULL, '2018-09-15 00:01:10', '2018-09-15 04:17:21', '2018-09-14 02:48:34', '2018-09-14 23:28:10', NULL),
(4, 14, 1, 1, 2, 'sdaf', 1, NULL, '2018-09-18 02:20:57', '2018-10-22 21:27:47', '2018-09-14 03:25:54', '2018-09-15 01:29:25', NULL),
(5, 14, 1, 1, 2, '90-', 1, NULL, '2018-09-15 00:12:58', '2018-09-15 01:38:33', '2018-09-14 03:43:26', '2018-09-14 23:15:49', NULL),
(6, 1, 2, 14, 1, '34', 1, NULL, '2018-09-15 00:24:04', '2018-09-15 00:12:31', '2018-09-14 23:33:46', '2018-09-14 23:36:42', NULL),
(7, 14, 1, 1, 2, 'p', 1, NULL, '2018-09-15 00:08:48', '2018-09-15 00:20:38', '2018-09-14 23:57:31', '2018-09-15 00:00:53', NULL),
(8, 1, 2, 14, 1, '345', 1, NULL, '2018-09-15 01:45:54', '2018-09-15 01:46:07', '2018-09-15 00:01:36', '2018-09-15 01:45:57', NULL),
(9, 14, 1, 1, 2, 'wer', 1, NULL, '2018-09-15 01:45:48', '2018-09-15 01:53:27', '2018-09-15 01:40:44', '2018-09-15 01:40:44', NULL),
(10, 1, 2, 14, 1, 'with file', 1, NULL, NULL, '2018-09-15 04:17:18', '2018-09-15 02:34:10', '2018-09-15 02:34:10', NULL),
(11, 14, 1, 1, 2, 'with file from buyer', 1, NULL, '2018-09-18 02:21:11', '2018-09-15 04:19:48', '2018-09-15 02:41:56', '2018-09-15 04:14:41', NULL),
(12, 1, 2, 14, 1, 'yu ffileeeee', 1, NULL, '2018-09-15 04:14:26', '2018-09-15 04:17:15', '2018-09-15 02:53:40', '2018-09-15 04:14:26', NULL),
(13, 1, 2, 14, 1, '34', 1, NULL, '2018-09-15 19:38:15', '2018-09-15 04:20:48', '2018-09-15 04:20:26', '2018-09-15 04:20:38', NULL),
(14, 0, 3, 14, 1, 'from admin', 1, NULL, '2018-09-16 23:37:33', '2018-09-15 20:46:39', '2018-09-15 19:49:24', '2018-09-15 20:46:28', NULL),
(15, 14, 1, 0, 0, '3423', 1, NULL, NULL, '2018-09-15 20:46:53', '2018-09-15 20:46:37', '2018-09-15 20:46:37', '2018-09-12 18:00:00'),
(16, 14, 1, 0, 3, 'e3r', 1, NULL, '2018-09-16 02:15:04', '2018-11-20 20:07:02', '2018-09-15 20:49:25', '2018-09-16 02:14:50', NULL),
(17, 0, 3, 1, 2, '33', 1, NULL, '2018-09-16 02:32:36', '2018-09-16 02:32:40', '2018-09-16 02:32:20', '2018-09-16 02:32:36', NULL),
(18, 14, 1, 1, 2, 'Regarding My Order 58MSPC4UL5SZ2', 3, 111, '2018-10-06 04:30:52', '2018-09-16 23:01:11', '2018-09-16 23:01:11', '2018-09-16 23:01:11', NULL),
(19, 14, 1, 1, 2, 'te', 1, NULL, '2018-09-18 02:19:49', '2018-09-16 23:02:38', '2018-09-16 23:02:38', '2018-09-16 23:02:38', NULL),
(20, 14, 1, 1, 2, 'Regarding My Order 8X3O3I8FDVPED', 3, 110, '2018-09-18 02:18:58', '2018-09-16 23:37:37', '2018-09-16 23:02:48', '2018-09-16 23:02:48', NULL),
(21, 14, 1, 1, 2, 'asdfasd', 1, NULL, NULL, '2018-11-16 23:34:08', '2018-11-16 23:34:08', '2018-11-16 23:34:08', NULL),
(22, 14, 1, 1, 2, 'asdf', 1, NULL, NULL, '2018-11-16 23:34:29', '2018-11-16 23:34:29', '2018-11-16 23:34:29', NULL),
(23, 14, 1, 1, 2, 'asdf', 1, NULL, NULL, '2018-11-16 23:35:59', '2018-11-16 23:35:59', '2018-11-16 23:35:59', NULL),
(24, 14, 1, 1, 2, 'asdf', 1, NULL, NULL, '2018-11-16 23:43:06', '2018-11-16 23:43:06', '2018-11-16 23:43:06', NULL),
(25, 14, 1, 1, 2, 'asdf', 1, NULL, NULL, '2018-11-16 23:48:56', '2018-11-16 23:48:56', '2018-11-16 23:48:56', NULL),
(26, 14, 1, 1, 2, 'asdf', 1, NULL, NULL, '2018-11-16 23:49:29', '2018-11-16 23:49:29', '2018-11-16 23:49:29', NULL),
(27, 14, 1, 1, 2, 'asdf', 1, NULL, NULL, '2018-11-16 23:50:10', '2018-11-16 23:50:10', '2018-11-16 23:50:10', NULL),
(28, 14, 1, 1, 2, 'asdf', 1, NULL, NULL, '2018-11-16 23:50:30', '2018-11-16 23:50:30', '2018-11-16 23:50:30', NULL),
(29, 14, 1, 1, 2, 'asdf', 1, NULL, NULL, '2018-11-16 23:50:47', '2018-11-16 23:50:47', '2018-11-16 23:50:47', NULL),
(30, 14, 1, 1, 2, 'asdf', 1, NULL, NULL, '2018-11-16 23:51:25', '2018-11-16 23:51:25', '2018-11-16 23:51:25', NULL),
(31, 14, 1, 1, 2, 'asdf', 1, NULL, NULL, '2018-11-16 23:51:54', '2018-11-16 23:51:54', '2018-11-16 23:51:54', NULL),
(32, 14, 1, 1, 2, 'asdf', 1, NULL, '2018-12-07 01:43:45', '2018-11-17 00:00:31', '2018-11-16 23:54:23', '2018-11-17 00:00:07', NULL);

-- --------------------------------------------------------

--
-- Table structure for table `message_files`
--

DROP TABLE IF EXISTS `message_files`;
CREATE TABLE IF NOT EXISTS `message_files` (
  `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT,
  `message_id` int(11) NOT NULL,
  `filename` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `original_filename` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `mime` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=6 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `message_files`
--

INSERT INTO `message_files` (`id`, `message_id`, `filename`, `original_filename`, `mime`, `created_at`, `updated_at`, `deleted_at`) VALUES
(1, 47, '84185a40-b8ff-11e8-bca7-957ee7782049.jpg', '39441647_1664980373630612_1553040511873318912_n.jpg', 'image/jpeg', '2018-09-15 02:53:40', '2018-09-15 02:53:40', NULL),
(2, 47, '8419a080-b8ff-11e8-b748-9bee91ddc0e8.jpg', '38836046_1042719259222315_8084384677466472448_n.jpg', 'image/jpeg', '2018-09-15 02:53:40', '2018-09-15 02:53:40', NULL),
(3, 53, '16c85140-b905-11e8-98f6-71c06b7626b5.jpg', '84185a40-b8ff-11e8-bca7-957ee7782049 (1).jpg', 'image/jpeg', '2018-09-15 03:33:34', '2018-09-15 03:33:34', NULL),
(4, 61, '80993f80-b909-11e8-aa5e-35327d882521.jpg', '37922641_996368580540451_6685028424840380416_n.jpg', 'image/jpeg', '2018-09-15 04:05:09', '2018-09-15 04:05:09', NULL),
(5, 66, '6974c870-b98d-11e8-b5dc-797f546b5321.jpg', '84185a40-b8ff-11e8-bca7-957ee7782049.jpg', 'image/jpeg', '2018-09-15 19:49:24', '2018-09-15 19:49:24', NULL);

-- --------------------------------------------------------

--
-- Table structure for table `message_items`
--

DROP TABLE IF EXISTS `message_items`;
CREATE TABLE IF NOT EXISTS `message_items` (
  `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT,
  `message_id` int(11) NOT NULL,
  `sender` tinyint(1) NOT NULL DEFAULT '1',
  `message` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `seen_at` timestamp NULL DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=95 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `message_items`
--

INSERT INTO `message_items` (`id`, `message_id`, `sender`, `message`, `seen_at`, `created_at`, `updated_at`) VALUES
(1, 1, 1, 'werwer', '2018-09-15 00:01:26', '2018-09-12 03:54:03', '2018-09-15 00:01:26'),
(2, 1, 0, 'hello', '2018-09-15 00:20:51', '2018-09-13 01:19:45', '2018-09-15 00:20:51'),
(3, 1, 0, 'sdf', '2018-09-15 00:20:51', '2018-09-13 01:20:22', '2018-09-15 00:20:51'),
(4, 1, 0, 'adsf ad\n\nasdf\nasd', '2018-09-15 00:20:51', '2018-09-13 01:38:03', '2018-09-15 00:20:51'),
(5, 1, 1, 'asdfa', '2018-09-15 00:01:26', '2018-09-13 03:08:12', '2018-09-15 00:01:26'),
(6, 1, 1, 'yyret', '2018-09-15 00:01:26', '2018-09-13 03:09:37', '2018-09-15 00:01:26'),
(7, 1, 0, 'hear me?', '2018-09-15 00:20:51', '2018-09-13 03:11:14', '2018-09-15 00:20:51'),
(8, 1, 1, 'dfd', '2018-09-15 00:01:26', '2018-09-13 03:13:39', '2018-09-15 00:01:26'),
(9, 1, 1, 'ggr', '2018-09-15 00:01:26', '2018-09-13 03:14:00', '2018-09-15 00:01:26'),
(10, 1, 0, 'wer', '2018-09-15 00:20:51', '2018-09-13 03:14:08', '2018-09-15 00:20:51'),
(11, 1, 0, 'nice to meet you', '2018-09-15 00:20:51', '2018-09-13 03:24:16', '2018-09-15 00:20:51'),
(12, 2, 1, 'dfaedf', '2018-10-18 04:58:35', '2018-09-13 22:43:46', '2018-10-18 04:58:35'),
(13, 2, 1, 'hear me?', '2018-10-18 04:58:35', '2018-09-14 02:25:51', '2018-10-18 04:58:35'),
(14, 2, 0, 'yeah!!!!', '2018-09-15 01:29:08', '2018-09-14 02:28:18', '2018-09-15 01:29:08'),
(15, 2, 1, 'nice', '2018-10-18 04:58:35', '2018-09-14 02:31:13', '2018-10-18 04:58:35'),
(16, 3, 1, 'werwer', '2018-09-15 00:01:10', '2018-09-14 02:48:34', '2018-09-15 00:01:10'),
(17, 2, 1, '3', '2018-10-18 04:58:35', '2018-09-14 02:48:43', '2018-10-18 04:58:35'),
(18, 2, 1, 'yu', '2018-10-18 04:58:35', '2018-09-14 02:49:16', '2018-10-18 04:58:35'),
(19, 3, 1, 'erer', '2018-09-15 00:01:10', '2018-09-14 02:55:00', '2018-09-15 00:01:10'),
(20, 2, 1, '4545', '2018-10-18 04:58:35', '2018-09-14 03:01:32', '2018-10-18 04:58:35'),
(21, 2, 1, 'er', '2018-10-18 04:58:35', '2018-09-14 03:02:02', '2018-10-18 04:58:35'),
(22, 2, 1, 'er', '2018-10-18 04:58:35', '2018-09-14 03:02:21', '2018-10-18 04:58:35'),
(23, 2, 1, 'dd', '2018-10-18 04:58:35', '2018-09-14 03:03:17', '2018-10-18 04:58:35'),
(24, 3, 1, '34', '2018-09-15 00:01:10', '2018-09-14 03:03:31', '2018-09-15 00:01:10'),
(25, 1, 0, '45', '2018-09-15 00:20:51', '2018-09-14 03:24:42', '2018-09-15 00:20:51'),
(26, 4, 1, 'erer', '2018-09-18 02:20:57', '2018-09-14 03:25:54', '2018-09-18 02:20:57'),
(27, 4, 0, 'erer', '2018-10-22 21:27:47', '2018-09-14 03:29:45', '2018-10-22 21:27:47'),
(28, 5, 1, '90', '2018-09-15 00:12:58', '2018-09-14 03:43:26', '2018-09-15 00:12:58'),
(29, 6, 1, '3434', '2018-09-15 00:24:04', '2018-09-14 23:33:46', '2018-09-15 00:24:04'),
(30, 6, 1, '34', '2018-09-15 00:24:04', '2018-09-14 23:34:26', '2018-09-15 00:24:04'),
(31, 6, 1, 'er', '2018-09-15 00:24:04', '2018-09-14 23:35:41', '2018-09-15 00:24:04'),
(32, 6, 1, 'dd', '2018-09-15 00:24:04', '2018-09-14 23:36:20', '2018-09-15 00:24:04'),
(33, 6, 0, 'erw', '2018-09-15 00:12:31', '2018-09-14 23:36:42', '2018-09-15 00:12:31'),
(34, 7, 1, '00', '2018-09-15 00:08:48', '2018-09-14 23:57:31', '2018-09-15 00:08:48'),
(35, 7, 1, 'wer', '2018-09-15 00:08:48', '2018-09-15 00:00:27', '2018-09-15 00:08:48'),
(36, 7, 0, 'ty', '2018-09-15 00:20:38', '2018-09-15 00:00:43', '2018-09-15 00:20:38'),
(37, 7, 0, 'tyt', '2018-09-15 00:20:38', '2018-09-15 00:00:53', '2018-09-15 00:20:38'),
(38, 8, 1, '45', '2018-09-15 01:45:54', '2018-09-15 00:01:36', '2018-09-15 01:45:54'),
(39, 4, 1, 'trtr', '2018-09-18 02:20:57', '2018-09-15 00:04:00', '2018-09-18 02:20:57'),
(40, 2, 0, 'oo', '2018-09-15 01:29:08', '2018-09-15 00:18:56', '2018-09-15 01:29:08'),
(41, 2, 1, '56', '2018-10-18 04:58:35', '2018-09-15 01:29:10', '2018-10-18 04:58:35'),
(42, 4, 0, '4545', '2018-10-22 21:27:47', '2018-09-15 01:29:25', '2018-10-22 21:27:47'),
(43, 9, 1, '3434', '2018-09-15 01:45:48', '2018-09-15 01:40:44', '2018-09-15 01:45:48'),
(44, 8, 0, '[[', '2018-09-15 01:46:07', '2018-09-15 01:45:57', '2018-09-15 01:46:07'),
(45, 10, 1, '2342', NULL, '2018-09-15 02:34:10', '2018-09-15 02:34:10'),
(46, 11, 1, '34234', '2018-09-18 02:21:11', '2018-09-15 02:41:56', '2018-09-18 02:21:11'),
(47, 12, 1, '324', '2018-09-15 04:14:26', '2018-09-15 02:53:40', '2018-09-15 04:14:26'),
(48, 12, 1, 'sdf', '2018-09-15 04:14:26', '2018-09-15 03:32:08', '2018-09-15 04:14:26'),
(49, 12, 1, 'sdf', '2018-09-15 04:14:26', '2018-09-15 03:32:08', '2018-09-15 04:14:26'),
(50, 12, 1, 'er', '2018-09-15 04:14:26', '2018-09-15 03:32:17', '2018-09-15 04:14:26'),
(51, 12, 1, 'er', '2018-09-15 04:14:26', '2018-09-15 03:32:17', '2018-09-15 04:14:26'),
(52, 12, 1, 'd', '2018-09-15 04:14:26', '2018-09-15 03:32:39', '2018-09-15 04:14:26'),
(53, 12, 1, 'file dilam', '2018-09-15 04:14:26', '2018-09-15 03:33:34', '2018-09-15 04:14:26'),
(54, 12, 1, 'der', '2018-09-15 04:14:26', '2018-09-15 03:34:03', '2018-09-15 04:14:26'),
(55, 12, 0, 'asdf', '2018-09-15 04:17:15', '2018-09-15 04:02:53', '2018-09-15 04:17:15'),
(56, 12, 0, 'asdf', '2018-09-15 04:17:15', '2018-09-15 04:02:53', '2018-09-15 04:17:15'),
(57, 12, 0, 'asdfe', '2018-09-15 04:17:15', '2018-09-15 04:03:11', '2018-09-15 04:17:15'),
(58, 12, 0, 'gr', '2018-09-15 04:17:15', '2018-09-15 04:03:16', '2018-09-15 04:17:15'),
(59, 12, 0, 'yu', '2018-09-15 04:17:15', '2018-09-15 04:03:51', '2018-09-15 04:17:15'),
(60, 12, 0, 'er', '2018-09-15 04:17:15', '2018-09-15 04:04:00', '2018-09-15 04:17:15'),
(61, 12, 0, '5656', '2018-09-15 04:17:15', '2018-09-15 04:05:09', '2018-09-15 04:17:15'),
(62, 12, 0, '34', '2018-09-15 04:17:15', '2018-09-15 04:14:26', '2018-09-15 04:17:15'),
(63, 11, 1, 'er', '2018-09-18 02:21:11', '2018-09-15 04:14:41', '2018-09-18 02:21:11'),
(64, 13, 1, '34', '2018-09-15 19:38:15', '2018-09-15 04:20:26', '2018-09-15 19:38:15'),
(65, 13, 0, 'rer', '2018-09-15 04:20:48', '2018-09-15 04:20:38', '2018-09-15 04:20:48'),
(66, 14, 1, '324234', '2018-09-16 23:37:33', '2018-09-15 19:49:24', '2018-09-16 23:37:33'),
(67, 14, 0, '5', '2018-09-15 20:46:39', '2018-09-15 19:52:35', '2018-09-15 20:46:39'),
(68, 14, 0, 'rere', '2018-09-15 20:46:39', '2018-09-15 20:46:28', '2018-09-15 20:46:39'),
(69, 15, 1, '234324', NULL, '2018-09-15 20:46:37', '2018-09-15 20:46:37'),
(70, 16, 1, '3434', '2018-09-16 02:15:04', '2018-09-15 20:49:25', '2018-09-16 02:15:04'),
(71, 16, 0, 'rere', '2018-11-20 20:07:02', '2018-09-16 01:21:54', '2018-11-20 20:07:02'),
(72, 16, 1, 'erer', '2018-09-16 02:15:04', '2018-09-16 02:14:50', '2018-09-16 02:15:04'),
(73, 17, 1, 'rer', '2018-09-16 02:32:36', '2018-09-16 02:32:20', '2018-09-16 02:32:36'),
(74, 17, 0, 'got it bro', '2018-09-16 02:32:40', '2018-09-16 02:32:36', '2018-09-16 02:32:40'),
(75, 18, 1, 'rere', '2018-10-06 04:30:52', '2018-09-16 23:01:11', '2018-10-06 04:30:52'),
(76, 19, 1, 'wwe', '2018-09-18 02:19:49', '2018-09-16 23:02:38', '2018-09-18 02:19:49'),
(77, 20, 1, 'rtr', '2018-09-18 02:18:58', '2018-09-16 23:02:48', '2018-09-18 02:18:58'),
(78, 21, 1, 'asdfas', NULL, '2018-11-16 23:34:08', '2018-11-16 23:34:08'),
(79, 22, 1, 'asdf', NULL, '2018-11-16 23:34:29', '2018-11-16 23:34:29'),
(80, 23, 1, 'asdf', NULL, '2018-11-16 23:35:59', '2018-11-16 23:35:59'),
(81, 24, 1, 'asdf', NULL, '2018-11-16 23:43:06', '2018-11-16 23:43:06'),
(82, 25, 1, 'asdf', NULL, '2018-11-16 23:48:56', '2018-11-16 23:48:56'),
(83, 26, 1, 'asdf', NULL, '2018-11-16 23:49:29', '2018-11-16 23:49:29'),
(84, 27, 1, 'asdf', NULL, '2018-11-16 23:50:10', '2018-11-16 23:50:10'),
(85, 28, 1, 'asdf', NULL, '2018-11-16 23:50:30', '2018-11-16 23:50:30'),
(86, 29, 1, 'asdf', NULL, '2018-11-16 23:50:47', '2018-11-16 23:50:47'),
(87, 30, 1, 'asdf', NULL, '2018-11-16 23:51:25', '2018-11-16 23:51:25'),
(88, 31, 1, 'asdf', NULL, '2018-11-16 23:51:54', '2018-11-16 23:51:54'),
(89, 32, 1, 'asdf', '2018-12-07 01:43:45', '2018-11-16 23:54:23', '2018-12-07 01:43:45'),
(90, 32, 1, 'asdf fer 445', '2018-12-07 01:43:45', '2018-11-16 23:56:22', '2018-12-07 01:43:45'),
(91, 32, 1, 'wer', '2018-12-07 01:43:45', '2018-11-16 23:57:54', '2018-12-07 01:43:45'),
(92, 32, 1, 'wer', '2018-12-07 01:43:45', '2018-11-16 23:58:21', '2018-12-07 01:43:45'),
(93, 32, 1, 'wer asdf4545', '2018-12-07 01:43:45', '2018-11-16 23:59:33', '2018-12-07 01:43:45'),
(94, 32, 1, 'wer asdf4545', '2018-12-07 01:43:45', '2018-11-17 00:00:07', '2018-12-07 01:43:45');

-- --------------------------------------------------------

--
-- Table structure for table `metas`
--

DROP TABLE IF EXISTS `metas`;
CREATE TABLE IF NOT EXISTS `metas` (
  `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT,
  `page` int(11) NOT NULL,
  `category` int(11) DEFAULT NULL,
  `vendor` int(11) DEFAULT NULL,
  `title` varchar(500) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `description` varchar(500) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=10 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `metas`
--

INSERT INTO `metas` (`id`, `page`, `category`, `vendor`, `title`, `description`, `created_at`, `updated_at`) VALUES
(2, 1, NULL, NULL, 'StylePick - Home', 'rtqr', '2018-07-10 09:15:19', '2018-07-10 09:21:03'),
(3, 2, NULL, NULL, 'StylePick - Vendors', NULL, '2018-07-10 09:20:04', '2018-07-10 09:21:17'),
(4, 3, NULL, NULL, NULL, NULL, '2018-07-10 09:20:51', '2018-07-10 09:20:51'),
(5, 4, 1, NULL, 'Women', 'cxvbxcv', '2018-07-10 09:27:24', '2018-07-10 09:28:26'),
(6, 5, NULL, 1, 'StylePick - Binjar', 'df', '2018-07-10 09:32:55', '2018-07-10 10:15:38'),
(7, 5, NULL, 2, NULL, NULL, '2018-07-10 09:33:31', '2018-07-10 09:33:31'),
(8, 6, NULL, NULL, 'StylePick - About Us', 'dd', '2018-07-16 23:29:19', '2018-07-16 23:29:32'),
(9, 7, NULL, NULL, NULL, NULL, '2018-08-09 23:17:48', '2018-08-09 23:17:48');

-- --------------------------------------------------------

--
-- Table structure for table `meta_buyers`
--

DROP TABLE IF EXISTS `meta_buyers`;
CREATE TABLE IF NOT EXISTS `meta_buyers` (
  `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT,
  `verified` tinyint(1) NOT NULL,
  `active` tinyint(1) NOT NULL,
  `block` tinyint(1) NOT NULL DEFAULT '0',
  `user_id` int(11) DEFAULT NULL,
  `company_name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `primary_customer_market` int(11) NOT NULL,
  `seller_permit_number` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `sell_online` tinyint(1) DEFAULT NULL,
  `website` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `attention` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `billing_location` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `billing_address` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `billing_unit` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `billing_city` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `billing_state_id` int(11) DEFAULT NULL,
  `billing_state` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `billing_zip` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `billing_country_id` int(11) NOT NULL,
  `billing_phone` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `billing_fax` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `billing_commercial` tinyint(1) DEFAULT NULL,
  `hear_about_us` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `hear_about_us_other` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `receive_offers` tinyint(1) NOT NULL,
  `sales2_path` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `sales1_path` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `ein_path` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=7 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `meta_buyers`
--

INSERT INTO `meta_buyers` (`id`, `verified`, `active`, `block`, `user_id`, `company_name`, `primary_customer_market`, `seller_permit_number`, `sell_online`, `website`, `attention`, `billing_location`, `billing_address`, `billing_unit`, `billing_city`, `billing_state_id`, `billing_state`, `billing_zip`, `billing_country_id`, `billing_phone`, `billing_fax`, `billing_commercial`, `hear_about_us`, `hear_about_us_other`, `receive_offers`, `sales2_path`, `sales1_path`, `ein_path`, `created_at`, `updated_at`, `deleted_at`) VALUES
(3, 1, 1, 0, 14, 'Buyer LTD', 1, '54654654', NULL, NULL, NULL, 'US', 'Raj', '12', 'gsdfgfga', 10, NULL, '5656', 1, '4576767', '4545', 0, 'bing', NULL, 1, NULL, NULL, NULL, '2018-05-24 10:22:34', '2018-07-27 20:41:20', NULL),
(4, 1, 1, 0, 19, 'newer', 2, '343434', 0, NULL, '53453', 'US', '45345', NULL, '45', 8, NULL, '34535', 1, '4545', NULL, 0, 'yahoo', NULL, 1, NULL, NULL, NULL, '2018-06-22 07:18:27', '2018-07-16 01:45:47', NULL),
(5, 0, 0, 0, 20, 'Buyer File LTD', 1, '34342', NULL, NULL, NULL, 'US', 'eqrwer', NULL, 'qewrqew', 8, NULL, 'weqrqwe', 1, '342423', NULL, 0, 'yahoo', NULL, 1, '/files/buyer/5cc184d0-7c61-11e8-965f-cb774fc2e7cb.pdf', '/files/buyer/5cc15790-7c61-11e8-9614-a51810cf5135.jpg', '/files/buyer/5cc118a0-7c61-11e8-8567-f52e793c25fa.jpg', '2018-06-30 06:30:24', '2018-07-22 22:48:58', '2018-07-22 22:48:58'),
(6, 1, 1, 0, 22, 'adfasdf', 1, '123123', NULL, NULL, NULL, 'US', 'adfad', NULL, 'asdf', 11, NULL, 'asfd', 1, 'qwerq', NULL, 0, 'yahoo', NULL, 1, '/files/buyer/1f6af6b0-89cb-11e8-9a1e-0b48755dbdb3.jpg', '/files/buyer/1f6acdf0-89cb-11e8-8f2e-d73308415a69.jpg', '/files/buyer/1f6a8f10-89cb-11e8-a36b-cd69018c9096.jpg', '2018-07-17 01:10:13', '2018-07-17 01:10:13', NULL);

-- --------------------------------------------------------

--
-- Table structure for table `meta_vendors`
--

DROP TABLE IF EXISTS `meta_vendors`;
CREATE TABLE IF NOT EXISTS `meta_vendors` (
  `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT,
  `verified` tinyint(1) NOT NULL,
  `active` tinyint(1) NOT NULL,
  `live` tinyint(1) NOT NULL DEFAULT '0',
  `company_name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `business_name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `website` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `business_category` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `year_established` int(11) DEFAULT NULL,
  `primary_customer_market` int(11) NOT NULL DEFAULT '1',
  `billing_location` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `billing_address` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `billing_unit` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `billing_city` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `billing_state_id` int(11) DEFAULT NULL,
  `billing_state` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `billing_zip` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `billing_country_id` int(11) NOT NULL,
  `billing_phone` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `billing_alternate_phone` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `billing_fax` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `billing_commercial` tinyint(1) NOT NULL,
  `factory_location` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `factory_address` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `factory_unit` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `factory_city` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `factory_state_id` int(11) DEFAULT NULL,
  `factory_state` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `factory_zip` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `factory_country_id` int(11) NOT NULL,
  `factory_phone` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `factory_alternate_phone` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `factory_evening_phone` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `factory_fax` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `factory_commercial` tinyint(1) DEFAULT NULL,
  `company_info` varchar(1000) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `hear_about_us` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `hear_about_us_other` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `order_notice` varchar(3000) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `size_chart` varchar(3000) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `min_order` double(8,2) NOT NULL DEFAULT '0.00',
  `receive_offers` tinyint(1) NOT NULL,
  `setting_estimated_shipping_charge` tinyint(1) NOT NULL DEFAULT '0',
  `setting_consolidation` tinyint(1) NOT NULL DEFAULT '0',
  `setting_sort_activation_date` tinyint(1) NOT NULL DEFAULT '0',
  `setting_unverified_checkout` tinyint(1) NOT NULL DEFAULT '0',
  `setting_unverified_user` tinyint(1) NOT NULL DEFAULT '0',
  `setting_not_logged` tinyint(1) NOT NULL DEFAULT '0',
  `show_in_main_slider` tinyint(1) NOT NULL DEFAULT '0',
  `show_in_mobile_main_slider` tinyint(1) NOT NULL DEFAULT '0',
  `feature_vendor` tinyint(1) NOT NULL,
  `main_slider_at` timestamp NULL DEFAULT NULL,
  `verified_at` timestamp NULL DEFAULT NULL,
  `activated_at` timestamp NULL DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=11 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `meta_vendors`
--

INSERT INTO `meta_vendors` (`id`, `verified`, `active`, `live`, `company_name`, `business_name`, `website`, `business_category`, `year_established`, `primary_customer_market`, `billing_location`, `billing_address`, `billing_unit`, `billing_city`, `billing_state_id`, `billing_state`, `billing_zip`, `billing_country_id`, `billing_phone`, `billing_alternate_phone`, `billing_fax`, `billing_commercial`, `factory_location`, `factory_address`, `factory_unit`, `factory_city`, `factory_state_id`, `factory_state`, `factory_zip`, `factory_country_id`, `factory_phone`, `factory_alternate_phone`, `factory_evening_phone`, `factory_fax`, `factory_commercial`, `company_info`, `hear_about_us`, `hear_about_us_other`, `order_notice`, `size_chart`, `min_order`, `receive_offers`, `setting_estimated_shipping_charge`, `setting_consolidation`, `setting_sort_activation_date`, `setting_unverified_checkout`, `setting_unverified_user`, `setting_not_logged`, `show_in_main_slider`, `show_in_mobile_main_slider`, `feature_vendor`, `main_slider_at`, `verified_at`, `activated_at`, `created_at`, `updated_at`, `deleted_at`) VALUES
(1, 1, 1, 1, 'Binjar', 'Binjar Inc.', 'www.binjar.com', NULL, 1992, 1, 'CA', 'Banashree', '2', 'Dhaka', 68, 'Rampura', '1219', 2, '01670985422', NULL, '123456', 1, 'US', 'cuet', '2', 'Chittagong', 11, 'Raozan', '3152', 1, '123456789', '45664', NULL, '4555', NULL, 'Company Info in details\r\n\r\nYoo', 'other', 'Bola jabena', '<p>&nbsp;asdf<strong> asd ORDER</strong></p>', '<p>&nbsp;asdfa<em> ds</em></p>', 10.00, 1, 0, 0, 1, 0, 1, 0, 1, 1, 1, '2018-08-16 00:05:11', NULL, '2018-08-06 04:56:06', '2018-05-08 09:03:20', '2018-10-22 20:29:54', NULL),
(2, 1, 1, 1, 'Yellow 232', 'Yellow Ltd.', NULL, NULL, NULL, 1, 'US', 'Bangladesh', '2', 'Rsadf', 5, NULL, '231', 1, '123123456456', NULL, '456456', 0, 'US', 'Bangladesh', '2', 'Rsadf', 5, NULL, '231', 1, '123123456456', NULL, NULL, '456456', 0, NULL, 'other_search', NULL, NULL, NULL, 0.00, 1, 0, 0, 0, 0, 0, 0, 1, 1, 1, '2018-08-16 00:05:10', NULL, NULL, '2018-05-23 15:16:41', '2018-10-22 20:29:55', NULL),
(6, 0, 0, 0, 'dasdf2', 'asdf', NULL, NULL, NULL, 1, 'US', 'asdf', NULL, 'asdf', 61, NULL, '3434', 1, '234234', NULL, NULL, 0, 'US', 'asdf', NULL, 'asdf', 60, NULL, '3434', 1, '234234', NULL, NULL, NULL, 0, NULL, 'google', NULL, NULL, NULL, 0.00, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, NULL, NULL, NULL, '2018-05-25 11:07:52', '2018-10-22 20:28:57', NULL),
(7, 1, 1, 0, 'bgsfdg', 'df', NULL, NULL, NULL, 1, 'US', 'asdaf', NULL, 'sdf', 64, NULL, '3434', 1, '56565', NULL, NULL, 1, 'US', 'asdaf', NULL, 'sdf', 62, NULL, '3434', 1, '56565', NULL, NULL, NULL, 0, NULL, 'other_search', NULL, NULL, NULL, 0.00, 1, 0, 0, 0, 0, 0, 0, 1, 0, 0, NULL, NULL, NULL, '2018-06-09 04:41:13', '2018-10-22 20:28:58', NULL),
(8, 0, 0, 0, 'Cat\'s Eyr', 'Cat\'s Eye LTD', NULL, NULL, NULL, 1, 'US', 'asdfa', NULL, 'asdfads', 5, NULL, 'a34', 1, '324324234', NULL, NULL, 0, 'US', 'asdfa', NULL, 'asdfads', 5, NULL, 'a34', 1, '324324234', NULL, NULL, NULL, 0, NULL, 'bing', NULL, NULL, NULL, 0.00, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, NULL, NULL, NULL, '2018-06-12 13:37:00', '2018-06-12 13:37:00', NULL),
(9, 0, 0, 0, 'Buyer LTDdsafdad', 'asdfasd', NULL, NULL, NULL, 1, 'US', 'fasdfasd', NULL, 'asdfasd', 4, NULL, '34343', 1, '23423', NULL, NULL, 0, 'US', 'fasdfasd', NULL, 'asdfasd', 8, NULL, '34343', 1, '23423', NULL, NULL, NULL, 0, NULL, 'bing', NULL, NULL, NULL, 0.00, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, NULL, NULL, NULL, '2018-06-22 02:14:14', '2018-06-22 02:14:14', NULL),
(10, 0, 0, 0, 'primary customer', 'asdf', NULL, NULL, NULL, 4, 'US', 'sdaf', NULL, 'zxcv', 61, NULL, 'zxcv', 1, 'asdf', NULL, NULL, 0, 'US', 'sdaf', NULL, 'zxcv', 7, NULL, 'zxcv', 1, 'asdf', NULL, NULL, NULL, 0, NULL, 'yahoo', NULL, NULL, NULL, 0.00, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, NULL, NULL, NULL, '2018-07-10 11:52:04', '2018-08-25 23:07:59', '2018-08-25 23:07:59');

-- --------------------------------------------------------

--
-- Table structure for table `migrations`
--

DROP TABLE IF EXISTS `migrations`;
CREATE TABLE IF NOT EXISTS `migrations` (
  `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT,
  `migration` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `batch` int(11) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=216 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `migrations`
--

INSERT INTO `migrations` (`id`, `migration`, `batch`) VALUES
(46, '2014_10_12_000000_create_users_table', 1),
(47, '2014_10_12_100000_create_password_resets_table', 1),
(48, '2018_05_05_133752_create_default_categories_table', 1),
(49, '2018_05_08_080401_create_countries_table', 1),
(50, '2018_05_08_083857_create_states_table', 1),
(51, '2018_05_08_134438_create_meta_vendors_table', 1),
(52, '2018_05_09_103818_create_sizes_table', 2),
(53, '2018_05_11_105225_create_packs_table', 3),
(54, '2018_05_11_120421_create_master_colors_table', 4),
(56, '2018_05_11_135750_create_colors_table', 5),
(57, '2018_05_12_103732_create_made_in_countries_table', 6),
(58, '2018_05_12_134524_create_master_fabrics_table', 7),
(59, '2018_05_12_151609_create_fabrics_table', 8),
(60, '2018_05_14_101752_create_industries_table', 9),
(62, '2018_05_14_122651_add_year_establish_in_meta_vendors_table', 10),
(63, '2018_05_14_133652_create_industry_user_table', 11),
(64, '2018_05_14_152938_add_size_chart_column_in_meta_vendors_table', 12),
(65, '2018_05_15_092930_add_sort_column_in_default_categories_table', 13),
(66, '2018_05_15_110154_add_user_id_column_in_users_table', 14),
(67, '2018_05_15_110903_remove_user_id_column_in_meta_vendors_table', 15),
(68, '2018_05_15_125606_add_status_column_in_users_table', 16),
(69, '2018_05_15_131551_add_last_login_column_in_users_table', 17),
(70, '2018_05_15_135817_remove_verified_column_in_users_table', 18),
(71, '2018_05_15_135945_add_verified_column_in_meta_vendors_table', 19),
(72, '2018_05_15_164157_create_user_permission_table', 20),
(73, '2018_05_16_081643_add_settings_columns_in_meta_vendors_table', 21),
(74, '2018_05_16_091300_create_login_history_table', 22),
(79, '2018_05_16_121659_create_categories_table', 23),
(80, '2018_05_16_173514_change_user_id_to_meta_vendors_in_packs_table', 24),
(81, '2018_05_17_072246_change_user_id_to_meta_vendor_id_in_fabrics_table', 25),
(82, '2018_05_17_072801_change_user_id_to_meta_vendor_id_in_made_in_countries_table', 26),
(83, '2018_05_17_084204_create_body_sizes_table', 27),
(84, '2018_05_17_113905_create_patterns_table', 28),
(85, '2018_05_17_124941_create_lengths_table', 29),
(86, '2018_05_17_133027_create_styles_table', 30),
(87, '2018_05_17_163110_change_user_id_to_meta_vendor_id_in_made_in_colors_table', 31),
(88, '2018_05_18_142050_create_item_images_table', 32),
(89, '2018_05_18_180811_create_items_table', 33),
(90, '2018_05_18_182509_create_color_item_table', 34),
(91, '2018_05_18_194317_add_color_id_column_in_item_images_table', 35),
(92, '2018_05_19_150357_add_activation_at_column_in_items_table', 36),
(93, '2018_05_21_085041_add_meta_vendor_id_in_items_column', 37),
(94, '2018_05_21_104611_change_descrtiption_to_nullable_in_items_table', 38),
(95, '2018_05_21_181848_add_sort_column_in_item_images_table', 39),
(96, '2018_05_22_183025_add_image_path_column_in_master_colors_table', 40),
(97, '2018_05_24_155751_create_meta_buyers_table', 41),
(99, '2018_05_25_052549_create_cart_items_table', 42),
(100, '2018_05_25_150654_add_active_column_in_meta_vendors_table', 43),
(101, '2018_05_26_115243_add_location_columns_in_meta_vendors_table', 44),
(103, '2018_05_28_135447_create_vendor_images_table', 45),
(105, '2018_05_28_182550_create_orders_table', 46),
(106, '2018_05_28_200922_add_vendor_meta_id_column_in_cart_items_table', 47),
(109, '2018_05_29_085525_create_order_items_table', 48),
(111, '2018_05_29_095042_add_order_number_column_in_orders_table', 49),
(112, '2018_05_29_164751_add_per_unit_price_column_in_order_items_table', 50),
(113, '2018_05_30_083752_create_notifications_table', 51),
(114, '2018_05_30_181718_add_verify_active_column_in_meta_buyers_table', 52),
(115, '2018_05_30_183944_add_block_column_in_meta_buyers_table', 53),
(116, '2018_05_31_055248_add_shipping_location_column_in_meta_buyers_table', 54),
(118, '2018_05_31_060140_add_store_no_column_in_meta_buyers_table', 55),
(119, '2018_05_31_192012_create_couriers_table', 56),
(120, '2018_06_01_083044_create_admin_ship_methods_table', 57),
(121, '2018_06_01_094139_create_shipping_methods_table', 58),
(122, '2018_06_01_191253_add_shipping_method_id_in_orders_table', 59),
(123, '2018_06_04_090630_add_show_main_slider_column_in_meta_vendors_table', 60),
(124, '2018_06_05_090127_create_visitors_table', 61),
(125, '2018_06_07_204324_create_category_banners_table', 62),
(126, '2018_06_08_101221_add_main_slider_column_in_items_table', 63),
(127, '2018_06_08_180010_add_soft_delete_in_orders_table', 64),
(128, '2018_06_09_153920_add_shipping_location_column_in_orders_table', 65),
(129, '2018_06_09_155854_add_shipping_country_id_column_in_orders_table', 66),
(130, '2018_06_10_085852_add_code_column_in_colors_table', 67),
(131, '2018_06_10_111156_add_tracking_number_column_in_orders_table', 68),
(133, '2018_06_11_090147_add_user_id_column_in_meta_buyers_table', 69),
(134, '2018_06_11_154425_change_card_number_length_in_orders_table', 70),
(135, '2018_06_11_190820_add_new_top_slider_column_in_items_table', 71),
(136, '2018_06_12_151328_create_wish_list_items_table', 72),
(138, '2018_06_12_181955_add_list_image_path_in_item_images_table', 73),
(139, '2018_06_12_190655_add_thumbs_image_path_in_item_images_table', 74),
(140, '2018_06_13_090427_add_style_no_column_in_order_items_table', 75),
(141, '2018_06_13_093013_add_company_name_column_in_orders_table', 76),
(142, '2018_06_13_094203_add_name_column_in_orders_table', 77),
(143, '2018_06_13_094740_add_shipping_column_in_orders_table', 78),
(144, '2018_06_13_105024_add_notes_column_in_orders_table', 79),
(145, '2018_06_13_160933_add_image_path_column_in_colors_table', 80),
(146, '2018_06_13_180012_create_slider_items_table', 81),
(147, '2018_06_22_090108_remove_sub_category_from_body_sizes_table', 82),
(148, '2018_06_22_090425_add_parent_category_id_in_body_sizes_table', 83),
(149, '2018_06_22_101056_change_sub_category_id_to_parent_category_id_in_patterns_table', 84),
(150, '2018_06_22_102143_change_sub_category_id_to_parent_category_id_in_styles_table', 85),
(152, '2018_06_22_124629_create_buyer_shipping_addresses_table', 86),
(153, '2018_06_22_130248_remove_shipping_address_from_meta_buyers_table', 87),
(154, '2018_06_22_141254_rename_state_to_state_text_in_buyer_shipping_address_table', 88),
(155, '2018_06_22_175526_add_shipping_address_id_in_orders_table', 89),
(156, '2018_06_26_095141_add_user_id_column_in_visitors_table', 90),
(157, '2018_06_26_103908_create_block_users_table', 91),
(158, '2018_06_28_164609_add_fabric_column_in_items_table', 92),
(159, '2018_06_30_122324_add_ein_column_in_meta_buyers_table', 93),
(160, '2018_07_02_104759_add_show_in_mobile_main_slider_column_in_meta_vendors_table', 94),
(161, '2018_07_02_122353_create_reviews_table', 95),
(162, '2018_07_04_140041_add_min_order_column_in_meta_vendors_table', 96),
(163, '2018_07_04_143920_create_coupons_table', 97),
(164, '2018_07_04_161626_add_coupon_column_in_orders_table', 98),
(165, '2018_07_05_091020_change_amount_to_nullable_in_coupons_table', 99),
(166, '2018_07_06_153545_add_description_column_in_coupons_table', 100),
(167, '2018_07_06_155243_add_coupon_description_column_in_orders_table', 101),
(169, '2018_07_07_092740_add_activated_at_column_in_meta_vendors_table', 102),
(170, '2018_07_10_130143_change_master_color_id_to_nullable_in_colors_table', 103),
(171, '2018_07_10_145657_create_metas_table', 104),
(173, '2018_07_10_163055_create_settings_table', 105),
(174, '2018_07_10_173010_add_primary_customer_market_column_in_meta_vendors_table', 106),
(175, '2018_07_12_131429_create_store_credits_table', 107),
(176, '2018_07_13_022817_create_store_credit_transections_table', 108),
(177, '2018_07_13_042725_add_store_credit_column_in_orders_table', 109),
(178, '2018_07_14_014418_add_discount_column_in_categories_table', 110),
(179, '2018_07_16_073544_add_deleted_at_column_in_users_table', 111),
(180, '2018_07_16_074154_add_deleted_at_in_meta_buyers_table', 112),
(181, '2018_07_17_015851_create_item_views_table', 113),
(182, '2018_07_18_025849_add_description_column_in_packs_table', 114),
(183, '2018_07_18_071723_add_ship_method_text_column_in_shipping_methods_table', 115),
(184, '2018_07_19_021433_create_category_item_table', 116),
(185, '2018_07_20_104316_add_route_column_in_visitors_column', 117),
(186, '2018_07_20_113058_add_soft_delete_in_store_credit_transection', 118),
(187, '2018_07_20_113254_add_soft_delete_in_reviews', 119),
(188, '2018_07_25_044348_add_reset_token_column_in_users_table', 120),
(189, '2018_07_26_042122_add_order_id_column_in_notifications_table', 121),
(190, '2018_07_28_023508_add_shipping_location_unit_in_orders_table', 122),
(192, '2018_07_31_083217_create_cards_table', 123),
(193, '2018_08_01_033739_add_billing_location_column_in_cards_table', 124),
(194, '2018_08_01_084847_add_card_location_in_orders_table', 125),
(195, '2018_08_06_073124_create_opening_soons_table', 126),
(196, '2018_08_06_085600_add_sort_column_in_opening_soons_table', 127),
(197, '2018_08_07_064156_add_live_column_in_meta_vendors_table', 128),
(198, '2018_08_16_055708_add_main_slider_at_column_in_meta_vendors_table', 129),
(199, '2018_08_17_010932_add_back_order_column_in_order_items_table', 130),
(200, '2018_08_26_050453_add_soft_delete_in_meta_vendors_table', 131),
(201, '2018_08_28_050237_change_parent_category_id_to_sub_category_id_in_styles_table', 132),
(204, '2018_09_12_081746_create_messages_table', 133),
(206, '2018_09_12_094604_create_message_items_table', 134),
(207, '2018_09_13_050611_add_sender_column_in_message_items_table', 135),
(208, '2018_09_14_093618_add_seen_at_column_in_messages_table', 136),
(209, '2018_09_15_054525_add_sender_seen_at_column_in_messages_table', 137),
(211, '2018_09_15_080021_create_message_files_table', 138),
(212, '2018_09_17_045447_add_order_id_column_in_messages_table', 139),
(214, '2018_10_17_055907_create_best_items_table', 140),
(215, '2018_10_23_021240_add_feature_vendor_column_meta_vendors_table', 141);

-- --------------------------------------------------------

--
-- Table structure for table `notifications`
--

DROP TABLE IF EXISTS `notifications`;
CREATE TABLE IF NOT EXISTS `notifications` (
  `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT,
  `user_id` int(11) NOT NULL,
  `order_id` int(11) DEFAULT NULL,
  `text` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `link` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `view` tinyint(1) NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=2 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `notifications`
--

INSERT INTO `notifications` (`id`, `user_id`, `order_id`, `text`, `link`, `view`, `created_at`, `updated_at`, `deleted_at`) VALUES
(1, 14, 61, 'Binjar updated order no. XWAGYCVYXA021', 'http://localhost/wholesalepeople/public/buyer/order/61', 0, '2018-07-25 22:34:16', '2018-07-25 22:35:56', '2018-07-25 22:35:56');

-- --------------------------------------------------------

--
-- Table structure for table `opening_soons`
--

DROP TABLE IF EXISTS `opening_soons`;
CREATE TABLE IF NOT EXISTS `opening_soons` (
  `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT,
  `name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `description` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `sort` int(11) DEFAULT NULL,
  `image_path` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=5 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `opening_soons`
--

INSERT INTO `opening_soons` (`id`, `name`, `description`, `sort`, `image_path`, `created_at`, `updated_at`) VALUES
(1, 'bb1', 'adsfads', 4, 'images/opening_soon/d4c4cad0-9990-11e8-9a04-719c9cf5102e.jpg', '2018-08-06 02:53:15', '2018-08-06 03:18:31'),
(4, 'wrewer', 'afadsfasda sdf asdf', NULL, 'images/opening_soon/0325d390-9995-11e8-ac6d-b511902964b7.jpg', '2018-08-06 03:23:11', '2018-08-06 03:23:11');

-- --------------------------------------------------------

--
-- Table structure for table `orders`
--

DROP TABLE IF EXISTS `orders`;
CREATE TABLE IF NOT EXISTS `orders` (
  `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT,
  `status` int(11) NOT NULL,
  `user_id` int(11) NOT NULL,
  `vendor_meta_id` int(11) NOT NULL,
  `order_number` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `invoice_number` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `tracking_number` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `name` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `email` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `company_name` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `shipping` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `shipping_address_id` int(11) DEFAULT NULL,
  `shipping_method_id` int(11) DEFAULT NULL,
  `shipping_location` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `shipping_address` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `shipping_unit` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `shipping_city` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `shipping_state` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `shipping_state_text` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `shipping_state_id` int(11) DEFAULT NULL,
  `shipping_zip` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `shipping_country` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `shipping_country_id` int(11) DEFAULT NULL,
  `shipping_phone` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `billing_location` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `billing_address` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `billing_unit` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `billing_city` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `billing_state` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `billing_state_text` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `billing_state_id` int(11) DEFAULT NULL,
  `billing_zip` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `billing_country` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `billing_country_id` int(11) DEFAULT NULL,
  `billing_phone` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `card_number` varchar(1500) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `card_full_name` varchar(1500) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `card_expire` varchar(1500) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `card_cvc` varchar(1500) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `card_country_id` int(11) DEFAULT NULL,
  `card_zip` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `card_state` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `card_state_id` int(11) DEFAULT NULL,
  `card_city` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `card_address` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `card_location` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `coupon_amount` double(8,2) DEFAULT NULL,
  `coupon_type` int(11) DEFAULT NULL,
  `coupon` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `coupon_description` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `subtotal` double(8,2) DEFAULT NULL,
  `discount` double(8,2) DEFAULT NULL,
  `store_credit` double(8,2) NOT NULL DEFAULT '0.00',
  `shipping_cost` double(8,2) DEFAULT NULL,
  `total` double(8,2) DEFAULT NULL,
  `note` varchar(1500) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=123 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `orders`
--

INSERT INTO `orders` (`id`, `status`, `user_id`, `vendor_meta_id`, `order_number`, `invoice_number`, `tracking_number`, `name`, `email`, `company_name`, `shipping`, `shipping_address_id`, `shipping_method_id`, `shipping_location`, `shipping_address`, `shipping_unit`, `shipping_city`, `shipping_state`, `shipping_state_text`, `shipping_state_id`, `shipping_zip`, `shipping_country`, `shipping_country_id`, `shipping_phone`, `billing_location`, `billing_address`, `billing_unit`, `billing_city`, `billing_state`, `billing_state_text`, `billing_state_id`, `billing_zip`, `billing_country`, `billing_country_id`, `billing_phone`, `card_number`, `card_full_name`, `card_expire`, `card_cvc`, `card_country_id`, `card_zip`, `card_state`, `card_state_id`, `card_city`, `card_address`, `card_location`, `coupon_amount`, `coupon_type`, `coupon`, `coupon_description`, `subtotal`, `discount`, `store_credit`, `shipping_cost`, `total`, `note`, `created_at`, `updated_at`, `deleted_at`) VALUES
(1, 2, 14, 1, '5AQXC67NX2S6J', NULL, NULL, 'Buyer', 'buyer@gmail.com', 'Buyer LTD', NULL, NULL, 1, 'US', 'Raj', NULL, 'gsdfgfga', 'District of Columbia', NULL, 10, '5656', 'United States', 1, '4576767', 'US', 'Raj', NULL, 'gsdfgfga', 'District of Columbia', NULL, 10, '5656', 'United States', 1, '4576767', 'eyJpdiI6IjJISWVcL0JkNWs0ZGMzVnZOZVdTWVBBPT0iLCJ2YWx1ZSI6ImhOaUg1RHpXRjIxamVpVGlrUFdONUdlRk4yR2ZFdDlLYlVNcmFKRFRKejg9IiwibWFjIjoiMDUzYjRjMjE0MmU4MjMwMTZjNWM1OGNkNzNlMGZmOGViNmY4ODQ1ZWU4MmY1YzQ3ZjJjM2NlMjIxNmYyMTRmYSJ9', 'eyJpdiI6ImlNQ01mOHRXRVFxeUhacjBFTk5XeUE9PSIsInZhbHVlIjoia2VESUtncGYrZnhsbjVQTU9PbEwyZz09IiwibWFjIjoiNGZkNjljODlkMjMzZWM5YjJiMzVkMTc0NWU4Mjg2ZjkyNzFjYTQ2MDZhZjVjN2M1MDhmM2FlMzlhMGYwNzc3MSJ9', 'eyJpdiI6InBuQW1sdTNOajBEUzlaMzJcL2pDSk5RPT0iLCJ2YWx1ZSI6IjROQWpEWUxhRmErZUJKQXFXSFdWUHc9PSIsIm1hYyI6IjdhMjYxOGJhMjcwYTk4M2IxM2Q1MTI1MGYyNWFlNjRlOWExYmUzNDNjYmExNWJkYTE3NDc0YTNiMDQ1YmVmZmMifQ==', 'eyJpdiI6InFmKzhOWGRZYVI1dStCUVlWdnZvRkE9PSIsInZhbHVlIjoiWWNSVUJ5NFg5OUdVeFV6XC9xN3c4bEE9PSIsIm1hYyI6ImJhZTNlNDdhNDNiYTcwYmFmYzFiMDQwZWY0YzFjOGEzYmQ0Yjg5Zjk5NWQxYTNkODNkMGI3Mzk1ZTZkYjEwY2UifQ==', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 369.00, 0.00, 0.00, 0.00, 369.00, NULL, '2018-06-13 03:40:11', '2018-06-13 04:17:50', '2018-06-13 04:17:50'),
(2, 2, 14, 1, 'R8OTQBW7KDPQW', NULL, NULL, 'First Customer', 'buyer@gmail.com', 'Buyer LTD', 'UPS Ground', NULL, 1, 'US', 'Raj', NULL, 'gsdfgfga', 'District of Columbia', NULL, 10, '5656', 'United States', 1, '4576767', 'US', 'Raj', NULL, 'gsdfgfga', 'District of Columbia', NULL, 10, '5656', 'United States', 1, '4576767', 'eyJpdiI6InFNdUJqNEx3eDl0bjBlM3lPY3RtZlE9PSIsInZhbHVlIjoiN2VvVzdhTk9XZVwvMGYwMkNYaUY1NHA3QU1yZ1pcL2c2MXBwYzNhM09taW1jPSIsIm1hYyI6IjYyYjNkZWQzNTgzMGY1NjI2ZDU5Yzc0MDgwNjZiNmNhMWI2OWNhYTcxYmFjZmNiNWJjMzVjNjk3NTg0MjBhOTkifQ==', 'eyJpdiI6IjZYdmtQVnViekpJMXlcLzdIbmFidGpnPT0iLCJ2YWx1ZSI6IjNGU2hWaTV6VjA3XC9mQXcrMndIeFVRPT0iLCJtYWMiOiI4YmQxMjE3MjlkODVkODBjNTU4MjU2YTg3MGRhNTA0NGUwOTE4YjQ1ZWUyOTRjYTg3ZTUzMDRjZWUwMTA2MzVmIn0=', 'eyJpdiI6IkgzN1dQQThYQlArNmV1eW1PRXJFVUE9PSIsInZhbHVlIjoicjdHVHBjK1BROUtmVThpNTZFeWRiZz09IiwibWFjIjoiNzc5OGZiODMzNzM1YmNlOTM0MGU5Y2FkZWI5ZWJkMWZhMDZmZGMxMzA4YmI3Zjc0NDY3NmFiNzMwZWFkZTE1OCJ9', 'eyJpdiI6IjJsbytnTFpxNnZWZ1N5SHhpWk5TY3c9PSIsInZhbHVlIjoia3lwektjelhmSGlwTkxPQUl1aFVTQT09IiwibWFjIjoiYmMwNzUxNjJmY2M5ZDZlYmI0MjZkZTdkM2EwYzVmMzU3Mzk0Mjg4MTU4MThkZDExMDhkZjFjYWFiZjM3MThjNSJ9', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 87.00, 0.00, 0.00, 0.00, 87.00, NULL, '2018-06-13 03:51:36', '2018-06-13 04:06:32', '2018-06-13 04:06:32'),
(3, 2, 14, 1, 'SG5WEJLCTWG8M', NULL, NULL, 'First Customer', 'buyer@gmail.com', 'Buyer LTD', 'UPS Ground', NULL, 1, NULL, 'Raj', NULL, 'gsdfgfga', 'District of Columbia', NULL, NULL, '5656', 'United States', NULL, '4576767', NULL, 'Raj', NULL, 'gsdfgfga', 'District of Columbia', NULL, NULL, '5656', 'United States', NULL, '4576767', 'eyJpdiI6InFNdUJqNEx3eDl0bjBlM3lPY3RtZlE9PSIsInZhbHVlIjoiN2VvVzdhTk9XZVwvMGYwMkNYaUY1NHA3QU1yZ1pcL2c2MXBwYzNhM09taW1jPSIsIm1hYyI6IjYyYjNkZWQzNTgzMGY1NjI2ZDU5Yzc0MDgwNjZiNmNhMWI2OWNhYTcxYmFjZmNiNWJjMzVjNjk3NTg0MjBhOTkifQ==', 'eyJpdiI6IjZYdmtQVnViekpJMXlcLzdIbmFidGpnPT0iLCJ2YWx1ZSI6IjNGU2hWaTV6VjA3XC9mQXcrMndIeFVRPT0iLCJtYWMiOiI4YmQxMjE3MjlkODVkODBjNTU4MjU2YTg3MGRhNTA0NGUwOTE4YjQ1ZWUyOTRjYTg3ZTUzMDRjZWUwMTA2MzVmIn0=', 'eyJpdiI6IkgzN1dQQThYQlArNmV1eW1PRXJFVUE9PSIsInZhbHVlIjoicjdHVHBjK1BROUtmVThpNTZFeWRiZz09IiwibWFjIjoiNzc5OGZiODMzNzM1YmNlOTM0MGU5Y2FkZWI5ZWJkMWZhMDZmZGMxMzA4YmI3Zjc0NDY3NmFiNzMwZWFkZTE1OCJ9', 'eyJpdiI6IjJsbytnTFpxNnZWZ1N5SHhpWk5TY3c9PSIsInZhbHVlIjoia3lwektjelhmSGlwTkxPQUl1aFVTQT09IiwibWFjIjoiYmMwNzUxNjJmY2M5ZDZlYmI0MjZkZTdkM2EwYzVmMzU3Mzk0Mjg4MTU4MThkZDExMDhkZjFjYWFiZjM3MThjNSJ9', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 87.00, 0.00, 0.00, 0.00, 87.00, NULL, '2018-06-13 04:06:32', '2018-06-25 10:07:45', '2018-06-25 10:07:45'),
(4, 2, 14, 1, '6ATHETDDL3XQR', NULL, NULL, 'First Customer', 'buyer@gmail.com', 'Buyer LTD', 'UPS Ground', NULL, 1, 'US', 'Raj', NULL, 'gsdfgfga', 'District of Columbia', NULL, 10, '5656', 'United States', 1, '4576767', 'US', 'Raj', NULL, 'gsdfgfga', 'District of Columbia', NULL, 10, '5656', 'United States', 1, '4576767', 'eyJpdiI6InJVU0tDSmdyeTN0aWdXenRiVUMrc1E9PSIsInZhbHVlIjoiMGEzOVNBaFdwSElYR1paM3Y1RTB0V0t4RDFPRGFVeGZqbXVldk94TFRNZz0iLCJtYWMiOiJhMTVhM2NiMjE2YzJjMTIwYjU2MDhiYjliZmE2ZGNiODU3MTYxYjQyZGIwMTI2MDMxZjM2ZDliZDUzODc0NWMzIn0=', 'eyJpdiI6IjkzM0lySG9CdjBzQWxydTVhampsNkE9PSIsInZhbHVlIjoiek8weWJuVkdkbTNETUVhMys2MGtCUT09IiwibWFjIjoiNTI3YTMxMWEyNmRkMmE0YzM0OWEyOGQxYjkyNjU4NDk0MzdhYmIxMTczMGI2MWVkNDNhMTdkZTMxOTU3NDYxOSJ9', 'eyJpdiI6IlJ1T1o4Nno1STg2WW8zbGwwXC9UWEF3PT0iLCJ2YWx1ZSI6IlM2c0RGamZoQjlpRmp3OEIra0RhSlE9PSIsIm1hYyI6ImE0YTBiNDQ4YTQ0NzIwZjgyOGJhZDNlZTc0Mjg1YzkzNjMyODg3YzcxZTlhMjY5ODJlZTBlYjQ2YmQ5ZjRiOGEifQ==', 'eyJpdiI6IjNUVmMyOEcwZGljcTV0dDNtOW5uQXc9PSIsInZhbHVlIjoiRVhOdlwvUTV3aUZMRkxjZjU2UXVTTkE9PSIsIm1hYyI6IjRiY2QxNTI1MDI3MDcyZWRmMjg5OWZmNjY4ZTNjYWRhNGUxNzg0YjI0MDE0ZDkzYTE4YjNiMjQ3MmY4ODE2NmQifQ==', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 181.50, 0.00, 0.00, 0.00, 181.50, NULL, '2018-06-13 04:59:12', '2018-06-26 09:46:14', NULL),
(5, 2, 14, 2, 'FAF7QIFE9IS39', NULL, NULL, 'First Customer', 'buyer@gmail.com', 'Buyer LTD', 'Truck', NULL, 3, 'US', 'Raj', NULL, 'gsdfgfga', 'District of Columbia', NULL, 10, '5656', 'United States', 1, '4576767', 'US', 'Raj', NULL, 'gsdfgfga', 'District of Columbia', NULL, 10, '5656', 'United States', 1, '4576767', 'eyJpdiI6Ik5PSmY4NHZUbnpJODBaOXhvU2c0VEE9PSIsInZhbHVlIjoiOU42YVlDN2ZuNU4xRm5DNjF4K3dTb1wvZVdHZzVXOGt3XC8zNnV5NlA3K3NnPSIsIm1hYyI6IjBkZWRiZjJhZjk2NTNmNjdiN2FjODZjNTNlYWVhZWFhN2E0NGI1ODA2NzQ2YzUzYjg0YWZmMGNmMTRkMDM0YzQifQ==', 'eyJpdiI6IjhsUUwzSzN4Sk9DU013RzVqa0VZaXc9PSIsInZhbHVlIjoiendcL21SQ0dTTUVCeDZsM0dwUzJaOWc9PSIsIm1hYyI6IjE4OTkzYzA5MzFmNWU2OWYxNDYxYmNjMDI3Y2M3MGRjYTUxNmU1Njg0ZjRiMDM0ZmIyZTc5MTU2M2U3ZDE4MjMifQ==', 'eyJpdiI6IkMyT3RPRWpQZGpmM24zT0hablBLUGc9PSIsInZhbHVlIjoiM0RuVnlaSzZTdjVcL0QzbEppRFBBT3c9PSIsIm1hYyI6ImJjZmQzNzdmYzA3NWY1MmU0OTA3NWEwM2IwMzU0Y2NjN2U2NDRmN2I3MDM5Njk0NjQxM2Q1MzViMTI1N2FiMTYifQ==', 'eyJpdiI6InNaK0lua3dvMGJhMWhpK1BHd2c5WHc9PSIsInZhbHVlIjoianE5bDdCaDhsck1Mc081aGdFc2l6Zz09IiwibWFjIjoiOGRjZTRiMGMxMmNhNmMyZDg0ZDE5ZDRkOTkzNmVjZTMzODk1MjRlNWRiOTNlZjYwOTU3ZTFjOThlNGEwNTI4ZCJ9', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 116.00, 0.00, 0.00, 0.00, 116.00, NULL, '2018-06-13 04:59:12', '2018-06-13 05:25:31', NULL),
(6, 2, 14, 1, 'MVYUUPSQACTZ4', NULL, NULL, 'First Customer', 'buyer@gmail.com', 'Buyer LTD', 'UPS Ground', NULL, 1, 'US', 'Raj', NULL, 'gsdfgfga', 'District of Columbia', NULL, 10, '5656', 'United States', 1, '4576767', 'US', 'Raj', NULL, 'gsdfgfga', 'District of Columbia', NULL, 10, '5656', 'United States', 1, '4576767', 'eyJpdiI6Ijk4clNWWDA0cmpvMHliVUsrUGRsK1E9PSIsInZhbHVlIjoidWo4WUlGeW9pbklkSnYxcCtSdUwyMkNwRGtGbkdkSENIelBoUUkyb2p6TT0iLCJtYWMiOiJjMGE1MDdmMGU4ZjA3ZmFiYThlZjI5ZDY0MjE3NzNhNjc5NmU3NDY2NTNkYmQyYWJlZjgzOTViNDM0ZjEyMWI3In0=', 'eyJpdiI6IktKeXRqVXg3eWUwZmpOWkNxcDdidlE9PSIsInZhbHVlIjoiM1didWZnbHhsTFF5UThWZmNWQVQydz09IiwibWFjIjoiNDE5YTk2YmQ0YmQ5ZGE3ZmUzNWQwYmE4NzIwOTMzNDAyNDM0NTg3MzdlMGJlNWQ0NzU0NDc4NGQ3ZWZkNzg2MCJ9', 'eyJpdiI6IjA1SzYwWTJHM3ZGUmRmQVJqem42Y2c9PSIsInZhbHVlIjoiaW9LUU9BSVk2dW9kSFRiU3NZbFZCQT09IiwibWFjIjoiMTZlY2NlMzk2NmM0NDNjNGYzYjYzNTg4YmUwMjc1NWEzMjE4ZTg3NGZjMmM4MGNkZmU1Njk0MmFlODg0ZDRlNSJ9', 'eyJpdiI6IlFlYXFzYjhnUGRqZHA0SHlPS2tmd3c9PSIsInZhbHVlIjoiNnVzOGMwXC94aTM2blpZSm96TVZHeEE9PSIsIm1hYyI6IjY2NGE1NWQxM2YzMzkyYmM1OGRjZTg0N2JhOWI2MjE0ODc1ZjMyZWY1ODUzY2Y2YmQ5YWUzZjVlMDI4N2YwMzcifQ==', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 93.00, 0.00, 0.00, 0.00, 93.00, 'fff', '2018-06-13 05:27:07', '2018-06-13 05:28:11', NULL),
(7, 2, 14, 1, 'VP186W74H2BVI', NULL, NULL, 'First Customer', 'buyer@gmail.com', 'Buyer LTD', 'UPS Ground', NULL, 1, 'US', 'Raj', NULL, 'gsdfgfga', 'District of Columbia', NULL, 10, '5656', 'United States', 1, '4576767', 'US', 'Raj', NULL, 'gsdfgfga', 'District of Columbia', NULL, 10, '5656', 'United States', 1, '4576767', 'eyJpdiI6IjBDQWVxUUNmYUZXYjNSelhxSGZpWEE9PSIsInZhbHVlIjoiN1NvM0w2aCs3dWNiUWlnNEZyTTl5U0pEWmdac2I2Z1BIbU1rbFwvZTJ2cjg9IiwibWFjIjoiNjIxYTgwOWM0Y2JkNDA3MGQ3NzQ5M2RlYmVjYTk5Nzg1OWY4NzJlYjU5ZjJkNzk2MWE0NjQ1ODIwMWFkNGZmNyJ9', 'eyJpdiI6IjVOaU5EQlJhK0FmTjVyQ0dvXC9qUVlnPT0iLCJ2YWx1ZSI6IjJzdU04XC9rVTJidGZtRzd0NG1nSTV3PT0iLCJtYWMiOiJhNjNhOGQ4YmE1NGQ3OTRjZTJjZjdhY2MzYjg3M2M4MTA4NDBmMTI3YWY1NDdkMGQ3ZTdmZDZjMWIzZTFjNWI1In0=', 'eyJpdiI6IldPSkJBWHpkRlpQM0ZWbmtERXJMblE9PSIsInZhbHVlIjoiTHh5QjZHSVRqcWQrTkkxSWtPSk12dz09IiwibWFjIjoiZjA0MGI4NDA2OTMwMWNlNWEyZTgxMDgxZTQ0OTU4NGJjZTMyMDM0ZmY2ZTlmMTU0YjcxZTY2YzFjOGJjYzgwNiJ9', 'eyJpdiI6IlJ0Rlp0MEJ2MDVYSUhxOXk5aXcrSXc9PSIsInZhbHVlIjoiS29vaXdkZlYrSDJhTjFZOXdvTVlSUT09IiwibWFjIjoiYjk1ODAxNTE4NzYyYzY4MGIyNGY0MjRkOGQzODNmY2E4YWE4ZTk2ZDViZGIwMjc4NTY0OTliYTk0YzRhODY1OSJ9', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 93.00, 0.00, 0.00, 0.00, 93.00, NULL, '2018-06-22 05:26:31', '2018-06-25 10:14:22', NULL),
(8, 1, 14, 1, '27S71GL7XXL8D', NULL, NULL, 'First Customer', 'buyer@gmail.com', 'Buyer LTD', NULL, 6, NULL, 'US', 'asdf', NULL, 'dfadf', 'Alaska', NULL, 1, NULL, 'United States', 1, '3434', 'US', 'Raj', NULL, 'gsdfgfga', 'District of Columbia', NULL, 10, '5656', 'United States', 1, '4576767', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 93.00, 0.00, 0.00, 0.00, 93.00, NULL, '2018-06-22 11:06:33', '2018-06-22 12:02:23', NULL),
(9, 1, 14, 1, 'GRW9HHK58KM8G', NULL, NULL, 'First Customer', 'buyer@gmail.com', 'Buyer LTD', 'UPS Ground', 1, 1, 'US', 'Raj', NULL, 'gsdfgfga', 'District of Columbia', NULL, 10, NULL, 'United States', 1, '4576767', 'US', 'Raj', NULL, 'gsdfgfga', 'District of Columbia', NULL, 10, '5656', 'United States', 1, '4576767', 'eyJpdiI6Imk3eHdiNlJmUjRRWFViN01vSjNhbVE9PSIsInZhbHVlIjoiRzIyeFlMRGJqaWFlUkM5d1VDbVJPRnc3Tm12UmVyT1h3dTRTQ0ljb2NDQT0iLCJtYWMiOiIzNWUyYmYyMDY5MDViMTUzNzAyMzE1M2ZiYTM1NmY4YzBiNzZhNDE4MWQwOGQ4Y2NlZWNiZTVjZGM5MjY3OGU1In0=', 'eyJpdiI6ImFqMVF2MUhBXC9QQStoQlF4dUY1aE9BPT0iLCJ2YWx1ZSI6InpsWkMyUm8yWnFTYVNnTldUUis3dEE9PSIsIm1hYyI6IjEzZDFiMjA0YTU2NTliMDZlYTRiMjY2ZGY1MjRhNTJlNTNiMTI0ZDRkYWI5OWFmYjk2MDk3Yjk4MGU0ODIwOGMifQ==', 'eyJpdiI6ImVMU1ZvT2JIZlhqSEFoOTRHU1BBZ1E9PSIsInZhbHVlIjoidTF0Vld6Mm91VzRveERKSEpVcjE0Zz09IiwibWFjIjoiMzZlOTVmYmYxNTkzYjI1OTZiMzFiNWU2NmFiMTdhNzExNTg3MGQwYTAzNjIyZGFhYjhhOGIwZTkxNjQzNjg2ZSJ9', 'eyJpdiI6IkdEb2ZcLzVVWWhNQVk5M1R3cUdZaGpnPT0iLCJ2YWx1ZSI6Im5OQTIzOFdPVU16bjY5MzVta2R4blE9PSIsIm1hYyI6IjM0NDU3ZTE1ZWMzMTFhNDU5MjllNWFhOGE2ZDQ5NDFhMjg2ZDAxNzgzMGEzNTQ0YmYyZGJlMGY4OTZkZTU5YzEifQ==', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 93.00, 0.00, 0.00, 0.00, 93.00, NULL, '2018-06-23 05:34:55', '2018-06-23 06:05:30', NULL),
(10, 1, 14, 1, 'MUT0JN9F6CZFD', NULL, NULL, 'First Customer', 'buyer@gmail.com', 'Buyer LTD', NULL, 13, NULL, 'US', 'jhaka naka', NULL, 'rer', 'American Samoa', NULL, 3, NULL, 'United States', 1, '3434', 'US', 'Raj', NULL, 'gsdfgfga', 'District of Columbia', NULL, 10, '5656', 'United States', 1, '4576767', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 93.00, 0.00, 0.00, 0.00, 93.00, NULL, '2018-06-23 09:24:30', '2018-06-23 10:23:03', NULL),
(11, 1, 14, 1, 'YOWAGBCTSR4GC', NULL, NULL, 'First Customer', 'buyer@gmail.com', 'Buyer LTD', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'US', 'Raj', NULL, 'gsdfgfga', 'District of Columbia', NULL, 10, '5656', 'United States', 1, '4576767', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 93.00, 0.00, 0.00, 0.00, 93.00, NULL, '2018-06-23 11:35:49', '2018-06-23 11:35:49', NULL),
(12, 1, 14, 1, '7ID83NGNJC8CI', NULL, NULL, 'First Customer', 'buyer@gmail.com', 'Buyer LTD', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'US', 'Raj', NULL, 'gsdfgfga', 'District of Columbia', NULL, 10, '5656', 'United States', 1, '4576767', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 93.00, 0.00, 0.00, 0.00, 93.00, NULL, '2018-06-23 11:39:54', '2018-06-23 11:39:54', NULL),
(13, 1, 14, 1, 'L3OGLUJCJ3BI9', NULL, NULL, 'First Customer', 'buyer@gmail.com', 'Buyer LTD', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'US', 'Raj', NULL, 'gsdfgfga', 'District of Columbia', NULL, 10, '5656', 'United States', 1, '4576767', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 93.00, 0.00, 0.00, 0.00, 93.00, NULL, '2018-06-23 11:40:26', '2018-06-23 11:40:26', NULL),
(14, 2, 14, 1, 'IGBM24KWV1EJY', NULL, NULL, 'First Customer', 'buyer@gmail.com', 'Buyer LTD', 'UPS 3', 1, 2, 'US', 'Raj', NULL, 'gsdfgfga', 'District of Columbia', NULL, 10, NULL, 'United States', 1, '4576767', 'US', 'Raj', NULL, 'gsdfgfga', 'District of Columbia', NULL, 10, '5656', 'United States', 1, '4576767', 'eyJpdiI6IlZcL3h6VGVic1wvZEs5QVc0dHVVcVk5Zz09IiwidmFsdWUiOiJpQ1NRcGdPa3BIXC9yaTBKQXlldnpFK0U2V3JFYTVCYlBtTGZydmNrK3Zraz0iLCJtYWMiOiJjOGRlYWJiOGFiMGQxZDYwOGIwZWRkODM3NjM5ZTM2NTFiNmVmYzI5YzZhZTQyM2ZmMjQ1ZGQ4ZDYxMWQ4YWVkIn0=', 'eyJpdiI6IkxcL2dDZlhLYnBUb3E0UlwvRCtucFhLZz09IiwidmFsdWUiOiJOK0pJYmh1dTUxQkp6R3Y5VFwvRlltQT09IiwibWFjIjoiYTIyMWZjYjFkMjkxMzAxYTRmNjY4NDNjNTA1NmUzYzlmZThhZDdmM2VjZTBlZGZhOGE2ZGE4MTUzMzhkNmZiYiJ9', 'eyJpdiI6IlBkaTdJakxXYmFobVlvVFZJOTl0Z3c9PSIsInZhbHVlIjoibDh3WlwvXC9mZk5WTHk0WGtLbFhNR3BBPT0iLCJtYWMiOiI0Y2ViMzBlNjc5NzUzMzc5NGNjZWU5NTFiMjU0NzQ2MDA5NGFjOTk1ZDlhODkxNmU3YWJhOWZjMmQ2YTk2ZWNiIn0=', 'eyJpdiI6InpsaDh0elJvaTJSbmxCRlpjUUdDcGc9PSIsInZhbHVlIjoieks5TnVIV2RvYUwwUEZvQ0xWVkJLQT09IiwibWFjIjoiNWNlYjBkOTFiN2RkM2RhODljYjU0ZWY3NWI4NDZmZjQ3MjZlZjJiODM4MDMwNmM2YTk0ZTBiOTRjODIxMjczNiJ9', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 93.00, 0.00, 0.00, 0.00, 93.00, 'b', '2018-06-23 11:40:55', '2018-06-25 10:04:06', '2018-06-25 10:04:06'),
(15, 2, 14, 2, 'SLI2XHDFFIKYE', NULL, NULL, 'First Customer', 'buyer@gmail.com', 'Buyer LTD', 'Truck', 1, 3, 'US', 'Raj', NULL, 'gsdfgfga', 'District of Columbia', NULL, 10, NULL, 'United States', 1, '4576767', 'US', 'Raj', NULL, 'gsdfgfga', 'District of Columbia', NULL, 10, '5656', 'United States', 1, '4576767', 'eyJpdiI6IjdiVnNvcTYzWFF1ZVI1eExrR0pjMHc9PSIsInZhbHVlIjoiaitpc2loOHMrTmR5VXdzanFBdGhoRDV2anp0M3hLY1lBUzg4VlNDNEk1ST0iLCJtYWMiOiIwOWY5OWNhM2JjZjk3NzA0NThhZmVmZTQzMjFmMjk1ODRjMDI1NzlkN2M3MGJmYTcxMTI4NTk0MjdjOWZhYzc0In0=', 'eyJpdiI6ImpsVjhJMFwvbVBhb1pSdEJNRHdNd253PT0iLCJ2YWx1ZSI6Ikd2U0VGaWVSdDIxV0JqK0VEamUrRFE9PSIsIm1hYyI6IjViM2NhNzA3NWE3YWZkNTQxYmZiZjc3NGQwOTY1OGZjODdiYTViYjg1ZDExNTBhZmQ1MjU5OWVmMTRmOTNhMjEifQ==', 'eyJpdiI6IkpXY1pQYktOaUhEd0ZaVUtsS3M2TlE9PSIsInZhbHVlIjoieWlmQVdSWUNOZmNYOHRHSmNSMVVZQT09IiwibWFjIjoiZmY2OTI3OTE0YzIzNTAxN2E3MTVlMzMzOGIyOTEzNjc2ODY2OTM3MTljNzAwZjk2NmYyYTZiZDhjNzFhZDJhYiJ9', 'eyJpdiI6Ik5aZlpOWGMyeUxYV09SbkVTSzIrUkE9PSIsInZhbHVlIjoiQm1hY3NwTDl1NVwvQVNlS2lsdFRmUmc9PSIsIm1hYyI6IjZmMDA1NWU2MTIwNDBmNTNhMGIyZGY5NDNiNmNlMjY4ZTIxNjA0MWQ2NmIwNDk2YjJhNzhhNzhmMjgxOTA2NTUifQ==', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 116.00, 0.00, 0.00, 0.00, 116.00, 'y', '2018-06-23 11:40:55', '2018-06-23 11:57:59', NULL),
(16, 1, 14, 1, 'ONIBEZHKB9Q3H', NULL, NULL, 'First Customer', 'buyer@gmail.com', 'Buyer LTD', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'US', 'Raj', NULL, 'gsdfgfga', 'District of Columbia', NULL, 10, '5656', 'United States', 1, '4576767', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 93.00, 0.00, 0.00, 0.00, 93.00, NULL, '2018-06-23 12:45:43', '2018-06-23 12:45:43', NULL),
(17, 1, 14, 1, 'U6TXOUVWWCB3M', NULL, NULL, 'First Customer', 'buyer@gmail.com', 'Buyer LTD', 'UPS Ground', 1, 1, 'US', 'Raj', NULL, 'gsdfgfga', 'District of Columbia', NULL, 10, NULL, 'United States', 1, '4576767', 'US', 'Raj', NULL, 'gsdfgfga', 'District of Columbia', NULL, 10, '5656', 'United States', 1, '4576767', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 93.00, 0.00, 0.00, 0.00, 93.00, NULL, '2018-06-23 12:46:38', '2018-06-23 12:46:43', NULL),
(18, 1, 14, 1, 'D1E0079GX579T', NULL, NULL, 'First Customer', 'buyer@gmail.com', 'Buyer LTD', 'UPS Ground', 1, 1, 'US', 'Raj', NULL, 'gsdfgfga', 'District of Columbia', NULL, 10, NULL, 'United States', 1, '4576767', 'US', 'Raj', NULL, 'gsdfgfga', 'District of Columbia', NULL, 10, '5656', 'United States', 1, '4576767', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 93.00, 0.00, 0.00, 0.00, 93.00, NULL, '2018-06-23 13:14:18', '2018-06-23 13:14:23', NULL),
(19, 7, 14, 1, 'CA7U4LDQBPOF3', NULL, NULL, 'First Customer', 'buyer@gmail.com', 'Buyer LTD', 'UPS 3', NULL, 2, NULL, 'Raj', NULL, 'gsdfgfga', 'District of Columbia', NULL, NULL, NULL, 'United States', NULL, '4576767', NULL, 'Raj', NULL, 'gsdfgfga', 'District of Columbia', NULL, NULL, '5656', 'United States', NULL, '4576767', 'eyJpdiI6IlZcL3h6VGVic1wvZEs5QVc0dHVVcVk5Zz09IiwidmFsdWUiOiJpQ1NRcGdPa3BIXC9yaTBKQXlldnpFK0U2V3JFYTVCYlBtTGZydmNrK3Zraz0iLCJtYWMiOiJjOGRlYWJiOGFiMGQxZDYwOGIwZWRkODM3NjM5ZTM2NTFiNmVmYzI5YzZhZTQyM2ZmMjQ1ZGQ4ZDYxMWQ4YWVkIn0=', 'eyJpdiI6IkxcL2dDZlhLYnBUb3E0UlwvRCtucFhLZz09IiwidmFsdWUiOiJOK0pJYmh1dTUxQkp6R3Y5VFwvRlltQT09IiwibWFjIjoiYTIyMWZjYjFkMjkxMzAxYTRmNjY4NDNjNTA1NmUzYzlmZThhZDdmM2VjZTBlZGZhOGE2ZGE4MTUzMzhkNmZiYiJ9', 'eyJpdiI6IlBkaTdJakxXYmFobVlvVFZJOTl0Z3c9PSIsInZhbHVlIjoibDh3WlwvXC9mZk5WTHk0WGtLbFhNR3BBPT0iLCJtYWMiOiI0Y2ViMzBlNjc5NzUzMzc5NGNjZWU5NTFiMjU0NzQ2MDA5NGFjOTk1ZDlhODkxNmU3YWJhOWZjMmQ2YTk2ZWNiIn0=', 'eyJpdiI6InpsaDh0elJvaTJSbmxCRlpjUUdDcGc9PSIsInZhbHVlIjoieks5TnVIV2RvYUwwUEZvQ0xWVkJLQT09IiwibWFjIjoiNWNlYjBkOTFiN2RkM2RhODljYjU0ZWY3NWI4NDZmZjQ3MjZlZjJiODM4MDMwNmM2YTk0ZTBiOTRjODIxMjczNiJ9', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 93.00, 0.00, 0.00, 0.00, 93.00, NULL, '2018-06-25 10:04:06', '2018-06-27 09:09:11', NULL),
(20, 2, 14, 1, 'LQBU54NZ5WE93', NULL, NULL, 'First Customer', 'buyer@gmail.com', 'Buyer LTD', 'UPS Ground', 1, 1, 'US', 'Raj', NULL, 'gsdfgfga', 'District of Columbia', NULL, 10, NULL, 'United States', 1, '4576767', 'US', 'Raj', NULL, 'gsdfgfga', 'District of Columbia', NULL, 10, '5656', 'United States', 1, '4576767', 'eyJpdiI6IjNBcEdYdFRxdTRHUitURGRzQnBkS1E9PSIsInZhbHVlIjoicEtvSUxUcWZDenErQWJaK0xXR1grMmlKU01OT3lreWdCYlMxZXhrbXp6UT0iLCJtYWMiOiIwNzczYWZkYzliMzBhOGRmYTcwODdlYzZlNDZkNTg0ZmExMGViODdlNGE1Yzc5NjUzMzgyYWY4NzdhY2YxZWJkIn0=', 'eyJpdiI6IkttOVFJV09xNE40UmxKdytMOGYrU1E9PSIsInZhbHVlIjoiM3BHcVFBaFpJcnFkc2d0QVdaWXMzV0pMVkZEd2xPaExsSU8rc2RhejFXVT0iLCJtYWMiOiJhMWUwOTI2YmQ0ZjlhZDYyMjRjZjdjNDk5ZGFjMTJjNDNiYmY0ZjVkNGM3NDk5YWNjOWRkZmM4Y2FiZGUwYjFjIn0=', 'eyJpdiI6ImhWU29RbEFrWWZ4WEpUbXhrTVJBK3c9PSIsInZhbHVlIjoiaGRuMnd2bldLd09RVmZiMGhhU05MZz09IiwibWFjIjoiNmQwNDQyZTZjMzljNTNkNmE5MzU2ODUwYzU3MmU5NTkyZTU0ZmFjMDcwZTcxZDBiMTAyOTI2MmZhZDEwMzc1OSJ9', 'eyJpdiI6IkVQUk4rK1Z6MHNUVHFsamxheDdDUHc9PSIsInZhbHVlIjoiMGJhZ3ZFbUtIaGMyY2wxbUNcL1hcLzdRPT0iLCJtYWMiOiI4MzhjNGFkYzRkNjBmNjEzZWMxZWQ3YzA4MmViYmMxYzhlYzE4YTBmMGFlNmE5NDFlMmRmNzc4NzU3MzlhNDE4In0=', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 93.00, 0.00, 10.00, 0.00, 83.00, NULL, '2018-06-25 11:35:37', '2018-07-15 23:47:36', NULL),
(21, 2, 14, 1, 'BVLO6Z4WYCX04', NULL, NULL, 'First Customer', 'shantotrs@gmail.com', 'Buyer LTD', 'UPS Ground', 1, 1, 'US', 'Raj', NULL, 'gsdfgfga', 'District of Columbia', NULL, 10, NULL, 'United States', 1, '4576767', 'US', 'Raj', NULL, 'gsdfgfga', 'District of Columbia', NULL, 10, '5656', 'United States', 1, '4576767', 'eyJpdiI6IklZY1wvQ21Rb1wvVmVrclZKa0o5cFU0UT09IiwidmFsdWUiOiJudEV3MWcxa2hOczNoZ2pUUWgyNDRDcjFaaTVXbW9pNnBRSzVDRXYzWHdnPSIsIm1hYyI6IjNjZTg0OTI5MDgxNWRhMzJjNDI3ZGE2YmY0NDM3NWIyMjM3MmYwNDM1MjNlMjk4MjhkZThlZWE1YzEzMmVhNGIifQ==', 'eyJpdiI6IlpoTTAzajNvMEEySnhMcytXZEtPeHc9PSIsInZhbHVlIjoiYWdTc1lEb05lT1wvN3JNRHhIYkZMekE9PSIsIm1hYyI6IjVmMDE3ZmQwM2E5YTIwYWE1NGViODgwZWNiNTRlYjFiNWNmMDU1NGQ1MzQ5Y2Q2NzRjZWFmODU3M2ExYTY1OWYifQ==', 'eyJpdiI6IjViMU1XWU5wblluQ3M5WWNYa0dcLzZnPT0iLCJ2YWx1ZSI6Ink2ZUt3NEdZM05QdnRDaDU3bFpSa1E9PSIsIm1hYyI6ImVmNzI4YTg0MWFlMjRhNWFlYTQ3MDdkZjNhOTIxYTg2N2I2ZDhkOThhNDJkNjFjZGQzMWQ5MzNmYTNiNDZkMWEifQ==', 'eyJpdiI6InU5bDh1TkkrclZTT29LQUtBbVRGMHc9PSIsInZhbHVlIjoiY0NoNnJBN3JSS0tvanFySWRYNmdVQT09IiwibWFjIjoiYjgwM2M0OGIwMTdiYjk1YTEzNThmMTQ5MzEwMjJhMjUwYzBiZmI2YmFiNmYxNzU0NDBkNTExNzQ3NzhlZDY5MiJ9', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 93.00, 0.00, 0.00, 0.00, 93.00, NULL, '2018-06-26 02:54:39', '2018-06-26 02:54:58', NULL),
(22, 2, 14, 1, '2JBD278ABNUOR', NULL, NULL, 'First Customer', 'shantotrs@gmail.com', 'Buyer LTD', 'UPS 3', 1, 2, 'US', 'Raj', NULL, 'gsdfgfga', 'District of Columbia', NULL, 10, NULL, 'United States', 1, '4576767', 'US', 'Raj', NULL, 'gsdfgfga', 'District of Columbia', NULL, 10, '5656', 'United States', 1, '4576767', 'eyJpdiI6InZsRTFjTTZYbFc1K205NUgzVlI1QlE9PSIsInZhbHVlIjoiQ2k0RGRvbGpaUmhSTzdCN0krZlhzQTUzbTFSc3FISHFQK3AxQnc3Nmx0VT0iLCJtYWMiOiJmNjA2MzhiYTUwOTJhNWM2OWIxODlkNWJkMWQ1OTM4M2JmMjNkYmEwZmViNjMxMWM3MThmNDFhY2M4ZDNhNjQ4In0=', 'eyJpdiI6ImdJOFlBQjNRcFJmbmtnSFQ3MzNkT2c9PSIsInZhbHVlIjoiVG1SMEpaYldPVXZPOG9zSU9pSXdpdz09IiwibWFjIjoiMmZhYjFkYTg3MDkzZDUzZTY4ZGQxMWYxMmNjYjBlODVjYzJlOWI5YmZmNTc4ZWQ5MGU5YmMyYTJlNTdhZjI3NiJ9', 'eyJpdiI6IkZrZ0dwZVZ4Qlo3dFpCelhkK2xWTXc9PSIsInZhbHVlIjoiOGp2VWhWaXgxQVwvRzBZV3lDMGdXXC93PT0iLCJtYWMiOiI1OGE0MjgyZTE5NWJlMDg2OTU0MjAwMWY4ZTUyOGI1YTlhYjBjODk5ODk4NDA3NDNjYTIyM2FjYjQ3MTVkNzVkIn0=', 'eyJpdiI6IlR4aHlvUFd2SFBvWXBabVo4UWlHOWc9PSIsInZhbHVlIjoiNmJXVlNqTGFzWnFod2JwUDE4RHZoQT09IiwibWFjIjoiY2M5YjY5M2I4NzkwMTg5NjI4ODYzODVhOGM5MjgyMGIwODRkMGEyZDEzMDlmYWM1YzZlMDhjMjFjZmY0YzAxYiJ9', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 90.00, 0.00, 0.00, 0.00, 90.00, NULL, '2018-06-26 02:56:36', '2018-06-26 02:56:53', NULL),
(23, 3, 14, 1, 'Q13OYI0LZBOBX', NULL, NULL, 'First Customer', 'shantotrs@gmail.com', 'Buyer LTD', 'UPS Ground', 1, 1, 'US', 'Raj', NULL, 'gsdfgfga', 'District of Columbia', NULL, 10, NULL, 'United States', 1, '4576767', 'US', 'Raj', NULL, 'gsdfgfga', 'District of Columbia', NULL, 10, '5656', 'United States', 1, '4576767', 'eyJpdiI6IjN3U0s2S0c1Y0pTZFF4NjZCaW1ydnc9PSIsInZhbHVlIjoiU1ROMlM2R1BXNWZJUUxSNWpBRVVjclhcL1BBMytJeWdSeDFqVWRxbm9BZkE9IiwibWFjIjoiNTExNzQ0ODU5NDYzOTZlYjA0OTU5MDE4NzEwYjExMDM5OGM0ODFmMGQzZDI2ZWZkYWMxNjNhYjU4OWE4YmZjOSJ9', 'eyJpdiI6ImpSKzd3YjR0a09UMmZIQmZqNTVNOGc9PSIsInZhbHVlIjoiM2FRTmFOOFFqdGZEenZtNEpYeUk2UCtPMDdOMjVhRmlXWkhkQVNXUktOST0iLCJtYWMiOiJjNWY3YjRjOTY3MDFiNDQ3NTRjNDE5ZDk5ZWUwNzA2MGM4NTRiZDA4NTUyYzMwOTI1YzNlMmM1YWZjMWM2YTkyIn0=', 'eyJpdiI6IkdIKzhoSzZRYW9LekY2cmlLV3drbmc9PSIsInZhbHVlIjoiTm0zRGtOMFlmY0xtdVV3N1pVaFlEdz09IiwibWFjIjoiMmI2ZTIwOTkwYTNkOGYyYWQ4OTg3ZDBhNzA5ZDMzMzAxYjZiMTU1NzhjMDNhN2JjMWRiNGQ2ZDEyMjY1YjI4NCJ9', 'eyJpdiI6IlV0Wk5EY2kzclRRZlB3Z3NoSDFneHc9PSIsInZhbHVlIjoiMHpxVGt4a1pPZjBRRlRzQ2hyMVBmdz09IiwibWFjIjoiNmVkOWE1OGFlZDE4ZWZmM2Y4ZTVmMmMxY2U1ZjhlNjQ1OWQyMzc4MTMyZWUwMmYzZjZjNjZlYWFjY2Q2Yzg5ZiJ9', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 708.00, 0.00, 0.00, 0.00, 708.00, 'binjar', '2018-06-26 03:25:07', '2018-06-27 08:59:14', NULL),
(24, 2, 14, 1, 'JYKZJE4AFI65C', NULL, NULL, 'First Customer', 'shantotrs@gmail.com', 'Buyer LTD', 'UPS Ground', 1, 1, 'US', 'Raj', NULL, 'gsdfgfga', 'District of Columbia', NULL, 10, NULL, 'United States', 1, '4576767', 'US', 'Raj', NULL, 'gsdfgfga', 'District of Columbia', NULL, 10, '5656', 'United States', 1, '4576767', 'eyJpdiI6InRpMXBKYllyVUsxU3o2VE1iVEpoZHc9PSIsInZhbHVlIjoiYlh3dVhkV1pQYkE0MmNNaWNjTEVCV1wvUjVmZ0NrUTZoenBacDhkaFZRUWM9IiwibWFjIjoiMjlkZDAyN2Q5YWE3ZjUwMmZhODUyMTQ0YTlkODNmMDUzNzg5MDgxZjdlNTg4MmFmYmUwZjdlZTM1N2FiMzNhYyJ9', 'eyJpdiI6IlN4UkZSZTdaWkprUTZJcjl2VDZWaUE9PSIsInZhbHVlIjoiTnNRSEdOemtBdDRGUDhRVkRyMHdHU3ZWaGxMb25PQVl2TnR0UnJtY0VUcz0iLCJtYWMiOiI2NDM0YTMwYjczY2FiNzRlNTYyODM4ZmJkZGIyYWJmOGVkZTNiZWI3NDg3YjE5Y2U4ZjhkMzhhZTM0Yjc3MWY1In0=', 'eyJpdiI6IjZXNzkrckgzVWI2S1JJeXJnKzEzM2c9PSIsInZhbHVlIjoiNTBZbjJGUHBTOXA4bHljV0o5VXp2UT09IiwibWFjIjoiZWEwMTk0MzQ1Nzc2MDEyMWM4MzA5NTI1NDM3NmY5MmY3MDIyMTMxMmE3NWE1ODkyZDJkNjYzODgyYmFkZDAyNSJ9', 'eyJpdiI6Ilp4QkkyTDB1RlRYRk9WcG9JODZ0blE9PSIsInZhbHVlIjoiQ0FwUGlFMkdMR3RHeTFsRmdZT3hcL0E9PSIsIm1hYyI6IjhhMTUwYTBkZjVhNmU4ZjIwZmJkYzMyNjJjMTIxOGM5ZjlkOTU3NjhhMTEzOTg2MjY4YzAwNmE3ZTU4Njg5OTYifQ==', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 318.00, 0.00, 0.00, 0.00, 318.00, NULL, '2018-06-28 03:19:25', '2018-06-28 03:19:50', NULL),
(25, 2, 14, 1, 'MCGQZ9AI54O8T', NULL, NULL, 'First Customer', 'shantotrs@gmail.com', 'Buyer LTD', 'UPS Ground', 1, 1, 'US', 'Raj', NULL, 'gsdfgfga', 'District of Columbia', NULL, 10, NULL, 'United States', 1, '4576767', 'US', 'Raj', NULL, 'gsdfgfga', 'District of Columbia', NULL, 10, '5656', 'United States', 1, '4576767', 'eyJpdiI6ImtlRXhcL3QrMFRoeVJmWXNoM2hQaU1nPT0iLCJ2YWx1ZSI6InJXYmVaK29pTWFtZDJ3WlwvUkY5dFlYTWZrN1RYNkFOdnFMTW9cL0RESkNHaz0iLCJtYWMiOiI3Y2I5ZWRlZjE5ODM1MWUwNzFlYjE1YzZjMDFmMmZjYzI2YWEyMzA5MDVlOGM0YmYzMmI3MGE1MmRiOTdiYmQxIn0=', 'eyJpdiI6IndoeFg1K2RqbzA5UTdIUEJPRHY0ekE9PSIsInZhbHVlIjoiNVNHUW5NQWgzOWdEZU1nXC81aVFzM21mZGcxY241NWZIWUsrWlJmRGRVTFU9IiwibWFjIjoiZThkZWFlY2E2NjkzY2E3MzUwYmNjNzkxNjEzYzkzZGUyZjcyNTZiNzY0MDhiYzEzZjBmZmM4NGI1M2UwMzRjNiJ9', 'eyJpdiI6Ikd4SFIwY2VcL1daVzV3aTJKeTZmUWpnPT0iLCJ2YWx1ZSI6IkVVQjdVR3kwXC9uQUxNSFRPbTlpSjZ3PT0iLCJtYWMiOiJiMjMzYzFlMTZhY2JlNzBhZmFjYTM4NmM0ODA5YjE0OGQwMWFkOTM1MjllZWRlMWI2ZWUyYmU2MDJjNmE4MWM0In0=', 'eyJpdiI6IjVIbXFBSnB4bXpRQTdrQjJKelMzdEE9PSIsInZhbHVlIjoiSFFUTmdWUFUyR2ZnSWtiT3BhYThzQT09IiwibWFjIjoiMmExNzg0OThlZTg3MzlkZDI0OWZiNjg4OGVhNWY0NjI1NGQ0MGEyODhiNWEyNTFjZTU3ZmMxNDE0MWM4OTA0OCJ9', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 318.00, 0.00, 0.00, 0.00, 318.00, NULL, '2018-06-28 03:23:54', '2018-06-28 03:24:09', NULL),
(26, 2, 14, 2, 'PJMX4WCFLVUWD', NULL, NULL, 'First Customer', 'shantotrs@gmail.com', 'Buyer LTD', 'Truck', 1, 3, 'US', 'Raj', NULL, 'gsdfgfga', 'District of Columbia', NULL, 10, NULL, 'United States', 1, '4576767', 'US', 'Raj', NULL, 'gsdfgfga', 'District of Columbia', NULL, 10, '5656', 'United States', 1, '4576767', 'eyJpdiI6IndrZzdselBOdzhsMFZVWUI4UWZvSGc9PSIsInZhbHVlIjoibnZ5MGthdHRFOTI5NVJGUlFJT2xmTTR1Wis1XC9xZlU1XC9yRHJ4NWUrQ1lVPSIsIm1hYyI6ImQ4NTE3MWUwOTdiZjkxMWZkMDk1ZWU5NjNhZmY0ODM4NTc1ZmE3ZDYyZTI5YTUxYWZkYWMzMzQ2Yjk0MjUxZmQifQ==', 'eyJpdiI6IkYxNUZndlRVQmptVTRpTTBEQVcrVkE9PSIsInZhbHVlIjoiYU0rYkRKXC85cEV2WktxcGswbk9rTUE9PSIsIm1hYyI6IjkzYjRlMDVlOTA3YmY0YjJkMmI4MjA5M2M2NDAzNTc4MzI5OTgxM2MwOWI3OTAyYjA4YjVlNTk0YzhiMTZjNDcifQ==', 'eyJpdiI6ImRoWEJENzZxa2c2VjI4OE1SSFwvYjdBPT0iLCJ2YWx1ZSI6IjBsWjdhb1hxQ2JOSFwvaUMxVzZIT1NRPT0iLCJtYWMiOiJlMzg5YjQxYmFkZjQ3MzUwNmM1NzIyZTMyYzBlOTI2Y2E0OTBhMWU4OWEwZWZkODc2Yjg4ZjcxY2IyMDM5MTEyIn0=', 'eyJpdiI6Ijc1ZFwvaFRFMnc3VmFVdFdSS2tvZWFnPT0iLCJ2YWx1ZSI6ImJzSHoxVUx2YWhqOXJnQks0VWhCNUE9PSIsIm1hYyI6IjEzNTU2YTFmYTNjMzBhNjcwMTM5NTY5MDA3MTUyNmZhZDcwMjEyMWZiOGJmODgyNGQ2M2RkNmZmZWU3YmRhMzEifQ==', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 29.00, 0.00, 0.00, 0.00, 29.00, NULL, '2018-07-02 06:02:01', '2018-07-02 06:03:03', NULL),
(27, 1, 14, 2, '4Q87OEW04NJF5', NULL, NULL, 'First Customer', 'shantotrs@gmail.com', 'Buyer LTD', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'US', 'Raj', NULL, 'gsdfgfga', 'District of Columbia', NULL, 10, '5656', 'United States', 1, '4576767', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 29.00, 0.00, 0.00, 0.00, 29.00, NULL, '2018-07-04 08:28:38', '2018-07-04 08:28:38', NULL),
(28, 1, 14, 1, 'F0RURZCDIL4HZ', NULL, NULL, 'First Customer', 'shantotrs@gmail.com', 'Buyer LTD', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'US', 'Raj', NULL, 'gsdfgfga', 'District of Columbia', NULL, 10, '5656', 'United States', 1, '4576767', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 93.00, 0.00, 0.00, 0.00, 93.00, NULL, '2018-07-04 08:29:12', '2018-07-04 08:29:12', NULL),
(29, 1, 14, 2, 'A8AD9O2XP2US6', NULL, NULL, 'First Customer', 'shantotrs@gmail.com', 'Buyer LTD', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'US', 'Raj', NULL, 'gsdfgfga', 'District of Columbia', NULL, 10, '5656', 'United States', 1, '4576767', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 29.00, 0.00, 0.00, 0.00, 29.00, NULL, '2018-07-04 08:29:12', '2018-07-04 08:29:12', NULL),
(30, 1, 14, 1, 'LFFADD8YPCNFY', NULL, NULL, 'First Customer', 'shantotrs@gmail.com', 'Buyer LTD', NULL, 1, NULL, 'US', 'Raj', NULL, 'gsdfgfga', 'District of Columbia', NULL, 10, NULL, 'United States', 1, '4576767', 'US', 'Raj', NULL, 'gsdfgfga', 'District of Columbia', NULL, 10, '5656', 'United States', 1, '4576767', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 93.00, 0.00, 0.00, 0.00, 93.00, NULL, '2018-07-04 08:31:10', '2018-07-04 08:31:12', NULL),
(31, 1, 14, 1, 'Z5DGOYNQU7OUM', NULL, NULL, 'First Customer', 'shantotrs@gmail.com', 'Buyer LTD', 'UPS Ground', 1, 1, 'US', 'Raj', NULL, 'gsdfgfga', 'District of Columbia', NULL, 10, NULL, 'United States', 1, '4576767', 'US', 'Raj', NULL, 'gsdfgfga', 'District of Columbia', NULL, 10, '5656', 'United States', 1, '4576767', 'eyJpdiI6ImR5bmx1K29POWFZN3E5NHhpdVRzK0E9PSIsInZhbHVlIjoiUWx4cUFGQXZlQVB1UXJnekZYWGtlSUUzUWpQWUoybmhYWE1uU1lpdTlNMD0iLCJtYWMiOiI2MTkyZTIxYzhjNGYzNzFlNDI5ODQyM2JiMmFiNGJkNWI2YWU1MzFlZThkYWE2N2JkNzBjZGNjYzIxNjhkNTNmIn0=', 'eyJpdiI6InpsTFpGNHk0VjBQb1FVVzA4THpXb3c9PSIsInZhbHVlIjoiOUo0TStzeEIxUUJ2RjRZd1BhZ1wveVE9PSIsIm1hYyI6ImNjMGEzOWRiNGJjMGE4YmUwNjQ3NzMxZDdkNDE0ODdmYTlhZjlmODg3M2IzZjdlYTU1N2Q1ZTdmY2IxZTNhZTQifQ==', 'eyJpdiI6ImNHTkdtMEU2cm9HbVwvMEtydVRLNm5nPT0iLCJ2YWx1ZSI6ImRsZ1lrOHFOdU9MYmFYMDJjMCtobnc9PSIsIm1hYyI6ImZlNmZiMmYwOTY0NjU1MTI3MDE4Mjc4YTRmNTY5MDRhNmFiNzc5ZmZmMmNlMzBjYmMzZThmMWZkMTFhOWRmMTgifQ==', 'eyJpdiI6ImliMmZpTkhZb1JxTGNsMGF0XC8zRVJRPT0iLCJ2YWx1ZSI6InR6NDJsRVBoVFQyTEdZR0NzVWpiU1E9PSIsIm1hYyI6IjMzMDdlZDNkOGYzYWYyZTdjZTVmMGRmNTYzNzZlMzEwNTg5MzllMWU4N2M5MDNmMDMyYzQzMGVkZmQ4OGM1MmIifQ==', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 93.00, 0.00, 0.00, 0.00, 93.00, NULL, '2018-07-04 09:47:23', '2018-07-04 09:47:43', NULL),
(32, 2, 14, 1, 'L5IZGY7JTYZSD', NULL, NULL, 'First Customer', 'shantotrs@gmail.com', 'Buyer LTD', 'UPS Ground', 1, 1, 'US', 'Raj', NULL, 'gsdfgfga', 'District of Columbia', NULL, 10, NULL, 'United States', 1, '4576767', 'US', 'Raj', NULL, 'gsdfgfga', 'District of Columbia', NULL, 10, '5656', 'United States', 1, '4576767', 'eyJpdiI6IjJmQ0hKZ1ltZU9xOUdDbXJwTWpqUFE9PSIsInZhbHVlIjoiR21QWk1uNXF0OUtjY1wvTEtJNjF2bkZHVURPMXdOYVNIMHR4akFES1FaSWs9IiwibWFjIjoiNGVhYjc1MjgyY2IxMzQwMzljNzBiYzBhNjE2ODBkNjQxNTNkNTI3MmY0MzRkODViMjhlNjZiMGEwZGJjYWIyZSJ9', 'eyJpdiI6IjFyT2JKTDBcL1NjdEZWbThJcVRHNnZBPT0iLCJ2YWx1ZSI6Img2RU9HOGVNWXRrSXNKYTc0UTZZS2c9PSIsIm1hYyI6IjhkNGQ4YzkwZTQ5NzZhMmNmNzJlYTcxNTNkZjFkOTdhMGYyM2NlM2I0ODZkZTA4YTk4YzI0OGMwNDc4OWNlMjIifQ==', 'eyJpdiI6IlM3WTFFUXk0ZWFoRzVtbEVJRkJmdHc9PSIsInZhbHVlIjoiZGY5T0V5RXpyNXBSNyt2SXIyTEpBZz09IiwibWFjIjoiNjA2MDZlOTI4ZDUyMjQ0Y2U5MjNkODJjMjNiYmUyNzVhMTE5ZWE2NDc1MjYyMzMwZTUzMGFjYzJjMTk0YTBmMiJ9', 'eyJpdiI6ImZcL3AwbUlhU0g2TFF3R0tWXC90amlvZz09IiwidmFsdWUiOiJpN0RzeXlOUVI4Mm8yWGtIOGJSN093PT0iLCJtYWMiOiJiZGE0YzQxYTI3MjI3MmM1MTdkZmJhYjg1ODRhMWEzODEwMmE0NmRhYjZmM2FhMDYwNjFiNTk2YjU5NTYyODA4In0=', NULL, NULL, NULL, NULL, NULL, NULL, NULL, 4.00, 2, 'EID', NULL, 93.00, 3.72, 0.00, 0.00, 89.28, NULL, '2018-07-04 09:50:49', '2018-07-04 10:42:48', NULL),
(33, 2, 14, 2, 'CRUZN0BRJQCDP', NULL, NULL, 'First Customer', 'shantotrs@gmail.com', 'Buyer LTD', 'Truck', 1, 3, 'US', 'Raj', NULL, 'gsdfgfga', 'District of Columbia', NULL, 10, NULL, 'United States', 1, '4576767', 'US', 'Raj', NULL, 'gsdfgfga', 'District of Columbia', NULL, 10, '5656', 'United States', 1, '4576767', 'eyJpdiI6ImUyZ1pvVnFMZVd6enkyaUIwYzVXamc9PSIsInZhbHVlIjoiOEw1anU4QlUzbUN6SVZ1enhFYnNiRVFwMWoxZVpTWE5iWk5VZldmUFRLRT0iLCJtYWMiOiJmYjViZTIzNmJlZmUyNGJjOTk3NGExMTdmYzg1OWY5NDM1N2U0MGY1OWM3ZWVmZDliZGFhYmE4MDE0MmQ4NzJiIn0=', 'eyJpdiI6ImFmM1UxWHVuWTBsbG15M0Y1VnB0XC93PT0iLCJ2YWx1ZSI6InhEQ0lEeHNXd1JremEzdE1IZkZWRnc9PSIsIm1hYyI6IjExYTNiYzcwNDYxMTQ1YmMxZDk4ZDIxNTg2NjI5OTFmN2Q3YjE5MTA0MTg1ZmZjNWNjYmJjMmQ3YmM5NzdiZjUifQ==', 'eyJpdiI6InMzcmFBaE9VZ3Vpd0V0YUJDazl2OFE9PSIsInZhbHVlIjoiNVFkelpSZW1wYzlsaWsyXC9CU3FtSnc9PSIsIm1hYyI6ImY2NWUyOGI5ODM1MDU0Mzg2YzE0MDEyOGI4ZTZmZDc4MWFmOTEzZmMxZTRjNDUyYzQ4ZDAwNjdiNjE1ZmZmNjAifQ==', 'eyJpdiI6Im85RWd0MVBMNHRwMVlDeFhhVlwvc1NBPT0iLCJ2YWx1ZSI6IlJRR0dOdDk5cmFWNkxmQ0FnTzcxaWc9PSIsIm1hYyI6IjY5MzRjOWM0NzM1YzE4NDNhOTQwN2YyNGI0MWI5YmJjMGQ0NGIxNWVmNmEyZTM3NzViNzY0OTVlYTg0ZDAyZjkifQ==', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 29.00, 0.00, 0.00, 0.00, 29.00, NULL, '2018-07-04 09:50:49', '2018-07-04 10:42:53', NULL),
(34, 1, 14, 1, 'JE8X8JDKAOYOH', NULL, NULL, 'First Customer', 'shantotrs@gmail.com', 'Buyer LTD', 'UPS Ground', 1, 1, 'US', 'Raj', NULL, 'gsdfgfga', 'District of Columbia', NULL, 10, NULL, 'United States', 1, '4576767', 'US', 'Raj', NULL, 'gsdfgfga', 'District of Columbia', NULL, 10, '5656', 'United States', 1, '4576767', 'eyJpdiI6ImVuWUlvUlwveDBFRFwveFhybW14U2NYQT09IiwidmFsdWUiOiJhVmh2UEVBemp3YWxzd3lwVHFOd081NkUra0Y2bDFNbHQ4bWFzN0ROd0hnPSIsIm1hYyI6ImQzNjc3NmU1ZjBkMmZkYWZhMTM1MjhkN2M1ZDIzNTBmMDBiNGEzYWMzY2ZmOTYxNzY1ZjJhZGQyNGYyMzQxYjkifQ==', 'eyJpdiI6IkpaODE3QWdhYW5RczhJZElYWms3OHc9PSIsInZhbHVlIjoiRVpIeWhBd1dcL3A0aWJiZDk3RzhvR1E9PSIsIm1hYyI6IjkyMWU5MzcyNzBmNzk2YzAxN2QxNDg3MzNiNGJhNGU4YTEzYzE1YmY2OTI3Y2MzNGIxMDljNDlmMDljZjc2NzgifQ==', 'eyJpdiI6IjNuaEJLOWJsbFpRbit2RkpOcmdaeEE9PSIsInZhbHVlIjoiYTMzSHhaMk9ES3F4VHd0UytwMXoyZz09IiwibWFjIjoiOThhMmVhZDMyODljZDNkNjFiYzdjMjU0NjQxZmIyZDQ2YTgxNWYyNWY2NTM0NmQyMmIzNGMyN2Q1NTUyYTI1OCJ9', 'eyJpdiI6ImxOWk5ZZUN3Tml4OGZXR0JWRVRIMlE9PSIsInZhbHVlIjoiWU5LXC9iQUhoR0FUeFFCUmJMNWw4MEE9PSIsIm1hYyI6ImEzNGQ5MzE1MjE4MWU0NGNlMTc2MjI5MDg3NDViMGRmN2UwMDk1MGY2YmZjMGVlNWQ3YmU0NWRmMjA1NWJlYzEifQ==', NULL, NULL, NULL, NULL, NULL, NULL, NULL, 50.00, 1, 'KURBANI2', NULL, 93.00, 50.00, 0.00, 0.00, 43.00, NULL, '2018-07-04 10:43:14', '2018-07-04 10:48:38', NULL),
(35, 1, 14, 1, '6PDGUNCEQTSIQ', NULL, NULL, 'First Customer', 'shantotrs@gmail.com', 'Buyer LTD', 'UPS Ground', 1, 1, 'US', 'Raj', NULL, 'gsdfgfga', 'District of Columbia', NULL, 10, NULL, 'United States', 1, '4576767', 'US', 'Raj', NULL, 'gsdfgfga', 'District of Columbia', NULL, 10, '5656', 'United States', 1, '4576767', 'eyJpdiI6IjcxTzlWTkJ5MjNKclZIdHJoMnVIalE9PSIsInZhbHVlIjoiR2JxVFZJUHR5SFNKcm16R1JZZldkaEJ4RTBtN3o2ZEJ4Nm1XWW02ZEhEaz0iLCJtYWMiOiJiNWJiMDNmNGNiZGFmNGFkYWFjMmQ3N2YyY2VhZTFmODcwOTllNzU4MGQ0Y2M3ODQ3NzQ4ZjY4MGNjMmQ0OWVjIn0=', 'eyJpdiI6IjFOOW04NTJvdUdRU2pNWjIrdHpRWUE9PSIsInZhbHVlIjoianFUcmZDdlBuT0hudUQxSWxTam9FZz09IiwibWFjIjoiMWM4MDM4NGMwOTM0MzczN2I5MDk1ZGQ3MmU3ODkwNGI2YWZjZDhlODg1MjU3MjY5NWQzZmI5ZTllYjg3MmY0NiJ9', 'eyJpdiI6IldQeWpUeXRQVWZseDkyakN6SGJ6NGc9PSIsInZhbHVlIjoiU1VoWW5GTFR2NEw1cGE3NVpQSzFjdz09IiwibWFjIjoiYjI1N2I3Mzc2ZDRjZGExYTEwNDJjOTU3NGM2ZjlkZTJmZGVkNTRiMjQzY2U5ZmVlZmRmNTU5ZTFjNWY4MmE5YyJ9', 'eyJpdiI6IjJtRmdGSE1GTzVpZTBsY254SHJmRVE9PSIsInZhbHVlIjoidHVXSTMxXC9xd0xlTzVRTllRK0hxZXc9PSIsIm1hYyI6IjBiMzQ1YjFkYzY2YTA3OThkMzQyZWExMDI0OTdiZDNlMTI5NWM4NDc3ZmM2N2E2YjliMjAxYTAxNjhhY2ExYmMifQ==', NULL, NULL, NULL, NULL, NULL, NULL, NULL, 50.00, 1, 'KURBANI2', NULL, 93.00, 50.00, 0.00, 0.00, 43.00, NULL, '2018-07-04 10:48:46', '2018-07-04 10:53:35', NULL),
(36, 1, 14, 1, '9GLKFBZ5ADFGC', NULL, NULL, 'First Customer', 'shantotrs@gmail.com', 'Buyer LTD', 'UPS Ground', 1, 1, 'US', 'Raj', NULL, 'gsdfgfga', 'District of Columbia', NULL, 10, NULL, 'United States', 1, '4576767', 'US', 'Raj', NULL, 'gsdfgfga', 'District of Columbia', NULL, 10, '5656', 'United States', 1, '4576767', 'eyJpdiI6Ik5qcE1ET1RTalhwbTd4aEs3ZHdjYnc9PSIsInZhbHVlIjoiTTdqM21YcUxpMnhCNVVIUmhhSFRhTlI1NWRLb0VxeTVGamx3QmYzUWNpRT0iLCJtYWMiOiI5MmJiYTZmZDA5ZTVhMzViNjJiNDQ0M2FkMmUzYTlmNDJjYWYyNjc5MDVkMDVhZmE1MjNhNmFiZDc2MzQzNGFhIn0=', 'eyJpdiI6InN4R1N6c0VsSHZlQUo4NGdLYlBKblE9PSIsInZhbHVlIjoidTA5Z1FzU2N0NXM0YVlyNUJEdU5sZz09IiwibWFjIjoiMzI2MzJlM2I5NmI0YmRlNDAyNzczMGY1MTExMGM4OWY4MTM4OGY0MjkxYWYxYjg4ZmRiNDU2MjBmNTZjZTYyYyJ9', 'eyJpdiI6IkNoNzNmY21ZOWR6U0lTekZDKzFcL1dnPT0iLCJ2YWx1ZSI6InpySHpVMG4yS3greGVqXC9HU1wvQ1NIUT09IiwibWFjIjoiZjAzMjAxMzE3YzY1ZDcyYmRkZDIwZjRlNDM1ZmRhOTBkZTI5ZmMzNDQwMTM4ODJiZmNiNjk4MmI3NTc2N2M1NSJ9', 'eyJpdiI6ImUyUXlzeFZyV0NwSlwvTVExTkJKYWJ3PT0iLCJ2YWx1ZSI6IjRYREE0MnlhTDVQcEloYkZXTUE2XC9nPT0iLCJtYWMiOiI3MGFlOTZkNWYxNTM4NDczZDc1OWFhZTY3ZTk2NWY4Yjc1OWE1YTQ5ZmU0NGYyODRkZWVjMjEzNWE0YjAyYTMyIn0=', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 93.00, 0.00, 0.00, 0.00, 93.00, NULL, '2018-07-04 12:54:40', '2018-07-04 12:54:58', NULL),
(37, 2, 14, 1, 'NPN4RBUGIKL5V', NULL, NULL, 'First Customer', 'shantotrs@gmail.com', 'Buyer LTD', 'UPS Ground', 1, 1, 'US', 'Raj', NULL, 'gsdfgfga', 'District of Columbia', NULL, 10, NULL, 'United States', 1, '4576767', 'US', 'Raj', NULL, 'gsdfgfga', 'District of Columbia', NULL, 10, '5656', 'United States', 1, '4576767', 'eyJpdiI6IkdyQjVhNUhCWWxOUHMzRUFFTEk4dVE9PSIsInZhbHVlIjoiMlpBMEtTZjhrU09oQkdkUVFtM2xkZGc5cHB2YVBHbWV0RTcybXZHTVlqND0iLCJtYWMiOiIxZDE1Yzg5YzFlZjI2MTJlY2U2NGMyMzVjN2EwYjlhMjU2MzEzY2EzMjk5Y2JkNDA0YTk0ODcxMjk3MTBjNDg1In0=', 'eyJpdiI6InN6N2lvYVRCdGlcLzJjNTk2Q2pNT3lnPT0iLCJ2YWx1ZSI6Im5KbGpXRXZyenF6RmI4TjhzaWllRVE9PSIsIm1hYyI6IjQyNGFmZThhZjIxMWU3NTQxYTdlN2EyNWUyNWY5N2EwYjFjZTI3ZTJjMGE1YjllNGE1YWU2NGI4NDA4MmQyOTAifQ==', 'eyJpdiI6Im56RmZzeUcwMk9pZEZaN3I5Ymt3ekE9PSIsInZhbHVlIjoiMUR4Q2xcL1VncTJMelJHSzZ4Y3k5TFE9PSIsIm1hYyI6ImZiN2I0ZmNjM2I3MGEzOTYyNGMwMWU4MjFhZWU2M2FmM2M0NTBmYzAwNDIyNTEwYWY4ZGQ1ODQ0MDE0ODgwYTcifQ==', 'eyJpdiI6IndPeGhaQThEUFlxRkVKcG5zbGFrQnc9PSIsInZhbHVlIjoiamJPT1VDQ1lNSDRLaWlDRjVLMHN6dz09IiwibWFjIjoiYzdiYTc1N2YwOGRiYTQ4NjRkMzNmYTUxZjExZWZkZWJkYjE1YzAyZDA4MTRiNWUxOWI5ZDViZDEwMjg5MmMyNyJ9', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 3, 'FREE', NULL, 93.00, 0.00, 0.00, 0.00, 93.00, NULL, '2018-07-05 03:48:05', '2018-07-05 03:56:05', NULL),
(38, 2, 14, 1, 'MNO7RFX6WBA5L', NULL, NULL, 'First Customer', 'shantotrs@gmail.com', 'Buyer LTD', 'UPS Ground', 1, 1, 'US', 'Raj', NULL, 'gsdfgfga', 'District of Columbia', NULL, 10, NULL, 'United States', 1, '4576767', 'US', 'Raj', NULL, 'gsdfgfga', 'District of Columbia', NULL, 10, '5656', 'United States', 1, '4576767', 'eyJpdiI6Imphb3pydlNXVXdQaDdNcERYenZKRHc9PSIsInZhbHVlIjoiXC9zMlZYdERiS3dPemFBa204TzFZK1wvRldcL0RWVUsrbnA3NmhpWlJDOUxqYz0iLCJtYWMiOiJjYWFhZjk5OTc2OWMwOGIyNjFiMjEyNTBhNDZjMjMzYjQ5NjJjMTI3ZDMxMDdiODU1NDM5N2JkZWYyNGE0OTY5In0=', 'eyJpdiI6ImwyQzBNcHhSMldhN0FUemJmR2ltdFE9PSIsInZhbHVlIjoiZVRrNzJ5T1FXUUdRNThrckFiSXE2dz09IiwibWFjIjoiM2Q5OWVmOWU5Yjc0ZGZlYThmMmNlZjk0OGRmZDFjYWEzNWRkZWI5Mzk5NmVmMGY0MTJlMTAxNWMwMzhhNjZkMSJ9', 'eyJpdiI6InRoc0pFZzVHU0ZVN3l4bTZUSlF5TkE9PSIsInZhbHVlIjoiTTB1cDMwa1poWmtcLzhMamF4NUs1dUE9PSIsIm1hYyI6IjBjMjI1NzVkYTAyMzE3MjczYTBjODA1NzQ5N2U4MDJiZmMxMDQ2MjA3NjkzYzMwMTAyOWNiZGU5YzMyM2M4NTEifQ==', 'eyJpdiI6IkdScUhxT0lOQUJ6V0Y4dFZlSnNcL1ZRPT0iLCJ2YWx1ZSI6ImJvaVFMTndmVzBFUU9kRUdCSnFuR3c9PSIsIm1hYyI6IjcwODJhMGZhY2QxZTk5MjA3OGJhNWU0MjUzZDdlNGFkZTk3ODhhMGQ1NGI0OWM0OTQxMDBkOTUwNzU4NzE2MWEifQ==', NULL, NULL, NULL, NULL, NULL, NULL, NULL, 4.00, 2, 'EID', 'wer', 705.00, 3.48, 0.00, 0.00, 701.52, NULL, '2018-07-06 09:56:27', '2018-07-09 04:56:23', NULL),
(39, 2, 14, 1, '41C251UEG69EB', NULL, NULL, 'First Customer', 'shantotrs@gmail.com', 'Buyer LTD', 'UPS Ground', 1, 1, 'US', 'Raj', NULL, 'gsdfgfga', 'District of Columbia', NULL, 10, NULL, 'United States', 1, '4576767', 'US', 'Raj', NULL, 'gsdfgfga', 'District of Columbia', NULL, 10, '5656', 'United States', 1, '4576767', 'eyJpdiI6InhZdG9IU0JzNE1lVmtPa25sOGVSVHc9PSIsInZhbHVlIjoiYmtnbmVDdG5qbnFhRGpDbGRZcTl0eEswWGR0OXNFbWhqMlJET2RVYU9PZz0iLCJtYWMiOiIyMWJkNmFkN2FjZjNkYzY3ZDhmM2EyNWQ0OTYxMTljNzE3YjA0ZjMxZWJiMmRmNDVmOWFhNzE4MWIxMTRkNmRkIn0=', 'eyJpdiI6ImpKRitHVEIrRjgwY2FwYTAxRHFydXc9PSIsInZhbHVlIjoid0xTdTdQQ3BGQWdjM29BSmluelRDVzVUWWhkc09vUDNLcEJoMzdBNzdNRT0iLCJtYWMiOiI1NTAzNzhmODliOWQ1MzdkMTk4ZWNiYjUwNDM4YThkOGUxYmJjZDdkN2M4MmRkMzhiMDZhYzRkMjQ0NmZjMjc4In0=', 'eyJpdiI6InYwcHRtcW9zSmg2VWxPUXFEWDJXOXc9PSIsInZhbHVlIjoiaDMzRkVhYzRpaXl2V1JYXC9ndHpJUlE9PSIsIm1hYyI6Ijc0NTgwMzdiYmIwM2Y3NzYwODc2ZDEwOWU4ZmMzZDZmZjQ1ZWNjMTRlMTA4NDE1YzQwOTI3ZWM2YzBhYzYzNjUifQ==', 'eyJpdiI6Inptek1heVgxaFRFK3A1blAwTm5rTXc9PSIsInZhbHVlIjoiamttWTRwRExGU2xLNmJQQlRWdGhMZz09IiwibWFjIjoiMmY1ZTViNjUzMDdjYjQwNDQ2NjIwMDU1YWI4Zjk3ZmNkYjRjMmUyYTdiYjc1NDczZTcwMTdlZjA2OThjMDI2MSJ9', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 87.00, 0.00, 0.00, 0.00, 87.00, NULL, '2018-07-12 19:50:52', '2018-07-12 19:51:47', NULL),
(40, 1, 14, 1, '7I5ABXSUXTPY0', NULL, NULL, 'First Customer', 'shantotrs@gmail.com', 'Buyer LTD', NULL, 1, NULL, 'US', 'Raj', NULL, 'gsdfgfga', 'District of Columbia', NULL, 10, NULL, 'United States', 1, '4576767', 'US', 'Raj', NULL, 'gsdfgfga', 'District of Columbia', NULL, 10, '5656', 'United States', 1, '4576767', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 93.00, 0.00, 0.00, 0.00, 93.00, NULL, '2018-07-12 21:56:10', '2018-07-12 21:56:14', NULL),
(41, 1, 14, 2, 'R3JCJY0D8CWLR', NULL, NULL, 'First Customer', 'shantotrs@gmail.com', 'Buyer LTD', NULL, 1, NULL, 'US', 'Raj', NULL, 'gsdfgfga', 'District of Columbia', NULL, 10, NULL, 'United States', 1, '4576767', 'US', 'Raj', NULL, 'gsdfgfga', 'District of Columbia', NULL, 10, '5656', 'United States', 1, '4576767', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 29.00, 0.00, 0.00, 0.00, 29.00, NULL, '2018-07-12 21:56:10', '2018-07-12 21:56:14', NULL),
(42, 1, 14, 1, '98CUBLTWNOKF7', NULL, NULL, 'First Customer', 'shantotrs@gmail.com', 'Buyer LTD', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'US', 'Raj', NULL, 'gsdfgfga', 'District of Columbia', NULL, 10, '5656', 'United States', 1, '4576767', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 93.00, 0.00, 0.00, 0.00, 93.00, NULL, '2018-07-12 22:22:17', '2018-07-12 22:22:17', NULL),
(43, 1, 14, 2, 'NEVC12O5OP9FL', NULL, NULL, 'First Customer', 'shantotrs@gmail.com', 'Buyer LTD', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'US', 'Raj', NULL, 'gsdfgfga', 'District of Columbia', NULL, 10, '5656', 'United States', 1, '4576767', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 29.00, 0.00, 0.00, 0.00, 29.00, NULL, '2018-07-12 22:22:17', '2018-07-12 22:22:17', NULL),
(44, 1, 14, 1, '4XGC9X4WOG4KX', NULL, NULL, 'First Customer', 'shantotrs@gmail.com', 'Buyer LTD', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'US', 'Raj', NULL, 'gsdfgfga', 'District of Columbia', NULL, 10, '5656', 'United States', 1, '4576767', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 93.00, 0.00, 0.00, 0.00, 93.00, NULL, '2018-07-12 22:22:22', '2018-07-12 22:22:22', NULL),
(45, 1, 14, 2, '2MEBSM1A85A3Z', NULL, NULL, 'First Customer', 'shantotrs@gmail.com', 'Buyer LTD', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'US', 'Raj', NULL, 'gsdfgfga', 'District of Columbia', NULL, 10, '5656', 'United States', 1, '4576767', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 29.00, 0.00, 0.00, 0.00, 29.00, NULL, '2018-07-12 22:22:22', '2018-07-12 22:22:22', NULL),
(46, 1, 14, 1, 'XU8FBXLWLOH3Z', NULL, NULL, 'First Customer', 'shantotrs@gmail.com', 'Buyer LTD', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'US', 'Raj', NULL, 'gsdfgfga', 'District of Columbia', NULL, 10, '5656', 'United States', 1, '4576767', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 93.00, 0.00, 0.00, 0.00, 93.00, NULL, '2018-07-12 22:22:31', '2018-07-12 22:22:31', NULL),
(47, 1, 14, 1, 'OPA47H7GXHGV2', NULL, NULL, 'First Customer', 'shantotrs@gmail.com', 'Buyer LTD', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'US', 'Raj', NULL, 'gsdfgfga', 'District of Columbia', NULL, 10, '5656', 'United States', 1, '4576767', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 93.00, 0.00, 0.00, 0.00, 93.00, NULL, '2018-07-12 22:26:04', '2018-07-12 22:26:05', NULL),
(48, 1, 14, 2, 'SSF3SCCYS5NA4', NULL, NULL, 'First Customer', 'shantotrs@gmail.com', 'Buyer LTD', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'US', 'Raj', NULL, 'gsdfgfga', 'District of Columbia', NULL, 10, '5656', 'United States', 1, '4576767', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 29.00, 0.00, 0.00, 0.00, 29.00, NULL, '2018-07-12 22:26:05', '2018-07-12 22:26:05', NULL),
(49, 1, 14, 1, 'UOANOFD5TEMXF', NULL, NULL, 'First Customer', 'shantotrs@gmail.com', 'Buyer LTD', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'US', 'Raj', NULL, 'gsdfgfga', 'District of Columbia', NULL, 10, '5656', 'United States', 1, '4576767', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 93.00, 0.00, 0.00, 0.00, 93.00, NULL, '2018-07-12 22:26:20', '2018-07-12 22:26:20', NULL),
(50, 1, 14, 1, '6CVZRLH7CQ6GZ', NULL, NULL, 'First Customer', 'shantotrs@gmail.com', 'Buyer LTD', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'US', 'Raj', NULL, 'gsdfgfga', 'District of Columbia', NULL, 10, '5656', 'United States', 1, '4576767', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 93.00, 0.00, 0.00, 0.00, 93.00, NULL, '2018-07-12 22:29:56', '2018-07-12 22:29:56', NULL),
(51, 1, 14, 1, 'EYNCUP78ZVEVP', NULL, NULL, 'First Customer', 'shantotrs@gmail.com', 'Buyer LTD', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'US', 'Raj', NULL, 'gsdfgfga', 'District of Columbia', NULL, 10, '5656', 'United States', 1, '4576767', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 93.00, 0.00, 15.00, 0.00, 93.00, NULL, '2018-07-12 22:31:16', '2018-07-12 22:31:16', NULL),
(52, 2, 14, 1, '1OAQSSFWRHSUG', NULL, NULL, 'First Customer', 'shantotrs@gmail.com', 'Buyer LTD', 'UPS Ground', 1, 1, 'US', 'Raj', NULL, 'gsdfgfga', 'District of Columbia', NULL, 10, NULL, 'United States', 1, '4576767', 'US', 'Raj', NULL, 'gsdfgfga', 'District of Columbia', NULL, 10, '5656', 'United States', 1, '4576767', 'eyJpdiI6IlRXMmhnT0NyK041cjg3TlE4OThhcmc9PSIsInZhbHVlIjoiR1d5cHg1ZGMyK1U2NFRyeURDRlROUFArRDJqTnBabE1sMEdRTmpcL2pLWG89IiwibWFjIjoiNjQ5ZGQxYjQ0MjczMjIwZjY5ZWFkZTg3YmM0NWI4NDUzZWI5YmJjM2I3MjI3M2Q3NjVlN2I1OWY0ZjEzOWQ5YSJ9', 'eyJpdiI6IllTUnJ4UUJwME1rR3lNU1JESjJ1c0E9PSIsInZhbHVlIjoiM1JjNGM3ZUFublVmaFc4ZHBlcUhQY2N4Q0lcL0t5T0ZZWUp6YjROQVBlODg9IiwibWFjIjoiNDJmMTdkYmE2YjhjMzBiNjFlNjkwMWIxODE4MTllZTIyMDA2ZDc0MTM3OWZlMzM2NjIyN2Y1ZGNkMTA2NzU2ZiJ9', 'eyJpdiI6Ik01M1RRQ0t0QWc5Q251anppUFFSSVE9PSIsInZhbHVlIjoidXRlWHdjU0NVZmNxWkQraGt3bFhsZz09IiwibWFjIjoiNWYzMzUyZmY0ZDhjZmZhNWFlNjRmZGJiOGU5ZTMwOGUwMTk0YjYzMTMxZWM1YzFhMDIxODhhMjUyZGM0NTdjMiJ9', 'eyJpdiI6Inp4ejV0Qmd0RyswRWY2WWp1XC8xN1p3PT0iLCJ2YWx1ZSI6IllcL2R2UkRuMURIdjVcL1l0QkdjSTNhZz09IiwibWFjIjoiMzkyOWFkNTU3ODM1YzcyZmE5ZTk5YmY5OGFmMWMwOTg0MTliNTk1ZTVmMzJiMzk0N2YyYjVkYWYyMWY5ZTA3NyJ9', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 93.00, 0.00, 15.00, 0.00, 78.00, NULL, '2018-07-12 22:31:58', '2018-07-12 23:11:14', NULL),
(53, 2, 14, 1, 'D7VSQ9XGX41DC', NULL, NULL, 'First Customer', 'shantotrs@gmail.com', 'Buyer LTD', 'UPS Ground', 1, 1, 'US', 'Raj', NULL, 'gsdfgfga', 'District of Columbia', NULL, 10, NULL, 'United States', 1, '4576767', 'US', 'Raj', NULL, 'gsdfgfga', 'District of Columbia', NULL, 10, '5656', 'United States', 1, '4576767', 'eyJpdiI6IkFVU3liVFJPUElDZWNidmlyMzVJV0E9PSIsInZhbHVlIjoicGI3OEIyUzhwM0Z1QTVYWWI1MUo0cHUrVGkrcFJlT3ZUOFhyXC9jTjd0QXc9IiwibWFjIjoiNmRlYjlhM2JjY2U2NGIzY2RkZjIyNTY1MmFkZjlmZjYwNDM2MGE5ZGJkYmNkMDk2MWIyZmRjMjRmNDFiYWMxNCJ9', 'eyJpdiI6IlBSN3VsaFRuTDBacytzK3lQQTZcL0VRPT0iLCJ2YWx1ZSI6ImZDRXlBbTFxRXN4QzFZa0s1VjBuWnc9PSIsIm1hYyI6IjhjMDdkZGYwMTVlNzI0NzEyMWIwOTA4NmYyMGU2M2E1YzA4ZWZmZTMyODcxYThjMDBkMDAyNGI1MGE1N2ZlNGIifQ==', 'eyJpdiI6IlhsbnphTUpcL2pOOWNpRlRjWlpiWm1RPT0iLCJ2YWx1ZSI6Ilg4OHFJOXdKdzZrUHh4MFV2UWZ6M1E9PSIsIm1hYyI6IjNlZjdhNGNhMzllNWVhYTU5ZDgwNDNiODA1NmUwMmQxMzBiYmY0ZmRlNTdhOGY5MzhlYTJjNTA4NmU4NzQzOTMifQ==', 'eyJpdiI6Ind5OHAwUEJBdG9YM3FHNEg2NmtsblE9PSIsInZhbHVlIjoiNzBNeVFDVWU3ZWZsWkpHR1RXOWVYZz09IiwibWFjIjoiYWYzZThlNjI5YTM4ZTM3ZDc5N2I1MDJhMWQ1YTlhMjYzYTkxYTBhNjliMWQwNDQ4ZDZkZDMwNjBlYTJiOGIyMCJ9', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 163.20, 0.00, 0.00, 0.00, 163.20, NULL, '2018-07-13 20:26:37', '2018-07-13 20:27:06', NULL);
INSERT INTO `orders` (`id`, `status`, `user_id`, `vendor_meta_id`, `order_number`, `invoice_number`, `tracking_number`, `name`, `email`, `company_name`, `shipping`, `shipping_address_id`, `shipping_method_id`, `shipping_location`, `shipping_address`, `shipping_unit`, `shipping_city`, `shipping_state`, `shipping_state_text`, `shipping_state_id`, `shipping_zip`, `shipping_country`, `shipping_country_id`, `shipping_phone`, `billing_location`, `billing_address`, `billing_unit`, `billing_city`, `billing_state`, `billing_state_text`, `billing_state_id`, `billing_zip`, `billing_country`, `billing_country_id`, `billing_phone`, `card_number`, `card_full_name`, `card_expire`, `card_cvc`, `card_country_id`, `card_zip`, `card_state`, `card_state_id`, `card_city`, `card_address`, `card_location`, `coupon_amount`, `coupon_type`, `coupon`, `coupon_description`, `subtotal`, `discount`, `store_credit`, `shipping_cost`, `total`, `note`, `created_at`, `updated_at`, `deleted_at`) VALUES
(54, 2, 14, 1, '6EH0KFK1NL1PO', NULL, NULL, 'First Customer', 'shantotrs@gmail.com', 'Buyer LTD', 'UPS Ground', 1, 1, 'US', 'Raj', NULL, 'gsdfgfga', 'District of Columbia', NULL, 10, NULL, 'United States', 1, '4576767', 'US', 'Raj', NULL, 'gsdfgfga', 'District of Columbia', NULL, 10, '5656', 'United States', 1, '4576767', 'eyJpdiI6IlwvcnJWZUJwdkt6T1FQSEhFYVJ5NmZ3PT0iLCJ2YWx1ZSI6InBQT1MwQnhIMnZ4NDRDalFrbVhvZEFDZVdUYjBUUEo5NjBJbGw3NUhBTmc9IiwibWFjIjoiYzYwM2Y1ZjgxYTgwMjM3YzMxYTkyYjYzNGY3ZDM0NmQ3ZmZmNWIxMmY2Y2Y3ZmVhOWY3ZWM2YTAwZWUxOGY1YiJ9', 'eyJpdiI6ImRLdFRNbkw0T2lqS3NIVkNONjRoWEE9PSIsInZhbHVlIjoiWXowZ3U3Mng2YngwXC85bkVtb0RxZUE9PSIsIm1hYyI6ImRmODA3NWY1ZDdkNWQ5Zjk5YTgyYWNkMDc0ZjUyYmMwMTFjNjk2NGFmMDhkM2E5NjQ3ZjBhZDY2MDczMWJiOTkifQ==', 'eyJpdiI6InQrZW9sUmtDclMwandcL0tMazhnNVZnPT0iLCJ2YWx1ZSI6InNDVU1xb0NpcGNBd2ZoNmZzQjN6WFE9PSIsIm1hYyI6IjNlY2EyODE4YTZmOGZjYzFhZWI4ZTYyNGNmNmU0MDMxYzM4YTRiNzI0OGU4NGY0NTllMzI3MWNlYmE3YmU4MDMifQ==', 'eyJpdiI6InNhdk03VGhKV1NJUVk5M0x0VjQwdGc9PSIsInZhbHVlIjoiVEVRQjlZNmUzQkNQTjdcL0RsS2d6OEE9PSIsIm1hYyI6ImRjZDA5YmRkZWMzZjlmMmZlOGJkODE2M2ZkNjExMjRhMjljYjU1ZDg5MTI5NGRhNTc2ZjZkNTIyOTZkZmU2ZWEifQ==', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 76.50, 0.00, 20.00, 0.00, 56.50, NULL, '2018-07-13 22:24:17', '2018-07-13 22:24:33', NULL),
(55, 2, 14, 1, '5R2E30HKH2R6U', NULL, NULL, 'First Customer', 'shantotrs@gmail.com', 'Buyer LTD', 'UPS Ground', 1, 1, 'US', 'Raj', NULL, 'gsdfgfga', 'District of Columbia', NULL, 10, NULL, 'United States', 1, '4576767', 'US', 'Raj', NULL, 'gsdfgfga', 'District of Columbia', NULL, 10, '5656', 'United States', 1, '4576767', 'eyJpdiI6IktCMENKQjZtZkxQOVZkdEl1bnArbHc9PSIsInZhbHVlIjoiVjNBY1JuOUNsRjNaN0Y4cnZNZE9mN2VndnFVRk80Ujl3TFhJQkVEbEk4OD0iLCJtYWMiOiJmMjIzZWNkMmI4ZDJhNDkwZDg2M2E1YTk1YzI5NzIyYmM3MjhjYzlkMDU0ZTZmNzJjMTAyOGFlNzI0ZjBjZDU0In0=', 'eyJpdiI6IkZqQ1wvdU9SckNzUXlnQUJqdldsTldRPT0iLCJ2YWx1ZSI6InV5WHhBVDMwemM5YmhsNVJhWDBSVmc9PSIsIm1hYyI6IjM5NDM1MDM3ZTU3MmZhNGEzZDI3NTY3YjUwNmZiY2IzOTNlOWEzNGQzOTYyZGNiNTJmZGY3NTVkOGZhNDRlYjYifQ==', 'eyJpdiI6Im1ESTdHVlwvamQwb3JcL2FUUTJ4NFV5Zz09IiwidmFsdWUiOiJcL1FTZlBmem5MbW4rQ1dmYk1nTGJnUT09IiwibWFjIjoiMmY4MzZhZmUzZDBiMjdiYjU0ZWE3NmZiZTllNWE1MDVlYTQ2YjcxOGZlMmY4ZGE1ZDYwM2RhZWZjZDIxNWQ0MCJ9', 'eyJpdiI6IjJ1a3FpN2ZKSGVkbVBoOWtMQVJCb0E9PSIsInZhbHVlIjoia3BCdEE3UTJvNFk0bzlJSDF2bGtFUT09IiwibWFjIjoiMTFlYjA5NzdkMWIwMzNmYTNjMTU0MGIyOWNjODQxNzVkZTk4ZDk3MjA4NTk0MmI2MDk4MmViOTA0ZjgzMTY3NSJ9', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 76.50, 0.00, 20.00, 0.00, 56.50, NULL, '2018-07-13 22:24:57', '2018-07-13 22:31:37', NULL),
(56, 2, 14, 1, '88KN5ZSOVB6FK', NULL, NULL, 'First Customer', 'shantotrs@gmail.com', 'Buyer LTD', 'UPS Ground', 1, 1, 'US', 'Raj', NULL, 'gsdfgfga', 'District of Columbia', NULL, 10, NULL, 'United States', 1, '4576767', 'US', 'Raj', NULL, 'gsdfgfga', 'District of Columbia', NULL, 10, '5656', 'United States', 1, '4576767', 'eyJpdiI6IkIyU04yZTVxWUJoaGJIUnJiWWUxb0E9PSIsInZhbHVlIjoiQlIxOWNiMkl6OE10Y0hYclhrREVmOE1qSGlxT1g0SDdsNXpsTmxvZStyTT0iLCJtYWMiOiI3NGVlMzU5NjRkMDgxYzFhZmU5MzVhNDJmMTc2OGEwMDE0NmUxNmFmN2M2ZmQyNWVlNGY3MGE4YWNlZjY1MjJkIn0=', 'eyJpdiI6IjBTU21CVldIQjVZWUhLcjZUMFV0V2c9PSIsInZhbHVlIjoicXkwb3pTV3kweUJINVpOV1RMOU9HUT09IiwibWFjIjoiNDMwNjExYTI5MjRjODQzNmI0ZThmYTAxNDdmZWYxOTZjMDBjZWE1ZThmYmY3YTliYzcyMmNjNGY2NWM3NmNlMCJ9', 'eyJpdiI6ImpGNjY3RjZsU1JEOHo0K3I4bTFINVE9PSIsInZhbHVlIjoiRDNpRGY3dWo4Uk56YUR4MXNpa0RhQT09IiwibWFjIjoiMmQ3ZjAxYjdhZmI1MGJhZmZlODIyZjZkYTgyMzRiMDU3N2I2NjFkZWI1OGNiZWRjMWI0ZTBjMGYyMmQ1YzM2NCJ9', 'eyJpdiI6IkVBMHVTSUhZVnRaVVRaUmtmRWd5OEE9PSIsInZhbHVlIjoiQ1VcL0szMFhMOTk1THpUMEE2MEhnRkE9PSIsIm1hYyI6ImMzMTBkYWQyYzg3YjE4YTNmYWU5ZTJjNDY0NDhkZjgwYmM0ZGZiNDBiOTY4YTVmNjg5NmRiOWUwNGFkOWIwM2EifQ==', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 153.00, 0.00, 20.00, 0.00, 133.00, NULL, '2018-07-13 22:33:05', '2018-07-13 23:25:42', NULL),
(57, 1, 14, 2, 'I9R2RGHFOHCVE', NULL, NULL, 'First Customer', 'shantotrs@gmail.com', 'Buyer LTD', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'US', 'Raj', NULL, 'gsdfgfga', 'District of Columbia', NULL, 10, '5656', 'United States', 1, '4576767', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 29.00, 0.00, 0.00, 0.00, 29.00, NULL, '2018-07-13 23:52:50', '2018-07-13 23:52:50', NULL),
(58, 1, 14, 2, 'LNS0UM50BMOBL', NULL, NULL, 'First Customer', 'shantotrs@gmail.com', 'Buyer LTD', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'US', 'Raj', NULL, 'gsdfgfga', 'District of Columbia', NULL, 10, '5656', 'United States', 1, '4576767', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 29.00, 0.00, 0.00, 0.00, 29.00, NULL, '2018-07-13 23:53:02', '2018-07-13 23:53:02', NULL),
(59, 1, 14, 2, 'F2FEB9XMQX9ST', NULL, NULL, 'First Customer', 'shantotrs@gmail.com', 'Buyer LTD', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'US', 'Raj', NULL, 'gsdfgfga', 'District of Columbia', NULL, 10, '5656', 'United States', 1, '4576767', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 29.00, 0.00, 0.00, 0.00, 29.00, NULL, '2018-07-13 23:53:08', '2018-07-13 23:53:08', NULL),
(60, 2, 14, 1, 'ZQT0SJ8KT5QTP', NULL, NULL, 'First Customer', 'shantotrs@gmail.com', 'Buyer LTD', 'UPS Ground', 1, 1, 'US', 'Raj', NULL, 'gsdfgfga', 'District of Columbia', NULL, 10, NULL, 'United States', 1, '4576767', 'US', 'Raj', NULL, 'gsdfgfga', 'District of Columbia', NULL, 10, '5656', 'United States', 1, '4576767', 'eyJpdiI6InBzcUpCUVJOcFdGSmNxUnErOW5QQ0E9PSIsInZhbHVlIjoicVdmMmZERWlCY2tVY1VPZ2FQcm5mczNBcG02Z3Q5RTJCOHVGQklJbVdcL0k9IiwibWFjIjoiMDVmOWE3ODI1NmExOGY4YjczODUyOTdjYzljN2M1ZmM5NWI3NWUzNTI2ZDE3MzY5OTExNWQwOWNlZjU4M2NjOCJ9', 'eyJpdiI6Ilh2eExPQ3NZTmZ6MXdXK0dHN1ZiQVE9PSIsInZhbHVlIjoiSFJ0WThjcDZOMnd2TU1ERXpRSTUwZz09IiwibWFjIjoiYjYwNjhhNDZhNTgxYTIxMGQxYmQ5MGE3MGFkM2YwN2Q0ODkzNDQ2ZWNlYTAzMzkyN2E0ZTI3YzM5YTUxMzg2YiJ9', 'eyJpdiI6InBja1YzOFhXclwvVzVGWndTNTBOUzBnPT0iLCJ2YWx1ZSI6InBnZllVZkNcL3owT0xVYkQ0dEwzNUtBPT0iLCJtYWMiOiI1MWM4OTYyMTBkYTYxNmFkNDZkMWUxMDc2YThjM2M0NTRlYjY2YzFmMWIzYjJhNTljMzJmZDFkMTdhNjIwYTEwIn0=', 'eyJpdiI6IlZiRzFUemg4Y08xZ1lENUxxeERMdXc9PSIsInZhbHVlIjoiaXFqQjFIcGI1d09GcXZkUjBUXC8zVnc9PSIsIm1hYyI6Ijg1ZjFjOWM5MzQ2NWIzNGY4NDljY2UwNWU2MjM3NzEyNDk3MjZiNDEzZjk4NDhlY2I1MGM1MjBmZTZlZWI0YzAifQ==', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 169.50, 0.00, 0.00, 0.00, 169.50, NULL, '2018-07-16 00:20:04', '2018-07-16 00:38:22', NULL),
(61, 2, 14, 1, 'XWAGYCVYXA021', NULL, NULL, 'First Customer', 'shantotrs@gmail.com', 'Buyer LTD', 'jhk2', 1, 4, 'US', 'Raj', NULL, 'gsdfgfga', 'District of Columbia', NULL, 10, NULL, 'United States', 1, '4576767', 'US', 'Raj', NULL, 'gsdfgfga', 'District of Columbia', NULL, 10, '5656', 'United States', 1, '4576767', 'eyJpdiI6InJNZk1NeEI0b3JuZXBCNVRiK1pyaGc9PSIsInZhbHVlIjoiQnVmMk9iRFRmZkM4Q2tROG5uclwvbFwvXC92Rnl0ZzVLSUJWNXpqK0d6Wld2ST0iLCJtYWMiOiJmOGVhYTI1OTY2ZWUyYTA0Zjg3NTYyYzVkNmFkZjcxMmRiZGQxZWIyZmNkMTAwYmNjZWEwYmI5Y2ZmMTEyZDFjIn0=', 'eyJpdiI6IlQ1eGxNZ2k1OFlvcGxiWVJKQ281ZGc9PSIsInZhbHVlIjoieG1BeUVhYk9iRUpcL2I2MVwvSUxEVW1hRlwvQUpNK25scW50aWNNa25kXC9OSTA9IiwibWFjIjoiZTNmMjEyOWU3OTZiMzgyYzBjZGZjMzQ0NWQ3MDYzNTczOTk1NzFkYjQ4NWYzNDFiYmI1MThlM2MzMTdkMDc5NCJ9', 'eyJpdiI6IjJsRzV5S0V1Q2w2STNGS3hFR01hZUE9PSIsInZhbHVlIjoiUXRmdnJhRHZTN0J4cDNtRFJXM1psUT09IiwibWFjIjoiZDE5MDdmYjgyMzkyMTE0OWMzMjU2ZGI2ZDAyZDViZWEyY2E5Njg2MDcxMzlmMzI5ODIzZDQ2MjM4OWZiMTg0MiJ9', 'eyJpdiI6IlRVaHJIXC8yb2tkRm84ajFGelRNaXp3PT0iLCJ2YWx1ZSI6InRDR3ZRZWNhaUltSkZhSU90TkJpTVE9PSIsIm1hYyI6ImZhNTBhNDQ4N2Y5MTgxMmUxMDYyMWYzNjVlNzRhZDkyY2U2NzQzNDU0M2U4MjJkMmM5MzZhMzZlNmEyMzc2ODAifQ==', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 93.00, 0.00, 0.00, 0.00, 93.00, NULL, '2018-07-18 02:06:57', '2018-07-25 22:35:56', '2018-07-25 22:35:56'),
(62, 1, 14, 2, 'YBB51VRGKMJ1W', NULL, NULL, 'First Customer', 'shantotrs@gmail.com', 'Buyer LTD', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'US', 'Raj', NULL, 'gsdfgfga', 'District of Columbia', NULL, 10, '5656', 'United States', 1, '4576767', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 29.00, 0.00, 12.00, 0.00, 17.00, NULL, '2018-07-18 05:30:09', '2018-07-18 05:30:09', NULL),
(63, 1, 14, 1, 'JMTBFMH94MHGX', NULL, NULL, 'First Customer', 'shantotrs@gmail.com', 'Buyer LTD', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'US', 'Raj', NULL, 'gsdfgfga', 'District of Columbia', NULL, 10, '5656', 'United States', 1, '4576767', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 93.00, 0.00, 0.00, 0.00, 93.00, NULL, '2018-07-18 05:33:57', '2018-07-18 05:33:57', NULL),
(64, 1, 14, 2, '1P3WS91FQRT1Z', NULL, NULL, 'First Customer', 'shantotrs@gmail.com', 'Buyer LTD', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'US', 'Raj', NULL, 'gsdfgfga', 'District of Columbia', NULL, 10, '5656', 'United States', 1, '4576767', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 29.00, 0.00, 0.00, 0.00, 29.00, NULL, '2018-07-18 05:33:57', '2018-07-18 05:33:57', NULL),
(65, 1, 14, 1, 'FCW198XYRQ819', NULL, NULL, 'First Customer', 'shantotrs@gmail.com', 'Buyer LTD', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'US', 'Raj', NULL, 'gsdfgfga', 'District of Columbia', NULL, 10, '5656', 'United States', 1, '4576767', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 93.00, 0.00, 0.00, 0.00, 93.00, NULL, '2018-07-18 05:34:06', '2018-07-18 05:34:06', NULL),
(66, 1, 14, 2, 'C5L2E2P8968VT', NULL, NULL, 'First Customer', 'shantotrs@gmail.com', 'Buyer LTD', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'US', 'Raj', NULL, 'gsdfgfga', 'District of Columbia', NULL, 10, '5656', 'United States', 1, '4576767', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 29.00, 0.00, 12.00, 0.00, 17.00, NULL, '2018-07-18 05:34:06', '2018-07-18 05:34:06', NULL),
(67, 1, 14, 1, '5MT81K0U4A641', NULL, NULL, 'First Customer', 'shantotrs@gmail.com', 'Buyer LTD', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'US', 'Raj', NULL, 'gsdfgfga', 'District of Columbia', NULL, 10, '5656', 'United States', 1, '4576767', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 93.00, 0.00, 0.00, 0.00, 93.00, NULL, '2018-07-18 05:34:15', '2018-07-18 05:34:15', NULL),
(68, 1, 14, 1, 'H6CO66PCXJ9AS', NULL, NULL, 'First Customer', 'shantotrs@gmail.com', 'Buyer LTD', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'US', 'Raj', NULL, 'gsdfgfga', 'District of Columbia', NULL, 10, '5656', 'United States', 1, '4576767', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 93.00, 0.00, 0.00, 0.00, 93.00, NULL, '2018-07-18 05:34:19', '2018-07-18 05:34:19', NULL),
(69, 1, 14, 2, 'RUBFH905CLNNL', NULL, NULL, 'First Customer', 'shantotrs@gmail.com', 'Buyer LTD', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'US', 'Raj', NULL, 'gsdfgfga', 'District of Columbia', NULL, 10, '5656', 'United States', 1, '4576767', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 29.00, 0.00, 12.00, 0.00, 17.00, NULL, '2018-07-18 05:34:19', '2018-07-18 05:34:19', NULL),
(70, 2, 14, 1, 'EU1FI7ITC7WUT', NULL, NULL, 'First Customer', 'shantotrs@gmail.com', 'Buyer LTD', 'jhk2', 16, 4, 'US', 'with unit', '566', 'wer', 'Alaska', NULL, 1, NULL, 'United States', 1, '23423', 'US', 'Raj', '12', 'gsdfgfga', 'District of Columbia', NULL, 10, '5656', 'United States', 1, '4576767', 'eyJpdiI6IllraVliRHRqWDZkWGM4NnFMZVFubmc9PSIsInZhbHVlIjoiQjNPQVwvdlFFdlFRXC9FM0ZMMVBkK1daSThhSk1sWFpZSW9pT1pYeTJ3citnPSIsIm1hYyI6IjJlNDg0ZTBlZjIyZGMxMjQwYjZhY2U1ZGNmNDk4ODY0YmY5ZTVmYWVkNGNlNGYxNTlmMmY2NTAwYTc0YWRlYTcifQ==', 'eyJpdiI6IkpBcTVGVVhjdm91aHplcVpmYXR2ZHc9PSIsInZhbHVlIjoibFwvaE5ZOWpPaEMxSTU1alRrM3ZyMkE9PSIsIm1hYyI6IjY0ZjA1ZGVhNTVkM2VmNGI1ZjEzYTc0NWY4MGZhNDZkZTljODM2MmY1MWY4MzQwZThhNDUyMzg4ODg2ZjYxZWUifQ==', 'eyJpdiI6IjF5cWd2S0pGUmVva2FNbXRtbzdyYXc9PSIsInZhbHVlIjoiWWNMeFhZRUdjUVoyN1htR25ZUjJKUT09IiwibWFjIjoiM2M2ODE0OTdmNTk2NWRhZTM5Yjc1ZjNlYmM2ZmViNGI2MzJkMmFkNWIwYWNmOTBkZGQ2ZWQ1ZGQwZDExNWJlZCJ9', 'eyJpdiI6Inljcjc0NnVUdG5MV1dwMnVpY1ZQekE9PSIsInZhbHVlIjoidERHOEhyditWY0JFXC92c1VFV3FCY0E9PSIsIm1hYyI6IjEwNGQzMDAwOTFiMTgxMTRhZjBiMGM4NGIzMzNiZjczNTllNTMxNGRjNjMwYzYwOWIxODQ3MmM3NTZjZGQzNzAifQ==', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 93.00, 0.00, 0.00, 0.00, 93.00, NULL, '2018-07-27 20:41:29', '2018-07-27 20:44:42', NULL),
(71, 1, 14, 1, 'LR1Q5CDC6NK22', NULL, NULL, 'First Customer', 'shantotrs@gmail.com', 'Buyer LTD', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'US', 'Raj', '12', 'gsdfgfga', 'District of Columbia', NULL, 10, '5656', 'United States', 1, '4576767', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 549.00, 0.00, 0.00, 0.00, 549.00, NULL, '2018-07-28 00:23:43', '2018-07-28 00:23:43', NULL),
(72, 1, 14, 2, 'RQ6Y79Z2FKGIF', NULL, NULL, 'First Customer', 'shantotrs@gmail.com', 'Buyer LTD', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'US', 'Raj', '12', 'gsdfgfga', 'District of Columbia', NULL, 10, '5656', 'United States', 1, '4576767', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 29.00, 0.00, 0.00, 0.00, 29.00, NULL, '2018-07-28 00:23:43', '2018-07-28 00:23:43', NULL),
(73, 1, 14, 1, 'RVTHHLB2A4WKW', NULL, NULL, 'First Customer', 'shantotrs@gmail.com', 'Buyer LTD', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'US', 'Raj', '12', 'gsdfgfga', 'District of Columbia', NULL, 10, '5656', 'United States', 1, '4576767', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 549.00, 0.00, 0.00, 0.00, 549.00, NULL, '2018-07-28 01:02:32', '2018-07-28 01:02:33', NULL),
(74, 1, 14, 2, '9ZOH1P3HW5WMP', NULL, NULL, 'First Customer', 'shantotrs@gmail.com', 'Buyer LTD', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'US', 'Raj', '12', 'gsdfgfga', 'District of Columbia', NULL, 10, '5656', 'United States', 1, '4576767', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 29.00, 0.00, 0.00, 0.00, 29.00, NULL, '2018-07-28 01:02:33', '2018-07-28 01:02:33', NULL),
(75, 2, 14, 1, 'JMPNNR9G3DRJW', NULL, NULL, 'First Customer', 'shantotrs@gmail.com', 'Buyer LTD', 'UPS Ground', 1, 1, 'US', 'Raj', NULL, 'gsdfgfga', 'District of Columbia', NULL, 10, NULL, 'United States', 1, '4576767', 'US', 'Raj', '12', 'gsdfgfga', 'District of Columbia', NULL, 10, '5656', 'United States', 1, '4576767', '375000000000007', 'adsfasd', '12/19', '1234', NULL, NULL, NULL, NULL, NULL, NULL, NULL, 50.00, 1, 'KURBANI2', '234234', 549.00, 50.00, 0.00, 0.00, 499.00, NULL, '2018-07-28 02:27:58', '2018-07-28 02:49:58', NULL),
(76, 2, 14, 2, 'NYVI3OQ65UCNA', NULL, NULL, 'First Customer', 'shantotrs@gmail.com', 'Buyer LTD', 'Truck', 1, 3, 'US', 'Raj', NULL, 'gsdfgfga', 'District of Columbia', NULL, 10, NULL, 'United States', 1, '4576767', 'US', 'Raj', '12', 'gsdfgfga', 'District of Columbia', NULL, 10, '5656', 'United States', 1, '4576767', '375000000000007', 'adsfasd', '12/19', '1234', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 29.00, 0.00, 0.00, 0.00, 29.00, NULL, '2018-07-28 02:27:58', '2018-07-28 02:50:00', NULL),
(77, 1, 14, 1, '1Y3009O2V1HXL', NULL, NULL, 'First Customer', 'shantotrs@gmail.com', 'Buyer LTD', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'US', 'Raj', '12', 'gsdfgfga', 'District of Columbia', NULL, 10, '5656', 'United States', 1, '4576767', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 50.00, 1, 'KURBANI2', '234234', 549.00, 50.00, 0.00, 0.00, 499.00, NULL, '2018-07-28 02:51:00', '2018-07-28 02:51:14', NULL),
(78, 1, 14, 2, 'ZK1X0129JUY5B', NULL, NULL, 'First Customer', 'shantotrs@gmail.com', 'Buyer LTD', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'US', 'Raj', '12', 'gsdfgfga', 'District of Columbia', NULL, 10, '5656', 'United States', 1, '4576767', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 29.00, 0.00, 0.00, 0.00, 29.00, NULL, '2018-07-28 02:51:00', '2018-07-28 02:51:00', NULL),
(79, 1, 14, 1, 'JPZV4SOYS16CV', NULL, NULL, 'First Customer', 'shantotrs@gmail.com', 'Buyer LTD', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'US', 'Raj', '12', 'gsdfgfga', 'District of Columbia', NULL, 10, '5656', 'United States', 1, '4576767', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 549.00, 0.00, 0.00, 0.00, 549.00, NULL, '2018-07-29 19:30:13', '2018-07-29 19:30:13', NULL),
(80, 1, 14, 2, 'ZNEPZARQIZYZ7', NULL, NULL, 'First Customer', 'shantotrs@gmail.com', 'Buyer LTD', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'US', 'Raj', '12', 'gsdfgfga', 'District of Columbia', NULL, 10, '5656', 'United States', 1, '4576767', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 29.00, 0.00, 0.00, 0.00, 29.00, NULL, '2018-07-29 19:30:13', '2018-07-29 19:30:13', NULL),
(81, 1, 14, 1, 'K53E64YWV0ZXY', NULL, NULL, 'First Customer', 'shantotrs@gmail.com', 'Buyer LTD', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'US', 'Raj', '12', 'gsdfgfga', 'District of Columbia', NULL, 10, '5656', 'United States', 1, '4576767', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 50.00, 1, 'KURBANI2', '234234', 549.00, 50.00, 0.00, 0.00, 499.00, NULL, '2018-07-30 01:27:31', '2018-07-30 01:32:22', NULL),
(82, 1, 14, 2, 'TG65RSUEZ3D0H', NULL, NULL, 'First Customer', 'shantotrs@gmail.com', 'Buyer LTD', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'US', 'Raj', '12', 'gsdfgfga', 'District of Columbia', NULL, 10, '5656', 'United States', 1, '4576767', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 29.00, 0.00, 0.00, 0.00, 29.00, NULL, '2018-07-30 01:27:31', '2018-07-30 01:27:31', NULL),
(83, 3, 14, 1, 'HCILEPO8TR32I', NULL, NULL, 'First Customer', 'shantotrs@gmail.com', 'Buyer LTD', 'UPS Ground', 1, 1, 'US', 'Raj', NULL, 'gsdfgfga', 'District of Columbia', NULL, 10, NULL, 'United States', 1, '4576767', 'US', 'Raj', '12', 'gsdfgfga', 'District of Columbia', NULL, 10, '5656', 'United States', 1, '4576767', '375000000000007', '2323', '12/19', '1234', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 549.00, 0.00, 0.00, 0.00, 549.00, NULL, '2018-07-30 19:50:26', '2018-07-31 19:34:29', NULL),
(84, 2, 14, 2, 'BY3T2AC0A2XTC', NULL, NULL, 'First Customer', 'shantotrs@gmail.com', 'Buyer LTD', 'Truck', 1, 3, 'US', 'Raj', NULL, 'gsdfgfga', 'District of Columbia', NULL, 10, NULL, 'United States', 1, '4576767', 'US', 'Raj', '12', 'gsdfgfga', 'District of Columbia', NULL, 10, '5656', 'United States', 1, '4576767', 'eyJpdiI6Ik93WWZRS2E4NFFnTFl6NWkxT2QwNmc9PSIsInZhbHVlIjoiXC9ydFo3WXNnNmpcL1gwcTViSVNHaHB1VzhOdFVQSEdtVVB1VVNwbEJMQk1zPSIsIm1hYyI6IjI4YTMwMmFlMGE3YzI4ZDk5ZGI1MmQ3OTNkNjhjZTBiMDU4MWNlMDBhZGJiM2UzZTE2OTIzODhiMjdkYTA2N2EifQ==', 'eyJpdiI6Im53Y0krcHpyS2txSWthY2hwWDVJQWc9PSIsInZhbHVlIjoiOHM4K3lLTEZpRklSNWhFOGJyODBIUT09IiwibWFjIjoiYzBlNWUyMTIzMzQ2Y2JkYmRlMmUwOWEzMzAyYjI4OTBjZjgzZjc0YjYxZDMxZjM3ZTYzYTcwMDgxNDIzMzQ4MCJ9', 'eyJpdiI6ImZoWmJKNU5WOEM0Q0RFOWNtZXMzZ1E9PSIsInZhbHVlIjoiZndlZ0FxSmZKYzNRK25WcnNFVWhJUT09IiwibWFjIjoiYjljNjk4ZGNkOTYxMTUyOGRmM2M1NWM4MTY3MjU2MzNlMTZlZTczNzdlMzdmNjkzODkxNzBiMDkzNDdiMDM3NyJ9', 'eyJpdiI6IkZkVTFsRUlqTkRcLzV4dGNiVkVKVXJRPT0iLCJ2YWx1ZSI6IlVwSXlWRzlReDg5eHlLXC9UNThiRDJnPT0iLCJtYWMiOiIxMTFlMjMwYzI1OWU0MDAyM2VkMTQ4NjIxZTIxYjAxMWQ5ZTdhMzBjMTlkZWQ1MjI3M2UwOWVmYmZiY2I0MGYwIn0=', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 29.00, 0.00, 0.00, 0.00, 29.00, 'treee', '2018-07-30 19:55:45', '2018-07-30 20:02:27', NULL),
(85, 1, 14, 1, '16ZE0BS39RDR8', NULL, NULL, 'First Customer', 'shantotrs@gmail.com', 'Buyer LTD', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'US', 'Raj', '12', 'gsdfgfga', 'District of Columbia', NULL, 10, '5656', 'United States', 1, '4576767', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 279.00, 0.00, 0.00, 0.00, 279.00, NULL, '2018-07-30 21:51:08', '2018-07-30 21:51:08', NULL),
(86, 1, 14, 1, '0MILOCM7FUZLL', NULL, NULL, 'First Customer', 'shantotrs@gmail.com', 'Buyer LTD', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'US', 'Raj', '12', 'gsdfgfga', 'District of Columbia', NULL, 10, '5656', 'United States', 1, '4576767', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 279.00, 0.00, 0.00, 0.00, 279.00, NULL, '2018-07-30 22:13:35', '2018-07-30 22:13:35', NULL),
(87, 1, 14, 1, 'AOKW1I1OUW34K', NULL, NULL, 'First Customer', 'shantotrs@gmail.com', 'Buyer LTD', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'US', 'Raj', '12', 'gsdfgfga', 'District of Columbia', NULL, 10, '5656', 'United States', 1, '4576767', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 279.00, 0.00, 0.00, 0.00, 279.00, NULL, '2018-07-31 01:02:24', '2018-07-31 01:02:24', NULL),
(88, 1, 14, 1, 'XKXNFFWY57XC4', NULL, NULL, 'First Customer', 'shantotrs@gmail.com', 'Buyer LTD', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'US', 'Raj', '12', 'gsdfgfga', 'District of Columbia', NULL, 10, '5656', 'United States', 1, '4576767', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 279.00, 0.00, 0.00, 0.00, 279.00, NULL, '2018-07-31 22:09:51', '2018-07-31 22:09:51', NULL),
(89, 1, 14, 1, 'B2JY55K3NAZMH', NULL, NULL, 'First Customer', 'shantotrs@gmail.com', 'Buyer LTD', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'US', 'Raj', '12', 'gsdfgfga', 'District of Columbia', NULL, 10, '5656', 'United States', 1, '4576767', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 279.00, 0.00, 0.00, 0.00, 279.00, NULL, '2018-07-31 22:37:36', '2018-07-31 22:37:36', NULL),
(90, 1, 14, 1, 'OQEAC95USAB86', NULL, NULL, 'First Customer', 'shantotrs@gmail.com', 'Buyer LTD', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'US', 'Raj', '12', 'gsdfgfga', 'District of Columbia', NULL, 10, '5656', 'United States', 1, '4576767', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 279.00, 0.00, 0.00, 0.00, 279.00, NULL, '2018-07-31 22:41:50', '2018-07-31 22:41:50', NULL),
(91, 2, 14, 1, 'Z0Y9X2BH56LY5', NULL, NULL, 'First Customer', 'shantotrs@gmail.com', 'Buyer LTD', 'UPS Ground', 1, 1, 'US', 'Raj', NULL, 'gsdfgfga', 'District of Columbia', NULL, 10, NULL, 'United States', 1, '4576767', 'US', 'Raj', '12', 'gsdfgfga', 'District of Columbia', NULL, 10, '5656', 'United States', 1, '4576767', 'eyJpdiI6IlBRWGtSYjFXR1JlQnpZWWcrYkphYnc9PSIsInZhbHVlIjoiUFhnYnllYW5hU1RYMWJBUEFxUGRuWmN0VFNSU1dFK1h6b0U2emludmFMRT0iLCJtYWMiOiJkNTZhYThlMDFhOTg3YTlmNGYxMjUxM2RmMzk0OTFiNmY3MGQ2MWQ5YmMwY2M4NGQyYjYxYmYyM2Q5OWZjNjk0In0=', 'eyJpdiI6ImhGOGJ5RHpwdG5mNGFveUY4OVg1YXc9PSIsInZhbHVlIjoieDlQYm1Nc3hjTEJ5UXRRXC9XVnN2Nnc9PSIsIm1hYyI6IjlkY2YyYjdmZDc2NmNlMTgzYzI5ZjRiMjYzODZiZGQ0NjhhZTg2Mzc5YjE1YjVjYWZhNmVmMThiZjAwMzU0ZmEifQ==', 'eyJpdiI6Im9jaVVlcExuZDg5MGZ1cENLQzF3aXc9PSIsInZhbHVlIjoiZDRuRUwzeVBQeGlxRGo2YUlrTkQwQT09IiwibWFjIjoiMzZjMDNiY2I2OWZlZjk2NGM2OGIzNjYyMGZkODM3MjgyOWM5ZDk1NTIwZTc1NzlkOWM1NmYwNmM5NWI1OGFiZSJ9', 'eyJpdiI6Im56TGhjNjY2bjh5TE1vVE5aUTNLaWc9PSIsInZhbHVlIjoid0pwRVZBdXdDbTA4aHJqWjVDcnNhQT09IiwibWFjIjoiNmQwOTExOGMxMzU4OWM2NTRkMGEyZTE2Yjk4ODc4ZjdkMjAyNzI4Y2ZmMzdhNzAwNGFhMTU2OGFjZTU3NTU1MiJ9', 1, '123', NULL, 3, '123123', '12312', 'US', NULL, NULL, NULL, NULL, 279.00, 0.00, 0.00, 0.00, 279.00, NULL, '2018-08-01 01:40:15', '2018-08-01 02:58:54', NULL),
(92, 2, 14, 1, 'D43VLX6EZ6SEZ', NULL, NULL, 'First Customer', 'shantotrs@gmail.com', 'Buyer LTD', 'UPS Ground', 1, 1, 'US', 'Raj', NULL, 'gsdfgfga', 'District of Columbia', NULL, 10, NULL, 'United States', 1, '4576767', 'US', 'Raj', '12', 'gsdfgfga', 'District of Columbia', NULL, 10, '5656', 'United States', 1, '4576767', 'eyJpdiI6Ik5jMngwMkpLM1ZlSUNXc1p5XC93SHNRPT0iLCJ2YWx1ZSI6IlVBZjFhdjROMGhwd3ZROUtHWWd6eXV3VEFoc2dEMEo5WXAxSEk5dkdcL2ZzPSIsIm1hYyI6IjM2N2IzMzYzOGM2NDNmZDhjOTM3MTNiYTY2MTRlYzU1MDk4YzE3ZTk4M2ZhZDk3MTlkNzhkYThiMjVlMWNiNWEifQ==', 'eyJpdiI6InZOekxHUTI4bEJ6emNKbW9rUytXTmc9PSIsInZhbHVlIjoiWjR0TzU1RHJnZmVPd1hqNFh3UmUyUT09IiwibWFjIjoiZjViNjA3MDRmY2Y1Y2JlZWU1ODVkOGVkNmNiYzMxMGNlZmU1ODhhYmE0ZWM5MmRiYjY1YzkxZmUyODU1ODMyYSJ9', 'eyJpdiI6IkU3cksrM1drOHRNVTkwc1NCdElVYkE9PSIsInZhbHVlIjoiYWJDc0FpTm81a0ZCNUc4MXRMYUhQUT09IiwibWFjIjoiNTc4Mzg0NTc4ZTZhZDZkZmQyMDIyMmUyZjhjNGE4NjQ3MGM4MzBjZmZlMzVjMTg1YTQ3ODM1MDFiOTVkNzEwZSJ9', 'eyJpdiI6InlOcTkwSU9QZWdjSFdCdGRiRVAzXC9RPT0iLCJ2YWx1ZSI6IkZBV3B5Q3craGZqODVnSUNwYnJPUkE9PSIsIm1hYyI6IjA3OTU0M2Y4Y2UyYjgwZGNlM2VlNjZjNmNjNWFiMWVmZWY0MDMxODEwNTA5MWQ3MGVlZTU5MWQ3NmI2NjNkODkifQ==', 2, '2323', NULL, 67, '2323', '2323', 'CA', NULL, NULL, NULL, NULL, 279.00, 0.00, 0.00, 0.00, 279.00, NULL, '2018-08-01 03:01:09', '2018-08-01 03:01:17', NULL),
(93, 2, 14, 1, 'H0HXTWGSCOZ3C', NULL, NULL, 'First Customer', 'shantotrs@gmail.com', 'Buyer LTD', 'UPS Ground', 1, 1, 'US', 'Raj', NULL, 'gsdfgfga', 'District of Columbia', NULL, 10, NULL, 'United States', 1, '4576767', 'US', 'Raj', '12', 'gsdfgfga', 'District of Columbia', NULL, 10, '5656', 'United States', 1, '4576767', 'eyJpdiI6Ik5jMngwMkpLM1ZlSUNXc1p5XC93SHNRPT0iLCJ2YWx1ZSI6IlVBZjFhdjROMGhwd3ZROUtHWWd6eXV3VEFoc2dEMEo5WXAxSEk5dkdcL2ZzPSIsIm1hYyI6IjM2N2IzMzYzOGM2NDNmZDhjOTM3MTNiYTY2MTRlYzU1MDk4YzE3ZTk4M2ZhZDk3MTlkNzhkYThiMjVlMWNiNWEifQ==', 'eyJpdiI6InZOekxHUTI4bEJ6emNKbW9rUytXTmc9PSIsInZhbHVlIjoiWjR0TzU1RHJnZmVPd1hqNFh3UmUyUT09IiwibWFjIjoiZjViNjA3MDRmY2Y1Y2JlZWU1ODVkOGVkNmNiYzMxMGNlZmU1ODhhYmE0ZWM5MmRiYjY1YzkxZmUyODU1ODMyYSJ9', 'eyJpdiI6IjR2ODk3MVhzSVwvUzVkdmZhMU1IekdnPT0iLCJ2YWx1ZSI6IjRGVG9uS1dBTDJUNFV2QlJaM0RVVHc9PSIsIm1hYyI6IjVjYjE0ZmIxYjY5MmUyZjVhNThkYmU5MTM2OWM0NDRkNDhlMzFiNjIyNjdmZWRlMzQ1ZjViNDQzYWZkZDgxMmYifQ==', 'eyJpdiI6InlOcTkwSU9QZWdjSFdCdGRiRVAzXC9RPT0iLCJ2YWx1ZSI6IkZBV3B5Q3craGZqODVnSUNwYnJPUkE9PSIsIm1hYyI6IjA3OTU0M2Y4Y2UyYjgwZGNlM2VlNjZjNmNjNWFiMWVmZWY0MDMxODEwNTA5MWQ3MGVlZTU5MWQ3NmI2NjNkODkifQ==', 2, '2323', NULL, 67, '2323', '2323', 'CA', NULL, NULL, NULL, NULL, 504.00, 0.00, 0.00, 0.00, 504.00, NULL, '2018-08-01 19:08:51', '2018-08-01 19:14:06', NULL),
(94, 2, 14, 1, 'ZVUTM1OQ42HSF', NULL, '3434', 'First Customer', 'shantotrs@gmail.com', 'Buyer LTD', 'UPS Ground', 1, 1, 'US', 'Raj', NULL, 'gsdfgfga', 'District of Columbia', NULL, 10, NULL, 'United States', 1, '4576767', 'US', 'Raj', '12', 'gsdfgfga', 'District of Columbia', NULL, 10, '5656', 'United States', 1, '4576767', 'eyJpdiI6Ik5jMngwMkpLM1ZlSUNXc1p5XC93SHNRPT0iLCJ2YWx1ZSI6IlVBZjFhdjROMGhwd3ZROUtHWWd6eXV3VEFoc2dEMEo5WXAxSEk5dkdcL2ZzPSIsIm1hYyI6IjM2N2IzMzYzOGM2NDNmZDhjOTM3MTNiYTY2MTRlYzU1MDk4YzE3ZTk4M2ZhZDk3MTlkNzhkYThiMjVlMWNiNWEifQ==', 'eyJpdiI6InZOekxHUTI4bEJ6emNKbW9rUytXTmc9PSIsInZhbHVlIjoiWjR0TzU1RHJnZmVPd1hqNFh3UmUyUT09IiwibWFjIjoiZjViNjA3MDRmY2Y1Y2JlZWU1ODVkOGVkNmNiYzMxMGNlZmU1ODhhYmE0ZWM5MmRiYjY1YzkxZmUyODU1ODMyYSJ9', 'eyJpdiI6IjJ5c2NSTFVKOVZPTmU1QjNVNFE5eWc9PSIsInZhbHVlIjoiNlpUNDNOUTVCXC91VGxXdjBtRklFY2c9PSIsIm1hYyI6IjU2NTk5ZWY2MzE0NDkyZWUyNjdjZTVmMDc5NGE3ZDkwMDA1MWI2Y2MwMDI5NTFlZWUxMGI5YmQxN2U0NmI2M2QifQ==', 'eyJpdiI6InlOcTkwSU9QZWdjSFdCdGRiRVAzXC9RPT0iLCJ2YWx1ZSI6IkZBV3B5Q3craGZqODVnSUNwYnJPUkE9PSIsIm1hYyI6IjA3OTU0M2Y4Y2UyYjgwZGNlM2VlNjZjNmNjNWFiMWVmZWY0MDMxODEwNTA5MWQ3MGVlZTU5MWQ3NmI2NjNkODkifQ==', 2, '2323', NULL, 67, '2323', '2323', 'CA', NULL, NULL, NULL, NULL, 318.00, 0.00, 0.00, 0.00, 318.00, NULL, '2018-08-01 20:37:46', '2018-08-16 19:36:51', NULL),
(95, 2, 14, 2, '6S89G1S48SK9J', NULL, NULL, 'First Customer', 'shantotrs@gmail.com', 'Buyer LTD', 'Truck', 1, 3, 'US', 'Raj', NULL, 'gsdfgfga', 'District of Columbia', NULL, 10, NULL, 'United States', 1, '4576767', 'US', 'Raj', '12', 'gsdfgfga', 'District of Columbia', NULL, 10, '5656', 'United States', 1, '4576767', 'eyJpdiI6Ik5jMngwMkpLM1ZlSUNXc1p5XC93SHNRPT0iLCJ2YWx1ZSI6IlVBZjFhdjROMGhwd3ZROUtHWWd6eXV3VEFoc2dEMEo5WXAxSEk5dkdcL2ZzPSIsIm1hYyI6IjM2N2IzMzYzOGM2NDNmZDhjOTM3MTNiYTY2MTRlYzU1MDk4YzE3ZTk4M2ZhZDk3MTlkNzhkYThiMjVlMWNiNWEifQ==', 'eyJpdiI6InZOekxHUTI4bEJ6emNKbW9rUytXTmc9PSIsInZhbHVlIjoiWjR0TzU1RHJnZmVPd1hqNFh3UmUyUT09IiwibWFjIjoiZjViNjA3MDRmY2Y1Y2JlZWU1ODVkOGVkNmNiYzMxMGNlZmU1ODhhYmE0ZWM5MmRiYjY1YzkxZmUyODU1ODMyYSJ9', 'eyJpdiI6ImpLQmljTFBCSWt3djdVdEh0SEdJMFE9PSIsInZhbHVlIjoicVlFNWs4RGdycEFMUHIrN29adzMxZz09IiwibWFjIjoiM2E5YTBjMzVmYWE4YWY1OWMyYjAwYTAzMzgxMjJhYzMyZDFmNWZhOWM4NDA4NmQwNmZkYzYxYjAyM2IwNTZiNiJ9', 'eyJpdiI6InlOcTkwSU9QZWdjSFdCdGRiRVAzXC9RPT0iLCJ2YWx1ZSI6IkZBV3B5Q3craGZqODVnSUNwYnJPUkE9PSIsIm1hYyI6IjA3OTU0M2Y4Y2UyYjgwZGNlM2VlNjZjNmNjNWFiMWVmZWY0MDMxODEwNTA5MWQ3MGVlZTU5MWQ3NmI2NjNkODkifQ==', 2, '2323', NULL, 67, '2323', '2323', 'CA', NULL, NULL, NULL, NULL, 116.00, 0.00, 0.00, 0.00, 116.00, NULL, '2018-08-01 20:37:46', '2018-08-01 20:37:57', NULL),
(96, 1, 14, 1, '3JXS1LXZ8BSTS', NULL, NULL, 'First Customer', 'shantotrs@gmail.com', 'Buyer LTD', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'US', 'Raj', '12', 'gsdfgfga', 'District of Columbia', NULL, 10, '5656', 'United States', 1, '4576767', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 225.00, 0.00, 0.00, 0.00, 225.00, NULL, '2018-08-01 21:00:15', '2018-08-01 21:00:15', NULL),
(97, 1, 14, 1, 'VTIM9HWFFBGT6', NULL, NULL, 'First Customer', 'shantotrs@gmail.com', 'Buyer LTD', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'US', 'Raj', '12', 'gsdfgfga', 'District of Columbia', NULL, 10, '5656', 'United States', 1, '4576767', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 225.00, 0.00, 0.00, 0.00, 225.00, NULL, '2018-08-01 21:25:24', '2018-08-01 21:25:24', NULL),
(98, 1, 14, 2, 'I162B4YWAB3ZW', NULL, NULL, 'First Customer', 'shantotrs@gmail.com', 'Buyer LTD', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'US', 'Raj', '12', 'gsdfgfga', 'District of Columbia', NULL, 10, '5656', 'United States', 1, '4576767', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 116.00, 0.00, 0.00, 0.00, 116.00, NULL, '2018-08-09 19:22:27', '2018-08-09 19:22:27', NULL),
(99, 1, 14, 1, 'CF4IPQ827QSTH', NULL, NULL, 'First Customer', 'shantotrs@gmail.com', 'Buyer LTD', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'US', 'Raj', '12', 'gsdfgfga', 'District of Columbia', NULL, 10, '5656', 'United States', 1, '4576767', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 50.00, 1, 'KURBANI2', '234234', 93.00, 50.00, 0.00, 0.00, 43.00, NULL, '2018-08-09 19:22:55', '2018-08-09 19:23:01', NULL),
(100, 1, 14, 1, 'O4IGR9HQ3V60P', NULL, NULL, 'First Customer', 'shantotrs@gmail.com', 'Buyer LTD', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'US', 'Raj', '12', 'gsdfgfga', 'District of Columbia', NULL, 10, '5656', 'United States', 1, '4576767', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 50.00, 1, 'KURBANI2', '234234', 93.00, 50.00, 0.00, 0.00, 43.00, NULL, '2018-08-09 19:40:05', '2018-08-09 19:40:14', NULL),
(101, 6, 14, 1, 'GNEFX483OC6DB', NULL, NULL, 'First Customer', 'shantotrs@gmail.com', 'Buyer LTD', 'UPS Ground', NULL, 1, NULL, 'Raj', NULL, 'gsdfgfga', 'District of Columbia', NULL, NULL, NULL, 'United States', NULL, '4576767', NULL, 'Raj', NULL, 'gsdfgfga', 'District of Columbia', NULL, NULL, '5656', 'United States', NULL, '4576767', 'eyJpdiI6Ik5jMngwMkpLM1ZlSUNXc1p5XC93SHNRPT0iLCJ2YWx1ZSI6IlVBZjFhdjROMGhwd3ZROUtHWWd6eXV3VEFoc2dEMEo5WXAxSEk5dkdcL2ZzPSIsIm1hYyI6IjM2N2IzMzYzOGM2NDNmZDhjOTM3MTNiYTY2MTRlYzU1MDk4YzE3ZTk4M2ZhZDk3MTlkNzhkYThiMjVlMWNiNWEifQ==', 'eyJpdiI6InZOekxHUTI4bEJ6emNKbW9rUytXTmc9PSIsInZhbHVlIjoiWjR0TzU1RHJnZmVPd1hqNFh3UmUyUT09IiwibWFjIjoiZjViNjA3MDRmY2Y1Y2JlZWU1ODVkOGVkNmNiYzMxMGNlZmU1ODhhYmE0ZWM5MmRiYjY1YzkxZmUyODU1ODMyYSJ9', 'eyJpdiI6IjJ5c2NSTFVKOVZPTmU1QjNVNFE5eWc9PSIsInZhbHVlIjoiNlpUNDNOUTVCXC91VGxXdjBtRklFY2c9PSIsIm1hYyI6IjU2NTk5ZWY2MzE0NDkyZWUyNjdjZTVmMDc5NGE3ZDkwMDA1MWI2Y2MwMDI5NTFlZWUxMGI5YmQxN2U0NmI2M2QifQ==', 'eyJpdiI6InlOcTkwSU9QZWdjSFdCdGRiRVAzXC9RPT0iLCJ2YWx1ZSI6IkZBV3B5Q3craGZqODVnSUNwYnJPUkE9PSIsIm1hYyI6IjA3OTU0M2Y4Y2UyYjgwZGNlM2VlNjZjNmNjNWFiMWVmZWY0MDMxODEwNTA5MWQ3MGVlZTU5MWQ3NmI2NjNkODkifQ==', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 186.00, 0.00, 0.00, 0.00, 186.00, NULL, '2018-08-16 19:35:50', '2018-08-16 19:35:50', NULL),
(102, 6, 14, 1, 'BB4DZA3681YKB', NULL, NULL, 'First Customer', 'shantotrs@gmail.com', 'Buyer LTD', 'UPS Ground', NULL, 1, NULL, 'Raj', NULL, 'gsdfgfga', 'District of Columbia', NULL, NULL, NULL, 'United States', NULL, '4576767', NULL, 'Raj', NULL, 'gsdfgfga', 'District of Columbia', NULL, NULL, '5656', 'United States', NULL, '4576767', 'eyJpdiI6Ik5jMngwMkpLM1ZlSUNXc1p5XC93SHNRPT0iLCJ2YWx1ZSI6IlVBZjFhdjROMGhwd3ZROUtHWWd6eXV3VEFoc2dEMEo5WXAxSEk5dkdcL2ZzPSIsIm1hYyI6IjM2N2IzMzYzOGM2NDNmZDhjOTM3MTNiYTY2MTRlYzU1MDk4YzE3ZTk4M2ZhZDk3MTlkNzhkYThiMjVlMWNiNWEifQ==', 'eyJpdiI6InZOekxHUTI4bEJ6emNKbW9rUytXTmc9PSIsInZhbHVlIjoiWjR0TzU1RHJnZmVPd1hqNFh3UmUyUT09IiwibWFjIjoiZjViNjA3MDRmY2Y1Y2JlZWU1ODVkOGVkNmNiYzMxMGNlZmU1ODhhYmE0ZWM5MmRiYjY1YzkxZmUyODU1ODMyYSJ9', 'eyJpdiI6IjJ5c2NSTFVKOVZPTmU1QjNVNFE5eWc9PSIsInZhbHVlIjoiNlpUNDNOUTVCXC91VGxXdjBtRklFY2c9PSIsIm1hYyI6IjU2NTk5ZWY2MzE0NDkyZWUyNjdjZTVmMDc5NGE3ZDkwMDA1MWI2Y2MwMDI5NTFlZWUxMGI5YmQxN2U0NmI2M2QifQ==', 'eyJpdiI6InlOcTkwSU9QZWdjSFdCdGRiRVAzXC9RPT0iLCJ2YWx1ZSI6IkZBV3B5Q3craGZqODVnSUNwYnJPUkE9PSIsIm1hYyI6IjA3OTU0M2Y4Y2UyYjgwZGNlM2VlNjZjNmNjNWFiMWVmZWY0MDMxODEwNTA5MWQ3MGVlZTU5MWQ3NmI2NjNkODkifQ==', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 75.00, 0.00, 0.00, 0.00, 75.00, NULL, '2018-08-16 19:36:51', '2018-09-04 20:02:35', '2018-09-04 20:02:35'),
(103, 5, 14, 1, '1ROVDYK41NVL6', NULL, NULL, 'First Customer', 'shantotrs@gmail.com', 'Buyer LTD', 'UPS Ground', 1, 1, 'US', 'Raj', NULL, 'gsdfgfga', 'District of Columbia', NULL, 10, NULL, 'United States', 1, '4576767', 'US', 'Raj', '12', 'gsdfgfga', 'District of Columbia', NULL, 10, '5656', 'United States', 1, '4576767', 'eyJpdiI6Ik5jMngwMkpLM1ZlSUNXc1p5XC93SHNRPT0iLCJ2YWx1ZSI6IlVBZjFhdjROMGhwd3ZROUtHWWd6eXV3VEFoc2dEMEo5WXAxSEk5dkdcL2ZzPSIsIm1hYyI6IjM2N2IzMzYzOGM2NDNmZDhjOTM3MTNiYTY2MTRlYzU1MDk4YzE3ZTk4M2ZhZDk3MTlkNzhkYThiMjVlMWNiNWEifQ==', 'eyJpdiI6InZOekxHUTI4bEJ6emNKbW9rUytXTmc9PSIsInZhbHVlIjoiWjR0TzU1RHJnZmVPd1hqNFh3UmUyUT09IiwibWFjIjoiZjViNjA3MDRmY2Y1Y2JlZWU1ODVkOGVkNmNiYzMxMGNlZmU1ODhhYmE0ZWM5MmRiYjY1YzkxZmUyODU1ODMyYSJ9', 'eyJpdiI6IkwwaDhBczJkT256d2FVcVhhM21Zdnc9PSIsInZhbHVlIjoiN1M2ckRKTSs5dmE3Wlpyd3RabXl6dz09IiwibWFjIjoiYzE0NjJhMTAwOGQxNGI3Yzg1ZjA2ZGFmN2Y4YjE3ZmRjOTljNDg5OWZiZGE1Y2I2NTcxYmFhZDg0MGU3ZDkwYyJ9', 'eyJpdiI6InlOcTkwSU9QZWdjSFdCdGRiRVAzXC9RPT0iLCJ2YWx1ZSI6IkZBV3B5Q3craGZqODVnSUNwYnJPUkE9PSIsIm1hYyI6IjA3OTU0M2Y4Y2UyYjgwZGNlM2VlNjZjNmNjNWFiMWVmZWY0MDMxODEwNTA5MWQ3MGVlZTU5MWQ3NmI2NjNkODkifQ==', 2, '2323', NULL, 67, '2323', '2323', 'CA', NULL, NULL, NULL, NULL, 93.00, 0.00, 0.00, 0.00, 93.00, NULL, '2018-08-28 19:23:11', '2018-09-04 19:55:14', NULL),
(104, 2, 14, 2, 'SCP48TK1BFL1D', NULL, NULL, 'First Customer', 'shantotrs@gmail.com', 'Buyer LTD', 'Truck', 1, 3, 'US', 'Raj', NULL, 'gsdfgfga', 'District of Columbia', NULL, 10, NULL, 'United States', 1, '4576767', 'US', 'Raj', '12', 'gsdfgfga', 'District of Columbia', NULL, 10, '5656', 'United States', 1, '4576767', 'eyJpdiI6Ik5jMngwMkpLM1ZlSUNXc1p5XC93SHNRPT0iLCJ2YWx1ZSI6IlVBZjFhdjROMGhwd3ZROUtHWWd6eXV3VEFoc2dEMEo5WXAxSEk5dkdcL2ZzPSIsIm1hYyI6IjM2N2IzMzYzOGM2NDNmZDhjOTM3MTNiYTY2MTRlYzU1MDk4YzE3ZTk4M2ZhZDk3MTlkNzhkYThiMjVlMWNiNWEifQ==', 'eyJpdiI6InZOekxHUTI4bEJ6emNKbW9rUytXTmc9PSIsInZhbHVlIjoiWjR0TzU1RHJnZmVPd1hqNFh3UmUyUT09IiwibWFjIjoiZjViNjA3MDRmY2Y1Y2JlZWU1ODVkOGVkNmNiYzMxMGNlZmU1ODhhYmE0ZWM5MmRiYjY1YzkxZmUyODU1ODMyYSJ9', 'eyJpdiI6IlkzaHUwRXhYSElGNXZObDBDbEtjV1E9PSIsInZhbHVlIjoiVXlSYWY2dG1rZ000ak5salBMWlZBQT09IiwibWFjIjoiNmRjY2ZhMWZmMTdlYTcxZjU2NzdhZWZkZWU5MmI5YWIwOWI0NzAxNjY4MDVjN2UwZDJjZjU0ZjczYWMzMzFmMiJ9', 'eyJpdiI6InlOcTkwSU9QZWdjSFdCdGRiRVAzXC9RPT0iLCJ2YWx1ZSI6IkZBV3B5Q3craGZqODVnSUNwYnJPUkE9PSIsIm1hYyI6IjA3OTU0M2Y4Y2UyYjgwZGNlM2VlNjZjNmNjNWFiMWVmZWY0MDMxODEwNTA5MWQ3MGVlZTU5MWQ3NmI2NjNkODkifQ==', 2, '2323', NULL, 67, '2323', '2323', 'CA', NULL, NULL, NULL, NULL, 116.00, 0.00, 0.00, 0.00, 116.00, NULL, '2018-08-28 19:23:11', '2018-08-28 19:23:28', NULL),
(105, 6, 14, 1, 'T0UKI8O8RQY0R', NULL, NULL, 'First Customer', 'shantotrs@gmail.com', 'Buyer LTD', 'UPS Ground', NULL, 1, NULL, 'Raj', NULL, 'gsdfgfga', 'District of Columbia', NULL, NULL, NULL, 'United States', NULL, '4576767', NULL, 'Raj', NULL, 'gsdfgfga', 'District of Columbia', NULL, NULL, '5656', 'United States', NULL, '4576767', 'eyJpdiI6Ik5jMngwMkpLM1ZlSUNXc1p5XC93SHNRPT0iLCJ2YWx1ZSI6IlVBZjFhdjROMGhwd3ZROUtHWWd6eXV3VEFoc2dEMEo5WXAxSEk5dkdcL2ZzPSIsIm1hYyI6IjM2N2IzMzYzOGM2NDNmZDhjOTM3MTNiYTY2MTRlYzU1MDk4YzE3ZTk4M2ZhZDk3MTlkNzhkYThiMjVlMWNiNWEifQ==', 'eyJpdiI6InZOekxHUTI4bEJ6emNKbW9rUytXTmc9PSIsInZhbHVlIjoiWjR0TzU1RHJnZmVPd1hqNFh3UmUyUT09IiwibWFjIjoiZjViNjA3MDRmY2Y1Y2JlZWU1ODVkOGVkNmNiYzMxMGNlZmU1ODhhYmE0ZWM5MmRiYjY1YzkxZmUyODU1ODMyYSJ9', 'eyJpdiI6IjJ5c2NSTFVKOVZPTmU1QjNVNFE5eWc9PSIsInZhbHVlIjoiNlpUNDNOUTVCXC91VGxXdjBtRklFY2c9PSIsIm1hYyI6IjU2NTk5ZWY2MzE0NDkyZWUyNjdjZTVmMDc5NGE3ZDkwMDA1MWI2Y2MwMDI5NTFlZWUxMGI5YmQxN2U0NmI2M2QifQ==', 'eyJpdiI6InlOcTkwSU9QZWdjSFdCdGRiRVAzXC9RPT0iLCJ2YWx1ZSI6IkZBV3B5Q3craGZqODVnSUNwYnJPUkE9PSIsIm1hYyI6IjA3OTU0M2Y4Y2UyYjgwZGNlM2VlNjZjNmNjNWFiMWVmZWY0MDMxODEwNTA5MWQ3MGVlZTU5MWQ3NmI2NjNkODkifQ==', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 75.00, 0.00, 0.00, 0.00, 75.00, NULL, '2018-09-04 20:02:35', '2018-09-04 20:02:35', NULL),
(106, 2, 14, 1, '9896G1LPP2JQY', NULL, NULL, 'First Customer', 'shantotrs@gmail.com', 'Buyer LTD', 'UPS Ground', 1, 1, 'US', 'Raj', NULL, 'gsdfgfga', 'District of Columbia', NULL, 10, NULL, 'United States', 1, '4576767', 'US', 'Raj', '12', 'gsdfgfga', 'District of Columbia', NULL, 10, '5656', 'United States', 1, '4576767', 'eyJpdiI6Ik5jMngwMkpLM1ZlSUNXc1p5XC93SHNRPT0iLCJ2YWx1ZSI6IlVBZjFhdjROMGhwd3ZROUtHWWd6eXV3VEFoc2dEMEo5WXAxSEk5dkdcL2ZzPSIsIm1hYyI6IjM2N2IzMzYzOGM2NDNmZDhjOTM3MTNiYTY2MTRlYzU1MDk4YzE3ZTk4M2ZhZDk3MTlkNzhkYThiMjVlMWNiNWEifQ==', 'eyJpdiI6InZOekxHUTI4bEJ6emNKbW9rUytXTmc9PSIsInZhbHVlIjoiWjR0TzU1RHJnZmVPd1hqNFh3UmUyUT09IiwibWFjIjoiZjViNjA3MDRmY2Y1Y2JlZWU1ODVkOGVkNmNiYzMxMGNlZmU1ODhhYmE0ZWM5MmRiYjY1YzkxZmUyODU1ODMyYSJ9', 'eyJpdiI6ImV6MHRXb1dlM2ZqR0twb05tRk5KQmc9PSIsInZhbHVlIjoiSW9Ec3YzWDZqTXl0Qm1paEZwK1dZUT09IiwibWFjIjoiMTA5OTM5OWMyMjRkNWNmY2FlZjIzMjg0ZmJjNThjMWM3ZmEyMGQyMWM0MDdlZTg2MDUxYjY5NzkwMjhhNGZjMCJ9', 'eyJpdiI6InlOcTkwSU9QZWdjSFdCdGRiRVAzXC9RPT0iLCJ2YWx1ZSI6IkZBV3B5Q3craGZqODVnSUNwYnJPUkE9PSIsIm1hYyI6IjA3OTU0M2Y4Y2UyYjgwZGNlM2VlNjZjNmNjNWFiMWVmZWY0MDMxODEwNTA5MWQ3MGVlZTU5MWQ3NmI2NjNkODkifQ==', 2, '2323', NULL, 67, '2323', '2323', 'CA', NULL, NULL, NULL, NULL, 1457.10, 0.00, 0.00, 0.00, 1457.10, NULL, '2018-09-04 20:04:58', '2018-09-04 20:06:49', NULL),
(107, 6, 14, 1, 'BHB4FFYO29G67', NULL, NULL, 'First Customer', 'shantotrs@gmail.com', 'Buyer LTD', 'UPS Ground', NULL, 1, NULL, 'Raj', NULL, 'gsdfgfga', 'District of Columbia', NULL, NULL, NULL, 'United States', NULL, '4576767', NULL, 'Raj', NULL, 'gsdfgfga', 'District of Columbia', NULL, NULL, '5656', 'United States', NULL, '4576767', 'eyJpdiI6Ik5jMngwMkpLM1ZlSUNXc1p5XC93SHNRPT0iLCJ2YWx1ZSI6IlVBZjFhdjROMGhwd3ZROUtHWWd6eXV3VEFoc2dEMEo5WXAxSEk5dkdcL2ZzPSIsIm1hYyI6IjM2N2IzMzYzOGM2NDNmZDhjOTM3MTNiYTY2MTRlYzU1MDk4YzE3ZTk4M2ZhZDk3MTlkNzhkYThiMjVlMWNiNWEifQ==', 'eyJpdiI6InZOekxHUTI4bEJ6emNKbW9rUytXTmc9PSIsInZhbHVlIjoiWjR0TzU1RHJnZmVPd1hqNFh3UmUyUT09IiwibWFjIjoiZjViNjA3MDRmY2Y1Y2JlZWU1ODVkOGVkNmNiYzMxMGNlZmU1ODhhYmE0ZWM5MmRiYjY1YzkxZmUyODU1ODMyYSJ9', 'eyJpdiI6ImV6MHRXb1dlM2ZqR0twb05tRk5KQmc9PSIsInZhbHVlIjoiSW9Ec3YzWDZqTXl0Qm1paEZwK1dZUT09IiwibWFjIjoiMTA5OTM5OWMyMjRkNWNmY2FlZjIzMjg0ZmJjNThjMWM3ZmEyMGQyMWM0MDdlZTg2MDUxYjY5NzkwMjhhNGZjMCJ9', 'eyJpdiI6InlOcTkwSU9QZWdjSFdCdGRiRVAzXC9RPT0iLCJ2YWx1ZSI6IkZBV3B5Q3craGZqODVnSUNwYnJPUkE9PSIsIm1hYyI6IjA3OTU0M2Y4Y2UyYjgwZGNlM2VlNjZjNmNjNWFiMWVmZWY0MDMxODEwNTA5MWQ3MGVlZTU5MWQ3NmI2NjNkODkifQ==', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 93.00, 0.00, 0.00, 0.00, 93.00, NULL, '2018-09-04 20:05:46', '2018-09-04 20:05:46', NULL),
(108, 2, 14, 1, '9Z1Q63HKEKRTT', NULL, NULL, 'First Customer', 'shantotrs@gmail.com', 'Buyer LTD', 'UPS Ground', 1, 1, 'US', 'Raj', NULL, 'gsdfgfga', 'District of Columbia', NULL, 10, NULL, 'United States', 1, '4576767', 'US', 'Raj', '12', 'gsdfgfga', 'District of Columbia', NULL, 10, '5656', 'United States', 1, '4576767', 'eyJpdiI6Ik5jMngwMkpLM1ZlSUNXc1p5XC93SHNRPT0iLCJ2YWx1ZSI6IlVBZjFhdjROMGhwd3ZROUtHWWd6eXV3VEFoc2dEMEo5WXAxSEk5dkdcL2ZzPSIsIm1hYyI6IjM2N2IzMzYzOGM2NDNmZDhjOTM3MTNiYTY2MTRlYzU1MDk4YzE3ZTk4M2ZhZDk3MTlkNzhkYThiMjVlMWNiNWEifQ==', 'eyJpdiI6InZOekxHUTI4bEJ6emNKbW9rUytXTmc9PSIsInZhbHVlIjoiWjR0TzU1RHJnZmVPd1hqNFh3UmUyUT09IiwibWFjIjoiZjViNjA3MDRmY2Y1Y2JlZWU1ODVkOGVkNmNiYzMxMGNlZmU1ODhhYmE0ZWM5MmRiYjY1YzkxZmUyODU1ODMyYSJ9', 'eyJpdiI6IjN3VldadnZPRFZCM1VrMEVTYjFNN0E9PSIsInZhbHVlIjoiY0xTMUwzNmVobVwva01VS0tLOFNOZUE9PSIsIm1hYyI6IjQ4NGJlZmIzNTQxMDkzNzJmYmYyMDZjZDI4ZTMzYzQzNDJkZWMyNzljMDI2MDU3ZjEyYzNlNWNjM2I2YTdkYzUifQ==', 'eyJpdiI6InlOcTkwSU9QZWdjSFdCdGRiRVAzXC9RPT0iLCJ2YWx1ZSI6IkZBV3B5Q3craGZqODVnSUNwYnJPUkE9PSIsIm1hYyI6IjA3OTU0M2Y4Y2UyYjgwZGNlM2VlNjZjNmNjNWFiMWVmZWY0MDMxODEwNTA5MWQ3MGVlZTU5MWQ3NmI2NjNkODkifQ==', 2, '2323', NULL, 67, '2323', '2323', 'CA', NULL, NULL, NULL, NULL, 90.00, 0.00, 0.00, 0.00, 90.00, NULL, '2018-09-13 20:27:41', '2018-09-13 20:27:49', NULL),
(109, 1, 14, 1, 'SQZYQ0PVDBM0U', NULL, NULL, 'First Customer', 'shantotrs@gmail.com', 'Buyer LTD', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'US', 'Raj', '12', 'gsdfgfga', 'District of Columbia', NULL, 10, '5656', 'United States', 1, '4576767', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 90.00, 0.00, 0.00, 0.00, 90.00, NULL, '2018-09-13 20:32:54', '2018-09-13 20:32:54', NULL),
(110, 2, 14, 1, '8X3O3I8FDVPED', NULL, NULL, 'First Customer', 'shantotrs@gmail.com', 'Buyer LTD', 'UPS Ground', 1, 1, 'US', 'Raj', NULL, 'gsdfgfga', 'DC', NULL, 10, '5656', 'United States', 1, '4576767', 'US', 'Raj', '12', 'gsdfgfga', 'DC', NULL, 10, '5656', 'United States', 1, '4576767', 'eyJpdiI6Ik5jMngwMkpLM1ZlSUNXc1p5XC93SHNRPT0iLCJ2YWx1ZSI6IlVBZjFhdjROMGhwd3ZROUtHWWd6eXV3VEFoc2dEMEo5WXAxSEk5dkdcL2ZzPSIsIm1hYyI6IjM2N2IzMzYzOGM2NDNmZDhjOTM3MTNiYTY2MTRlYzU1MDk4YzE3ZTk4M2ZhZDk3MTlkNzhkYThiMjVlMWNiNWEifQ==', 'eyJpdiI6InZOekxHUTI4bEJ6emNKbW9rUytXTmc9PSIsInZhbHVlIjoiWjR0TzU1RHJnZmVPd1hqNFh3UmUyUT09IiwibWFjIjoiZjViNjA3MDRmY2Y1Y2JlZWU1ODVkOGVkNmNiYzMxMGNlZmU1ODhhYmE0ZWM5MmRiYjY1YzkxZmUyODU1ODMyYSJ9', 'eyJpdiI6ImtZem9xQkJIZElpSHJ0enBRK3lxMXc9PSIsInZhbHVlIjoiV1hsVzI3NjBhenFGeW1SYmNaeCtRdz09IiwibWFjIjoiOGUxNGE1OGQ3MDNmNmJlODM2ZjlmNDM5NzhlYmI2NDdkMzNhYTM5MDdiNTIwNzlkNGFlODE4ZTllYmY0ODgyNCJ9', 'eyJpdiI6InlOcTkwSU9QZWdjSFdCdGRiRVAzXC9RPT0iLCJ2YWx1ZSI6IkZBV3B5Q3craGZqODVnSUNwYnJPUkE9PSIsIm1hYyI6IjA3OTU0M2Y4Y2UyYjgwZGNlM2VlNjZjNmNjNWFiMWVmZWY0MDMxODEwNTA5MWQ3MGVlZTU5MWQ3NmI2NjNkODkifQ==', 2, '2323', NULL, 67, '2323', '2323', 'CA', NULL, NULL, NULL, NULL, 90.00, 0.00, 0.00, 0.00, 90.00, NULL, '2018-09-13 20:35:20', '2018-09-13 21:04:42', NULL),
(111, 2, 14, 1, '58MSPC4UL5SZ2', NULL, NULL, 'First Customer', 'shantotrs@gmail.com', 'Buyer LTD', 'UPS Ground', 16, 1, 'US', 'with unit', '566', 'wer', 'AK', NULL, 1, '3434', 'United States', 1, '23423', 'US', 'Raj', '12', 'gsdfgfga', 'DC', NULL, 10, '5656', 'United States', 1, '4576767', 'eyJpdiI6Ik5jMngwMkpLM1ZlSUNXc1p5XC93SHNRPT0iLCJ2YWx1ZSI6IlVBZjFhdjROMGhwd3ZROUtHWWd6eXV3VEFoc2dEMEo5WXAxSEk5dkdcL2ZzPSIsIm1hYyI6IjM2N2IzMzYzOGM2NDNmZDhjOTM3MTNiYTY2MTRlYzU1MDk4YzE3ZTk4M2ZhZDk3MTlkNzhkYThiMjVlMWNiNWEifQ==', 'eyJpdiI6InZOekxHUTI4bEJ6emNKbW9rUytXTmc9PSIsInZhbHVlIjoiWjR0TzU1RHJnZmVPd1hqNFh3UmUyUT09IiwibWFjIjoiZjViNjA3MDRmY2Y1Y2JlZWU1ODVkOGVkNmNiYzMxMGNlZmU1ODhhYmE0ZWM5MmRiYjY1YzkxZmUyODU1ODMyYSJ9', 'eyJpdiI6IjlETTFcL2NoNVZIczhEakNpU0djaUJ3PT0iLCJ2YWx1ZSI6InFOT1I4VEU4S0JlVyt4Z1k5Qk1wXC93PT0iLCJtYWMiOiIxY2MxMGQxYTY0OTZhNTc1MDNlMTllMjA2MGQ1ZjZhOTllNzY1Yzg5ZDBkZjc0OGViMGI0NzM4YzU5YTg1Y2M2In0=', 'eyJpdiI6InlOcTkwSU9QZWdjSFdCdGRiRVAzXC9RPT0iLCJ2YWx1ZSI6IkZBV3B5Q3craGZqODVnSUNwYnJPUkE9PSIsIm1hYyI6IjA3OTU0M2Y4Y2UyYjgwZGNlM2VlNjZjNmNjNWFiMWVmZWY0MDMxODEwNTA5MWQ3MGVlZTU5MWQ3NmI2NjNkODkifQ==', 2, '2323', NULL, 67, '2323', '2323', 'CA', NULL, NULL, NULL, NULL, 87.00, 0.00, 0.00, 0.00, 87.00, NULL, '2018-09-13 23:18:45', '2018-09-13 23:19:00', NULL),
(112, 2, 14, 1, 'I5BMSZBKXHILC', NULL, NULL, 'First Customer', 'shantotrs@gmail.com', 'Buyer LTD', 'UPS Ground', 1, 1, 'US', 'Raj', NULL, 'gsdfgfga', 'DC', NULL, 10, '5656', 'United States', 1, '4576767', 'US', 'Raj', '12', 'gsdfgfga', 'DC', NULL, 10, '5656', 'United States', 1, '4576767', 'eyJpdiI6Ik5jMngwMkpLM1ZlSUNXc1p5XC93SHNRPT0iLCJ2YWx1ZSI6IlVBZjFhdjROMGhwd3ZROUtHWWd6eXV3VEFoc2dEMEo5WXAxSEk5dkdcL2ZzPSIsIm1hYyI6IjM2N2IzMzYzOGM2NDNmZDhjOTM3MTNiYTY2MTRlYzU1MDk4YzE3ZTk4M2ZhZDk3MTlkNzhkYThiMjVlMWNiNWEifQ==', 'eyJpdiI6InZOekxHUTI4bEJ6emNKbW9rUytXTmc9PSIsInZhbHVlIjoiWjR0TzU1RHJnZmVPd1hqNFh3UmUyUT09IiwibWFjIjoiZjViNjA3MDRmY2Y1Y2JlZWU1ODVkOGVkNmNiYzMxMGNlZmU1ODhhYmE0ZWM5MmRiYjY1YzkxZmUyODU1ODMyYSJ9', 'eyJpdiI6Ink0bDhrNlo2TE1BQXhnbTVENGkzQ0E9PSIsInZhbHVlIjoiXC9PblBWQ0NHNXJJQXVZNTBJM2hGWWc9PSIsIm1hYyI6ImFjNDcyNjkyODhiNDVhMjNmODAzOGNmYTM1OGI4NDY5NTBmZGNhNTYzMWM2MWE4Nzg2MDYwMmFiNjY0ODhjMTgifQ==', 'eyJpdiI6InlOcTkwSU9QZWdjSFdCdGRiRVAzXC9RPT0iLCJ2YWx1ZSI6IkZBV3B5Q3craGZqODVnSUNwYnJPUkE9PSIsIm1hYyI6IjA3OTU0M2Y4Y2UyYjgwZGNlM2VlNjZjNmNjNWFiMWVmZWY0MDMxODEwNTA5MWQ3MGVlZTU5MWQ3NmI2NjNkODkifQ==', 2, '2323', NULL, 67, '2323', '2323', 'CA', 4.00, 2, 'EID', 'wer', 360.00, 32.40, 0.00, 0.00, 327.60, NULL, '2018-09-17 20:24:15', '2018-09-17 20:43:31', NULL),
(113, 6, 14, 1, 'SJTSHGCRPL6QQ', NULL, NULL, 'First Customer', 'shantotrs@gmail.com', 'Buyer LTD', 'UPS Ground', NULL, 1, NULL, 'Raj', NULL, 'gsdfgfga', 'DC', NULL, NULL, '5656', 'United States', NULL, '4576767', NULL, 'Raj', NULL, 'gsdfgfga', 'DC', NULL, NULL, '5656', 'United States', NULL, '4576767', 'eyJpdiI6Ik5jMngwMkpLM1ZlSUNXc1p5XC93SHNRPT0iLCJ2YWx1ZSI6IlVBZjFhdjROMGhwd3ZROUtHWWd6eXV3VEFoc2dEMEo5WXAxSEk5dkdcL2ZzPSIsIm1hYyI6IjM2N2IzMzYzOGM2NDNmZDhjOTM3MTNiYTY2MTRlYzU1MDk4YzE3ZTk4M2ZhZDk3MTlkNzhkYThiMjVlMWNiNWEifQ==', 'eyJpdiI6InZOekxHUTI4bEJ6emNKbW9rUytXTmc9PSIsInZhbHVlIjoiWjR0TzU1RHJnZmVPd1hqNFh3UmUyUT09IiwibWFjIjoiZjViNjA3MDRmY2Y1Y2JlZWU1ODVkOGVkNmNiYzMxMGNlZmU1ODhhYmE0ZWM5MmRiYjY1YzkxZmUyODU1ODMyYSJ9', 'eyJpdiI6Ink0bDhrNlo2TE1BQXhnbTVENGkzQ0E9PSIsInZhbHVlIjoiXC9PblBWQ0NHNXJJQXVZNTBJM2hGWWc9PSIsIm1hYyI6ImFjNDcyNjkyODhiNDVhMjNmODAzOGNmYTM1OGI4NDY5NTBmZGNhNTYzMWM2MWE4Nzg2MDYwMmFiNjY0ODhjMTgifQ==', 'eyJpdiI6InlOcTkwSU9QZWdjSFdCdGRiRVAzXC9RPT0iLCJ2YWx1ZSI6IkZBV3B5Q3craGZqODVnSUNwYnJPUkE9PSIsIm1hYyI6IjA3OTU0M2Y4Y2UyYjgwZGNlM2VlNjZjNmNjNWFiMWVmZWY0MDMxODEwNTA5MWQ3MGVlZTU5MWQ3NmI2NjNkODkifQ==', NULL, NULL, NULL, NULL, NULL, NULL, NULL, 4.00, 2, 'EID', 'wer', 450.00, 0.00, 0.00, 0.00, 450.00, NULL, '2018-09-17 20:43:31', '2018-09-17 20:43:31', NULL);
INSERT INTO `orders` (`id`, `status`, `user_id`, `vendor_meta_id`, `order_number`, `invoice_number`, `tracking_number`, `name`, `email`, `company_name`, `shipping`, `shipping_address_id`, `shipping_method_id`, `shipping_location`, `shipping_address`, `shipping_unit`, `shipping_city`, `shipping_state`, `shipping_state_text`, `shipping_state_id`, `shipping_zip`, `shipping_country`, `shipping_country_id`, `shipping_phone`, `billing_location`, `billing_address`, `billing_unit`, `billing_city`, `billing_state`, `billing_state_text`, `billing_state_id`, `billing_zip`, `billing_country`, `billing_country_id`, `billing_phone`, `card_number`, `card_full_name`, `card_expire`, `card_cvc`, `card_country_id`, `card_zip`, `card_state`, `card_state_id`, `card_city`, `card_address`, `card_location`, `coupon_amount`, `coupon_type`, `coupon`, `coupon_description`, `subtotal`, `discount`, `store_credit`, `shipping_cost`, `total`, `note`, `created_at`, `updated_at`, `deleted_at`) VALUES
(114, 2, 14, 1, 'ZMYTRG7T8YVRO', NULL, NULL, 'First Customer', 'shantotrs@gmail.com', 'Buyer LTD', 'UPS Ground', 1, 1, 'US', 'Raj', NULL, 'gsdfgfga', 'DC', NULL, 10, '5656', 'United States', 1, '4576767', 'US', 'Raj', '12', 'gsdfgfga', 'DC', NULL, 10, '5656', 'United States', 1, '4576767', 'eyJpdiI6Ik5jMngwMkpLM1ZlSUNXc1p5XC93SHNRPT0iLCJ2YWx1ZSI6IlVBZjFhdjROMGhwd3ZROUtHWWd6eXV3VEFoc2dEMEo5WXAxSEk5dkdcL2ZzPSIsIm1hYyI6IjM2N2IzMzYzOGM2NDNmZDhjOTM3MTNiYTY2MTRlYzU1MDk4YzE3ZTk4M2ZhZDk3MTlkNzhkYThiMjVlMWNiNWEifQ==', 'eyJpdiI6InZOekxHUTI4bEJ6emNKbW9rUytXTmc9PSIsInZhbHVlIjoiWjR0TzU1RHJnZmVPd1hqNFh3UmUyUT09IiwibWFjIjoiZjViNjA3MDRmY2Y1Y2JlZWU1ODVkOGVkNmNiYzMxMGNlZmU1ODhhYmE0ZWM5MmRiYjY1YzkxZmUyODU1ODMyYSJ9', 'eyJpdiI6ImtYTHQyU0VqdjloWDY1TmVjaWpHcWc9PSIsInZhbHVlIjoiY2JBalJaQmxhRER0bFBCanRTUmVTdz09IiwibWFjIjoiZjMyYTYwNGFkMzEwNjlmOTZlNzYzZTFhNWU4ZTBhOTE1YmU4N2NiM2Y2Yzg5YjgzZGU4NzFlZmU1NDdhMzRmMyJ9', 'eyJpdiI6InlOcTkwSU9QZWdjSFdCdGRiRVAzXC9RPT0iLCJ2YWx1ZSI6IkZBV3B5Q3craGZqODVnSUNwYnJPUkE9PSIsIm1hYyI6IjA3OTU0M2Y4Y2UyYjgwZGNlM2VlNjZjNmNjNWFiMWVmZWY0MDMxODEwNTA5MWQ3MGVlZTU5MWQ3NmI2NjNkODkifQ==', 2, '2323', NULL, 67, '2323', '2323', 'CA', 4.00, 2, 'EID', 'wer', 90.00, 3.60, 0.00, 0.00, 79.20, NULL, '2018-09-17 20:47:58', '2018-09-17 20:49:30', NULL),
(115, 6, 14, 1, '867KLO99EOHSA', NULL, NULL, 'First Customer', 'shantotrs@gmail.com', 'Buyer LTD', 'UPS Ground', NULL, 1, NULL, 'Raj', NULL, 'gsdfgfga', 'DC', NULL, NULL, '5656', 'United States', NULL, '4576767', NULL, 'Raj', NULL, 'gsdfgfga', 'DC', NULL, NULL, '5656', 'United States', NULL, '4576767', 'eyJpdiI6Ik5jMngwMkpLM1ZlSUNXc1p5XC93SHNRPT0iLCJ2YWx1ZSI6IlVBZjFhdjROMGhwd3ZROUtHWWd6eXV3VEFoc2dEMEo5WXAxSEk5dkdcL2ZzPSIsIm1hYyI6IjM2N2IzMzYzOGM2NDNmZDhjOTM3MTNiYTY2MTRlYzU1MDk4YzE3ZTk4M2ZhZDk3MTlkNzhkYThiMjVlMWNiNWEifQ==', 'eyJpdiI6InZOekxHUTI4bEJ6emNKbW9rUytXTmc9PSIsInZhbHVlIjoiWjR0TzU1RHJnZmVPd1hqNFh3UmUyUT09IiwibWFjIjoiZjViNjA3MDRmY2Y1Y2JlZWU1ODVkOGVkNmNiYzMxMGNlZmU1ODhhYmE0ZWM5MmRiYjY1YzkxZmUyODU1ODMyYSJ9', 'eyJpdiI6ImtYTHQyU0VqdjloWDY1TmVjaWpHcWc9PSIsInZhbHVlIjoiY2JBalJaQmxhRER0bFBCanRTUmVTdz09IiwibWFjIjoiZjMyYTYwNGFkMzEwNjlmOTZlNzYzZTFhNWU4ZTBhOTE1YmU4N2NiM2Y2Yzg5YjgzZGU4NzFlZmU1NDdhMzRmMyJ9', 'eyJpdiI6InlOcTkwSU9QZWdjSFdCdGRiRVAzXC9RPT0iLCJ2YWx1ZSI6IkZBV3B5Q3craGZqODVnSUNwYnJPUkE9PSIsIm1hYyI6IjA3OTU0M2Y4Y2UyYjgwZGNlM2VlNjZjNmNjNWFiMWVmZWY0MDMxODEwNTA5MWQ3MGVlZTU5MWQ3NmI2NjNkODkifQ==', NULL, NULL, NULL, NULL, NULL, NULL, NULL, 4.00, 2, 'EID', 'wer', 90.00, 3.60, 0.00, 0.00, 86.40, NULL, '2018-09-17 20:49:30', '2018-09-17 20:49:30', NULL),
(116, 2, 14, 1, '1GXUIV9L9G4NS', NULL, NULL, 'First Customer', 'shantotrs@gmail.com', 'Buyer LTD', 'UPS Ground', 1, 1, 'US', 'Raj', NULL, 'gsdfgfga', 'DC', NULL, 10, '5656', 'United States', 1, '4576767', 'US', 'Raj', '12', 'gsdfgfga', 'DC', NULL, 10, '5656', 'United States', 1, '4576767', 'eyJpdiI6Ik5jMngwMkpLM1ZlSUNXc1p5XC93SHNRPT0iLCJ2YWx1ZSI6IlVBZjFhdjROMGhwd3ZROUtHWWd6eXV3VEFoc2dEMEo5WXAxSEk5dkdcL2ZzPSIsIm1hYyI6IjM2N2IzMzYzOGM2NDNmZDhjOTM3MTNiYTY2MTRlYzU1MDk4YzE3ZTk4M2ZhZDk3MTlkNzhkYThiMjVlMWNiNWEifQ==', 'eyJpdiI6InZOekxHUTI4bEJ6emNKbW9rUytXTmc9PSIsInZhbHVlIjoiWjR0TzU1RHJnZmVPd1hqNFh3UmUyUT09IiwibWFjIjoiZjViNjA3MDRmY2Y1Y2JlZWU1ODVkOGVkNmNiYzMxMGNlZmU1ODhhYmE0ZWM5MmRiYjY1YzkxZmUyODU1ODMyYSJ9', 'eyJpdiI6IkdMd3BTa0xYUDIwcHBDcldpOVo0UVE9PSIsInZhbHVlIjoiOFwvalNDSzByaWVxOHhmWHM3YjV1c1E9PSIsIm1hYyI6IjUyYzUzZjczMjdlNmQ4ZDUzYzBlMzliMTVlMjU4NDg2ZmI2MzMzODQ2NGZmZWE3YzQ2NDZkODkwMWRjMTE0ZGEifQ==', 'eyJpdiI6InlOcTkwSU9QZWdjSFdCdGRiRVAzXC9RPT0iLCJ2YWx1ZSI6IkZBV3B5Q3craGZqODVnSUNwYnJPUkE9PSIsIm1hYyI6IjA3OTU0M2Y4Y2UyYjgwZGNlM2VlNjZjNmNjNWFiMWVmZWY0MDMxODEwNTA5MWQ3MGVlZTU5MWQ3NmI2NjNkODkifQ==', 2, '2323', NULL, 67, '2323', '2323', 'CA', 50.00, 1, 'KURBANI2', '234234', 450.00, 25.00, 0.00, 0.00, 375.00, NULL, '2018-09-17 21:21:12', '2018-09-17 21:21:55', NULL),
(117, 6, 14, 1, '7IE19NNNC0GKM', NULL, NULL, 'First Customer', 'shantotrs@gmail.com', 'Buyer LTD', 'UPS Ground', NULL, 1, NULL, 'Raj', NULL, 'gsdfgfga', 'DC', NULL, NULL, '5656', 'United States', NULL, '4576767', NULL, 'Raj', NULL, 'gsdfgfga', 'DC', NULL, NULL, '5656', 'United States', NULL, '4576767', 'eyJpdiI6Ik5jMngwMkpLM1ZlSUNXc1p5XC93SHNRPT0iLCJ2YWx1ZSI6IlVBZjFhdjROMGhwd3ZROUtHWWd6eXV3VEFoc2dEMEo5WXAxSEk5dkdcL2ZzPSIsIm1hYyI6IjM2N2IzMzYzOGM2NDNmZDhjOTM3MTNiYTY2MTRlYzU1MDk4YzE3ZTk4M2ZhZDk3MTlkNzhkYThiMjVlMWNiNWEifQ==', 'eyJpdiI6InZOekxHUTI4bEJ6emNKbW9rUytXTmc9PSIsInZhbHVlIjoiWjR0TzU1RHJnZmVPd1hqNFh3UmUyUT09IiwibWFjIjoiZjViNjA3MDRmY2Y1Y2JlZWU1ODVkOGVkNmNiYzMxMGNlZmU1ODhhYmE0ZWM5MmRiYjY1YzkxZmUyODU1ODMyYSJ9', 'eyJpdiI6IkdMd3BTa0xYUDIwcHBDcldpOVo0UVE9PSIsInZhbHVlIjoiOFwvalNDSzByaWVxOHhmWHM3YjV1c1E9PSIsIm1hYyI6IjUyYzUzZjczMjdlNmQ4ZDUzYzBlMzliMTVlMjU4NDg2ZmI2MzMzODQ2NGZmZWE3YzQ2NDZkODkwMWRjMTE0ZGEifQ==', 'eyJpdiI6InlOcTkwSU9QZWdjSFdCdGRiRVAzXC9RPT0iLCJ2YWx1ZSI6IkZBV3B5Q3craGZqODVnSUNwYnJPUkE9PSIsIm1hYyI6IjA3OTU0M2Y4Y2UyYjgwZGNlM2VlNjZjNmNjNWFiMWVmZWY0MDMxODEwNTA5MWQ3MGVlZTU5MWQ3NmI2NjNkODkifQ==', NULL, NULL, NULL, NULL, NULL, NULL, NULL, 50.00, 1, 'KURBANI2', '234234', 270.00, 25.00, 0.00, 0.00, 245.00, NULL, '2018-09-17 21:21:55', '2018-09-17 21:21:55', NULL),
(118, 1, 14, 1, '0H8GJ3TYW7KEP', NULL, NULL, 'First Customer', 'shantotrs@gmail.com', 'Buyer LTD', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'US', 'Raj', '12', 'gsdfgfga', 'DC', NULL, 10, '5656', 'United States', 1, '4576767', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 279.00, 0.00, 0.00, 0.00, 279.00, NULL, '2018-10-05 20:38:43', '2018-10-05 20:38:43', NULL),
(119, 2, 14, 1, '2E5HYBARQ1UJ6', NULL, NULL, 'First Customer', 'shantotrs@gmail.com', 'Buyer LTD', 'UPS Ground', 1, 1, 'US', 'Raj', NULL, 'gsdfgfga', 'DC', NULL, 10, '5656', 'United States', 1, '4576767', 'US', 'Raj', '12', 'gsdfgfga', 'DC', NULL, 10, '5656', 'United States', 1, '4576767', 'eyJpdiI6Ik5jMngwMkpLM1ZlSUNXc1p5XC93SHNRPT0iLCJ2YWx1ZSI6IlVBZjFhdjROMGhwd3ZROUtHWWd6eXV3VEFoc2dEMEo5WXAxSEk5dkdcL2ZzPSIsIm1hYyI6IjM2N2IzMzYzOGM2NDNmZDhjOTM3MTNiYTY2MTRlYzU1MDk4YzE3ZTk4M2ZhZDk3MTlkNzhkYThiMjVlMWNiNWEifQ==', 'eyJpdiI6InZOekxHUTI4bEJ6emNKbW9rUytXTmc9PSIsInZhbHVlIjoiWjR0TzU1RHJnZmVPd1hqNFh3UmUyUT09IiwibWFjIjoiZjViNjA3MDRmY2Y1Y2JlZWU1ODVkOGVkNmNiYzMxMGNlZmU1ODhhYmE0ZWM5MmRiYjY1YzkxZmUyODU1ODMyYSJ9', 'eyJpdiI6IlQzeTJUUDdheVVxSFA4TTF6VW1meUE9PSIsInZhbHVlIjoiVmVuNG56dWVFZUllN1VDQTU1SG5mUT09IiwibWFjIjoiOTg2MWIyMjAyOWM2MWZjZTY2MmE4Y2VmYThmMjYyOWU1YWFjMTI5OTEwZmUwZmQ0ZDZhZWEwZGQ3ZWQzMGM5ZSJ9', 'eyJpdiI6InlOcTkwSU9QZWdjSFdCdGRiRVAzXC9RPT0iLCJ2YWx1ZSI6IkZBV3B5Q3craGZqODVnSUNwYnJPUkE9PSIsIm1hYyI6IjA3OTU0M2Y4Y2UyYjgwZGNlM2VlNjZjNmNjNWFiMWVmZWY0MDMxODEwNTA5MWQ3MGVlZTU5MWQ3NmI2NjNkODkifQ==', 2, '2323', NULL, 67, '2323', '2323', 'CA', NULL, NULL, NULL, NULL, 87.00, 0.00, 0.00, 0.00, 87.00, NULL, '2018-10-29 02:11:03', '2018-10-29 02:11:06', NULL),
(120, 2, 14, 1, 'L6XVR582G00AC', 'rt', NULL, 'First Customer', 'shantotrs@gmail.com', 'Buyer LTD', 'UPS Ground', 1, 1, 'US', 'Raj', NULL, 'gsdfgfga', 'DC', NULL, 10, '5656', 'United States', 1, '4576767', 'US', 'Raj', '12', 'gsdfgfga', 'DC', NULL, 10, '5656', 'United States', 1, '4576767', 'eyJpdiI6InBqWHU3RW92RjJYcElwQ0pzSitcL1VBPT0iLCJ2YWx1ZSI6IlR4ejRrMGY5S2lIeTVqZkdEXC9jdWxzeUF2SzNIZU5udW9Gd0pkWHBvbFVJPSIsIm1hYyI6IjExM2QyMjI4OGQ4ZTBjZjQ1ZGFmYmU3YWYwMWIwNzI1YWIzYzU4ZGJmMzlkODkzZThiZmNlMjIwY2YzM2MxNjQifQ==', 'eyJpdiI6InZOekxHUTI4bEJ6emNKbW9rUytXTmc9PSIsInZhbHVlIjoiWjR0TzU1RHJnZmVPd1hqNFh3UmUyUT09IiwibWFjIjoiZjViNjA3MDRmY2Y1Y2JlZWU1ODVkOGVkNmNiYzMxMGNlZmU1ODhhYmE0ZWM5MmRiYjY1YzkxZmUyODU1ODMyYSJ9', 'eyJpdiI6Ikorbk5VWWtFeEF5NWc0OFRCRitzRFE9PSIsInZhbHVlIjoiaWFSeVlSUDR5SmZyc3dxQ1JBYnFNUT09IiwibWFjIjoiMDVmYjU1ODNhZjMxYzJjYjVmYmFiNWRmYzBlMjlkNjE5OWQxOWIyMmZkNTA4ZDE3MjgzZjRjOWIxNmJkYTczNyJ9', 'eyJpdiI6InlOcTkwSU9QZWdjSFdCdGRiRVAzXC9RPT0iLCJ2YWx1ZSI6IkZBV3B5Q3craGZqODVnSUNwYnJPUkE9PSIsIm1hYyI6IjA3OTU0M2Y4Y2UyYjgwZGNlM2VlNjZjNmNjNWFiMWVmZWY0MDMxODEwNTA5MWQ3MGVlZTU5MWQ3NmI2NjNkODkifQ==', 2, '2323', NULL, 67, '2323', '2323', 'CA', NULL, NULL, NULL, NULL, 837.00, 0.00, 0.00, 0.00, 837.00, NULL, '2018-10-29 02:13:10', '2018-11-20 19:51:35', NULL),
(121, 2, 14, 1, '9HILFAJU0OEHG', NULL, NULL, 'First Customer', 'shantotrs@gmail.com', 'Buyer LTD', 'jhk2', 11, 4, 'INT', 'sdfdfdf', '45', 'rtrtr', 'erer', 'erer', NULL, 'ter', 'Bangladesh', 20, 'erer', 'US', 'Raj', '12', 'gsdfgfga', 'DC', NULL, 10, '5656', 'United States', 1, '4576767', 'eyJpdiI6Ijl1REptTE5DazVycEJEdW96RGpQMkE9PSIsInZhbHVlIjoiYVFcL3pldldOQjRGeHhXMHdjTnJBcnIyOGZRU04zS1NaTjV0MWNsQnBwQ0E9IiwibWFjIjoiYjI5MGI3MDA2NWE1MWEwOTEwNTMzODQ5MmNiNDJhZWE3YTE5NWFiYzBiZDkzOTE0YjI1Mjc1ZWFkMzBmYjk3NyJ9', 'eyJpdiI6IkhEMGhIa0l0YzlTZzVKQ2FzNGRvYUE9PSIsInZhbHVlIjoibHZMY2NvUk1rdFlwMlRTMVVJdDdEQT09IiwibWFjIjoiZDI4YzU1YTZjOGJjN2JlMDdmZWFlNWY0YWIwMTEwZmEyOWFiNTQ1MTQ2M2Q0NmM4ODA3OTM1ZTQxYjBjOTM5OSJ9', 'eyJpdiI6IjJqNWFleHdUYXFWY3pVYmkzditpdkE9PSIsInZhbHVlIjoiYVMyNUJvQjV1SmprNzJFbFBTb21Mdz09IiwibWFjIjoiZWE1NTkxZTg1ZWI0N2JkZDdiMjliODBmNDQ2Zjg5MTk0N2RlOWIwMzliMmM3YjA2MWI4MWQ5ZjdlZTI1OGUwYSJ9', 'eyJpdiI6Ilp0R1VSMEV1OElFSlFFcHVIYVJFXC9BPT0iLCJ2YWx1ZSI6InV5WnN0VEVrUHhYZGZOcWFaQlJFeXc9PSIsIm1hYyI6ImU3MTlhZmVhYmU0NTAwYzgwMTUyZDE4NzVkNzUwZjUwMzM2ODZkNmFmOTFiYjU5YTE1ZWVlMWZhN2IwNjYwNmEifQ==', 1, '4334', NULL, 9, 'asdf', 'adfa', 'US', NULL, NULL, NULL, NULL, 90.00, 0.00, 0.00, 0.00, 90.00, NULL, '2018-11-20 20:08:26', '2018-11-20 20:09:53', NULL),
(122, 1, 14, 1, 'SBW1TEAHOZZB6', NULL, NULL, 'First Customer', 'shantotrs@gmail.com', 'Buyer LTD', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'US', 'Raj', '12', 'gsdfgfga', 'DC', NULL, 10, '5656', 'United States', 1, '4576767', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 4.00, 2, 'EID', 'wer', 558.00, 22.32, 0.00, 0.00, 535.68, NULL, '2018-11-23 19:31:00', '2018-11-23 19:31:53', NULL);

-- --------------------------------------------------------

--
-- Table structure for table `order_items`
--

DROP TABLE IF EXISTS `order_items`;
CREATE TABLE IF NOT EXISTS `order_items` (
  `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT,
  `order_id` int(11) NOT NULL,
  `item_id` int(11) NOT NULL,
  `style_no` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `color` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `size` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `item_per_pack` int(11) NOT NULL,
  `pack` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `qty` int(11) NOT NULL,
  `total_qty` int(11) NOT NULL,
  `per_unit_price` double(8,2) NOT NULL,
  `amount` double(8,2) NOT NULL,
  `back_order` tinyint(1) NOT NULL DEFAULT '0',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=195 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `order_items`
--

INSERT INTO `order_items` (`id`, `order_id`, `item_id`, `style_no`, `color`, `size`, `item_per_pack`, `pack`, `qty`, `total_qty`, `per_unit_price`, `amount`, `back_order`, `created_at`, `updated_at`) VALUES
(1, 1, 15, '9403 WHT', 'BLACK', 'S-M-L', 6, '2-2-2', 1, 6, 15.00, 90.00, 0, '2018-06-13 03:40:11', '2018-06-13 03:40:11'),
(2, 1, 14, '9389 RD  .', 'RED', 'S-M-L', 6, '2-2-2', 2, 12, 15.50, 186.00, 0, '2018-06-13 03:40:11', '2018-06-13 03:40:11'),
(3, 1, 14, '9389 RD  .', 'GREEN', 'S-M-L', 6, '2-2-2', 1, 6, 15.50, 93.00, 0, '2018-06-13 03:40:11', '2018-06-13 03:40:11'),
(4, 3, 17, '7437 COR .', 'CORAL', 'S-M-L', 6, '2-2-2', 1, 6, 14.50, 87.00, 0, '2018-06-13 03:51:36', '2018-06-13 04:18:07'),
(5, 4, 14, '9389 RD  .', 'RED', 'S-M-L', 5, '2-1-2', 6, 30, 15.50, 465.00, 0, '2018-06-13 04:59:12', '2018-06-26 09:41:30'),
(6, 5, 18, '2314. WH  .', 'JADE', '2/2/2', 2, '2', 4, 8, 14.50, 116.00, 0, '2018-06-13 04:59:12', '2018-06-13 04:59:12'),
(7, 6, 14, '9389 RD  .', 'GREEN', 'S-M-L', 6, '2-2-2', 1, 6, 15.50, 93.00, 0, '2018-06-13 05:27:07', '2018-06-13 05:27:07'),
(8, 7, 14, '9389 RD  .', 'GREEN', 'S-M-L', 6, '2-2-2', 1, 6, 15.50, 93.00, 0, '2018-06-22 05:26:31', '2018-06-22 05:26:31'),
(9, 8, 14, '9389 RD  .', 'GREEN', 'S-M-L', 6, '2-2-2', 1, 6, 15.50, 93.00, 0, '2018-06-22 11:06:33', '2018-06-22 11:06:33'),
(10, 9, 14, '9389 RD  .', 'GREEN', 'S-M-L', 6, '2-2-2', 1, 6, 15.50, 93.00, 0, '2018-06-23 05:34:55', '2018-06-23 05:34:55'),
(11, 10, 14, '9389 RD  .', 'GREEN', 'S-M-L', 6, '2-2-2', 1, 6, 15.50, 93.00, 0, '2018-06-23 09:24:30', '2018-06-23 09:24:30'),
(12, 11, 14, '9389 RD  .', 'GREEN', 'S-M-L', 6, '2-2-2', 1, 6, 15.50, 93.00, 0, '2018-06-23 11:35:49', '2018-06-23 11:35:49'),
(13, 12, 14, '9389 RD  .', 'GREEN', 'S-M-L', 6, '2-2-2', 1, 6, 15.50, 93.00, 0, '2018-06-23 11:39:54', '2018-06-23 11:39:54'),
(14, 13, 14, '9389 RD  .', 'GREEN', 'S-M-L', 6, '2-2-2', 1, 6, 15.50, 93.00, 0, '2018-06-23 11:40:26', '2018-06-23 11:40:26'),
(15, 19, 14, '9389 RD  .', 'GREEN', 'S-M-L', 6, '2-2-2', 1, 6, 15.50, 93.00, 0, '2018-06-23 11:40:55', '2018-06-27 09:09:11'),
(16, 15, 19, '9376 PINK', 'PINK', '2/2/2', 2, '2', 4, 8, 14.50, 116.00, 0, '2018-06-23 11:40:55', '2018-06-23 11:40:55'),
(17, 16, 14, '9389 RD  .', 'GREEN', 'S-M-L', 6, '2-2-2', 1, 6, 15.50, 93.00, 0, '2018-06-23 12:45:43', '2018-06-23 12:45:43'),
(18, 17, 14, '9389 RD  .', 'GREEN', 'S-M-L', 6, '2-2-2', 1, 6, 15.50, 93.00, 0, '2018-06-23 12:46:38', '2018-06-23 12:46:38'),
(19, 18, 14, '9389 RD  .', 'GREEN', 'S-M-L', 6, '2-2-2', 1, 6, 15.50, 93.00, 0, '2018-06-23 13:14:18', '2018-06-23 13:14:18'),
(20, 20, 14, '9389 RD  .', 'GREEN', 'S-M-L', 6, '2-2-2', 1, 6, 15.50, 93.00, 0, '2018-06-25 11:35:37', '2018-07-15 23:47:36'),
(21, 21, 14, '9389 RD  .', 'GREEN', 'S-M-L', 6, '2-2-2', 1, 6, 15.50, 93.00, 0, '2018-06-26 02:54:39', '2018-06-26 02:54:39'),
(22, 22, 15, '9403 WHT', 'BLACK', 'S-M-L', 6, '2-2-2', 1, 6, 15.00, 90.00, 0, '2018-06-26 02:56:36', '2018-06-26 02:56:36'),
(23, 23, 16, '7378 WHT', 'BLUE', '3-3', 6, '3-3', 1, 6, 12.50, 75.00, 0, '2018-06-26 03:25:07', '2018-06-27 08:59:14'),
(24, 4, 16, '7378 WHT', 'BLUE', '3-3', 6, '3-3', 2, 12, 12.50, 150.00, 0, '2018-06-26 09:32:21', '2018-06-26 09:36:43'),
(25, 4, 16, '7378 WHT', 'PINK', '3-3', 5, '3-2', 1, 5, 12.50, 62.50, 0, '2018-06-26 09:32:21', '2018-06-26 09:36:43'),
(27, 23, 14, '9389 RD  .', 'RED', 'S-M-L', 6, '2-2-2', 5, 30, 15.50, 465.00, 0, '2018-06-27 08:19:26', '2018-06-27 08:59:14'),
(28, 22, 14, '9389 RD  .', 'RED', 'S-M-L', 6, '2-2-2', 2, 12, 15.50, 186.00, 0, '2018-06-27 08:19:35', '2018-06-27 08:19:35'),
(29, 23, 14, '9389 RD  .', 'WHITE', 'S-M-L', 6, '2-2-2', 1, 6, 15.50, 93.00, 0, '2018-06-27 08:21:12', '2018-06-27 08:59:14'),
(30, 23, 16, '7378 WHT', 'WHITE', '3-3', 6, '3-3', 1, 6, 12.50, 75.00, 0, '2018-06-27 08:21:18', '2018-06-27 08:59:14'),
(31, 22, 15, '9403 WHT', 'WHITE', 'S-M-L', 6, '2-2-2', 1, 6, 15.00, 90.00, 0, '2018-06-28 02:23:00', '2018-06-28 02:23:00'),
(32, 24, 16, '7378 WHT', 'WHITE', '3-3', 6, '3-3', 1, 6, 12.50, 75.00, 0, '2018-06-28 03:19:25', '2018-06-28 03:19:25'),
(33, 24, 14, '9389 RD  .', 'RED', 'S-M-L', 6, '2-2-2', 1, 6, 15.50, 93.00, 0, '2018-06-28 03:19:25', '2018-06-28 03:19:25'),
(34, 24, 16, '7378 WHT', 'PINK', '3-3', 6, '3-3', 2, 12, 12.50, 150.00, 0, '2018-06-28 03:19:25', '2018-06-28 03:19:25'),
(35, 25, 16, '7378 WHT', 'WHITE', '3-3', 6, '3-3', 1, 6, 12.50, 75.00, 0, '2018-06-28 03:23:54', '2018-06-28 03:23:54'),
(36, 25, 14, '9389 RD  .', 'RED', 'S-M-L', 6, '2-2-2', 1, 6, 15.50, 93.00, 0, '2018-06-28 03:23:54', '2018-06-28 03:23:54'),
(37, 25, 16, '7378 WHT', 'PINK', '3-3', 6, '3-3', 2, 12, 12.50, 150.00, 0, '2018-06-28 03:23:54', '2018-06-28 03:23:54'),
(38, 26, 18, '2314. WH  .', 'WHITE', '2/2/2', 2, '2', 1, 2, 14.50, 29.00, 0, '2018-07-02 06:02:01', '2018-07-02 06:02:01'),
(39, 27, 19, '9376 PINK', 'MINT', '2/2/2', 2, '2', 1, 2, 14.50, 29.00, 0, '2018-07-04 08:28:38', '2018-07-04 08:28:38'),
(40, 28, 14, '9389 RD  .', 'GREEN', 'S-M-L', 6, '2-2-2', 1, 6, 15.50, 93.00, 0, '2018-07-04 08:29:12', '2018-07-04 08:29:12'),
(41, 29, 19, '9376 PINK', 'MINT', '2/2/2', 2, '2', 1, 2, 14.50, 29.00, 0, '2018-07-04 08:29:12', '2018-07-04 08:29:12'),
(42, 30, 14, '9389 RD  .', 'GREEN', 'S-M-L', 6, '2-2-2', 1, 6, 15.50, 93.00, 0, '2018-07-04 08:31:10', '2018-07-04 08:31:10'),
(43, 31, 14, '9389 RD  .', 'GREEN', 'S-M-L', 6, '2-2-2', 1, 6, 15.50, 93.00, 0, '2018-07-04 09:47:23', '2018-07-04 09:47:23'),
(44, 32, 14, '9389 RD  .', 'GREEN', 'S-M-L', 6, '2-2-2', 1, 6, 15.50, 93.00, 0, '2018-07-04 09:50:49', '2018-07-04 09:50:49'),
(45, 33, 19, '9376 PINK', 'MINT', '2/2/2', 2, '2', 1, 2, 14.50, 29.00, 0, '2018-07-04 09:50:49', '2018-07-04 09:50:49'),
(46, 34, 14, '9389 RD  .', 'RED', 'S-M-L', 6, '2-2-2', 1, 6, 15.50, 93.00, 0, '2018-07-04 10:43:14', '2018-07-04 10:43:14'),
(47, 35, 14, '9389 RD  .', 'RED', 'S-M-L', 6, '2-2-2', 1, 6, 15.50, 93.00, 0, '2018-07-04 10:48:46', '2018-07-04 10:48:46'),
(48, 36, 14, '9389 RD  .', 'RED', 'S-M-L', 6, '2-2-2', 1, 6, 15.50, 93.00, 0, '2018-07-04 12:54:41', '2018-07-04 12:54:41'),
(49, 37, 14, '9389 RD  .', 'RED', 'S-M-L', 6, '2-2-2', 1, 6, 15.50, 93.00, 0, '2018-07-05 03:48:05', '2018-07-05 03:48:05'),
(50, 38, 17, '7437 COR .', 'CORAL', 'S-M-L', 6, '2-2-2', 1, 6, 14.50, 87.00, 0, '2018-07-06 09:56:27', '2018-07-09 04:56:41'),
(51, 38, 16, '7378 WHT', 'PINK', '3-3', 6, '3-3', 7, 42, 12.50, 525.00, 0, '2018-07-07 11:39:30', '2018-07-09 04:56:41'),
(52, 38, 14, '9389 RD  .', 'RED', 'S-M-L', 6, '2-2-2', 2, 12, 15.50, 186.00, 0, '2018-07-07 11:40:01', '2018-07-09 04:59:33'),
(53, 39, 17, '7437 COR .', 'CORAL', 'S-M-L', 6, '2-2-2', 1, 6, 14.50, 87.00, 0, '2018-07-12 19:50:52', '2018-07-12 19:50:52'),
(54, 40, 14, '9389 RD  .', 'GREEN', 'S-M-L', 6, '2-2-2', 1, 6, 15.50, 93.00, 0, '2018-07-12 21:56:10', '2018-07-12 21:56:10'),
(55, 41, 19, '9376 PINK', 'PINK', '2/2/2', 2, '2', 1, 2, 14.50, 29.00, 0, '2018-07-12 21:56:10', '2018-07-12 21:56:10'),
(56, 42, 14, '9389 RD  .', 'GREEN', 'S-M-L', 6, '2-2-2', 1, 6, 15.50, 93.00, 0, '2018-07-12 22:22:17', '2018-07-12 22:22:17'),
(57, 43, 19, '9376 PINK', 'PINK', '2/2/2', 2, '2', 1, 2, 14.50, 29.00, 0, '2018-07-12 22:22:17', '2018-07-12 22:22:17'),
(58, 44, 14, '9389 RD  .', 'GREEN', 'S-M-L', 6, '2-2-2', 1, 6, 15.50, 93.00, 0, '2018-07-12 22:22:22', '2018-07-12 22:22:22'),
(59, 45, 19, '9376 PINK', 'PINK', '2/2/2', 2, '2', 1, 2, 14.50, 29.00, 0, '2018-07-12 22:22:22', '2018-07-12 22:22:22'),
(60, 46, 14, '9389 RD  .', 'GREEN', 'S-M-L', 6, '2-2-2', 1, 6, 15.50, 93.00, 0, '2018-07-12 22:22:31', '2018-07-12 22:22:31'),
(61, 47, 14, '9389 RD  .', 'GREEN', 'S-M-L', 6, '2-2-2', 1, 6, 15.50, 93.00, 0, '2018-07-12 22:26:05', '2018-07-12 22:26:05'),
(62, 48, 19, '9376 PINK', 'PINK', '2/2/2', 2, '2', 1, 2, 14.50, 29.00, 0, '2018-07-12 22:26:05', '2018-07-12 22:26:05'),
(63, 49, 14, '9389 RD  .', 'GREEN', 'S-M-L', 6, '2-2-2', 1, 6, 15.50, 93.00, 0, '2018-07-12 22:26:20', '2018-07-12 22:26:20'),
(64, 50, 14, '9389 RD  .', 'GREEN', 'S-M-L', 6, '2-2-2', 1, 6, 15.50, 93.00, 0, '2018-07-12 22:29:56', '2018-07-12 22:29:56'),
(65, 51, 14, '9389 RD  .', 'GREEN', 'S-M-L', 6, '2-2-2', 1, 6, 15.50, 93.00, 0, '2018-07-12 22:31:16', '2018-07-12 22:31:16'),
(66, 52, 14, '9389 RD  .', 'GREEN', 'S-M-L', 6, '2-2-2', 1, 6, 15.50, 93.00, 0, '2018-07-12 22:31:58', '2018-07-12 23:11:26'),
(67, 53, 78, 'adf', 'BLACK', '3-3', 6, '3-3', 1, 6, 27.20, 163.20, 0, '2018-07-13 20:26:37', '2018-07-13 20:28:24'),
(68, 54, 67, 'LT3751', 'BLACK', 'S-M-L', 6, '2-2-2', 1, 6, 12.75, 76.50, 0, '2018-07-13 22:24:17', '2018-07-13 22:24:17'),
(69, 55, 67, 'LT3751', 'BLACK', 'S-M-L', 6, '2-2-2', 1, 6, 12.75, 76.50, 0, '2018-07-13 22:24:57', '2018-07-13 22:24:57'),
(70, 56, 67, 'LT3751', 'BLACK', 'S-M-L', 6, '2-2-2', 2, 12, 12.75, 153.00, 0, '2018-07-13 22:33:05', '2018-07-13 22:33:05'),
(71, 57, 19, '9376 PINK', 'PINK', '2/2/2', 2, '2', 1, 2, 14.50, 29.00, 0, '2018-07-13 23:52:50', '2018-07-13 23:52:50'),
(72, 58, 19, '9376 PINK', 'PINK', '2/2/2', 2, '2', 1, 2, 14.50, 29.00, 0, '2018-07-13 23:53:02', '2018-07-13 23:53:02'),
(73, 59, 19, '9376 PINK', 'PINK', '2/2/2', 2, '2', 1, 2, 14.50, 29.00, 0, '2018-07-13 23:53:08', '2018-07-13 23:53:08'),
(74, 60, 67, 'LT3751', 'BLACK', 'S-M-L', 6, '2-2-2', 1, 6, 12.75, 76.50, 0, '2018-07-16 00:20:04', '2018-07-16 00:20:04'),
(75, 60, 14, '9389 RD  .', 'GREEN', 'S-M-L', 6, '2-2-2', 1, 6, 15.50, 93.00, 0, '2018-07-16 00:20:04', '2018-07-16 00:20:04'),
(76, 61, 14, '9389 RD  .', 'GREEN', 'S-M-L', 6, '2-2-2', 1, 6, 15.50, 93.00, 0, '2018-07-18 02:06:57', '2018-07-25 22:34:16'),
(77, 62, 19, '9376 PINK', 'MINT', '2/2/2', 2, '2', 1, 2, 14.50, 29.00, 0, '2018-07-18 05:30:09', '2018-07-18 05:30:09'),
(78, 63, 14, '9389 RD  .', 'GREEN', 'S-M-L', 6, '2-2-2', 1, 6, 15.50, 93.00, 0, '2018-07-18 05:33:57', '2018-07-18 05:33:57'),
(79, 64, 19, '9376 PINK', 'MINT', '2/2/2', 2, '2', 1, 2, 14.50, 29.00, 0, '2018-07-18 05:33:57', '2018-07-18 05:33:57'),
(80, 65, 14, '9389 RD  .', 'GREEN', 'S-M-L', 6, '2-2-2', 1, 6, 15.50, 93.00, 0, '2018-07-18 05:34:06', '2018-07-18 05:34:06'),
(81, 66, 19, '9376 PINK', 'MINT', '2/2/2', 2, '2', 1, 2, 14.50, 29.00, 0, '2018-07-18 05:34:06', '2018-07-18 05:34:06'),
(82, 67, 14, '9389 RD  .', 'GREEN', 'S-M-L', 6, '2-2-2', 1, 6, 15.50, 93.00, 0, '2018-07-18 05:34:15', '2018-07-18 05:34:15'),
(83, 68, 14, '9389 RD  .', 'GREEN', 'S-M-L', 6, '2-2-2', 1, 6, 15.50, 93.00, 0, '2018-07-18 05:34:19', '2018-07-18 05:34:19'),
(84, 69, 19, '9376 PINK', 'MINT', '2/2/2', 2, '2', 1, 2, 14.50, 29.00, 0, '2018-07-18 05:34:19', '2018-07-18 05:34:19'),
(85, 70, 14, '9389 RD  .', 'GREEN', 'S-M-L', 6, '2-2-2', 1, 6, 15.50, 93.00, 0, '2018-07-27 20:41:29', '2018-07-27 20:41:29'),
(86, 71, 14, '9389 RD  .', 'RED', 'S-M-L', 6, '2-2-2', 2, 12, 15.50, 186.00, 0, '2018-07-28 00:23:43', '2018-07-28 00:23:43'),
(87, 71, 14, '9389 RD  .', 'GREEN', 'S-M-L', 6, '2-2-2', 1, 6, 15.50, 93.00, 0, '2018-07-28 00:23:43', '2018-07-28 00:23:43'),
(88, 71, 15, '9403 WHT', 'WHITE', 'S-M-L', 6, '2-2-2', 2, 12, 15.00, 180.00, 0, '2018-07-28 00:23:43', '2018-07-28 00:23:43'),
(89, 71, 15, '9403 WHT', 'BLACK', 'S-M-L', 6, '2-2-2', 1, 6, 15.00, 90.00, 0, '2018-07-28 00:23:43', '2018-07-28 00:23:43'),
(90, 72, 19, '9376 PINK', 'MINT', '2/2/2', 2, '2', 1, 2, 14.50, 29.00, 0, '2018-07-28 00:23:43', '2018-07-28 00:23:43'),
(91, 73, 14, '9389 RD  .', 'RED', 'S-M-L', 6, '2-2-2', 2, 12, 15.50, 186.00, 0, '2018-07-28 01:02:32', '2018-07-28 01:02:32'),
(92, 73, 14, '9389 RD  .', 'GREEN', 'S-M-L', 6, '2-2-2', 1, 6, 15.50, 93.00, 0, '2018-07-28 01:02:32', '2018-07-28 01:02:32'),
(93, 73, 15, '9403 WHT', 'WHITE', 'S-M-L', 6, '2-2-2', 2, 12, 15.00, 180.00, 0, '2018-07-28 01:02:33', '2018-07-28 01:02:33'),
(94, 73, 15, '9403 WHT', 'BLACK', 'S-M-L', 6, '2-2-2', 1, 6, 15.00, 90.00, 0, '2018-07-28 01:02:33', '2018-07-28 01:02:33'),
(95, 74, 19, '9376 PINK', 'MINT', '2/2/2', 2, '2', 1, 2, 14.50, 29.00, 0, '2018-07-28 01:02:33', '2018-07-28 01:02:33'),
(96, 75, 14, '9389 RD  .', 'RED', 'S-M-L', 6, '2-2-2', 2, 12, 15.50, 186.00, 0, '2018-07-28 02:27:58', '2018-07-28 02:27:58'),
(97, 75, 14, '9389 RD  .', 'GREEN', 'S-M-L', 6, '2-2-2', 1, 6, 15.50, 93.00, 0, '2018-07-28 02:27:58', '2018-07-28 02:27:58'),
(98, 75, 15, '9403 WHT', 'WHITE', 'S-M-L', 6, '2-2-2', 2, 12, 15.00, 180.00, 0, '2018-07-28 02:27:58', '2018-07-28 02:27:58'),
(99, 75, 15, '9403 WHT', 'BLACK', 'S-M-L', 6, '2-2-2', 1, 6, 15.00, 90.00, 0, '2018-07-28 02:27:58', '2018-07-28 02:27:58'),
(100, 76, 19, '9376 PINK', 'MINT', '2/2/2', 2, '2', 1, 2, 14.50, 29.00, 0, '2018-07-28 02:27:58', '2018-07-28 02:27:58'),
(101, 77, 14, '9389 RD  .', 'RED', 'S-M-L', 6, '2-2-2', 2, 12, 15.50, 186.00, 0, '2018-07-28 02:51:00', '2018-07-28 02:51:00'),
(102, 77, 14, '9389 RD  .', 'GREEN', 'S-M-L', 6, '2-2-2', 1, 6, 15.50, 93.00, 0, '2018-07-28 02:51:00', '2018-07-28 02:51:00'),
(103, 77, 15, '9403 WHT', 'WHITE', 'S-M-L', 6, '2-2-2', 2, 12, 15.00, 180.00, 0, '2018-07-28 02:51:00', '2018-07-28 02:51:00'),
(104, 77, 15, '9403 WHT', 'BLACK', 'S-M-L', 6, '2-2-2', 1, 6, 15.00, 90.00, 0, '2018-07-28 02:51:00', '2018-07-28 02:51:00'),
(105, 78, 19, '9376 PINK', 'MINT', '2/2/2', 2, '2', 1, 2, 14.50, 29.00, 0, '2018-07-28 02:51:00', '2018-07-28 02:51:00'),
(106, 79, 14, '9389 RD  .', 'RED', 'S-M-L', 6, '2-2-2', 2, 12, 15.50, 186.00, 0, '2018-07-29 19:30:13', '2018-07-29 19:30:13'),
(107, 79, 14, '9389 RD  .', 'GREEN', 'S-M-L', 6, '2-2-2', 1, 6, 15.50, 93.00, 0, '2018-07-29 19:30:13', '2018-07-29 19:30:13'),
(108, 79, 15, '9403 WHT', 'WHITE', 'S-M-L', 6, '2-2-2', 2, 12, 15.00, 180.00, 0, '2018-07-29 19:30:13', '2018-07-29 19:30:13'),
(109, 79, 15, '9403 WHT', 'BLACK', 'S-M-L', 6, '2-2-2', 1, 6, 15.00, 90.00, 0, '2018-07-29 19:30:13', '2018-07-29 19:30:13'),
(110, 80, 19, '9376 PINK', 'MINT', '2/2/2', 2, '2', 1, 2, 14.50, 29.00, 0, '2018-07-29 19:30:13', '2018-07-29 19:30:13'),
(111, 81, 14, '9389 RD  .', 'RED', 'S-M-L', 6, '2-2-2', 2, 12, 15.50, 186.00, 0, '2018-07-30 01:27:31', '2018-07-30 01:27:31'),
(112, 81, 14, '9389 RD  .', 'GREEN', 'S-M-L', 6, '2-2-2', 1, 6, 15.50, 93.00, 0, '2018-07-30 01:27:31', '2018-07-30 01:27:31'),
(113, 81, 15, '9403 WHT', 'WHITE', 'S-M-L', 6, '2-2-2', 2, 12, 15.00, 180.00, 0, '2018-07-30 01:27:31', '2018-07-30 01:27:31'),
(114, 81, 15, '9403 WHT', 'BLACK', 'S-M-L', 6, '2-2-2', 1, 6, 15.00, 90.00, 0, '2018-07-30 01:27:31', '2018-07-30 01:27:31'),
(115, 82, 19, '9376 PINK', 'MINT', '2/2/2', 2, '2', 1, 2, 14.50, 29.00, 0, '2018-07-30 01:27:31', '2018-07-30 01:27:31'),
(116, 83, 14, '9389 RD  .', 'RED', 'S-M-L', 6, '2-2-2', 2, 12, 15.50, 186.00, 0, '2018-07-30 19:50:26', '2018-07-31 19:34:29'),
(117, 83, 14, '9389 RD  .', 'GREEN', 'S-M-L', 6, '2-2-2', 1, 6, 15.50, 93.00, 0, '2018-07-30 19:50:26', '2018-07-31 19:34:29'),
(118, 83, 15, '9403 WHT', 'WHITE', 'S-M-L', 6, '2-2-2', 2, 12, 15.00, 180.00, 0, '2018-07-30 19:50:26', '2018-07-31 19:34:29'),
(119, 83, 15, '9403 WHT', 'BLACK', 'S-M-L', 6, '2-2-2', 1, 6, 15.00, 90.00, 0, '2018-07-30 19:50:26', '2018-07-31 19:34:29'),
(120, 84, 19, '9376 PINK', 'MINT', '2/2/2', 2, '2', 1, 2, 14.50, 29.00, 0, '2018-07-30 19:55:45', '2018-07-30 19:55:45'),
(121, 85, 14, '9389 RD  .', 'GREEN', 'S-M-L', 6, '2-2-2', 1, 6, 15.50, 93.00, 0, '2018-07-30 21:51:08', '2018-07-30 21:51:08'),
(122, 85, 14, '9389 RD  .', 'RED', 'S-M-L', 6, '2-2-2', 2, 12, 15.50, 186.00, 0, '2018-07-30 21:51:08', '2018-07-30 21:51:08'),
(123, 86, 14, '9389 RD  .', 'GREEN', 'S-M-L', 6, '2-2-2', 1, 6, 15.50, 93.00, 0, '2018-07-30 22:13:35', '2018-07-30 22:13:35'),
(124, 86, 14, '9389 RD  .', 'RED', 'S-M-L', 6, '2-2-2', 2, 12, 15.50, 186.00, 0, '2018-07-30 22:13:35', '2018-07-30 22:13:35'),
(125, 87, 14, '9389 RD  .', 'GREEN', 'S-M-L', 6, '2-2-2', 1, 6, 15.50, 93.00, 0, '2018-07-31 01:02:24', '2018-07-31 01:02:24'),
(126, 87, 14, '9389 RD  .', 'RED', 'S-M-L', 6, '2-2-2', 2, 12, 15.50, 186.00, 0, '2018-07-31 01:02:24', '2018-07-31 01:02:24'),
(127, 88, 14, '9389 RD  .', 'GREEN', 'S-M-L', 6, '2-2-2', 1, 6, 15.50, 93.00, 0, '2018-07-31 22:09:51', '2018-07-31 22:09:51'),
(128, 88, 14, '9389 RD  .', 'RED', 'S-M-L', 6, '2-2-2', 2, 12, 15.50, 186.00, 0, '2018-07-31 22:09:51', '2018-07-31 22:09:51'),
(129, 89, 14, '9389 RD  .', 'GREEN', 'S-M-L', 6, '2-2-2', 1, 6, 15.50, 93.00, 0, '2018-07-31 22:37:36', '2018-07-31 22:37:36'),
(130, 89, 14, '9389 RD  .', 'RED', 'S-M-L', 6, '2-2-2', 2, 12, 15.50, 186.00, 0, '2018-07-31 22:37:36', '2018-07-31 22:37:36'),
(131, 90, 14, '9389 RD  .', 'GREEN', 'S-M-L', 6, '2-2-2', 1, 6, 15.50, 93.00, 0, '2018-07-31 22:41:50', '2018-07-31 22:41:50'),
(132, 90, 14, '9389 RD  .', 'RED', 'S-M-L', 6, '2-2-2', 2, 12, 15.50, 186.00, 0, '2018-07-31 22:41:50', '2018-07-31 22:41:50'),
(133, 91, 14, '9389 RD  .', 'GREEN', 'S-M-L', 6, '2-2-2', 1, 6, 15.50, 93.00, 0, '2018-08-01 01:40:15', '2018-08-01 01:40:15'),
(134, 91, 14, '9389 RD  .', 'RED', 'S-M-L', 6, '2-2-2', 2, 12, 15.50, 186.00, 0, '2018-08-01 01:40:15', '2018-08-01 01:40:15'),
(135, 92, 14, '9389 RD  .', 'RED', 'S-M-L', 6, '2-2-2', 2, 12, 15.50, 186.00, 0, '2018-08-01 03:01:09', '2018-08-01 03:01:09'),
(136, 92, 14, '9389 RD  .', 'GREEN', 'S-M-L', 6, '2-2-2', 1, 6, 15.50, 93.00, 0, '2018-08-01 03:01:09', '2018-08-01 03:01:09'),
(137, 93, 16, '7378 WHT', 'PINK', '3-3', 6, '3-3', 2, 12, 12.50, 150.00, 0, '2018-08-01 19:08:51', '2018-08-01 19:08:51'),
(138, 93, 16, '7378 WHT', 'BLUE', '3-3', 6, '3-3', 1, 6, 12.50, 75.00, 0, '2018-08-01 19:08:51', '2018-08-01 19:08:51'),
(139, 93, 14, '9389 RD  .', 'GREEN', 'S-M-L', 6, '2-2-2', 1, 6, 15.50, 93.00, 0, '2018-08-01 19:14:06', '2018-08-01 19:14:06'),
(140, 93, 14, '9389 RD  .', 'RED', 'S-M-L', 6, '2-2-2', 2, 12, 15.50, 186.00, 0, '2018-08-01 19:14:06', '2018-08-01 19:14:06'),
(141, 101, 14, '9389 RD  .', 'RED', 'S-M-L', 6, '2-2-2', 2, 12, 15.50, 186.00, 0, '2018-08-01 20:37:46', '2018-08-16 19:35:50'),
(142, 105, 16, '7378 WHT', 'PINK', '3-3', 6, '3-3', 1, 6, 12.50, 75.00, 0, '2018-08-01 20:37:46', '2018-09-04 20:02:35'),
(143, 94, 16, '7378 WHT', 'BLUE', '3-3', 6, '3-3', 3, 18, 12.50, 225.00, 0, '2018-08-01 20:37:46', '2018-08-01 23:18:39'),
(144, 94, 14, '9389 RD  .', 'GREEN', 'S-M-L', 6, '2-2-2', 1, 6, 15.50, 93.00, 0, '2018-08-01 20:37:46', '2018-08-01 23:18:39'),
(145, 95, 18, '2314. WH  .', 'WHITE', '2/2/2', 2, '2', 1, 2, 14.50, 29.00, 0, '2018-08-01 20:37:46', '2018-08-01 20:37:46'),
(146, 95, 18, '2314. WH  .', 'JADE', '2/2/2', 2, '2', 1, 2, 14.50, 29.00, 0, '2018-08-01 20:37:46', '2018-08-01 20:37:46'),
(147, 95, 19, '9376 PINK', 'PINK', '2/2/2', 2, '2', 2, 4, 14.50, 58.00, 0, '2018-08-01 20:37:46', '2018-08-01 20:37:46'),
(148, 96, 16, '7378 WHT', 'PINK', '3-3', 6, '3-3', 2, 12, 12.50, 150.00, 0, '2018-08-01 21:00:15', '2018-08-01 21:00:15'),
(149, 96, 16, '7378 WHT', 'BLUE', '3-3', 6, '3-3', 1, 6, 12.50, 75.00, 0, '2018-08-01 21:00:15', '2018-08-01 21:00:15'),
(150, 97, 16, '7378 WHT', 'PINK', '3-3', 6, '3-3', 2, 12, 12.50, 150.00, 0, '2018-08-01 21:25:24', '2018-08-01 21:25:24'),
(151, 97, 16, '7378 WHT', 'BLUE', '3-3', 6, '3-3', 1, 6, 12.50, 75.00, 0, '2018-08-01 21:25:24', '2018-08-01 21:25:24'),
(152, 98, 18, '2314. WH  .', 'WHITE', '2/2/2', 2, '2', 1, 2, 14.50, 29.00, 0, '2018-08-09 19:22:27', '2018-08-09 19:22:27'),
(153, 98, 19, '9376 PINK', 'PINK', '2/2/2', 2, '2', 2, 4, 14.50, 58.00, 0, '2018-08-09 19:22:27', '2018-08-09 19:22:27'),
(154, 98, 19, '9376 PINK', 'MINT', '2/2/2', 2, '2', 1, 2, 14.50, 29.00, 0, '2018-08-09 19:22:27', '2018-08-09 19:22:27'),
(155, 99, 14, '9389 RD  .', 'GREEN', 'S-M-L', 6, '2-2-2', 1, 6, 15.50, 93.00, 0, '2018-08-09 19:22:55', '2018-08-09 19:22:55'),
(156, 100, 14, '9389 RD  .', 'GREEN', 'S-M-L', 6, '2-2-2', 1, 6, 15.50, 93.00, 0, '2018-08-09 19:40:05', '2018-08-09 19:40:05'),
(157, 101, 14, '9389 RD  .', 'RED', 'S-M-L', 6, '2-2-2', 2, 12, 15.50, 186.00, 1, '2018-08-16 19:35:50', '2018-08-16 19:35:50'),
(158, 94, 16, '7378 WHT', 'PINK', '3-3', 6, '3-3', 1, 6, 12.50, 75.00, 1, '2018-08-16 19:36:51', '2018-08-16 19:36:51'),
(159, 103, 14, '9389 RD  .', 'GREEN', 'S-M-L', 6, '2-2-2', 1, 6, 15.50, 93.00, 0, '2018-08-28 19:23:11', '2018-09-04 19:55:14'),
(160, 104, 18, '2314. WH  .', 'WHITE', '2/2/2', 2, '2', 1, 2, 14.50, 29.00, 0, '2018-08-28 19:23:11', '2018-08-28 19:23:11'),
(161, 104, 19, '9376 PINK', 'PINK', '2/2/2', 2, '2', 2, 4, 14.50, 58.00, 0, '2018-08-28 19:23:11', '2018-08-28 19:23:11'),
(162, 104, 19, '9376 PINK', 'MINT', '2/2/2', 2, '2', 1, 2, 14.50, 29.00, 0, '2018-08-28 19:23:11', '2018-08-28 19:23:11'),
(163, 102, 16, '7378 WHT', 'PINK', '3-3', 6, '3-3', 1, 6, 12.50, 75.00, 1, '2018-09-04 20:02:35', '2018-09-04 20:02:35'),
(164, 106, 67, 'LT3751', 'BLACK', 'S-M-L', 6, '2-2-2', 1, 6, 12.75, 76.50, 0, '2018-09-04 20:04:58', '2018-09-04 20:04:58'),
(165, 106, 77, 'pant', 'tte', '3-3', 6, '3-3', 3, 18, 30.60, 550.80, 0, '2018-09-04 20:04:58', '2018-09-04 20:06:49'),
(166, 106, 77, 'pant', 'BLACK', '3-3', 6, '3-3', 2, 12, 30.60, 367.20, 0, '2018-09-04 20:04:58', '2018-09-04 20:06:49'),
(167, 107, 14, '9389 RD  .', 'WHITE', 'S-M-L', 6, '2-2-2', 1, 6, 15.50, 93.00, 0, '2018-09-04 20:04:58', '2018-09-04 20:05:46'),
(168, 106, 14, '9389 RD  .', 'RED', 'S-M-L', 6, '2-2-2', 2, 12, 15.50, 186.00, 0, '2018-09-04 20:04:58', '2018-09-04 20:06:49'),
(169, 106, 14, '9389 RD  .', 'GREEN', 'S-M-L', 6, '2-2-2', 1, 6, 15.50, 93.00, 0, '2018-09-04 20:04:59', '2018-09-04 20:06:49'),
(170, 106, 77, 'pant', 'BEIGE', '3-3', 6, '3-3', 1, 6, 30.60, 183.60, 0, '2018-09-04 20:04:59', '2018-09-04 20:06:49'),
(171, 106, 14, '9389 RD  .', 'WHITE', 'S-M-L', 6, '2-2-2', 1, 6, 15.50, 93.00, 1, '2018-09-04 20:05:46', '2018-09-04 20:05:46'),
(172, 108, 15, '9403 WHT', 'BLACK', 'S-M-L', 6, '2-2-2', 1, 6, 15.00, 90.00, 0, '2018-09-13 20:27:41', '2018-09-13 20:27:41'),
(173, 109, 15, '9403 WHT', 'BLACK', 'S-M-L', 6, '2-2-2', 1, 6, 15.00, 90.00, 0, '2018-09-13 20:32:54', '2018-09-13 20:32:54'),
(174, 110, 15, '9403 WHT', 'BLACK', 'S-M-L', 6, '2-2-2', 1, 6, 15.00, 90.00, 0, '2018-09-13 20:35:20', '2018-09-13 21:04:42'),
(175, 111, 17, '7437 COR .', 'CORAL', 'S-M-L', 6, '2-2-2', 1, 6, 14.50, 87.00, 0, '2018-09-13 23:18:45', '2018-09-13 23:18:45'),
(176, 113, 15, '9403 WHT', 'WHITE', 'S-M-L', 6, '2-2-2', 5, 30, 15.00, 450.00, 0, '2018-09-17 20:24:15', '2018-09-17 20:43:31'),
(177, 112, 15, '9403 WHT', 'BLACK', 'S-M-L', 6, '2-2-2', 4, 24, 15.00, 360.00, 0, '2018-09-17 20:24:15', '2018-09-17 20:24:15'),
(178, 112, 15, '9403 WHT', 'WHITE', 'S-M-L', 6, '2-2-2', 5, 30, 15.00, 450.00, 1, '2018-09-17 20:43:31', '2018-09-17 20:43:31'),
(179, 114, 15, '9403 WHT', 'WHITE', 'S-M-L', 6, '2-2-2', 1, 6, 15.00, 90.00, 0, '2018-09-17 20:47:58', '2018-09-17 20:47:58'),
(180, 115, 15, '9403 WHT', 'BLACK', 'S-M-L', 6, '2-2-2', 1, 6, 15.00, 90.00, 0, '2018-09-17 20:47:58', '2018-09-17 20:49:30'),
(181, 114, 15, '9403 WHT', 'BLACK', 'S-M-L', 6, '2-2-2', 1, 6, 15.00, 90.00, 1, '2018-09-17 20:49:30', '2018-09-17 20:49:30'),
(182, 116, 15, '9403 WHT', 'WHITE', 'S-M-L', 6, '2-2-2', 5, 30, 15.00, 450.00, 0, '2018-09-17 21:21:12', '2018-09-17 21:21:12'),
(183, 117, 15, '9403 WHT', 'BLACK', 'S-M-L', 6, '2-2-2', 3, 18, 15.00, 270.00, 0, '2018-09-17 21:21:12', '2018-09-17 21:21:55'),
(184, 116, 15, '9403 WHT', 'BLACK', 'S-M-L', 6, '2-2-2', 3, 18, 15.00, 270.00, 1, '2018-09-17 21:21:55', '2018-09-17 21:21:55'),
(185, 118, 14, '9389 RD  .', 'RED', 'S-M-L', 6, '2-2-2', 2, 12, 15.50, 186.00, 0, '2018-10-05 20:38:43', '2018-10-05 20:38:43'),
(186, 118, 14, '9389 RD  .', 'GREEN', 'S-M-L', 6, '2-2-2', 1, 6, 15.50, 93.00, 0, '2018-10-05 20:38:43', '2018-10-05 20:38:43'),
(187, 119, 17, '7437 COR .', 'CORAL', 'S-M-L', 6, '2-2-2', 1, 6, 14.50, 87.00, 0, '2018-10-29 02:11:03', '2018-10-29 02:11:03'),
(188, 120, 123, '9383 TEST', 'WHITE', 'S-M-L', 6, '2-2-2', 4, 24, 15.50, 372.00, 0, '2018-10-29 02:13:10', '2018-11-04 23:39:39'),
(189, 120, 123, '9383 TEST', 'RED', 'S-M-L', 6, '2-2-2', 3, 18, 15.50, 279.00, 0, '2018-10-29 02:13:10', '2018-11-04 23:39:39'),
(190, 120, 123, '9383 TEST', 'GREEN', 'S-M-L', 6, '2-2-2', 2, 12, 15.50, 186.00, 0, '2018-10-29 02:13:10', '2018-11-04 23:39:39'),
(191, 121, 15, '9403 WHT', 'BLACK', 'S-M-L', 6, '2-2-2', 1, 6, 15.00, 90.00, 0, '2018-11-20 20:08:26', '2018-11-20 20:08:26'),
(192, 122, 123, '9383 TEST', 'WHITE', 'S-M-L', 6, '2-2-2', 2, 12, 15.50, 186.00, 0, '2018-11-23 19:31:00', '2018-11-23 19:31:00'),
(193, 122, 123, '9383 TEST', 'RED', 'S-M-L', 6, '2-2-2', 2, 12, 15.50, 186.00, 0, '2018-11-23 19:31:00', '2018-11-23 19:31:00'),
(194, 122, 123, '9383 TEST', 'GREEN', 'S-M-L', 6, '2-2-2', 2, 12, 15.50, 186.00, 0, '2018-11-23 19:31:00', '2018-11-23 19:31:00');

-- --------------------------------------------------------

--
-- Table structure for table `packs`
--

DROP TABLE IF EXISTS `packs`;
CREATE TABLE IF NOT EXISTS `packs` (
  `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT,
  `name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `status` tinyint(1) NOT NULL,
  `default` tinyint(1) NOT NULL,
  `vendor_meta_id` int(11) NOT NULL,
  `description` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `pack1` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `pack2` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `pack3` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `pack4` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `pack5` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `pack6` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `pack7` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `pack8` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `pack9` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `pack10` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=27 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `packs`
--

INSERT INTO `packs` (`id`, `name`, `status`, `default`, `vendor_meta_id`, `description`, `pack1`, `pack2`, `pack3`, `pack4`, `pack5`, `pack6`, `pack7`, `pack8`, `pack9`, `pack10`, `created_at`, `updated_at`, `deleted_at`) VALUES
(1, 'S-M-L', 1, 0, 1, NULL, '2', '2', '2', NULL, NULL, NULL, NULL, NULL, NULL, NULL, '2018-05-11 05:16:28', '2018-09-18 03:17:14', NULL),
(2, '3-3', 1, 0, 1, NULL, '3', '3', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '2018-05-11 05:30:54', '2018-09-18 03:17:14', NULL),
(3, 'ONE SIZE', 1, 0, 1, 'erer', '6', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '2018-05-16 11:39:34', '2018-09-18 03:17:14', NULL),
(4, '2/2/2', 1, 0, 2, NULL, '2', '2', '2', NULL, NULL, NULL, NULL, NULL, NULL, NULL, '2018-05-23 15:21:18', '2018-05-23 15:21:18', NULL),
(5, '3/3', 1, 0, 2, NULL, '3', '3', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '2018-05-23 15:21:28', '2018-05-23 15:21:28', NULL),
(6, 'ONE SIZE', 1, 0, 2, NULL, '6', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '2018-05-23 15:21:38', '2018-05-23 15:21:38', NULL),
(7, 'dsfd', 1, 0, 1, NULL, '1', '2', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '2018-05-26 08:59:37', '2018-09-18 03:17:14', NULL),
(8, '123', 1, 0, 1, NULL, '2', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '2018-05-26 10:34:42', '2018-05-26 10:42:56', '2018-05-26 10:42:56'),
(9, 'S-M-4', 1, 0, 1, NULL, '5', '5', '6', NULL, NULL, NULL, NULL, NULL, NULL, NULL, '2018-07-03 07:36:21', '2018-09-18 03:17:14', NULL),
(10, 't-t-s-3', 1, 0, 1, NULL, '2', '3', '45', '2', NULL, NULL, NULL, NULL, NULL, NULL, '2018-07-04 03:49:09', '2018-07-04 03:49:17', '2018-07-04 03:49:17'),
(11, '1', 1, 0, 1, 'dd', '2', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '2018-07-17 21:08:12', '2018-09-18 03:17:14', NULL),
(12, 'S-M-L', 1, 0, 1, NULL, '2', '2', '3', NULL, NULL, NULL, NULL, NULL, NULL, NULL, '2018-07-17 22:16:15', '2018-07-17 22:16:24', '2018-07-17 22:16:24'),
(13, 'S-M-L', 1, 0, 1, NULL, '2', '2', '3', NULL, NULL, NULL, NULL, NULL, NULL, NULL, '2018-07-17 22:16:55', '2018-09-18 03:17:14', NULL),
(14, 'S-M-L', 1, 0, 1, NULL, '2', '2', '3', NULL, NULL, NULL, NULL, NULL, NULL, NULL, '2018-07-17 22:18:03', '2018-09-18 03:17:14', NULL),
(15, 'S-M-L', 1, 0, 1, NULL, '2', '2', '3', NULL, NULL, NULL, NULL, NULL, NULL, NULL, '2018-07-17 22:51:47', '2018-09-18 03:17:14', NULL),
(16, 'S-M-L', 1, 0, 1, NULL, '2', '2', '3', NULL, NULL, NULL, NULL, NULL, NULL, NULL, '2018-07-17 22:51:51', '2018-09-18 03:17:14', NULL),
(17, 'S-M-L', 1, 0, 1, NULL, '2', '2', '3', NULL, NULL, NULL, NULL, NULL, NULL, NULL, '2018-07-17 23:16:21', '2018-09-18 03:17:14', NULL),
(18, 'S-M-L', 1, 0, 1, NULL, '2', '2', '3', NULL, NULL, NULL, NULL, NULL, NULL, NULL, '2018-07-18 23:05:27', '2018-09-18 03:17:14', NULL),
(19, 'S-M-L', 1, 0, 1, NULL, '2', '2', '3', NULL, NULL, NULL, NULL, NULL, NULL, NULL, '2018-07-18 23:05:59', '2018-09-18 03:17:14', NULL),
(20, 'S-M-L', 1, 0, 1, NULL, '2', '2', '3', NULL, NULL, NULL, NULL, NULL, NULL, NULL, '2018-07-19 03:59:55', '2018-09-18 03:17:14', NULL),
(21, 'S-M-L', 1, 0, 1, NULL, '2', '2', '3', NULL, NULL, NULL, NULL, NULL, NULL, NULL, '2018-07-19 04:27:54', '2018-09-18 03:17:14', NULL),
(22, 'S-M-L', 1, 1, 1, NULL, '2', '2', '3', NULL, NULL, NULL, NULL, NULL, NULL, NULL, '2018-08-09 02:38:25', '2018-09-18 03:17:14', NULL),
(23, 'S-M-L', 1, 0, 1, NULL, '2', '2', '3', NULL, NULL, NULL, NULL, NULL, NULL, NULL, '2018-08-09 02:38:52', '2018-09-18 03:17:14', NULL),
(24, '1', 1, 0, 1, NULL, '1', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '2018-08-15 22:10:36', '2018-09-18 03:17:14', NULL),
(25, 'S-M-L', 1, 0, 1, NULL, '2', '2', '3', NULL, NULL, NULL, NULL, NULL, NULL, NULL, '2018-08-16 03:07:36', '2018-09-18 03:17:14', NULL),
(26, 'S-M-L', 1, 0, 1, NULL, '2', '2', '3', NULL, NULL, NULL, NULL, NULL, NULL, NULL, '2018-08-16 03:08:05', '2018-09-18 03:17:14', NULL);

-- --------------------------------------------------------

--
-- Table structure for table `password_resets`
--

DROP TABLE IF EXISTS `password_resets`;
CREATE TABLE IF NOT EXISTS `password_resets` (
  `email` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `token` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  KEY `password_resets_email_index` (`email`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `patterns`
--

DROP TABLE IF EXISTS `patterns`;
CREATE TABLE IF NOT EXISTS `patterns` (
  `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT,
  `name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `parent_category_id` int(11) NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=28 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `patterns`
--

INSERT INTO `patterns` (`id`, `name`, `parent_category_id`, `created_at`, `updated_at`, `deleted_at`) VALUES
(1, '1', 1, '2018-05-17 06:44:46', '2018-05-17 06:44:52', '2018-05-17 06:44:52'),
(2, 'Burnout', 1, '2018-05-17 06:44:57', '2018-05-22 14:19:31', NULL),
(3, 'Check', 1, '2018-05-22 14:19:38', '2018-05-22 14:19:38', NULL),
(4, 'Color block', 1, '2018-05-22 14:19:46', '2018-05-22 14:19:46', NULL),
(5, 'Crochet', 1, '2018-05-22 14:19:55', '2018-05-22 14:19:55', NULL),
(6, 'Digital Print', 1, '2018-05-22 14:20:02', '2018-05-22 14:20:02', NULL),
(7, 'Embroidered', 1, '2018-05-22 14:20:11', '2018-05-22 14:20:11', NULL),
(8, 'Glitter/Sequins', 1, '2018-05-22 14:20:17', '2018-05-22 14:20:17', NULL),
(9, 'Lace', 1, '2018-05-22 14:20:27', '2018-05-22 14:20:27', NULL),
(10, 'Lettering', 1, '2018-05-22 14:20:33', '2018-05-22 14:20:33', NULL),
(11, 'Metallic', 1, '2018-05-22 14:20:41', '2018-05-22 14:20:41', NULL),
(12, 'Multi-Color', 1, '2018-05-22 14:20:47', '2018-05-22 14:20:47', NULL),
(13, 'Multicolor', 1, '2018-05-22 14:20:54', '2018-05-22 14:20:54', NULL),
(14, 'Ombre', 1, '2018-05-22 14:21:02', '2018-05-22 14:21:02', NULL),
(15, 'Plaid', 1, '2018-05-22 14:21:10', '2018-05-22 14:21:10', NULL),
(16, 'Polka Dot', 1, '2018-05-22 14:21:17', '2018-05-22 14:21:17', NULL),
(17, 'Print Screen', 1, '2018-05-22 14:21:24', '2018-05-22 14:21:24', NULL),
(18, 'Print Sublimation', 1, '2018-05-22 14:21:31', '2018-05-22 14:21:31', NULL),
(19, 'Print-Animal-Floral-Skull-Butterfly', 1, '2018-05-22 14:21:37', '2018-05-22 14:21:37', NULL),
(20, 'Rhinestones', 1, '2018-05-22 14:21:44', '2018-05-22 14:21:44', NULL),
(21, 'Ribbon/Bows', 1, '2018-05-22 14:21:51', '2018-05-22 14:21:51', NULL),
(22, 'Solid', 1, '2018-05-22 14:21:59', '2018-05-22 14:21:59', NULL),
(23, 'Striped', 1, '2018-05-22 14:22:07', '2018-05-22 14:22:07', NULL),
(24, 'Striped Sublimation', 1, '2018-05-22 14:22:15', '2018-05-22 14:22:15', NULL),
(27, 'tt', 5, '2018-06-22 04:18:28', '2018-06-22 04:18:41', '2018-06-22 04:18:41');

-- --------------------------------------------------------

--
-- Table structure for table `reviews`
--

DROP TABLE IF EXISTS `reviews`;
CREATE TABLE IF NOT EXISTS `reviews` (
  `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT,
  `order_id` int(11) NOT NULL,
  `meta_vendor_id` int(11) NOT NULL,
  `user_id` int(11) NOT NULL,
  `star` int(11) NOT NULL DEFAULT '0',
  `review` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `reply` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=7 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `reviews`
--

INSERT INTO `reviews` (`id`, `order_id`, `meta_vendor_id`, `user_id`, `star`, `review`, `reply`, `created_at`, `updated_at`, `deleted_at`) VALUES
(1, 26, 2, 14, 3, 'boss', NULL, '2018-07-03 04:40:53', '2018-07-03 07:45:04', NULL),
(2, 25, 1, 14, 2, 'Very nice service!2', NULL, '2018-07-03 07:45:04', '2018-07-03 09:48:19', NULL),
(3, 24, 1, 14, 4, 'df', NULL, '2018-07-03 07:58:59', '2018-07-03 09:48:19', NULL),
(4, 23, 1, 14, 4, 'erer', 'ere33', '2018-07-03 09:04:03', '2018-07-03 09:48:19', NULL),
(5, 61, 1, 14, 0, '1212', NULL, '2018-07-20 05:41:49', '2018-07-25 22:35:56', '2018-07-25 22:35:56'),
(6, 68, 1, 14, 0, 'll', NULL, '2018-07-20 06:00:56', '2018-07-20 06:00:56', NULL);

-- --------------------------------------------------------

--
-- Table structure for table `settings`
--

DROP TABLE IF EXISTS `settings`;
CREATE TABLE IF NOT EXISTS `settings` (
  `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT,
  `name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `value` varchar(5000) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=6 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `settings`
--

INSERT INTO `settings` (`id`, `name`, `value`, `created_at`, `updated_at`) VALUES
(2, 'buyer_home', '<p>asdf as<strong>asdfasdfasd</strong></p>\r\n\r\n<p>&nbsp;</p>\r\n\r\n<p>&nbsp;</p>\r\n\r\n<p><strong>asdfasdfasd</strong></p>', '2018-07-10 12:28:05', '2018-07-10 12:28:16'),
(3, 'welcome_notification', '<p>asd asdfasdfa<strong> a adsfasdfaa sdf5</strong></p>', '2018-07-11 22:33:28', '2018-08-03 02:07:19'),
(4, 'opening_soon', '0', '2018-08-03 02:18:24', '2018-08-03 02:34:30'),
(5, 'opening_date', '08/31/2018', '2018-08-03 02:23:01', '2018-08-03 02:30:22');

-- --------------------------------------------------------

--
-- Table structure for table `shipping_methods`
--

DROP TABLE IF EXISTS `shipping_methods`;
CREATE TABLE IF NOT EXISTS `shipping_methods` (
  `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT,
  `status` tinyint(1) NOT NULL,
  `default` tinyint(1) NOT NULL,
  `courier_id` int(11) NOT NULL,
  `ship_method_id` int(11) NOT NULL,
  `ship_method_text` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `list_order` int(11) NOT NULL,
  `vendor_meta_id` int(11) NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=5 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `shipping_methods`
--

INSERT INTO `shipping_methods` (`id`, `status`, `default`, `courier_id`, `ship_method_id`, `ship_method_text`, `list_order`, `vendor_meta_id`, `created_at`, `updated_at`, `deleted_at`) VALUES
(1, 1, 1, 1, 1, NULL, 1, 1, '2018-06-01 04:41:24', '2018-06-25 10:20:16', NULL),
(2, 1, 0, 1, 3, NULL, 2, 1, '2018-06-01 04:41:42', '2018-06-25 10:20:16', NULL),
(3, 1, 0, 3, 5, NULL, 1, 2, '2018-06-04 09:22:27', '2018-06-04 09:22:27', NULL),
(4, 1, 0, 0, 0, 'jhk2', 3, 1, '2018-07-18 01:54:52', '2018-07-18 01:59:58', NULL);

-- --------------------------------------------------------

--
-- Table structure for table `sizes`
--

DROP TABLE IF EXISTS `sizes`;
CREATE TABLE IF NOT EXISTS `sizes` (
  `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT,
  `name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `status` tinyint(1) NOT NULL,
  `default` tinyint(1) NOT NULL,
  `user_id` int(11) NOT NULL,
  `size1` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `size2` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `size3` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `size4` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `size5` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `size6` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `size7` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `size8` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `size9` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `size10` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=3 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `sizes`
--

INSERT INTO `sizes` (`id`, `name`, `status`, `default`, `user_id`, `size1`, `size2`, `size3`, `size4`, `size5`, `size6`, `size7`, `size8`, `size9`, `size10`, `created_at`, `updated_at`, `deleted_at`) VALUES
(1, 'Test Size', 1, 0, 1, 'S', 'M', 'L', NULL, NULL, NULL, NULL, NULL, NULL, NULL, '2018-05-09 06:42:40', '2018-05-11 08:16:24', NULL),
(2, 'Test Size 2', 0, 1, 1, 'S', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '2018-05-10 03:25:13', '2018-05-11 08:16:24', NULL);

-- --------------------------------------------------------

--
-- Table structure for table `slider_items`
--

DROP TABLE IF EXISTS `slider_items`;
CREATE TABLE IF NOT EXISTS `slider_items` (
  `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT,
  `vendor_meta_id` int(11) NOT NULL,
  `item_id` int(11) NOT NULL,
  `sort` int(11) NOT NULL,
  `type` int(11) NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=50 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `slider_items`
--

INSERT INTO `slider_items` (`id`, `vendor_meta_id`, `item_id`, `sort`, `type`, `created_at`, `updated_at`) VALUES
(38, 1, 17, 2, 3, '2018-06-13 12:55:04', '2018-06-13 14:07:05'),
(37, 1, 16, 3, 3, '2018-06-13 12:55:01', '2018-06-13 14:07:05'),
(36, 1, 15, 1, 3, '2018-06-13 12:54:59', '2018-06-13 14:07:05'),
(34, 1, 17, 1, 2, '2018-06-13 12:54:53', '2018-06-13 14:03:22'),
(33, 1, 15, 2, 5, '2018-06-13 12:54:50', '2018-06-13 14:16:10'),
(32, 1, 16, 1, 5, '2018-06-13 12:54:48', '2018-06-13 14:16:10'),
(31, 1, 16, 1, 4, '2018-06-13 12:54:45', '2018-11-20 19:57:19'),
(30, 1, 15, 2, 4, '2018-06-13 12:54:43', '2018-11-20 19:57:19'),
(47, 1, 17, 1, 1, '2018-07-06 12:18:29', '2018-11-20 19:57:18'),
(48, 1, 16, 3, 1, '2018-07-06 12:20:03', '2018-11-20 19:57:18'),
(49, 1, 15, 2, 1, '2018-07-06 12:21:21', '2018-11-20 19:57:18');

-- --------------------------------------------------------

--
-- Table structure for table `states`
--

DROP TABLE IF EXISTS `states`;
CREATE TABLE IF NOT EXISTS `states` (
  `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT,
  `country_id` int(11) NOT NULL,
  `name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `code` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=79 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `states`
--

INSERT INTO `states` (`id`, `country_id`, `name`, `code`) VALUES
(1, 1, 'Alaska', 'AK'),
(2, 1, 'Alabama', 'AL'),
(3, 1, 'American Samoa', 'AS'),
(4, 1, 'Arizona', 'AZ'),
(5, 1, 'Arkansas', 'AR'),
(6, 1, 'California', 'CA'),
(7, 1, 'Colorado', 'CO'),
(8, 1, 'Connecticut', 'CT'),
(9, 1, 'Delaware', 'DE'),
(10, 1, 'District of Columbia', 'DC'),
(11, 1, 'Federated States of Micronesia', 'FM'),
(12, 1, 'Florida', 'FL'),
(13, 1, 'Georgia', 'GA'),
(14, 1, 'Guam', 'GU'),
(15, 1, 'Hawaii', 'HI'),
(16, 1, 'Idaho', 'ID'),
(17, 1, 'Illinois', 'IL'),
(18, 1, 'Indiana', 'IN'),
(19, 1, 'Iowa', 'IA'),
(20, 1, 'Kansas', 'KS'),
(21, 1, 'Kentucky', 'KY'),
(22, 1, 'Louisiana', 'LA'),
(23, 1, 'Maine', 'ME'),
(24, 1, 'Marshall Islands', 'MH'),
(25, 1, 'Maryland', 'MD'),
(26, 1, 'Massachusetts', 'MA'),
(27, 1, 'Michigan', 'MI'),
(28, 1, 'Minnesota', 'MN'),
(29, 1, 'Mississippi', 'MS'),
(30, 1, 'Missouri', 'MO'),
(31, 1, 'Montana', 'MT'),
(32, 1, 'Nebraska', 'NE'),
(33, 1, 'Nevada', 'NV'),
(34, 1, 'New Hampshire', 'NH'),
(35, 1, 'New Jersey', 'NJ'),
(36, 1, 'New Mexico', 'NM'),
(37, 1, 'New York', 'NY'),
(38, 1, 'North Carolina', 'NC'),
(39, 1, 'North Dakota', 'ND'),
(40, 1, 'Northern Mariana Islands', 'MP'),
(41, 1, 'Ohio', 'OH'),
(42, 1, 'Oklahoma', 'OK'),
(43, 1, 'Oregon', 'OR'),
(44, 1, 'Palau', 'PW'),
(45, 1, 'Pennsylvania', 'PA'),
(46, 1, 'Puerto Rico', 'PR'),
(47, 1, 'Rhode Island', 'RI'),
(48, 1, 'South Carolina', 'SC'),
(49, 1, 'South Dakota', 'SD'),
(50, 1, 'Tennessee', 'TN'),
(51, 1, 'Texas', 'TX'),
(52, 1, 'Utah', 'UT'),
(53, 1, 'Vermont', 'VT'),
(54, 1, 'Virgin Islands', 'VI'),
(55, 1, 'Virginia', 'VA'),
(56, 1, 'Washington', 'WA'),
(57, 1, 'West Virginia', 'WV'),
(58, 1, 'Wisconsin', 'WI'),
(59, 1, 'Wyoming', 'WY'),
(60, 1, 'Armed Forces Africa', 'AE'),
(61, 1, 'Armed Forces Americas (except Canada)', 'AA'),
(62, 1, 'Armed Forces Canada', 'AE'),
(63, 1, 'Armed Forces Europe', 'AE'),
(64, 1, 'Armed Forces Middle East', 'AE'),
(65, 1, 'Armed Forces Pacific', 'AP'),
(66, 2, 'Alberta', 'AB'),
(67, 2, 'British Columbia', 'BC'),
(68, 2, 'Manitoba', 'MB'),
(69, 2, 'New Brunswick', 'NB'),
(70, 2, 'Newfoundland and Labrador', 'NL'),
(71, 2, 'Northwest Territories', 'NT'),
(72, 2, 'Nova Scotia', 'NS'),
(73, 2, 'Nunavut', 'NU'),
(74, 2, 'Ontario', 'ON'),
(75, 2, 'Prince Edward Island', 'PE'),
(76, 2, 'Quebec', 'QC'),
(77, 2, 'Saskatchewan', 'SK'),
(78, 2, 'Yukon', 'YT');

-- --------------------------------------------------------

--
-- Table structure for table `store_credits`
--

DROP TABLE IF EXISTS `store_credits`;
CREATE TABLE IF NOT EXISTS `store_credits` (
  `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT,
  `user_id` int(11) NOT NULL,
  `vendor_meta_id` int(11) NOT NULL,
  `amount` double(8,2) NOT NULL DEFAULT '0.00',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=3 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `store_credits`
--

INSERT INTO `store_credits` (`id`, `user_id`, `vendor_meta_id`, `amount`, `created_at`, `updated_at`) VALUES
(1, 14, 2, 20.00, '2018-07-12 21:34:49', '2018-07-18 02:26:28'),
(2, 14, 1, 22.00, '2018-07-20 05:34:40', '2018-11-04 23:39:39');

-- --------------------------------------------------------

--
-- Table structure for table `store_credit_transections`
--

DROP TABLE IF EXISTS `store_credit_transections`;
CREATE TABLE IF NOT EXISTS `store_credit_transections` (
  `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT,
  `user_id` int(11) NOT NULL,
  `vendor_meta_id` int(11) NOT NULL,
  `order_id` int(11) NOT NULL,
  `reason` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `amount` double(8,2) NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=12 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `store_credit_transections`
--

INSERT INTO `store_credit_transections` (`id`, `user_id`, `vendor_meta_id`, `order_id`, `reason`, `amount`, `created_at`, `updated_at`, `deleted_at`) VALUES
(2, 14, 1, 39, '1st reason', 10.00, '2018-07-12 21:34:49', '2018-07-12 21:34:49', NULL),
(3, 14, 1, 39, '1st reason33', 10.00, '2018-07-12 21:35:01', '2018-07-12 21:35:01', NULL),
(4, 14, 1, 56, 'Used', -20.00, '2018-07-13 22:33:21', '2018-07-13 23:25:42', NULL),
(9, 14, 1, 20, 'Used', -10.00, '2018-07-15 23:38:02', '2018-07-15 23:47:36', NULL),
(10, 14, 1, 61, 'dfads', 10.00, '2018-07-20 05:34:40', '2018-07-25 22:35:56', '2018-07-25 22:35:56'),
(11, 14, 1, 61, '34', 12.00, '2018-07-20 05:45:16', '2018-07-25 22:35:56', '2018-07-25 22:35:56');

-- --------------------------------------------------------

--
-- Table structure for table `styles`
--

DROP TABLE IF EXISTS `styles`;
CREATE TABLE IF NOT EXISTS `styles` (
  `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT,
  `name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `sub_category_id` int(11) NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=10 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `styles`
--

INSERT INTO `styles` (`id`, `name`, `sub_category_id`, `created_at`, `updated_at`, `deleted_at`) VALUES
(9, 'asdf2', 10, '2018-08-27 23:08:04', '2018-08-27 23:13:33', NULL);

-- --------------------------------------------------------

--
-- Table structure for table `users`
--

DROP TABLE IF EXISTS `users`;
CREATE TABLE IF NOT EXISTS `users` (
  `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT,
  `first_name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `last_name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `email` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `user_id` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `password` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `role` int(11) NOT NULL,
  `active` tinyint(1) NOT NULL DEFAULT '1',
  `vendor_meta_id` int(11) DEFAULT NULL,
  `buyer_meta_id` int(11) DEFAULT NULL,
  `remember_token` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `reset_token` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `last_login` timestamp NULL DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `users_email_unique` (`email`)
) ENGINE=MyISAM AUTO_INCREMENT=25 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `users`
--

INSERT INTO `users` (`id`, `first_name`, `last_name`, `email`, `user_id`, `password`, `role`, `active`, `vendor_meta_id`, `buyer_meta_id`, `remember_token`, `reset_token`, `last_login`, `created_at`, `updated_at`, `deleted_at`) VALUES
(1, 'Mr.', 'ss', 'shanto.razer@gmail.com', 'vendor', '$2y$10$Uumgr3En9yFT5fEUBw4OFeulreuynvKWNP2SnuYI2ic2vU8WpUFaG', 2, 1, 1, NULL, 'BG7gPtQlIYDe2KjGJsZB1H4LmvJZjUWbyYcYwqQHoHOcdvDSUxw1sMBgFgOn', NULL, '2019-01-07 23:18:52', '2018-05-08 09:03:20', '2019-01-07 23:18:52', NULL),
(2, 'Super', 'Admin', 'admin@gmail.com', NULL, '$2y$10$Q1pYXTCr.5Dxasr4byZR5uTxs3x8aFXDpPL7CX/r.6mHC1muwBw9m', 1, 1, NULL, NULL, 'vHlArtUOzK7BvHGe5gvYACxYn4dV3Vz4Or9hMzDZpeTRW5aD1JSxkUkwohrM', NULL, NULL, '2018-05-10 05:41:54', '2018-05-10 05:41:54', NULL),
(3, 'dd3ye', 'dder', NULL, '4352', '$2y$10$XDxkIkQ8yMarz5yjGxqIXOWzBx540aGXl10BOsViq9reiQw0Bq7iO', 4, 1, 1, NULL, NULL, NULL, NULL, '2018-05-15 08:10:43', '2018-06-28 13:01:12', NULL),
(11, 'asdf', 'cvcc', NULL, 'sdf', '$2y$10$Uumgr3En9yFT5fEUBw4OFeulreuynvKWNP2SnuYI2ic2vU8WpUFaG', 4, 0, 1, NULL, 'WlDJZsJhhLfHomBfixiMC7elJi4B2cvQGaYCwIsbYuQSU4k6IPcI1luyGIal', NULL, '2018-07-07 04:40:37', '2018-05-15 11:15:15', '2018-07-07 04:40:37', NULL),
(20, 'Buyer', 'with file', 'buyerfile@gmail.com-deleted', NULL, '$2y$10$zwRcndFry0lbWbGf8FPswe8054xXDzP/tl.9JYgIFN3V3ETDinstW', 3, 1, NULL, 5, NULL, NULL, NULL, '2018-06-30 06:30:24', '2018-07-22 22:48:58', '2018-07-22 22:48:58'),
(13, 'Yellow', 'Ltd.', 'yellow@gmail.com', 'yellow', '$2y$10$W95EC16sCnMZu1ZAJP7VAejKiEHzdLzgDW8PD3xSF4lSSwwdfwEmm', 2, 1, 2, NULL, 'w8Gszu4mSoWIuor0m5DDW7wFlNouORLqbw0W9vhpCFNF05EViY2KvZXIU8LB', NULL, '2018-07-30 20:05:04', '2018-05-23 15:16:41', '2018-07-30 20:05:04', NULL),
(14, 'First', 'Customer', 'shantotrs@gmail.com', NULL, '$2y$10$jt59OUMWN5WUTZN7WvkpBu2qvbdbcrejruPtY9U9zBqjcGeAkgIki', 3, 1, NULL, 3, 'jJkcxiImFLUhvmjgUiHYTmf04M4ZJgShwnBrU5M0SaZob0N8TwRSmBmIY4q1', NULL, '2018-11-23 19:30:25', '2018-05-24 10:22:35', '2018-11-23 19:30:25', NULL),
(15, 'asdf', 'asdf', 'ssdfsdaf@fads.com', 'asdf', '$2y$10$4paU7TTMgWH67HCz8J.a0uo9rqCszhdOvrKobPgImBDS7BKY6bwdC', 2, 1, 6, NULL, NULL, NULL, NULL, '2018-05-25 11:07:52', '2018-05-25 11:07:52', NULL),
(16, 'sdfg', 'dfgs', 'asdfa@dfads.com', 'sdfg', '$2y$10$7Rm3sFOwbpfRZ5KeBxL2VOaanYEnELtW4FL8R6ywwVgi7cCXnbRPy', 2, 1, 7, NULL, NULL, NULL, NULL, '2018-06-09 04:41:13', '2018-06-09 04:41:13', NULL),
(17, 'Cat', 'Eye', 'cats@gmail.com', 'cats', '$2y$10$116MosSjPUnP9TT.RfDH6OfZqiapTygRikttal1Skobm5AuBw7EpO', 2, 1, 8, NULL, NULL, NULL, NULL, '2018-06-12 13:37:00', '2018-06-12 13:37:00', NULL),
(18, 'asdfa', 'asdf', 'asdfasdfd@dfsf.com', 'admin@gmail.com', '$2y$10$HN7O/PIb796HVtPOr1tsiOHQvKLyhAMew7kBlEFd1Ta9YjcINGeRu', 2, 1, 9, NULL, NULL, NULL, NULL, '2018-06-22 02:14:14', '2018-06-22 02:14:14', NULL),
(19, 'New', 'buyer', 'newbuyer@gmail.com', NULL, '$2y$10$2YyXpCQy32JDLcWWEzV1NeqZjGGgnwHTwx24teL53ipi1N13uZlvS', 3, 1, NULL, 4, 'YtOhiVFePzX8u6Vk9Goshor0iyVsfkxYqAhHMBkyc6FzF5z8cgJzza83ffx2', NULL, '2018-08-07 20:40:22', '2018-06-22 07:18:27', '2018-08-07 20:40:22', NULL),
(21, 'adsf', 'asdf', 'primarycustomer@gmail.com', 'primarycustomer', '$2y$10$vEQ1gb3jp1BjrY1lRy5FDuY8ItQeULTMRD64NScrEWNqkBGcTVSLy', 2, 1, 10, NULL, NULL, NULL, NULL, '2018-07-10 11:52:04', '2018-07-10 11:52:04', NULL),
(22, 'adf', 'asdf', 'buyer2@gmail.com', NULL, '$2y$10$V/QwbtBEGmKhuM5/gWnLI.G52ibkK.oPOMAjmkricMOKRbjRz7KS.', 3, 1, NULL, 6, 'WloKtv9QXKY9DdEnahjG7bAkZWcsiEiF3Y4xuxHtMIWRGex5UjtzAzVSNmi0', NULL, '2018-07-30 19:39:54', '2018-07-17 01:10:13', '2018-07-30 19:39:54', NULL),
(23, 'Super', 'Admin', 'info@stylepick.net', NULL, '$2y$10$/3luni.JhwQ6fWXaMF2fn.J7gKYlTDc/GefX6Qb026/8wMfIwKZ6G', 1, 1, NULL, NULL, NULL, NULL, NULL, '2018-07-18 03:28:58', '2018-07-18 03:28:58', NULL),
(24, 'Super', 'Admin', 'info@whatef.com', NULL, '$2y$10$Hu0eTWR3bE3TYpzTBdNQ9eYA6ozeah6D5Z4P0H1t6cqS4eNtUjvCS', 1, 1, NULL, NULL, 'E1XtAS8s7zjIb4dkztDDwcISglaH3XdgfnEz7XschVUzcwK3WZjCx41rZnkE', NULL, NULL, '2018-07-18 03:28:58', '2018-07-18 03:28:58', NULL);

-- --------------------------------------------------------

--
-- Table structure for table `user_permission`
--

DROP TABLE IF EXISTS `user_permission`;
CREATE TABLE IF NOT EXISTS `user_permission` (
  `user_id` int(11) NOT NULL,
  `permission` int(11) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `user_permission`
--

INSERT INTO `user_permission` (`user_id`, `permission`) VALUES
(9, 3),
(9, 4),
(10, 2),
(10, 6),
(11, 10),
(11, 4),
(11, 3);

-- --------------------------------------------------------

--
-- Table structure for table `vendor_images`
--

DROP TABLE IF EXISTS `vendor_images`;
CREATE TABLE IF NOT EXISTS `vendor_images` (
  `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT,
  `vendor_meta_id` int(11) NOT NULL,
  `type` int(11) NOT NULL,
  `status` tinyint(1) NOT NULL,
  `image_path` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=20 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `vendor_images`
--

INSERT INTO `vendor_images` (`id`, `vendor_meta_id`, `type`, `status`, `image_path`, `created_at`, `updated_at`) VALUES
(2, 1, 1, 1, '/images/vendor/2ea88970-6293-11e8-a08f-439586466189.jpg', '2018-05-28 10:21:31', '2018-06-25 09:45:44'),
(15, 1, 4, 1, '/images/vendor/026c0d50-7aba-11e8-88fb-5fba2dec8265.jpg', '2018-06-28 03:59:55', '2018-06-28 04:00:13'),
(4, 1, 2, 0, '/images/vendor/452bb710-64a0-11e8-a8ac-25400377cade.jpg', '2018-05-31 01:00:15', '2018-06-02 13:40:59'),
(5, 1, 6, 1, '/images/vendor/fff17d20-65c1-11e8-81b0-af06f4b6d1c5.jpg', '2018-06-01 11:34:13', '2018-06-01 11:34:16'),
(6, 2, 6, 1, '/images/vendor/273bfc30-65c2-11e8-baf0-5bee5fcfc06d.jpg', '2018-06-01 11:35:19', '2018-06-01 11:35:20'),
(7, 1, 4, 0, '/images/vendor/e99cce10-661a-11e8-8e85-4972bd52967e.jpg', '2018-06-01 22:10:40', '2018-06-28 04:00:13'),
(8, 1, 3, 1, '/images/vendor/12f24d00-6625-11e8-9282-a7d80f96ea33.jpg', '2018-06-01 23:23:25', '2018-06-01 23:23:28'),
(9, 2, 3, 1, '/images/vendor/5f9a5960-6626-11e8-ae32-0babdb16beab.jpg', '2018-06-01 23:32:43', '2018-06-01 23:32:44'),
(10, 1, 2, 1, '/images/vendor/de19b050-669c-11e8-9d83-4f960a6d539e.jpg', '2018-06-02 13:40:56', '2018-06-02 13:40:59'),
(11, 2, 2, 1, '/images/vendor/ee58eee0-669c-11e8-baac-273876dfcd85.jpg', '2018-06-02 13:41:23', '2018-06-02 13:41:25'),
(12, 1, 5, 1, '/images/vendor/9d292790-67d5-11e8-8ecf-315a04d88a8a.jpg', '2018-06-04 02:59:39', '2018-06-04 03:00:00'),
(13, 2, 5, 1, '/images/vendor/d9533ca0-67d5-11e8-bd46-1f81aab239a4.jpg', '2018-06-04 03:01:20', '2018-06-04 03:01:22'),
(14, 1, 1, 0, '/images/vendor/cf88c950-788e-11e8-aea6-ed38c287150b.jpg', '2018-06-25 09:45:39', '2018-06-25 09:45:44'),
(16, 1, 1, 0, '/images/vendor/aafd92c0-7b00-11e8-b0fa-f99caf520ef5.jpg', '2018-06-28 12:25:43', '2018-06-28 12:25:43'),
(17, 1, 1, 0, '/images/vendor/ae6f9860-7b00-11e8-aefc-07f95e9f4ff6.jpg', '2018-06-28 12:25:49', '2018-06-28 12:25:49'),
(18, 2, 4, 1, '/images/vendor/6fa7e810-7ded-11e8-a43d-51a138b2083d.jpg', '2018-07-02 05:45:36', '2018-07-02 05:45:40'),
(19, 1, 7, 1, '/images/vendor/9b414810-7fb7-11e8-95e0-17e32655a017.jpg', '2018-07-04 12:25:19', '2018-07-04 12:25:28');

-- --------------------------------------------------------

--
-- Table structure for table `visitors`
--

DROP TABLE IF EXISTS `visitors`;
CREATE TABLE IF NOT EXISTS `visitors` (
  `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT,
  `user_id` int(11) DEFAULT NULL,
  `vendor_meta_id` int(11) DEFAULT NULL,
  `route` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `url` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `ip` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=768 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `visitors`
--

INSERT INTO `visitors` (`id`, `user_id`, `vendor_meta_id`, `route`, `url`, `ip`, `created_at`, `updated_at`) VALUES
(1, NULL, NULL, NULL, 'http://localhost/wholesalepeople/public/vendor/home/1', '::1', '2018-06-05 03:09:06', '2018-06-05 03:09:06'),
(2, NULL, NULL, NULL, 'http://localhost/wholesalepeople/public/vendor/home/1', '::1', '2018-06-05 03:09:50', '2018-06-05 03:09:50'),
(3, NULL, NULL, NULL, 'http://localhost/wholesalepeople/public/vendor/home/1', '192:12:1', '2018-06-05 03:09:57', '2018-06-05 03:09:57'),
(4, NULL, NULL, NULL, 'http://localhost/wholesalepeople/public/vendor/home/2', '3', '2018-06-08 10:31:36', '2018-06-08 10:31:36'),
(5, NULL, NULL, NULL, 'http://localhost/wholesalepeople/public/vendor/home/1', '3', '2018-06-08 11:35:39', '2018-06-08 11:35:39'),
(6, NULL, NULL, NULL, 'http://localhost/wholesalepeople/public/vendor/home/2', '3', '2018-06-08 11:35:42', '2018-06-08 11:35:42'),
(7, NULL, NULL, NULL, 'http://localhost/wholesalepeople/public/vendor/home/1', '3', '2018-06-09 02:01:51', '2018-06-09 02:01:51'),
(8, NULL, NULL, NULL, 'http://localhost/wholesalepeople/public/vendor/home/1', '3', '2018-06-09 12:17:25', '2018-06-09 12:17:25'),
(9, NULL, NULL, NULL, 'http://localhost/wholesalepeople/public/vendor/home/1', '3', '2018-06-09 12:17:37', '2018-06-09 12:17:37'),
(10, NULL, NULL, NULL, 'http://localhost/wholesalepeople/public/vendor/home/1', '3', '2018-06-09 12:19:01', '2018-06-09 12:19:01'),
(11, NULL, NULL, NULL, 'http://localhost/wholesalepeople/public/vendor/home/1', '3', '2018-06-09 12:20:01', '2018-06-09 12:20:01'),
(12, NULL, NULL, NULL, 'http://localhost/wholesalepeople/public/vendor/home/1', '3', '2018-06-09 12:33:35', '2018-06-09 12:33:35'),
(13, NULL, NULL, NULL, 'http://localhost/wholesalepeople/public/vendor/home/1', '3', '2018-06-09 12:33:56', '2018-06-09 12:33:56'),
(14, NULL, NULL, NULL, 'http://localhost/wholesalepeople/public/vendor/home/1', '3', '2018-06-09 12:54:55', '2018-06-09 12:54:55'),
(15, NULL, NULL, NULL, 'http://localhost/wholesalepeople/public/vendor/home/1', '3', '2018-06-09 12:56:28', '2018-06-09 12:56:28'),
(16, NULL, NULL, NULL, 'http://localhost/wholesalepeople/public/vendor/home/1', '3', '2018-06-09 12:56:43', '2018-06-09 12:56:43'),
(17, NULL, NULL, NULL, 'http://localhost/wholesalepeople/public/vendor/home/1', '3', '2018-06-09 12:58:40', '2018-06-09 12:58:40'),
(18, NULL, NULL, NULL, 'http://localhost/wholesalepeople/public/vendor/home/1', '3', '2018-06-10 02:04:04', '2018-06-10 02:04:04'),
(19, NULL, NULL, NULL, 'http://localhost/wholesalepeople/public/vendor/home/1', '3', '2018-06-10 04:20:33', '2018-06-10 04:20:33'),
(20, NULL, NULL, NULL, 'http://localhost/wholesalepeople/public/vendor/home/1', '3', '2018-06-10 04:22:04', '2018-06-10 04:22:04'),
(21, NULL, NULL, NULL, 'http://localhost/wholesalepeople/public/vendor/home/1', '3', '2018-06-10 04:22:18', '2018-06-10 04:22:18'),
(22, NULL, NULL, NULL, 'http://localhost/wholesalepeople/public/vendor/home/1', '3', '2018-06-10 04:22:58', '2018-06-10 04:22:58'),
(23, NULL, NULL, NULL, 'http://localhost/wholesalepeople/public/vendor/home/1', '3', '2018-06-10 04:22:59', '2018-06-10 04:22:59'),
(24, NULL, NULL, NULL, 'http://localhost/wholesalepeople/public/vendor/home/1', '3', '2018-06-10 04:23:08', '2018-06-10 04:23:08'),
(25, NULL, NULL, NULL, 'http://localhost/wholesalepeople/public/vendor/home/1', '3', '2018-06-10 04:23:18', '2018-06-10 04:23:18'),
(26, NULL, NULL, NULL, 'http://localhost/wholesalepeople/public/vendor/home/1', '3', '2018-06-10 04:23:39', '2018-06-10 04:23:39'),
(27, NULL, NULL, NULL, 'http://localhost/wholesalepeople/public/vendor/home/1', '3', '2018-06-11 02:54:35', '2018-06-11 02:54:35'),
(28, NULL, NULL, NULL, 'http://localhost/wholesalepeople/public/vendor/home/2', '3', '2018-06-11 02:54:38', '2018-06-11 02:54:38'),
(29, NULL, NULL, NULL, 'http://localhost/wholesalepeople/public/vendor/home/1', '3', '2018-06-11 02:54:41', '2018-06-11 02:54:41'),
(30, NULL, NULL, NULL, 'http://localhost/wholesalepeople/public/vendor/home/2', '3', '2018-06-11 02:54:44', '2018-06-11 02:54:44'),
(31, NULL, NULL, NULL, 'http://localhost/wholesalepeople/public/vendor/home/1', '3', '2018-06-11 02:54:47', '2018-06-11 02:54:47'),
(32, NULL, NULL, NULL, 'http://localhost/wholesalepeople/public/vendor/home/7', '3', '2018-06-11 03:10:50', '2018-06-11 03:10:50'),
(33, NULL, NULL, NULL, 'http://localhost/wholesalepeople/public/vendor/home/7', '3', '2018-06-11 03:11:38', '2018-06-11 03:11:38'),
(34, NULL, NULL, NULL, 'http://localhost/wholesalepeople/public/vendor/home/7', '3', '2018-06-11 03:12:00', '2018-06-11 03:12:00'),
(35, NULL, NULL, NULL, 'http://localhost/wholesalepeople/public/vendor/home/7', '3', '2018-06-11 03:13:15', '2018-06-11 03:13:15'),
(36, NULL, NULL, NULL, 'http://localhost/wholesalepeople/public/vendor/home/1', '3', '2018-06-11 11:22:47', '2018-06-11 11:22:47'),
(37, NULL, NULL, NULL, 'http://localhost/wholesalepeople/public/vendor/home/1', '3', '2018-06-12 02:38:32', '2018-06-12 02:38:32'),
(38, NULL, NULL, NULL, 'http://localhost/wholesalepeople/public/vendor/home/1', '3', '2018-06-12 10:52:16', '2018-06-12 10:52:16'),
(39, NULL, NULL, NULL, 'http://localhost/wholesalepeople/public/vendor/home/1', '3', '2018-06-12 11:06:34', '2018-06-12 11:06:34'),
(40, NULL, NULL, NULL, 'http://localhost/wholesalepeople/public/vendor/home/1', '3', '2018-06-12 11:10:20', '2018-06-12 11:10:20'),
(41, NULL, NULL, NULL, 'http://localhost/wholesalepeople/public/vendor/home/1', '3', '2018-06-12 11:10:42', '2018-06-12 11:10:42'),
(42, NULL, NULL, NULL, 'http://localhost/wholesalepeople/public/vendor/home/1', '3', '2018-06-12 11:10:57', '2018-06-12 11:10:57'),
(43, NULL, NULL, NULL, 'http://localhost/wholesalepeople/public/vendor/home/1', '3', '2018-06-12 11:11:04', '2018-06-12 11:11:04'),
(44, NULL, NULL, NULL, 'http://localhost/wholesalepeople/public/vendor/home/1', '3', '2018-06-12 11:11:34', '2018-06-12 11:11:34'),
(45, NULL, NULL, NULL, 'http://localhost/wholesalepeople/public/vendor/home/1', '3', '2018-06-12 11:11:35', '2018-06-12 11:11:35'),
(46, NULL, NULL, NULL, 'http://localhost/wholesalepeople/public/vendor/home/1', '3', '2018-06-12 11:12:01', '2018-06-12 11:12:01'),
(47, NULL, NULL, NULL, 'http://localhost/wholesalepeople/public/vendor/home/1', '3', '2018-06-12 11:12:03', '2018-06-12 11:12:03'),
(48, NULL, NULL, NULL, 'http://localhost/wholesalepeople/public/vendor/home/1', '3', '2018-06-12 11:12:06', '2018-06-12 11:12:06'),
(49, NULL, NULL, NULL, 'http://localhost/wholesalepeople/public/vendor/home/1', '3', '2018-06-12 11:17:46', '2018-06-12 11:17:46'),
(50, NULL, NULL, NULL, 'http://localhost/wholesalepeople/public/vendor/home/1', '3', '2018-06-13 11:20:58', '2018-06-13 11:20:58'),
(51, NULL, NULL, NULL, 'http://localhost/wholesalepeople/public/vendor/home/1', '3', '2018-06-13 11:21:21', '2018-06-13 11:21:21'),
(52, NULL, NULL, NULL, 'http://localhost/wholesalepeople/public/vendor/home/1', '3', '2018-06-13 11:21:43', '2018-06-13 11:21:43'),
(53, NULL, NULL, NULL, 'http://localhost/wholesalepeople/public/vendor/home/1', '3', '2018-06-13 11:21:50', '2018-06-13 11:21:50'),
(54, NULL, NULL, NULL, 'http://localhost/wholesalepeople/public/vendor/home/1', '3', '2018-06-13 11:21:52', '2018-06-13 11:21:52'),
(55, NULL, NULL, NULL, 'http://localhost/wholesalepeople/public/vendor/home/1', '3', '2018-06-13 13:51:00', '2018-06-13 13:51:00'),
(56, NULL, NULL, NULL, 'http://localhost/wholesalepeople/public/vendor/home/1', '3', '2018-06-13 14:18:16', '2018-06-13 14:18:16'),
(57, NULL, NULL, NULL, 'http://localhost/wholesalepeople/public/vendor/home/2', '3', '2018-06-13 14:18:24', '2018-06-13 14:18:24'),
(58, NULL, NULL, NULL, 'http://localhost/wholesalepeople/public/vendor/home/1', '3', '2018-06-13 14:18:27', '2018-06-13 14:18:27'),
(59, NULL, NULL, NULL, 'http://localhost/wholesalepeople/public/vendor/home/2', '3', '2018-06-13 14:18:30', '2018-06-13 14:18:30'),
(60, NULL, NULL, NULL, 'http://localhost/wholesalepeople/public/vendor/home/1', '3', '2018-06-23 05:20:59', '2018-06-23 05:20:59'),
(61, NULL, NULL, NULL, 'http://localhost/wholesalepeople/public/vendor/home/1', '3', '2018-06-23 07:45:10', '2018-06-23 07:45:10'),
(62, NULL, NULL, NULL, 'http://localhost/wholesalepeople/public/vendor/home/1', '3', '2018-06-23 07:45:16', '2018-06-23 07:45:16'),
(63, NULL, NULL, NULL, 'http://localhost/wholesalepeople/public/vendor/home/1', '3', '2018-06-23 07:46:50', '2018-06-23 07:46:50'),
(64, NULL, NULL, NULL, 'http://localhost/wholesalepeople/public/vendor/home/1', '3', '2018-06-23 07:47:42', '2018-06-23 07:47:42'),
(65, NULL, NULL, NULL, 'http://localhost/wholesalepeople/public/vendor/home/1', '3', '2018-06-23 07:47:43', '2018-06-23 07:47:43'),
(66, NULL, NULL, NULL, 'http://localhost/wholesalepeople/public/vendor/home/1', '3', '2018-06-23 07:49:18', '2018-06-23 07:49:18'),
(67, NULL, NULL, NULL, 'http://localhost/wholesalepeople/public/vendor/home/1', '3', '2018-06-23 07:49:49', '2018-06-23 07:49:49'),
(68, NULL, NULL, NULL, 'http://localhost/wholesalepeople/public/vendor/home/1', '3', '2018-06-23 07:51:20', '2018-06-23 07:51:20'),
(69, NULL, NULL, NULL, 'http://localhost/wholesalepeople/public/vendor/home/1', '3', '2018-06-23 07:53:40', '2018-06-23 07:53:40'),
(70, NULL, NULL, NULL, 'http://localhost/wholesalepeople/public/vendor/home/1', '3', '2018-06-23 11:11:30', '2018-06-23 11:11:30'),
(71, NULL, NULL, NULL, 'http://localhost/wholesalepeople/public/vendor/home/1', '3', '2018-06-23 11:12:38', '2018-06-23 11:12:38'),
(72, NULL, NULL, NULL, 'http://localhost/wholesalepeople/public/vendor/home/1', '3', '2018-06-23 11:12:47', '2018-06-23 11:12:47'),
(73, NULL, NULL, NULL, 'http://localhost/wholesalepeople/public/vendor/home/2', '3', '2018-06-23 11:40:49', '2018-06-23 11:40:49'),
(74, 14, NULL, NULL, 'http://localhost/wholesalepeople/public/vendor/home/1', '3', '2018-06-26 04:18:28', '2018-06-26 04:18:28'),
(75, NULL, NULL, NULL, 'http://localhost/wholesalepeople/public/vendor/home/1', '3', '2018-06-27 03:02:56', '2018-06-27 03:02:56'),
(76, NULL, NULL, NULL, 'http://localhost/wholesalepeople/public/vendor/home/1', '3', '2018-06-27 04:45:10', '2018-06-27 04:45:10'),
(77, NULL, NULL, NULL, 'http://localhost/wholesalepeople/public/vendor/home/1', '3', '2018-06-27 11:29:16', '2018-06-27 11:29:16'),
(78, NULL, NULL, NULL, 'http://localhost/wholesalepeople/public/vendor/home/2', '3', '2018-06-27 11:29:26', '2018-06-27 11:29:26'),
(79, 14, NULL, NULL, 'http://localhost/wholesalepeople/public/vendor/home/1', '3', '2018-06-27 12:08:07', '2018-06-27 12:08:07'),
(80, 14, NULL, NULL, 'http://localhost/wholesalepeople/public/vendor/home/1', '3', '2018-06-27 12:08:10', '2018-06-27 12:08:10'),
(81, NULL, NULL, NULL, 'http://localhost/wholesalepeople/public/vendor/home/1', '3', '2018-06-28 03:38:21', '2018-06-28 03:38:21'),
(82, NULL, NULL, NULL, 'http://localhost/wholesalepeople/public/vendor/home/1', '3', '2018-06-28 03:38:26', '2018-06-28 03:38:26'),
(83, NULL, NULL, NULL, 'http://localhost/wholesalepeople/public/vendor/home/2', '3', '2018-06-28 03:41:02', '2018-06-28 03:41:02'),
(84, NULL, NULL, NULL, 'http://localhost/wholesalepeople/public/vendor/home/2', '3', '2018-06-28 03:41:09', '2018-06-28 03:41:09'),
(85, NULL, NULL, NULL, 'http://localhost/wholesalepeople/public/vendor/home/2', '3', '2018-06-28 03:41:10', '2018-06-28 03:41:10'),
(86, NULL, NULL, NULL, 'http://localhost/wholesalepeople/public/vendor/home/1', '3', '2018-06-28 03:41:31', '2018-06-28 03:41:31'),
(87, NULL, NULL, NULL, 'http://localhost/wholesalepeople/public/vendor/home/2', '3', '2018-06-28 03:41:35', '2018-06-28 03:41:35'),
(88, NULL, NULL, NULL, 'http://localhost/wholesalepeople/public/vendor/home/1', '3', '2018-06-28 03:41:39', '2018-06-28 03:41:39'),
(89, NULL, NULL, NULL, 'http://localhost/wholesalepeople/public/vendor/home/1', '3', '2018-06-28 03:41:58', '2018-06-28 03:41:58'),
(90, NULL, NULL, NULL, 'http://localhost/wholesalepeople/public/vendor/home/1', '3', '2018-06-28 03:42:01', '2018-06-28 03:42:01'),
(91, NULL, NULL, NULL, 'http://localhost/wholesalepeople/public/vendor/home/1', '3', '2018-06-28 04:09:31', '2018-06-28 04:09:31'),
(92, 14, NULL, NULL, 'http://localhost/wholesalepeople/public/vendor/home/1', '3', '2018-06-28 06:42:19', '2018-06-28 06:42:19'),
(93, NULL, NULL, NULL, 'http://localhost/wholesalepeople/public/vendor/home/1', '3', '2018-06-28 12:35:06', '2018-06-28 12:35:06'),
(94, NULL, NULL, NULL, 'http://localhost/wholesalepeople/public/vendor/home/1', '3', '2018-06-29 10:45:45', '2018-06-29 10:45:45'),
(95, NULL, NULL, NULL, 'http://localhost/wholesalepeople/public/vendor/home/7', '3', '2018-06-29 10:52:13', '2018-06-29 10:52:13'),
(96, NULL, NULL, NULL, 'http://localhost/wholesalepeople/public/vendor/home/2', '3', '2018-06-29 10:52:19', '2018-06-29 10:52:19'),
(97, 14, NULL, NULL, 'http://localhost/wholesalepeople/public/vendor/home/7', '3', '2018-06-29 11:32:35', '2018-06-29 11:32:35'),
(98, 14, NULL, NULL, 'http://localhost/wholesalepeople/public/vendor/home/2', '3', '2018-06-29 11:34:28', '2018-06-29 11:34:28'),
(99, 14, NULL, NULL, 'http://localhost/wholesalepeople/public/vendor/home/7', '3', '2018-06-29 12:04:06', '2018-06-29 12:04:06'),
(100, 14, NULL, NULL, 'http://localhost/wholesalepeople/public/vendor/home/1', '3', '2018-06-29 12:05:00', '2018-06-29 12:05:00'),
(101, 14, NULL, NULL, 'http://localhost/wholesalepeople/public/vendor/home/1', '3', '2018-06-29 12:06:36', '2018-06-29 12:06:36'),
(102, 14, NULL, NULL, 'http://localhost/wholesalepeople/public/vendor/home/1', '3', '2018-06-29 12:09:16', '2018-06-29 12:09:16'),
(103, 14, NULL, NULL, 'http://localhost/wholesalepeople/public/vendor/home/1', '3', '2018-06-29 12:09:53', '2018-06-29 12:09:53'),
(104, 14, NULL, NULL, 'http://localhost/wholesalepeople/public/vendor/home/1', '3', '2018-06-29 12:10:48', '2018-06-29 12:10:48'),
(105, 14, NULL, NULL, 'http://localhost/wholesalepeople/public/vendor/home/1', '3', '2018-06-29 12:12:01', '2018-06-29 12:12:01'),
(106, 14, NULL, NULL, 'http://localhost/wholesalepeople/public/vendor/home/1', '3', '2018-06-29 12:13:51', '2018-06-29 12:13:51'),
(107, 14, NULL, NULL, 'http://localhost/wholesalepeople/public/vendor/home/1', '3', '2018-06-29 12:14:21', '2018-06-29 12:14:21'),
(108, 14, NULL, NULL, 'http://localhost/wholesalepeople/public/vendor/home/1', '3', '2018-06-29 12:14:56', '2018-06-29 12:14:56'),
(109, 14, NULL, NULL, 'http://localhost/wholesalepeople/public/vendor/home/2', '3', '2018-06-30 02:33:43', '2018-06-30 02:33:43'),
(110, 14, NULL, NULL, 'http://localhost/wholesalepeople/public/vendor/home/1', '3', '2018-06-30 03:13:23', '2018-06-30 03:13:23'),
(111, 14, NULL, NULL, 'http://localhost/wholesalepeople/public/vendor/home/1', '3', '2018-06-30 03:13:48', '2018-06-30 03:13:48'),
(112, 19, NULL, NULL, 'http://localhost/wholesalepeople/public/vendor/home/1', '3', '2018-06-30 05:24:31', '2018-06-30 05:24:31'),
(113, 14, NULL, NULL, 'http://localhost/wholesalepeople/public/vendor/home/1', '3', '2018-07-02 02:31:52', '2018-07-02 02:31:52'),
(114, 14, NULL, NULL, 'http://localhost/wholesalepeople/public/vendor/home/1', '3', '2018-07-02 02:32:10', '2018-07-02 02:32:10'),
(115, 14, NULL, NULL, 'http://localhost/wholesalepeople/public/vendor/home/1', '3', '2018-07-02 02:34:14', '2018-07-02 02:34:14'),
(116, 14, NULL, NULL, 'http://localhost/wholesalepeople/public/vendor/home/1', '3', '2018-07-02 02:34:33', '2018-07-02 02:34:33'),
(117, 14, NULL, NULL, 'http://localhost/wholesalepeople/public/vendor/home/1', '3', '2018-07-02 02:34:54', '2018-07-02 02:34:54'),
(118, 14, NULL, NULL, 'http://localhost/wholesalepeople/public/vendor/home/1', '3', '2018-07-02 02:35:25', '2018-07-02 02:35:25'),
(119, 14, NULL, NULL, 'http://localhost/wholesalepeople/public/vendor/home/1', '3', '2018-07-02 02:35:32', '2018-07-02 02:35:32'),
(120, 14, NULL, NULL, 'http://localhost/wholesalepeople/public/vendor/home/1', '3', '2018-07-02 02:40:22', '2018-07-02 02:40:22'),
(121, 14, NULL, NULL, 'http://localhost/wholesalepeople/public/vendor/home/2', '3', '2018-07-02 02:40:27', '2018-07-02 02:40:27'),
(122, 14, NULL, NULL, 'http://localhost/wholesalepeople/public/vendor/home/2', '3', '2018-07-02 02:40:46', '2018-07-02 02:40:46'),
(123, 14, NULL, NULL, 'http://localhost/wholesalepeople/public/vendor/home/1', '3', '2018-07-02 02:42:51', '2018-07-02 02:42:51'),
(124, 14, NULL, NULL, 'http://localhost/wholesalepeople/public/vendor/home/1', '3', '2018-07-02 02:52:11', '2018-07-02 02:52:11'),
(125, 14, NULL, NULL, 'http://localhost/wholesalepeople/public/vendor/home/1', '3', '2018-07-02 02:52:59', '2018-07-02 02:52:59'),
(126, 14, NULL, NULL, 'http://localhost/wholesalepeople/public/vendor/home/1', '3', '2018-07-02 02:53:04', '2018-07-02 02:53:04'),
(127, 14, NULL, NULL, 'http://localhost/wholesalepeople/public/vendor/home/1', '3', '2018-07-02 03:34:40', '2018-07-02 03:34:40'),
(128, NULL, NULL, NULL, 'http://localhost/wholesalepeople/public/vendor/home/1', '3', '2018-07-02 04:02:16', '2018-07-02 04:02:16'),
(129, 14, NULL, NULL, 'http://localhost/wholesalepeople/public/vendor/home/1', '3', '2018-07-03 07:49:03', '2018-07-03 07:49:03'),
(130, 14, NULL, NULL, 'http://localhost/wholesalepeople/public/vendor/home/1', '3', '2018-07-03 07:53:37', '2018-07-03 07:53:37'),
(131, 14, NULL, NULL, 'http://localhost/wholesalepeople/public/vendor/home/1', '3', '2018-07-03 07:55:24', '2018-07-03 07:55:24'),
(132, 14, NULL, NULL, 'http://localhost/wholesalepeople/public/vendor/home/1', '3', '2018-07-03 07:55:44', '2018-07-03 07:55:44'),
(133, 14, NULL, NULL, 'http://localhost/wholesalepeople/public/vendor/home/1', '3', '2018-07-03 07:56:01', '2018-07-03 07:56:01'),
(134, 14, NULL, NULL, 'http://localhost/wholesalepeople/public/vendor/home/1', '3', '2018-07-03 07:57:57', '2018-07-03 07:57:57'),
(135, 14, NULL, NULL, 'http://localhost/wholesalepeople/public/vendor/home/1', '3', '2018-07-03 07:58:28', '2018-07-03 07:58:28'),
(136, 14, NULL, NULL, 'http://localhost/wholesalepeople/public/vendor/home/1', '3', '2018-07-03 07:59:07', '2018-07-03 07:59:07'),
(137, 14, NULL, NULL, 'http://localhost/wholesalepeople/public/vendor/home/1', '3', '2018-07-03 07:59:12', '2018-07-03 07:59:12'),
(138, 14, NULL, NULL, 'http://localhost/wholesalepeople/public/vendor/home/1', '3', '2018-07-03 08:00:44', '2018-07-03 08:00:44'),
(139, 14, NULL, NULL, 'http://localhost/wholesalepeople/public/vendor/home/1', '3', '2018-07-03 08:00:59', '2018-07-03 08:00:59'),
(140, 14, NULL, NULL, 'http://localhost/wholesalepeople/public/vendor/home/1', '3', '2018-07-03 08:01:04', '2018-07-03 08:01:04'),
(141, 14, NULL, NULL, 'http://localhost/wholesalepeople/public/vendor/home/2', '3', '2018-07-03 08:01:10', '2018-07-03 08:01:10'),
(142, 14, NULL, NULL, 'http://localhost/wholesalepeople/public/vendor/home/7', '3', '2018-07-03 08:01:17', '2018-07-03 08:01:17'),
(143, 14, NULL, NULL, 'http://localhost/wholesalepeople/public/vendor/home/1', '3', '2018-07-03 08:01:22', '2018-07-03 08:01:22'),
(144, 14, NULL, NULL, 'http://localhost/wholesalepeople/public/vendor/home/1', '3', '2018-07-03 08:08:35', '2018-07-03 08:08:35'),
(145, 14, NULL, NULL, 'http://localhost/wholesalepeople/public/vendor/home/1', '3', '2018-07-03 08:08:56', '2018-07-03 08:08:56'),
(146, 14, NULL, NULL, 'http://localhost/wholesalepeople/public/vendor/home/1', '3', '2018-07-03 08:09:17', '2018-07-03 08:09:17'),
(147, 14, NULL, NULL, 'http://localhost/wholesalepeople/public/vendor/home/1', '3', '2018-07-03 08:10:14', '2018-07-03 08:10:14'),
(148, 14, NULL, NULL, 'http://localhost/wholesalepeople/public/vendor/home/1', '3', '2018-07-03 08:10:34', '2018-07-03 08:10:34'),
(149, 14, NULL, NULL, 'http://localhost/wholesalepeople/public/vendor/home/1', '3', '2018-07-03 08:11:44', '2018-07-03 08:11:44'),
(150, 14, NULL, NULL, 'http://localhost/wholesalepeople/public/vendor/home/1', '3', '2018-07-03 08:12:16', '2018-07-03 08:12:16'),
(151, 14, NULL, NULL, 'http://localhost/wholesalepeople/public/vendor/home/1', '3', '2018-07-03 08:13:07', '2018-07-03 08:13:07'),
(152, 14, NULL, NULL, 'http://localhost/wholesalepeople/public/vendor/home/1', '3', '2018-07-03 08:13:52', '2018-07-03 08:13:52'),
(153, 14, NULL, NULL, 'http://localhost/wholesalepeople/public/vendor/home/1', '3', '2018-07-03 08:17:24', '2018-07-03 08:17:24'),
(154, 14, NULL, NULL, 'http://localhost/wholesalepeople/public/vendor/home/1', '3', '2018-07-03 08:18:33', '2018-07-03 08:18:33'),
(155, 14, NULL, NULL, 'http://localhost/wholesalepeople/public/vendor/home/1', '3', '2018-07-03 08:19:07', '2018-07-03 08:19:07'),
(156, 14, NULL, NULL, 'http://localhost/wholesalepeople/public/vendor/home/1', '3', '2018-07-03 08:20:27', '2018-07-03 08:20:27'),
(157, 14, NULL, NULL, 'http://localhost/wholesalepeople/public/vendor/home/1', '3', '2018-07-03 08:21:34', '2018-07-03 08:21:34'),
(158, 14, NULL, NULL, 'http://localhost/wholesalepeople/public/vendor/home/1', '3', '2018-07-03 08:22:16', '2018-07-03 08:22:16'),
(159, 14, NULL, NULL, 'http://localhost/wholesalepeople/public/vendor/home/1', '3', '2018-07-03 08:23:36', '2018-07-03 08:23:36'),
(160, 14, NULL, NULL, 'http://localhost/wholesalepeople/public/vendor/home/1', '3', '2018-07-03 08:23:48', '2018-07-03 08:23:48'),
(161, 14, NULL, NULL, 'http://localhost/wholesalepeople/public/vendor/home/1', '3', '2018-07-03 08:25:20', '2018-07-03 08:25:20'),
(162, 14, NULL, NULL, 'http://localhost/wholesalepeople/public/vendor/home/1', '3', '2018-07-03 08:25:33', '2018-07-03 08:25:33'),
(163, 14, NULL, NULL, 'http://localhost/wholesalepeople/public/vendor/home/1', '3', '2018-07-03 08:26:02', '2018-07-03 08:26:02'),
(164, 14, NULL, NULL, 'http://localhost/wholesalepeople/public/vendor/home/1', '3', '2018-07-03 08:26:11', '2018-07-03 08:26:11'),
(165, 14, NULL, NULL, 'http://localhost/wholesalepeople/public/vendor/home/1', '3', '2018-07-03 08:26:35', '2018-07-03 08:26:35'),
(166, 14, NULL, NULL, 'http://localhost/wholesalepeople/public/vendor/home/1', '3', '2018-07-03 08:28:04', '2018-07-03 08:28:04'),
(167, 14, NULL, NULL, 'http://localhost/wholesalepeople/public/vendor/home/1', '3', '2018-07-03 08:28:15', '2018-07-03 08:28:15'),
(168, 14, NULL, NULL, 'http://localhost/wholesalepeople/public/vendor/home/1', '3', '2018-07-03 08:29:05', '2018-07-03 08:29:05'),
(169, 14, NULL, NULL, 'http://localhost/wholesalepeople/public/vendor/home/1', '3', '2018-07-03 08:32:56', '2018-07-03 08:32:56'),
(170, 14, NULL, NULL, 'http://localhost/wholesalepeople/public/vendor/home/1', '3', '2018-07-03 08:33:21', '2018-07-03 08:33:21'),
(171, 14, NULL, NULL, 'http://localhost/wholesalepeople/public/vendor/home/1', '3', '2018-07-03 08:34:28', '2018-07-03 08:34:28'),
(172, 14, NULL, NULL, 'http://localhost/wholesalepeople/public/vendor/home/1', '3', '2018-07-03 08:35:18', '2018-07-03 08:35:18'),
(173, 14, NULL, NULL, 'http://localhost/wholesalepeople/public/vendor/home/1', '3', '2018-07-03 08:36:31', '2018-07-03 08:36:31'),
(174, 14, NULL, NULL, 'http://localhost/wholesalepeople/public/vendor/home/1', '3', '2018-07-03 08:36:45', '2018-07-03 08:36:45'),
(175, 14, NULL, NULL, 'http://localhost/wholesalepeople/public/vendor/home/1', '3', '2018-07-03 08:37:12', '2018-07-03 08:37:12'),
(176, 14, NULL, NULL, 'http://localhost/wholesalepeople/public/vendor/home/1', '3', '2018-07-03 08:38:33', '2018-07-03 08:38:33'),
(177, 14, NULL, NULL, 'http://localhost/wholesalepeople/public/vendor/home/1', '3', '2018-07-03 08:38:49', '2018-07-03 08:38:49'),
(178, 14, NULL, NULL, 'http://localhost/wholesalepeople/public/vendor/home/1', '3', '2018-07-03 08:39:12', '2018-07-03 08:39:12'),
(179, 14, NULL, NULL, 'http://localhost/wholesalepeople/public/vendor/home/1', '3', '2018-07-03 08:39:44', '2018-07-03 08:39:44'),
(180, 14, NULL, NULL, 'http://localhost/wholesalepeople/public/vendor/home/1', '3', '2018-07-03 08:40:31', '2018-07-03 08:40:31'),
(181, 14, NULL, NULL, 'http://localhost/wholesalepeople/public/vendor/home/1', '3', '2018-07-03 08:44:11', '2018-07-03 08:44:11'),
(182, 14, NULL, NULL, 'http://localhost/wholesalepeople/public/vendor/home/1', '3', '2018-07-03 08:45:40', '2018-07-03 08:45:40'),
(183, 14, NULL, NULL, 'http://localhost/wholesalepeople/public/vendor/home/1', '3', '2018-07-03 08:47:03', '2018-07-03 08:47:03'),
(184, 14, NULL, NULL, 'http://localhost/wholesalepeople/public/vendor/home/1', '3', '2018-07-03 08:48:26', '2018-07-03 08:48:26'),
(185, 14, NULL, NULL, 'http://localhost/wholesalepeople/public/vendor/home/1', '3', '2018-07-03 08:48:41', '2018-07-03 08:48:41'),
(186, 14, NULL, NULL, 'http://localhost/wholesalepeople/public/vendor/home/1', '3', '2018-07-03 08:48:50', '2018-07-03 08:48:50'),
(187, 14, NULL, NULL, 'http://localhost/wholesalepeople/public/vendor/home/1', '3', '2018-07-03 08:49:10', '2018-07-03 08:49:10'),
(188, 14, NULL, NULL, 'http://localhost/wholesalepeople/public/vendor/home/1', '3', '2018-07-03 08:49:28', '2018-07-03 08:49:28'),
(189, 14, NULL, NULL, 'http://localhost/wholesalepeople/public/vendor/home/1', '3', '2018-07-03 08:49:48', '2018-07-03 08:49:48'),
(190, 14, NULL, NULL, 'http://localhost/wholesalepeople/public/vendor/home/1', '3', '2018-07-03 08:53:09', '2018-07-03 08:53:09'),
(191, 14, NULL, NULL, 'http://localhost/wholesalepeople/public/vendor/home/1', '3', '2018-07-03 08:53:32', '2018-07-03 08:53:32'),
(192, 14, NULL, NULL, 'http://localhost/wholesalepeople/public/vendor/home/1', '3', '2018-07-03 08:54:08', '2018-07-03 08:54:08'),
(193, 14, NULL, NULL, 'http://localhost/wholesalepeople/public/vendor/home/1', '3', '2018-07-03 08:54:26', '2018-07-03 08:54:26'),
(194, 14, NULL, NULL, 'http://localhost/wholesalepeople/public/vendor/home/1', '3', '2018-07-03 08:56:10', '2018-07-03 08:56:10'),
(195, 14, NULL, NULL, 'http://localhost/wholesalepeople/public/vendor/home/1', '3', '2018-07-03 08:56:53', '2018-07-03 08:56:53'),
(196, 14, NULL, NULL, 'http://localhost/wholesalepeople/public/vendor/home/1', '3', '2018-07-03 08:57:36', '2018-07-03 08:57:36'),
(197, 14, NULL, NULL, 'http://localhost/wholesalepeople/public/vendor/home/1', '3', '2018-07-03 08:57:51', '2018-07-03 08:57:51'),
(198, 14, NULL, NULL, 'http://localhost/wholesalepeople/public/vendor/home/1', '3', '2018-07-03 08:58:04', '2018-07-03 08:58:04'),
(199, 14, NULL, NULL, 'http://localhost/wholesalepeople/public/vendor/home/1', '3', '2018-07-03 08:58:22', '2018-07-03 08:58:22'),
(200, 14, NULL, NULL, 'http://localhost/wholesalepeople/public/vendor/home/1', '3', '2018-07-03 08:58:36', '2018-07-03 08:58:36'),
(201, 14, NULL, NULL, 'http://localhost/wholesalepeople/public/vendor/home/1', '3', '2018-07-03 08:58:55', '2018-07-03 08:58:55'),
(202, 14, NULL, NULL, 'http://localhost/wholesalepeople/public/vendor/home/1', '3', '2018-07-03 08:59:18', '2018-07-03 08:59:18'),
(203, 14, NULL, NULL, 'http://localhost/wholesalepeople/public/vendor/home/1', '3', '2018-07-03 09:00:10', '2018-07-03 09:00:10'),
(204, 14, NULL, NULL, 'http://localhost/wholesalepeople/public/vendor/home/1', '3', '2018-07-03 09:00:26', '2018-07-03 09:00:26'),
(205, 14, NULL, NULL, 'http://localhost/wholesalepeople/public/vendor/home/1', '3', '2018-07-03 09:00:33', '2018-07-03 09:00:33'),
(206, 14, NULL, NULL, 'http://localhost/wholesalepeople/public/vendor/home/1', '3', '2018-07-03 09:00:56', '2018-07-03 09:00:56'),
(207, 14, NULL, NULL, 'http://localhost/wholesalepeople/public/vendor/home/1', '3', '2018-07-03 09:01:17', '2018-07-03 09:01:17'),
(208, 14, NULL, NULL, 'http://localhost/wholesalepeople/public/vendor/home/1', '3', '2018-07-03 09:01:27', '2018-07-03 09:01:27'),
(209, 14, NULL, NULL, 'http://localhost/wholesalepeople/public/vendor/home/1', '3', '2018-07-03 09:02:24', '2018-07-03 09:02:24'),
(210, 14, NULL, NULL, 'http://localhost/wholesalepeople/public/vendor/home/1', '3', '2018-07-03 09:03:46', '2018-07-03 09:03:46'),
(211, 14, NULL, NULL, 'http://localhost/wholesalepeople/public/vendor/home/1', '3', '2018-07-03 09:03:53', '2018-07-03 09:03:53'),
(212, 14, NULL, NULL, 'http://localhost/wholesalepeople/public/vendor/home/1', '3', '2018-07-03 09:04:03', '2018-07-03 09:04:03'),
(213, 14, NULL, NULL, 'http://localhost/wholesalepeople/public/vendor/home/1', '3', '2018-07-03 09:04:38', '2018-07-03 09:04:38'),
(214, 14, NULL, NULL, 'http://localhost/wholesalepeople/public/vendor/home/2', '3', '2018-07-03 09:07:18', '2018-07-03 09:07:18'),
(215, 14, NULL, NULL, 'http://localhost/wholesalepeople/public/vendor/home/1', '3', '2018-07-03 09:07:29', '2018-07-03 09:07:29'),
(216, 14, NULL, NULL, 'http://localhost/wholesalepeople/public/vendor/home/1', '3', '2018-07-03 09:07:57', '2018-07-03 09:07:57'),
(217, 14, NULL, NULL, 'http://localhost/wholesalepeople/public/vendor/home/1', '3', '2018-07-03 11:31:45', '2018-07-03 11:31:45'),
(218, NULL, NULL, NULL, 'http://localhost/wholesalepeople/public/vendor/home/2', '3', '2018-07-03 12:34:12', '2018-07-03 12:34:12'),
(219, NULL, NULL, NULL, 'http://localhost/wholesalepeople/public/vendor/home/1', '3', '2018-07-03 12:37:45', '2018-07-03 12:37:45'),
(220, 14, NULL, NULL, 'http://localhost/wholesalepeople/public/vendor/home/2', '3', '2018-07-03 12:49:31', '2018-07-03 12:49:31'),
(221, 19, NULL, NULL, 'http://localhost/wholesalepeople/public/vendor/home/7', '3', '2018-07-03 13:18:17', '2018-07-03 13:18:17'),
(222, 19, NULL, NULL, 'http://localhost/wholesalepeople/public/vendor/home/7', '3', '2018-07-03 13:21:03', '2018-07-03 13:21:03'),
(223, 19, NULL, NULL, 'http://localhost/wholesalepeople/public/vendor/home/7', '3', '2018-07-03 13:21:11', '2018-07-03 13:21:11'),
(224, 14, NULL, NULL, 'http://localhost/wholesalepeople/public/vendor/home/1', '3', '2018-07-03 13:23:13', '2018-07-03 13:23:13'),
(225, 14, NULL, NULL, 'http://localhost/wholesalepeople/public/vendor/home/1', '3', '2018-07-03 13:23:37', '2018-07-03 13:23:37'),
(226, 14, NULL, NULL, 'http://localhost/wholesalepeople/public/vendor/home/2', '3', '2018-07-04 08:22:01', '2018-07-04 08:22:01'),
(227, 14, NULL, NULL, 'http://localhost/wholesalepeople/public/vendor/home/1', '3', '2018-07-04 10:43:03', '2018-07-04 10:43:03'),
(228, NULL, NULL, NULL, 'http://localhost/wholesalepeople/public/vendor/home/1', '3', '2018-07-05 02:10:41', '2018-07-05 02:10:41'),
(229, 14, NULL, NULL, 'http://localhost/wholesalepeople/public/vendor/home/1', '3', '2018-07-05 04:44:28', '2018-07-05 04:44:28'),
(230, 14, NULL, NULL, 'http://localhost/wholesalepeople/public/vendor/home/1', '3', '2018-07-05 04:46:07', '2018-07-05 04:46:07'),
(231, 14, NULL, NULL, 'http://localhost/wholesalepeople/public/vendor/home/1', '3', '2018-07-05 04:47:42', '2018-07-05 04:47:42'),
(232, 14, NULL, NULL, 'http://localhost/wholesalepeople/public/vendor/home/1', '3', '2018-07-05 04:51:18', '2018-07-05 04:51:18'),
(233, 14, NULL, NULL, 'http://localhost/wholesalepeople/public/vendor/home/1', '3', '2018-07-05 04:56:35', '2018-07-05 04:56:35'),
(234, 14, NULL, NULL, 'http://localhost/wholesalepeople/public/Binjar', '3', '2018-07-05 05:21:32', '2018-07-05 05:21:32'),
(235, 14, NULL, NULL, 'http://localhost/wholesalepeople/public/binjar', '3', '2018-07-05 05:27:01', '2018-07-05 05:27:01'),
(236, 14, NULL, NULL, 'http://localhost/wholesalepeople/public/binjar', '3', '2018-07-05 07:28:08', '2018-07-05 07:28:08'),
(237, 14, NULL, NULL, 'http://localhost/wholesalepeople/public/binjar', '3', '2018-07-05 07:28:59', '2018-07-05 07:28:59'),
(238, 14, NULL, NULL, 'http://localhost/wholesalepeople/public/binjar', '3', '2018-07-05 07:32:04', '2018-07-05 07:32:04'),
(239, 14, NULL, NULL, 'http://localhost/wholesalepeople/public/binjar', '3', '2018-07-05 07:34:21', '2018-07-05 07:34:21'),
(240, 14, NULL, NULL, 'http://localhost/wholesalepeople/public/binjar', '3', '2018-07-05 07:36:01', '2018-07-05 07:36:01'),
(241, 14, NULL, NULL, 'http://localhost/wholesalepeople/public/binjar', '3', '2018-07-05 07:36:06', '2018-07-05 07:36:06'),
(242, 14, NULL, NULL, 'http://localhost/wholesalepeople/public/binjar', '3', '2018-07-05 07:36:36', '2018-07-05 07:36:36'),
(243, 14, NULL, NULL, 'http://localhost/wholesalepeople/public/binjar', '3', '2018-07-05 07:56:38', '2018-07-05 07:56:38'),
(244, 14, NULL, NULL, 'http://localhost/wholesalepeople/public/binjar', '3', '2018-07-05 07:58:06', '2018-07-05 07:58:06'),
(245, 14, NULL, NULL, 'http://localhost/wholesalepeople/public/binjar', '3', '2018-07-05 08:01:23', '2018-07-05 08:01:23'),
(246, 14, NULL, NULL, 'http://localhost/wholesalepeople/public/binjar', '3', '2018-07-05 08:03:40', '2018-07-05 08:03:40'),
(247, 14, NULL, NULL, 'http://localhost/wholesalepeople/public/binjar', '3', '2018-07-05 08:04:53', '2018-07-05 08:04:53'),
(248, 14, NULL, NULL, 'http://localhost/wholesalepeople/public/binjar', '3', '2018-07-05 08:05:49', '2018-07-05 08:05:49'),
(249, 14, NULL, NULL, 'http://localhost/wholesalepeople/public/binjar', '3', '2018-07-05 08:06:37', '2018-07-05 08:06:37'),
(250, 14, NULL, NULL, 'http://localhost/wholesalepeople/public/binjar', '3', '2018-07-05 08:08:09', '2018-07-05 08:08:09'),
(251, 14, NULL, NULL, 'http://localhost/wholesalepeople/public/binjar', '3', '2018-07-05 08:08:15', '2018-07-05 08:08:15'),
(252, 14, NULL, NULL, 'http://localhost/wholesalepeople/public/binjar', '3', '2018-07-05 08:09:22', '2018-07-05 08:09:22'),
(253, 14, NULL, NULL, 'http://localhost/wholesalepeople/public/binjar', '3', '2018-07-05 08:09:49', '2018-07-05 08:09:49'),
(254, 14, NULL, NULL, 'http://localhost/wholesalepeople/public/binjar', '3', '2018-07-05 08:11:36', '2018-07-05 08:11:36'),
(255, 14, NULL, NULL, 'http://localhost/wholesalepeople/public/binjar', '3', '2018-07-05 08:13:16', '2018-07-05 08:13:16'),
(256, NULL, NULL, NULL, 'http://localhost/wholesalepeople/public/binjar', '3', '2018-07-05 12:44:15', '2018-07-05 12:44:15'),
(257, NULL, NULL, NULL, 'http://localhost/wholesalepeople/public/binjar', '3', '2018-07-05 12:44:26', '2018-07-05 12:44:26'),
(258, NULL, NULL, NULL, 'http://localhost/wholesalepeople/public/yellow', '3', '2018-07-05 12:44:37', '2018-07-05 12:44:37'),
(259, NULL, NULL, NULL, 'http://localhost/wholesalepeople/public/binjar', '3', '2018-07-05 12:45:00', '2018-07-05 12:45:00'),
(260, NULL, NULL, NULL, 'http://localhost/wholesalepeople/public/binjar', '3', '2018-07-05 12:45:23', '2018-07-05 12:45:23'),
(261, NULL, NULL, NULL, 'http://localhost/wholesalepeople/public/binjar', '3', '2018-07-05 12:45:31', '2018-07-05 12:45:31'),
(262, NULL, NULL, NULL, 'http://localhost/wholesalepeople/public/binjar', '3', '2018-07-05 12:46:40', '2018-07-05 12:46:40'),
(263, NULL, NULL, NULL, 'http://localhost/wholesalepeople/public/binjar', '3', '2018-07-05 12:55:24', '2018-07-05 12:55:24'),
(264, NULL, NULL, NULL, 'http://localhost/wholesalepeople/public/binjar', '3', '2018-07-05 12:57:10', '2018-07-05 12:57:10'),
(265, NULL, NULL, NULL, 'http://localhost/wholesalepeople/public/binjar', '3', '2018-07-06 02:32:30', '2018-07-06 02:32:30'),
(266, 14, NULL, NULL, 'http://localhost/wholesalepeople/public/yellow', '3', '2018-07-06 06:00:09', '2018-07-06 06:00:09'),
(267, 14, NULL, NULL, 'http://localhost/wholesalepeople/public/binjar', '3', '2018-07-06 06:00:13', '2018-07-06 06:00:13'),
(268, 14, NULL, NULL, 'http://localhost/wholesalepeople/public/yellow%20232', '3', '2018-07-06 06:00:30', '2018-07-06 06:00:30'),
(269, 14, NULL, NULL, 'http://localhost/wholesalepeople/public/yellow%20232', '3', '2018-07-06 08:04:47', '2018-07-06 08:04:47'),
(270, 14, NULL, NULL, 'http://localhost/wholesalepeople/public/yellow%20232', '3', '2018-07-06 08:13:41', '2018-07-06 08:13:41'),
(271, 14, NULL, NULL, 'http://localhost/wholesalepeople/public/yellow%20232', '3', '2018-07-06 08:16:50', '2018-07-06 08:16:50'),
(272, 14, NULL, NULL, 'http://localhost/wholesalepeople/public/yellow%20232', '3', '2018-07-06 08:17:34', '2018-07-06 08:17:34'),
(273, 14, NULL, NULL, 'http://localhost/wholesalepeople/public/yellow%20232', '3', '2018-07-06 08:17:35', '2018-07-06 08:17:35'),
(274, 14, NULL, NULL, 'http://localhost/wholesalepeople/public/yellow-232', '3', '2018-07-06 08:35:32', '2018-07-06 08:35:32'),
(275, 14, NULL, NULL, 'http://localhost/wholesalepeople/public/yellow-232', '3', '2018-07-06 08:38:09', '2018-07-06 08:38:09'),
(276, 14, NULL, NULL, 'http://localhost/wholesalepeople/public/yellow-232', '3', '2018-07-06 08:44:21', '2018-07-06 08:44:21'),
(277, 14, NULL, NULL, 'http://localhost/wholesalepeople/public/yellow-232', '3', '2018-07-06 08:52:20', '2018-07-06 08:52:20'),
(278, 14, NULL, NULL, 'http://localhost/wholesalepeople/public/yellow-232', '3', '2018-07-06 08:58:32', '2018-07-06 08:58:32'),
(279, 14, NULL, NULL, 'http://localhost/wholesalepeople/public/binjar', '3', '2018-07-06 10:12:28', '2018-07-06 10:12:28'),
(280, 14, NULL, NULL, 'http://localhost/wholesalepeople/public/binjar', '3', '2018-07-06 10:14:38', '2018-07-06 10:14:38'),
(281, NULL, NULL, NULL, 'http://localhost/wholesalepeople/public/binjar', '3', '2018-07-07 02:21:47', '2018-07-07 02:21:47'),
(282, 14, NULL, NULL, 'http://localhost/wholesalepeople/public/binjar', '3', '2018-07-07 03:14:18', '2018-07-07 03:14:18'),
(283, NULL, NULL, NULL, 'http://localhost/wholesalepeople/public/binjar', '3', '2018-07-07 03:14:32', '2018-07-07 03:14:32'),
(284, NULL, NULL, NULL, 'http://localhost/wholesalepeople/public/binjar', '3', '2018-07-07 03:36:48', '2018-07-07 03:36:48'),
(285, NULL, NULL, NULL, 'http://localhost/wholesalepeople/public/binjar', '3', '2018-07-07 04:12:20', '2018-07-07 04:12:20'),
(286, NULL, NULL, NULL, 'http://localhost/wholesalepeople/public/binjar', '3', '2018-07-07 04:17:42', '2018-07-07 04:17:42'),
(287, 14, NULL, NULL, 'http://localhost/wholesalepeople/public/binjar', '3', '2018-07-07 04:18:40', '2018-07-07 04:18:40'),
(288, 14, NULL, NULL, 'http://localhost/wholesalepeople/public/binjar', '3', '2018-07-07 04:24:56', '2018-07-07 04:24:56'),
(289, 14, NULL, NULL, 'http://localhost/wholesalepeople/public/binjar', '3', '2018-07-07 04:25:20', '2018-07-07 04:25:20'),
(290, 14, NULL, NULL, 'http://localhost/wholesalepeople/public/binjar', '3', '2018-07-07 04:27:35', '2018-07-07 04:27:35'),
(291, NULL, NULL, NULL, 'http://localhost/wholesalepeople/public/binjar', '3', '2018-07-09 05:04:47', '2018-07-09 05:04:47'),
(292, NULL, NULL, NULL, 'http://localhost/wholesalepeople/public/binjar', '3', '2018-07-09 05:04:56', '2018-07-09 05:04:56'),
(293, NULL, NULL, NULL, 'http://localhost/wholesalepeople/public/binjar', '3', '2018-07-09 05:04:57', '2018-07-09 05:04:57'),
(294, NULL, NULL, NULL, 'http://localhost/wholesalepeople/public/binjar', '3', '2018-07-09 05:13:33', '2018-07-09 05:13:33'),
(295, NULL, NULL, NULL, 'http://localhost/wholesalepeople/public/binjar', '3', '2018-07-09 05:18:50', '2018-07-09 05:18:50'),
(296, NULL, NULL, NULL, 'http://localhost/wholesalepeople/public/binjar', '3', '2018-07-09 05:21:26', '2018-07-09 05:21:26'),
(297, 14, NULL, NULL, 'http://localhost/wholesalepeople/public/binjar', '3', '2018-07-09 05:22:20', '2018-07-09 05:22:20'),
(298, 14, NULL, NULL, 'http://localhost/wholesalepeople/public/yellow-232', '3', '2018-07-09 05:23:57', '2018-07-09 05:23:57'),
(299, 14, NULL, NULL, 'http://localhost/wholesalepeople/public/yellow-232', '3', '2018-07-09 05:24:31', '2018-07-09 05:24:31'),
(300, 14, NULL, NULL, 'http://localhost/wholesalepeople/public/yellow-232', '3', '2018-07-09 05:40:59', '2018-07-09 05:40:59'),
(301, 14, NULL, NULL, 'http://localhost/wholesalepeople/public/yellow-232', '3', '2018-07-09 05:41:05', '2018-07-09 05:41:05'),
(302, 14, NULL, NULL, 'http://localhost/wholesalepeople/public/binjar', '3', '2018-07-09 06:15:11', '2018-07-09 06:15:11'),
(303, 14, NULL, NULL, 'http://localhost/wholesalepeople/public/binjar', '3', '2018-07-09 06:16:12', '2018-07-09 06:16:12'),
(304, 14, NULL, NULL, 'http://localhost/wholesalepeople/public/binjar', '3', '2018-07-09 06:16:27', '2018-07-09 06:16:27'),
(305, 14, NULL, NULL, 'http://localhost/wholesalepeople/public/binjar', '3', '2018-07-09 06:16:35', '2018-07-09 06:16:35'),
(306, 14, NULL, NULL, 'http://localhost/wholesalepeople/public/binjar', '3', '2018-07-09 06:17:14', '2018-07-09 06:17:14'),
(307, 14, NULL, NULL, 'http://localhost/wholesalepeople/public/binjar', '3', '2018-07-09 06:17:15', '2018-07-09 06:17:15'),
(308, 14, NULL, NULL, 'http://localhost/wholesalepeople/public/binjar', '3', '2018-07-09 06:17:44', '2018-07-09 06:17:44'),
(309, 14, NULL, NULL, 'http://localhost/wholesalepeople/public/binjar', '3', '2018-07-09 06:17:52', '2018-07-09 06:17:52'),
(310, 14, NULL, NULL, 'http://localhost/wholesalepeople/public/binjar', '3', '2018-07-09 06:18:14', '2018-07-09 06:18:14'),
(311, 14, NULL, NULL, 'http://localhost/wholesalepeople/public/binjar', '3', '2018-07-09 06:18:22', '2018-07-09 06:18:22'),
(312, 14, NULL, NULL, 'http://localhost/wholesalepeople/public/binjar', '3', '2018-07-09 06:46:27', '2018-07-09 06:46:27'),
(313, 14, NULL, NULL, 'http://localhost/wholesalepeople/public/binjar', '3', '2018-07-09 06:46:49', '2018-07-09 06:46:49'),
(314, 14, NULL, NULL, 'http://localhost/wholesalepeople/public/binjar', '3', '2018-07-09 06:47:02', '2018-07-09 06:47:02'),
(315, 14, NULL, NULL, 'http://localhost/wholesalepeople/public/binjar', '3', '2018-07-09 06:47:02', '2018-07-09 06:47:02'),
(316, 14, NULL, NULL, 'http://localhost/wholesalepeople/public/binjar', '3', '2018-07-09 06:49:06', '2018-07-09 06:49:06'),
(317, 14, NULL, NULL, 'http://localhost/wholesalepeople/public/binjar', '3', '2018-07-09 06:49:07', '2018-07-09 06:49:07'),
(318, 14, NULL, NULL, 'http://localhost/wholesalepeople/public/binjar', '3', '2018-07-09 06:49:08', '2018-07-09 06:49:08'),
(319, NULL, NULL, NULL, 'http://localhost/wholesalepeople/public/binjar', '3', '2018-07-09 06:49:18', '2018-07-09 06:49:18'),
(320, NULL, NULL, NULL, 'http://localhost/wholesalepeople/public/binjar', '3', '2018-07-09 06:49:22', '2018-07-09 06:49:22'),
(321, 14, NULL, NULL, 'http://localhost/wholesalepeople/public/binjar', '3', '2018-07-09 06:49:28', '2018-07-09 06:49:28'),
(322, NULL, NULL, NULL, 'http://localhost/wholesalepeople/public/binjar', '3', '2018-07-09 06:49:48', '2018-07-09 06:49:48'),
(323, NULL, NULL, NULL, 'http://localhost/wholesalepeople/public/binjar', '3', '2018-07-09 06:49:58', '2018-07-09 06:49:58'),
(324, 14, NULL, NULL, 'http://localhost/wholesalepeople/public/binjar', '3', '2018-07-09 06:50:12', '2018-07-09 06:50:12'),
(325, 14, NULL, NULL, 'http://localhost/wholesalepeople/public/binjar', '3', '2018-07-09 06:50:21', '2018-07-09 06:50:21'),
(326, 14, NULL, NULL, 'http://localhost/wholesalepeople/public/binjar', '3', '2018-07-09 12:54:42', '2018-07-09 12:54:42'),
(327, 14, NULL, NULL, 'http://localhost/wholesalepeople/public/binjar', '3', '2018-07-09 12:55:20', '2018-07-09 12:55:20'),
(328, 14, NULL, NULL, 'http://localhost/wholesalepeople/public/yellow-232', '3', '2018-07-09 12:55:27', '2018-07-09 12:55:27'),
(329, 14, NULL, NULL, 'http://localhost/wholesalepeople/public/binjar', '3', '2018-07-09 12:55:33', '2018-07-09 12:55:33'),
(330, 14, NULL, NULL, 'http://localhost/wholesalepeople/public/binjar', '3', '2018-07-09 12:56:36', '2018-07-09 12:56:36'),
(331, 14, NULL, NULL, 'http://localhost/wholesalepeople/public/binjar', '3', '2018-07-10 05:19:59', '2018-07-10 05:19:59'),
(332, 14, NULL, NULL, 'http://localhost/wholesalepeople/public/binjar', '3', '2018-07-10 05:20:12', '2018-07-10 05:20:12'),
(333, 14, NULL, NULL, 'http://localhost/wholesalepeople/public/binjar', '3', '2018-07-10 06:14:40', '2018-07-10 06:14:40'),
(334, 14, NULL, NULL, 'http://localhost/wholesalepeople/public/binjar', '3', '2018-07-10 06:14:44', '2018-07-10 06:14:44'),
(335, 14, NULL, NULL, 'http://localhost/wholesalepeople/public/binjar', '3', '2018-07-10 06:19:18', '2018-07-10 06:19:18'),
(336, 14, NULL, NULL, 'http://localhost/wholesalepeople/public/binjar', '3', '2018-07-10 06:19:19', '2018-07-10 06:19:19'),
(337, 14, NULL, NULL, 'http://localhost/wholesalepeople/public/binjar', '3', '2018-07-10 06:19:19', '2018-07-10 06:19:19'),
(338, 14, NULL, NULL, 'http://localhost/wholesalepeople/public/binjar', '3', '2018-07-10 06:19:20', '2018-07-10 06:19:20'),
(339, 14, NULL, NULL, 'http://localhost/wholesalepeople/public/binjar', '3', '2018-07-10 06:20:31', '2018-07-10 06:20:31'),
(340, 14, NULL, NULL, 'http://localhost/wholesalepeople/public/binjar', '3', '2018-07-10 06:20:32', '2018-07-10 06:20:32'),
(341, 14, NULL, NULL, 'http://localhost/wholesalepeople/public/binjar', '3', '2018-07-10 06:20:40', '2018-07-10 06:20:40'),
(342, 14, NULL, NULL, 'http://localhost/wholesalepeople/public/binjar', '3', '2018-07-10 06:20:41', '2018-07-10 06:20:41'),
(343, 14, NULL, NULL, 'http://localhost/wholesalepeople/public/binjar', '3', '2018-07-10 06:21:33', '2018-07-10 06:21:33'),
(344, 14, NULL, NULL, 'http://localhost/wholesalepeople/public/binjar', '3', '2018-07-10 06:21:34', '2018-07-10 06:21:34'),
(345, 14, NULL, NULL, 'http://localhost/wholesalepeople/public/binjar', '3', '2018-07-10 06:21:35', '2018-07-10 06:21:35'),
(346, 14, NULL, NULL, 'http://localhost/wholesalepeople/public/binjar', '3', '2018-07-10 06:21:35', '2018-07-10 06:21:35'),
(347, 14, NULL, NULL, 'http://localhost/wholesalepeople/public/binjar', '3', '2018-07-10 06:21:36', '2018-07-10 06:21:36'),
(348, 14, NULL, NULL, 'http://localhost/wholesalepeople/public/binjar', '3', '2018-07-10 06:21:37', '2018-07-10 06:21:37'),
(349, 14, NULL, NULL, 'http://localhost/wholesalepeople/public/binjar', '3', '2018-07-10 06:21:59', '2018-07-10 06:21:59'),
(350, 14, NULL, NULL, 'http://localhost/wholesalepeople/public/binjar', '3', '2018-07-10 06:22:43', '2018-07-10 06:22:43'),
(351, 14, NULL, NULL, 'http://localhost/wholesalepeople/public/binjar', '3', '2018-07-10 06:22:52', '2018-07-10 06:22:52'),
(352, 14, NULL, NULL, 'http://localhost/wholesalepeople/public/binjar', '3', '2018-07-10 06:23:11', '2018-07-10 06:23:11'),
(353, 14, NULL, NULL, 'http://localhost/wholesalepeople/public/binjar', '3', '2018-07-10 06:26:53', '2018-07-10 06:26:53'),
(354, NULL, NULL, NULL, 'http://localhost/wholesalepeople/public/binjar', '3', '2018-07-10 09:59:00', '2018-07-10 09:59:00'),
(355, NULL, NULL, NULL, 'http://localhost/wholesalepeople/public/binjar', '3', '2018-07-10 09:59:11', '2018-07-10 09:59:11'),
(356, NULL, NULL, NULL, 'http://localhost/wholesalepeople/public/binjar', '3', '2018-07-10 10:08:08', '2018-07-10 10:08:08'),
(357, NULL, NULL, NULL, 'http://localhost/wholesalepeople/public/binjar', '3', '2018-07-10 10:14:21', '2018-07-10 10:14:21'),
(358, NULL, NULL, NULL, 'http://localhost/wholesalepeople/public/binjar', '3', '2018-07-10 10:15:07', '2018-07-10 10:15:07'),
(359, NULL, NULL, NULL, 'http://localhost/wholesalepeople/public/binjar', '3', '2018-07-10 10:15:41', '2018-07-10 10:15:41'),
(360, NULL, NULL, NULL, 'http://localhost/wholesalepeople/public/binjar', '3', '2018-07-10 10:15:46', '2018-07-10 10:15:46'),
(361, NULL, NULL, NULL, 'http://localhost/wholesalepeople/public/binjar', '3', '2018-07-10 10:15:58', '2018-07-10 10:15:58'),
(362, 14, NULL, NULL, 'http://localhost/wholesalepeople/public/binjar', '3', '2018-07-10 12:43:06', '2018-07-10 12:43:06'),
(363, NULL, NULL, NULL, 'http://localhost/wholesalepeople/public/binjar', '3', '2018-07-11 19:51:50', '2018-07-11 19:51:50'),
(364, NULL, NULL, NULL, 'http://localhost/wholesalepeople/public/binjar', '3', '2018-07-11 19:52:11', '2018-07-11 19:52:11'),
(365, NULL, NULL, NULL, 'http://localhost/wholesalepeople/public/binjar', '3', '2018-07-11 19:52:17', '2018-07-11 19:52:17'),
(366, NULL, NULL, NULL, 'http://localhost/wholesalepeople/public/binjar', '3', '2018-07-11 19:56:26', '2018-07-11 19:56:26'),
(367, NULL, NULL, NULL, 'http://localhost/wholesalepeople/public/binjar', '3', '2018-07-11 19:56:37', '2018-07-11 19:56:37'),
(368, NULL, NULL, NULL, 'http://localhost/wholesalepeople/public/binjar', '3', '2018-07-11 19:56:38', '2018-07-11 19:56:38'),
(369, NULL, NULL, NULL, 'http://localhost/wholesalepeople/public/binjar', '3', '2018-07-11 19:56:42', '2018-07-11 19:56:42'),
(370, NULL, NULL, NULL, 'http://localhost/wholesalepeople/public/binjar', '3', '2018-07-11 19:56:50', '2018-07-11 19:56:50'),
(371, NULL, NULL, NULL, 'http://localhost/wholesalepeople/public/binjar', '3', '2018-07-11 19:56:57', '2018-07-11 19:56:57'),
(372, NULL, NULL, NULL, 'http://localhost/wholesalepeople/public/binjar', '3', '2018-07-11 19:56:58', '2018-07-11 19:56:58'),
(373, NULL, NULL, NULL, 'http://localhost/wholesalepeople/public/binjar', '3', '2018-07-11 19:57:01', '2018-07-11 19:57:01'),
(374, NULL, NULL, NULL, 'http://localhost/wholesalepeople/public/binjar', '3', '2018-07-11 19:57:10', '2018-07-11 19:57:10'),
(375, NULL, NULL, NULL, 'http://localhost/wholesalepeople/public/binjar', '3', '2018-07-11 19:58:01', '2018-07-11 19:58:01'),
(376, NULL, NULL, NULL, 'http://localhost/wholesalepeople/public/binjar', '3', '2018-07-12 19:39:40', '2018-07-12 19:39:40'),
(377, 14, NULL, NULL, 'http://localhost/wholesalepeople/public/binjar', '3', '2018-07-12 19:39:51', '2018-07-12 19:39:51'),
(378, 14, NULL, NULL, 'http://localhost/wholesalepeople/public/yellow-232', '3', '2018-07-12 21:42:29', '2018-07-12 21:42:29'),
(379, NULL, NULL, NULL, 'http://localhost/wholesalepeople/public/binjar', '3', '2018-07-12 23:01:35', '2018-07-12 23:01:35'),
(380, 14, NULL, NULL, 'http://localhost/wholesalepeople/public/binjar', '3', '2018-07-13 20:11:03', '2018-07-13 20:11:03'),
(381, 14, NULL, NULL, 'http://localhost/wholesalepeople/public/binjar', '3', '2018-07-13 20:20:05', '2018-07-13 20:20:05'),
(382, NULL, NULL, NULL, 'http://localhost/wholesalepeople/public/binjar', '3', '2018-07-13 20:40:11', '2018-07-13 20:40:11'),
(383, 14, NULL, NULL, 'http://localhost/wholesalepeople/public/binjar', '3', '2018-07-13 20:40:34', '2018-07-13 20:40:34'),
(384, 14, NULL, NULL, 'http://localhost/wholesalepeople/public/binjar', '3', '2018-07-13 20:49:15', '2018-07-13 20:49:15'),
(385, 14, NULL, NULL, 'http://localhost/wholesalepeople/public/binjar', '3', '2018-07-13 20:49:39', '2018-07-13 20:49:39'),
(386, 14, NULL, NULL, 'http://localhost/wholesalepeople/public/binjar', '3', '2018-07-13 20:49:58', '2018-07-13 20:49:58'),
(387, 14, NULL, NULL, 'http://localhost/wholesalepeople/public/binjar', '3', '2018-07-13 20:51:12', '2018-07-13 20:51:12'),
(388, 14, NULL, NULL, 'http://localhost/wholesalepeople/public/binjar', '3', '2018-07-13 20:56:17', '2018-07-13 20:56:17'),
(389, 14, NULL, NULL, 'http://localhost/wholesalepeople/public/binjar', '3', '2018-07-13 20:59:47', '2018-07-13 20:59:47'),
(390, 14, NULL, NULL, 'http://localhost/wholesalepeople/public/binjar', '3', '2018-07-13 21:01:42', '2018-07-13 21:01:42'),
(391, 14, NULL, NULL, 'http://localhost/wholesalepeople/public/binjar', '3', '2018-07-13 21:42:41', '2018-07-13 21:42:41'),
(392, 14, NULL, NULL, 'http://localhost/wholesalepeople/public/binjar', '3', '2018-07-13 21:42:55', '2018-07-13 21:42:55'),
(393, NULL, NULL, NULL, 'http://localhost/wholesalepeople/public/binjar', '3', '2018-07-13 21:47:33', '2018-07-13 21:47:33');
INSERT INTO `visitors` (`id`, `user_id`, `vendor_meta_id`, `route`, `url`, `ip`, `created_at`, `updated_at`) VALUES
(394, NULL, NULL, NULL, 'http://localhost/wholesalepeople/public/binjar', '3', '2018-07-13 21:47:34', '2018-07-13 21:47:34'),
(395, NULL, NULL, NULL, 'http://localhost/wholesalepeople/public/binjar', '3', '2018-07-13 21:47:35', '2018-07-13 21:47:35'),
(396, 14, NULL, NULL, 'http://localhost/wholesalepeople/public/binjar', '3', '2018-07-13 21:47:57', '2018-07-13 21:47:57'),
(397, 14, NULL, NULL, 'http://localhost/wholesalepeople/public/binjar', '3', '2018-07-13 21:48:16', '2018-07-13 21:48:16'),
(398, 14, NULL, NULL, 'http://localhost/wholesalepeople/public/binjar', '3', '2018-07-13 21:48:17', '2018-07-13 21:48:17'),
(399, 14, NULL, NULL, 'http://localhost/wholesalepeople/public/binjar', '3', '2018-07-13 21:48:17', '2018-07-13 21:48:17'),
(400, 14, NULL, NULL, 'http://localhost/wholesalepeople/public/binjar', '3', '2018-07-13 21:48:18', '2018-07-13 21:48:18'),
(401, 14, NULL, NULL, 'http://localhost/wholesalepeople/public/binjar', '3', '2018-07-13 21:48:26', '2018-07-13 21:48:26'),
(402, 14, NULL, NULL, 'http://localhost/wholesalepeople/public/binjar', '3', '2018-07-13 21:48:33', '2018-07-13 21:48:33'),
(403, 14, NULL, NULL, 'http://localhost/wholesalepeople/public/binjar', '3', '2018-07-13 21:48:34', '2018-07-13 21:48:34'),
(404, 14, NULL, NULL, 'http://localhost/wholesalepeople/public/binjar', '3', '2018-07-13 21:48:52', '2018-07-13 21:48:52'),
(405, 14, NULL, NULL, 'http://localhost/wholesalepeople/public/binjar', '3', '2018-07-13 21:49:10', '2018-07-13 21:49:10'),
(406, 14, NULL, NULL, 'http://localhost/wholesalepeople/public/binjar', '3', '2018-07-13 21:49:12', '2018-07-13 21:49:12'),
(407, 14, NULL, NULL, 'http://localhost/wholesalepeople/public/binjar', '3', '2018-07-13 21:49:58', '2018-07-13 21:49:58'),
(408, 14, NULL, NULL, 'http://localhost/wholesalepeople/public/binjar', '3', '2018-07-13 21:49:59', '2018-07-13 21:49:59'),
(409, 14, NULL, NULL, 'http://localhost/wholesalepeople/public/binjar', '3', '2018-07-13 21:49:59', '2018-07-13 21:49:59'),
(410, 14, NULL, NULL, 'http://localhost/wholesalepeople/public/binjar', '3', '2018-07-13 21:50:00', '2018-07-13 21:50:00'),
(411, 14, NULL, NULL, 'http://localhost/wholesalepeople/public/binjar', '3', '2018-07-13 21:50:11', '2018-07-13 21:50:11'),
(412, 14, NULL, NULL, 'http://localhost/wholesalepeople/public/binjar', '3', '2018-07-13 21:50:23', '2018-07-13 21:50:23'),
(413, 14, NULL, NULL, 'http://localhost/wholesalepeople/public/binjar', '3', '2018-07-13 22:24:46', '2018-07-13 22:24:46'),
(414, 14, NULL, NULL, 'http://localhost/wholesalepeople/public/binjar', '3', '2018-07-13 22:32:56', '2018-07-13 22:32:56'),
(415, 14, NULL, NULL, 'http://localhost/wholesalepeople/public/yellow-232', '3', '2018-07-13 23:29:28', '2018-07-13 23:29:28'),
(416, 14, NULL, NULL, 'http://localhost/wholesalepeople/public/binjar', '3', '2018-07-13 23:53:27', '2018-07-13 23:53:27'),
(417, 14, NULL, NULL, 'http://localhost/wholesalepeople/public/binjar', '3', '2018-07-13 23:55:26', '2018-07-13 23:55:26'),
(418, 14, NULL, NULL, 'http://localhost/wholesalepeople/public/binjar', '3', '2018-07-13 23:56:18', '2018-07-13 23:56:18'),
(419, 14, NULL, NULL, 'http://localhost/wholesalepeople/public/binjar', '3', '2018-07-14 00:31:20', '2018-07-14 00:31:20'),
(420, NULL, NULL, NULL, 'http://localhost/wholesalepeople/public/binjar', '3', '2018-07-14 02:25:19', '2018-07-14 02:25:19'),
(421, NULL, NULL, NULL, 'http://localhost/wholesalepeople/public/binjar', '3', '2018-07-14 02:25:59', '2018-07-14 02:25:59'),
(422, NULL, NULL, NULL, 'http://localhost/wholesalepeople/public/binjar', '3', '2018-07-14 02:34:03', '2018-07-14 02:34:03'),
(423, NULL, NULL, NULL, 'http://localhost/wholesalepeople/public/binjar', '3', '2018-07-14 02:34:08', '2018-07-14 02:34:08'),
(424, NULL, NULL, NULL, 'http://localhost/wholesalepeople/public/binjar', '3', '2018-07-14 02:41:29', '2018-07-14 02:41:29'),
(425, NULL, NULL, NULL, 'http://localhost/wholesalepeople/public/binjar', '3', '2018-07-14 02:48:03', '2018-07-14 02:48:03'),
(426, 14, NULL, NULL, 'http://localhost/wholesalepeople/public/yellow-232', '3', '2018-07-14 02:48:31', '2018-07-14 02:48:31'),
(427, 14, NULL, NULL, 'http://localhost/wholesalepeople/public/binjar', '3', '2018-07-14 04:40:03', '2018-07-14 04:40:03'),
(428, 14, NULL, NULL, 'http://localhost/wholesalepeople/public/binjar', '3', '2018-07-15 20:41:26', '2018-07-15 20:41:26'),
(429, 14, NULL, NULL, 'http://localhost/wholesalepeople/public/binjar', '3', '2018-07-15 20:51:09', '2018-07-15 20:51:09'),
(430, 14, NULL, NULL, 'http://localhost/wholesalepeople/public/bgsfdg', '3', '2018-07-15 20:51:52', '2018-07-15 20:51:52'),
(431, 14, NULL, NULL, 'http://localhost/wholesalepeople/public/binjar', '3', '2018-07-15 21:09:06', '2018-07-15 21:09:06'),
(432, 14, NULL, NULL, 'http://localhost/wholesalepeople/public/binjar', '3', '2018-07-15 21:09:12', '2018-07-15 21:09:12'),
(433, 14, NULL, NULL, 'http://localhost/wholesalepeople/public/binjar', '3', '2018-07-15 21:09:23', '2018-07-15 21:09:23'),
(434, 14, NULL, NULL, 'http://localhost/wholesalepeople/public/binjar', '3', '2018-07-15 21:10:10', '2018-07-15 21:10:10'),
(435, 14, NULL, NULL, 'http://localhost/wholesalepeople/public/binjar', '3', '2018-07-15 21:10:22', '2018-07-15 21:10:22'),
(436, 14, NULL, NULL, 'http://localhost/wholesalepeople/public/binjar', '3', '2018-07-15 21:10:43', '2018-07-15 21:10:43'),
(437, 14, NULL, NULL, 'http://localhost/wholesalepeople/public/binjar', '3', '2018-07-15 21:11:03', '2018-07-15 21:11:03'),
(438, 14, NULL, NULL, 'http://localhost/wholesalepeople/public/binjar', '3', '2018-07-15 21:11:23', '2018-07-15 21:11:23'),
(439, 14, NULL, NULL, 'http://localhost/wholesalepeople/public/binjar', '3', '2018-07-15 21:13:30', '2018-07-15 21:13:30'),
(440, 14, NULL, NULL, 'http://localhost/wholesalepeople/public/binjar', '3', '2018-07-15 21:13:38', '2018-07-15 21:13:38'),
(441, 14, NULL, NULL, 'http://localhost/wholesalepeople/public/binjar', '3', '2018-07-15 21:13:57', '2018-07-15 21:13:57'),
(442, 14, NULL, NULL, 'http://localhost/wholesalepeople/public/binjar', '3', '2018-07-15 21:14:04', '2018-07-15 21:14:04'),
(443, 14, NULL, NULL, 'http://localhost/wholesalepeople/public/binjar', '3', '2018-07-15 21:14:10', '2018-07-15 21:14:10'),
(444, 14, NULL, NULL, 'http://localhost/wholesalepeople/public/binjar', '3', '2018-07-15 21:14:22', '2018-07-15 21:14:22'),
(445, 14, NULL, NULL, 'http://localhost/wholesalepeople/public/binjar', '3', '2018-07-15 21:18:24', '2018-07-15 21:18:24'),
(446, 14, NULL, NULL, 'http://localhost/wholesalepeople/public/binjar', '3', '2018-07-15 21:22:37', '2018-07-15 21:22:37'),
(447, 14, NULL, NULL, 'http://localhost/wholesalepeople/public/binjar', '3', '2018-07-15 21:51:14', '2018-07-15 21:51:14'),
(448, 14, NULL, NULL, 'http://localhost/wholesalepeople/public/binjar', '3', '2018-07-15 22:05:51', '2018-07-15 22:05:51'),
(449, NULL, NULL, NULL, 'http://localhost/wholesalepeople/public/binjar', '3', '2018-07-16 00:16:22', '2018-07-16 00:16:22'),
(450, 14, NULL, NULL, 'http://localhost/wholesalepeople/public/yellow-232', '3', '2018-07-16 22:35:47', '2018-07-16 22:35:47'),
(451, 14, NULL, NULL, 'http://localhost/wholesalepeople/public/yellow-232', '3', '2018-07-16 22:36:00', '2018-07-16 22:36:00'),
(452, 14, NULL, NULL, 'http://localhost/wholesalepeople/public/binjar', '3', '2018-07-16 22:49:12', '2018-07-16 22:49:12'),
(453, 14, NULL, NULL, 'http://localhost/wholesalepeople/public/binjar', '3', '2018-07-16 23:16:07', '2018-07-16 23:16:07'),
(454, 14, NULL, NULL, 'http://localhost/wholesalepeople/public/binjar', '3', '2018-07-17 05:23:19', '2018-07-17 05:23:19'),
(455, 14, NULL, NULL, 'http://localhost/wholesalepeople/public/yellow-232', '::1', '2018-07-17 19:21:52', '2018-07-17 19:21:52'),
(456, 14, NULL, NULL, 'http://localhost/wholesalepeople/public/binjar', '::1', '2018-07-18 02:06:50', '2018-07-18 02:06:50'),
(457, 14, NULL, NULL, 'http://localhost/wholesalepeople/public/binjar', '::1', '2018-07-18 05:21:17', '2018-07-18 05:21:17'),
(458, 14, NULL, NULL, 'http://localhost/wholesalepeople/public/binjar', '::1', '2018-07-18 05:21:40', '2018-07-18 05:21:40'),
(459, 14, NULL, NULL, 'http://localhost/wholesalepeople/public/yellow-232', '::1', '2018-07-18 05:21:45', '2018-07-18 05:21:45'),
(460, 14, NULL, NULL, 'http://localhost/wholesalepeople/public/binjar', '::1', '2018-07-18 21:56:28', '2018-07-18 21:56:28'),
(461, 14, NULL, NULL, 'http://localhost/wholesalepeople/public/binjar', '::1', '2018-07-18 21:58:26', '2018-07-18 21:58:26'),
(462, 14, NULL, NULL, 'http://localhost/wholesalepeople/public/binjar', '::1', '2018-07-18 21:58:35', '2018-07-18 21:58:35'),
(463, 14, NULL, NULL, 'http://localhost/wholesalepeople/public/binjar', '::1', '2018-07-18 22:01:54', '2018-07-18 22:01:54'),
(464, 14, NULL, NULL, 'http://localhost/wholesalepeople/public/binjar', '::1', '2018-07-18 22:03:17', '2018-07-18 22:03:17'),
(465, 14, NULL, NULL, 'http://localhost/wholesalepeople/public/binjar', '::1', '2018-07-18 22:04:08', '2018-07-18 22:04:08'),
(466, NULL, NULL, NULL, 'http://localhost/wholesalepeople/public/binjar', '::1', '2018-07-18 22:21:42', '2018-07-18 22:21:42'),
(467, 14, NULL, NULL, 'http://localhost/wholesalepeople/public/binjar', '::1', '2018-07-18 22:21:50', '2018-07-18 22:21:50'),
(468, 14, NULL, NULL, 'http://localhost/wholesalepeople/public/binjar', '::1', '2018-07-18 22:24:32', '2018-07-18 22:24:32'),
(469, 14, NULL, NULL, 'http://localhost/wholesalepeople/public/binjar', '::1', '2018-07-18 22:24:40', '2018-07-18 22:24:40'),
(470, 14, NULL, NULL, 'http://localhost/wholesalepeople/public/binjar', '::1', '2018-07-18 22:25:39', '2018-07-18 22:25:39'),
(471, 14, NULL, NULL, 'http://localhost/wholesalepeople/public/binjar', '::1', '2018-07-19 20:04:59', '2018-07-19 20:04:59'),
(472, 14, NULL, NULL, 'http://localhost/wholesalepeople/public/binjar', '::1', '2018-07-19 21:26:58', '2018-07-19 21:26:58'),
(473, NULL, NULL, NULL, 'http://localhost/wholesalepeople/public/binjar', '::1', '2018-07-20 04:42:26', '2018-07-20 04:42:26'),
(474, 14, NULL, NULL, 'http://localhost/wholesalepeople/public/binjar', '::1', '2018-07-20 04:48:32', '2018-07-20 04:48:32'),
(475, 14, 1, 'item_details_page', 'http://localhost/wholesalepeople/public/item/details/14', '::1', '2018-07-20 04:52:04', '2018-07-20 04:52:04'),
(476, 14, 1, 'item_details_page', 'http://localhost/wholesalepeople/public/item/details/15', '::1', '2018-07-20 04:55:34', '2018-07-20 04:55:34'),
(477, 14, NULL, NULL, 'http://localhost/wholesalepeople/public/binjar', '::1', '2018-07-20 05:00:09', '2018-07-20 05:00:09'),
(478, 14, NULL, NULL, 'http://localhost/wholesalepeople/public/binjar', '::1', '2018-07-20 05:10:10', '2018-07-20 05:10:10'),
(479, 14, NULL, NULL, 'http://localhost/wholesalepeople/public/binjar', '::1', '2018-07-20 05:10:31', '2018-07-20 05:10:31'),
(480, 14, NULL, NULL, 'http://localhost/wholesalepeople/public/binjar', '::1', '2018-07-20 05:13:12', '2018-07-20 05:13:12'),
(481, 14, NULL, NULL, 'http://localhost/wholesalepeople/public/binjar', '::1', '2018-07-20 05:13:21', '2018-07-20 05:13:21'),
(482, 14, NULL, NULL, 'http://localhost/wholesalepeople/public/binjar', '::1', '2018-07-20 05:13:36', '2018-07-20 05:13:36'),
(483, 14, NULL, NULL, 'http://localhost/wholesalepeople/public/binjar', '::1', '2018-07-20 05:16:13', '2018-07-20 05:16:13'),
(484, 14, 1, 'item_details_page', 'http://localhost/wholesalepeople/public/item/details/14', '::1', '2018-07-20 05:16:19', '2018-07-20 05:16:19'),
(485, 14, 1, 'item_details_page', 'http://localhost/wholesalepeople/public/item/details/14', '::1', '2018-07-20 05:17:17', '2018-07-20 05:17:17'),
(486, 14, NULL, NULL, 'http://localhost/wholesalepeople/public/binjar', '::1', '2018-07-20 05:17:26', '2018-07-20 05:17:26'),
(487, 14, 1, 'item_details_page', 'http://localhost/wholesalepeople/public/item/details/14', '::1', '2018-07-20 05:17:32', '2018-07-20 05:17:32'),
(488, 14, NULL, NULL, 'http://localhost/wholesalepeople/public/binjar', '::1', '2018-07-20 05:57:27', '2018-07-20 05:57:27'),
(489, 14, NULL, NULL, 'http://localhost/wholesalepeople/public/binjar', '::1', '2018-07-20 05:58:32', '2018-07-20 05:58:32'),
(490, 14, NULL, NULL, 'http://localhost/wholesalepeople/public/binjar', '::1', '2018-07-20 05:58:33', '2018-07-20 05:58:33'),
(491, 14, NULL, NULL, 'http://localhost/wholesalepeople/public/binjar', '::1', '2018-07-20 05:58:34', '2018-07-20 05:58:34'),
(492, 14, NULL, NULL, 'http://localhost/wholesalepeople/public/binjar', '::1', '2018-07-20 05:59:36', '2018-07-20 05:59:36'),
(493, 14, NULL, NULL, 'http://localhost/wholesalepeople/public/binjar', '::1', '2018-07-20 05:59:37', '2018-07-20 05:59:37'),
(494, 14, NULL, NULL, 'http://localhost/wholesalepeople/public/binjar', '::1', '2018-07-20 05:59:38', '2018-07-20 05:59:38'),
(495, 14, NULL, NULL, 'http://localhost/wholesalepeople/public/binjar', '::1', '2018-07-20 05:59:38', '2018-07-20 05:59:38'),
(496, 14, NULL, NULL, 'http://localhost/wholesalepeople/public/binjar', '::1', '2018-07-20 06:00:47', '2018-07-20 06:00:47'),
(497, 14, NULL, NULL, 'http://localhost/wholesalepeople/public/binjar', '::1', '2018-07-20 06:00:56', '2018-07-20 06:00:56'),
(498, 14, 1, 'item_details_page', 'http://localhost/wholesalepeople/public/item/details/17', '::1', '2018-07-20 06:01:20', '2018-07-20 06:01:20'),
(499, 14, 1, 'item_details_page', 'http://localhost/wholesalepeople/public/item/details/15', '::1', '2018-07-28 00:23:26', '2018-07-28 00:23:26'),
(500, 14, 1, 'item_details_page', 'http://localhost/wholesalepeople/public/item/details/15', '::1', '2018-07-28 00:23:31', '2018-07-28 00:23:31'),
(501, 14, 1, 'item_details_page', 'http://localhost/wholesalepeople/public/item/details/14', '::1', '2018-07-28 00:23:35', '2018-07-28 00:23:35'),
(502, 14, 1, 'item_details_page', 'http://localhost/wholesalepeople/public/item/details/14', '::1', '2018-07-28 00:23:37', '2018-07-28 00:23:37'),
(503, 22, NULL, NULL, 'http://localhost/wholesalepeople/public/binjar', '::1', '2018-07-30 19:40:00', '2018-07-30 19:40:00'),
(504, 14, 1, 'item_details_page', 'http://localhost/wholesalepeople/public/item/details/14', '::1', '2018-07-30 21:51:01', '2018-07-30 21:51:01'),
(505, 14, 1, 'item_details_page', 'http://localhost/wholesalepeople/public/item/details/14', '::1', '2018-07-30 21:51:04', '2018-07-30 21:51:04'),
(506, 14, NULL, NULL, 'http://localhost/wholesalepeople/public/binjar', '::1', '2018-08-01 03:00:58', '2018-08-01 03:00:58'),
(507, 14, NULL, NULL, 'http://localhost/wholesalepeople/public/binjar', '::1', '2018-08-01 03:01:02', '2018-08-01 03:01:02'),
(508, 14, 1, 'item_details_page', 'http://localhost/wholesalepeople/public/item/details/14', '::1', '2018-08-01 03:01:03', '2018-08-01 03:01:03'),
(509, 14, 1, 'item_details_page', 'http://localhost/wholesalepeople/public/item/details/14', '::1', '2018-08-01 03:01:06', '2018-08-01 03:01:06'),
(510, 14, 1, 'item_details_page', 'http://localhost/wholesalepeople/public/item/details/16', '::1', '2018-08-01 19:08:47', '2018-08-01 19:08:47'),
(511, 14, 1, 'item_details_page', 'http://localhost/wholesalepeople/public/item/details/16', '::1', '2018-08-01 19:08:49', '2018-08-01 19:08:49'),
(512, 14, NULL, NULL, 'http://localhost/wholesalepeople/public/binjar', '::1', '2018-08-01 20:22:32', '2018-08-01 20:22:32'),
(513, 14, 1, 'item_details_page', 'http://localhost/wholesalepeople/public/item/details/14', '::1', '2018-08-01 20:22:34', '2018-08-01 20:22:34'),
(514, 14, 1, 'item_details_page', 'http://localhost/wholesalepeople/public/item/details/14', '::1', '2018-08-01 20:22:37', '2018-08-01 20:22:37'),
(515, 14, 1, 'item_details_page', 'http://localhost/wholesalepeople/public/item/details/16', '::1', '2018-08-01 20:22:41', '2018-08-01 20:22:41'),
(516, 14, 1, 'item_details_page', 'http://localhost/wholesalepeople/public/item/details/16', '::1', '2018-08-01 20:22:43', '2018-08-01 20:22:43'),
(517, 14, NULL, NULL, 'http://localhost/wholesalepeople/public/yellow-232', '::1', '2018-08-01 20:22:52', '2018-08-01 20:22:52'),
(518, 14, 2, 'item_details_page', 'http://localhost/wholesalepeople/public/item/details/19', '::1', '2018-08-01 20:22:54', '2018-08-01 20:22:54'),
(519, 14, 2, 'item_details_page', 'http://localhost/wholesalepeople/public/item/details/19', '::1', '2018-08-01 20:22:56', '2018-08-01 20:22:56'),
(520, 14, NULL, NULL, 'http://localhost/wholesalepeople/public/yellow-232', '::1', '2018-08-01 20:23:03', '2018-08-01 20:23:03'),
(521, 14, 2, 'item_details_page', 'http://localhost/wholesalepeople/public/item/details/18', '::1', '2018-08-01 20:23:04', '2018-08-01 20:23:04'),
(522, 14, 2, 'item_details_page', 'http://localhost/wholesalepeople/public/item/details/18', '::1', '2018-08-01 20:23:07', '2018-08-01 20:23:07'),
(523, 14, 1, 'item_details_page', 'http://localhost/wholesalepeople/public/item/details/16', '::1', '2018-08-01 21:00:08', '2018-08-01 21:00:08'),
(524, 14, 1, 'item_details_page', 'http://localhost/wholesalepeople/public/item/details/16', '::1', '2018-08-01 21:00:12', '2018-08-01 21:00:12'),
(525, 14, 1, 'item_details_page', 'http://localhost/wholesalepeople/public/item/details/16', '::1', '2018-08-01 21:18:48', '2018-08-01 21:18:48'),
(526, 14, NULL, NULL, 'http://localhost/wholesalepeople/public/binjar', '::1', '2018-08-02 01:08:57', '2018-08-02 01:08:57'),
(527, 14, 1, 'item_details_page', 'http://localhost/wholesalepeople/public/item/details/14', '::1', '2018-08-02 01:08:58', '2018-08-02 01:08:58'),
(528, 14, 1, 'item_details_page', 'http://localhost/wholesalepeople/public/item/details/14', '::1', '2018-08-02 01:09:02', '2018-08-02 01:09:02'),
(529, 14, 1, 'item_details_page', 'http://localhost/wholesalepeople/public/item/details/14', '::1', '2018-08-02 01:44:38', '2018-08-02 01:44:38'),
(530, 14, 1, 'item_details_page', 'http://localhost/wholesalepeople/public/item/details/14', '::1', '2018-08-02 01:45:17', '2018-08-02 01:45:17'),
(531, 14, 1, 'item_details_page', 'http://localhost/wholesalepeople/public/item/details/14', '::1', '2018-08-02 01:45:30', '2018-08-02 01:45:30'),
(532, 14, NULL, NULL, 'http://localhost/wholesalepeople/public/yellow-232', '::1', '2018-08-02 19:55:12', '2018-08-02 19:55:12'),
(533, 14, 2, 'item_details_page', 'http://localhost/wholesalepeople/public/item/details/19', '::1', '2018-08-02 19:55:13', '2018-08-02 19:55:13'),
(534, 14, 2, 'item_details_page', 'http://localhost/wholesalepeople/public/item/details/19', '::1', '2018-08-02 19:55:16', '2018-08-02 19:55:16'),
(535, 14, NULL, NULL, 'http://localhost/wholesalepeople/public/yellow-232', '::1', '2018-08-02 19:55:20', '2018-08-02 19:55:20'),
(536, 14, 2, 'item_details_page', 'http://localhost/wholesalepeople/public/item/details/18', '::1', '2018-08-02 19:55:21', '2018-08-02 19:55:21'),
(537, 14, 2, 'item_details_page', 'http://localhost/wholesalepeople/public/item/details/18', '::1', '2018-08-02 19:55:23', '2018-08-02 19:55:23'),
(538, NULL, NULL, NULL, 'http://localhost/wholesalepeople/public/yellow-232', '::1', '2018-08-05 22:29:25', '2018-08-05 22:29:25'),
(539, NULL, NULL, NULL, 'http://localhost/wholesalepeople/public/binjar', '::1', '2018-08-05 22:44:39', '2018-08-05 22:44:39'),
(540, NULL, NULL, NULL, 'http://localhost/wholesalepeople/public/yellow-232', '::1', '2018-08-05 22:51:31', '2018-08-05 22:51:31'),
(541, NULL, NULL, NULL, 'http://localhost/wholesalepeople/public/yellow-232', '::1', '2018-08-05 22:51:38', '2018-08-05 22:51:38'),
(542, 14, 2, 'item_details_page', 'http://localhost/wholesalepeople/public/item/details/19', '::1', '2018-08-06 04:11:58', '2018-08-06 04:11:58'),
(543, 14, 2, 'item_details_page', 'http://localhost/wholesalepeople/public/item/details/18', '::1', '2018-08-06 04:12:09', '2018-08-06 04:12:09'),
(544, 14, 2, 'item_details_page', 'http://localhost/wholesalepeople/public/item/details/18', '::1', '2018-08-06 04:13:07', '2018-08-06 04:13:07'),
(545, 14, 2, 'item_details_page', 'http://localhost/wholesalepeople/public/item/details/18', '::1', '2018-08-06 04:13:45', '2018-08-06 04:13:45'),
(546, 14, 2, 'item_details_page', 'http://localhost/wholesalepeople/public/item/details/18', '::1', '2018-08-06 04:13:51', '2018-08-06 04:13:51'),
(547, 14, 2, 'item_details_page', 'http://localhost/wholesalepeople/public/item/details/18', '::1', '2018-08-06 04:13:52', '2018-08-06 04:13:52'),
(548, 14, 2, 'item_details_page', 'http://localhost/wholesalepeople/public/item/details/18', '::1', '2018-08-06 04:13:53', '2018-08-06 04:13:53'),
(549, 14, 2, 'item_details_page', 'http://localhost/wholesalepeople/public/item/details/18', '::1', '2018-08-06 04:13:54', '2018-08-06 04:13:54'),
(550, 14, 2, 'item_details_page', 'http://localhost/wholesalepeople/public/item/details/19', '::1', '2018-08-06 04:14:02', '2018-08-06 04:14:02'),
(551, 14, 2, 'item_details_page', 'http://localhost/wholesalepeople/public/item/details/19', '::1', '2018-08-06 04:14:17', '2018-08-06 04:14:17'),
(552, 14, 2, 'item_details_page', 'http://localhost/wholesalepeople/public/item/details/19', '::1', '2018-08-06 04:14:20', '2018-08-06 04:14:20'),
(553, 14, 2, 'item_details_page', 'http://localhost/wholesalepeople/public/item/details/19', '::1', '2018-08-06 04:15:11', '2018-08-06 04:15:11'),
(554, 14, 2, 'item_details_page', 'http://localhost/wholesalepeople/public/item/details/18', '::1', '2018-08-06 04:15:14', '2018-08-06 04:15:14'),
(555, 14, NULL, NULL, 'http://localhost/wholesalepeople/public/yellow-232', '::1', '2018-08-06 04:15:35', '2018-08-06 04:15:35'),
(556, 14, 2, 'item_details_page', 'http://localhost/wholesalepeople/public/item/details/19', '::1', '2018-08-06 04:15:46', '2018-08-06 04:15:46'),
(557, 14, 2, 'item_details_page', 'http://localhost/wholesalepeople/public/item/details/18', '::1', '2018-08-06 04:15:48', '2018-08-06 04:15:48'),
(558, 14, 2, 'item_details_page', 'http://localhost/wholesalepeople/public/item/details/18', '::1', '2018-08-06 04:16:07', '2018-08-06 04:16:07'),
(559, 14, 2, 'item_details_page', 'http://localhost/wholesalepeople/public/item/details/18', '::1', '2018-08-06 04:16:08', '2018-08-06 04:16:08'),
(560, 14, 2, 'item_details_page', 'http://localhost/wholesalepeople/public/item/details/18', '::1', '2018-08-06 04:16:14', '2018-08-06 04:16:14'),
(561, 14, 2, 'item_details_page', 'http://localhost/wholesalepeople/public/item/details/18', '::1', '2018-08-06 04:16:15', '2018-08-06 04:16:15'),
(562, 14, 2, 'item_details_page', 'http://localhost/wholesalepeople/public/item/details/18', '::1', '2018-08-06 04:17:35', '2018-08-06 04:17:35'),
(563, 14, 2, 'item_details_page', 'http://localhost/wholesalepeople/public/item/details/19', '::1', '2018-08-06 04:17:37', '2018-08-06 04:17:37'),
(564, 14, 2, 'item_details_page', 'http://localhost/wholesalepeople/public/item/details/18', '::1', '2018-08-06 04:17:38', '2018-08-06 04:17:38'),
(565, 14, 2, 'item_details_page', 'http://localhost/wholesalepeople/public/item/details/19', '::1', '2018-08-06 04:17:42', '2018-08-06 04:17:42'),
(566, 14, 2, 'item_details_page', 'http://localhost/wholesalepeople/public/item/details/18', '::1', '2018-08-06 04:48:23', '2018-08-06 04:48:23'),
(567, 14, 2, 'item_details_page', 'http://localhost/wholesalepeople/public/item/details/18', '::1', '2018-08-06 04:51:37', '2018-08-06 04:51:37'),
(568, 14, 2, 'item_details_page', 'http://localhost/wholesalepeople/public/item/details/18', '::1', '2018-08-06 04:51:44', '2018-08-06 04:51:44'),
(569, 14, 1, 'item_details_page', 'http://localhost/wholesalepeople/public/item/details/14', '::1', '2018-08-06 04:51:48', '2018-08-06 04:51:48'),
(570, 14, 1, 'item_details_page', 'http://localhost/wholesalepeople/public/item/details/14', '::1', '2018-08-06 04:51:53', '2018-08-06 04:51:53'),
(571, 14, 1, 'item_details_page', 'http://localhost/wholesalepeople/public/item/details/14', '::1', '2018-08-06 04:52:07', '2018-08-06 04:52:07'),
(572, 14, 1, 'item_details_page', 'http://localhost/wholesalepeople/public/item/details/15', '::1', '2018-08-06 04:52:10', '2018-08-06 04:52:10'),
(573, 14, 1, 'item_details_page', 'http://localhost/wholesalepeople/public/item/details/15', '::1', '2018-08-06 04:52:15', '2018-08-06 04:52:15'),
(574, 14, 1, 'item_details_page', 'http://localhost/wholesalepeople/public/item/details/14', '::1', '2018-08-06 04:52:34', '2018-08-06 04:52:34'),
(575, 14, 1, 'item_details_page', 'http://localhost/wholesalepeople/public/item/details/14', '::1', '2018-08-06 04:52:39', '2018-08-06 04:52:39'),
(576, 14, 2, 'item_details_page', 'http://localhost/wholesalepeople/public/item/details/18', '::1', '2018-08-06 04:53:44', '2018-08-06 04:53:44'),
(577, 14, 2, 'item_details_page', 'http://localhost/wholesalepeople/public/item/details/19', '::1', '2018-08-06 04:54:46', '2018-08-06 04:54:46'),
(578, 14, 1, 'item_details_page', 'http://localhost/wholesalepeople/public/item/details/14', '::1', '2018-08-06 04:56:12', '2018-08-06 04:56:12'),
(579, 14, 2, 'item_details_page', 'http://localhost/wholesalepeople/public/item/details/19', '::1', '2018-08-06 05:07:25', '2018-08-06 05:07:25'),
(580, 14, 2, 'item_details_page', 'http://localhost/wholesalepeople/public/item/details/18', '::1', '2018-08-06 05:07:27', '2018-08-06 05:07:27'),
(581, 14, 2, 'item_details_page', 'http://localhost/wholesalepeople/public/item/details/19', '::1', '2018-08-06 05:07:28', '2018-08-06 05:07:28'),
(582, 14, 2, 'item_details_page', 'http://localhost/wholesalepeople/public/item/details/18', '::1', '2018-08-06 05:07:29', '2018-08-06 05:07:29'),
(583, 14, 1, 'item_details_page', 'http://localhost/wholesalepeople/public/item/details/14', '::1', '2018-08-06 05:07:31', '2018-08-06 05:07:31'),
(584, 14, 1, 'item_details_page', 'http://localhost/wholesalepeople/public/item/details/95', '::1', '2018-08-06 05:07:32', '2018-08-06 05:07:32'),
(585, 14, 1, 'item_details_page', 'http://localhost/wholesalepeople/public/item/details/14', '::1', '2018-08-06 05:07:33', '2018-08-06 05:07:33'),
(586, 14, 1, 'item_details_page', 'http://localhost/wholesalepeople/public/item/details/77', '::1', '2018-08-06 05:07:34', '2018-08-06 05:07:34'),
(587, 14, 1, 'item_details_page', 'http://localhost/wholesalepeople/public/item/details/78', '::1', '2018-08-06 05:07:35', '2018-08-06 05:07:35'),
(588, 14, 1, 'item_details_page', 'http://localhost/wholesalepeople/public/item/details/91', '::1', '2018-08-06 05:07:36', '2018-08-06 05:07:36'),
(589, 14, 1, 'item_details_page', 'http://localhost/wholesalepeople/public/item/details/92', '::1', '2018-08-06 05:07:37', '2018-08-06 05:07:37'),
(590, 14, 1, 'item_details_page', 'http://localhost/wholesalepeople/public/item/details/67', '::1', '2018-08-06 05:07:39', '2018-08-06 05:07:39'),
(591, 14, 1, 'item_details_page', 'http://localhost/wholesalepeople/public/item/details/15', '::1', '2018-08-06 05:07:41', '2018-08-06 05:07:41'),
(592, 14, 1, 'item_details_page', 'http://localhost/wholesalepeople/public/item/details/17', '::1', '2018-08-06 05:07:42', '2018-08-06 05:07:42'),
(593, 14, NULL, NULL, 'http://localhost/wholesalepeople/public/yellow-232', '::1', '2018-08-06 19:31:32', '2018-08-06 19:31:32'),
(594, 14, NULL, NULL, 'http://localhost/wholesalepeople/public/binjar', '::1', '2018-08-06 19:31:37', '2018-08-06 19:31:37'),
(595, 14, 1, 'item_details_page', 'http://localhost/wholesalepeople/public/item/details/14', '::1', '2018-08-06 19:32:01', '2018-08-06 19:32:01'),
(596, 14, 1, 'item_details_page', 'http://localhost/wholesalepeople/public/item/details/14', '::1', '2018-08-06 19:32:04', '2018-08-06 19:32:04'),
(597, 14, 1, 'item_details_page', 'http://localhost/wholesalepeople/public/item/details/17', '::1', '2018-08-06 19:32:08', '2018-08-06 19:32:08'),
(598, 14, 1, 'item_details_page', 'http://localhost/wholesalepeople/public/item/details/17', '::1', '2018-08-06 19:32:10', '2018-08-06 19:32:10'),
(599, 14, NULL, NULL, 'http://localhost/wholesalepeople/public/binjar', '::1', '2018-08-07 00:54:34', '2018-08-07 00:54:34'),
(600, 14, NULL, NULL, 'http://localhost/wholesalepeople/public/yellow-232', '::1', '2018-08-07 00:54:44', '2018-08-07 00:54:44'),
(601, 14, NULL, NULL, 'http://localhost/wholesalepeople/public/binjar', '::1', '2018-08-07 00:56:15', '2018-08-07 00:56:15'),
(602, 14, NULL, NULL, 'http://localhost/wholesalepeople/public/binjar', '::1', '2018-08-07 00:56:45', '2018-08-07 00:56:45'),
(603, 14, NULL, NULL, 'http://localhost/wholesalepeople/public/binjar', '::1', '2018-08-07 00:57:33', '2018-08-07 00:57:33'),
(604, 14, 1, 'item_details_page', 'http://localhost/wholesalepeople/public/item/details/67', '::1', '2018-08-07 01:12:36', '2018-08-07 01:12:36'),
(605, NULL, NULL, NULL, 'http://localhost/wholesalepeople/public/binjar', '::1', '2018-08-07 01:40:17', '2018-08-07 01:40:17'),
(606, NULL, NULL, NULL, 'http://localhost/wholesalepeople/public/binjar', '::1', '2018-08-07 01:40:36', '2018-08-07 01:40:36'),
(607, NULL, NULL, NULL, 'http://localhost/wholesalepeople/public/binjar', '::1', '2018-08-07 01:46:29', '2018-08-07 01:46:29'),
(608, NULL, NULL, NULL, 'http://localhost/wholesalepeople/public/binjar', '::1', '2018-08-07 01:51:56', '2018-08-07 01:51:56'),
(609, NULL, NULL, NULL, 'http://localhost/wholesalepeople/public/binjar', '::1', '2018-08-07 01:52:31', '2018-08-07 01:52:31'),
(610, NULL, NULL, NULL, 'http://localhost/wholesalepeople/public/binjar', '::1', '2018-08-07 01:53:01', '2018-08-07 01:53:01'),
(611, 19, NULL, NULL, 'http://localhost/wholesalepeople/public/binjar', '::1', '2018-08-07 20:40:31', '2018-08-07 20:40:31'),
(612, 14, NULL, NULL, 'http://localhost/wholesalepeople/public/binjar', '::1', '2018-08-09 19:22:47', '2018-08-09 19:22:47'),
(613, 14, 1, 'item_details_page', 'http://localhost/wholesalepeople/public/item/details/14', '::1', '2018-08-09 19:22:49', '2018-08-09 19:22:49'),
(614, 14, 1, 'item_details_page', 'http://localhost/wholesalepeople/public/item/details/14', '::1', '2018-08-09 19:22:51', '2018-08-09 19:22:51'),
(615, 14, 1, 'item_details_page', 'http://localhost/wholesalepeople/public/item/details/17', '::1', '2018-08-09 23:55:19', '2018-08-09 23:55:19'),
(616, 14, 2, 'item_details_page', 'http://localhost/wholesalepeople/public/item/details/19', '::1', '2018-08-09 23:55:28', '2018-08-09 23:55:28'),
(617, 14, 1, 'item_details_page', 'http://localhost/wholesalepeople/public/item/details/14', '::1', '2018-08-15 20:29:20', '2018-08-15 20:29:20'),
(618, 14, 1, 'item_details_page', 'http://localhost/wholesalepeople/public/item/details/14', '::1', '2018-08-15 20:29:50', '2018-08-15 20:29:50'),
(619, 14, 1, 'item_details_page', 'http://localhost/wholesalepeople/public/item/details/67', '::1', '2018-08-15 20:29:52', '2018-08-15 20:29:52'),
(620, 14, 1, 'item_details_page', 'http://localhost/wholesalepeople/public/item/details/14', '::1', '2018-08-15 20:35:25', '2018-08-15 20:35:25'),
(621, 14, 1, 'item_details_page', 'http://localhost/wholesalepeople/public/item/details/14', '::1', '2018-08-15 20:35:44', '2018-08-15 20:35:44'),
(622, 14, 1, 'item_details_page', 'http://localhost/wholesalepeople/public/item/details/14', '::1', '2018-08-15 21:05:57', '2018-08-15 21:05:57'),
(623, 14, 1, 'item_details_page', 'http://localhost/wholesalepeople/public/item/details/67', '::1', '2018-08-15 21:06:00', '2018-08-15 21:06:00'),
(624, 14, 1, 'item_details_page', 'http://localhost/wholesalepeople/public/item/details/67', '::1', '2018-08-15 21:13:50', '2018-08-15 21:13:50'),
(625, 14, 1, 'item_details_page', 'http://localhost/wholesalepeople/public/item/details/67', '::1', '2018-08-15 21:39:42', '2018-08-15 21:39:42'),
(626, 14, 1, 'item_details_page', 'http://localhost/wholesalepeople/public/item/details/14', '::1', '2018-08-15 21:41:37', '2018-08-15 21:41:37'),
(627, 14, 1, 'item_details_page', 'http://localhost/wholesalepeople/public/item/details/15', '::1', '2018-08-15 21:41:42', '2018-08-15 21:41:42'),
(628, NULL, NULL, NULL, 'http://localhost/wholesalepeople/public/binjar', '::1', '2018-08-26 03:47:39', '2018-08-26 03:47:39'),
(629, NULL, NULL, NULL, 'http://localhost/wholesalepeople/public/binjar', '::1', '2018-08-26 03:56:34', '2018-08-26 03:56:34'),
(630, 14, 1, 'item_details_page', 'http://localhost/wholesalepeople/public/item/details/14', '::1', '2018-09-04 20:04:41', '2018-09-04 20:04:41'),
(631, 14, 1, 'item_details_page', 'http://localhost/wholesalepeople/public/item/details/77', '::1', '2018-09-04 20:04:44', '2018-09-04 20:04:44'),
(632, 14, 1, 'item_details_page', 'http://localhost/wholesalepeople/public/item/details/67', '::1', '2018-09-04 20:04:45', '2018-09-04 20:04:45'),
(633, 14, 1, 'item_details_page', 'http://localhost/wholesalepeople/public/item/details/14', '::1', '2018-09-04 20:04:49', '2018-09-04 20:04:49'),
(634, 14, 1, 'item_details_page', 'http://localhost/wholesalepeople/public/item/details/77', '::1', '2018-09-04 20:04:53', '2018-09-04 20:04:53'),
(635, 14, 1, 'item_details_page', 'http://localhost/wholesalepeople/public/item/details/67', '::1', '2018-09-04 20:04:55', '2018-09-04 20:04:55'),
(636, 14, 1, 'item_details_page', 'http://localhost/wholesalepeople/public/item/details/14', '::1', '2018-09-04 20:15:17', '2018-09-04 20:15:17'),
(637, 14, 1, 'item_details_page', 'http://localhost/wholesalepeople/public/item/details/112', '::1', '2018-09-04 20:15:20', '2018-09-04 20:15:20'),
(638, 14, 1, 'item_details_page', 'http://localhost/wholesalepeople/public/item/details/15', '::1', '2018-09-13 20:27:35', '2018-09-13 20:27:35'),
(639, 14, 1, 'item_details_page', 'http://localhost/wholesalepeople/public/item/details/15', '::1', '2018-09-13 20:27:39', '2018-09-13 20:27:39'),
(640, 14, 1, 'item_details_page', 'http://localhost/wholesalepeople/public/item/details/15', '::1', '2018-09-13 20:32:47', '2018-09-13 20:32:47'),
(641, 14, 1, 'item_details_page', 'http://localhost/wholesalepeople/public/item/details/15', '::1', '2018-09-13 20:32:50', '2018-09-13 20:32:50'),
(642, 14, 1, 'item_details_page', 'http://localhost/wholesalepeople/public/item/details/17', '::1', '2018-09-13 23:18:40', '2018-09-13 23:18:40'),
(643, 14, 1, 'item_details_page', 'http://localhost/wholesalepeople/public/item/details/17', '::1', '2018-09-13 23:18:42', '2018-09-13 23:18:42'),
(644, 14, 1, 'item_details_page', 'http://localhost/wholesalepeople/public/item/details/15', '::1', '2018-09-17 20:24:09', '2018-09-17 20:24:09'),
(645, 14, 1, 'item_details_page', 'http://localhost/wholesalepeople/public/item/details/15', '::1', '2018-09-17 20:24:13', '2018-09-17 20:24:13'),
(646, 14, 1, 'item_details_page', 'http://localhost/wholesalepeople/public/item/details/15', '::1', '2018-09-17 20:47:53', '2018-09-17 20:47:53'),
(647, 14, 1, 'item_details_page', 'http://localhost/wholesalepeople/public/item/details/15', '::1', '2018-09-17 20:47:56', '2018-09-17 20:47:56'),
(648, 14, 1, 'item_details_page', 'http://localhost/wholesalepeople/public/item/details/15', '::1', '2018-09-17 21:21:05', '2018-09-17 21:21:05'),
(649, 14, 1, 'item_details_page', 'http://localhost/wholesalepeople/public/item/details/15', '::1', '2018-09-17 21:21:09', '2018-09-17 21:21:09'),
(650, 14, 1, 'item_details_page', 'http://localhost/wholesalepeople/public/item/details/67', '::1', '2018-09-18 23:57:48', '2018-09-18 23:57:48'),
(651, 14, 2, 'item_details_page', 'http://localhost/wholesalepeople/public/item/details/18', '::1', '2018-09-18 23:59:54', '2018-09-18 23:59:54'),
(652, 14, 2, 'item_details_page', 'http://localhost/wholesalepeople/public/item/details/18', '::1', '2018-09-19 00:00:01', '2018-09-19 00:00:01'),
(653, 14, NULL, NULL, 'http://localhost/wholesalepeople/public/yellow-232', '::1', '2018-09-19 00:00:09', '2018-09-19 00:00:09'),
(654, 14, NULL, NULL, 'http://localhost/wholesalepeople/public/yellow-232', '::1', '2018-09-19 00:03:31', '2018-09-19 00:03:31'),
(655, 14, 2, 'item_details_page', 'http://localhost/wholesalepeople/public/item/details/18', '::1', '2018-09-19 00:03:48', '2018-09-19 00:03:48'),
(656, 14, 1, 'item_details_page', 'http://localhost/wholesalepeople/public/item/details/14', '::1', '2018-09-19 00:05:09', '2018-09-19 00:05:09'),
(657, 14, 1, 'item_details_page', 'http://localhost/wholesalepeople/public/item/details/14', '::1', '2018-09-19 00:08:40', '2018-09-19 00:08:40'),
(658, 14, 1, 'item_details_page', 'http://localhost/wholesalepeople/public/item/details/14', '::1', '2018-09-19 00:08:46', '2018-09-19 00:08:46'),
(659, 14, 1, 'item_details_page', 'http://localhost/wholesalepeople/public/item/details/14', '::1', '2018-09-19 00:09:13', '2018-09-19 00:09:13'),
(660, 14, 1, 'item_details_page', 'http://localhost/wholesalepeople/public/item/details/14', '::1', '2018-09-19 00:09:16', '2018-09-19 00:09:16'),
(661, 14, 1, 'item_details_page', 'http://localhost/wholesalepeople/public/item/details/14', '::1', '2018-09-19 00:09:47', '2018-09-19 00:09:47'),
(662, 14, 1, 'item_details_page', 'http://localhost/wholesalepeople/public/item/details/14', '::1', '2018-09-19 00:09:50', '2018-09-19 00:09:50'),
(663, 14, 1, 'item_details_page', 'http://localhost/wholesalepeople/public/item/details/14', '::1', '2018-09-19 00:09:53', '2018-09-19 00:09:53'),
(664, 14, 1, 'item_details_page', 'http://localhost/wholesalepeople/public/item/details/14', '::1', '2018-09-19 00:09:55', '2018-09-19 00:09:55'),
(665, 14, 1, 'item_details_page', 'http://localhost/wholesalepeople/public/item/details/14', '::1', '2018-09-19 00:11:11', '2018-09-19 00:11:11'),
(666, 14, 1, 'item_details_page', 'http://localhost/wholesalepeople/public/item/details/14', '::1', '2018-09-19 00:11:39', '2018-09-19 00:11:39'),
(667, 14, 1, 'item_details_page', 'http://localhost/wholesalepeople/public/item/details/14', '::1', '2018-09-19 00:11:55', '2018-09-19 00:11:55'),
(668, 14, 1, 'item_details_page', 'http://localhost/wholesalepeople/public/item/details/14', '::1', '2018-09-19 00:12:11', '2018-09-19 00:12:11'),
(669, 14, 1, 'item_details_page', 'http://localhost/wholesalepeople/public/item/details/14', '::1', '2018-09-19 00:12:30', '2018-09-19 00:12:30'),
(670, 14, 1, 'item_details_page', 'http://localhost/wholesalepeople/public/item/details/14', '::1', '2018-09-19 00:12:32', '2018-09-19 00:12:32'),
(671, 14, 1, 'item_details_page', 'http://localhost/wholesalepeople/public/item/details/14', '::1', '2018-09-19 00:12:47', '2018-09-19 00:12:47'),
(672, 14, 1, 'item_details_page', 'http://localhost/wholesalepeople/public/item/details/14', '::1', '2018-09-19 00:13:20', '2018-09-19 00:13:20'),
(673, 14, 1, 'item_details_page', 'http://localhost/wholesalepeople/public/item/details/14', '::1', '2018-09-19 00:15:04', '2018-09-19 00:15:04'),
(674, 14, 1, 'item_details_page', 'http://localhost/wholesalepeople/public/item/details/14', '::1', '2018-09-19 00:15:06', '2018-09-19 00:15:06'),
(675, 14, 1, 'item_details_page', 'http://localhost/wholesalepeople/public/item/details/14', '::1', '2018-09-19 00:17:25', '2018-09-19 00:17:25'),
(676, 14, 1, 'item_details_page', 'http://localhost/wholesalepeople/public/item/details/14', '::1', '2018-09-19 00:18:24', '2018-09-19 00:18:24'),
(677, 14, 1, 'item_details_page', 'http://localhost/wholesalepeople/public/item/details/14', '::1', '2018-09-19 00:18:27', '2018-09-19 00:18:27'),
(678, 14, 1, 'item_details_page', 'http://localhost/wholesalepeople/public/item/details/14', '::1', '2018-09-19 00:18:55', '2018-09-19 00:18:55'),
(679, 14, 1, 'item_details_page', 'http://localhost/wholesalepeople/public/item/details/14', '::1', '2018-09-19 00:19:06', '2018-09-19 00:19:06'),
(680, 14, 1, 'item_details_page', 'http://localhost/wholesalepeople/public/item/details/67', '::1', '2018-09-19 00:21:09', '2018-09-19 00:21:09'),
(681, 14, 2, 'item_details_page', 'http://localhost/wholesalepeople/public/item/details/18', '::1', '2018-09-19 00:21:29', '2018-09-19 00:21:29'),
(682, 14, 1, 'item_details_page', 'http://localhost/wholesalepeople/public/item/details/67', '::1', '2018-09-19 00:21:34', '2018-09-19 00:21:34'),
(683, 14, 2, 'item_details_page', 'http://localhost/wholesalepeople/public/item/details/18', '::1', '2018-09-19 00:21:41', '2018-09-19 00:21:41'),
(684, 14, 1, 'item_details_page', 'http://localhost/wholesalepeople/public/item/details/67', '::1', '2018-09-19 00:21:46', '2018-09-19 00:21:46'),
(685, 14, 2, 'item_details_page', 'http://localhost/wholesalepeople/public/item/details/18', '::1', '2018-09-19 00:21:53', '2018-09-19 00:21:53'),
(686, 14, 1, 'item_details_page', 'http://localhost/wholesalepeople/public/item/details/67', '::1', '2018-09-19 00:22:53', '2018-09-19 00:22:53'),
(687, 14, 1, 'item_details_page', 'http://localhost/wholesalepeople/public/item/details/67', '::1', '2018-09-19 00:24:31', '2018-09-19 00:24:31'),
(688, 14, 1, 'item_details_page', 'http://localhost/wholesalepeople/public/item/details/15', '::1', '2018-09-19 00:24:51', '2018-09-19 00:24:51'),
(689, 14, 1, 'item_details_page', 'http://localhost/wholesalepeople/public/item/details/78', '::1', '2018-09-19 00:26:58', '2018-09-19 00:26:58'),
(690, 14, 1, 'item_details_page', 'http://localhost/wholesalepeople/public/item/details/15', '::1', '2018-09-19 01:03:20', '2018-09-19 01:03:20'),
(691, 14, 1, 'item_details_page', 'http://localhost/wholesalepeople/public/item/details/14', '::1', '2018-09-19 20:57:50', '2018-09-19 20:57:50'),
(692, 14, 1, 'item_details_page', 'http://localhost/wholesalepeople/public/item/details/91', '::1', '2018-09-19 20:58:52', '2018-09-19 20:58:52'),
(693, 14, 1, 'item_details_page', 'http://localhost/wholesalepeople/public/item/details/91', '::1', '2018-09-19 20:58:59', '2018-09-19 20:58:59'),
(694, 14, 1, 'item_details_page', 'http://localhost/wholesalepeople/public/item/details/67', '::1', '2018-09-19 20:59:16', '2018-09-19 20:59:16'),
(695, 14, 1, 'item_details_page', 'http://localhost/wholesalepeople/public/item/details/67', '::1', '2018-09-19 20:59:20', '2018-09-19 20:59:20'),
(696, 14, 1, 'item_details_page', 'http://localhost/wholesalepeople/public/item/details/17', '::1', '2018-09-19 21:06:02', '2018-09-19 21:06:02'),
(697, 14, 1, 'item_details_page', 'http://localhost/wholesalepeople/public/item/details/17', '::1', '2018-09-19 21:09:36', '2018-09-19 21:09:36'),
(698, 14, 1, 'item_details_page', 'http://localhost/wholesalepeople/public/item/details/15', '::1', '2018-09-19 21:09:40', '2018-09-19 21:09:40'),
(699, 14, 1, 'item_details_page', 'http://localhost/wholesalepeople/public/item/details/17', '::1', '2018-09-19 21:10:56', '2018-09-19 21:10:56'),
(700, 14, 1, 'item_details_page', 'http://localhost/wholesalepeople/public/item/details/67', '::1', '2018-09-19 21:14:54', '2018-09-19 21:14:54'),
(701, 14, 1, 'item_details_page', 'http://localhost/wholesalepeople/public/item/details/67', '::1', '2018-09-19 21:14:58', '2018-09-19 21:14:58'),
(702, 14, 1, 'item_details_page', 'http://localhost/wholesalepeople/public/item/details/91', '::1', '2018-09-19 21:18:39', '2018-09-19 21:18:39'),
(703, 14, 2, 'item_details_page', 'http://localhost/wholesalepeople/public/item/details/18', '::1', '2018-09-19 21:19:20', '2018-09-19 21:19:20'),
(704, 14, 2, 'item_details_page', 'http://localhost/wholesalepeople/public/item/details/18', '::1', '2018-09-19 21:19:30', '2018-09-19 21:19:30'),
(705, 14, 1, 'item_details_page', 'http://localhost/wholesalepeople/public/item/details/67', '::1', '2018-09-19 21:19:45', '2018-09-19 21:19:45'),
(706, 14, 1, 'item_details_page', 'http://localhost/wholesalepeople/public/item/details/67', '::1', '2018-09-19 21:19:47', '2018-09-19 21:19:47'),
(707, 14, 1, 'item_details_page', 'http://localhost/wholesalepeople/public/item/details/17', '::1', '2018-09-19 21:25:53', '2018-09-19 21:25:53'),
(708, 14, 1, 'item_details_page', 'http://localhost/wholesalepeople/public/item/details/17', '::1', '2018-09-19 21:25:56', '2018-09-19 21:25:56'),
(709, 14, 1, 'item_details_page', 'http://localhost/wholesalepeople/public/item/details/78', '::1', '2018-09-19 21:28:29', '2018-09-19 21:28:29'),
(710, 14, 1, 'item_details_page', 'http://localhost/wholesalepeople/public/item/details/92', '::1', '2018-09-19 21:43:14', '2018-09-19 21:43:14'),
(711, 14, 1, 'item_details_page', 'http://localhost/wholesalepeople/public/item/details/92', '::1', '2018-09-19 21:43:26', '2018-09-19 21:43:26'),
(712, 14, 1, 'item_details_page', 'http://localhost/wholesalepeople/public/item/details/67', '::1', '2018-09-19 21:45:41', '2018-09-19 21:45:41'),
(713, 14, 1, 'item_details_page', 'http://localhost/wholesalepeople/public/item/details/15', '::1', '2018-09-19 22:17:05', '2018-09-19 22:17:05'),
(714, 14, NULL, NULL, 'http://localhost/wholesalepeople/public/binjar', '::1', '2018-09-19 22:48:48', '2018-09-19 22:48:48'),
(715, 14, 1, 'item_details_page', 'http://localhost/wholesalepeople/public/item/details/15', '::1', '2018-09-19 22:55:56', '2018-09-19 22:55:56'),
(716, 14, 1, 'item_details_page', 'http://localhost/wholesalepeople/public/item/details/17', '::1', '2018-09-19 23:04:40', '2018-09-19 23:04:40'),
(717, NULL, NULL, NULL, 'http://localhost/wholesalepeople/public/binjar', '::1', '2018-09-24 00:14:05', '2018-09-24 00:14:05'),
(718, NULL, NULL, NULL, 'http://localhost/wholesalepeople/public/binjar', '::1', '2018-09-24 00:17:53', '2018-09-24 00:17:53'),
(719, NULL, NULL, NULL, 'http://localhost/wholesalepeople/public/binjar', '::1', '2018-09-24 00:30:27', '2018-09-24 00:30:27'),
(720, NULL, NULL, NULL, 'http://localhost/wholesalepeople/public/binjar', '::1', '2018-09-24 00:46:53', '2018-09-24 00:46:53'),
(721, NULL, NULL, NULL, 'http://localhost/wholesalepeople/public/binjar', '::1', '2018-09-24 00:46:55', '2018-09-24 00:46:55'),
(722, NULL, NULL, NULL, 'http://localhost/wholesalepeople/public/binjar', '::1', '2018-09-24 00:46:55', '2018-09-24 00:46:55'),
(723, NULL, NULL, NULL, 'http://localhost/wholesalepeople/public/binjar', '::1', '2018-09-24 00:46:57', '2018-09-24 00:46:57'),
(724, 14, 1, 'item_details_page', 'http://localhost/wholesalepeople/public/item/details/17', '::1', '2018-09-25 21:09:09', '2018-09-25 21:09:09'),
(725, 14, 2, 'item_details_page', 'http://localhost/wholesalepeople/public/item/details/19', '::1', '2018-09-25 21:09:16', '2018-09-25 21:09:16'),
(726, 14, 1, 'item_details_page', 'http://localhost/wholesalepeople/public/item/details/78', '::1', '2018-09-25 21:09:21', '2018-09-25 21:09:21'),
(727, 14, 1, 'item_details_page', 'http://localhost/wholesalepeople/public/item/details/14', '::1', '2018-09-28 06:22:52', '2018-09-28 06:22:52'),
(728, 14, 1, 'item_details_page', 'http://localhost/wholesalepeople/public/item/details/14', '::1', '2018-09-28 06:23:24', '2018-09-28 06:23:24'),
(729, 14, NULL, NULL, 'http://localhost/wholesalepeople/public/binjar', '::1', '2018-09-28 21:52:17', '2018-09-28 21:52:17'),
(730, 14, NULL, NULL, 'http://localhost/wholesalepeople/public/binjar', '::1', '2018-09-28 23:01:30', '2018-09-28 23:01:30'),
(731, 14, NULL, NULL, 'http://localhost/wholesalepeople/public/binjar', '::1', '2018-09-30 15:22:46', '2018-09-30 15:22:46'),
(732, 14, NULL, NULL, 'http://localhost/wholesalepeople/public/binjar', '::1', '2018-09-30 15:34:00', '2018-09-30 15:34:00'),
(733, 14, 1, 'item_details_page', 'http://localhost/wholesalepeople/public/item/details/17', '::1', '2018-09-30 16:45:40', '2018-09-30 16:45:40'),
(734, 14, NULL, NULL, 'http://localhost/wholesalepeople/public/binjar', '::1', '2018-10-05 20:38:34', '2018-10-05 20:38:34'),
(735, 14, 1, 'item_details_page', 'http://localhost/wholesalepeople/public/item/details/14', '::1', '2018-10-05 20:38:36', '2018-10-05 20:38:36'),
(736, 14, 1, 'item_details_page', 'http://localhost/wholesalepeople/public/item/details/14', '::1', '2018-10-05 20:38:39', '2018-10-05 20:38:39'),
(737, 14, NULL, NULL, 'http://localhost/wholesalepeople/public/binjar', '::1', '2018-10-17 00:37:51', '2018-10-17 00:37:51'),
(738, 14, NULL, NULL, 'http://localhost/wholesalepeople/public/binjar', '::1', '2018-10-17 00:42:20', '2018-10-17 00:42:20'),
(739, 14, NULL, NULL, 'http://localhost/wholesalepeople/public/yellow-232', '::1', '2018-10-17 00:42:28', '2018-10-17 00:42:28'),
(740, 14, NULL, NULL, 'http://localhost/wholesalepeople/public/binjar', '::1', '2018-10-17 00:42:32', '2018-10-17 00:42:32'),
(741, 14, NULL, NULL, 'http://localhost/wholesalepeople/public/binjar', '::1', '2018-10-17 00:43:35', '2018-10-17 00:43:35'),
(742, 14, NULL, NULL, 'http://localhost/wholesalepeople/public/binjar', '::1', '2018-10-17 00:43:36', '2018-10-17 00:43:36'),
(743, 14, 1, 'item_details_page', 'http://localhost/wholesalepeople/public/item/details/16', '::1', '2018-10-17 21:44:14', '2018-10-17 21:44:14'),
(744, 14, 1, 'item_details_page', 'http://localhost/wholesalepeople/public/item/details/123', '::1', '2018-10-18 04:04:14', '2018-10-18 04:04:14'),
(745, 14, NULL, NULL, 'http://localhost/wholesalepeople/public/yellow-232', '::1', '2018-10-18 04:16:49', '2018-10-18 04:16:49'),
(746, 14, 1, 'item_details_page', 'http://localhost/wholesalepeople/public/item/details/91', '::1', '2018-10-18 05:43:53', '2018-10-18 05:43:53'),
(747, 14, 1, 'item_details_page', 'http://localhost/wholesalepeople/public/item/details/17', '::1', '2018-10-18 06:03:31', '2018-10-18 06:03:31'),
(748, 14, NULL, NULL, 'http://localhost/wholesalepeople/public/binjar', '::1', '2018-10-22 21:07:11', '2018-10-22 21:07:11'),
(749, 14, 1, 'item_details_page', 'http://localhost/wholesalepeople/public/item/details/16', '::1', '2018-10-22 21:12:53', '2018-10-22 21:12:53'),
(750, 14, NULL, NULL, 'http://localhost/wholesalepeople/public/yellow-232', '::1', '2018-10-22 21:23:06', '2018-10-22 21:23:06'),
(751, 14, 1, 'item_details_page', 'http://localhost/wholesalepeople/public/item/details/123', '::1', '2018-10-22 21:33:39', '2018-10-22 21:33:39'),
(752, 14, NULL, NULL, 'http://localhost/wholesalepeople/public/binjar', '::1', '2018-10-29 02:09:44', '2018-10-29 02:09:44'),
(753, 14, NULL, NULL, 'http://localhost/wholesalepeople/public/binjar', '::1', '2018-10-29 02:10:55', '2018-10-29 02:10:55'),
(754, 14, 1, 'item_details_page', 'http://localhost/wholesalepeople/public/item/details/17', '::1', '2018-10-29 02:10:57', '2018-10-29 02:10:57'),
(755, 14, 1, 'item_details_page', 'http://localhost/wholesalepeople/public/item/details/17', '::1', '2018-10-29 02:11:00', '2018-10-29 02:11:00'),
(756, 14, NULL, NULL, 'http://localhost/wholesalepeople/public/binjar', '::1', '2018-10-29 02:13:03', '2018-10-29 02:13:03'),
(757, 14, 1, 'item_details_page', 'http://localhost/wholesalepeople/public/item/details/123', '::1', '2018-10-29 02:13:05', '2018-10-29 02:13:05'),
(758, 14, 1, 'item_details_page', 'http://localhost/wholesalepeople/public/item/details/123', '::1', '2018-10-29 02:13:08', '2018-10-29 02:13:08'),
(759, 14, NULL, NULL, 'http://localhost/wholesalepeople/public/binjar', '::1', '2018-11-20 20:03:59', '2018-11-20 20:03:59');
INSERT INTO `visitors` (`id`, `user_id`, `vendor_meta_id`, `route`, `url`, `ip`, `created_at`, `updated_at`) VALUES
(760, 14, 2, 'item_details_page', 'http://localhost/wholesalepeople/public/item/details/19', '::1', '2018-11-20 20:05:23', '2018-11-20 20:05:23'),
(761, 14, 1, 'item_details_page', 'http://localhost/wholesalepeople/public/item/details/15', '::1', '2018-11-20 20:05:59', '2018-11-20 20:05:59'),
(762, 14, 1, 'item_details_page', 'http://localhost/wholesalepeople/public/item/details/15', '::1', '2018-11-20 20:06:38', '2018-11-20 20:06:38'),
(763, 14, 1, 'item_details_page', 'http://localhost/wholesalepeople/public/item/details/17', '::1', '2018-11-23 19:30:41', '2018-11-23 19:30:41'),
(764, 14, NULL, NULL, 'http://localhost/wholesalepeople/public/binjar', '::1', '2018-11-23 19:30:47', '2018-11-23 19:30:47'),
(765, 14, 1, 'item_details_page', 'http://localhost/wholesalepeople/public/item/details/123', '::1', '2018-11-23 19:30:49', '2018-11-23 19:30:49'),
(766, 14, 1, 'item_details_page', 'http://localhost/wholesalepeople/public/item/details/123', '::1', '2018-11-23 19:30:53', '2018-11-23 19:30:53'),
(767, NULL, NULL, NULL, 'http://localhost/wholesalepeople/public/binjar', '::1', '2018-11-26 20:51:09', '2018-11-26 20:51:09');

-- --------------------------------------------------------

--
-- Table structure for table `wish_list_items`
--

DROP TABLE IF EXISTS `wish_list_items`;
CREATE TABLE IF NOT EXISTS `wish_list_items` (
  `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT,
  `user_id` int(11) NOT NULL,
  `item_id` int(11) NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=33 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `wish_list_items`
--

INSERT INTO `wish_list_items` (`id`, `user_id`, `item_id`, `created_at`, `updated_at`) VALUES
(32, 14, 19, '2018-08-09 23:55:31', '2018-08-09 23:55:31'),
(31, 14, 17, '2018-08-09 23:55:21', '2018-08-09 23:55:21');
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
