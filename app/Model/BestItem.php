<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;

class BestItem extends Model
{
    protected $fillable = [
        'vendor_meta_id', 'item_id', 'sort'
    ];
}
