<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Review extends Model
{
    use SoftDeletes;

    protected $fillable = [
        'order_id', 'meta_vendor_id', 'user_id', 'star', 'review', 'reply'
    ];

    public function user() {
        return $this->belongsTo('App\Model\User', 'user_id');
    }

    public function order() {
        return $this->belongsTo('App\Model\Order', 'order_id');
    }
}
