<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;

class OpeningSoon extends Model
{
    protected $fillable = [
        'name', 'description', 'image_path'
    ];
}
