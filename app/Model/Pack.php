<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Pack extends Model
{
    use SoftDeletes;

    protected $fillable = [
        'name', 'status', 'default', 'vendor_meta_id', 'pack1', 'pack2', 'pack3', 'pack4', 'pack5', 'pack6', 'pack7', 'pack8',
        'pack9', 'pack10', 'description'
    ];

    public function vendor() {
        return $this->belongsTo('App\Model\MetaVendor', 'vendor_meta_id');
    }
}
