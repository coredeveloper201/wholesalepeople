<?php

namespace App\Model;

use Illuminate\Notifications\Notifiable;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Database\Eloquent\SoftDeletes;
use DB;

class User extends Authenticatable
{
    use Notifiable;
    use SoftDeletes;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'first_name', 'last_name', 'email', 'user_id', 'password', 'role', 'active', 'vendor_meta_id', 'buyer_meta_id',
        'last_login', 'reset_token'
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token',
    ];

    public function getNameAttribute() {
        return $this->first_name.' '.$this->last_name;
    }

    public function vendor() {
        return $this->hasOne('App\Model\MetaVendor', 'id', 'vendor_meta_id');
    }

    public function buyer() {
        return $this->hasOne('App\Model\MetaBuyer', 'id', 'buyer_meta_id')
            ->with('billingState', 'billingCountry');
    }

    public function industries() {
        return $this->belongsToMany('App\Model\Industry');
    }

    public function permissions() {
        $permissions = [];

        $obj = DB::table('user_permission')->where('user_id', $this->id)->get();

        foreach($obj as $item)
            $permissions[] = $item->permission;

        return $permissions;
    }

    public function blockedVendorIds() {
        $ids = [];

        $obj = DB::table('block_users')->where('user_id', $this->id)->get();

        foreach($obj as $item)
            $ids[] = $item->vendor_meta_id;

        return $ids;
    }
}
