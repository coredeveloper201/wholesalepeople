<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Color extends Model
{
    use SoftDeletes;

    protected $fillable = [
        'name', 'status', 'master_color_id', 'image_path', 'vendor_meta_id', 'thumbs_image_path'
    ];

    public function vendor() {
        return $this->belongsTo('App\Model\MetaVendor', 'vendor_meta_id');
    }
}
