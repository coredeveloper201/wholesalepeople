<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Category extends Model
{
    use SoftDeletes;

    protected $fillable = [
        'name', 'parent', 'sort', 'd_category_id', 'd_category_second_id', 'd_category_third_id', 'status', 'vendor_meta_id',
        'discount'
    ];

    public function vendor() {
        return $this->belongsTo('App\Model\MetaVendor', 'vendor_meta_id');
    }

    public function subCategories() {
        return $this->hasMany('App\Model\Category', 'parent', 'id')
            ->where('status', 1)
            ->orderBy('sort');
    }
}
