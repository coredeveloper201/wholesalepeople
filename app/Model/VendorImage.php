<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;

class VendorImage extends Model
{
    protected $fillable = [
        'vendor_meta_id', 'type', 'image_path', 'status'
    ];

    public function vendor() {
        return $this->belongsTo('App\Model\MetaVendor', 'vendor_meta_id');
    }
}
