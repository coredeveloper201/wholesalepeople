<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;

class ItemImages extends Model
{
    protected $fillable = [
        'item_id', 'image_path', 'color_id', 'sort', 'list_image_path', 'thumbs_image_path'
    ];

    public function color() {
        return $this->belongsTo('App\Model\Color');
    }
}
