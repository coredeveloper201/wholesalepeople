<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Size extends Model
{
    use SoftDeletes;

    protected $fillable = [
        'name', 'status', 'default', 'user_id', 'size1', 'size2', 'size3', 'size4', 'size5', 'size6', 'size7', 'size8',
        'size9', 'size10'
    ];
}
