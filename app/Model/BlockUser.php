<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;

class BlockUser extends Model
{
    protected $fillable = [
        'user_id', 'vendor_meta_id'
    ];

    public function user() {
        return $this->belongsTo('App\Model\User');
    }
}
