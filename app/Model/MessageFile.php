<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class MessageFile extends Model
{
    use SoftDeletes;

    protected $fillable = [
        'message_id', 'filename', 'original_filename', 'mime'
    ];
}
