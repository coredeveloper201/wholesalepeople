<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Order extends Model
{
    use SoftDeletes;

    protected $fillable = [
        'status', 'user_id', 'vendor_meta_id', 'order_number', 'shipping_method_id', 'shipping_address', 'shipping_city', 'shipping_state',
        'shipping_zip', 'shipping_country', 'shipping_phone', 'billing_address', 'billing_city', 'billing_state', 'billing_zip',
        'billing_country', 'billing_phone', 'card_number', 'card_full_name', 'card_expire', 'card_cvc', 'subtotal',
        'discount', 'shipping_cost', 'total', 'shipping_location', 'shipping_state_id', 'shipping_state_text', 'billing_location',
        'billing_state_id', 'billing_state_text', 'shipping_country_id', 'billing_country_id', 'tracking_number',
        'company_name', 'email', 'name', 'shipping', 'note', 'shipping_address_id', 'coupon', 'coupon_type', 'coupon_amount',
        'coupon_description', 'store_credit', 'shipping_unit', 'billing_unit', 'card_location', 'card_address', 'card_city',
        'card_state_id', 'card_state', 'card_zip', 'card_country_id', 'invoice_number'
    ];

    public function user() {
        return $this->belongsTo('App\Model\User')->with('buyer');
    }

    public function vendor() {
        return $this->belongsTo('App\Model\MetaVendor', 'vendor_meta_id')
            ->with('billingState', 'billingCountry');
    }

    public function items() {
        return $this->hasMany('App\Model\OrderItem')->with('item');
    }

    public function shippingMethod() {
        return $this->belongsTo('App\Model\ShippingMethod')->with('shipMethod');
    }

    public function review() {
        return $this->hasMany('App\Model\Review', 'order_id', 'id');
    }

    public function storeCreditTransections() {
        return $this->hasMany('App\Model\StoreCreditTransection', 'order_id', 'id');
    }

    public function notifications() {
        return $this->hasMany('App\Model\Notification', 'order_id', 'id');
    }

}
