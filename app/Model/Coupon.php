<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;

class Coupon extends Model
{
    protected $fillable = [
        'vendor_meta_id', 'name', 'type', 'amount', 'multiple_use', 'description'
    ];
}
