<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class MetaBuyer extends Model
{
    use SoftDeletes;

    protected $table = 'meta_buyers';

    protected $fillable = [
        'verified', 'active', 'block', 'company_name', 'primary_customer_market', 'seller_permit_number', 'sell_online', 'website',
        'attention', 'billing_location', 'billing_address', 'billing_unit', 'billing_city', 'billing_state_id', 'billing_state',
        'billing_zip', 'billing_country_id', 'billing_phone', 'billing_fax', 'billing_commercial', 'hear_about_us',
        'hear_about_us_other', 'receive_offers', 'user_id', 'ein_path', 'sales1_path', 'sales2_path'
    ];

    public function user() {
        return $this->hasOne('App\Model\User', 'buyer_meta_id');
    }

    public function billingState() {
        return $this->belongsTo('App\Model\State', 'billing_state_id');
    }

    public function billingCountry() {
        return $this->belongsTo('App\Model\Country', 'billing_country_id');
    }
}
