<?php

namespace App\Model;

use App\Enumeration\MessageRole;
use App\Enumeration\MessageTopic;
use App\Enumeration\Role;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Support\Facades\Auth;

class Message extends Model
{
    use SoftDeletes;

    protected $fillable = [
        'sender_id', 'sender_type', 'receiver_id', 'receiver_type', 'title', 'topic', 'order_id', 'sender_seen_at', 'receiver_seen_at'
    ];

    public function receiverVendor() {
        return $this->belongsTo('App\Model\MetaVendor', 'receiver_id');
    }

    public function items() {
        return $this->hasMany('App\Model\MessageItem', 'message_id')->with('files')->orderBy('created_at');
    }

    public function senderUser() {
        return $this->belongsTo('App\Model\User', 'sender_id')->with('buyer');
    }

    public function order() {
        return $this->belongsTo('App\Model\Order', 'order_id');
    }

    public function isSender() {
        if (in_array(Auth::user()->role, [Role::$VENDOR_EMPLOYEE, Role::$VENDOR])) {
            // For Vendor
            if ($this->sender_type == MessageRole::$VENDOR && $this->sender_id == Auth::user()->vendor_meta_id)
                return true;
        } else if (in_array(Auth::user()->role, [Role::$BUYER])) {
            // For Buyer
            if ($this->sender_type == MessageRole::$BUYER && $this->sender_id == Auth::user()->id)
                return true;
        } else if (in_array(Auth::user()->role, [Role::$ADMIN])) {
            // For Admin
            if ($this->sender_type == MessageRole::$ADMIN)
                return true;
        }

        return false;
    }

    public function senderName() {
        if ($this->sender_type == MessageRole::$BUYER) {
            $user = User::where('id', $this->sender_id)->first();

            return $user->name;
        } else if ($this->sender_type == MessageRole::$VENDOR) {
            $user = MetaVendor::where('id', $this->sender_id)->first();

            return $user->company_name;
        } else if ($this->sender_type == MessageRole::$ADMIN) {
            return 'StylePick';
        }

        return '';
    }

    public function receiverName() {
        if ($this->receiver_type == MessageRole::$VENDOR) {
            $user = MetaVendor::where('id', $this->receiver_id)->first();

            return $user->company_name;
        } else if ($this->receiver_type == MessageRole::$BUYER) {
            $user = User::where('id', $this->receiver_id)->first();

            return $user->name;
        } else if ($this->receiver_type == MessageRole::$ADMIN) {
            return 'StylePick';
        }

        return '';
    }

    public function topicName() {
        if ($this->topic == MessageTopic::$GENERAL)
            return 'General';
        else if ($this->topic == MessageTopic::$PRODUCT)
            return 'Product';
        else if ($this->topic == MessageTopic::$ORDER)
            return 'Order';
        else if ($this->topic == MessageTopic::$PAYMENT)
            return 'Payment';
        else if ($this->topic == MessageTopic::$SHIPMENT)
            return 'Shipment';
        else if ($this->topic == MessageTopic::$RETURN)
            return 'Return';
        elseif ($this->topic == MessageTopic::$OTHER)
            return 'Other';
    }
}
