<?php

namespace App\Http\ViewComposers;

use App\Enumeration\MessageRole;
use App\Enumeration\VendorImageType;
use App\Model\Category;
use App\Model\DefaultCategory;
use App\Model\Message;
use App\Model\MetaVendor;
use App\Model\VendorImage;
use Illuminate\Support\Facades\Auth;
use Illuminate\View\View;
use Illuminate\Http\Request;
use Route;

class AdminLayout
{
    public function __construct(Request $request)
    {

    }

    /**
     * Bind data to the view.
     *
     * @param  View  $view
     * @return void
     */
    public function compose(View $view)
    {
        $categories = DefaultCategory::where('parent', 0)
            ->orderBy('sort')
            ->get();

        $vendors = MetaVendor::orderBy('company_name')->get();

        // Unread Message count
        $count = Message::where('sender_type', MessageRole::$ADMIN)->where('sender_seen_at', null)->count();
        $count += Message::where('receiver_type', MessageRole::$ADMIN)->where('receiver_seen_at', null)->count();

        $view->with([
            'categories' => $categories,
            'vendors' => $vendors,
            'unread_msg' => $count,
        ]);
    }
}