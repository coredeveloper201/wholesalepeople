<?php

namespace App\Http\ViewComposers;

use App\Enumeration\MessageRole;
use App\Enumeration\VendorImageType;
use App\Model\Category;
use App\Model\Message;
use App\Model\VendorImage;
use Illuminate\Support\Facades\Auth;
use Illuminate\View\View;
use Illuminate\Http\Request;
use Route;

class VendorLayout
{
    public function __construct(Request $request)
    {

    }

    /**
     * Bind data to the view.
     *
     * @param  View  $view
     * @return void
     */
    public function compose(View $view)
    {
        $categories = Category::where('parent', 0)->where('vendor_meta_id', Auth::user()->vendor_meta_id)
            ->where('status', 1)
            ->with('subCategories')->orderBy('sort')->orderBy('name')->get();

        // Logo
        $logo_path = '';

        $image = VendorImage::where('vendor_meta_id', Auth::user()->vendor_meta_id)
            ->where('type', VendorImageType::$LOGO)
            ->where('status', 1)->first();

        if ($image)
            $logo_path = asset($image->image_path);

        // Unread Message count
        $count = Message::where('sender_id', Auth::user()->vendor_meta_id)->where('sender_type', MessageRole::$VENDOR)->where('sender_seen_at', null)->count();
        $count += Message::where('receiver_id', Auth::user()->vendor_meta_id)->where('receiver_type', MessageRole::$VENDOR)->where('receiver_seen_at', null)->count();

        $view->with([
            'categories' => $categories,
            'logo_path' => $logo_path,
            'unread_msg' => $count,
        ]);
    }
}