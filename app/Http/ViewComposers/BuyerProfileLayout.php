<?php

namespace App\Http\ViewComposers;

use App\Enumeration\MessageRole;
use App\Enumeration\VendorImageType;
use App\Model\Category;
use App\Model\DefaultCategory;
use App\Model\Message;
use App\Model\MetaVendor;
use App\Model\VendorImage;
use Illuminate\Support\Facades\Auth;
use Illuminate\View\View;
use Illuminate\Http\Request;
use Route;

class BuyerProfileLayout
{
    public function __construct(Request $request)
    {

    }

    /**
     * Bind data to the view.
     *
     * @param  View  $view
     * @return void
     */
    public function compose(View $view)
    {
        $count = Message::where('sender_id', Auth::user()->id)->where('sender_type', MessageRole::$BUYER)->where('sender_seen_at', null)->count();
        $count += Message::where('receiver_id', Auth::user()->id)->where('receiver_type', MessageRole::$BUYER)->where('receiver_seen_at', null)->count();

        $view->with([
            'unread_msg' => $count,
        ]);
    }
}