<?php

namespace App\Http\ViewComposers;

use App\Enumeration\Page;
use App\Enumeration\Role;
use App\Enumeration\VendorImageType;
use App\Model\CartItem;
use App\Model\DefaultCategory;
use App\Model\Item;
use App\Model\Meta;
use App\Model\MetaVendor;
use App\Model\Notification;
use App\Model\VendorImage;
use Illuminate\Support\Facades\Auth;
use Illuminate\View\View;
use Illuminate\Http\Request;
use Route;

class MainLayout
{
    private $text;

    public function __construct(Request $request)
    {
        if (Route::currentRouteName() == 'vendor_or_parent_category') {
            $this->text = $request->text;
        }
    }

    /**
     * Bind data to the view.
     *
     * @param  View  $view
     * @return void
     */
    public function compose(View $view)
    {
        $activeVendors = MetaVendor::where('active', 1)->where('verified', 1)->where('live', 1)->pluck('id')->toArray();

        // New Arrivals
        $byArrivalDate = [];
        $byCategories = [];

        $totalCount = Item::where('status', 1)->whereIn('vendor_meta_id', $activeVendors)->count();
        $byArrivalDate[] = [
            'name' => 'All',
            'count' => number_format($totalCount),
            'day' => 'A'
        ];

        for($i=0; $i <= 6; $i++) {
            $count = Item::where('status', 1)
                ->whereIn('vendor_meta_id', $activeVendors)
                ->whereDate('activated_at', date('Y-m-d', strtotime("-$i day")))
                ->count();

            $byArrivalDate[] = [
                'name' => date('F j', strtotime("-$i day")),
                'count' => number_format($count),
                'day' => $i
            ];
        }

        $parentCategories = DefaultCategory::where('parent', 0)
            ->orderBy('sort')
            ->get();

        foreach ($parentCategories as $category) {
            $count = Item::where('status', 1)
                ->whereIn('vendor_meta_id', $activeVendors)
                ->where('default_parent_category', $category->id)
                ->count();

            $byCategories[] = [
                'id' => $category->id,
                'name' => $category->name,
                'count' => number_format($count)
            ];
        }

        // Vendors
        $vendors = MetaVendor::where('verified', 1)
            ->where('active', 1)
            ->where('live', 1)
            ->orderBy('company_name')->get();

        // Vendor menu banner
        $vendorMenuBanner = [];
        $imgs = VendorImage::where('status', 1)
            ->where('type', VendorImageType::$SMALL_AD_BANNER)
            ->whereHas('vendor', function ($q) {
                $q->where('verified', 1);
                $q->where('active', 1);
                $q->where('live', 1);
                $q->where('feature_vendor', 1);
            })
            ->limit(2)
            ->inRandomOrder()
            ->with('vendor')
            ->get();

        foreach ($imgs as $img)
            $vendorMenuBanner[] = [
                'vendor_id' => $img->vendor_meta_id,
                'vendor_name' => $img->vendor->company_name,
                'image_path' => $img->image_path
            ];

        // Default Categories
        $defaultCategories = [];
        $categoriesCollection = DefaultCategory::orderBy('sort')->orderBy('name')->get();

        foreach($categoriesCollection as $cc) {
            if ($cc->parent == 0) {
                $data = [
                    'id' => $cc->id,
                    'name' => $cc->name
                ];

                $subCategories = [];
                foreach($categoriesCollection as $item) {
                    if ($item->parent == $cc->id) {
                        $data2 = [
                            'id' => $item->id,
                            'name' => $item->name
                        ];

                        $data3 = [];
                        foreach($categoriesCollection as $item2) {
                            if ($item2->parent == $item->id) {
                                $data3[] = [
                                    'id' => $item2->id,
                                    'name' => $item2->name
                                ];
                            }
                        }

                        $data2['subCategories'] = $data3;
                        $subCategories[] = $data2;
                    }
                }

                $data['subCategories'] = $subCategories;
                $defaultCategories[] = $data;
            }
        }

        // Notification
        $notifications = [];

        if (Auth::check() && Auth::user()->role == Role::$BUYER)
            $notifications = Notification::where('user_id', Auth::user()->id)
                ->where('view', 0)->get();

        // Cart
        $cartItems = [];

        if (Auth::check() && Auth::user()->role == Role::$BUYER) {

            $items = CartItem::where('user_id', Auth::user()->id)
                ->with('item')->get();
            $totalPrice = 0;
            $totalQty = 0;

            foreach($items as $item) {
                $sizes = explode("-", $item->item->pack->name);
                $itemInPack = 0;

                for ($i = 1; $i <= sizeof($sizes); $i++) {
                    $var = 'pack' . $i;

                    if ($item->item->pack->$var != null)
                        $itemInPack += (int)$item->item->pack->$var;
                }

                $qty = $itemInPack * $item->quantity;
                $totalPrice += $itemInPack * $item->quantity * $item->item->price;
                $totalQty += $itemInPack * $item->quantity;

                if (array_key_exists($item->item->id, $cartItems))
                    $qty = (int) $cartItems[$item->item->id]['qty'] + (int) $qty;

                $image_path = asset('images/no-image.png');

                if (sizeof($item->item->images) > 0)
                    $image_path = asset($item->item->images[0]->thumbs_image_path);

                $cartItems[$item->item->id] = [
                    'name' => $item->item->style_no,
                    'qty' => $qty,
                    'image_path' => $image_path,
                    'price' => $item->item->price,
                    'details_url' => route('item_details_page', ['item' => $item->item->id])
                ];
            }

            $cartItems['total'] = [
                'total_price' => $totalPrice,
                'total_qty' => $totalQty
            ];
        }

        // Meta
        $meta_title = config('app.name');
        $meta_description = '';
        $meta = null;

        if (Route::currentRouteName() == 'home') {
            $meta = Meta::where('page', Page::$HOME)->first();
        } else if (Route::currentRouteName() == 'all_vendor_page') {
            $meta = Meta::where('page', Page::$VENDOR_ALL)->first();
        } else if (Route::currentRouteName() == 'new_arrival_home') {
            $meta = Meta::where('page', Page::$NEW_ARRIVAL)->first();
        } else if (Route::currentRouteName() == 'about_us') {
            $meta = Meta::where('page', Page::$ABOUT_US)->first();
        } else if (Route::currentRouteName() == 'contact_us') {
            $meta = Meta::where('page', Page::$CONTACT_US)->first();
        } else if (Route::currentRouteName() == 'return_exchange') {
            $meta = Meta::where('page', Page::$RETURN_EXCHANGE)->first();
        } else if (Route::currentRouteName() == 'show_schedule') {
            $meta = Meta::where('page', Page::$SHOW_SCHEDULE)->first();
        } else if (Route::currentRouteName() == 'photoshoot') {
            $meta = Meta::where('page', Page::$PHOTO_SHOOT)->first();
        } else if (Route::currentRouteName() == 'shipping_information') {
            $meta = Meta::where('page', Page::$SHIPPING_INFORMATION)->first();
        } else if (Route::currentRouteName() == 'refund_policy') {
            $meta = Meta::where('page', Page::$REFUND_POLICY)->first();
        } else if (Route::currentRouteName() == 'privacy_policy') {
            $meta = Meta::where('page', Page::$PRIVACY_POLICY)->first();
        } else if (Route::currentRouteName() == 'terms_conditions') {
            $meta = Meta::where('page', Page::$TERMS_CONDITIONS)->first();
        } else if (Route::currentRouteName() == 'vendor_or_parent_category') {
            $found = false;

            foreach ($parentCategories as $cat) {
                if (changeSpecialChar($cat->name) == $this->text) {
                    $meta = Meta::where('category', $cat->id)->first();

                    $found = true;
                    break;
                }
            }

            if (!$found) {
                foreach ($vendors as $v) {
                    if (changeSpecialChar($v->company_name) == $this->text) {
                        $meta = Meta::where('vendor', $v->id)->first();
                        break;
                    }
                }
            }
        }

        if ($meta) {
            if ($meta->title != NULL && $meta->title != '')
                $meta_title = $meta->title;

            if ($meta->title != NULL)
                $meta_description = $meta->description;
        }

        $view->with([
            'default_categories' => $defaultCategories,
            'cart_items' => $cartItems,
            'notifications' => $notifications,
            'vendors' => $vendors,
            'menu_vendor_banners' => $vendorMenuBanner,
            'by_arrival_dates' => $byArrivalDate,
            'by_category' => $byCategories,
            'meta_title' => $meta_title,
            'meta_description' => $meta_description,
            'unread_message' => 5
        ]);
    }
}