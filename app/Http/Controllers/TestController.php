<?php

namespace App\Http\Controllers;

use App\Model\Category;
use App\Model\DefaultCategory;
use App\Model\Item;
use App\Model\MetaVendor;
use App\Model\Order;
use Illuminate\Http\Request;
use File;
use Image;
use Mail;

class TestController extends Controller
{
    public function createFolder() {
        $vendors = MetaVendor::all();

        foreach ($vendors as $vendor) {
            File::makeDirectory(public_path('images/vendors/'.$vendor->id.'/colors'));
            File::makeDirectory(public_path('images/vendors/'.$vendor->id.'/colors/thumbs'));
        }
    }

    public function time() {
        echo date('d-m-Y H:i:s a');
    }

    public function itemCategory() {
        $items = Item::all();

        foreach ($items as $item)
            $item->categories()->sync([$item->category_id]);
    }

    public function test() {
        $order = Order::where('id', 61)->with('review', 'storeCreditTransections')->first();

        //dd($order);
    }

    public function froya() {
        $vendor = MetaVendor::where('id', 16)->first();
        $items = Item::where('vendor_meta_id', $vendor->id)->with('colors', 'images', 'pack', 'madeInCountry')->get();

        $data = [];

        foreach ($items as $item) {
            $anItem = [];

            $d1 = DefaultCategory::where('id', $item->default_parent_category)->first();
            $dc = $d1->name;

            if ($item->default_second_category != null) {
                $d2 = DefaultCategory::where('id', $item->default_second_category)->first();
                $dc .= ','.$d2->name;
            }

            if ($item->default_third_category != null) {
                $d2 = DefaultCategory::where('id', $item->default_third_category)->first();

                if ($d2)
                    $dc .= ','.$d2->name;
            }

            $anItem['styleNo'] = $item->style_no;
            $anItem['itemName'] = $item->name;
            $anItem['defaultCategory'] = $dc;

            //Size
            $anItem['size'] = $item->pack->name;

            // Pack
            $sizes = explode("-", $item->pack->name);
            $pack = '';

            for ($i = 1; $i <= sizeof($sizes); $i++) {
                $var = 'pack' . $i;

                if ($item->pack->$var != null) {
                    $pack .= $item->pack->$var . ',';
                } else {
                    $pack .= '0,';
                }
            }
            $pack = rtrim($pack, ",");

            $anItem['pack'] = $pack;
            $anItem['packQty'] = $item->min_qty;
            $anItem['unitPrice'] = $item->getOriginal('price');
            $anItem['originalPrice'] = $item->getOriginal('orig_price');

            // Available on
            $date = null;

            if ($item->available_on != null) {
                $date = date('d-m-y', strtotime($item->available_on));
            }

            $anItem['availableOn'] = $date;
            $anItem['productDescription'] = $item->description;
            $anItem['madeIn'] = ($item->madeInCountry != null ? $item->madeInCountry->name : '');
            $anItem['fabric'] = $item->fabric;

            // Colors
            $colors = '';

            foreach ($item->colors as $color)
                $colors .= $color->name.',';

            $colors = rtrim($colors, ',');

            $anItem['color'] = $colors;

            // Images
            $images = '';
            $images_color = '';

            foreach ($item->images as $image) {
                $images .= asset($image->image_path).',';

                if ($image->color != null)
                    $images_color .= $image->color->name.',';
                else
                    $images_color .= ',';
            }

            $images = rtrim($images, ',');
            $images_color = rtrim($images_color, ',');

            $anItem['images'] = $images;
            $anItem['images_color'] = $images_color;

            $data[] = $anItem;
        }

        return response()->json($data);
    }

    public function img() {
        $filepath = public_path('/images/vendors/1/list/0a6f4b00-8441-11e8-8ab4-3d650295a243.jpg');

        $img = Image::make($filepath);
        $img->save($filepath, 85);
    }

    public function ttt() {
        dd(is_writable(config('session.files')));
    }

    public function mailView() {
        //return view('emails.vendor.p');

        Mail::send('emails.vendor.p', [], function ($message) {
            $message->subject('New Order');
            $message->to('shanto.razer@gmail.com', 'Shanto');
        });

        //return view('emails.vendor.p');
    }
}
