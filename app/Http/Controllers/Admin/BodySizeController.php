<?php

namespace App\Http\Controllers\Admin;

use App\Model\BodySize;
use App\Model\DefaultCategory;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class BodySizeController extends Controller
{
    public function index() {
        $sizes = BodySize::orderBy('name')->with('category')->get();

        // Default Categories
        $defaultCategories = DefaultCategory::where('parent', 0)->orderBy('sort')->orderBy('name')->get();

        return view('admin.body_size.index', compact('sizes', 'defaultCategories'))
            ->with('page_title', 'Body Size');
    }

    public function addPost(Request $request) {
        BodySize::create([
            'name' => $request->name,
            'parent_category_id' => $request->d_parent_category,
        ]);

        return redirect()->route('admin_body_size')->with('message', 'Body Size Added!');
    }

    public function editPost(Request $request) {
        $size = BodySize::where('id', $request->id)->first();

        $size->name = $request->name;
        $size->parent_category_id = $request->d_parent_category;
        $size->save();

        return redirect()->route('admin_body_size')->with('message', 'Body Size Updated!');
    }

    public function delete(Request $request) {
        $size = BodySize::where('id', $request->id)->first();
        $size->delete();
    }
}
