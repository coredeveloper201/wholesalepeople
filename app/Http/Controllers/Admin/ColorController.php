<?php

namespace App\Http\Controllers\Admin;

use App\Model\Color;
use App\Model\MasterColor;
use App\Model\MetaVendor;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Uuid;

class ColorController extends Controller
{
    public function index(Request $request) {
        if (isset($request->id))
            $colors = Color::where('vendor_meta_id', $request->id)->orderBy('name')->with('vendor')->paginate(10);
        else
            $colors = Color::orderBy('name')->with('vendor')->paginate(10);

        $masterColors = MasterColor::orderBy('name')->get();
        $vendors = MetaVendor::orderBy('company_name')->get();

        return view('admin.color.index', compact('masterColors', 'colors', 'vendors'))->with('page_title', 'Color');
    }

    public function addPost(Request $request) {
        $request->validate([
            'vendor_id' => 'required',
            'color_name' => 'required',
            'master_color' => 'required',
            'photo' => 'nullable|mimes:jpeg,jpg,png,gif'
        ]);

        $imagePath = null;

        if ($request->photo) {
            $filename = Uuid::generate()->string;
            $file = $request->file('photo');
            $ext = $file->getClientOriginalExtension();

            $destinationPath = '/images/color';

            $file->move(public_path($destinationPath), $filename.".".$ext);

            $imagePath = $destinationPath."/".$filename.".".$ext;
        }

        Color::create([
            'name' => $request->color_name,
            'status' => $request->status,
            'master_color_id' => $request->master_color,
            'image_path' => $imagePath,
            'vendor_meta_id' => $request->vendor_id
        ]);

        return redirect()->back()->with('message', 'Color Added!');
    }

    public function editPost(Request $request) {
        $request->validate([
            'vendor_id' => 'required',
            'color_name' => 'required',
            'master_color' => 'required',
            'photo' => 'nullable|mimes:jpeg,jpg,png,gif'
        ]);

        $color = Color::where('id', $request->colorId)->first();

        $imagePath = null;

        if ($request->photo) {
            if ($color->image_path != null)
                File::delete(public_path($color->image_path));

            $filename = Uuid::generate()->string;
            $file = $request->file('photo');
            $ext = $file->getClientOriginalExtension();

            $destinationPath = '/images/color';

            $file->move(public_path($destinationPath), $filename.".".$ext);

            $imagePath = $destinationPath."/".$filename.".".$ext;

            $color->image_path = $imagePath;
        }

        $color->name = $request->color_name;
        $color->status = $request->status;
        $color->master_color_id = $request->master_color;
        $color->vendor_meta_id = $request->vendor_id;

        $color->save();

        return redirect()->back()->with('message', 'Color Updated!');
    }

    public function delete(Request $request) {
        $color = Color::where('id', $request->id)->first();
        $color->delete();
    }
}
