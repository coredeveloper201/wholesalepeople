<?php

namespace App\Http\Controllers\Admin;

use App\Model\DefaultCategory;
use App\Model\Length;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class LengthController extends Controller
{
    public function index() {
        $lengths = Length::orderBy('name')->with('category')->get();

        // Default Categories
        $defaultCategories = [];

        $categoriesCollection = DefaultCategory::orderBy('sort')->orderBy('name')->get();

        foreach($categoriesCollection as $cc) {
            if ($cc->parent == 0) {
                $data = [
                    'id' => $cc->id,
                    'name' => $cc->name
                ];

                $subCategories = [];
                foreach($categoriesCollection as $item) {
                    if ($item->parent == $cc->id) {
                        $data2 = [
                            'id' => $item->id,
                            'name' => $item->name
                        ];

                        $subCategories[] = $data2;
                    }
                }

                $data['subCategories'] = $subCategories;
                $defaultCategories[] = $data;
            }
        }

        return view('admin.length.index', compact('lengths', 'defaultCategories'))
            ->with('page_title', 'Length');
    }

    public function addPost(Request $request) {
        Length::create([
            'name' => $request->name,
            'sub_category_id' => $request->d_second_parent_category,
        ]);

        return redirect()->route('admin_length')->with('message', 'Length Added!');
    }

    public function editPost(Request $request) {
        $length = Length::where('id', $request->id)->first();

        $length->name = $request->name;
        $length->sub_category_id = $request->d_second_parent_category;
        $length->save();

        return redirect()->route('admin_length')->with('message', 'Length Updated!');
    }

    public function delete(Request $request) {
        $size = Length::where('id', $request->id)->first();
        $size->delete();
    }
}
