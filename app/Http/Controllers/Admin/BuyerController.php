<?php

namespace App\Http\Controllers\Admin;

use App\Model\Country;
use App\Model\MetaBuyer;
use App\Model\State;
use App\Model\StoreCredit;
use App\Model\StoreCreditTransection;
use App\Model\User;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Hash;
use DB;

class BuyerController extends Controller
{
    public function allBuyer(Request $request) {
        $query = MetaBuyer::query();

        if ($request->s && ($request->s != null || $request->s != '')) {
            if ($request->name && $request->name == '1') {
                $query->whereHas('user', function ($q) use ($request) {
                    $q->where(DB::raw("CONCAT(`first_name`, ' ', `last_name`)"), 'LIKE', "%" . $request->s . "%");
                });
            }

            if ($request->company_name && $request->company_name == '1') {
                $query->where('company_name', 'like', '%' . $request->s . '%');
            }
        }

        if ($request->active && $request->active == '1')
            $query->where('active', 1);

        if ($request->verified && $request->verified == '1')
            $query->where('verified', 1);

        if ($request->block && $request->block == '1')
            $query->where('block', 1);

        $buyers = $query->with('user')->orderBy('created_at', 'desc')->paginate(10);

        $appends = [];
        foreach ($request->all() as $key => $value) {
            $appends[$key] = ($value == null) ? '' : $value;
        }

        return view('admin.customer.all', compact('buyers', 'appends'))->with('page_title', 'All Customer');
    }

    public function changeStatus(Request $request) {
        $buyers = MetaBuyer::where('id', $request->id)->first();
        $buyers->active = $request->status;
        $buyers->save();
    }

    public function changeVerified(Request $request) {
        $buyers = MetaBuyer::where('id', $request->id)->first();
        $buyers->verified = $request->status;
        $buyers->save();
    }

    public function changeBlock(Request $request) {
        $buyers = MetaBuyer::where('id', $request->id)->first();
        $buyers->block = $request->status;
        $buyers->save();
    }

    public function edit(MetaBuyer $buyer) {
        $buyer->load('user');
        $countries = Country::orderBy('name')->get();
        $usStates = State::where('country_id', 1)->orderBy('name')->get()->toArray();
        $caStates =State::where('country_id', 2)->orderBy('name')->get()->toArray();

        return view('admin.customer.edit', compact('countries', 'usStates', 'caStates', 'buyer'))->with('page_title', 'Edit Customer');
    }

    public function editPost(MetaBuyer $buyer, Request $request) {
        $messages = [
            'required' => 'This field is required.',
        ];

        $rules = [
            'companyName' => 'required|string|max:255',
            'firstName' => 'required|string|max:255',
            'lastName' => 'required|string|max:255',
            'email' => 'required|string|email|max:255|unique:users,email,'.$buyer->user->id,
            'sellerPermitNumber' => 'required|string|max:255',
        ];

        if ($request->sellOnline && $request->sellOnline == '1')
            $rules['website'] = 'required|string|max:255';

        if ($request->password != '')
            $rules['password'] = 'required|string|min:6';

        $request->validate($rules, $messages);



        $buyer->company_name = $request->companyName;
        $buyer->primary_customer_market = $request->primaryCustomerMarket;
        $buyer->seller_permit_number = $request->sellerPermitNumber;
        $buyer->sell_online = $request->sellOnline;
        $buyer->website = $request->website;

        $buyer->user->first_name = $request->firstName;
        $buyer->user->last_name = $request->lastName;
        $buyer->user->email = $request->email;

        if ($request->password != '')
            $buyer->user->password = Hash::make($request->password);

        $buyer->save();
        $buyer->user->save();

        return redirect()->route('admin_all_buyer')->with('message', 'Updated!');
    }

    public function delete(Request $request) {
        $meta = MetaBuyer::where('id', $request->id)->first();
        $user = User::where('id', $meta->user->id)->first();
        $user->email = $user->email.'-deleted';
        $user->save();

        StoreCredit::where('user_id', $user->id)->delete();
        StoreCreditTransection::where('user_id', $user->id)->delete();

        $user->delete();
        $meta->delete();
    }
}
