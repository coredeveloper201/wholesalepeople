<?php

namespace App\Http\Controllers\Admin;

use App\Model\DefaultCategory;
use App\Model\Pattern;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class PatternController extends Controller
{
    public function index() {
        $patterns = Pattern::orderBy('name')->with('category')->get();

        // Default Categories
        $defaultCategories = DefaultCategory::where('parent', 0)->orderBy('sort')->orderBy('name')->get();

        return view('admin.pattern.index', compact('patterns', 'defaultCategories'))
            ->with('page_title', 'Pattern');
    }

    public function addPost(Request $request) {
        Pattern::create([
            'name' => $request->name,
            'parent_category_id' => $request->d_parent_category,
        ]);

        return redirect()->route('admin_pattern')->with('message', 'Pattern Added!');
    }

    public function editPost(Request $request) {
        $pattern = Pattern::where('id', $request->id)->first();

        $pattern->name = $request->name;
        $pattern->parent_category_id = $request->d_parent_category;
        $pattern->save();

        return redirect()->route('admin_pattern')->with('message', 'Pattern Updated!');
    }

    public function delete(Request $request) {
        $size = Pattern::where('id', $request->id)->first();
        $size->delete();
    }
}
