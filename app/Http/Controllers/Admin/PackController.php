<?php

namespace App\Http\Controllers\Admin;

use App\Model\MetaVendor;
use App\Model\Pack;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class PackController extends Controller
{
    public function index(Request $request) {
        if (isset($request->id))
            $packs = Pack::where('vendor_meta_id', $request->id)->orderBy('name')->with('vendor')->paginate(10);
        else
            $packs = Pack::orderBy('name')->with('vendor')->paginate(10);
        
        $vendors = MetaVendor::orderBy('company_name')->get();

        return view('admin.pack.index', compact('packs', 'vendors'))->with('page_title', 'Pack');
    }

    public function addPost(Request $request) {
        $rules = [
            'vendor_id' => 'required',
            'pack_name' => 'required',
            'p1' => 'required|integer'
        ];

        if ($request->p3 && $request->p3 != "")
            $rules['p2'] = 'required|integer';

        if ($request->p4 && $request->p4 != "") {
            $rules['p2'] = 'required|integer';
            $rules['p3'] = 'required|integer';
        }

        if ($request->p5 && $request->p5 != "") {
            $rules['p2'] = 'required|integer';
            $rules['p3'] = 'required|integer';
            $rules['p4'] = 'required|integer';
        }

        if ($request->p6 && $request->p6 != "") {
            $rules['p2'] = 'required|integer';
            $rules['p3'] = 'required|integer';
            $rules['p4'] = 'required|integer';
            $rules['p5'] = 'required|integer';
        }

        if ($request->p7 && $request->p7 != "") {
            $rules['p2'] = 'required|integer';
            $rules['p3'] = 'required|integer';
            $rules['p4'] = 'required|integer';
            $rules['p5'] = 'required|integer';
            $rules['p6'] = 'required|integer';
        }

        if ($request->p8 && $request->p8 != "") {
            $rules['p2'] = 'required|integer';
            $rules['p3'] = 'required|integer';
            $rules['p4'] = 'required|integer';
            $rules['p5'] = 'required|integer';
            $rules['p6'] = 'required|integer';
            $rules['p7'] = 'required|integer';
        }

        if ($request->p9 && $request->p9 != "") {
            $rules['p2'] = 'required|integer';
            $rules['p3'] = 'required|integer';
            $rules['p4'] = 'required|integer';
            $rules['p5'] = 'required|integer';
            $rules['p6'] = 'required|integer';
            $rules['p7'] = 'required|integer';
            $rules['p8'] = 'required|integer';
        }

        if ($request->p10 && $request->p10 != "") {
            $rules['p2'] = 'required|integer';
            $rules['p3'] = 'required|integer';
            $rules['p4'] = 'required|integer';
            $rules['p5'] = 'required|integer';
            $rules['p6'] = 'required|integer';
            $rules['p7'] = 'required|integer';
            $rules['p8'] = 'required|integer';
            $rules['p9'] = 'required|integer';
        }

        $request->validate($rules);

        if ($request->default) {
            Pack::where('vendor_meta_id', $request->vendor_id)->update([ 'default' => 0 ]);
        }

        $pack = Pack::create([
            'name' => $request->pack_name,
            'status' => $request->status,
            'default' => ($request->default) ? 1 : 0,
            'vendor_meta_id' => $request->vendor_id,
            'pack1' => $request->p1,
            'pack2' => $request->p2,
            'pack3' => $request->p3,
            'pack4' => $request->p4,
            'pack5' => $request->p5,
            'pack6' => $request->p6,
            'pack7' => $request->p7,
            'pack8' => $request->p8,
            'pack9' => $request->p9,
            'pack10' => $request->p10,
        ]);

        return redirect()->back()->with('message', 'Pack Added!');
    }

    public function editPost(Request $request) {
        $rules = [
            'vendor_id' => 'required',
            'pack_name' => 'required',
            'p1' => 'required|integer'
        ];

        if ($request->p3 && $request->p3 != "")
            $rules['p2'] = 'required|integer';

        if ($request->p4 && $request->p4 != "") {
            $rules['p2'] = 'required|integer';
            $rules['p3'] = 'required|integer';
        }

        if ($request->p5 && $request->p5 != "") {
            $rules['p2'] = 'required|integer';
            $rules['p3'] = 'required|integer';
            $rules['p4'] = 'required|integer';
        }

        if ($request->p6 && $request->p6 != "") {
            $rules['p2'] = 'required|integer';
            $rules['p3'] = 'required|integer';
            $rules['p4'] = 'required|integer';
            $rules['p5'] = 'required|integer';
        }

        if ($request->p7 && $request->p7 != "") {
            $rules['p2'] = 'required|integer';
            $rules['p3'] = 'required|integer';
            $rules['p4'] = 'required|integer';
            $rules['p5'] = 'required|integer';
            $rules['p6'] = 'required|integer';
        }

        if ($request->p8 && $request->p8 != "") {
            $rules['p2'] = 'required|integer';
            $rules['p3'] = 'required|integer';
            $rules['p4'] = 'required|integer';
            $rules['p5'] = 'required|integer';
            $rules['p6'] = 'required|integer';
            $rules['p7'] = 'required|integer';
        }

        if ($request->p9 && $request->p9 != "") {
            $rules['p2'] = 'required|integer';
            $rules['p3'] = 'required|integer';
            $rules['p4'] = 'required|integer';
            $rules['p5'] = 'required|integer';
            $rules['p6'] = 'required|integer';
            $rules['p7'] = 'required|integer';
            $rules['p8'] = 'required|integer';
        }

        if ($request->p10 && $request->p10 != "") {
            $rules['p2'] = 'required|integer';
            $rules['p3'] = 'required|integer';
            $rules['p4'] = 'required|integer';
            $rules['p5'] = 'required|integer';
            $rules['p6'] = 'required|integer';
            $rules['p7'] = 'required|integer';
            $rules['p8'] = 'required|integer';
            $rules['p9'] = 'required|integer';
        }

        $request->validate($rules);

        if ($request->default) {
            Pack::where('vendor_meta_id', $request->vendor_id)->update([ 'default' => 0 ]);
        }

        $pack = Pack::where('id', $request->packId)->first();
        $pack->name = $request->pack_name;
        $pack->status = $request->status;
        $pack->default = ($request->default) ? 1 : 0;
        $pack->	vendor_meta_id = $request->vendor_id;
        $pack->pack1 = $request->p1;
        $pack->pack2 = $request->p2;
        $pack->pack3 = $request->p3;
        $pack->pack4 = $request->p4;
        $pack->pack5 = $request->p5;
        $pack->pack6 = $request->p6;
        $pack->pack7 = $request->p7;
        $pack->pack8 = $request->p8;
        $pack->pack9 = $request->p9;
        $pack->pack10 = $request->p10;
        $pack->save();

        return redirect()->back()->with('message', 'Pack Updated!');
    }

    public function delete(Request $request) {
        $pack = Pack::where('id', $request->id)->first();
        $pack->delete();
    }

    public function changeStatus(Request $request) {
        $pack = Pack::where('id', $request->id)->first();
        $pack->status = $request->status;
        $pack->save();
    }
}
