<?php

namespace App\Http\Controllers\Admin;

use App\Enumeration\Role;
use App\Model\User;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Hash;
use Auth;

class AuthController extends Controller
{
    public function login() {
        return view('admin.auth.login');
    }

    public function loginPost(Request $request) {
        $user = User::where('email', $request->email)->where('role', Role::$ADMIN)->first();

        if (!$user)
            return redirect()->route('login_admin')->with('message', 'User not found.')->withInput();

        if (Hash::check($request->password, $user->password)) {
            if ($request->remember_me)
                Auth::login($user, true);
            else
                Auth::login($user);

            return redirect()->route('admin_dashboard');
        }

        return redirect()->route('login_admin')->with('message', 'Invalid Password.')->withInput();
    }

    public function logout() {
        Auth::logout();

        return redirect()->route('login_admin');
    }
}
