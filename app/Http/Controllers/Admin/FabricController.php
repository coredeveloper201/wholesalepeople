<?php

namespace App\Http\Controllers\Admin;

use App\Model\Fabric;
use App\Model\MasterFabric;
use App\Model\MetaVendor;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class FabricController extends Controller
{
    public function index(Request $request) {
        if (isset($request->id))
            $fabrics = Fabric::where('vendor_meta_id', $request->id)->orderBy('name')->with('vendor')->paginate(10);
        else
            $fabrics = Fabric::orderBy('name')->with('vendor')->paginate(10);

        $masterFabrics = MasterFabric::orderBy('name')->get();
        $vendors = MetaVendor::orderBy('company_name')->get();

        return view('admin.fabric.index', compact('masterFabrics', 'fabrics', 'vendors'))->with('page_title', 'Fabric');
    }

    public function fabricAdd(Request $request) {
        $request->validate([
            'vendor_id' => 'required',
            'master_fabric' => 'required',
            'fabric_description' => 'required',
        ]);

        if ($request->default) {
            Fabric::where('vendor_meta_id', $request->vendor_id)->update([ 'default' => 0 ]);
        }

        $fabric = Fabric::create([
            'name' => $request->fabric_description,
            'status' => $request->statusFabric,
            'default' => ($request->defaultFabric) ? 1 : 0,
            'vendor_meta_id' => $request->vendor_id,
            'master_fabric_id' => $request->master_fabric
        ]);

        return redirect()->back()->with('message', 'Fabric Added!');
    }

    public function fabricUpdate(Request $request) {
        $request->validate([
            'vendor_id' => 'required',
            'master_fabric' => 'required',
            'fabric_description' => 'required',
        ]);

        if ($request->default) {
            Fabric::where('vendor_meta_id', $request->vendor_id)->update([ 'default' => 0 ]);
        }

        $fabric = Fabric::where('id', $request->fabricId)->first();
        $fabric->name = $request->fabric_description;
        $fabric->status = $request->statusFabric;
        $fabric->default = ($request->defaultFabric) ? 1 : 0;
        $fabric->vendor_meta_id = $request->vendor_id;
        $fabric->master_fabric_id = $request->master_fabric;

        $fabric->save();

        return redirect()->back()->with('message', 'Fabric Updated!');
    }

    public function fabricDelete(Request $request) {
        Fabric::where('id', $request->id)->delete();
    }

    public function fabricChangeStatus(Request $request) {
        $pack = Fabric::where('id', $request->id)->first();
        $pack->status = $request->status;
        $pack->save();
    }
}
