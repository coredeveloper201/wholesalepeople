<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Mail;

class StaticPagesController extends Controller
{
    public function aboutUs() {
        return view('pages.static.about_us');
    }

    public function contactUs() {
        return view('pages.static.contact_us');
    }

    public function contactUsPost(Request $request) {
        $request->validate([
            'name' => 'required|string|max:191',
            'email' => 'required|email|max:191',
            'phone' => 'nullable|max:191',
            'comment' => 'required|string|max:1000',
        ]);

        $data = [
            'name' => $request->name,
            'email' => $request->email,
            'phone' => $request->phone,
            'comment' => $request->comment,
        ];


        Mail::send('emails.contact_us', $data, function ($message) use ($request) {
            $message->subject('Contact Us');
            $message->to(config('mail.from.address'), config('mail.from.name'));
        });

        return redirect()->route('contact_us')->with('message', 'Successfully Submitted!');
    }

    public function returnExchange() {
        return view('pages.static.return_exchange');
    }

    public function showSchedule() {
        return view('pages.static.show_schedule');
    }

    public function photoshoot() {
        return view('pages.static.photoshoot');
    }

    public function shippingInformation() {
        return view('pages.static.shipping_information');
    }

    public function refundPolicy() {
        return view('pages.static.refund_policy');
    }

    public function privacyPolicy() {
        return view('pages.static.privacy_policy');
    }

    public function termsConditions() {
        return view('pages.static.terms_and_conditions');
    }
}
