<?php

namespace App\Http\Controllers\Vendor;

use App\Model\Size;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Auth;

class SizeController extends Controller
{
    public function index() {
        $sizes = Size::where('user_id', Auth::user()->id)->get();

        return view('vendor.dashboard.item_settings.size', compact('sizes'))->with('page_title', 'Size');
    }

    public function addPost(Request $request) {
        $rules = [
            'size_name' => 'required',
            's1' => 'required'
        ];

        if ($request->s3 && $request->s3 != "")
            $rules['s2'] = 'required';

        if ($request->s4 && $request->s4 != "") {
            $rules['s2'] = 'required';
            $rules['s3'] = 'required';
        }

        if ($request->s5 && $request->s5 != "") {
            $rules['s2'] = 'required';
            $rules['s3'] = 'required';
            $rules['s4'] = 'required';
        }

        if ($request->s6 && $request->s6 != "") {
            $rules['s2'] = 'required';
            $rules['s3'] = 'required';
            $rules['s4'] = 'required';
            $rules['s5'] = 'required';
        }

        if ($request->s7 && $request->s7 != "") {
            $rules['s2'] = 'required';
            $rules['s3'] = 'required';
            $rules['s4'] = 'required';
            $rules['s5'] = 'required';
            $rules['s6'] = 'required';
        }

        if ($request->s8 && $request->s8 != "") {
            $rules['s2'] = 'required';
            $rules['s3'] = 'required';
            $rules['s4'] = 'required';
            $rules['s5'] = 'required';
            $rules['s6'] = 'required';
            $rules['s7'] = 'required';
        }

        if ($request->s9 && $request->s9 != "") {
            $rules['s2'] = 'required';
            $rules['s3'] = 'required';
            $rules['s4'] = 'required';
            $rules['s5'] = 'required';
            $rules['s6'] = 'required';
            $rules['s7'] = 'required';
            $rules['s8'] = 'required';
        }

        if ($request->s10 && $request->s10 != "") {
            $rules['s2'] = 'required';
            $rules['s3'] = 'required';
            $rules['s4'] = 'required';
            $rules['s5'] = 'required';
            $rules['s6'] = 'required';
            $rules['s7'] = 'required';
            $rules['s8'] = 'required';
            $rules['s9'] = 'required';
        }

        $request->validate($rules);

        if ($request->default) {
            Size::where('user_id', Auth::user()->id)->update([ 'default' => 0 ]);
        }

        $size = Size::create([
            'name' => $request->size_name,
            'status' => $request->status,
            'default' => ($request->default) ? 1 : 0,
            'user_id' => Auth::user()->id,
            'size1' => $request->s1,
            'size2' => $request->s2,
            'size3' => $request->s3,
            'size4' => $request->s4,
            'size5' => $request->s5,
            'size6' => $request->s6,
            'size7' => $request->s7,
            'size8' => $request->s8,
            'size9' => $request->s9,
            'size10' => $request->s10,
        ]);

        return redirect()->route('vendor_size')->with('message', 'Size Added!');
    }

    public function editPost(Request $request) {
        $rules = [
            'size_name' => 'required',
            's1' => 'required'
        ];

        if ($request->s3 && $request->s3 != "")
            $rules['s2'] = 'required';

        if ($request->s4 && $request->s4 != "") {
            $rules['s2'] = 'required';
            $rules['s3'] = 'required';
        }

        if ($request->s5 && $request->s5 != "") {
            $rules['s2'] = 'required';
            $rules['s3'] = 'required';
            $rules['s4'] = 'required';
        }

        if ($request->s6 && $request->s6 != "") {
            $rules['s2'] = 'required';
            $rules['s3'] = 'required';
            $rules['s4'] = 'required';
            $rules['s5'] = 'required';
        }

        if ($request->s7 && $request->s7 != "") {
            $rules['s2'] = 'required';
            $rules['s3'] = 'required';
            $rules['s4'] = 'required';
            $rules['s5'] = 'required';
            $rules['s6'] = 'required';
        }

        if ($request->s8 && $request->s8 != "") {
            $rules['s2'] = 'required';
            $rules['s3'] = 'required';
            $rules['s4'] = 'required';
            $rules['s5'] = 'required';
            $rules['s6'] = 'required';
            $rules['s7'] = 'required';
        }

        if ($request->s9 && $request->s9 != "") {
            $rules['s2'] = 'required';
            $rules['s3'] = 'required';
            $rules['s4'] = 'required';
            $rules['s5'] = 'required';
            $rules['s6'] = 'required';
            $rules['s7'] = 'required';
            $rules['s8'] = 'required';
        }

        if ($request->s10 && $request->s10 != "") {
            $rules['s2'] = 'required';
            $rules['s3'] = 'required';
            $rules['s4'] = 'required';
            $rules['s5'] = 'required';
            $rules['s6'] = 'required';
            $rules['s7'] = 'required';
            $rules['s8'] = 'required';
            $rules['s9'] = 'required';
        }

        $request->validate($rules);

        if ($request->default) {
            Size::where('user_id', Auth::user()->id)->update([ 'default' => 0 ]);
        }

        $size = Size::where('id', $request->sizeId)->first();
        $size->name = $request->size_name;
        $size->status = $request->status;
        $size->default = ($request->default) ? 1 : 0;
        $size->size1 = $request->s1;
        $size->size2 = $request->s2;
        $size->size3 = $request->s3;
        $size->size4 = $request->s4;
        $size->size5 = $request->s5;
        $size->size6 = $request->s6;
        $size->size7 = $request->s7;
        $size->size8 = $request->s8;
        $size->size9 = $request->s9;
        $size->size10 = $request->s10;
        $size->save();
        $size->touch();

        return redirect()->route('vendor_size')->with('message', 'Size Updated!');
    }

    public function delete(Request $request) {
        $size = Size::where('id', $request->id)->first();
        $size->delete();
    }

    public function changeStatus(Request $request) {
        $size = Size::where('id', $request->id)->first();
        $size->status = $request->status;
        $size->save();
    }

    public function changeDefault(Request $request) {
        Size::where('user_id', Auth::user()->id)->update([ 'default' => 0 ]);
        Size::where('id', $request->id)->update([ 'default' => 1 ]);
    }
}
