<?php

namespace App\Http\Controllers\Vendor;

use App\Model\Item;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Auth;

class OtherController extends Controller
{
    public function getItems(Request $request) {
        $query = Item::query();
        $query->where('status', 1);
        $query->where('vendor_meta_id', Auth::user()->vendor_meta_id);

        if ($request->search && $request->search != '')
            $query->where('style_no', 'like', '%'.$request->search.'%');

        if ($request->category && $request->category != '')
            $query->whereHas('categories', function ($query) use ($request) {
                $query->where('id', $request->category);
            });

        $query->orderBy('activated_at', 'desc');

        $items = $query->paginate(21);
        $paginationView = $items->links();
        $paginationView = trim(preg_replace('/\r\n/', ' ', $paginationView));

        foreach($items as &$item) {
            // Image
            $imagePath = asset('images/no-image.png');

            if (sizeof($item->images) > 0)
                $imagePath = asset($item->images[0]->list_image_path);

            $item->imagePath = $imagePath;
        }

        return ['items' => $items->toArray(), 'pagination' => $paginationView];
    }
}
