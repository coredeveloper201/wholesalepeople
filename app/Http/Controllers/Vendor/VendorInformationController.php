<?php

namespace App\Http\Controllers\Vendor;

use App\Model\Country;
use App\Model\Industry;
use App\Model\State;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Auth;

class VendorInformationController extends Controller
{
    public function index() {
        $industries = Industry::orderBy('name')->get();
        $user = Auth::user();
        $user->load('vendor', 'industries');

        $countries = Country::orderBy('name')->get();
        $usStates = State::where('country_id', 1)->orderBy('name')->get()->toArray();
        $caStates =State::where('country_id', 2)->orderBy('name')->get()->toArray();

        return view('vendor.dashboard.administration.vendor_information.index', compact('industries', 'user', 'countries', 'usStates', 'caStates'))
            ->with('page_title', 'Vendor Information');
    }

    public function companyInformationPost(Request $request) {
        $request->validate([
            'company_description' => 'nullable|max:1000',
            'year_established' => 'nullable|digits:4',
            'business_category' => 'nullable|max:255',
            'email' => 'required|email|max:255|unique:users,email,'.Auth::user()->id,
            'showroom_address' => 'required|max:255',
            'showroom_city' => 'required|max:255',
            'showroom_zip_code' => 'required|max:255',
            'showroom_country' => 'required',
            'showroom_tel' => 'required|max:255',
            'showroom_alt' => 'nullable|max:255',
            'warehouse_address' => 'required|max:255',
            'warehouse_city' => 'required|max:255',
            'warehouse_zip_code' => 'required|max:255',
            'warehouse_country' => 'required',
            'warehouse_tel' => 'required|max:255',
            'warehouse_alt' => 'nullable|max:255',
            'first_name' => 'required|max:255',
            'last_name' => 'required|max:255',
        ]);

        $user = Auth::user();
        $user->load('vendor');

        $user->vendor->company_info = $request->company_description;
        $user->vendor->year_established = $request->year_established;
        $user->vendor->business_category = $request->business_category;
        $user->email = $request->email;
        $user->first_name = $request->first_name;
        $user->last_name = $request->last_name;
        $user->vendor->billing_address = $request->showroom_address;
        $user->vendor->billing_city = $request->showroom_city;
        $user->vendor->billing_zip = $request->showroom_zip_code;
        $user->vendor->billing_country_id = $request->showroom_country;
        $user->vendor->billing_phone = $request->showroom_tel;
        $user->vendor->billing_alternate_phone = $request->showroom_alt;
        $user->vendor->factory_address = $request->warehouse_address;
        $user->vendor->factory_city = $request->warehouse_city;
        $user->vendor->factory_zip = $request->warehouse_zip_code;
        $user->vendor->factory_country_id = $request->warehouse_country;
        $user->vendor->factory_phone = $request->warehouse_tel;
        $user->vendor->factory_alternate_phone = $request->warehouse_alt;
        $user->vendor->primary_customer_market = $request->primaryCustomerMarket;

        $showroomState = null;
        $warehouseState = null;
        if ($request->warehouse_state && $request->warehouse_state != '')
            $warehouseState = $request->warehouse_state;

        if ($request->showroom_state && $request->showroom_state != '')
            $showroomState = $request->showroom_state;

        $user->vendor->billing_state_id = $showroomState;
        $user->vendor->factory_state_id = $warehouseState;


        $user->industries()->sync($request->industry);
        $user->vendor->save();
        $user->save();

        return redirect()->route('vendor_vendor_information')->with('message', 'Company Info Updated!');
    }

    public function sizeChartPost(Request $request) {
        $user = Auth::user();
        $user->load('vendor');
        $user->vendor->size_chart = $request->description;
        $user->vendor->save();
    }

    public function orderNoticePost(Request $request) {
        $user = Auth::user();
        $user->load('vendor');
        $user->vendor->order_notice = $request->description;
        $user->vendor->save();
    }
}
