<?php

namespace App\Http\Controllers\Vendor;

use App\Enumeration\Availability;
use App\Model\BodySize;
use App\Model\CartItem;
use App\Model\Category;
use App\Model\Color;
use App\Model\DefaultCategory;
use App\Model\Fabric;
use App\Model\Item;
use App\Model\ItemImages;
use App\Model\ItemView;
use App\Model\Length;
use App\Model\MadeInCountry;
use App\Model\MasterColor;
use App\Model\MasterFabric;
use App\Model\Pack;
use App\Model\Pattern;
use App\Model\SliderItem;
use App\Model\Style;
use App\Model\WishListItem;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\URL;
use Uuid;
use DateTime;
use App\Libraries\SSP;
use Carbon\Carbon;
use File;
use Excel;
use Image;

class ItemController extends Controller
{
    public function createNewItemIndex() {
        $categories = Category::where('vendor_meta_id', Auth::user()->vendor_meta_id)
            ->where('status', 1)->where('parent', 0)
            ->with('subCategories')->orderBy('name')->get();

        $packs = Pack::where('vendor_meta_id', Auth::user()->vendor_meta_id)->where('status', 1)->orderBy('name')->get();
        $fabrics = Fabric::where('vendor_meta_id', Auth::user()->vendor_meta_id)->where('status', 1)->orderBy('name')->get();
        $madeInCountries = MadeInCountry::where('vendor_meta_id', Auth::user()->vendor_meta_id)->where('status', 1)->orderBy('name')->get();
        $bodySizes = BodySize::orderBy('name')->get();
        $patterns = Pattern::orderBy('name')->get();
        $lengths = Length::orderBy('name')->get();
        $styles = Style::orderBy('name')->get();
        $colors = Color::where('vendor_meta_id', Auth::user()->vendor_meta_id)->where('status', 1)->orderBy('name')->get();

        // Master Fabric
        /*$masterFabricsIds = [];

        foreach($fabrics as $fabric)
            $masterFabricsIds[] = $fabric->master_fabric_id;

        $masterFabricsIds = array_unique($masterFabricsIds);*/

        $masterFabrics = MasterFabric::orderBy('name')->get();

        // Default Categories
        $defaultCategories = [];

        $categoriesCollection = DefaultCategory::orderBy('sort')->orderBy('name')->get();

        foreach($categoriesCollection as $cc) {
            if ($cc->parent == 0) {
                $data = [
                    'id' => $cc->id,
                    'name' => $cc->name
                ];

                $subCategories = [];
                foreach($categoriesCollection as $item) {
                    if ($item->parent == $cc->id) {
                        $data2 = [
                            'id' => $item->id,
                            'name' => $item->name
                        ];

                        $data3 = [];
                        foreach($categoriesCollection as $item2) {
                            if ($item2->parent == $item->id) {
                                $data3[] = [
                                    'id' => $item2->id,
                                    'name' => $item2->name
                                ];
                            }
                        }

                        $data2['subCategories'] = $data3;
                        $subCategories[] = $data2;
                    }
                }

                $data['subCategories'] = $subCategories;
                $defaultCategories[] = $data;
            }
        }

        // Master Color
        $masterColors = MasterColor::orderBy('name')->get();

        return view('vendor.dashboard.create_new_item.index', compact('categories', 'packs', 'fabrics', 'madeInCountries',
            'defaultCategories', 'bodySizes', 'patterns', 'lengths', 'styles', 'colors', 'masterFabrics', 'masterColors'))
            ->with('page_title', 'Create a New Item');
    }

    public function addColor(Request $request) {
        if ($request->id == '' || $request->name == '')
            return response()->json(['success' => false, 'message' => 'Invalid parameters.']);

        $mc = Color::where('vendor_meta_id', Auth::user()->vendor_meta_id)
            ->where('name', $request->name)->first();

        if ($mc)
            return response()->json(['success' => false, 'message' => 'Already have this color.']);

        $vendorId = Auth::user()->vendor_meta_id;

        if ($request->vendorId && $request->vendorId != '')
            $vendorId = $request->vendorId;

        $mc = Color::create([
            'name' => $request->name,
            'status' => 1,
            'master_color_id' => $request->id,
            'vendor_meta_id' => $vendorId,
        ]);

        return response()->json(['success' => true, 'color' => $mc->toArray()]);
    }

    public function createNewItemPost(Request $request) {
        $request->validate([
            'style_no' => 'required|max: 255',
            'category' => 'required',
            'price' => 'required|regex:/^-?[0-9]+(?:\.[0-9]{1,2})?$/',
            'orig_price' => 'nullable|numeric|min:'.$request->price,
            'size' => 'required',
            'sorting' => 'nullable|integer',
            'description' => 'nullable|max:500',
            'd_parent_category' => 'required',
            'keywords' => 'nullable|max:200',
            'min_qty' => 'nullable|integer',
            'memo' => 'nullable|max:255',
            'vendor_style_no' => 'nullable|max:255',
            'colors' => 'required',
        ]);

        $availableDate = null;
        if ($request->available_on) {
            $availableOn = DateTime::createFromFormat('m/d/Y', $request->available_on);
            $availableDate = $availableOn->format('Y-m-d');
        }

        $exclusive = 0;
        $evenColor =0;

        if ($request->exclusive)
            $exclusive = 1;

        if ($request->even_color)
            $evenColor = 1;

        $item = Item::create([
            'vendor_meta_id' => Auth::user()->vendor_meta_id,
            'status' => $request->status,
            'style_no' => $request->style_no,
            'price' => $request->price,
            'orig_price' => $request->orig_price,
            'pack_id' => $request->size,
            'sorting' => $request->sorting,
            'description' => $request->description,
            'available_on' => $availableDate,
            'availability' => $request->availability,
            'name' => $request->item_name,
            'default_parent_category' => $request->d_parent_category,
            'default_second_category' => $request->d_second_parent_category,
            'default_third_category' => $request->d_third_parent_category,
            'exclusive' => $exclusive,
            'min_qty' => $request->min_qty,
            'even_color' => $evenColor,
            'fabric' => $request->fabric,
            'made_in_id' => $request->made_n,
            'labeled' => $request->labeled,
            'keywords' => $request->keywords,
            'body_size_id' => $request->body_size,
            'pattern_id' => $request->pattern,
            'length_id' => $request->length,
            'style_id' => $request->style,
            'master_fabric_id' => $request->master_fabric,
            'memo' => $request->memo,
            'vendor_style_no' => $request->vendor_style_no,
            'activated_at' => ($request->status == '1' ? Carbon::now()->toDateTimeString() : null)
        ]);

        $item->colors()->attach($request->colors);
        $item->categories()->attach($request->category);

        if ($request->imagesId) {
            for ($i = 0; $i < sizeof($request->imagesId); $i++) {
                $image = ItemImages::where('id', $request->imagesId[$i])->first();

                $filename = Uuid::generate()->string;
                $ext = pathinfo($image->image_path, PATHINFO_EXTENSION);

                $listSavePath = 'images/vendors/'.Auth::user()->vendor_meta_id.'/list/' . $filename . '.' . $ext;
                $originalSavePath = 'images/vendors/'.Auth::user()->vendor_meta_id.'/original/' . $filename . '.' . $ext;
                $thumbsSavePath = 'images/vendors/'.Auth::user()->vendor_meta_id.'/thumbs/' . $filename . '.' . $ext;

                // List Image
                $img = Image::make(public_path($image->image_path))->resize(200, 300);
                $img->save(public_path($listSavePath), 100);

                // Thumbs Image
                $thumb = Image::make(public_path($image->image_path))->resize(150, 150);
                $thumb->save(public_path($thumbsSavePath), 100);

                File::move(public_path($image->image_path), public_path($originalSavePath));

                $image->item_id = $item->id;
                $image->color_id = $request->imageColor[$i];
                $image->sort = $i + 1;
                $image->image_path = $originalSavePath;
                $image->list_image_path = $listSavePath;
                $image->thumbs_image_path = $thumbsSavePath;
                $image->save();

                /*ItemImages::where('id', $request->imagesId[$i])->update([
                    'item_id' => $item->id,
                    'color_id' => $request->imageColor[$i],
                    'sort' => $i + 1,
                ]);*/
            }
        }


        //return redirect()->route('vendor_item_list_by_category', ['category' => $item->category_id])->with('message', 'Item Added!');
        return redirect()->route('vendor_item_list_all');
    }

    public function uploadImage(Request $request) {
        $filename = Uuid::generate()->string;
        $file = $request->file('file');
        $ext = $file->getClientOriginalExtension();
        $destinationPath = '/images/item';

        $file->move(public_path($destinationPath), $filename.".".$ext);

        $imagePath = $destinationPath."/".$filename.".".$ext;

        $image = ItemImages::create([
            'image_path' => $imagePath
        ]);

        $image->fullPath = asset($imagePath);

        return response()->json(['success' => true, 'data' => $image->toArray()]);
    }

    public function itemListByCategory(Category $category, Request $request) {
        $category->load('subCategories');

        // With Sub Category
        $withSubCat = [$category->id];

        foreach ($category->subCategories as $sub)
            $withSubCat[] = $sub->id;

        // Active Items
        $activeItemsQuery = Item::query();
        $activeItemsQuery->where('status', 1)
            ->where('vendor_meta_id', Auth::user()->vendor_meta_id)
            ->whereHas('categories', function ($query) use ($withSubCat) {
                $query->whereIn('id', $withSubCat);
            })
            ->with('categories', 'images');

        // Search
        if (isset($request->text) && $request->text != '') {
            if (isset($request->style) && $request->style == '1')
                $activeItemsQuery->where('style_no', 'like', '%' . $request->text . '%');

            if (isset($request->vendorStyle) && $request->vendorStyle == '1')
                $activeItemsQuery->where('vendor_style_no', 'like', '%' . $request->text . '%');

            if (isset($request->des) && $request->des == '1')
                $activeItemsQuery->where('description', 'like', '%' . $request->text . '%');

            if (isset($request->name) && $request->name == '1')
                $activeItemsQuery->where('name', 'like', '%' . $request->text . '%');
        }

        // Order
        if (isset($request->s1) && $request->s1 != '') {
            if ($request->s1 == '4')
                $activeItemsQuery->orderBy('price');
            else if ($request->s1 == '1')
                $activeItemsQuery->orderBy('updated_at', 'desc');
            else if ($request->s1 == '2')
                $activeItemsQuery->orderBy('created_at', 'desc');
            else if ($request->s1 == '3')
                $activeItemsQuery->orderBy('activated_at', 'desc');
            else if ($request->s1 == '5')
                $activeItemsQuery->orderBy('price', 'desc');
            else if ($request->s1 == '6')
                $activeItemsQuery->orderBy('style_no');
        } else {
            $activeItemsQuery->orderBy('activated_at', 'desc');
        }

        $activeItems = $activeItemsQuery->paginate(40, ['*'], 'p1');


        // Inactive Items
        $inactiveItemsQuery = Item::query();
        $inactiveItemsQuery->where('status', 0)
            ->where('vendor_meta_id', Auth::user()->vendor_meta_id)
            ->whereHas('categories', function ($query) use ($withSubCat) {
                $query->whereIn('id', $withSubCat);
            })
            ->with('categories', 'images');

        // Search
        if (isset($request->text) && $request->text != '') {
            if (isset($request->style) && $request->style == '1')
                $inactiveItemsQuery->where('style_no', 'like', '%' . $request->text . '%');

            if (isset($request->vendorStyle) && $request->vendorStyle == '1')
                $inactiveItemsQuery->where('vendor_style_no', 'like', '%' . $request->text . '%');

            if (isset($request->des) && $request->des == '1')
                $inactiveItemsQuery->where('description', 'like', '%' . $request->text . '%');

            if (isset($request->name) && $request->name == '1')
                $inactiveItemsQuery->where('name', 'like', '%' . $request->text . '%');
        }

        // Order

        if (isset($request->s2) && $request->s2 != '') {
            if ($request->s2 == '4')
                $inactiveItemsQuery->orderBy('price');
            else if ($request->s2 == '1')
                $inactiveItemsQuery->orderBy('updated_at', 'desc');
            else if ($request->s2 == '2')
                $inactiveItemsQuery->orderBy('created_at', 'desc');
            else if ($request->s2 == '3') {
                $inactiveItemsQuery->orderBy('activated_at', 'desc');
                $inactiveItemsQuery->orderBy('created_at', 'desc');
            } else if ($request->s2 == '5')
                $inactiveItemsQuery->orderBy('price', 'desc');
            else if ($request->s2 == '6')
                $inactiveItemsQuery->orderBy('style_no');
        } else {
            $inactiveItemsQuery->orderBy('activated_at', 'desc');
            $inactiveItemsQuery->orderBy('created_at', 'desc');
        }

        $inactiveItems = $inactiveItemsQuery->paginate(40, ['*'], 'p2');

        $appends = [
            'p1' => $activeItems->currentPage(),
            'p2' => $inactiveItems->currentPage(),
        ];

        foreach ($request->all() as $key => $value) {
            if ($key != 'p1' && $key != 'p2')
                $appends[$key] = ($value == null) ? '' : $value;
        }

        $categories = Category::where('parent', 0)->where('vendor_meta_id', Auth::user()->vendor_meta_id)
            ->where('status', 1)->orderBy('sort')->orderBy('name')->with('subCategories')->get();

        return view('vendor.dashboard.item_list.index', compact('category', 'activeItems', 'inactiveItems', 'categories', 'appends'))
            ->with('page_title', $category->name);
    }

    public function itemsChangeCategory(Request $request) {
        Item::whereIn('id', $request->ids)->update(['category_id' => $request->category_id]);
    }

    public function itemsChangeToInactive(Request $request) {
        CartItem::whereIn('item_id', $request->ids)->delete();
        WishListItem::whereIn('item_id', $request->ids)->delete();
        SliderItem::whereIn('item_id', $request->ids)->delete();
        ItemView::whereIn('item_id', $request->ids)->delete();
        Item::whereIn('id', $request->ids)->update(['status' => 0]);
    }

    public function itemsChangeToActive(Request $request) {
        $time = Carbon::now();

        Item::whereIn('id', $request->ids)->update([
            'status' => 1,
            'activated_at' => $time->toDateTimeString()
        ]);
    }

    public function itemsDelete(Request $request) {
        CartItem::whereIn('item_id', $request->ids)->delete();
        WishListItem::whereIn('item_id', $request->ids)->delete();
        SliderItem::whereIn('item_id', $request->ids)->delete();
        ItemView::whereIn('item_id', $request->ids)->delete();
        Item::whereIn('id', $request->ids)->delete();
    }

    public function checkStyleNo(Request $request) {
        if (isset($request->except))
            $item = Item::where('style_no', $request->style_no)
                ->where('vendor_meta_id', Auth::user()->vendor_meta_id)
                ->where('id', '!=', $request->except)->first();
        else
            $item = Item::where('style_no', $request->style_no)
                ->where('vendor_meta_id', Auth::user()->vendor_meta_id)->first();

        if ($item)
            return response()->json(['success' => false, 'message' => 'This style no. already taken.']);
        else
            return response()->json(['success' => true, 'message' => 'No item found.']);
    }

    public function editItem(Item $item) {
        $item->load('colors', 'images', 'categories');
        $categories = Category::where('vendor_meta_id', Auth::user()->vendor_meta_id)
            ->where('status', 1)->where('parent', 0)
            ->with('subCategories')->orderBy('name')->get();

        $packs = Pack::where('vendor_meta_id', Auth::user()->vendor_meta_id)->where('status', 1)->orderBy('name')->get();
        $fabrics = Fabric::where('vendor_meta_id', Auth::user()->vendor_meta_id)->where('status', 1)->orderBy('name')->get();
        $madeInCountries = MadeInCountry::where('vendor_meta_id', Auth::user()->vendor_meta_id)->where('status', 1)->orderBy('name')->get();
        $bodySizes = BodySize::orderBy('name')->get();
        $patterns = Pattern::orderBy('name')->get();
        $lengths = Length::orderBy('name')->get();
        $styles = Style::orderBy('name')->get();
        $colors = Color::where('vendor_meta_id', Auth::user()->vendor_meta_id)->where('status', 1)->orderBy('name')->get();

        // Images color id
        $imagesColorIds = [];
        foreach($item->images as $img)
            $imagesColorIds[] = $img->color_id;

        // Master Fabric
        /*$masterFabricsIds = [];

        foreach($fabrics as $fabric)
            $masterFabricsIds[] = $fabric->master_fabric_id;

        $masterFabricsIds = array_unique($masterFabricsIds);*/

        $masterFabrics = MasterFabric::orderBy('name')->get();

        // Default Categories
        $defaultCategories = [];

        $categoriesCollection = DefaultCategory::orderBy('sort')->orderBy('name')->get();

        foreach($categoriesCollection as $cc) {
            if ($cc->parent == 0) {
                $data = [
                    'id' => $cc->id,
                    'name' => $cc->name
                ];

                $subCategories = [];
                foreach($categoriesCollection as $cat) {
                    if ($cat->parent == $cc->id) {
                        $data2 = [
                            'id' => $cat->id,
                            'name' => $cat->name
                        ];

                        $data3 = [];
                        foreach($categoriesCollection as $item2) {
                            if ($item2->parent == $cat->id) {
                                $data3[] = [
                                    'id' => $item2->id,
                                    'name' => $item2->name
                                ];
                            }
                        }

                        $data2['subCategories'] = $data3;
                        $subCategories[] = $data2;
                    }
                }

                $data['subCategories'] = $subCategories;
                $defaultCategories[] = $data;
            }
        }

        // Master Color
        $masterColors = MasterColor::orderBy('name')->get();

        if (session('message') == null) {
            session(['back_url' => URL::previous()]);
        }

        return view('vendor.dashboard.item_list.edit_item', compact('categories', 'packs', 'fabrics', 'madeInCountries',
            'defaultCategories', 'bodySizes', 'patterns', 'lengths', 'styles', 'colors', 'masterFabrics', 'item', 'imagesColorIds', 'masterColors'))
            ->with('page_title', 'Item Edit');
    }

    public function editItemPost(Item $item, Request $request) {
        $request->validate([
            'style_no' => 'required|max: 255',
            'category' => 'required',
            'price' => 'required|regex:/^-?[0-9]+(?:\.[0-9]{1,2})?$/',
            'orig_price' => 'nullable|numeric|min:'.$request->price,
            'size' => 'required',
            'sorting' => 'nullable|integer',
            'description' => 'nullable|max:500',
            'd_parent_category' => 'required',
            'keywords' => 'nullable|max:200',
            'min_qty' => 'nullable|integer',
            'memo' => 'nullable|max:255',
            'vendor_style_no' => 'nullable|max:255',
            'colors' => 'required'
        ]);
        $ChangeInfo = $request->ChangeInfo;
        // ChangeInfo = [ colors, category, imagesId ]

        $availableDate = null;
        if ($request->available_on) {
            $availableOn = DateTime::createFromFormat('m/d/Y', $request->available_on);
            $availableDate = $availableOn->format('Y-m-d');
        }

        $exclusive = 0;
        $evenColor =0;

        if ($request->exclusive)
            $exclusive = 1;

        if ($request->even_color)
            $evenColor = 1;

        if ($item->status == '0' && $request->status == '1')
            $item->activated_at = Carbon::now()->toDateTimeString();

        $item->status = $request->status;
        $item->style_no = $request->style_no;
        $item->price = $request->price;
        $item->orig_price = $request->orig_price;
        $item->pack_id = $request->size;
        $item->sorting = $request->sorting;
        $item->description = $request->description;
        $item->available_on = $availableDate;
        $item->availability = $request->availability;
        $item->name = $request->item_name;
        $item->default_parent_category = $request->d_parent_category;
        $item->default_second_category = $request->d_second_parent_category;
        $item->default_third_category = $request->d_third_parent_category;
        $item->exclusive = $exclusive;
        $item->min_qty = $request->min_qty;
        $item->even_color = $evenColor;
        $item->fabric = $request->fabric;
        $item->made_in_id = $request->made_n;
        $item->labeled = $request->labeled;
        $item->keywords = $request->keywords;
        $item->body_size_id = $request->body_size;
        $item->pattern_id = $request->pattern;
        $item->length_id = $request->length;
        $item->style_id = $request->style;
        $item->master_fabric_id = $request->master_fabric;
        $item->memo = $request->memo;
        $item->vendor_style_no = $request->vendor_style_no;

        $item->save();
        $item->touch();
        if(isset($ChangeInfo['color__']) && $ChangeInfo['color__'] == 1){
            $item->colors()->sync($request->colors);
        }
        if(isset($ChangeInfo['category__']) && $ChangeInfo['category__'] == 1){
            $item->categories()->sync($request->category);
        }

        if((isset($ChangeInfo['imagesId__']) && $ChangeInfo['imagesId__'] == 1) || (isset($ChangeInfo['colors__']) && $ChangeInfo['colors__'] == 1)){
            if ($request->imagesId) {
                for ($i = 0; $i < sizeof($request->imagesId); $i++) {
                    $image = ItemImages::where('id', $request->imagesId[$i])->first();

                    if ($image->item_id == null || $image->list_image_path == null) {
                        $filename = Uuid::generate()->string;
                        $ext = pathinfo($image->image_path, PATHINFO_EXTENSION);

                        $listSavePath = 'images/vendors/'.$item->vendor->id.'/list/' . $filename . '.' . $ext;
                        $originalSavePath = 'images/vendors/'.$item->vendor->id.'/original/' . $filename . '.' . $ext;
                        $thumbsSavePath = 'images/vendors/'.$item->vendor->id.'/thumbs/' . $filename . '.' . $ext;

                        // List Image
                        $img = Image::make(public_path($image->image_path))->resize(200, 300);
                        $img->save(public_path($listSavePath), 100);

                        // Thumbs Image
                        $thumb = Image::make(public_path($image->image_path))->resize(150, 150);
                        $thumb->save(public_path($thumbsSavePath), 100);

                        File::move(public_path($image->image_path), public_path($originalSavePath));

                        $image->image_path = $originalSavePath;
                        $image->list_image_path = $listSavePath;
                        $image->thumbs_image_path = $thumbsSavePath;
                    }

                    $image->item_id = $item->id;
                    $image->color_id = $request->imageColor[$i];
                    $image->sort = $i + 1;
                    $image->save();
                }
                // Delete Images
                $images = ItemImages::where('item_id', $item->id)->whereNotIn('id', $request->imagesId)->get();
                foreach ($images as $image) {
                    if ($image->image_path != null)
                        File::delete(public_path($image->image_path));

                    if ($image->list_image_path != null)
                        File::delete(public_path($image->list_image_path));

                    if ($image->thumbs_image_path != null)
                        File::delete(public_path($image->thumbs_image_path));

                    $image->delete();
                }
            } else {
                // Delete Images
                $images = ItemImages::where('item_id', $item->id)->get();
                foreach ($images as $image) {
                    if ($image->image_path != null)
                        File::delete(public_path($image->image_path));

                    if ($image->list_image_path != null)
                        File::delete(public_path($image->list_image_path));

                    if ($image->thumbs_image_path != null)
                        File::delete(public_path($image->thumbs_image_path));

                    $image->delete();
                }
            }
        }

        return redirect()->back()->with('message', 'Item Updated!');
    }

    public function itemListAll(Request $request) {
        // Active Items
        $activeItemsQuery = Item::query();
        $activeItemsQuery->where('status', 1)
            ->where('vendor_meta_id', Auth::user()->vendor_meta_id)
            ->with('categories', 'images');

        // Search
        if (isset($request->text) && $request->text != '') {
            if (isset($request->style) && $request->style == '1')
                $activeItemsQuery->where('style_no', 'like', '%' . $request->text . '%');

            if (isset($request->vendorStyle) && $request->vendorStyle == '1')
                $activeItemsQuery->where('vendor_style_no', 'like', '%' . $request->text . '%');

            if (isset($request->des) && $request->des == '1')
                $activeItemsQuery->where('description', 'like', '%' . $request->text . '%');

            if (isset($request->name) && $request->name == '1')
                $activeItemsQuery->where('name', 'like', '%' . $request->text . '%');
        }

        // Active Items Order
        if (isset($request->s1) && $request->s1 != '') {
            if ($request->s1 == '4')
                $activeItemsQuery->orderBy('price');
            else if ($request->s1 == '1')
                $activeItemsQuery->orderBy('updated_at', 'desc');
            else if ($request->s1 == '2')
                $activeItemsQuery->orderBy('created_at', 'desc');
            else if ($request->s1 == '3')
                $activeItemsQuery->orderBy('activated_at', 'desc');
            else if ($request->s1 == '5')
                $activeItemsQuery->orderBy('price', 'desc');
            else if ($request->s1 == '6')
                $activeItemsQuery->orderBy('style_no');
        } else {
            $activeItemsQuery->orderBy('activated_at', 'desc');
        }

        $activeItems = $activeItemsQuery->paginate(40, ['*'], 'p1');

        // Inactive Items
        $inactiveItemsQuery = Item::query();
        $inactiveItemsQuery->where('status', 0)
            ->where('vendor_meta_id', Auth::user()->vendor_meta_id)
            ->with('categories', 'images');

        // Search
        if (isset($request->text) && $request->text != '') {
            if (isset($request->style) && $request->style == '1')
                $inactiveItemsQuery->where('style_no', 'like', '%' . $request->text . '%');

            if (isset($request->vendorStyle) && $request->vendorStyle == '1')
                $inactiveItemsQuery->where('vendor_style_no', 'like', '%' . $request->text . '%');

            if (isset($request->des) && $request->des == '1')
                $inactiveItemsQuery->where('description', 'like', '%' . $request->text . '%');

            if (isset($request->name) && $request->name == '1')
                $inactiveItemsQuery->where('name', 'like', '%' . $request->text . '%');
        }

        // Inactive order
        if (isset($request->s2) && $request->s2 != '') {
            if ($request->s2 == '4')
                $inactiveItemsQuery->orderBy('price');
            else if ($request->s2 == '1')
                $inactiveItemsQuery->orderBy('updated_at', 'desc');
            else if ($request->s2 == '2')
                $inactiveItemsQuery->orderBy('created_at', 'desc');
            else if ($request->s2 == '3') {
                $inactiveItemsQuery->orderBy('activated_at', 'desc');
                $inactiveItemsQuery->orderBy('created_at', 'desc');
            } else if ($request->s2 == '5')
                $inactiveItemsQuery->orderBy('price', 'desc');
            else if ($request->s2 == '6')
                $inactiveItemsQuery->orderBy('style_no');
        } else {
            $inactiveItemsQuery->orderBy('activated_at', 'desc');
            $inactiveItemsQuery->orderBy('created_at', 'desc');
        }

        $inactiveItems = $inactiveItemsQuery->paginate(40, ['*'], 'p2');

        $appends = [
            'p1' => $activeItems->currentPage(),
            'p2' => $inactiveItems->currentPage(),
        ];

        foreach ($request->all() as $key => $value) {
            if ($key != 'p1' && $key != 'p2')
                $appends[$key] = ($value == null) ? '' : $value;
        }

        $categories = Category::where('parent', 0)->where('vendor_meta_id', Auth::user()->vendor_meta_id)
            ->where('status', 1)->orderBy('sort')->orderBy('name')->with('subCategories')->get();

        return view('vendor.dashboard.item_list.index', compact( 'activeItems', 'inactiveItems', 'categories', 'appends'))
            ->with('page_title', 'Edit All Items');
    }

    public function cloneItem(Item $item) {
        $item->load('colors', 'images');
        $categories = Category::where('vendor_meta_id', Auth::user()->vendor_meta_id)
            ->where('status', 1)->where('parent', 0)
            ->with('subCategories')->orderBy('name')->get();

        $packs = Pack::where('vendor_meta_id', Auth::user()->vendor_meta_id)->where('status', 1)->orderBy('name')->get();
        $fabrics = Fabric::where('vendor_meta_id', Auth::user()->vendor_meta_id)->where('status', 1)->orderBy('name')->get();
        $madeInCountries = MadeInCountry::where('vendor_meta_id', Auth::user()->vendor_meta_id)->where('status', 1)->orderBy('name')->get();
        $bodySizes = BodySize::orderBy('name')->get();
        $patterns = Pattern::orderBy('name')->get();
        $lengths = Length::orderBy('name')->get();
        $styles = Style::orderBy('name')->get();
        $colors = Color::where('vendor_meta_id', Auth::user()->vendor_meta_id)->where('status', 1)->orderBy('name')->get();

        // Images color id
        $imagesColorIds = [];
        foreach($item->images as $img)
            $imagesColorIds[] = $img->color_id;

        // Master Fabric
        /*$masterFabricsIds = [];

        foreach($fabrics as $fabric)
            $masterFabricsIds[] = $fabric->master_fabric_id;

        $masterFabricsIds = array_unique($masterFabricsIds);*/

        $masterFabrics = MasterFabric::orderBy('name')->get();

        // Default Categories
        $defaultCategories = [];

        $categoriesCollection = DefaultCategory::orderBy('sort')->orderBy('name')->get();

        foreach($categoriesCollection as $cc) {
            if ($cc->parent == 0) {
                $data = [
                    'id' => $cc->id,
                    'name' => $cc->name
                ];

                $subCategories = [];
                foreach($categoriesCollection as $cat) {
                    if ($cat->parent == $cc->id) {
                        $data2 = [
                            'id' => $cat->id,
                            'name' => $cat->name
                        ];

                        $data3 = [];
                        foreach($categoriesCollection as $item2) {
                            if ($item2->parent == $cat->id) {
                                $data3[] = [
                                    'id' => $item2->id,
                                    'name' => $item2->name
                                ];
                            }
                        }

                        $data2['subCategories'] = $data3;
                        $subCategories[] = $data2;
                    }
                }

                $data['subCategories'] = $subCategories;
                $defaultCategories[] = $data;
            }
        }

        // Master Color
        $masterColors = MasterColor::orderBy('name')->get();

        return view('vendor.dashboard.item_list.clone_item', compact('categories', 'packs', 'fabrics', 'madeInCountries',
            'defaultCategories', 'bodySizes', 'patterns', 'lengths', 'styles', 'colors', 'masterFabrics', 'item', 'imagesColorIds', 'masterColors'))
            ->with('page_title', 'Create a New Item');
    }

    public function cloneItemPost(Item $old_item, Request $request) {
        $request->validate([
            'style_no' => 'required|max: 255',
            'category' => 'required',
            'price' => 'required|numeric',
            'orig_price' => 'nullable|numeric',
            'size' => 'required',
            'sorting' => 'nullable|integer',
            'description' => 'nullable|max:500',
            'd_parent_category' => 'required',
            'keywords' => 'nullable|max:200',
            'min_qty' => 'nullable|integer',
            'memo' => 'nullable|max:255',
            'vendor_style_no' => 'nullable|max:255',
        ]);

        $availableDate = null;
        if ($request->available_on) {
            $availableOn = DateTime::createFromFormat('m/d/Y', $request->available_on);
            $availableDate = $availableOn->format('Y-m-d');
        }

        $exclusive = 0;
        $evenColor =0;

        if ($request->exclusive)
            $exclusive = 1;

        if ($request->even_color)
            $evenColor = 1;

        $item = Item::create([
            'vendor_meta_id' => Auth::user()->vendor_meta_id,
            'status' => $request->status,
            'style_no' => $request->style_no,
            'price' => $request->price,
            'orig_price' => $request->orig_price,
            'pack_id' => $request->size,
            'sorting' => $request->sorting,
            'description' => $request->description,
            'available_on' => $availableDate,
            'availability' => $request->availability,
            'name' => $request->item_name,
            'default_parent_category' => $request->d_parent_category,
            'default_second_category' => $request->d_second_parent_category,
            'default_third_category' => $request->d_third_parent_category,
            'exclusive' => $exclusive,
            'min_qty' => $request->min_qty,
            'even_color' => $evenColor,
            'fabric' => $request->fabric,
            'made_in_id' => $request->made_n,
            'labeled' => $request->labeled,
            'keywords' => $request->keywords,
            'body_size_id' => $request->body_size,
            'pattern_id' => $request->pattern,
            'length_id' => $request->length,
            'style_id' => $request->style,
            'master_fabric_id' => $request->master_fabric,
            'memo' => $request->memo,
            'vendor_style_no' => $request->vendor_style_no,
            'activated_at' => ($request->status == '1' ? Carbon::now()->toDateTimeString() : null)
        ]);

        $item->colors()->attach($request->colors);
        $item->categories()->attach($request->category);

        if ($request->imagesId) {
            for ($i = 0; $i < sizeof($request->imagesId); $i++) {
                $tmp = ItemImages::where('id', $request->imagesId[$i])->first();

                if (is_null($tmp->item_id)) {
                    $filename = Uuid::generate()->string;
                    $ext = pathinfo($tmp->image_path, PATHINFO_EXTENSION);

                    $listSavePath = 'images/vendors/'.Auth::user()->vendor_meta_id.'/list/' . $filename . '.' . $ext;
                    $originalSavePath = 'images/vendors/'.Auth::user()->vendor_meta_id.'/original/' . $filename . '.' . $ext;
                    $thumbsSavePath = 'images/vendors/'.Auth::user()->vendor_meta_id.'/thumbs/' . $filename . '.' . $ext;

                    // List Image
                    $img = Image::make(public_path($tmp->image_path))->resize(200, 300);
                    $img->save(public_path($listSavePath), 100);

                    // Thumbs Image
                    $thumb = Image::make(public_path($tmp->image_path))->resize(150, 150);
                    $thumb->save(public_path($thumbsSavePath), 100);

                    File::move(public_path($tmp->image_path), public_path($originalSavePath));

                    $tmp->item_id = $item->id;
                    $tmp->color_id = $request->imageColor[$i];
                    $tmp->sort = $i + 1;
                    $tmp->image_path = $originalSavePath;
                    $tmp->list_image_path = $listSavePath;
                    $tmp->thumbs_image_path = $thumbsSavePath;
                    $tmp->save();

                    /*$tmp->item_id = $item->id;
                    $tmp->color_id = $request->imageColor[$i];
                    $tmp->sort = $i + 1;
                    $tmp->save();*/
                } else {
                    $filename = Uuid::generate()->string;
                    $ext = pathinfo($tmp->image_path, PATHINFO_EXTENSION);

                    $listSavePath = 'images/vendors/'.Auth::user()->vendor_meta_id.'/list/' . $filename . '.' . $ext;
                    $originalSavePath = 'images/vendors/'.Auth::user()->vendor_meta_id.'/original/' . $filename . '.' . $ext;
                    $thumbsSavePath = 'images/vendors/'.Auth::user()->vendor_meta_id.'/thumbs/' . $filename . '.' . $ext;

                    // List Image
                    $img = Image::make(public_path($tmp->image_path))->resize(200, 300);
                    $img->save(public_path($listSavePath), 100);

                    // Thumbs Image
                    $thumb = Image::make(public_path($tmp->image_path))->resize(150, 150);
                    $thumb->save(public_path($thumbsSavePath), 100);

                    File::copy(public_path($tmp->image_path), public_path($originalSavePath));

                    ItemImages::create([
                        'item_id' => $item->id,
                        'color_id' => $request->imageColor[$i],
                        'sort' => $i+1,
                        'image_path' => $originalSavePath,
                        'list_image_path' => $listSavePath,
                        'thumbs_image_path' => $thumbsSavePath,
                    ]);
                }

            }
        }

        return redirect()->route('vendor_item_list_all')->with('message', 'Item Added!');
    }

    public function dataImportView() {
        return view('vendor.dashboard.data_import')->with('page_title', 'Data Import');
    }

    public function dataImportReadFile(Request $request) {
        $file = $request->file('file');
        $ext = $file->getClientOriginalExtension();

        if (in_array($ext, ['xlsx', 'csv', 'xls', 'xlsx'])) {
            $excel = Excel::load($file->getRealPath(), function ($reader) {
                $content = $reader->get();
            });

            $data = $excel->get()->toArray();



            if (sizeof($data) == 0)
                return redirect()->back()->with('error', 'Invalid file');

            $item = $data[0];
            $items = [];

            if (sizeof($item) >= 2 && sizeof($item) <= 3 && array_key_exists("styleno", $item) && array_key_exists("images", $item)) {
                foreach ($data as $item)
                    $items [] = $item;

                return view('vendor.dashboard.image_import_stats', compact('items'));
            }

            if (!array_key_exists("styleno", $item) ||
                !array_key_exists("vendorstyleno", $item) ||
                !array_key_exists("itemname", $item) ||
                !array_key_exists("vendorcategory", $item) ||
                !array_key_exists("defaultcategory", $item) ||
                !array_key_exists("size", $item) ||
                !array_key_exists("pack", $item) ||
                !array_key_exists("packqty", $item) ||
                !array_key_exists("unitprice", $item) ||
                !array_key_exists("originalprice", $item) ||
                !array_key_exists("availableon", $item) ||
                !array_key_exists("productdescription", $item) ||
                !array_key_exists("bodysize", $item) ||
                !array_key_exists("madein", $item) ||
                !array_key_exists("pattern", $item) ||
                !array_key_exists("length", $item) ||
                !array_key_exists("style", $item) ||
                !array_key_exists("fabric", $item) ||
                !array_key_exists("searchkeyword", $item) ||
                !array_key_exists("inhousememo", $item) ||
                !array_key_exists("color", $item)) {

                return redirect()->back()->with('error', 'Invalid file');
            }

            foreach ($data as &$item) {
                if ($item['styleno'] != null || $item['styleno'] != '') {
                    // Available On
                    $date = '';

                    if ($item['availableon'] != null || $item['availableon'] != '') {
                        try {
                            $date = $item['availableon']->format('Y-m-d');
                        } catch (\Throwable $e) {

                        }

                        $item['availableon'] = $date;
                    }

                    $items[] = $item;
                }
            }

            return view('vendor.dashboard.data_import_stats', compact('items'));
        } else {
            return redirect()->back()->with('error', 'Invalid file');
        }
    }

    public function dataImportUpload(Request $request) {
        // Style no check
        $found = false;

        $item = Item::where('vendor_meta_id', Auth::user()->vendor_meta_id)
            ->where('style_no', $request->styleno)
            ->first();

        if ($item) {
            $found = true;
            //return response()->json(['success' => false, 'message' => 'Style No. already taken.']);
        }

        // Vendor Category
        if ($request->vendorcategory != null && $request->vendorcategory == '')
            return response()->json(['success' => false, 'message' => 'Vendor category required.']);

        $vendorCategories = explode(',', $request->vendorcategory);

        $vendorCategory = Category::where('vendor_meta_id', Auth::user()->vendor_meta_id)
            ->where('status', 1)
            ->where('name', $vendorCategories[0])
            ->first();

        if (!$vendorCategory)
            return response()->json(['success' => false, 'message' => 'Vendor category not found.']);

        if (sizeof($vendorCategories) > 1) {
            $vendorCategory = Category::where('vendor_meta_id', Auth::user()->vendor_meta_id)
                ->where('status', 1)
                ->where('name', $vendorCategories[1])
                ->first();

            if (!$vendorCategory)
                return response()->json(['success' => false, 'message' => 'Vendor sub category not found.']);
        }

        // Default Category Check
        if ($request->defaultcategory != null && $request->defaultcategory == '')
            return response()->json(['success' => false, 'message' => 'Default category required.']);

        $dc = explode(',', $request->defaultcategory);
        $defaultCategory = DefaultCategory::where('name', $dc[0])
            ->where('parent', 0)
            ->first();

        if (!$defaultCategory)
            return response()->json(['success' => false, 'message' => 'Default category not found.']);

        // Second default category check
        $defaultCategorySecondId = null;
        if (sizeof($dc) > 1) {
            $defaultCategorySecond = DefaultCategory::where('name', $dc[1])
                ->where('parent', $defaultCategory->id)
                ->first();

            if (!$defaultCategorySecond)
                return response()->json(['success' => false, 'message' => 'Invalid Default Sub category.']);
            else
                $defaultCategorySecondId = $defaultCategorySecond->id;
        }

        // Third default category check
        $defaultCategoryThirdId = null;
        if (sizeof($dc) > 2) {
            $defaultCategoryThird = DefaultCategory::where('name', $dc[2])
                ->where('parent', $defaultCategorySecond->id)
                ->first();

            if (!$defaultCategoryThird)
                return response()->json(['success' => false, 'message' => 'Invalid Default Sub category.']);
            else
                $defaultCategoryThirdId = $defaultCategoryThird->id;
        }

        // Size Check
        if ($request->size != null && $request->size == '')
            return response()->json(['success' => false, 'message' => 'Size is required.']);

        $pack = Pack::where('vendor_meta_id', Auth::user()->vendor_meta_id)
            ->where('status', 1)
            ->where('name', $request->size)
            ->first();

        if (!$pack)
            return response()->json(['success' => false, 'message' => 'Size not found.']);

        // Fabric
        /*$fabricId = null;

        if ($request->fabric != null && $request->fabric != '') {
            $fabric = Fabric::where('vendor_meta_id', Auth::user()->vendor_meta_id)
                ->where('status', 1)
                ->where('name', $request->fabric)
                ->first();

            if ($fabric)
                $fabricId = $fabric->id;
        }*/

        // Made In Country
        $madeInId = null;

        if ($request->madein != null && $request->madein != '') {
            $madeIn = MadeInCountry::where('vendor_meta_id', Auth::user()->vendor_meta_id)
                ->where('status', 1)
                ->where('name', $request->madein)
                ->first();

            if ($madeIn)
                $madeInId = $madeIn->id;
        }

        // Body Size
        $bodySizeId = null;

        if ($request->bodysize != null && $request->bodysize != '') {
            $bodySize = BodySize::where('name', $request->bodysize)
                ->where('parent_category_id', $defaultCategory->id)->first();

            if ($bodySize)
                $bodySizeId = $bodySize->id;
        }

        // Pattern
        $patternId = null;

        if ($request->pattern != null && $request->pattern != '') {
            $pattern = Pattern::where('name', $request->pattern)
                ->where('parent_category_id', $defaultCategory->id)->first();

            if ($pattern)
                $patternId = $pattern->id;
        }

        // Length
        $lengthId = null;

        if ($request->length != null && $request->length != '' && $defaultCategorySecondId != null) {
            $length = Length::where('name', $request->length)
                ->where('sub_category_id', $defaultCategorySecondId)->first();

            if ($length)
                $lengthId = $length->id;
        }

        // Style
        $styleId = null;

        if ($request->style != null && $request->style != '') {
            $style = Style::where('name', $request->style)
                ->where('sub_category_id', $defaultCategorySecondId)->first();

            if ($style)
                $styleId = $style->id;
        }

        // Availability
        $availability = Availability::$IN_STOCK;

        if ($request->availableon != null) {
            if(time() < strtotime($request->availableon)) {
                $availability = Availability::$ARRIVES_SOON;
            }
        }

        // Colors check
        if ($request->color != null && $request->color == '')
            return response()->json(['success' => false, 'message' => 'Color is required.']);

        $colorIds = [];
        $colors = explode(',', $request->color);

        foreach ($colors as $color) {
            $c = Color::where('status', 1)
                ->where('vendor_meta_id', Auth::user()->vendor_meta_id)
                ->where('name', $color)
                ->first();

            if (!$c) {
                $c = Color::create([
                    'name' => $color,
                    'status' => 1,
                    'vendor_meta_id' => Auth::user()->vendor_meta_id
                ]);
            }

            $colorIds[] = $c->id;
        }

        if (sizeof($colorIds) == 0)
            return response()->json(['success' => false, 'message' => 'Color(s) not found.']);

        // Create Item
        if ($found) {
            $item->price = $request->unitprice;
            $item->orig_price = $request->originalprice;
            $item->pack_id = $pack->id;
            $item->description = $request->productdescription;
            $item->available_on = $request->availableon;
            $item->availability = $availability;
            $item->name = $request->itemname;
            $item->default_parent_category = $defaultCategory->id;
            $item->default_second_category = $defaultCategorySecondId;
            $item->default_third_category = $defaultCategoryThirdId;
            $item->min_qty = $request->packqty;
            $item->fabric = $request->fabric;
            $item->made_in_id = $madeInId;
            $item->keywords = $request->searchkeyword;
            $item->body_size_id = $bodySizeId;
            $item->pattern_id = $patternId;
            $item->length_id = $lengthId;
            $item->style_id = $styleId;
            $item->memo = $request->inhousememo;
            $item->vendor_style_no = $request->vendorstyleno;

            $item->save();
            $item->touch();

            $item->colors()->detach();
            $item->categories()->detach();
            $item->images()->delete();
        } else {
            $item = Item::create([
                'vendor_meta_id' => Auth::user()->vendor_meta_id,
                'status' => 0,
                'style_no' => $request->styleno,
                'price' => $request->unitprice,
                'orig_price' => $request->originalprice,
                'pack_id' => $pack->id,
                'description' => $request->productdescription,
                'available_on' => $request->availableon,
                'availability' => $availability,
                'name' => $request->itemname,
                'default_parent_category' => $defaultCategory->id,
                'default_second_category' => $defaultCategorySecondId,
                'default_third_category' => $defaultCategoryThirdId,
                'min_qty' => $request->packqty,
                'fabric' => $request->fabric,
                'made_in_id' => $madeInId,
                'keywords' => $request->searchkeyword,
                'body_size_id' => $bodySizeId,
                'pattern_id' => $patternId,
                'length_id' => $lengthId,
                'style_id' => $styleId,
                'memo' => $request->inhousememo,
                'vendor_style_no' => $request->vendorstyleno,
            ]);
        }

        $item->colors()->attach($colorIds);
        $item->categories()->attach([$vendorCategory->id]);

        // Images
        if ($request->images != '') {
            $urls = explode(',', $request->images);

            $sort = 1;
            foreach ($urls as $url) {
                $filename = Uuid::generate()->string;
                $ext = pathinfo($url, PATHINFO_EXTENSION);


                $listSavePath = 'images/vendors/'.Auth::user()->vendor_meta_id.'/list/' . $filename . '.' . $ext;
                $originalSavePath = 'images/vendors/'.Auth::user()->vendor_meta_id.'/original/' . $filename . '.' . $ext;
                $thumbsSavePath = 'images/vendors/'.Auth::user()->vendor_meta_id.'/thumbs/' . $filename . '.' . $ext;

                // List Image
                $img = Image::make($url)->resize(200, 300);
                $img->save(public_path($listSavePath), 100);

                // Thumbs Image
                $thumb = Image::make($url)->resize(150, 150);
                $thumb->save(public_path($thumbsSavePath), 100);

                File::copy($url, public_path($originalSavePath));
                //File::copy($url, public_path('images/item/' . $filename . '.' . $ext));

                ItemImages::create([
                    'item_id' => $item->id,
                    'sort' => $sort,
                    'image_path' => $originalSavePath,
                    'list_image_path' => $listSavePath,
                    'thumbs_image_path' => $thumbsSavePath,
                ]);

                $sort++;
            }
        }

        return response()->json(['success' => true, 'message' => 'Completed']);
    }

    public function dataImportImage(Request $request) {
        $item = Item::where('vendor_meta_id', Auth::user()->vendor_meta_id)
            ->where('style_no', $request->styleno)
            ->first();

        if (!$item) {
            return response()->json(['success' => false, 'message' => 'Style No. not found.']);
        }

        $item->images()->delete();

        // Images
        if ($request->images != '') {
            $urls = explode(',', $request->images);

            $sort = 1;
            foreach ($urls as $url) {
                $filename = Uuid::generate()->string;
                $ext = pathinfo($url, PATHINFO_EXTENSION);


                $listSavePath = 'images/vendors/'.Auth::user()->vendor_meta_id.'/list/' . $filename . '.' . $ext;
                $originalSavePath = 'images/vendors/'.Auth::user()->vendor_meta_id.'/original/' . $filename . '.' . $ext;
                $thumbsSavePath = 'images/vendors/'.Auth::user()->vendor_meta_id.'/thumbs/' . $filename . '.' . $ext;

                // List Image
                $img = Image::make($url)->resize(200, 300);
                $img->save(public_path($listSavePath), 100);

                // Thumbs Image
                $thumb = Image::make($url)->resize(150, 150);
                $thumb->save(public_path($thumbsSavePath), 100);

                File::copy($url, public_path($originalSavePath));
                //File::copy($url, public_path('images/item/' . $filename . '.' . $ext));

                ItemImages::create([
                    'item_id' => $item->id,
                    'sort' => $sort,
                    'image_path' => $originalSavePath,
                    'list_image_path' => $listSavePath,
                    'thumbs_image_path' => $thumbsSavePath,
                ]);

                $sort++;
            }
        }

        return response()->json(['success' => true, 'message' => 'Completed']);
    }

    public function itemCategorySave(Request $request) {
        $categoriesIds = Category::whereIn('id', $request->categories)->where('vendor_meta_id', Auth::user()->vendor_meta_id)
            ->pluck('id')->toArray();

        if (sizeof($categoriesIds) > 0) {
            foreach ($request->ids as $id) {
                $item = Item::where('id', $id)->where('vendor_meta_id', Auth::user()->vendor_meta_id)->first();
                $item->categories()->detach();
                $item->categories()->attach($categoriesIds);
            }
        }
    }
}
