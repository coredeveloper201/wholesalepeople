<?php

namespace App\Http\Controllers\Vendor;

use App\Enumeration\OrderStatus;
use App\Enumeration\Role;
use App\Model\BlockUser;
use App\Model\Order;
use App\Model\User;
use App\Model\Visitor;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;

class CustomerController extends Controller
{
    public function allCustomer() {
        /*$visitors = Visitor::where('url', route('vendor_or_parent_category', ['text' => changeSpecialChar(Auth::user()->vendor->company_name)]))
            ->where('user_id', '!=', NULL)
            ->orderBy('created_at', 'desc')
            ->select('user_id')
            ->distinct()
            ->get();*/

        $visitors = Visitor::select(DB::raw('user_id, MAX(created_at) d'))
            ->where('url', route('vendor_or_parent_category', ['text' => changeSpecialChar(Auth::user()->vendor->company_name)]))
            ->where('user_id', '!=', NULL)
            ->groupBy('user_id')
            ->orderBy('d', 'desc')
            ->get();

        $sql = "SELECT user_id, COUNT('user_id') count
                    FROM orders
                    WHERE status != ".OrderStatus::$INIT." AND vendor_meta_id = ".Auth::user()->vendor_meta_id."
                    GROUP BY user_id";
        $orders = DB::select($sql);

        $ids = [];
        foreach ($visitors as $visitor)
            $ids[] = $visitor->user_id;

        $ordersCount = [];
        foreach ($orders as $order) {
            $ordersCount[$order->user_id] = $order->count;
            //$ids[] = $order->user_id;
        }

        //$ids = array_unique($ids);
        $users = [];

        if (sizeof($ids) > 0) {
            $users = User::whereIn('id', $ids)
                ->orderByRaw(\DB::raw("FIELD(id, " . implode(',', $ids) . " )"))
                ->with('buyer')
                ->paginate(10);
        }

        // Visit count
        foreach ($users as &$user) {
            $count = Visitor::where('user_id', $user->id)
                ->where('url', route('vendor_or_parent_category', ['text' => changeSpecialChar(Auth::user()->vendor->company_name)]))
                ->count();

            $user->visitCount= $count;
        }

        // Blocked user
        $blockedIds = [];
        $blockedUsers = BlockUser::where('vendor_meta_id', Auth::user()->vendor_meta_id)->get();

        foreach ($blockedUsers as $blockedUser)
            $blockedIds[] = $blockedUser->user_id;

        return view('vendor.dashboard.customers.all', compact('users', 'ordersCount', 'blockedIds'))->with('page_title', 'All Customers');
    }

    public function blockCustomer(Request $request) {
        if ($request->status == 1) {
            $block = BlockUser::where('vendor_meta_id', Auth::user()->vendor_meta_id)
                ->where('user_id', $request->id)
                ->first();

            if (!$block) {
                BlockUser::create([
                    'vendor_meta_id' => Auth::user()->vendor_meta_id,
                    'user_id' => $request->id,
                ]);
            }
        } else {
            BlockUser::where('vendor_meta_id', Auth::user()->vendor_meta_id)
                ->where('user_id', $request->id)
                ->delete();
        }
    }

    /*public function blockCustomer() {
        $users= BlockUser::where('vendor_meta_id', Auth::user()->vendor_meta_id)->with('user')->paginate(10);

        return view('vendor.dashboard.customers.block', compact('users'))->with('page_title', 'Block Customers');
    }

    public function addBlockCustomer(Request $request) {
        $user = User::where('email', $request->email)
            ->where('role', Role::$BUYER)
            ->first();

        if (!$user)
            return response()->json(['success' => false, 'message' => 'Customer not found.']);

        $blockUser = BlockUser::where('user_id', $user->id)
            ->where('vendor_meta_id', Auth::user()->vendor_meta_id)
            ->first();

        if ($blockUser)
            return response()->json(['success' => false, 'message' => 'This customer already added!']);

        BlockUser::create([
            'user_id' => $user->id,
            'vendor_meta_id' => Auth::user()->vendor_meta_id
        ]);

        return response()->json(['success' => true, 'message' => 'Successfully added!']);
    }

    public function deleteBlockCustomer(Request $request) {
        BlockUser::where('id', $request->id)
            ->where('vendor_meta_id', Auth::user()->vendor_meta_id)->delete();
    }*/
}
