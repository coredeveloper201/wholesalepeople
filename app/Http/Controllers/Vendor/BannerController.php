<?php

namespace App\Http\Controllers\Vendor;

use App\Enumeration\SliderType;
use App\Enumeration\VendorImageType;
use App\Model\Category;
use App\Model\Item;
use App\Model\SliderItem;
use App\Model\VendorImage;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Auth;
use Validator;
use Uuid;
use File;

class BannerController extends Controller
{
    public function index(Request $request) {
        $parameters = [];
        $appends = array();

        if ($request->type){
            $parameters [] = array('type', '=', $request->type);
            $appends['type'] = $request->type;
        }

        if ($request->status){
            $status = ($request->status == '2') ? 0 : 1;
            $parameters [] = array('status', '=', $status);
            $appends['status'] = $request->status;
        }

        $images = VendorImage::where('vendor_meta_id', Auth::user()->vendor_meta_id)->where($parameters)->paginate(10);

        return view('vendor.dashboard.marketing_tools.banner.index', compact('images', 'appends'))->with('page_title', 'Banner Manager');
    }

    public function addPost(Request $request) {
        $request->validate([
            'logo' => 'nullable|mimes:jpeg,jpg,png|dimensions:width=230,height=54',
            'homepage_banner' => 'nullable|mimes:jpeg,jpg,png|dimensions:width=910,height=326',
            'small_ad_banner' => 'nullable|mimes:jpeg,jpg,png|dimensions:width=364,height=364',
            'mobile_main_banner' => 'nullable|mimes:jpeg,jpg,png|dimensions:width=593,height=400',
            'bidding_big_banner' => 'nullable|mimes:jpeg,jpg,png|dimensions:width=1400,height=400',
            'bidding_small_banner' => 'nullable|mimes:jpeg,jpg,png|dimensions:width=376,height=270',
            'top_banner' => 'nullable|mimes:jpeg,jpg,png|dimensions:width=1200,height=250',
        ]);

        if ($request->logo) {
            $file = $request->file('logo');
            $this->uploadFile($file, VendorImageType::$LOGO);
        }

        if ($request->homepage_banner) {
            $file = $request->file('homepage_banner');
            $this->uploadFile($file, VendorImageType::$HOME_PAGE_BANNER);
        }

        if ($request->small_ad_banner) {
            $file = $request->file('small_ad_banner');
            $this->uploadFile($file, VendorImageType::$SMALL_AD_BANNER);
        }

        if ($request->mobile_main_banner) {
            $file = $request->file('mobile_main_banner');
            $this->uploadFile($file, VendorImageType::$MOBILE_MAIN_BANNER);
        }

        if ($request->bidding_big_banner) {
            $file = $request->file('bidding_big_banner');
            $this->uploadFile($file, VendorImageType::$BIDDING_BIG_BANNER);
        }

        if ($request->bidding_small_banner) {
            $file = $request->file('bidding_small_banner');
            $this->uploadFile($file, VendorImageType::$BIDDING_SMALL_BANNER);
        }

        if ($request->top_banner) {
            $file = $request->file('top_banner');
            $this->uploadFile($file, VendorImageType::$TOP_BANNER);
        }

        return redirect()->back();
    }

    public function delete(Request $request) {
        $image = VendorImage::where('id', $request->id)->first();
        File::delete(public_path($image->image_path));
        $image->delete();
    }

    public function active(Request $request) {
        VendorImage::where('type', $request->type)
            ->where('vendor_meta_id', Auth::user()->vendor_meta_id)->update(['status' => 0]);
        VendorImage::where('id', $request->id)->update(['status' => 1]);
    }

    public function uploadFile($file, $type) {
        $filename = Uuid::generate()->string;
        $ext = $file->getClientOriginalExtension();

        $destinationPath = '/images/vendor';
        $file->move(public_path($destinationPath), $filename.".".$ext);
        $imagePath = $destinationPath."/".$filename.".".$ext;

        VendorImage::create([
            'vendor_meta_id' => Auth::user()->vendor_meta_id,
            'type' => $type,
            'image_path' => $imagePath,
            'status' => 0
        ]);
    }

    public function bannerItems() {
        $items = Item::where('status', 1)
            ->where('vendor_meta_id', Auth::user()->vendor_meta_id)
            ->orderBy('activated_at', 'desc')
            ->paginate(21);

        $categories = Category::where('vendor_meta_id', Auth::user()->vendor_meta_id)
            ->orderBy('sort')
            ->get();

        $mainSliderItems = SliderItem::where('vendor_meta_id', Auth::user()->vendor_meta_id)
            ->where('type', SliderType::$MAIN_SLIDER)
            ->orderBy('sort')
            ->with('item')
            ->get();

        $categoryTopSliderItems = SliderItem::where('vendor_meta_id', Auth::user()->vendor_meta_id)
            ->where('type', SliderType::$CATEGORY_TOP_SLIDER)
            ->orderBy('sort')
            ->with('item')
            ->get();

        $categorySecondSliderItems = SliderItem::where('vendor_meta_id', Auth::user()->vendor_meta_id)
            ->where('type', SliderType::$CATEGORY_SECOND_SLIDER)
            ->orderBy('sort')
            ->with('item')
            ->get();

        $newTopSliderItems = SliderItem::where('vendor_meta_id', Auth::user()->vendor_meta_id)
            ->where('type', SliderType::$NEW_ARRIVAL_TOP_SLIDER)
            ->orderBy('sort')
            ->with('item')
            ->get();

        $newSecondSliderItems = SliderItem::where('vendor_meta_id', Auth::user()->vendor_meta_id)
            ->where('type', SliderType::$NEW_ARRIVAL_SECOND_SLIDER)
            ->orderBy('sort')
            ->with('item')
            ->get();

        return view('vendor.dashboard.marketing_tools.banner_items.index',
            compact('items', 'mainSliderItems', 'categoryTopSliderItems', 'categorySecondSliderItems', 'newTopSliderItems', 'newSecondSliderItems', 'categories'))
            ->with('page_title', 'Banner Items');
    }

    public function bannerItemAdd(Request $request) {
        $query = SliderItem::query();
        $query->where('vendor_meta_id', Auth::user()->vendor_meta_id);

        if ($request->type == SliderType::$MAIN_SLIDER) {
            $query->where('type', SliderType::$MAIN_SLIDER);
            $count = $query->count();

            if ($count >= 8)
                return response()->json(['success' => false, 'message' => 'Already added 8 items']);
            else {
                $item = SliderItem::where([
                    ['vendor_meta_id', Auth::user()->vendor_meta_id],
                    ['item_id', $request->id],
                    ['type', SliderType::$MAIN_SLIDER]
                ])->first();

                if ($item)
                    return response()->json(['success' => false, 'message' => 'Already added this item']);

                $maxSort = SliderItem::where([
                    ['vendor_meta_id', Auth::user()->vendor_meta_id],
                    ['type', SliderType::$MAIN_SLIDER]
                ])->max('sort');

                if (!$maxSort)
                    $maxSort = 0;

                SliderItem::create([
                    'vendor_meta_id' => Auth::user()->vendor_meta_id,
                    'item_id' => $request->id,
                    'sort' => (int) $maxSort + 1,
                    'type' => SliderType::$MAIN_SLIDER
                ]);

                return response()->json(['success' => true]);
            }
        }
        else if ($request->type == SliderType::$CATEGORY_TOP_SLIDER) {
            $query->where('type', SliderType::$CATEGORY_TOP_SLIDER);
            $count = $query->count();

            if ($count >= 6)
                return response()->json(['success' => false, 'message' => 'Already added 6 items']);
            else {
                $item = SliderItem::where([
                    ['vendor_meta_id', Auth::user()->vendor_meta_id],
                    ['item_id', $request->id],
                    ['type', SliderType::$CATEGORY_TOP_SLIDER]
                ])->first();

                if ($item)
                    return response()->json(['success' => false, 'message' => 'Already added this item']);

                $maxSort = SliderItem::where([
                    ['vendor_meta_id', Auth::user()->vendor_meta_id],
                    ['type', SliderType::$CATEGORY_TOP_SLIDER]
                ])->max('sort');

                if (!$maxSort)
                    $maxSort = 0;

                SliderItem::create([
                    'vendor_meta_id' => Auth::user()->vendor_meta_id,
                    'item_id' => $request->id,
                    'sort' => (int) $maxSort + 1,
                    'type' => SliderType::$CATEGORY_TOP_SLIDER
                ]);

                return response()->json(['success' => true]);
            }
        }
        else if ($request->type == SliderType::$CATEGORY_SECOND_SLIDER) {
            $query->where('type', SliderType::$CATEGORY_SECOND_SLIDER);
            $count = $query->count();

            if ($count >= 6)
                return response()->json(['success' => false, 'message' => 'Already added 6 items']);
            else {
                $item = SliderItem::where([
                    ['vendor_meta_id', Auth::user()->vendor_meta_id],
                    ['item_id', $request->id],
                    ['type', SliderType::$CATEGORY_SECOND_SLIDER]
                ])->first();

                if ($item)
                    return response()->json(['success' => false, 'message' => 'Already added this item']);

                $maxSort = SliderItem::where([
                    ['vendor_meta_id', Auth::user()->vendor_meta_id],
                    ['type', SliderType::$CATEGORY_SECOND_SLIDER]
                ])->max('sort');

                if (!$maxSort)
                    $maxSort = 0;

                SliderItem::create([
                    'vendor_meta_id' => Auth::user()->vendor_meta_id,
                    'item_id' => $request->id,
                    'sort' => (int) $maxSort + 1,
                    'type' => SliderType::$CATEGORY_SECOND_SLIDER
                ]);

                return response()->json(['success' => true]);
            }
        }
        else if ($request->type == SliderType::$NEW_ARRIVAL_TOP_SLIDER) {
            $query->where('type', SliderType::$NEW_ARRIVAL_TOP_SLIDER);
            $count = $query->count();

            if ($count >= 6)
                return response()->json(['success' => false, 'message' => 'Already added 6 items']);
            else {
                $item = SliderItem::where([
                    ['vendor_meta_id', Auth::user()->vendor_meta_id],
                    ['item_id', $request->id],
                    ['type', SliderType::$NEW_ARRIVAL_TOP_SLIDER]
                ])->first();

                if ($item)
                    return response()->json(['success' => false, 'message' => 'Already added this item']);

                $maxSort = SliderItem::where([
                    ['vendor_meta_id', Auth::user()->vendor_meta_id],
                    ['type', SliderType::$NEW_ARRIVAL_TOP_SLIDER]
                ])->max('sort');

                if (!$maxSort)
                    $maxSort = 0;

                SliderItem::create([
                    'vendor_meta_id' => Auth::user()->vendor_meta_id,
                    'item_id' => $request->id,
                    'sort' => (int) $maxSort + 1,
                    'type' => SliderType::$NEW_ARRIVAL_TOP_SLIDER
                ]);

                return response()->json(['success' => true]);
            }
        }
        else if ($request->type == SliderType::$NEW_ARRIVAL_SECOND_SLIDER) {
            $query->where('type', SliderType::$NEW_ARRIVAL_SECOND_SLIDER);
            $count = $query->count();

            if ($count >= 6)
                return response()->json(['success' => false, 'message' => 'Already added 6 items']);
            else {
                $item = SliderItem::where([
                    ['vendor_meta_id', Auth::user()->vendor_meta_id],
                    ['item_id', $request->id],
                    ['type', SliderType::$NEW_ARRIVAL_SECOND_SLIDER]
                ])->first();

                if ($item)
                    return response()->json(['success' => false, 'message' => 'Already added this item']);

                $maxSort = SliderItem::where([
                    ['vendor_meta_id', Auth::user()->vendor_meta_id],
                    ['type', SliderType::$NEW_ARRIVAL_SECOND_SLIDER]
                ])->max('sort');

                if (!$maxSort)
                    $maxSort = 0;

                SliderItem::create([
                    'vendor_meta_id' => Auth::user()->vendor_meta_id,
                    'item_id' => $request->id,
                    'sort' => (int) $maxSort + 1,
                    'type' => SliderType::$NEW_ARRIVAL_SECOND_SLIDER
                ]);

                return response()->json(['success' => true]);
            }
        }

        return response()->json(['success' => false, 'message' => '']);
    }

    public function bannerItemRemove(Request $request) {
        SliderItem::where('id', $request->id)->delete();
    }

    public function bannerItemsSort(Request $request) {
        $sort = 1;

        foreach ($request->ids as $id) {
            SliderItem::where('id', $id)->update(['sort' => $sort]);
            $sort++;
        }
    }
}
