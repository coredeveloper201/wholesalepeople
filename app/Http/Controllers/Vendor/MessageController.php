<?php

namespace App\Http\Controllers\Vendor;

use App\Enumeration\MessageRole;
use App\Enumeration\Role;
use App\Model\Message;
use App\Model\MessageFile;
use App\Model\MessageItem;
use App\Model\User;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Auth;
use Carbon\Carbon;
use Illuminate\Support\Facades\Storage;
use Uuid;
use File;

class MessageController extends Controller
{
    public function index() {
        $customers = User::where('role', Role::$BUYER)->where('active', 1)->get();

        // Inbox
        $inboxes = Message::where('receiver_type', MessageRole::$VENDOR)
            ->where('receiver_id', Auth::user()->vendor_meta_id)
            ->orderBy('updated_at', 'desc')
            ->with('items')->get();

        $inboxes2 =Message::where('sender_type', MessageRole::$VENDOR)
            ->where('sender_id', Auth::user()->vendor_meta_id)
            ->orderBy('updated_at', 'desc')
            ->with(['items' => function($query) {
                $query->where('sender', 0);
            }])->get();

        foreach ($inboxes2 as $inbox) {
            if (sizeof($inbox->items) > 0) {
                $inboxes->add($inbox);
            }
        }

        $inboxes = $inboxes->sortByDesc('updated_at');

        // Outbox
        $outboxes = [];
        $tmp = Message::where('sender_type', MessageRole::$VENDOR)
            ->where('sender_id', Auth::user()->vendor_meta_id)
            ->orderBy('updated_at', 'desc')
            ->with(['items' => function($query) {
                $query->where('sender', 0);
            }])->get();

        foreach ($tmp as $t) {
            if (sizeof($t->items) == 0) {
                $outboxes[] = $t;
            }
        }

        return view('vendor.dashboard.message.index', compact('inboxes', 'customers', 'outboxes'))->with('page_title', 'Messages');
    }

    public function chatbox($id) {
        $message = Message::where('id', $id)
            ->where(function ($query) {
                $query->where('receiver_id', Auth::user()->vendor_meta_id)
                    ->orWhere('sender_id', Auth::user()->vendor_meta_id);
            })->with('items')->first();

        if (!$message)
            abort(404);

        $isSender = $message->isSender();
        $message->timestamps = false;

        if (!$isSender) {
            $personName = $message->senderName();

            MessageItem::where('message_id', $message->id)->where('sender', 1)->update([
                'seen_at' => Carbon::now()->toDateTimeString()
            ]);

            $message->receiver_seen_at = Carbon::now()->toDateTimeString();

        } else {
            MessageItem::where('message_id', $message->id)->where('sender', 0)->update([
                'seen_at' => Carbon::now()->toDateTimeString()
            ]);

            $message->sender_seen_at = Carbon::now()->toDateTimeString();
            $personName = $message->receiverName();
        }

        $message->save();

        return view('vendor.dashboard.message.chat', compact('message', 'isSender', 'personName'))->with('page_title', 'Messages');
    }

    public function addMessage(Request $request) {
        $message = Message::where('id', $request->id)
            ->where(function ($query) {
                $query->where('receiver_id', Auth::user()->vendor_meta_id)
                    ->orWhere('sender_id', Auth::user()->vendor_meta_id);
            })->with('items')->first();

        if (!$message)
            return response()->json(['success' => false, 'message' => 'Unauthorized']);

        $item = MessageItem::create([
            'message_id' => $message->id,
            'sender' => $message->isSender(),
            'message' => $request->msg,
        ]);

        // Files
        if ($request->attachments) {
            foreach ($request->attachments as $f) {
                $extension = $f->getClientOriginalExtension();
                $filename = Uuid::generate()->string . '.' . $extension;
                $mime = $f->getMimeType();

                Storage::disk('message_files')->put($filename, file_get_contents($f));

                MessageFile::create([
                    'message_id' => $item->id,
                    'filename' => $filename,
                    'original_filename' => $f->getClientOriginalName(),
                    'mime' => $mime
                ]);
            }
        }

        if ($message->isSender())
            $message->receiver_seen_at = null;
        else
            $message->sender_seen_at = null;

        $message->touch();
        $message->save();

        return response()->json(['success' => true, 'message' => date('F j, Y H:i A', strtotime($item->created_at))]);
    }

    public function addMessageParent(Request $request) {
        if ($request->type == '1') {
            foreach ($request->customers as $customer) {
                $message = Message::create([
                    'sender_id' => Auth::user()->vendor_meta_id,
                    'sender_type' => MessageRole::$VENDOR,
                    'receiver_id' => $customer,
                    'receiver_type' => MessageRole::$BUYER,
                    'title' => $request->title,
                    'topic' => $request->topic,
                    'sender_seen_at' => Carbon::now()->toDateTimeString()
                ]);

                $item = MessageItem::create([
                    'message_id' => $message->id,
                    'message' => $request->message
                ]);

                // Files
                if ($request->attachments) {
                    foreach ($request->attachments as $f) {
                        $extension = $f->getClientOriginalExtension();
                        $filename = Uuid::generate()->string . '.' . $extension;
                        $mime = $f->getMimeType();

                        Storage::disk('message_files')->put($filename, file_get_contents($f));

                        MessageFile::create([
                            'message_id' => $item->id,
                            'filename' => $filename,
                            'original_filename' => $f->getClientOriginalName(),
                            'mime' => $mime
                        ]);
                    }
                }
            }
        } else {
            $message = Message::create([
                'sender_id' => Auth::user()->vendor_meta_id,
                'sender_type' => MessageRole::$VENDOR,
                'receiver_id' => 0,
                'receiver_type' => MessageRole::$ADMIN,
                'title' => $request->title,
                'topic' => $request->topic,
                'sender_seen_at' => Carbon::now()->toDateTimeString()
            ]);

            $item = MessageItem::create([
                'message_id' => $message->id,
                'message' => $request->message
            ]);

            // Files
            if ($request->attachments) {
                foreach ($request->attachments as $f) {
                    $extension = $f->getClientOriginalExtension();
                    $filename = Uuid::generate()->string . '.' . $extension;
                    $mime = $f->getMimeType();

                    Storage::disk('message_files')->put($filename, file_get_contents($f));

                    MessageFile::create([
                        'message_id' => $item->id,
                        'filename' => $filename,
                        'original_filename' => $f->getClientOriginalName(),
                        'mime' => $mime
                    ]);
                }
            }
        }
    }
}
