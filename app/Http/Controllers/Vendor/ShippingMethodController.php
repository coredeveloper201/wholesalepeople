<?php

namespace App\Http\Controllers\Vendor;

use App\Model\Courier;
use App\Model\ShippingMethod;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Auth;

class ShippingMethodController extends Controller
{
    public function index() {
        $couriers = Courier::with('shipMethods')->orderBy('name')->get();
        $shippingMethods = ShippingMethod::where('vendor_meta_id', Auth::user()->vendor_meta_id)
            ->with('courier', 'shipMethod')
            ->orderBy('list_order')->get();

        return view('vendor.dashboard.administration.shipping_method.index', compact('shippingMethods', 'couriers'))
            ->with('page_title', 'Shipping Methods');
    }

    public function addPost(Request $request) {
        $rules = [
            'courier' => 'required',
            'list_order' => 'required|integer'
        ];

        if ($request->courier == 0)
            $rules['ship_method_text'] = 'required|max:191';
        else
            $rules['ship_method'] = 'required';

        $request->validate($rules);

        if ($request->default) {
            ShippingMethod::where('vendor_meta_id', Auth::user()->vendor_meta_id)->update([ 'default' => 0 ]);
        }

        ShippingMethod::create([
            'status' => $request->status,
            'default' => ($request->default) ? 1 : 0,
            'courier_id' => $request->courier,
            'ship_method_id' => ($request->courier == '0' ? 0 : $request->ship_method),
            'ship_method_text' => $request->ship_method_text,
            'list_order' => $request->list_order,
            'vendor_meta_id' => Auth::user()->vendor_meta_id
        ]);

        return redirect()->back()->with('message', 'Ship Method Added!');
    }

    public function editPost(Request $request) {
        $rules = [
            'courier' => 'required',
            'list_order' => 'required|integer'
        ];

        if ($request->courier == 0)
            $rules['ship_method_text'] = 'required|max:191';
        else
            $rules['ship_method'] = 'required';

        $request->validate($rules);

        if ($request->default) {
            ShippingMethod::where('vendor_meta_id', Auth::user()->vendor_meta_id)->update([ 'default' => 0 ]);
        }

        ShippingMethod::where('id', $request->shippingMethodId)->update([
            'status' => $request->status,
            'default' => ($request->default) ? 1 : 0,
            'courier_id' => $request->courier,
            'ship_method_id' => ($request->courier == '0' ? 0 : $request->ship_method),
            'ship_method_text' => $request->ship_method_text,
            'list_order' => $request->list_order,
        ]);

        return redirect()->back()->with('message', 'Ship Method Updated!');
    }

    public function delete(Request $request) {
        ShippingMethod::where('id', $request->id)->delete();
    }

    public function changeStatus(Request $request) {
        ShippingMethod::where('id', $request->id)->update(['status' => $request->status]);
    }

    public function changeDefault(Request $request) {
        ShippingMethod::where('vendor_meta_id', Auth::user()->vendor_meta_id)->update([ 'default' => 0 ]);
        ShippingMethod::where('id', $request->id)->update([ 'default' => 1 ]);
    }
}
