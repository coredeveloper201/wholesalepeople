<?php

namespace App\Http\Controllers\Vendor;

use App\Enumeration\CouponType;
use App\Enumeration\OrderStatus;
use App\Enumeration\VendorImageType;
use App\Model\CartItem;
use App\Model\Category;
use App\Model\Color;
use App\Model\Item;
use App\Model\MetaBuyer;
use App\Model\Notification;
use App\Model\Order;
use App\Model\OrderItem;
use App\Model\ShippingMethod;
use App\Model\StoreCredit;
use App\Model\StoreCreditTransection;
use App\Model\User;
use App\Model\VendorImage;
use Illuminate\Contracts\Encryption\DecryptException;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Hash;
use PDF;
Use DateTime;
use Validator;

class OrderController extends Controller
{
    public function newOrders() {
        $orders = Order::where('vendor_meta_id', Auth::user()->vendor_meta_id)
            ->with('shippingMethod')
            ->where('status', OrderStatus::$NEW_ORDER)
            ->orderBy('created_at', 'desc')
            ->get();

        foreach($orders as &$order) {
            $count = Order::where('status', '!=', OrderStatus::$INIT)
                ->where('user_id', $order->user->id)
                ->where('vendor_meta_id', $order->vendor_meta_id)
                ->where('created_at', '<=', $order->created_at)
                ->count();
            $order->count = $count;
        }

        return view('vendor.dashboard.orders.new_orders', compact('orders'))->with('page_title', 'New Orders');
    }

    public function orderDetails(Order $order) {
        $allItems = [];
        $order->load('user', 'items', 'shippingMethod');
        $itemIds = [];

        $shippingMethods = ShippingMethod::where('status', 1)
            ->where('vendor_meta_id', Auth::user()->vendor_meta_id)
            ->with('shipMethod')
            ->orderBy('list_order')
            ->get();

        $count = Order::where('status', '!=', OrderStatus::$INIT)
            ->where('user_id', $order->user->id)
            ->where('vendor_meta_id', $order->vendor_meta_id)
            ->where('created_at', '<=', $order->created_at)
            ->count();

        $countText = 'This is the '.$this->addOrdinalNumberSuffix($count).' order.';

        foreach($order->items as $item) {
            $allItems[$item->item_id][] = $item;
            $itemIds[] = $item->id;
        }

        $products = Item::where('status', 1)
            ->where('vendor_meta_id', Auth::user()->vendor_meta_id)
            ->orderBy('activated_at', 'desc')
            ->paginate(21);

        $categories = Category::where('vendor_meta_id', Auth::user()->vendor_meta_id)
            ->orderBy('sort')
            ->get();

        return view('vendor.dashboard.orders.order_details', compact('order', 'allItems', 'countText',
            'itemIds', 'shippingMethods', 'products', 'categories'))
            ->with('page_title', 'Order Details');
    }

    public function orderDetailsPost(Order $order, Request $request) {
        $order->load('items');

        $rules = [
            'input_discount' => 'required|numeric|min:0',
            'input_shipping_cost' => 'required|numeric|min:0',
            'tracking_number' => 'nullable|max:191',
            'shipping_method_id' => 'required',
            'input_store_credit' => 'required|numeric|min:0',
        ];

        foreach ($order->items as $item) {
            if ($item->back_order == 1)
                continue;

            $rules['size_'.$item->id.'.*'] = 'required|integer|min:0';
            $rules['pack_'.$item->id] = 'required|integer|min:0';
            $rules['unit_price_'.$item->id] = 'required|regex:/^[0-9]+(?:\.[0-9]{1,2})?$/';
        }

        $validator = Validator::make($request->all(), $rules);

        if ($validator->fails()) {
            return redirect()->back()
                ->withErrors($validator)
                ->withInput();
        }

        $moreStoreCredit = $request->input_store_credit - $order->store_credit;
        $sc = StoreCredit::where('vendor_meta_id', $order->vendor_meta_id)->where('user_id', $order->user_id)->first();

        $validator->after(function ($validator) use($request, $moreStoreCredit, $order, $sc) {
            if ($moreStoreCredit > 0) {
                if (!$sc) {
                    $validator->errors()->add('input_store_credit', 'Insufficient Balance.');
                } else {
                    if ($moreStoreCredit > $sc->amount) {
                        $validator->errors()->add('input_store_credit', 'Insufficient Balance.');
                    }
                }
            }
        });

        if ($validator->fails()) {
            return redirect()->back()
                ->withErrors($validator)
                ->withInput();
        }

        if ($sc) {
            $sc->amount -= $moreStoreCredit;
            $sc->save();
            $sc->touch();
        }

        if ($request->input_store_credit > 0) {
            $transection = StoreCreditTransection::where('order_id', $order->id)->where('amount', '<', 0)->first();

            if (!$transection) {
                StoreCreditTransection::create([
                    'user_id' => $order->user_id,
                    'vendor_meta_id' => $order->vendor_meta_id,
                    'order_id' => $order->id,
                    'reason' => 'Used',
                    'amount' => $request->input_store_credit * (-1),
                ]);
            } else {
                $transection->amount = $request->input_store_credit * (-1);
                $transection->save();
                $transection->touch();
            }
        } else {
            StoreCreditTransection::where('order_id', $order->id)->where('amount', '<', 0)->delete();
        }

        $subTotal = 0;

        foreach ($order->items as $item) {
            if ($item->back_order == 1)
                continue;

            $sizeVar = 'size_'.$item->id;
            $packVar = 'pack_'.$item->id;
            $perUnitPriceVar = 'unit_price_'.$item->id;
            $itemPerPack = 0;
            $size = '';

            foreach ($request->$sizeVar as $s) {
                $size .= $s.'-';
                $itemPerPack += (int) $s;
            }

            $size = rtrim($size, "-");

            $item->pack = $size;
            $item->qty = $request->$packVar;
            $item->item_per_pack = $itemPerPack;
            $item->total_qty = $itemPerPack * (int) $request->$packVar;
            $item->per_unit_price = $request->$perUnitPriceVar;
            $item->amount = $request->$perUnitPriceVar * $itemPerPack * $request->$packVar;
            $item->save();

            $subTotal += $item->amount;
        }

        $shippingMethod = ShippingMethod::where('id', $request->shipping_method_id)->first();

        $order->subtotal = $subTotal;
        $order->discount = $request->input_discount;
        $order->shipping_cost = $request->input_shipping_cost;
        $order->store_credit = $request->input_store_credit;
        $order->total = $order->subtotal + (float) $request->input_shipping_cost - (float) $request->input_discount - $order->store_credit;
        $order->status = $request->order_status;
        $order->tracking_number = $request->tracking_number;
        $order->invoice_number = $request->invoice_number;
        $order->shipping_method_id = $request->shipping_method_id;
        $order->shipping = $shippingMethod->courier_id == 0 ? $shippingMethod->ship_method_text : $shippingMethod->shipMethod->name;
        $order->save();
        $order->touch();

        if($request->notify_user && $request->notify_user) {
            Notification::create([
                'user_id' => $order->user_id,
                'order_id' => $order->id,
                'text' => $order->vendor->company_name.' updated order no. '.$order->order_number,
                'link' => route('show_order_details', ['order' => $order->id]),
                'view' => 0
            ]);
        }

        return redirect()->back()->with('message', 'Order Updated!');
    }

    public function confirmOrders() {
        $orders = Order::where('vendor_meta_id', Auth::user()->vendor_meta_id)
            ->where('status', OrderStatus::$CONFIRM_ORDER)
            ->orderBy('created_at', 'desc')
            ->get();

        foreach($orders as &$order) {
            $count = Order::where('status', '!=', OrderStatus::$INIT)
                ->where('user_id', $order->user->id)
                ->where('vendor_meta_id', $order->vendor_meta_id)
                ->where('created_at', '<=', $order->created_at)
                ->count();
            $order->count = $count;
        }

        return view('vendor.dashboard.orders.confirmed_orders', compact('orders'))->with('page_title', 'Confirmed Orders');
    }

    public function backedOrders() {
        $orders = Order::where('vendor_meta_id', Auth::user()->vendor_meta_id)
            ->where('status', OrderStatus::$BACK_ORDER)
            ->orderBy('created_at', 'desc')
            ->get();

        foreach($orders as &$order) {
            $count = Order::where('status', '!=', OrderStatus::$INIT)
                ->where('user_id', $order->user->id)
                ->where('vendor_meta_id', $order->vendor_meta_id)
                ->where('created_at', '<=', $order->created_at)
                ->count();
            $order->count = $count;
        }

        return view('vendor.dashboard.orders.back_orders', compact('orders'))->with('page_title', 'Back Orders');
    }

    public function shippedOrders() {
        $orders = Order::where('vendor_meta_id', Auth::user()->vendor_meta_id)
            ->whereIn('status', [OrderStatus::$FULLY_SHIPPED_ORDER, OrderStatus::$PARTIALLY_SHIPPED_ORDER])
            ->orderBy('created_at', 'desc')
            ->get();

        foreach($orders as &$order) {
            $count = Order::where('status', '!=', OrderStatus::$INIT)
                ->where('user_id', $order->user->id)
                ->where('vendor_meta_id', $order->vendor_meta_id)
                ->where('created_at', '<=', $order->created_at)
                ->count();
            $order->count = $count;
        }


        return view('vendor.dashboard.orders.shipped_orders', compact('orders'))->with('page_title', 'Shipped Orders');
    }

    public function cancelledOrders() {
        $orders = Order::where('vendor_meta_id', Auth::user()->vendor_meta_id)
            ->whereIn('status', [OrderStatus::$CANCEL_BY_AGREEMENT, OrderStatus::$CANCEL_BY_BUYER, OrderStatus::$CANCEL_BY_VENDOR])
            ->orderBy('created_at', 'desc')
            ->get();

        foreach($orders as &$order) {
            $count = Order::where('status', '!=', OrderStatus::$INIT)
                ->where('user_id', $order->user->id)
                ->where('vendor_meta_id', $order->vendor_meta_id)
                ->where('created_at', '<=', $order->created_at)
                ->count();
            $order->count = $count;
        }

        return view('vendor.dashboard.orders.cancel_orders', compact('orders'))->with('page_title', 'Cancel Orders');
    }

    public function returnedOrders() {
        $orders = Order::where('vendor_meta_id', Auth::user()->vendor_meta_id)
            ->where('status', OrderStatus::$RETURNED)
            ->orderBy('created_at', 'desc')
            ->get();

        foreach($orders as &$order) {
            $count = Order::where('status', '!=', OrderStatus::$INIT)
                ->where('user_id', $order->user->id)
                ->where('vendor_meta_id', $order->vendor_meta_id)
                ->where('created_at', '<=', $order->created_at)
                ->count();
            $order->count = $count;
        }

        return view('vendor.dashboard.orders.returned', compact('orders'))->with('page_title', 'Return Orders');
    }

    public function allOrders(Request $request) {
        $parameters = [];
        $userIds = [];

        // Search
        if (isset($request->text) && $request->text !='') {
            if (isset($request->search) && $request->search == '1') {
                $metaBuyers = MetaBuyer::where('company_name', 'like', '%' . $request->text . '%')->get();

                foreach ($metaBuyers as $buyer)
                    $userIds[] = $buyer->user_id;
            } else if (isset($request->search) && $request->search == '2') {
                $parameters[] = ['order_number', 'like', '%' . $request->text . '%'];
            } else if (isset($request->search) && $request->search == '3') {
                $parameters[] = ['tracking_number', 'like', '%' . $request->text . '%'];
            }
        }

        if (isset($request->ship) && $request->ship !='') {
            if ($request->ship == '1')
                $parameters[] = ['status', '=', OrderStatus::$PARTIALLY_SHIPPED_ORDER];
            else if ($request->ship == '2')
                $parameters[] = ['status', '=', OrderStatus::$FULLY_SHIPPED_ORDER];
        }


        if (isset($request->date) && $request->date !='') {
            if ($request->date == '1') {
                $parameters[] = ['created_at', '>', date('Y-m-d 23:59:59', strtotime('-1 days'))];
            } else if ($request->date == '2') {
                $day = date('w');
                $week_start = date('Y-m-d', strtotime('-'.$day.' days'));
                $parameters[] = ['created_at', '>=', $week_start];
            } else if ($request->date == '3') {
                $parameters[] = ['created_at', '>', date('Y-m-01')];
            } else if ($request->date == '5') {
                $parameters[] = ['created_at', '>', date('Y-01-01')];
            } else if ($request->date == '6') {
                $parameters[] = ['created_at', '>=', date('Y-m-d', strtotime('-1 days'))];
                $parameters[] = ['created_at', '<=', date('Y-m-d 23:59:59', strtotime('-1 days'))];
            } else if ($request->date == '8') {
                $month_ini = new DateTime("first day of last month");
                $month_end = new DateTime("last day of last month");

                $parameters[] = ['created_at', '>=', $month_ini->format('Y-m-d')];
                $parameters[] = ['created_at', '<=', $month_end->format('Y-m-d')];
            } else if ($request->date == '10') {
                $parameters[] = ['created_at', '>=', date("Y-m-d",strtotime("last year January 1st"))];
                $parameters[] = ['created_at', '<=', date("Y-m-d",strtotime("last year December 31st"))];
            } else if ($request->date == '13') {
                $parameters[] = ['created_at', '>=', date('Y-m-d', strtotime('-7 days'))];
            } else if ($request->date == '14') {
                $parameters[] = ['created_at', '>=', date('Y-m-d', strtotime('-30 days'))];
            } else if ($request->date == '15') {
                $parameters[] = ['created_at', '>=', date('Y-m-d', strtotime('-90 days'))];
            } else if ($request->date == '16') {
                $parameters[] = ['created_at', '>=', date('Y-m-d', strtotime('-365 days'))];
            } else if ($request->date == '0') {
                $starDate = DateTime::createFromFormat('m/d/Y', $request->startDate);
                $endDate = DateTime::createFromFormat('m/d/Y', $request->endDate);

                $parameters[] = ['created_at', '>=', $starDate->format('Y-m-d 00:00:00')];
                $parameters[] = ['created_at', '<=', $endDate->format('Y-m-d 23:59:59')];
            }
        } else {
            $parameters[] = ['created_at', '>=', date('Y-m-d', strtotime('-90 days'))];
        }

        $totals = [];

        // New Orders
        $newOrdersQuery = Order::query();
        $newOrdersQuery->where('vendor_meta_id', Auth::user()->vendor_meta_id)
            ->where('status', OrderStatus::$NEW_ORDER)
            ->where($parameters)
            ->orderBy('created_at', 'desc');

        if (isset($request->text) && $request->text != '' && isset($request->search) && $request->search == '1') {
            $newOrdersQuery->whereIn('user_id', $userIds);
        }

        $newOrders = $newOrdersQuery->paginate(10, ['*'], 'p1');
        $totals['new'] = $newOrdersQuery->sum('total');


        foreach($newOrders as &$order) {
            $count = Order::where('status', '!=', OrderStatus::$INIT)
                ->where('user_id', $order->user->id)
                ->where('vendor_meta_id', $order->vendor_meta_id)
                ->where('created_at', '<=', $order->created_at)
                ->count();
            $order->count = $count;
        }

        // Confirm Orders
        $confirmOrdersQuery = Order::query();
        $confirmOrdersQuery->where('vendor_meta_id', Auth::user()->vendor_meta_id)
            ->where('status', OrderStatus::$CONFIRM_ORDER)
            ->where($parameters)
            ->orderBy('created_at', 'desc');

        if (isset($request->text) && $request->text != '' && isset($request->search) && $request->search == '1') {
            $confirmOrdersQuery->whereIn('user_id', $userIds);
        }

        $confirmOrders = $confirmOrdersQuery->paginate(10, ['*'], 'p2');
        $totals['confirm'] = $confirmOrdersQuery->sum('total');

        foreach($confirmOrders as &$order) {
            $count = Order::where('status', '!=', OrderStatus::$INIT)
                ->where('user_id', $order->user->id)
                ->where('vendor_meta_id', $order->vendor_meta_id)
                ->where('created_at', '<=', $order->created_at)
                ->count();
            $order->count = $count;
        }


        // Back Orders
        $backOrdersQuery = Order::query();

        $backOrdersQuery->where('vendor_meta_id', Auth::user()->vendor_meta_id)
            ->where('status', OrderStatus::$BACK_ORDER)
            ->where($parameters)
            ->orderBy('created_at', 'desc');

        if (isset($request->text) && $request->text != '' && isset($request->search) && $request->search == '1') {
            $backOrdersQuery->whereIn('user_id', $userIds);
        }

        $backOrders = $backOrdersQuery->paginate(10, ['*'], 'p3');
        $totals['back'] = $backOrdersQuery->sum('total');

        foreach($backOrders as &$order) {
            $count = Order::where('status', '!=', OrderStatus::$INIT)
                ->where('user_id', $order->user->id)
                ->where('vendor_meta_id', $order->vendor_meta_id)
                ->where('created_at', '<=', $order->created_at)
                ->count();
            $order->count = $count;
        }

        // Shipped Orders
        $shippedOrdersQuery = Order::query();
        $shippedOrdersQuery->where('vendor_meta_id', Auth::user()->vendor_meta_id)
            ->whereIn('status', [OrderStatus::$FULLY_SHIPPED_ORDER, OrderStatus::$PARTIALLY_SHIPPED_ORDER])
            ->where($parameters)
            ->orderBy('created_at', 'desc');

        if (isset($request->text) && $request->text != '' && isset($request->search) && $request->search == '1') {
            $shippedOrdersQuery->whereIn('user_id', $userIds);
        }

        $shippedOrders = $shippedOrdersQuery->paginate(10, ['*'], 'p4');
        $totals['shipped'] = $shippedOrdersQuery->sum('total');

        foreach($shippedOrders as &$order) {
            $count = Order::where('status', '!=', OrderStatus::$INIT)
                ->where('user_id', $order->user->id)
                ->where('vendor_meta_id', $order->vendor_meta_id)
                ->where('created_at', '<=', $order->created_at)
                ->count();
            $order->count = $count;
        }

        // Cancel Orders
        $cancelOrdersQuery = Order::query();
        $cancelOrdersQuery->where('vendor_meta_id', Auth::user()->vendor_meta_id)
            ->whereIn('status', [OrderStatus::$CANCEL_BY_AGREEMENT, OrderStatus::$CANCEL_BY_BUYER, OrderStatus::$CANCEL_BY_VENDOR])
            ->where($parameters)
            ->orderBy('created_at', 'desc');

        if (isset($request->text) && $request->text != '' && isset($request->search) && $request->search == '1') {
            $shippedOrdersQuery->whereIn('user_id', $userIds);
        }

        $cancelOrders = $cancelOrdersQuery->paginate(10, ['*'], 'p5');
        $totals['cancel'] = $cancelOrdersQuery->sum('total');

        foreach($cancelOrders as &$order) {
            $count = Order::where('status', '!=', OrderStatus::$INIT)
                ->where('user_id', $order->user->id)
                ->where('vendor_meta_id', $order->vendor_meta_id)
                ->where('created_at', '<=', $order->created_at)
                ->count();
            $order->count = $count;
        }

        $appends = [
            'p1' => $newOrders->currentPage(),
            'p2' => $confirmOrders->currentPage(),
            'p3' => $confirmOrders->currentPage(),
            'p4' => $confirmOrders->currentPage(),
            'p5' => $confirmOrders->currentPage(),
        ];

        foreach ($request->all() as $key => $value) {
            if ($key != 'p1' && $key != 'p2' && $key != 'p3' && $key != 'p4')
                $appends[$key] = ($value == null) ? '' : $value;
        }

        return view('vendor.dashboard.orders.all_orders', compact('newOrders', 'confirmOrders', 'backOrders',
            'appends', 'shippedOrders', 'cancelOrders', 'totals'))
            ->with('page_title', 'All Orders');
    }

    public function addOrdinalNumberSuffix($num) {
        if (!in_array(($num % 100),array(11,12,13))){
            switch ($num % 10) {
                // Handle 1st, 2nd, 3rd
                case 1:  return $num.'st';
                case 2:  return $num.'nd';
                case 3:  return $num.'rd';
            }
        }
        return $num.'th';
    }

    public function incompleteOrders() {
        /*$sql = "SELECT user_id
                FROM `cart_items`
                WHERE vendor_meta_id=".Auth::user()->vendor_meta_id."
                GROUP BY user_id";

        $result = DB::select($sql);*/

        $result = CartItem::select(DB::raw('user_id, vendor_meta_id, meta_vendors.id, company_name, MAX(cart_items.updated_at) d'))
            ->join('meta_vendors', 'meta_vendors.id', '=', 'cart_items.vendor_meta_id')
            ->where('meta_vendors.id', Auth::user()->vendor_meta_id)
            ->groupBy('user_id', 'vendor_meta_id', 'meta_vendors.id', 'company_name')
            ->orderBy('d', 'desc')
            ->paginate(20);


        $userIds = [];

        $orders = [];

        foreach ($result as $r)
            $userIds[] = $r->user_id;


        foreach ($userIds as $id) {
            $total = 0;

            $cartItems = CartItem::where('user_id', $id)
                ->where('vendor_meta_id', Auth::user()->vendor_meta_id)
                ->orderBy('updated_at', 'desc')
                ->with('item')
                ->get();


            foreach ($cartItems as $item) {
                $sizes = explode("-", $item->item->pack->name);
                $itemInPack = 0;

                for($i=1; $i <= sizeof($sizes); $i++) {
                    $var = 'pack'.$i;

                    if ($item->item->pack->$var != null)
                        $itemInPack += (int) $item->item->pack->$var;
                }

                $total += $itemInPack * $item->quantity * $item->item->price;
            }

            $orders [] = [
                'user_id' => $id,
                'total' => $total,
                'updated_at' => $cartItems[0]->updated_at
            ];
        }

        /*$orders = Order::where('vendor_meta_id', Auth::user()->vendor_meta_id)
            ->where('status', OrderStatus::$INIT)
            ->orderBy('created_at', 'desc')
            ->paginate(10);*/

        return view('vendor.dashboard.orders.incomplete', compact('orders', 'result'))->with('page_title', 'Incomplete Checkouts');
    }

    public function incompleteOrderDetails($order) {
        $cartItems = CartItem::where('vendor_meta_id', Auth::user()->vendor_meta_id)
            ->where('user_id', $order)
            ->with('item', 'color')
            ->get();

        //$order->load('user', 'items');
        $allItems = [];

        foreach($cartItems as $item)
            $allItems[$item->item_id][] = $item;

        return view('vendor.dashboard.orders.incomplete_details', compact('allItems'))->with('page_title', 'Incomplete Checkouts');
    }

    public function printPdf(Request $request) {
        $orderIds = explode(',', $request->order);
        $orders = [];
        $items = [];
        $logo_paths = [];

        foreach ($orderIds as $orderId) {
            $order = Order::where('id', $orderId)->with('user', 'items')->first();
            $allItems = [];

            // Decrypt
            $cardNumber = '';
            $cardFullName = '';
            $cardExpire = '';
            $cardCvc = '';

            try {
                $cardNumber = decrypt($order->card_number);
                $cardFullName = decrypt($order->card_full_name);
                $cardExpire = decrypt($order->card_expire);
                $cardCvc = decrypt($order->card_cvc);
            } catch (DecryptException $e) {

            }

            $order->card_number = $cardNumber;
            $order->card_full_name = $cardFullName;
            $order->card_expire = $cardExpire;
            $order->card_cvc = $cardCvc;

            foreach ($order->items as $item)
                $allItems[$item->item_id][] = $item;

            // Vendor Logo
            $logo_path = '';
            $vendorLogo = VendorImage::where('status', 1)
                ->where('vendor_meta_id', $order->vendor_meta_id)
                ->where('type', VendorImageType::$LOGO)
                ->first();

            if ($vendorLogo)
                $logo_path = public_path($vendorLogo->image_path);

            $orders[] = $order;
            $items[] = $allItems;
            $logo_paths[] = $logo_path;
        }

        $data = [
            'all_items' => $items,
            'orders' => $orders,
            'logo_paths' => $logo_paths
        ];

        $pdf = PDF::setOptions(['isHtml5ParserEnabled' => true, 'isRemoteEnabled' => true])->loadView('vendor.dashboard.orders.pdf.with_image', $data);
        return $pdf->stream('invoice.pdf');
    }

    public function printPdfWithOutImage(Request $request) {
        $orderIds = explode(',', $request->order);
        $orders = [];
        $items = [];
        $logo_paths = [];

        foreach ($orderIds as $orderId) {
            $order = Order::where('id', $orderId)->with('user', 'items')->first();
            $allItems = [];

            // Decrypt
            $cardNumber = '';
            $cardFullName = '';
            $cardExpire = '';
            $cardCvc = '';

            try {
                $cardNumber = decrypt($order->card_number);
                $cardFullName = decrypt($order->card_full_name);
                $cardExpire = decrypt($order->card_expire);
                $cardCvc = decrypt($order->card_cvc);
            } catch (DecryptException $e) {

            }

            $order->card_number = $cardNumber;
            $order->card_full_name = $cardFullName;
            $order->card_expire = $cardExpire;
            $order->card_cvc = $cardCvc;

            foreach ($order->items as $item)
                $allItems[$item->item_id][] = $item;

            // Vendor Logo
            $logo_path = '';
            $vendorLogo = VendorImage::where('status', 1)
                ->where('vendor_meta_id', $order->vendor_meta_id)
                ->where('type', VendorImageType::$LOGO)
                ->first();

            if ($vendorLogo)
                $logo_path = asset($vendorLogo->image_path);

            $orders[] = $order;
            $items[] = $allItems;
            $logo_paths[] = $logo_path;
        }


        $data = [
            'all_items' => $items,
            'orders' => $orders,
            'logo_paths' => $logo_paths
        ];

        $pdf = PDF::loadView('vendor.dashboard.orders.pdf.without_image', $data);
        return $pdf->stream('invoice.pdf');
    }

    public function printPacklist(Request $request) {
        $ids = explode(',', $request->order);

        $orderNumbers = [];

        $orders = Order::whereIn('id', $ids)->with('items')->get();

        foreach ($orders as $order)
            $orderNumbers[] = $order->order_number;

        $sql = "SELECT style_no, color, SUM(total_qty) count
                FROM order_items
                WHERE order_id IN (".$request->order.")
                GROUP BY style_no, color";
        $items = DB::select($sql);

        $data = [
            'orders' => $orderNumbers,
            'items' => $items
        ];

        $pdf = PDF::loadView('vendor.dashboard.orders.pdf.packlist', $data);
        return $pdf->stream('packlist.pdf');
    }

    public function createBackorder(Request $request) {
        $orderItem = OrderItem::where('id', $request->ids[0])->first();
        $subTotal = OrderItem::whereIn('id', $request->ids)->sum('amount');
        $order = $orderItem->order;

        $newOrderDiscount = 0;

        if ($order->discount > 0 && $order->coupon_type == CouponType::$PERCENTAGE) {
            $newOrderDiscount = ($order->coupon_amount / 100) * $subTotal;
        } else if ($order->discount > 0 && $order->coupon_type == CouponType::$FIXED_PRICE) {
            $newOrderDiscount = $order->coupon_amount / 2;
        }

        $newOrder = Order::create([
            'status' => OrderStatus::$BACK_ORDER,
            'user_id' => $order->user_id,
            'vendor_meta_id' => $order->vendor_meta_id,
            'order_number' => $this->generateRandomString(13),
            'company_name' => $order->company_name,
            'email' => $order->email,
            'name' => $order->name,
            'shipping' => $order->shipping,
            'shipping_method_id' => $order->shipping_method_id,
            'shipping_address' => $order->shipping_address,
            'shipping_city' => $order->shipping_city,
            'shipping_state' => $order->shipping_state,
            'shipping_zip' => $order->shipping_zip,
            'shipping_country' => $order->shipping_country,
            'shipping_phone' => $order->shipping_phone,
            'billing_address' => $order->billing_address,
            'billing_city' => $order->billing_city,
            'billing_state' => $order->billing_state,
            'billing_zip' => $order->billing_zip,
            'billing_country' => $order->billing_country,
            'billing_phone' => $order->billing_phone,
            'card_number' => $order->card_number,
            'card_full_name' => $order->card_full_name,
            'card_expire' => $order->card_expire,
            'card_cvc' => $order->card_cvc,
            'coupon_amount' => $order->coupon_amount,
            'coupon_type' => $order->coupon_type,
            'coupon' => $order->coupon,
            'coupon_description' => $order->coupon_description,
            'subtotal' => $subTotal,
            'discount' => $newOrderDiscount,
            'shipping_cost' => 0,
            'total' => $subTotal - $newOrderDiscount,
        ]);

        $redirectUrl = '';

        if (sizeof($order->items) == sizeof($request->ids)) {
            $order->review()->delete();
            $order->notifications()->delete();
            $order->storeCreditTransections()->delete();
            $order->delete();

            $redirectUrl = route('vendor_all_orders');
        } else {
            $discount = 0;

            if ($order->discount > 0 && $order->coupon_type == CouponType::$PERCENTAGE) {
                $discount = ($order->coupon_amount / 100) * ($subTotal);
            } else if ($order->discount > 0 && $order->coupon_type == CouponType::$FIXED_PRICE) {
                $discount = $order->coupon_amount / 2;
            }

            $order->discount = $order->discount - $discount;
            $order->total = $order->total - $subTotal - $discount;
            $order->subtotal = $order->subtotal - $subTotal;
            $order->save();
            $redirectUrl = route('vendor_order_details', ['order' => $order->id]);
        }

        OrderItem::whereIn('id', $request->ids)->update(['order_id' => $newOrder->id]);

        foreach ($request->ids as $id) {
            $orderItem = OrderItem::where('id', $id)->first();
            $new = $orderItem->replicate();
            $new->back_order = 1;
            $new->order_id = $order->id;
            $new->save();
        }

        return response()->json(['success' => true, 'url' => $redirectUrl]);
    }

    public function outOfStock(Request $request) {
        $orderItem = OrderItem::where('id', $request->ids[0])->first();
        $subTotal = OrderItem::whereIn('id', $request->ids)->sum('amount');
        $order = $orderItem->order;

        $order->total = $order->total - $subTotal;
        $order->subtotal = $order->subtotal - $subTotal;
        $order->save();

        OrderItem::whereIn('id', $request->ids)->update([
            'qty' => 0,
            'total_qty' => 0,
            'amount' => 0,
        ]);
    }

    public function deleteOrderItem(Request $request) {
        $orderItem = OrderItem::where('id', $request->ids[0])->first();
        $subTotal = OrderItem::whereIn('id', $request->ids)->sum('amount');
        $order = $orderItem->order;

        $order->total = $order->total - $subTotal;
        $order->subtotal = $order->subtotal - $subTotal;
        $order->save();

        OrderItem::whereIn('id', $request->ids)->delete();
    }

    public function checkPassword(Request $request) {
        $password = $request->password;

        if (Hash::check($password, Auth::user()->password)) {
            $order = Order::where('id', $request->orderID)->first();

            // Decrypt
            $cardNumber = '';
            $cardFullName = '';
            $cardExpire = '';
            $cardCvc = '';

            try {
                $cardNumber = decrypt($order->card_number);
                $cardFullName = decrypt($order->card_full_name);
                $cardExpire = decrypt($order->card_expire);
                $cardCvc = decrypt($order->card_cvc);
            } catch (DecryptException $e) {

            }

            $order->card_number = $cardNumber;
            $order->card_full_name = $cardFullName;
            $order->card_expire = $cardExpire;
            $order->card_cvc = $cardCvc;

            return response()->json([
                'success' => true,
                'fullName' => $order->card_full_name,
                'number' => $order->card_number,
                'cvc' => $order->card_cvc,
                'expire' => $order->card_expire,
            ]);
        }

        return response()->json(['success' => false]);
    }

    public function maskCardNumber(Request $request) {
        $order = Order::where('id', $request->id)->first();

        // Decrypt
        $cardNumber = '';

        try {
            $cardNumber = decrypt($order->card_number);
        } catch (DecryptException $e) {

        }

        $order->card_number = $cardNumber;

        $mask = str_repeat("*", (strlen($order->card_number) - 4)).substr($order->card_number,-4,4);
        $order->card_number = encrypt($mask);
        $order->save();

        return response()->json(['success' => true, 'mask' => $mask]);
    }

    public function deleteOrder(Request $request) {
        $order = Order::where('id', $request->id)->first();

        $order->review()->delete();
        $order->notifications()->delete();
        $order->storeCreditTransections()->delete();
        $order->delete();
    }

    public function itemDetails(Request $request) {
        $item = Item::where('id', $request->id)
            ->where('status', 1)
            ->with('colors')
            ->first();

        return response()->json($item->toArray());
    }

    public function addItem(Request $request) {
        $item = Item::where('id', $request->item_id)
            ->where('status', 1)
            ->with('pack')
            ->first();

        $newAmount = 0;

        foreach ($request->colors as $color_id => $count) {
            if ($count != null || $count != '') {
                $count = (int) $count;
                $color = Color::where('id', $color_id)->first();

                $previous = OrderItem::where('order_id', $request->order_id)
                    ->where('item_id', $item->id)
                    ->where('color', $color->name)
                    ->first();

                if ($previous) {
                    $previous->qty = $previous->qty + $count;
                    $previous->total_qty = $previous->total_qty + ($previous->item_per_pack * $count);
                    $previous->amount = $previous->total_qty * $previous->per_unit_price;

                    $newAmount += ($previous->item_per_pack * $count) * $previous->per_unit_price;
                    $previous->save();
                } else {
                    $tmp = OrderItem::where('order_id', $request->order_id)
                        ->where('item_id', $item->id)
                        ->first();

                    if ($tmp) {
                        $sizes = explode("-", $tmp->size);
                    } else {
                        $sizes = explode("-", $item->pack->name);
                    }

                    $pack = '';
                    $itemInPack = 0;

                    for ($i = 1; $i <= sizeof($sizes); $i++) {
                        $var = 'pack' . $i;

                        if ($item->pack->$var != null) {
                            $pack .= $item->pack->$var . '-';
                            $itemInPack += (int)$item->pack->$var;
                        } else {
                            $pack .= '0-';
                        }
                    }

                    $pack = rtrim($pack, "-");

                    OrderItem::create([
                        'order_id' => $request->order_id,
                        'item_id' => $item->id,
                        'style_no' => $item->style_no,
                        'color' => $color->name,
                        'size' => $item->pack->name,
                        'item_per_pack' => $itemInPack,
                        'pack' => $pack,
                        'qty' => $count,
                        'total_qty' => $itemInPack * $count,
                        'per_unit_price' => $item->price,
                        'amount' => $item->price * $itemInPack * $count,
                    ]);

                    $newAmount += $item->price * $itemInPack * $count;
                }
            }
        }

        $order = Order::where('id', $request->order_id)->first();
        $order->subtotal = $order->subtotal + $newAmount;
        $order->total = $order->total + $newAmount;
        $order->save();
    }

    public function generateRandomString($length) {
        $characters = '0123456789ABCDEFGHIJKLMNOPQRSTUVWXYZ';
        $charactersLength = strlen($characters);
        $randomString = '';
        for ($i = 0; $i < $length; $i++) {
            $randomString .= $characters[rand(0, $charactersLength - 1)];
        }
        return $randomString;
    }
}
