<?php

namespace App\Http\Controllers\Vendor;

use App\Model\BestItem;
use App\Model\Category;
use App\Model\Item;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Auth;
use DB;

class BestItemController extends Controller
{
    public function index() {
        $itemsIds = BestItem::where('vendor_meta_id', Auth::user()->vendor_meta_id)->orderBy('sort')->pluck('item_id')->toArray();
        $items = [];

        if (sizeof($itemsIds) > 0) {
            $items = Item::where('status', 1)->whereIn('id', $itemsIds)
                ->orderByRaw(\DB::raw("FIELD(id, " . implode(',', $itemsIds) . " )"))->get();
        }

        $products = Item::where('status', 1)
            ->where('vendor_meta_id', Auth::user()->vendor_meta_id)
            ->orderBy('activated_at', 'desc')
            ->paginate(21);

        $categories = Category::where('vendor_meta_id', Auth::user()->vendor_meta_id)
            ->orderBy('sort')
            ->get();

        return view('vendor.dashboard.best_items.index', compact('products', 'categories', 'items'))->with('page_title', 'Best Items');
    }

    public function add(Request $request) {
        $item = Item::where('vendor_meta_id', Auth::user()->vendor_meta_id)->where('status', 1)
            ->where('id', $request->id)->first();

        if ($item) {
            $previous = BestItem::where('item_id', $item->id)->first();

            if (!$previous) {
                BestItem::create([
                    'vendor_meta_id' => Auth::user()->vendor_meta_id,
                    'item_id' => $request->id
                ]);
            }
        }
    }

    public function sort(Request $request) {
        for($i=0; $i < sizeof($request->ids); $i++) {
            BestItem::where('item_id', $request->ids[$i])
                ->where('vendor_meta_id', Auth::user()->vendor_meta_id)
                ->update([
                    'sort' => (int) $request->sort[$i]
                ]);
        }

        return redirect()->back()->with('message', 'Updated!');
    }

    public function delete(Request $request) {
        BestItem::where('item_id', $request->id)->where('vendor_meta_id', Auth::user()->vendor_meta_id)->delete();
    }
}
