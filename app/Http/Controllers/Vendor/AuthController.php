<?php

namespace App\Http\Controllers\Vendor;

use App\Enumeration\Role;
use App\Model\Country;
use App\Model\LoginHistory;
use App\Model\MetaVendor;
use App\Model\State;
use App\Model\User;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Hash;
use Auth;
use Carbon\Carbon;
use File;

class AuthController extends Controller
{
    public function register() {
        $countries = Country::orderBy('name')->get();
        $usStates = State::where('country_id', 1)->orderBy('name')->get()->toArray();
        $caStates =State::where('country_id', 2)->orderBy('name')->get()->toArray();

        return view('vendor.auth.register', compact('countries', 'usStates', 'caStates'))->with('page_title', 'Vendor Register');
    }

    public function registerPost(Request $request) {
        $messages = [
            'required' => 'This field is required.',
        ];

        $rules = [
            'companyName' => 'required|string|max:255',
            'firstName' => 'required|string|max:255',
            'userId' => 'required|string|max:255|unique:users,user_id',
            'website' => 'nullable|string|max:255',
            'businessName' => 'required|string|max:255',
            'lastName' => 'required|string|max:255',
            'password' => 'required|string|min:6',
            'email' => 'required|string|email|max:255|unique:users',
            'address' => 'required|string|max:255',
            'unit' => 'nullable|string|max:255',
            'city' => 'required|string|max:255',
            'zipCode' => 'required|string|max:255',
            'country' => 'required',
            'phone' => 'required|max:255',
            'fax' => 'nullable|max:255',
            'factoryAddress' => 'required|string|max:255',
            'factoryUnit' => 'nullable|string|max:255',
            'factoryCity' => 'required|string|max:255',
            'factoryZipCode' => 'required|string|max:255',
            'factoryCountry' => 'required',
            'factoryPhone' => 'required|max:255',
            'factoryEveningPhone' => 'nullable|max:255',
            'factoryAlternatePhone' => 'nullable|max:255',
            'factoryFax' => 'nullable|max:255',
            'companyInformation' => 'nullable|max:1000',
            'hearAboutUs' => 'required',
        ];

        if ($request->location == "INT")
            $rules['state'] = 'required|string|max:255';
        else
            $rules['stateSelect'] = 'required';

        if ($request->factoryLocation == "INT")
            $rules['factoryState'] = 'required|string|max:255';
        else
            $rules['factoryStateSelect'] = 'required';

        if ($request->hearAboutUs && $request->hearAboutUs == "other")
            $rules['hearAboutUsOtherText'] = 'required|string|max:255';

        $request->validate($rules, $messages);

        $state_id = null;
        $state = null;
        $factory_state_id = null;
        $factory_state = null;
        $hearFromOtherText = null;

        if ($request->location == "INT")
            $state = $request->state;
        else
            $state_id = $request->stateSelect;

        if ($request->factoryLocation == "INT")
            $factory_state = $request->factoryState;
        else
            $factory_state_id = $request->factoryStateSelect;

        if ($request->hearAboutUs && $request->hearAboutUs == "other")
            $hearFromOtherText = $request->hearAboutUsOtherText;

        $meta = MetaVendor::create([
            'company_name' => $request->companyName,
            'business_name' => $request->businessName,
            'website' => $request->website,
            'primary_customer_market' => $request->primaryCustomerMarket,
            'billing_location' => $request->location,
            'billing_address' => $request->address,
            'billing_unit' => $request->unit,
            'billing_city' => $request->city,
            'billing_state_id' => $state_id,
            'billing_state' => $state,
            'billing_zip' => $request->zipCode,
            'billing_country_id' => $request->country,
            'billing_phone' => $request->phone,
            'billing_fax' => $request->fax,
            'billing_commercial' => ($request->showroomCommercial == null) ? 0 : 1,
            'factory_location' => $request->factoryLocation,
            'factory_address' => $request->factoryAddress,
            'factory_unit' => $request->factoryUnit,
            'factory_city' => $request->factoryCity,
            'factory_state_id' => $factory_state_id,
            'factory_state' => $factory_state,
            'factory_zip' => $request->factoryZipCode,
            'factory_country_id' => $request->factoryCountry,
            'factory_phone' => $request->factoryPhone,
            'factory_alternate_phone' => $request->factoryAlternatePhone,
            'factory_evening_phone' => $request->factoryEveningPhone,
            'factory_fax' => $request->factoryFax,
            'factory_commercial' => ($request->factoryCommercial == null) ? 0 : 1,
            'company_info' => $request->companyInformation,
            'hear_about_us' => $request->hearAboutUs,
            'hear_about_us_other' => $hearFromOtherText,
            'receive_offers' => $request->receiveSpecialOffers,
            'verified' => 0,
            'active' => 0
        ]);

        $user = User::create([
            'first_name' => $request->firstName,
            'last_name' => $request->lastName,
            'email' => $request->email,
            'user_id' => $request->userId,
            'password' => Hash::make($request->password),
            'role' => Role::$VENDOR,
            'vendor_meta_id' => $meta->id,
        ]);

        File::makeDirectory(public_path('images/vendors/'.$meta->id));
        File::makeDirectory(public_path('images/vendors/'.$meta->id.'/list'));
        File::makeDirectory(public_path('images/vendors/'.$meta->id.'/original'));
        File::makeDirectory(public_path('images/vendors/'.$meta->id.'/thumbs'));
        File::makeDirectory(public_path('images/vendors/'.$meta->id.'/colors'));
        File::makeDirectory(public_path('images/vendors/'.$meta->id.'/colors/thumbs'));

        return redirect()->route('register_vendor_complete');
    }

    public function registerComplete() {
        return view('vendor.auth.complete')->with('page_title', 'Vendor Registration Complete');
    }

    public function login() {
        return view('vendor.auth.login');
    }

    public function loginPost(Request $request) {
        $user = User::where('user_id', $request->username)
            ->whereIn('role', [Role::$VENDOR, Role::$VENDOR_EMPLOYEE])
            ->with('vendor')->first();

        if (!$user)
            return redirect()->route('login_vendor')->with('message', 'Username not found.')->withInput();

        if ($user->active == 0)
            return redirect()->route('login_vendor')->with('message', 'User is inactivate.')->withInput();

        if ($user->vendor->active == 0)
            return redirect()->route('login_vendor')->with('message', 'Vendor is inactivate.')->withInput();

        if ($user->vendor->verified == 0)
            return redirect()->route('login_vendor')->with('message', 'Vendor is not verified.')->withInput();


        if (Hash::check($request->password, $user->password)) {
            if ($request->remember_me)
                Auth::login($user, true);
            else
                Auth::login($user);

            $user->last_login = Carbon::now()->toDateTimeString();
            $user->save();

            LoginHistory::create([
                'user_id' => $user->id,
                'ip' => $this->getRealIpAddr()
            ]);

            return redirect()->route('vendor_dashboard');
        }

        return redirect()->route('login_vendor')->with('message', 'Invalid Password.')->withInput();
    }

    public function logout() {
        Auth::logout();

        return redirect()->route('login_vendor');
    }

    public function getRealIpAddr()
    {
        if (!empty($_SERVER['HTTP_CLIENT_IP']))   //check ip from share internet
        {
            $ip=$_SERVER['HTTP_CLIENT_IP'];
        }
        elseif (!empty($_SERVER['HTTP_X_FORWARDED_FOR']))   //to check ip is pass from proxy
        {
            $ip=$_SERVER['HTTP_X_FORWARDED_FOR'];
        }
        else
        {
            $ip=$_SERVER['REMOTE_ADDR'];
        }
        return $ip;
    }
}
