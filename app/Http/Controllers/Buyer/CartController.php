<?php

namespace App\Http\Controllers\Buyer;

use App\Model\CartItem;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Auth;

class CartController extends Controller
{
    public function addToCart(Request $request) {
        for ($i=0; $i < sizeof($request->colors); $i++) {
            $cartItem = CartItem::where([
                ['user_id', Auth::user()->id],
                ['item_id', $request->itemId],
                ['color_id', $request->colors[$i]]
            ])->first();

            if ($cartItem) {
                $cartItem->quantity += (int) $request->qty[$i];
                $cartItem->save();
            } else {
                CartItem::create([
                    'user_id' => Auth::user()->id,
                    'vendor_meta_id' => $request->vendor_id,
                    'item_id' => $request->itemId,
                    'color_id' => $request->colors[$i],
                    'quantity' => $request->qty[$i],
                ]);
            }
        }
    }

    public function addToCartSuccess() {
        return back()->with('message', 'Added to cart.');
    }

    public function showCart() {
        $temp = [];
        $cartItems = [];

        $cartObjs = CartItem::where('user_id', Auth::user()->id)
            ->with('item', 'color')
            ->get()
            ->sortBy(function($useritem, $key) {
                return $useritem->item->vendor_meta_id;
            });

        foreach($cartObjs as $obj) {
            $temp[$obj->item->vendor_meta_id][$obj->item->id][] = $obj;
        }

        $checkOutVendor = [];
        $vendorCounter = 0;
        foreach ($temp as $vendorId => $vendor) {
            $itemCounter = 0;
            $checkOutVendor[] = $vendorId;

            foreach($vendor as $itemId => $item) {
                $cartItems[$vendorCounter][$itemCounter] = $item;
                $itemCounter++;
            }

            $vendorCounter++;
        }

        return view('pages.cart', compact('cartItems', 'checkOutVendor'))->with('page_title', 'Cart');
    }

    public function updateCart(Request $request) {
        for($i=0; $i < sizeof($request->ids); $i++) {
            if ($request->qty[$i] == '0')
                CartItem::where('id', $request->ids[$i])->delete();
            else
                CartItem::where('id', $request->ids[$i])->update(['quantity' => $request->qty[$i]]);
        }
    }

    public function updateCartSuccess() {
        return back()->with('message', 'Cart Updated!');
    }

    public function deleteCart(Request $request) {
        CartItem::where('id', $request->id)->delete();
    }
}
