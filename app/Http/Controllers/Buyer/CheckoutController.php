<?php

namespace App\Http\Controllers\Buyer;

use App\Enumeration\CouponType;
use App\Enumeration\OrderStatus;
use App\Enumeration\Role;
use App\Enumeration\VendorImageType;
use App\Http\Controllers\Vendor\OrderController;
use App\Model\BuyerShippingAddress;
use App\Model\Card;
use App\Model\CartItem;
use App\Model\Country;
use App\Model\Coupon;
use App\Model\MetaVendor;
use App\Model\Order;
use App\Model\OrderItem;
use App\Model\ShippingMethod;
use App\Model\State;
use App\Model\StoreCredit;
use App\Model\StoreCreditTransection;
use App\Model\User;
use App\Model\VendorImage;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Auth;
use Illuminate\Contracts\Encryption\DecryptException;
use CreditCard;
use Validator;
use Mail;
use PDF;

class CheckoutController extends Controller
{
    public function create(Request $request) {
        $user = Auth::user();
        $user->load('buyer');
        $orderIds = [];

        $counter = 0;
        foreach ($request->ids as $id) {
            $storeCredit = 0;
            $vendor = MetaVendor::where('id', $id)->first();

            if (isset($request->storeCredit[$counter]) && is_numeric($request->storeCredit[$counter])) {
                $sc = StoreCredit::where('vendor_meta_id', $id)->where('user_id', Auth::user()->id)->first();

                if (!$sc) {
                    return response()->json(['success' => false, 'message' => 'Insufficient Store Credit for '.$vendor->company_name]);
                }

                if ($sc->amount < $request->storeCredit[$counter])
                    return response()->json(['success' => false, 'message' => 'Insufficient Store Credit for '.$vendor->company_name]);
                else
                    $storeCredit = (float) $request->storeCredit[$counter];
            }

            $order = Order::create([
                'status' => OrderStatus::$INIT,
                'user_id' => $user->id,
                'vendor_meta_id' => $id,
                'name' => $user->first_name.' '.$user->last_name,
                'company_name' => $user->buyer->company_name,
                'email' => $user->email,
                'billing_location' => $user->buyer->billing_location,
                'billing_address' => $user->buyer->billing_address,
                'billing_unit' => $user->buyer->billing_unit,
                'billing_city' => $user->buyer->billing_city,
                'billing_state' => ($user->buyer->billingState == null) ? $user->buyer->billing_state : $user->buyer->billingState->code,
                'billing_state_id' => $user->buyer->billing_state_id,
                'billing_state_text' => $user->buyer->billing_state,
                'billing_zip' => $user->buyer->billing_zip,
                'billing_country' => $user->buyer->billingCountry->name,
                'billing_country_id' => $user->buyer->billing_country_id,
                'billing_phone' => $user->buyer->billing_phone,
            ]);

            $orderIds[] = $order->id;

            // Cart Items
            $cartItems = CartItem::where('user_id', $order->user_id)
                ->where('vendor_meta_id', $order->vendor_meta_id)
                ->get();
            $subTotal = 0;
            $orderNumber = $this->generateRandomString(13);

            foreach ($cartItems as $cartItem) {
                $sizes = explode("-", $cartItem->item->pack->name);
                $pack = '';

                $itemInPack = 0;

                for ($i = 1; $i <= sizeof($sizes); $i++) {
                    $var = 'pack' . $i;

                    if ($cartItem->item->pack->$var != null) {
                        $pack .= $cartItem->item->pack->$var . '-';
                        $itemInPack += (int)$cartItem->item->pack->$var;
                    } else {
                        $pack .= '0-';
                    }
                }

                $pack = rtrim($pack, "-");
                $subTotal += $itemInPack * $cartItem->quantity * $cartItem->item->price;

                OrderItem::create([
                    'order_id' => $order->id,
                    'item_id' => $cartItem->item_id,
                    'style_no' => $cartItem->item->style_no,
                    'color' => $cartItem->color->name,
                    'size' => $cartItem->item->pack->name,
                    'item_per_pack' => $itemInPack,
                    'pack' => $pack,
                    'qty' => $cartItem->quantity,
                    'total_qty' => $itemInPack * $cartItem->quantity,
                    'per_unit_price' => $cartItem->item->price,
                    'amount' => $itemInPack * $cartItem->quantity * $cartItem->item->price,
                ]);
            }

            $order->order_number = $orderNumber;
            $order->subtotal = $subTotal;
            $order->discount = 0;
            $order->shipping_cost = 0;
            $order->total = $subTotal-$storeCredit;

            if ($storeCredit > $subTotal)
                $storeCredit = $subTotal;

            $order->store_credit = $storeCredit;
            $order->save();

            $counter++;
        }

        return response()->json(['success' => true, 'message' => encrypt(implode(",", $orderIds))]);
    }

    public function index(Request $request) {
        $user = Auth::user();
        $user->load('buyer');

        // Check Orders
        $ids = [];

        try {
            $ids = decrypt($request->id);
            $ids = explode(',', $ids);
        } catch (DecryptException $e) {

        }

        if (sizeof($ids) == 0)
            abort(404);

        //$orders = Order::whereIn('id', $ids)->with('items')->get();

        $countries = Country::orderBy('name')->get();
        $usStates = State::where('country_id', 1)->orderBy('name')->get()->toArray();
        $caStates =State::where('country_id', 2)->orderBy('name')->get()->toArray();

        $address = BuyerShippingAddress::where('user_id', Auth::user()->id)->where('default', 1)->first();

        $shippingAddresses = BuyerShippingAddress::where('user_id', Auth::user()->id)->get();

        // Order Details
        $orders = [];
        $allOrdersTotal = 0;

        foreach ($ids as $id) {
            $order = Order::where('id', $id)->with(['user', 'vendor' => function($query) {
                $query->with('shippingMethods');
            }])->first();

            $temp = [];
            $cartItems = [];

            $cartObjs = CartItem::where('user_id', $order->user_id)
                ->where('vendor_meta_id', $order->vendor_meta_id)
                ->with('item', 'color')
                ->get()
                ->sortBy(function ($useritem, $key) {
                    return $useritem->item->vendor_meta_id;
                });

            foreach ($cartObjs as $obj) {
                $temp[$obj->item->id][] = $obj;
            }

            $itemCounter = 0;
            foreach ($temp as $itemId => $item) {
                $cartItems[$itemCounter] = $item;
                $itemCounter++;
            }

            $order->cartItems = $cartItems;
            $orders[] = $order;

            $allOrdersTotal += $order->total;
        }

        $cards = Card::where('user_id', Auth::user()->id)->get();

        return view('buyer.checkout.single', compact('address', 'countries', 'usStates', 'caStates',
            'shippingAddresses', 'orders', 'allOrdersTotal', 'cards'));

        /*return view('buyer.checkout.index', compact('user', 'countries', 'usStates', 'caStates',
            'order', 'address', 'shippingAddresses'))
            ->with('page_title', 'Checkout');*/
    }

    public function addressPost(Request $request) {
        // Check Orders
        $ids = [];

        try {
            $ids = decrypt($request->orders);
            $ids = explode(',', $ids);
        } catch (DecryptException $e) {

        }

        if (sizeof($ids) == 0)
            abort(404);

        $shippingAddress = BuyerShippingAddress::where('id', $request->address_id)->with('state', 'country')->first();

        Order::whereIn('id', $ids)->update([
            'shipping_address_id' => $request->address_id,
            'shipping_location' => $shippingAddress->location,
            'shipping_address' => $shippingAddress->address,
            'shipping_unit' => $shippingAddress->unit,
            'shipping_city' => $shippingAddress->city,
            'shipping_state' => ($shippingAddress->state == null) ? $shippingAddress->state_text : $shippingAddress->state->name,
            'shipping_state_id' => $shippingAddress->state_id,
            'shipping_state_text' => $shippingAddress->state_text,
            'shipping_zip' => $shippingAddress->zipCode,
            'shipping_country' => $shippingAddress->country->name,
            'shipping_country_id' => $shippingAddress->country->id,
            'shipping_phone' => $shippingAddress->phone,
        ]);

        return redirect()->route('show_shipping_method', ['id' => $request->orders]);
    }

    public function shipping(Request $request) {
        // Check Orders
        $ids = [];

        try {
            $ids = decrypt($request->id);
            $ids = explode(',', $ids);
        } catch (DecryptException $e) {

        }

        if (sizeof($ids) == 0)
            abort(404);

        $orders = Order::whereIn('id', $ids)->with(['user', 'vendor' => function($query) {
            $query->with('shippingMethods');
        }])->get();

        /*$shipping_methods = ShippingMethod::where('vendor_meta_id', $order->vendor_meta_id)
            ->where('status', 1)
            ->with('courier', 'shipMethod')->get();*/

        return view('buyer.checkout.shipping', compact( 'orders'))->with('page_title', 'Checkout');
    }

    public function shippingPost(Request $request) {
        // Check orders
        $ids = [];

        try {
            $ids = decrypt($request->orderIds);
            $ids = explode(',', $ids);
        } catch (DecryptException $e) {

        }

        if (sizeof($ids) == 0)
            abort(404);

        $rules = [];

        $counter = 0;
        foreach ($ids as $id) {
            $rules['shipping_method.'.$counter] = 'required';
            $counter++;
        }

        $request->validate($rules);

        for($i=0; $i<sizeof($request->orderId); $i++) {
            $shippingMethod = ShippingMethod::where('id', $request->shipping_method[$i])->with('shipMethod')->first();

            Order::where('id', $request->orderId[$i])->update([
                'shipping' => $shippingMethod->courier_id == 0 ? $shippingMethod->ship_method_text : $shippingMethod->shipMethod->name,
                'shipping_method_id' => $request->shipping_method[$i]
            ]);
        }


        return redirect()->route('show_payment', ['id' => $request->orderIds]);
    }

    public function payment(Request $request) {
        // Check orders
        $ids = [];

        try {
            $ids = decrypt($request->id);
            $ids = explode(',', $ids);
        } catch (DecryptException $e) {

        }

        if (sizeof($ids) == 0)
            abort(404);

        $order = Order::whereIn('id', $ids)->first();

        // Decrypt
        $cardNumber = '';
        $cardFullName = '';
        $cardExpire = '';
        $cardCvc = '';

        try {
            $cardNumber = decrypt($order->card_number);
            $cardFullName = decrypt($order->card_full_name);
            $cardExpire = decrypt($order->card_expire);
            $cardCvc = decrypt($order->card_cvc);
        } catch (DecryptException $e) {

        }

        $order->card_number = $cardNumber;
        $order->card_full_name = $cardFullName;
        $order->card_expire = $cardExpire;
        $order->card_cvc = $cardCvc;

        return view('buyer.checkout.payment', compact('user', 'order'))->with('page_title', 'Checkout');
    }

    public function paymentPost(Request $request) {
        $validator = Validator::make($request->all(), [
            'number' => 'required|max:191|min:6',
            'name' => 'required|max:191',
            'expiry' => 'required|date_format:"m/y"',
            'cvc' => 'required'
        ]);

        if ($validator->fails()) {
            return redirect()->back()
                ->withErrors($validator)
                ->withInput();
        }

        $validator->after(function ($validator) use($request) {
            // Card Number Check
            $card = CreditCard::validCreditCard($request->number);

            if (!$card['valid'])
                $validator->errors()->add('number', 'Invalid Card Number');

            // CVC Check
            $validCvc = CreditCard::validCvc($request->cvc, $card['type']);
            if (!$validCvc)
                $validator->errors()->add('cvc', 'Invalid CVC');

            // Expiry Check
            $tmp  = explode('/', $request->expiry);
            $validDate = CreditCard::validDate('20'.$tmp[1], $tmp[0]);
            if (!$validDate)
                $validator->errors()->add('expiry', 'Invalid Expiry');
        });

        if ($validator->fails()) {
            return redirect()->back()
                ->withErrors($validator)
                ->withInput();
        }

        // Check orders
        $ids = [];

        try {
            $ids = decrypt($request->id);
            $ids = explode(',', $ids);
        } catch (DecryptException $e) {

        }

        if (sizeof($ids) == 0)
            abort(404);

        foreach ($ids as $id) {
            Order::where('id', $id)->update([
                'card_number' => encrypt($request->number),
                'card_full_name' => encrypt($request->name),
                'card_expire' => encrypt($request->expiry),
                'card_cvc' => encrypt($request->cvc),
            ]);
        }

        return redirect()->route('show_checkout_review', ['id' => $request->id]);
    }

    public function review(Request $request) {
        // Check orders
        $ids = [];

        try {
            $ids = decrypt($request->id);
            $ids = explode(',', $ids);
        } catch (DecryptException $e) {

        }

        if (sizeof($ids) == 0)
            abort(404);

        $orders = [];

        foreach ($ids as $id) {
            $order = Order::where('id', $id)->with('user', 'vendor')->first();

            $temp = [];
            $cartItems = [];

            $cartObjs = CartItem::where('user_id', $order->user_id)
                ->where('vendor_meta_id', $order->vendor_meta_id)
                ->with('item', 'color')
                ->get()
                ->sortBy(function ($useritem, $key) {
                    return $useritem->item->vendor_meta_id;
                });

            foreach ($cartObjs as $obj) {
                $temp[$obj->item->id][] = $obj;
            }

            $itemCounter = 0;
            foreach ($temp as $itemId => $item) {
                $cartItems[$itemCounter] = $item;
                $itemCounter++;
            }

            $order->cartItems = $cartItems;
            $orders[] = $order;

            // Decrypt
            $cardNumber = '';
            $cardFullName = '';
            $cardExpire = '';
            $cardCvc = '';

            try {
                $cardNumber = decrypt($order->card_number);
                $cardNumber = str_repeat("*", (strlen($cardNumber) - 4)).substr($cardNumber,-4,4);
                $cardFullName = decrypt($order->card_full_name);
                $cardExpire = decrypt($order->card_expire);
                $cardCvc = decrypt($order->card_cvc);
            } catch (DecryptException $e) {

            }

            $order->card_number = $cardNumber;
            $order->card_full_name = $cardFullName;
            $order->card_expire = $cardExpire;
            $order->card_cvc = $cardCvc;
        }

        return view('buyer.checkout.review', compact('orders'))->with('page_title', 'Checkout');
    }

    public function complete(Request $request) {
        // Check orders
        $ids = [];

        try {
            $ids = decrypt($request->id);
            $ids = explode(',', $ids);
        } catch (DecryptException $e) {

        }

        if (sizeof($ids) == 0)
            abort(404);

        foreach ($ids as $id) {
            $noteVar = 'note_'.$id;

            $order = Order::where('id', $id)->first();

            if ($order->status == OrderStatus::$INIT) {
                $order->status = OrderStatus::$NEW_ORDER;
                $order->note = $request->$noteVar;
                $order->save();

                if ($order->store_credit > 0) {
                    StoreCreditTransection::create([
                        'user_id' => Auth::user()->id,
                        'vendor_meta_id' => $order->vendor_meta_id,
                        'order_id' => $order->id,
                        'reason' => 'Used',
                        'amount' => $order->store_credit * (-1),
                    ]);

                    $storeCredit = StoreCredit::where('user_id', Auth::user()->id)->where('vendor_meta_id', $order->vendor_meta_id)->first();

                    $storeCredit->amount -= $order->store_credit;
                    $storeCredit->save();
                    $storeCredit->touch();
                }

                $pdfData = $this->getPdfData($order);

                // Send Mail to Buyer
                Mail::send('emails.buyer.order_confirmation', ['order' => $order], function ($message) use ($order, $pdfData) {
                    $message->subject('Order Confirmed');
                    $message->to($order->email, $order->name);
                    $message->attachData($pdfData, $order->order_number.'.pdf');
                });

                // Send Mail to Vendor
                $user = User::where('role', Role::$VENDOR)
                    ->where('vendor_meta_id', $order->vendor_meta_id)->first();

                Mail::send('emails.vendor.new_order', ['order' => $order], function ($message) use ($order, $pdfData, $user) {
                    $message->subject('New Order - '.$order->order_number);
                    $message->to($user->email, $user->first_name.' '.$user->last_name);
                    $message->attachData($pdfData, $order->order_number.'.pdf');
                });

                CartItem::where('user_id', $order->user_id)
                    ->where('vendor_meta_id', $order->vendor_meta_id)
                    ->delete();
            }
        }

        return redirect()->route('checkout_complete_view', ['id' => $request->id]);
    }

    public function completeView(Request $request) {
        // Check orders
        $ids = [];

        try {
            $ids = decrypt($request->id);
            $ids = explode(',', $ids);
        } catch (DecryptException $e) {

        }

        if (sizeof($ids) == 0)
            abort(404);

        $orders = Order::whereIn('id', $ids)->get();

        return view('buyer.checkout.complete', compact('orders', 'totalValue'));
    }

    public function addressSelect(Request $request) {
        $ids = explode(',', $request->id);
        Order::whereIn('id', $ids)->update(['shipping_address_id' => $request->shippingId]);
    }

    public function generateRandomString($length) {
        $characters = '0123456789ABCDEFGHIJKLMNOPQRSTUVWXYZ';
        $charactersLength = strlen($characters);
        $randomString = '';
        for ($i = 0; $i < $length; $i++) {
            $randomString .= $characters[rand(0, $charactersLength - 1)];
        }
        return $randomString;
    }

    public function getPdfData($order) {
        $allItems = [];
        $order->load('user', 'items');

        // Decrypt
        $cardNumber = '';
        $cardFullName = '';
        $cardExpire = '';
        $cardCvc = '';

        try {
            $cardNumber = decrypt($order->card_number);
            $cardFullName = decrypt($order->card_full_name);
            $cardExpire = decrypt($order->card_expire);
            $cardCvc = decrypt($order->card_cvc);
        } catch (DecryptException $e) {

        }

        $order->card_number = $cardNumber;
        $order->card_full_name = $cardFullName;
        $order->card_expire = $cardExpire;
        $order->card_cvc = $cardCvc;

        foreach($order->items as $item)
            $allItems[$item->item_id][] = $item;

        // Vendor Logo
        $logo_path = '';
        $vendorLogo = VendorImage::where('status', 1)
            ->where('vendor_meta_id', $order->vendor_meta_id)
            ->where('type', VendorImageType::$LOGO)
            ->first();

        if ($vendorLogo)
            $logo_path = public_path($vendorLogo->image_path);

        $data = [
            'all_items' => [$allItems],
            'orders' => [$order],
            'logo_paths' => [$logo_path]
        ];

        $pdf = PDF::setOptions(['isHtml5ParserEnabled' => true, 'isRemoteEnabled' => true])->loadView('vendor.dashboard.orders.pdf.with_image', $data);
        return $pdf->output();
    }

    public function applyCoupon(Request $request) {
        $order = Order::where('id', $request->id)->where('user_id', Auth::user()->id)->first();

        if (!$order)
            return response()->json(['success' => false, 'message' => 'Invalid Order.']);

        $coupon = Coupon::where('vendor_meta_id', $order->vendor_meta_id)
            ->where('name', $request->coupon)
            ->first();

        if (!$coupon)
            return response()->json(['success' => false, 'message' => 'Invalid Coupon.']);

        if ($coupon->multiple_use == 0) {
            $previous = Order::where('user_id', Auth::user()->id)
                ->where('status', '!=', OrderStatus::$INIT)
                ->where('vendor_meta_id', $coupon->vendor_meta_id)
                ->where('coupon', $coupon->name)
                ->first();

            if ($previous)
                return response()->json(['success' => false, 'message' => 'Already used this coupon.']);
        }

        $subTotal = $order->subtotal;
        $discount = 0;

        if ($coupon->type == CouponType::$FIXED_PRICE)
            $discount = $coupon->amount;
        else if ($coupon->type == CouponType::$PERCENTAGE){
            $discount = ($coupon->amount / 100) * $subTotal;
        } else if ($coupon->type == CouponType::$FREE_SHIPPING){
            $discount = 0;
        }

        if ($discount > $subTotal)
            $discount = $subTotal;

        $order->discount = $discount;
        $order->total = $subTotal - $discount;
        $order->coupon = $coupon->name;
        $order->coupon_type = $coupon->type;
        $order->coupon_amount = $coupon->amount;
        $order->coupon_description = $coupon->description;
        $order->save();

        return response()->json(['success' => true, 'message' => 'Success.']);
    }

    public function singleCheckoutPost(Request $request) {
        // Check orders
        $ids = [];

        try {
            $ids = decrypt($request->orders);
            $ids = explode(',', $ids);
        } catch (DecryptException $e) {

        }

        if (sizeof($ids) == 0)
            abort(404);

        $counter = 0;
        $rules = [
            'address_id' => 'required',
            'card_id' => 'required'
        ];

        foreach ($ids as $id) {
            $rules['shipping_method.'.$counter] = 'required';
            $counter++;
        }

        $validator = Validator::make($request->all(), $rules);

        if ($validator->fails()) {
            return redirect()->back()
                ->withErrors($validator)
                ->withInput();
        }

        $shippingAddress = BuyerShippingAddress::where('id', $request->address_id)->with('state', 'country')->first();
        $card = Card::where('id', $request->card_id)->where('user_id', Auth::user()->id)->first();

        if (!$card)
            abort(404);

        $counter = 0;
        foreach ($ids as $id) {
            $order = Order::where('id', $id)->where('status', OrderStatus::$INIT)->first();

            if ($order) {
                // Update Shipping Address
                $order->shipping_address_id = $request->address_id;
                $order->shipping_location = $shippingAddress->location;
                $order->shipping_address = $shippingAddress->address;
                $order->shipping_unit = $shippingAddress->unit;
                $order->shipping_city = $shippingAddress->city;
                $order->shipping_state = ($shippingAddress->state == null) ? $shippingAddress->state_text : $shippingAddress->state->code;
                $order->shipping_state_id = $shippingAddress->state_id;
                $order->shipping_state_text = $shippingAddress->state_text;
                $order->shipping_zip = $shippingAddress->zip;
                $order->shipping_country = $shippingAddress->country->name;
                $order->shipping_country_id = $shippingAddress->country->id;
                $order->shipping_phone = $shippingAddress->phone;

                // Update Payment
                $order->card_number = $card->card_number;
                $order->card_full_name = $card->card_name;
                $order->card_expire = encrypt($card->card_expiry);
                $order->card_cvc = $card->card_cvc;

                $order->card_location = $card->billing_location;
                $order->card_address = $card->billing_address;
                $order->card_city = $card->billing_city;
                $order->card_state_id = $card->billing_state_id;
                $order->card_state = $card->billing_state;
                $order->card_zip = $card->billing_zip;
                $order->card_country_id = $card->billing_country_id;


                // Update Shipping Method
                $shippingMethod = ShippingMethod::where('id', $request->shipping_method[$counter])->with('shipMethod')->first();

                $order->shipping = $shippingMethod->courier_id == 0 ? $shippingMethod->ship_method_text : $shippingMethod->shipMethod->name;
                $order->shipping_method_id = $request->shipping_method[$counter];

                // Note
                $noteVar = 'note_'.$id;
                $order->note = $request->$noteVar;

                $order->status = OrderStatus::$NEW_ORDER;
                $order->save();

                // Store Credit Transection
                if ($order->store_credit > 0) {
                    StoreCreditTransection::create([
                        'user_id' => Auth::user()->id,
                        'vendor_meta_id' => $order->vendor_meta_id,
                        'order_id' => $order->id,
                        'reason' => 'Used',
                        'amount' => $order->store_credit * (-1),
                    ]);

                    $storeCredit = StoreCredit::where('user_id', Auth::user()->id)->where('vendor_meta_id', $order->vendor_meta_id)->first();

                    $storeCredit->amount -= $order->store_credit;
                    $storeCredit->save();
                    $storeCredit->touch();
                }

                // Send Mail
                $pdfData = $this->getPdfData($order);

                // Send Mail to Buyer
                Mail::send('emails.buyer.order_confirmation', ['order' => $order], function ($message) use ($order, $pdfData) {
                    $message->subject('Order Confirmed');
                    $message->to($order->email, $order->name);
                    $message->attachData($pdfData, $order->order_number.'.pdf');
                });

                // Send Mail to Vendor
                $user = User::where('role', Role::$VENDOR)
                    ->where('vendor_meta_id', $order->vendor_meta_id)->first();

                Mail::send('emails.vendor.new_order', ['order' => $order], function ($message) use ($order, $pdfData, $user) {
                    $message->subject('New Order - '.$order->order_number);
                    $message->to($user->email, $user->first_name.' '.$user->last_name);
                    $message->attachData($pdfData, $order->order_number.'.pdf');
                });

                // Remove from cart
                CartItem::where('user_id', $order->user_id)
                    ->where('vendor_meta_id', $order->vendor_meta_id)
                    ->delete();
            }

            $counter++;
        }

        return redirect()->route('checkout_complete_view', ['id' => $request->orders]);
    }
}
