<?php

namespace App\Http\Controllers\Api;

use App\Enumeration\Role;
use App\Model\Category;
use App\Model\DefaultCategory;
use App\Model\User;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Hash;

class CategoryController extends Controller
{
    public function defaultCategories() {
        // Default Categories
        $defaultCategories = [];

        $categoriesCollection = DefaultCategory::orderBy('sort')->orderBy('name')->get();

        foreach($categoriesCollection as $cc) {
            if ($cc->parent == 0) {
                $data = [
                    'id' => $cc->id,
                    'name' => $cc->name
                ];

                $subCategories = [];
                foreach($categoriesCollection as $item) {
                    if ($item->parent == $cc->id) {
                        $data2 = [
                            'id' => $item->id,
                            'name' => $item->name
                        ];

                        $data3 = [];
                        foreach($categoriesCollection as $item2) {
                            if ($item2->parent == $item->id) {
                                $data3[] = [
                                    'id' => $item2->id,
                                    'name' => $item2->name
                                ];
                            }
                        }

                        $data2['subCategories'] = $data3;
                        $subCategories[] = $data2;
                    }
                }

                $data['subCategories'] = $subCategories;
                $defaultCategories[] = $data;
            }
        }

        return response()->json($defaultCategories);
    }

    public function vendorCategories(Request $request) {
        if (!isset($request->username) || !isset($request->password))
            return response()->json(['success' => false, 'message' => 'username & password parameter required.']);

        $user = User::where('user_id', $request->username)
            ->whereIn('role', [Role::$VENDOR, Role::$VENDOR_EMPLOYEE])
            ->with('vendor')->first();

        if (!$user)
            return response()->json(['success' => false, 'message' => 'Username not found.']);

        if ($user->vendor->active == 0)
            return response()->json(['success' => false, 'message' => 'Vendor is inactivate.']);

        if ($user->vendor->verified == 0)
            return response()->json(['success' => false, 'message' => 'Vendor is not verified.']);

        if (Hash::check($request->password, $user->password)) {
            $categories = [];

            $cats = Category::where('vendor_meta_id', $user->vendor->id)
                ->where('status', 1)->where('parent', 0)
                ->with('subCategories')->orderBy('sort')->get();

            foreach ($cats as $cat) {
                $sub = [];

                foreach ($cat->subCategories as $subCat) {
                    $sub[] = [
                        'id' => $subCat->id,
                        'name' => $subCat->name
                    ];
                }

                $categories[] = [
                    'id' => $cat->id,
                    'name' => $cat->name,
                    'sub' => $sub
                ];
            }
            return response()->json(['success' => true, 'items' => $categories]);
        }

        return response()->json(['success' => false, 'message' => 'Invalid Password.']);
    }
}
