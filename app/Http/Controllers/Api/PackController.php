<?php

namespace App\Http\Controllers\Api;

use App\Enumeration\Role;
use App\Model\Pack;
use App\Model\User;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Hash;

class PackController extends Controller
{
    public function addPack(Request $request) {
        if (!isset($request->username) || !isset($request->password))
            return response()->json(['success' => false, 'message' => 'username & password parameter required.']);

        $user = User::where('user_id', $request->username)
            ->whereIn('role', [Role::$VENDOR, Role::$VENDOR_EMPLOYEE])
            ->with('vendor')->first();

        if (!$user)
            return response()->json(['success' => false, 'message' => 'Username not found.']);

        if ($user->vendor->active == 0)
            return response()->json(['success' => false, 'message' => 'Vendor is inactivate.']);

        if ($user->vendor->verified == 0)
            return response()->json(['success' => false, 'message' => 'Vendor is not verified.']);

        if (Hash::check($request->password, $user->password)) {
            $requiredParameters = ['size', 'pack'];

            foreach ($requiredParameters as $parameter) {
                if (!isset($request->$parameter) || $request->$parameter == '')
                    return response()->json(['success' => false, 'message' => 'These parameters required: '.implode(',', $requiredParameters)]);
            }

            $previous = Pack::where('vendor_meta_id', $user->vendor_meta_id)
                ->where('name', $request->size)
                ->first();

            if ($previous)
                return response()->json(['success' => false, 'message' => 'Already exists.']);

            $packs = explode('-', $request->pack);

            foreach ($packs as $pack) {
                if (!ctype_digit($pack))
                    return response()->json(['success' => false, 'message' => 'Invalid pack name.']);
            }

            $pack = new Pack;

            $pack->name = $request->size;
            $pack->status = 1;
            $pack->default = 0;
            $pack->vendor_meta_id = $user->vendor_meta_id;

            for($i=1; $i<=sizeof($packs); $i++) {
                $tmp = 'pack'.$i;

                $pack->$tmp = $packs[$i-1];
            }

            $pack->save();

            return response()->json(['success' => true, 'message' => 'Pack added.']);
        }

        return response()->json(['success' => false, 'message' => 'Invalid Password.']);
    }

    public function getPacks(Request $request) {
        if (!isset($request->username) || !isset($request->password))
            return response()->json(['success' => false, 'message' => 'username & password parameter required.']);

        $user = User::where('user_id', $request->username)
            ->whereIn('role', [Role::$VENDOR, Role::$VENDOR_EMPLOYEE])
            ->with('vendor')->first();

        if (!$user)
            return response()->json(['success' => false, 'message' => 'Username not found.']);

        if ($user->vendor->active == 0)
            return response()->json(['success' => false, 'message' => 'Vendor is inactivate.']);

        if ($user->vendor->verified == 0)
            return response()->json(['success' => false, 'message' => 'Vendor is not verified.']);

        if (Hash::check($request->password, $user->password)) {
            $packs = Pack::where('vendor_meta_id', $user->vendor_meta_id)->get();

            return response()->json(['success' => true, 'data' => $packs->toArray()]);
        }

        return response()->json(['success' => false, 'message' => 'Invalid Password.']);
    }

    public function getPackDetails(Request $request) {
        if (!isset($request->username) || !isset($request->password))
            return response()->json(['success' => false, 'message' => 'username & password parameter required.']);

        $user = User::where('user_id', $request->username)
            ->whereIn('role', [Role::$VENDOR, Role::$VENDOR_EMPLOYEE])
            ->with('vendor')->first();

        if (!$user)
            return response()->json(['success' => false, 'message' => 'Username not found.']);

        if ($user->vendor->active == 0)
            return response()->json(['success' => false, 'message' => 'Vendor is inactivate.']);

        if ($user->vendor->verified == 0)
            return response()->json(['success' => false, 'message' => 'Vendor is not verified.']);

        if (Hash::check($request->password, $user->password)) {
            $pack = Pack::where('vendor_meta_id', $user->vendor_meta_id)->where('id', $request->packId)->first();

            return response()->json(['success' => true, 'data' => $pack->toArray()]);
        }

        return response()->json(['success' => false, 'message' => 'Invalid Password.']);
    }
}
