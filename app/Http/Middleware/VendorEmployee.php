<?php

namespace App\Http\Middleware;

use App\Enumeration\Role;
use Closure;
use Auth;

class VendorEmployee
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        if (Auth::check() && (Auth::user()->role == Role::$VENDOR || Auth::user()->role == Role::$VENDOR_EMPLOYEE)) {
            return $next($request);

        }

        return redirect()->route('login_vendor');
    }
}
