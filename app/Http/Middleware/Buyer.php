<?php

namespace App\Http\Middleware;

use App\Enumeration\Role;
use App\Model\Category;
use Closure;
use Auth;

class Buyer
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        if (Auth::check() && (Auth::user()->role == Role::$VENDOR || Auth::user()->role == Role::$VENDOR_EMPLOYEE)) {
            if ($request->route()->getName() == 'vendor_category_page') {
                if ($request->category->vendor_meta_id == Auth::user()->vendor_meta_id)
                    return $next($request);
            } else if ($request->route()->getName() == 'vendor_category_all_page') {
                if ($request->vendor->id == Auth::user()->vendor_meta_id)
                    return $next($request);
            } else if ($request->route()->getName() == 'vendor_category_get_items') {
                if ($request->vendor == Auth::user()->vendor_meta_id)
                    return $next($request);
            }
        }


        if (!Auth::check() || Auth::user()->role != Role::$BUYER) {
            return redirect()->route('buyer_login');
        }

        return $next($request);
    }
}
