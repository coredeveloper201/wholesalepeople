<?php

namespace App\Http\Middleware;

use App\Enumeration\Role;
use App\Model\Category;
use App\Model\Color;
use App\Model\Fabric;
use App\Model\Item;
use App\Model\MadeInCountry;
use App\Model\Order;
use App\Model\OrderItem;
use App\Model\Pack;
use App\Model\ShippingMethod;
use App\Model\SliderItem;
use App\Model\User;
use App\Model\VendorImage;
use Closure;
use Illuminate\Support\Facades\Auth;

class ValidVendor
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        $route = $request->route()->getName();
        $vendorId = Auth::user()->vendor_meta_id;

        if ($route == 'vendor_pack_edit_post') {
            $item = Pack::where('id', $request->packId)
                ->where('vendor_meta_id', $vendorId)->first();

            if (!$item)
                abort(404);
        } else if ($route == 'vendor_pack_delete' || $route == 'vendor_pack_change_status' || $route == 'vendor_pack_change_default') {
            $item = Pack::where('id', $request->id)
                ->where('vendor_meta_id', $vendorId)->first();

            if (!$item)
                abort(404);
        } else if ($route == 'vendor_color_edit_post') {
            $item = Color::where('id', $request->colorId)
                ->where('vendor_meta_id', $vendorId)->first();

            if (!$item)
                abort(404);
        } else if ($route == 'vendor_color_delete') {
            $item = Color::where('id', $request->id)
                ->where('vendor_meta_id', $vendorId)->first();

            if (!$item)
                abort(404);
        } else if ($route == 'vendor_made_in_country_update' || $route == 'vendor_made_in_country_delete' ||
            $route == 'vendor_made_in_country_change_status' || $route == 'vendor_made_in_country_change_default') {

            $item = MadeInCountry::where('id', $request->id)
                ->where('vendor_meta_id', $vendorId)->first();

            if (!$item)
                abort(404);
        } else if ($route == 'vendor_fabric_update' || $route == 'vendor_fabric_delete' ||
            $route == 'vendor_fabric_change_status' || $route == 'vendor_fabric_change_default') {

            $item = Fabric::where('id', $request->id)
                ->where('vendor_meta_id', $vendorId)->first();

            if (!$item)
                abort(404);
        } else if ($route == 'vendor_delete_account_post' || $route == 'vendor_status_update_account_post') {
            $item = User::where('id', $request->id)
                ->where('vendor_meta_id', $vendorId)->first();

            if (!$item)
                abort(404);
        } else if ($route == 'vendor_category_detail' || $route == 'vendor_category_delete') {
            $item = Category::where('id', $request->id)
                ->where('vendor_meta_id', $vendorId)->first();

            if (!$item)
                abort(404);
        } else if ($route == 'vendor_category_edit_post') {
            $item = Category::where('id', $request->categoryId)
                ->where('vendor_meta_id', $vendorId)->first();

            if (!$item)
                abort(404);
        } else if ($route == 'vendor_edit_item' || $route == 'vendor_edit_item_post' || $route == 'vendor_clone_item') {
            if (Auth::user()->role != Role::$ADMIN) {
                $item = $request->route()->parameter('item');

                if (!$item || $item->vendor_meta_id != $vendorId)
                    abort(404);
            }
        } else if ($route == 'vendor_item_list_by_category') {
            $item = $request->route()->parameter('category');

            if (!$item || $item->vendor_meta_id != $vendorId)
                abort(404);
        } else if ($route == 'vendor_item_list_change_category' || $route == 'vendor_item_list_change_to_inactive' ||
            $route == 'vendor_item_list_change_to_active' || $route == 'vendor_item_list_delete') {

            if (Auth::user()->role != Role::$ADMIN) {
                $items = Item::whereIn('id', $request->ids)
                    ->where('vendor_meta_id', $vendorId)
                    ->get();

                if (sizeof($items) != sizeof($request->ids))
                    abort(404);
            }
        } else if ($route == 'vendor_banner_delete' || $route == 'vendor_banner_active') {
            $item = VendorImage::where('id', $request->id)
                ->where('vendor_meta_id', $vendorId)->first();

            if (!$item)
                abort(404);
        } else if ($route == 'vendor_banner_item_remove') {
            $item = SliderItem::where('id', $request->id)
                ->where('vendor_meta_id', $vendorId)->first();

            if (!$item)
                abort(404);
        } else if ($route == 'vendor_banner_item_sort') {
            $items = SliderItem::whereIn('id', $request->ids)
                ->where('vendor_meta_id', $vendorId)->get();

            if (sizeof($items) != sizeof($request->ids))
                abort(404);
        } else if ($route == 'vendor_order_details' || $route == 'vendor_order_details_post') {
            if (Auth::user()->role != Role::$ADMIN) {
                $item = $request->route()->parameter('order');

                if (!$item || $item->vendor_meta_id != $vendorId)
                    abort(404);
            }
        } else if ($route == 'vendor_print_pdf' || $route == 'vendor_print_pdf_without_image' || $route == 'vendor_print_packlist') {
            if (Auth::user()->role != Role::$ADMIN) {
                $ids = explode(',', $request->order);

                $orders = Order::whereIn('id', $ids)->where('vendor_meta_id', Auth::user()->vendor_meta_id)->count();

                if (sizeof($ids) != $orders)
                    abort(404);
            }
        } else if ($route == 'vendor_create_back_order' || $route == 'vendor_out_of_stock' || $route == 'deleteOrderItem') {
            if (Auth::user()->role != Role::$ADMIN) {
                $items = OrderItem::whereIn('id', $request->ids)->with('order')->get();

                foreach ($items as $item) {
                    if ($item->order->vendor_meta_id != $vendorId)
                        abort(404);
                }
            }
        } else if ($route == 'vendor_delete_order' || $route == 'maskCardNumber') {
            if (Auth::user()->role != Role::$ADMIN) {
                $item = Order::where('id', $request->id)
                    ->where('vendor_meta_id', $vendorId)->first();

                if (!$item)
                    abort(404);
            }
        } else if ($route == 'vendor_delete_order') {
            if (Auth::user()->role != Role::$ADMIN) {
                $item = Order::where('id', $request->orderID)
                    ->where('vendor_meta_id', $vendorId)->first();

                if (!$item)
                    abort(404);
            }
        } else if ($route == 'vendor_shipping_method_edit_post') {
            $item = ShippingMethod::where('id', $request->shippingMethodId)
                ->where('vendor_meta_id', $vendorId)->first();

            if (!$item)
                abort(404);
        } else if ($route == 'vendor_shipping_method_delete' || $route == 'vendor_shipping_method_change_status' ||
            $route == 'vendor_shipping_method_change_default') {

            $item = ShippingMethod::where('id', $request->id)
                ->where('vendor_meta_id', $vendorId)->first();

            if (!$item)
                abort(404);
        } else if ($route == 'vendor_get_item_details') {
            if (Auth::user()->role != Role::$ADMIN) {
                $item = Item::where('id', $request->id)
                    ->where('vendor_meta_id', $vendorId)->first();

                if (!$item)
                    abort(404);
            }
        } else if ($route == 'vendor_order_add_item') {
            if (Auth::user()->role != Role::$ADMIN) {
                $item = Item::where('id', $request->item_id)
                    ->where('vendor_meta_id', $vendorId)->first();

                if (!$item)
                    abort(404);
            }
        }

        return $next($request);
    }
}
