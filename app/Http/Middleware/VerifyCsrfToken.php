<?php

namespace App\Http\Middleware;

use Illuminate\Foundation\Http\Middleware\VerifyCsrfToken as Middleware;

class VerifyCsrfToken extends Middleware
{
    /**
     * The URIs that should be excluded from CSRF verification.
     *
     * @var array
     */
    protected $except = [
        'vendor/create_new_item/upload/image',
        'vendor/items/import/read_file',
        'vendor/login/post',
        'admin/login/post',
        'buyer/login/post',
    ];
}
