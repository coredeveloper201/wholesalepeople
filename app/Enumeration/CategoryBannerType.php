<?php
namespace App\Enumeration;

class CategoryBannerType {
    public static $TOP_SLIDER = 1;
    public static $SMALL_BANNER = 2;
    public static $SECOND_SLIDER = 3;
    public static $TOP_BANNER = 4;
}