<?php
    namespace App\Enumeration;

    class Page {
        public static $HOME = 1;
        public static $VENDOR_ALL = 2;
        public static $NEW_ARRIVAL = 3;
        public static $PARENT_CATEGORY = 4;
        public static $VENDOR_HOME = 5;

        public static $ABOUT_US = 6;
        public static $CONTACT_US = 7;
        public static $RETURN_EXCHANGE = 8;
        public static $SHOW_SCHEDULE = 9;
        public static $PHOTO_SHOOT = 10;
        public static $SHIPPING_INFORMATION = 11;
        public static $REFUND_POLICY = 12;
        public static $PRIVACY_POLICY = 13;
        public static $TERMS_CONDITIONS = 14;
    }