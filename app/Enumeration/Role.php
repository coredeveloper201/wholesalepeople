<?php
    namespace App\Enumeration;

    class Role {
        public static $ADMIN = 1;
        public static $VENDOR = 2;
        public static $BUYER = 3;
        public static $VENDOR_EMPLOYEE = 4;
    }
?>